/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_MonsterResource
Date                  : 2023-10-07 09:07:31
*/


INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1, 114, 0, '90', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2, 128, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (3, 131, 0, '28', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (4, 149, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (5, 77, 0, '61', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (6, 76, 0, '21', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (7, 147, 0, '19', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (8, 148, 0, '20', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (9, 133, 0, '12', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (10, 115, 0, '31', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (11, 81, 0, '17', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (12, 138, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (13, 137, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (14, 136, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (15, 134, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (16, 135, 0, '10002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (17, 83, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (18, 88, 0, '434', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (19, 127, 0, '25', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (20, 153, 0, '27', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (21, 154, 0, '173', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (22, 145, 0, '15', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (23, 146, 0, '16', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (24, 93, 0, '24', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (25, 155, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (26, 98, 0, '29', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (27, 36, 0, '187', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (28, 94, 0, '63', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (29, 75, 0, '35', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (30, 156, 0, '9', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (31, 168, 0, '10011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (32, 167, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (33, 166, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (34, 164, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (35, 165, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (36, 162, 0, '10067', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (37, 161, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (38, 160, 0, '10066', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (39, 159, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (40, 158, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (41, 157, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (42, 163, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (43, 169, 0, '10002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (44, 78, 0, '55', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (45, 201, 0, '26', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (46, 202, 0, '47', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (47, 204, 0, '46', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (48, 203, 0, '43', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (49, 205, 0, '44', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (50, 206, 0, '45', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (51, 95, 0, '33', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (52, 92, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (53, 74, 0, '38', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (54, 97, 0, '180', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (55, 209, 0, '39', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (56, 232, 0, '26', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (57, 233, 0, '12', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (58, 35, 0, '192', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (59, 33, 0, '42', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (60, 234, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (61, 129, 0, '50', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (62, 86, 0, '89', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (63, 109, 0, '321', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (64, 118, 0, '321', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (65, 240, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (66, 241, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (67, 242, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (68, 243, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (69, 244, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (70, 245, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (71, 246, 2, '12', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (72, 246, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (73, 252, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (74, 253, 0, 'TestModel', 1, 3);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (75, 255, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (76, 254, 0, '10043', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (77, 99, 0, '58', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (78, 258, 0, 'TestModel', 5, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (79, 259, 0, 'TestModel', 5, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (80, 260, 0, 'TestModel', 1, 3);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (81, 261, 0, '10026', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (82, 262, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (83, 263, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (84, 264, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (85, 265, 0, '10016', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (86, 266, 0, '10014', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (87, 267, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (88, 268, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (89, 269, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (90, 270, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (91, 271, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (92, 272, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (93, 273, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (94, 274, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (95, 275, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (96, 276, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (97, 277, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (98, 278, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (99, 279, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (100, 280, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (101, 281, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (102, 282, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (103, 283, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (104, 284, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (105, 285, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (106, 286, 0, '10024', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (107, 287, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (108, 288, 0, '10014', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (109, 289, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (110, 290, 0, '10043', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (111, 291, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (112, 292, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (113, 293, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (114, 294, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (115, 295, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (116, 296, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (117, 297, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (118, 298, 0, '10002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (119, 299, 0, '10011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (120, 300, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (121, 301, 0, '10028', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (122, 302, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (123, 303, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (124, 304, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (125, 305, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (126, 306, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (127, 307, 0, '10002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (128, 308, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (129, 309, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (130, 310, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (131, 311, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (132, 312, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (133, 313, 0, '10008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (134, 314, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (135, 315, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (136, 316, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (137, 317, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (138, 318, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (139, 319, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (140, 320, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (141, 321, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (142, 322, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (143, 323, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (144, 324, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (145, 325, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (146, 326, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (147, 327, 0, '10012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (148, 328, 0, '10013', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (149, 329, 0, '10009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (150, 330, 0, '10016', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (151, 331, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (152, 332, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (153, 333, 0, '10067', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (154, 334, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (155, 335, 0, '10007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (156, 336, 0, '13', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (157, 79, 0, '52', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (158, 80, 0, '51', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (159, 337, 0, '10095', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (160, 338, 0, '10008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (161, 339, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (162, 340, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (163, 341, 0, '10009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (164, 342, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (165, 343, 0, '10012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (166, 344, 0, '10011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (167, 345, 0, '10011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (168, 346, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (169, 347, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (170, 348, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (171, 349, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (172, 350, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (173, 351, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (174, 352, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (175, 353, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (176, 354, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (177, 355, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (178, 356, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (179, 357, 0, '10040', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (180, 358, 0, '161', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (181, 359, 0, '106', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (182, 360, 0, '117', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (183, 361, 0, '116', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (184, 362, 0, '109', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (185, 363, 0, '112', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (186, 364, 0, '108', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (187, 365, 0, '115', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (188, 366, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (189, 368, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (190, 369, 0, '110', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (191, 370, 0, '20', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (192, 371, 0, '113', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (193, 372, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (194, 373, 0, '10011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (195, 374, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (196, 375, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (197, 376, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (198, 377, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (199, 378, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (200, 379, 0, '10013', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (201, 380, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (202, 381, 0, '10012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (203, 382, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (204, 383, 0, '10014', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (205, 384, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (206, 385, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (207, 386, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (208, 387, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (209, 388, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (210, 389, 0, '60', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (211, 85, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (212, 390, 0, '85', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (213, 391, 0, '80', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (214, 394, 0, '85', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (215, 393, 0, '86', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (216, 392, 0, '53', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (217, 395, 0, '22', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (218, 130, 0, '68', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (219, 396, 0, '22', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (220, 398, 0, '62', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (221, 397, 0, '171', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (222, 82, 0, '57', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (223, 400, 0, '94', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (224, 399, 0, '93', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (225, 102, 0, '70', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (226, 401, 0, '323', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (227, 414, 0, '200', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (228, 411, 0, '68', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (229, 413, 0, '68', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (230, 415, 0, '22', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (231, 406, 0, '65', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (232, 407, 0, '160', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (233, 90, 0, '183', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (234, 34, 0, '186', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (235, 404, 0, '36', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (236, 104, 0, '66', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (237, 419, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (238, 403, 0, '81', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (239, 412, 0, '88', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (240, 418, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (241, 402, 0, '91', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (242, 410, 0, '2', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (243, 37, 0, '54', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (244, 405, 0, '87', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (245, 420, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (246, 421, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (247, 422, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (248, 423, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (249, 424, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (250, 431, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (251, 430, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (252, 429, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (253, 428, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (254, 425, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (255, 433, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (256, 432, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (257, 427, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (258, 426, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (259, 87, 0, '97', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (260, 435, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (261, 416, 0, '138', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (262, 247, 0, '10024', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (263, 106, 0, '100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (264, 125, 0, '441', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (265, 124, 0, '89', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (266, 123, 0, '89', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (267, 122, 0, '139', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (268, 121, 0, '142', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (269, 120, 0, '141', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (270, 45, 0, '148', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (271, 46, 0, '89', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (272, 49, 0, '29', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (273, 48, 0, '188', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (274, 117, 0, '144', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (275, 113, 0, '145', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (276, 112, 0, '146', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (277, 111, 0, '147', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (278, 43, 0, '149', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (279, 41, 0, '150', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (280, 107, 0, '99', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (281, 105, 0, '101', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (282, 73, 0, '102', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (283, 197, 0, '103', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (284, 417, 0, '71', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (285, 436, 0, '29', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (286, 437, 0, '438', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (287, 438, 0, '30', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (288, 439, 0, '40', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (289, 440, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (290, 441, 0, '48', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (291, 442, 0, '49', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (292, 443, 0, '445', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (293, 445, 0, '17', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (294, 446, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (295, 447, 0, '243', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (296, 448, 0, '63', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (297, 449, 0, '242', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (298, 450, 0, '67', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (299, 451, 0, '54', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (300, 452, 0, '205', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (301, 501, 0, '181', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (302, 480, 0, '435', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (303, 479, 0, '436', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (304, 481, 0, '38', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (305, 455, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (306, 457, 0, '226', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (307, 454, 0, '73', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (308, 458, 0, '232', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (309, 456, 0, '224', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (310, 468, 0, '231', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (311, 469, 0, '230', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (312, 470, 0, '233', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (313, 482, 1, '', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (314, 478, 0, '22', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (315, 487, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (316, 476, 0, '22', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (317, 489, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (318, 488, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (319, 502, 0, '48', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (320, 498, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (321, 505, 0, '136', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (322, 503, 0, '89', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (323, 504, 0, '91', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (324, 507, 0, '437', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (325, 508, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (326, 509, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (327, 510, 0, '25', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (328, 511, 0, '35', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (329, 500, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (330, 512, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (331, 513, 0, '91', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (332, 514, 0, '36', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (333, 499, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (334, 515, 0, '433', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (335, 516, 0, '30', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (336, 495, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (337, 518, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (338, 519, 0, '40', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (339, 520, 0, '179', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (340, 521, 0, '154', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (341, 524, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (342, 506, 0, '36', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (343, 525, 0, '36', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (344, 526, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (345, 528, 0, '91', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (346, 529, 0, '30', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (347, 530, 0, '40', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (348, 531, 0, '59', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (349, 533, 0, '36', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (350, 475, 0, '22', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (351, 482, 0, '51', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (352, 494, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (353, 477, 0, '157', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (354, 497, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (355, 492, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (356, 490, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (357, 491, 0, '32', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (358, 459, 0, '64', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (359, 453, 0, '76', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (360, 463, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (361, 461, 0, '108', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (362, 460, 0, '27', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (363, 462, 0, '27', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (364, 472, 0, '158', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (365, 473, 0, '163', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (366, 496, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (367, 486, 0, '155', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (368, 484, 0, '31', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (369, 485, 0, '31', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (370, 483, 0, '156', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (371, 464, 0, '225', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (372, 471, 0, '166', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (373, 467, 0, '29', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (374, 466, 0, '229', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (375, 70, 0, '107', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (376, 465, 0, '432', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (377, 546, 0, '28', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (378, 545, 0, '29', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (379, 544, 0, '15', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (380, 543, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (381, 542, 0, '38', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (382, 541, 0, '21', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (383, 548, 0, '40', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (384, 547, 0, '63', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (385, 408, 0, '75', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (386, 38, 0, '72', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (387, 96, 0, '172', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (388, 549, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (389, 550, 0, '98', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (390, 538, 0, '114', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (391, 409, 0, '78', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (392, 522, 0, '439', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (393, 557, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (394, 556, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (395, 555, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (396, 554, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (397, 553, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (398, 552, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (399, 551, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (400, 559, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (401, 560, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (402, 558, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (403, 561, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (404, 562, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (405, 563, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (406, 564, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (407, 565, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (408, 566, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (409, 567, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (410, 568, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (411, 569, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (412, 570, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (413, 573, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (414, 572, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (415, 571, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (416, 574, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (417, 540, 0, '453', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (418, 575, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (419, 576, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (420, 577, 0, '424', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (421, 578, 0, '62', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (422, 579, 0, '111', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (423, 517, 0, '56', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (424, 91, 0, '56', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (425, 493, 0, '54', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (426, 101, 0, '67', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (427, 535, 0, '67', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (428, 599, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (429, 598, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (430, 597, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (431, 596, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (432, 595, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (433, 590, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (434, 591, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (435, 592, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (436, 593, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (437, 594, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (438, 580, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (439, 581, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (440, 582, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (441, 583, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (442, 584, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (443, 585, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (444, 586, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (445, 587, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (446, 588, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (447, 589, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (448, 534, 0, '54', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (449, 444, 0, '440', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (450, 536, 0, '254', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (451, 600, 0, '10066', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (452, 601, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (453, 602, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (454, 609, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (455, 610, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (456, 606, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (457, 607, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (458, 608, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (459, 605, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (460, 604, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (461, 603, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (462, 626, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (463, 625, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (464, 624, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (465, 623, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (466, 622, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (467, 638, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (468, 637, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (469, 636, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (470, 635, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (471, 634, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (472, 633, 0, '10002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (473, 632, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (474, 631, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (475, 630, 0, '10007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (476, 629, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (477, 628, 0, '10034', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (478, 627, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (479, 640, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (480, 639, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (481, 645, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (482, 644, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (483, 643, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (484, 642, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (485, 641, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (486, 100, 0, '83', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (487, 84, 0, '104', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (488, 662, 0, '127', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (489, 661, 0, '132', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (490, 660, 0, '131', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (491, 659, 0, '123', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (492, 658, 0, '128', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (493, 657, 0, '129', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (494, 656, 0, '120', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (495, 655, 0, '122', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (496, 654, 0, '77', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (497, 653, 0, '134', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (498, 652, 0, '121', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (499, 651, 0, '69', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (500, 650, 0, '125', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (501, 649, 0, '124', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (502, 648, 0, '126', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (503, 647, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (504, 646, 0, '74', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (505, 663, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (506, 664, 0, '119', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (507, 665, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (508, 666, 0, '87', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (509, 667, 0, '67', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (510, 669, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (511, 668, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (512, 670, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (513, 678, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (514, 677, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (515, 676, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (516, 675, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (517, 674, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (518, 673, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (519, 672, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (520, 671, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (521, 679, 0, '10033', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (522, 680, 0, '10027', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (523, 681, 0, '10030', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (524, 682, 0, '10067', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (525, 683, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (526, 684, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (527, 686, 0, '10032', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (528, 687, 0, '10012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (529, 688, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (530, 689, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (531, 690, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (532, 685, 0, '10024', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (533, 691, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (534, 693, 0, '92', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (535, 692, 0, '79', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (536, 694, 0, '24', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (537, 696, 0, '133', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (538, 697, 0, '84', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (539, 698, 0, '135', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (540, 695, 0, '10043', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (541, 699, 0, '151', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (542, 700, 0, '10048', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (543, 706, 0, '24', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (544, 705, 0, '137', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (545, 703, 0, '77', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (546, 702, 0, '428', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (547, 701, 0, '121', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (548, 704, 0, '125', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (549, 621, 0, '10049', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (550, 620, 0, '270', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (551, 619, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (552, 618, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (553, 617, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (554, 616, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (555, 615, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (556, 614, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (557, 613, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (558, 612, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (559, 611, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (560, 708, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (561, 707, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (562, 709, 0, '10049', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (563, 710, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (564, 711, 0, '33', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (565, 712, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (566, 714, 0, '10084', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (567, 713, 0, '10084', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (568, 715, 0, '10084', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (569, 716, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (570, 717, 0, '10050', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (571, 722, 0, '10042', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (572, 721, 0, '10042', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (573, 720, 0, '10042', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (574, 719, 0, '10013', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (575, 718, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (576, 725, 0, '10117', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (577, 724, 0, '10117', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (578, 723, 0, '10117', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (579, 726, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (580, 727, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (581, 733, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (582, 732, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (583, 731, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (584, 730, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (585, 734, 0, '10084', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (586, 728, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (587, 40, 0, '201', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (588, 735, 0, '169', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (589, 736, 0, '176', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (590, 737, 0, '177', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (591, 738, 0, '175', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (592, 739, 0, '191', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (593, 740, 0, '189', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (594, 741, 0, '178', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (595, 742, 0, '182', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (596, 532, 0, '152', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (597, 527, 0, '153', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (598, 523, 0, '168', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (599, 474, 0, '174', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (600, 119, 0, '164', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (601, 108, 0, '167', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (602, 103, 0, '162', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (603, 89, 0, '170', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (604, 757, 0, '10034', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (605, 756, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (606, 755, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (607, 754, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (608, 753, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (609, 752, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (610, 751, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (611, 750, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (612, 748, 0, '10067', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (613, 747, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (614, 746, 0, '10024', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (615, 745, 0, '10043', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (616, 744, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (617, 743, 0, '10046', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (618, 761, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (619, 760, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (620, 759, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (621, 758, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (622, 110, 0, '165', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (623, 749, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (624, 763, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (625, 762, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (626, 764, 0, '10029', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (627, 765, 0, '10041', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (628, 766, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (629, 767, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (630, 768, 0, '323', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (631, 769, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (632, 792, 0, '132', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (633, 793, 0, '20000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (634, 537, 0, '130', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (635, 794, 0, '210', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (636, 795, 0, '214', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (637, 796, 0, '221', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (638, 797, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (639, 804, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (640, 803, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (641, 802, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (642, 801, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (643, 800, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (644, 799, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (645, 798, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (646, 812, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (647, 805, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (648, 806, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (649, 807, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (650, 808, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (651, 809, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (652, 810, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (653, 811, 0, '222', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (654, 813, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (655, 814, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (656, 815, 0, '20001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (657, 816, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (658, 196, 0, '206', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (659, 817, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (660, 818, 0, '223', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (661, 819, 0, '202', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (662, 820, 0, '211', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (663, 821, 0, '212', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (664, 822, 0, '213', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (665, 823, 0, '215', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (666, 824, 0, '216', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (667, 825, 0, '217', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (668, 826, 0, '236', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (669, 827, 0, '237', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (670, 828, 0, '203', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (671, 829, 0, '204', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (672, 831, 0, '207', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (673, 832, 0, '208', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (674, 833, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (675, 834, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (676, 836, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (677, 837, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (678, 839, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (679, 838, 0, '234', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (680, 840, 0, '238', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (681, 841, 0, '239', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (682, 842, 0, '240', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (683, 843, 0, '241', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (684, 844, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (685, 846, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (686, 845, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (687, 847, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (688, 830, 0, '206', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (689, 835, 0, '219', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (690, 849, 0, '252', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (691, 848, 0, '244', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (692, 850, 0, '245', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (693, 851, 0, '246', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (694, 852, 0, '247', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (695, 853, 0, '248', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (696, 854, 0, '251', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (697, 855, 0, '250', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (698, 856, 0, '10051', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (699, 857, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (700, 858, 0, '252', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (701, 859, 0, '10131', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (702, 860, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (703, 861, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (704, 862, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (705, 863, 0, '244', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (706, 864, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (707, 865, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (708, 866, 0, '244', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (709, 867, 0, '10051', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (710, 868, 0, '258', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (711, 869, 0, '259', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (712, 870, 0, '260', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (713, 871, 0, '261', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (714, 872, 0, '262', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (715, 873, 0, '263', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (716, 874, 0, '264', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (717, 875, 0, '265', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (718, 876, 0, '266', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (719, 877, 0, '267', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (720, 878, 0, '268', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (721, 879, 0, '269', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (722, 880, 0, '270', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (723, 881, 0, '271', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (724, 882, 0, '272', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (725, 883, 0, '273', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (726, 884, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (727, 885, 0, '10044', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (728, 886, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (729, 887, 0, '10054', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (730, 888, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (731, 889, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (732, 890, 0, '10044', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (733, 891, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (734, 892, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (735, 893, 0, '10040', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (736, 894, 0, '274', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (737, 895, 0, '270', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (738, 791, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (739, 790, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (740, 789, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (741, 788, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (742, 896, 0, '259', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (743, 897, 0, '260', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (744, 898, 0, '256', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (745, 899, 0, '255', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (746, 787, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (747, 786, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (748, 785, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (749, 784, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (750, 783, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (751, 782, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (752, 781, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (753, 780, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (754, 779, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (755, 778, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (756, 777, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (757, 776, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (758, 775, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (759, 774, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (760, 773, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (761, 772, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (762, 771, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (763, 770, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (764, 900, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (765, 901, 0, '10027', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (766, 902, 0, '10044', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (767, 903, 0, '30000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (768, 904, 0, '30001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (769, 905, 0, '30002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (770, 906, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (771, 907, 0, '276', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (772, 908, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (773, 909, 0, '10051', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (774, 910, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (775, 911, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (776, 912, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (777, 913, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (778, 914, 0, '277', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (779, 915, 0, '10046', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (780, 916, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (781, 917, 0, '279', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (782, 918, 0, '280', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (783, 919, 0, '281', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (784, 920, 0, '282', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (785, 921, 0, '283', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (786, 922, 0, '284', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (787, 923, 0, '285', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (788, 924, 0, '286', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (789, 925, 0, '290', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (790, 926, 0, '10039', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (791, 927, 0, '10039', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (792, 929, 0, '10065', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (793, 930, 0, '10065', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (794, 928, 0, '10045', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (795, 931, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (796, 932, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (797, 933, 0, '10038', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (798, 934, 0, '10036', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (799, 935, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (800, 936, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (801, 937, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (802, 939, 0, '10027', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (803, 940, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (804, 941, 0, '10117', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (805, 942, 0, '268', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (806, 943, 0, '273', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (807, 944, 0, '10037', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (808, 938, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (809, 945, 0, '10058', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (810, 946, 0, '301', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (811, 947, 0, '10064', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (812, 948, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (813, 949, 0, '322', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (814, 950, 0, '325', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (815, 951, 0, '324', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (816, 953, 0, '10043', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (817, 954, 0, '10038', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (818, 955, 0, '327', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (819, 956, 0, '328', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (820, 957, 0, '329', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (821, 958, 0, '10068', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (822, 960, 0, '10050', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (823, 961, 0, '10050', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (824, 962, 0, '10050', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (825, 963, 0, '10050', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (826, 959, 0, '10071', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (827, 964, 0, '10072', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (828, 965, 0, '10055', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (829, 966, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (830, 967, 0, '10094', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (831, 968, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (832, 969, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (833, 970, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (834, 971, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (835, 972, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (836, 973, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (837, 974, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (838, 975, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (839, 976, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (840, 977, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (841, 978, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (842, 979, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (843, 980, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (844, 981, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (845, 982, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (846, 983, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (847, 984, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (848, 985, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (849, 986, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (850, 987, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (851, 988, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (852, 989, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (853, 990, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (854, 991, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (855, 992, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (856, 993, 0, '40005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (857, 994, 0, '40004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (858, 995, 0, '332', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (859, 996, 0, '40000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (860, 997, 0, '40006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (861, 998, 0, '40002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (862, 999, 0, '303', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (863, 1000, 0, '304', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (864, 1001, 0, '310', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (865, 1002, 0, '313', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (866, 1003, 0, '318', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (867, 1004, 0, '309', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (868, 1005, 0, '312', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (869, 1006, 0, '314', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (870, 1007, 0, '316', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (871, 1008, 0, '305', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (872, 1009, 0, '300', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (873, 1010, 0, '302', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (874, 1011, 0, '336', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (875, 1012, 0, '308', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (876, 1013, 0, '307', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (877, 1014, 0, '335', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (878, 1015, 0, '339', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (879, 1016, 0, '317', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (880, 1017, 0, '315', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (881, 1018, 0, '320', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (882, 1019, 0, '319', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (883, 1020, 0, '326', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (884, 1021, 0, '332', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (885, 1022, 0, '333', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (886, 1023, 0, '337', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (887, 1024, 0, '10062', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (888, 1025, 0, '10060', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (889, 1026, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (890, 1027, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (891, 1028, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (892, 1029, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (893, 1030, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (894, 1031, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (895, 1032, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (896, 1033, 0, '10012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (897, 1034, 0, '10011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (898, 1035, 0, '10009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (899, 1036, 0, '10061', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (900, 1037, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (901, 1038, 0, '10070', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (902, 1039, 0, '10073', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (903, 1040, 0, '10075', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (904, 1053, 0, '10034', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (905, 1052, 0, '30001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (906, 1051, 0, '10076', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (907, 1050, 0, '10061', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (908, 1049, 0, '10073', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (909, 1048, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (910, 1047, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (911, 1042, 0, '10014', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (912, 1046, 0, '10007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (913, 1045, 0, '10032', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (914, 1044, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (915, 1041, 0, '10069', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (916, 1054, 0, '10076', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (917, 1055, 0, '10074', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (918, 1056, 0, '10060', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (919, 1057, 0, '10027', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (920, 1058, 0, '338', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (921, 1059, 0, '334', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (922, 1060, 0, '331', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (923, 1061, 0, '10036', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (924, 1062, 0, '10037', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (925, 1063, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (926, 1064, 0, '10004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (927, 1066, 0, '10067', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (928, 1043, 0, '10069', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (929, 367, 0, '10066', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (930, 1067, 0, '10065', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (931, 1068, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (932, 1070, 0, '362', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (933, 1071, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (934, 1074, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (935, 1073, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (936, 1072, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (937, 1075, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (938, 1076, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (939, 1077, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (940, 1078, 0, '341', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (941, 1079, 0, '348', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (942, 1080, 0, '340', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (943, 1081, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (944, 1082, 0, '200', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (945, 1083, 0, '200', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (946, 1084, 0, '200', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (947, 1087, 0, '324', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (948, 1086, 0, '324', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (949, 1085, 0, '324', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (950, 1088, 0, '255', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (951, 1089, 0, '255', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (952, 1090, 0, '255', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (953, 1091, 0, '290', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (954, 1092, 0, '290', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (955, 1093, 0, '290', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (956, 1094, 0, '285', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (957, 1095, 0, '285', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (958, 1096, 0, '286', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (959, 1097, 0, '286', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (960, 1098, 0, '334', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (961, 1099, 0, '338', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (962, 1100, 0, '331', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (963, 1101, 0, '331', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (964, 1102, 0, '331', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (965, 1069, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (966, 1154, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (967, 1153, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (968, 1152, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (969, 1151, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (970, 1150, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (971, 1149, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (972, 1148, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (973, 1147, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (974, 1146, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (975, 1145, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (976, 1144, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (977, 1143, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (978, 1142, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (979, 1141, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (980, 1140, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (981, 1139, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (982, 1138, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (983, 1137, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (984, 1136, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (985, 1135, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (986, 1134, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (987, 1133, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (988, 1132, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (989, 1131, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (990, 1130, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (991, 1129, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (992, 1128, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (993, 1127, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (994, 1126, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (995, 1125, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (996, 1124, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (997, 1123, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (998, 1122, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (999, 1121, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1000, 1120, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1001, 1119, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1002, 1118, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1003, 1117, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1004, 1116, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1005, 1115, 0, '10081', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1006, 1174, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1007, 1173, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1008, 1172, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1009, 1171, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1010, 1170, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1011, 1169, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1012, 1168, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1013, 1167, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1014, 1166, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1015, 1165, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1016, 1164, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1017, 1163, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1018, 1162, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1019, 1161, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1020, 1160, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1021, 1159, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1022, 1158, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1023, 1157, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1024, 1156, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1025, 1155, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1026, 1176, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1027, 1177, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1028, 1178, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1029, 1179, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1030, 1180, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1031, 1181, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1032, 1182, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1033, 1183, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1034, 1175, 0, '10082', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1035, 1184, 0, '357', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1036, 1185, 0, '360', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1037, 1186, 0, '370', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1038, 1187, 0, '361', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1039, 1188, 0, '356', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1040, 1189, 0, '363', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1041, 1190, 0, '369', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1042, 1191, 0, '349', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1043, 1192, 0, '359', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1044, 1193, 0, '355', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1045, 1194, 0, '358', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1046, 1195, 0, '40007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1047, 1196, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1048, 1197, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1049, 1198, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1050, 1199, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1051, 1200, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1052, 1201, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1053, 1202, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1054, 1203, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1055, 1206, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1056, 1207, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1057, 1208, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1058, 1209, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1059, 1210, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1060, 1211, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1061, 1212, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1062, 1213, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1063, 1214, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1064, 1215, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1065, 1216, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1066, 1217, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1067, 1218, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1068, 1219, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1069, 1220, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1070, 1221, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1071, 1222, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1072, 1223, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1073, 1224, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1074, 1225, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1075, 1226, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1076, 1227, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1077, 1103, 0, '345', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1078, 1107, 0, '346', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1079, 1111, 0, '347', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1080, 1104, 0, '364', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1081, 1108, 0, '365', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1082, 1112, 0, '366', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1083, 1105, 0, '342', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1084, 1109, 0, '343', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1085, 1113, 0, '344', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1086, 1106, 0, '352', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1087, 1110, 0, '353', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1088, 1114, 0, '354', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1089, 1205, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1090, 1204, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1091, 1228, 0, '488', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1092, 1229, 0, '352', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1093, 1230, 0, '656', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1094, 1232, 0, '373', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1095, 1231, 0, '483', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1096, 1233, 0, '355', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1097, 1234, 0, '359', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1098, 1235, 0, '349', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1099, 1236, 0, '358', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1100, 1237, 0, '620', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1101, 1238, 0, '658', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1102, 1239, 0, '622', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1103, 1240, 0, '623', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1104, 1241, 0, '489', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1105, 1242, 0, '366', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1106, 1243, 0, '509', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1107, 1244, 0, '484', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1108, 1245, 0, '367', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1109, 1246, 0, '626', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1110, 1247, 0, '664', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1111, 1248, 0, '667', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1112, 1249, 0, '668', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1113, 1251, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1114, 1250, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1115, 1252, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1116, 1253, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1117, 1254, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1118, 1255, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1119, 1256, 0, '379', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1120, 1257, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1121, 1258, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1122, 1259, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1123, 1260, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1124, 1261, 0, '379', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1125, 1262, 0, '379', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1126, 1263, 0, '379', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1127, 1264, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1128, 1265, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1129, 1266, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1130, 1267, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1131, 1268, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1132, 1269, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1133, 1270, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1134, 1271, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1135, 1272, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1136, 1273, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1137, 1274, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1138, 1275, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1139, 1276, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1140, 1277, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1141, 1278, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1142, 1279, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1143, 1280, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1144, 1281, 0, '10045', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1145, 1282, 0, '87', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1146, 1286, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1147, 1287, 0, '10044', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1148, 1285, 0, '15', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1149, 1284, 0, '16', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1150, 1283, 0, '27', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1151, 1288, 0, '391', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1152, 1289, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1153, 1290, 0, '10083', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1154, 1291, 0, '10088', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1155, 1292, 0, '10086', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1156, 1293, 0, '10087', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1157, 1294, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1158, 1295, 0, '338', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1159, 1296, 0, '10068', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1160, 1297, 0, '10068', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1161, 1298, 0, '10090', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1162, 1299, 0, '10090', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1163, 1300, 0, '10090', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1164, 1301, 0, '10090', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1165, 1302, 0, '10089', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1166, 1303, 0, '10089', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1167, 1304, 0, '10089', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1168, 1305, 0, '10089', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1169, 1306, 0, '10091', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1170, 1307, 0, '10041', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1171, 1308, 0, '400', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1172, 1309, 0, '385', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1173, 1310, 0, '390', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1174, 1311, 0, '395', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1175, 1312, 0, '398', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1176, 1314, 0, '386', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1177, 1315, 0, '368', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1178, 1316, 0, '389', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1179, 1317, 0, '372', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1180, 1318, 0, '393', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1181, 1320, 0, '388', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1182, 1321, 0, '384', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1183, 1319, 0, '377', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1184, 1313, 0, '392', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1185, 1334, 0, '383', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1186, 1335, 0, '397', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1187, 1336, 0, '380', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1188, 1337, 0, '381', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1189, 1338, 0, '387', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1190, 1339, 0, '394', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1191, 1340, 0, '399', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1192, 1341, 0, '404', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1193, 1322, 0, '375', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1194, 1323, 0, '382', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1195, 1324, 0, '406', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1196, 1331, 0, '378', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1197, 1332, 0, '374', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1198, 1333, 0, '408', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1199, 1326, 0, '373', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1200, 1325, 0, '396', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1201, 1327, 0, '409', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1202, 1328, 0, '367', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1203, 1329, 0, '371', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1204, 1330, 0, '412', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1205, 1342, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1206, 1343, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1207, 1344, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1208, 1345, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1209, 1346, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1210, 1347, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1211, 1348, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1212, 1349, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1213, 1351, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1214, 1350, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1215, 1362, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1216, 1360, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1217, 1361, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1218, 1363, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1219, 1359, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1220, 1356, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1221, 1357, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1222, 1358, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1223, 1355, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1224, 1353, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1225, 1352, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1226, 1354, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1227, 1366, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1228, 1365, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1229, 1367, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1230, 1364, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1231, 1383, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1232, 1377, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1233, 1376, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1234, 1381, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1235, 1379, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1236, 1378, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1237, 1382, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1238, 1380, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1239, 1388, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1240, 1389, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1241, 1390, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1242, 1386, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1243, 1385, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1244, 1384, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1245, 1387, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1246, 1391, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1247, 1368, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1248, 1369, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1249, 1370, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1250, 1371, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1251, 1374, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1252, 1375, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1253, 1372, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1254, 1373, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1255, 1394, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1256, 1398, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1257, 1396, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1258, 1397, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1259, 1399, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1260, 1392, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1261, 1395, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1262, 1393, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1263, 1406, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1264, 1404, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1265, 1407, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1266, 1400, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1267, 1403, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1268, 1402, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1269, 1401, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1270, 1405, 0, '10080', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1271, 1417, 0, '383', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1272, 1418, 0, '397', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1273, 1419, 0, '387', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1274, 1422, 0, '403', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1275, 1421, 0, '399', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1276, 1420, 0, '372', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1277, 1423, 0, '345', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1278, 1424, 0, '364', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1279, 1425, 0, '342', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1280, 1426, 0, '352', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1281, 1427, 0, '346', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1282, 1428, 0, '343', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1283, 1429, 0, '353', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1284, 1430, 0, '365', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1285, 1431, 0, '347', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1286, 1432, 0, '366', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1287, 1433, 0, '344', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1288, 1434, 0, '354', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1289, 1435, 0, '375', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1290, 1436, 0, '396', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1291, 1437, 0, '367', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1292, 1439, 0, '382', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1293, 1438, 0, '378', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1294, 1440, 0, '373', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1295, 1441, 0, '371', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1296, 1442, 0, '374', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1297, 1443, 0, '406', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1298, 1444, 0, '409', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1299, 1445, 0, '412', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1300, 1446, 0, '408', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1301, 1449, 0, '413', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1302, 1492, 0, '25', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1303, 1493, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1304, 1447, 0, '10051', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1305, 1448, 0, '330', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1306, 1494, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1307, 1495, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1308, 1496, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1309, 1497, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1310, 1498, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1311, 1499, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1312, 1500, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1313, 1501, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1314, 1450, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1315, 1451, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1316, 1452, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1317, 1453, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1318, 1454, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1319, 1455, 0, '30000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1320, 1456, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1321, 1457, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1322, 1458, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1323, 1459, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1324, 1460, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1325, 1461, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1326, 1462, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1327, 1463, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1328, 1464, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1329, 1465, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1330, 1466, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1331, 1467, 0, '30000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1332, 1468, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1333, 1469, 0, '30001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1334, 1470, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1335, 1471, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1336, 1472, 0, '30001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1337, 1473, 0, '30001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1338, 1474, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1339, 1475, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1340, 1476, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1341, 1477, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1342, 1478, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1343, 1479, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1344, 1480, 0, '30001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1345, 1481, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1346, 1482, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1347, 1483, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1348, 1484, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1349, 1486, 0, '30000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1350, 1485, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1351, 1487, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1352, 1488, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1353, 1489, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1354, 1490, 0, '10054', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1355, 1491, 0, '30002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1356, 1502, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1357, 1503, 0, '10005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1358, 1505, 0, '181', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1359, 1506, 0, '303', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1360, 1507, 0, '413', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1361, 1508, 0, '322', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1362, 1509, 0, '269', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1363, 1510, 0, '331', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1364, 1511, 0, '338', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1365, 1512, 0, '341', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1366, 1513, 0, '285', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1367, 1514, 0, '265', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1368, 1516, 0, '264', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1369, 1517, 0, '168', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1370, 1518, 0, '304', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1371, 1519, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1372, 1520, 0, '325', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1373, 1521, 0, '188', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1374, 1522, 0, '348', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1375, 1524, 0, '261', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1376, 1525, 0, '70', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1377, 1526, 0, '114', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1378, 1527, 0, '105', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1379, 1515, 0, '136', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1380, 1523, 0, '281', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1381, 1528, 0, '130', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1382, 1529, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1383, 1415, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1384, 1414, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1385, 1413, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1386, 1412, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1387, 1411, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1388, 1410, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1389, 1409, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1390, 1408, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1391, 1530, 0, '401', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1392, 1531, 0, '405', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1393, 1532, 0, '414', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1394, 1534, 0, '402', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1395, 1533, 1, '', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1396, 1535, 0, '410', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1397, 1537, 0, '410', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1398, 1539, 0, '410', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1399, 1538, 0, '410', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1400, 1540, 0, '410', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1401, 1536, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1402, 1541, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1403, 1542, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1404, 1543, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1405, 1544, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1406, 1545, 0, '379', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1407, 1546, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1408, 1533, 0, '407', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1409, 1547, 0, '407', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1410, 1504, 0, '274', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1411, 1548, 0, '10084', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1412, 1416, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1413, 1549, 0, '334', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1414, 1550, 0, '334', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1415, 1551, 0, '413', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1416, 1552, 0, '331', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1417, 1553, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1418, 1554, 0, '345', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1419, 1555, 0, '364', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1420, 1556, 0, '342', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1421, 1557, 0, '352', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1422, 1558, 0, '346', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1423, 1559, 0, '365', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1424, 1560, 0, '343', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1425, 1561, 0, '353', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1426, 1563, 0, '334', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1427, 1562, 0, '334', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1428, 1564, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1429, 1569, 0, '23', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1430, 1573, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1431, 1568, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1432, 1572, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1433, 1567, 0, '34', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1434, 1571, 0, '83', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1435, 1566, 0, '83', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1436, 1570, 0, '42', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1437, 1565, 0, '42', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1438, 1575, 0, '10016', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1439, 1576, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1440, 1577, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1441, 1585, 0, '354', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1442, 1584, 0, '344', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1443, 1580, 0, '343', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1444, 1578, 0, '346', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1445, 1583, 0, '366', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1446, 1579, 0, '365', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1447, 1581, 0, '353', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1448, 1582, 0, '347', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1449, 1609, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1450, 1608, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1451, 1607, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1452, 1605, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1453, 1606, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1454, 1604, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1455, 1603, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1456, 1602, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1457, 1601, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1458, 1600, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1459, 1599, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1460, 1598, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1461, 1597, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1462, 1596, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1463, 1595, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1464, 1594, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1465, 1592, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1466, 1591, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1467, 1590, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1468, 1589, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1469, 1588, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1470, 1587, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1471, 1586, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1472, 1593, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1473, 1610, 0, '345', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1474, 1611, 0, '364', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1475, 1612, 0, '342', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1476, 1613, 0, '352', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1477, 1574, 0, '10066', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1478, 1614, 0, '10066', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1479, 1615, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1480, 1616, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1481, 1617, 0, '10034', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1482, 1618, 0, '10034', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1483, 1619, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1484, 1620, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1485, 1621, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1486, 1622, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1487, 1623, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1488, 1624, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1489, 1625, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1490, 1626, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1491, 1627, 0, '10066', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1492, 1630, 0, '413', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1493, 1629, 0, '326', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1494, 1628, 0, '333', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1495, 1631, 0, '446', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1496, 1632, 0, '447', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1497, 1633, 0, '416', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1498, 1634, 0, '431', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1499, 1636, 0, '430', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1500, 1635, 0, '455', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1501, 1637, 0, '422', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1502, 1638, 0, '425', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1503, 1639, 0, '452', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1504, 1640, 0, '427', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1505, 1641, 0, '421', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1506, 1642, 0, '448', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1507, 1643, 0, '451', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1508, 1644, 0, '426', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1509, 1645, 0, '417', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1510, 1646, 0, '423', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1511, 1647, 0, '450', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1512, 1648, 0, '429', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1513, 1649, 0, '10007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1514, 1654, 0, '270', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1515, 1650, 0, '10009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1516, 1651, 0, '10007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1517, 1652, 0, '10009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1518, 1653, 0, '10008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1519, 1657, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1520, 1655, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1521, 1656, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1522, 1659, 0, '10042', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1523, 1658, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1524, 1688, 0, '367', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1525, 1689, 0, '326', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1526, 1690, 0, '333', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1527, 1710, 0, '76', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1528, 1712, 0, '166', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1529, 1711, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1530, 1713, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1531, 1714, 0, '10063', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1532, 1715, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1533, 1716, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1534, 1717, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1535, 1718, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1536, 1719, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1537, 1720, 0, '10118', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1538, 1721, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1539, 1722, 0, '10118', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1540, 1723, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1541, 1660, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1542, 1661, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1543, 1662, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1544, 1663, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1545, 1664, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1546, 1665, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1547, 1666, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1548, 1667, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1549, 1687, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1550, 1686, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1551, 1685, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1552, 1684, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1553, 1668, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1554, 1669, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1555, 1671, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1556, 1670, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1557, 1672, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1558, 1673, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1559, 1674, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1560, 1675, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1561, 1677, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1562, 1676, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1563, 1678, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1564, 1680, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1565, 1681, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1566, 1682, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1567, 1683, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1568, 1679, 0, '10100', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1569, 1691, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1570, 1692, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1571, 1693, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1572, 1694, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1573, 1695, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1574, 1696, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1575, 1697, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1576, 1698, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1577, 1699, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1578, 1700, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1579, 1701, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1580, 1702, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1581, 1703, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1582, 1704, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1583, 1705, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1584, 1706, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1585, 1707, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1586, 1708, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1587, 1709, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1588, 1724, 0, '419', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1589, 1725, 0, '420', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1590, 1726, 0, '415', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1591, 1727, 0, '418', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1592, 1728, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1593, 1729, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1594, 1730, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1595, 1731, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1596, 1732, 0, '479', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1597, 1733, 0, '480', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1598, 1734, 0, '481', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1599, 1735, 0, '482', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1600, 1741, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1601, 1742, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1602, 1743, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1603, 1744, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1604, 1745, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1605, 1746, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1606, 1747, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1607, 1748, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1608, 1749, 0, '390', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1609, 1750, 0, '399', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1610, 1751, 0, '10036', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1611, 1752, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1612, 1753, 0, '10037', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1613, 1754, 0, '10059', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1614, 1755, 0, '10034', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1615, 1756, 0, '10039', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1616, 1757, 0, '10045', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1617, 1758, 0, '10073', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1618, 1759, 0, '10032', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1619, 1762, 0, '10076', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1620, 1763, 0, '10042', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1621, 1764, 0, '10039', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1622, 1765, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1623, 1766, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1624, 1768, 0, '10045', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1625, 1769, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1626, 1771, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1627, 1773, 0, '10061', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1628, 1774, 0, '10042', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1629, 1775, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1630, 1776, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1631, 1779, 0, '10101', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1632, 1760, 0, '10110', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1633, 1761, 0, '10111', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1634, 1767, 0, '10112', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1635, 1770, 0, '10115', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1636, 1772, 0, '10113', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1637, 1778, 0, '10113', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1638, 1777, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1639, 1780, 0, '88', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1640, 1781, 0, '89', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1641, 1782, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1642, 1783, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1643, 1784, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1644, 1785, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1645, 1786, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1646, 1787, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1647, 1788, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1648, 1789, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1649, 1790, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1650, 1791, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1651, 1792, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1652, 1793, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1653, 1794, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1654, 1795, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1655, 1796, 0, '10044', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1656, 1797, 0, '10044', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1657, 1798, 0, '466', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1658, 1813, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1659, 1814, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1660, 1815, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1661, 1799, 0, '461', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1662, 1800, 0, '468', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1663, 1801, 0, '469', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1664, 1802, 0, '470', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1665, 1803, 0, '471', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1666, 1804, 0, '472', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1667, 1805, 0, '462', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1668, 1806, 0, '463', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1669, 1807, 0, '464', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1670, 1808, 0, '467', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1671, 1809, 0, '465', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1672, 1810, 0, '473', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1673, 1811, 0, '474', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1674, 1812, 0, '475', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1675, 1816, 0, '10102', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1676, 1817, 0, '10103', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1677, 1818, 0, '10104', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1678, 1819, 0, '10106', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1679, 1820, 0, '10107', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1680, 1821, 0, '10108', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1681, 1822, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1682, 1823, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1683, 1824, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1684, 1835, 0, '10101', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1685, 1836, 0, '10037', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1686, 1837, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1687, 1838, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1688, 1839, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1689, 1840, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1690, 1841, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1691, 1842, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1692, 1843, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1693, 1846, 0, '40004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1694, 1845, 0, '40004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1695, 1844, 0, '40004', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1696, 1863, 0, '473', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1697, 1864, 0, '474', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1698, 1853, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1699, 1854, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1700, 1855, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1701, 1856, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1702, 1857, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1703, 1858, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1704, 1859, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1705, 1860, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1706, 1861, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1707, 1862, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1708, 1847, 0, '40005', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1709, 1852, 0, '40007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1710, 1851, 0, '40007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1711, 1850, 0, '40007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1712, 1849, 0, '40007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1713, 1848, 0, '40007', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1714, 1825, 0, '40010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1715, 1826, 0, '40010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1716, 1827, 0, '40010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1717, 1828, 0, '40010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1718, 1829, 0, '40010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1719, 1830, 0, '40011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1720, 1831, 0, '40011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1721, 1832, 0, '40011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1722, 1833, 0, '40011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1723, 1834, 0, '40011', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1724, 1865, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1725, 1866, 0, '10113', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1726, 1868, 0, '10110', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1727, 1867, 0, '10115', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1728, 1869, 0, '472', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1729, 1870, 0, '471', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1730, 1871, 0, '477', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1731, 1872, 0, '478', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1732, 1873, 0, '10119', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1733, 1874, 0, '158', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1734, 1875, 0, '492', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1735, 1877, 0, '494', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1736, 1878, 0, '495', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1737, 1879, 0, '449', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1738, 1880, 0, '496', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1739, 1876, 0, '493', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1740, 1881, 0, '476', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1741, 1882, 0, '476', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1742, 1883, 0, '489', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1743, 1884, 0, '501', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1744, 1885, 0, '487', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1745, 1886, 0, '484', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1746, 1887, 0, '490', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1747, 1888, 0, '486', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1748, 1889, 0, '499', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1749, 1890, 0, '507', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1750, 1892, 0, '10107', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1751, 1891, 0, '10106', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1752, 1893, 0, '10108', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1753, 1894, 0, '10101', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1754, 1895, 0, '10103', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1755, 1896, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1756, 1897, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1757, 1898, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1758, 1899, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1759, 1901, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1760, 1900, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1761, 1902, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1762, 1903, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1763, 1904, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1764, 1905, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1765, 1906, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1766, 1907, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1767, 1908, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1768, 1909, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1769, 1910, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1770, 1911, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1771, 1912, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1772, 1913, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1773, 1923, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1774, 1922, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1775, 1921, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1776, 1920, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1777, 1919, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1778, 1918, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1779, 1917, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1780, 1916, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1781, 1915, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1782, 1914, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1783, 1924, 0, '10121', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1784, 1949, 0, '516', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1785, 1950, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1786, 1951, 0, '501', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1787, 1952, 0, '485', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1788, 1954, 0, '504', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1789, 1955, 0, '443', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1790, 1956, 0, '490', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1791, 1957, 0, '487', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1792, 1958, 0, '507', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1793, 1959, 0, '498', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1794, 1960, 0, '442', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1795, 1961, 0, '491', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1796, 1962, 0, '499', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1797, 1963, 0, '486', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1798, 1964, 0, '484', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1799, 1965, 0, '488', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1800, 1966, 0, '497', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1801, 1967, 0, '483', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1802, 1953, 0, '489', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1803, 1968, 0, '402', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1804, 1969, 0, '418', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1805, 1972, 0, '418', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1806, 1970, 0, '407', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1807, 1971, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1808, 1973, 0, '407', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1809, 1974, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1810, 1975, 0, '402', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1811, 1976, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1812, 1977, 0, '442', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1813, 1978, 0, '502', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1814, 1979, 0, '510', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1815, 1980, 0, '10027', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1816, 1981, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1817, 1982, 0, '10018', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1818, 1983, 0, '532', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1819, 1984, 0, '527', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1820, 1985, 0, '528', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1821, 1986, 0, '529', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1822, 1987, 0, '526', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1823, 1988, 0, '530', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1824, 1989, 0, '541', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1825, 1990, 0, '536', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1826, 1991, 0, '538', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1827, 1992, 0, '534', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1828, 1993, 0, '524', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1829, 1994, 0, '537', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1830, 1995, 0, '525', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1831, 1996, 0, '539', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1832, 1997, 0, '540', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1833, 1998, 0, '503', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1834, 1999, 0, '535', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1835, 2000, 0, '533', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1836, 2001, 0, '10124', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1837, 2002, 0, '10125', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1838, 2003, 0, '10126', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1839, 2007, 0, '519', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1840, 2011, 0, '523', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1841, 2008, 0, '518', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1842, 2009, 0, '520', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1843, 2010, 0, '517', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1844, 2012, 0, '270', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1845, 2014, 0, '397', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1846, 2013, 0, '384', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1847, 2004, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1848, 2005, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1849, 2006, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1850, 2015, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1851, 2016, 0, '508', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1852, 2018, 0, '511', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1853, 2019, 0, '531', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1854, 2020, 0, '512', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1855, 2021, 0, '515', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1856, 2017, 0, '509', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1857, 2022, 0, '514', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1858, 2023, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1859, 2024, 0, '528', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1860, 2025, 0, '526', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1861, 2026, 0, '532', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1862, 2027, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1863, 2028, 0, '10110', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1864, 2029, 0, '221', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1865, 2030, 0, '10105', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1866, 2031, 0, '10109', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1867, 2032, 0, '10109', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1868, 2033, 0, '10123', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1869, 2034, 0, '10030', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1870, 2035, 0, '10024', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1871, 2036, 0, '220', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1872, 2037, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1873, 2038, 0, '10038', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1874, 2039, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1875, 2040, 0, '532', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1876, 2041, 0, '527', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1877, 2042, 0, '529', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1878, 2043, 0, '528', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1879, 2044, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1880, 2045, 0, '542', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1881, 2046, 0, '543', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1882, 2047, 0, '544', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1883, 2048, 0, '545', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1884, 2049, 0, '546', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1885, 2050, 0, '443', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1886, 2051, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1887, 2053, 0, '504', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1888, 2054, 0, '498', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1889, 2052, 0, '497', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1890, 2055, 0, '483', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1891, 2056, 0, '442', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1892, 2057, 0, '10052', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1893, 2058, 0, '554', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1894, 2060, 0, '553', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1895, 2059, 0, '552', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1896, 2061, 0, '10127', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1897, 2062, 0, '10127', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1898, 2063, 0, '10127', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1899, 2064, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1900, 2065, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1901, 2066, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1902, 2067, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1903, 2068, 0, '511', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1904, 2069, 0, '531', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1905, 2070, 0, '551', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1906, 2071, 1, '', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1907, 2071, 0, '560', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1908, 2072, 0, '561', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1909, 2073, 0, '559', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1910, 2074, 0, '556', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1911, 2075, 0, '555', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1912, 2077, 0, '564', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1913, 2078, 0, '563', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1914, 2079, 0, '557', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1915, 2080, 0, '558', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1916, 2076, 0, '562', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1917, 2081, 0, '10128', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1918, 2082, 0, '10037', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1919, 2083, 0, '10002', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1920, 2084, 0, '10129', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1921, 2085, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1922, 2086, 0, '10003', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1923, 2087, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1924, 2088, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1925, 2089, 0, '10131', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1926, 2090, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1927, 2091, 0, '10027', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1928, 2092, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1929, 2093, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1930, 2094, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1931, 2095, 0, '10036', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1932, 2096, 0, '10085', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1933, 2097, 0, '10040', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1934, 2098, 0, '10031', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1935, 2099, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1936, 2100, 0, '565', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1937, 2101, 0, '521', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1938, 2102, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1939, 2103, 0, '10019', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1940, 2104, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1941, 2105, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1942, 2106, 0, '10006', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1943, 2107, 0, '10040', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1944, 2108, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1945, 2109, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1946, 2110, 0, '10116', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1947, 2111, 0, '10109', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1948, 2112, 0, '133', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1949, 2113, 0, '10106', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1950, 2114, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1951, 2115, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1952, 2116, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1953, 2117, 0, '10088', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1954, 2118, 0, '10131', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1955, 2119, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1956, 2120, 0, '10086', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1957, 2121, 0, '10130', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1958, 2122, 0, '10039', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1959, 2123, 0, '10086', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1960, 2124, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1961, 2125, 0, '10103', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1962, 2126, 0, '10116', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1963, 2127, 0, '10105', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1964, 2128, 0, '10012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1965, 2129, 0, '10000', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1966, 2130, 0, '10023', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1967, 2131, 0, '10108', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1968, 2132, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1969, 2133, 0, '10074', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1970, 2134, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1971, 2135, 0, '10087', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1972, 2136, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1973, 2137, 0, '10095', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1974, 2138, 0, '10069', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1975, 2139, 0, '10087', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1976, 2140, 0, '574', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1977, 2141, 0, '568', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1978, 2142, 0, '573', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1979, 2143, 0, '569', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1980, 2144, 0, '575', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1981, 2145, 0, '572', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1982, 2146, 0, '571', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1983, 2147, 0, '570', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1984, 2148, 0, '566', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1985, 2149, 0, '567', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1986, 2150, 0, '515', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1987, 2151, 0, '512', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1988, 2152, 0, '443', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1989, 2153, 0, '624', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1990, 2154, 0, '625', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1991, 2155, 0, '442', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1992, 2156, 0, '644', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1993, 2157, 0, '645', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1994, 2158, 0, '669', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1995, 2159, 0, '666', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1996, 2160, 0, '621', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1997, 2161, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1998, 2162, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (1999, 2163, 0, '356', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2000, 2164, 0, '361', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2001, 2165, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2002, 2166, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2003, 2167, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2004, 2168, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2005, 2169, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2006, 2170, 0, '491', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2007, 2171, 0, '454', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2008, 2172, 0, '522', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2009, 2173, 0, '577', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2010, 2174, 0, '576', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2011, 2175, 0, '582', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2012, 2176, 0, '578', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2013, 2177, 0, '581', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2014, 2178, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2015, 2179, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2016, 2180, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2017, 2181, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2018, 2182, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2019, 2183, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2020, 2184, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2021, 2185, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2022, 2186, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2023, 2187, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2024, 2188, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2025, 2189, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2026, 2190, 0, '40008', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2027, 2191, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2028, 2192, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2029, 2193, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2030, 2194, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2031, 2195, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2032, 2196, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2033, 2197, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2034, 2198, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2035, 2199, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2036, 2200, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2037, 2201, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2038, 2202, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2039, 2203, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2040, 2204, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2041, 2205, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2042, 2206, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2043, 2207, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2044, 2208, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2045, 2209, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2046, 2210, 0, '40009', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2047, 2211, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2048, 2212, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2049, 2213, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2050, 2214, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2051, 2215, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2052, 2216, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2053, 2217, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2054, 2218, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2055, 2219, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2056, 2220, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2057, 2222, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2058, 2221, 0, '40012', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2059, 2223, 0, '579', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2060, 2224, 0, '580', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2061, 2228, 0, '514', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2062, 2229, 0, '512', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2063, 2225, 0, '508', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2064, 2227, 0, '509', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2065, 2226, 0, '515', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2066, 2230, 0, '10051', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2067, 2232, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2068, 2233, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2069, 2235, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2070, 2234, 0, '10116', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2071, 2236, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2072, 2237, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2073, 2240, 0, '10118', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2074, 2238, 0, '10108', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2075, 2241, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2076, 2239, 0, '10088', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2077, 2242, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2078, 2243, 0, '10120', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2079, 2231, 0, '10132', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2080, 2244, 0, '10133', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2081, 2245, 0, '10133', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2082, 2246, 0, '10133', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2083, 2247, 0, '10133', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2084, 2248, 0, '10134', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2085, 2249, 0, '10134', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2086, 2250, 0, '10134', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2087, 2251, 0, '10134', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2088, 2314, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2089, 2320, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2090, 2321, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2091, 2323, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2092, 2322, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2093, 2328, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2094, 2329, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2095, 2330, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2096, 2331, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2097, 2324, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2098, 2325, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2099, 2326, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2100, 2327, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2101, 2332, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2102, 2333, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2103, 2334, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2104, 2335, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2105, 2252, 0, '583', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2106, 2253, 0, '600', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2107, 2254, 0, '617', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2108, 2255, 0, '608', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2109, 2257, 0, '604', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2110, 2256, 0, '601', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2111, 2258, 0, '605', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2112, 2307, 0, '546', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2113, 2308, 0, '547', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2114, 2309, 0, '548', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2115, 2315, 0, '545', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2116, 2316, 0, '618', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2117, 2317, 0, '619', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2118, 2259, 0, '606', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2119, 2260, 0, '586', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2120, 2261, 0, '607', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2121, 2262, 0, '603', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2122, 2263, 0, '597', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2123, 2264, 0, '596', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2124, 2265, 0, '595', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2125, 2266, 0, '588', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2126, 2267, 0, '589', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2127, 2268, 0, '593', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2128, 2269, 0, '594', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2129, 2270, 0, '585', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2130, 2271, 0, '591', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2131, 2272, 0, '590', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2132, 2273, 0, '592', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2133, 2274, 0, '584', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2134, 2275, 0, '602', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2135, 2276, 0, '587', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2136, 2277, 0, '610', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2137, 2278, 0, '609', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2138, 2279, 0, '599', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2139, 2280, 0, '616', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2140, 2281, 0, '611', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2141, 2282, 0, '613', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2142, 2283, 0, '614', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2143, 2284, 0, '612', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2144, 2285, 0, '597', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2145, 2286, 0, '596', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2146, 2287, 0, '595', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2147, 2288, 0, '588', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2148, 2289, 0, '589', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2149, 2290, 0, '593', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2150, 2291, 0, '594', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2151, 2292, 0, '585', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2152, 2293, 0, '591', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2153, 2294, 0, '590', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2154, 2295, 0, '592', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2155, 2296, 0, '584', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2156, 2297, 0, '602', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2157, 2298, 0, '587', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2158, 2299, 0, '610', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2159, 2300, 0, '609', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2160, 2301, 0, '599', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2161, 2302, 0, '616', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2162, 2303, 0, '611', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2163, 2304, 0, '613', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2164, 2305, 0, '614', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2165, 2306, 0, '612', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2166, 2310, 0, '593', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2167, 2311, 0, '594', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2168, 2312, 0, '616', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2169, 2313, 0, '611', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2170, 2318, 0, '615', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2171, 2319, 0, '598', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2172, 2336, 0, '10022', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2173, 2337, 0, '10035', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2174, 2338, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2175, 2339, 0, '10017', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2176, 2340, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2177, 2341, 0, '10024', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2178, 2345, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2179, 2343, 0, '10078', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2180, 2344, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2181, 2342, 0, '10117', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2182, 2347, 0, '10043', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2183, 2348, 0, '10053', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2184, 2349, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2185, 2346, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2186, 2350, 0, '10079', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2187, 2351, 0, '10103', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2188, 2352, 0, '10097', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2189, 2353, 0, '10038', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2190, 2360, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2191, 2354, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2192, 2355, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2193, 2356, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2194, 2357, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2195, 2358, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2196, 2359, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2197, 2361, 0, '10013', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2198, 2362, 0, '10014', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2199, 2363, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2200, 2364, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2201, 2365, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2202, 2366, 0, '10010', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2203, 2367, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2204, 2368, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2205, 2369, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2206, 2370, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2207, 2371, 0, '10096', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2208, 2372, 0, '620', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2209, 2373, 0, '621', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2210, 2374, 0, '622', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2211, 2375, 0, '623', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2212, 2376, 0, '624', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2213, 2377, 0, '625', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2214, 2378, 0, '626', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2215, 2379, 0, '627', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2216, 2380, 0, '632', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2217, 2381, 0, '633', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2218, 2382, 0, '629', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2219, 2383, 0, '664', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2220, 2384, 0, '621', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2221, 2385, 0, '622', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2222, 2386, 0, '623', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2223, 2387, 0, '624', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2224, 2388, 0, '625', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2225, 2389, 0, '626', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2226, 2391, 0, '630', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2227, 2390, 0, '628', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2228, 2392, 0, '631', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2229, 2393, 0, '665', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2230, 2394, 0, '635', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2231, 2395, 0, '636', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2232, 2396, 0, '637', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2233, 2397, 0, '638', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2234, 2398, 0, '639', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2235, 2399, 0, '641', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2236, 2400, 0, '642', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2237, 2401, 0, '643', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2238, 2402, 0, '644', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2239, 2403, 0, '645', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2240, 2404, 0, '646', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2241, 2405, 0, '668', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2242, 2406, 0, '667', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2243, 2407, 0, '666', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2244, 2408, 0, '669', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2245, 2409, 0, '491', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2246, 2410, 0, '521', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2247, 2411, 0, '551', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2248, 2412, 0, '454', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2249, 2413, 0, '522', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2251, 2415, 0, '340', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2252, 2416, 0, '501', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2253, 2417, 0, '485', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2254, 2418, 0, '489', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2255, 2419, 0, '443', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2256, 2420, 0, '504', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2257, 2421, 0, '414', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2258, 2422, 0, '402', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2259, 2423, 0, '490', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2260, 2424, 0, '487', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2261, 2425, 0, '507', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2262, 2426, 0, '411', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2263, 2427, 0, '498', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2264, 2428, 0, '415', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2265, 2429, 0, '418', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2266, 2430, 0, '499', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2267, 2431, 0, '486', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2268, 2432, 0, '484', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2269, 2433, 0, '488', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2270, 2434, 0, '497', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2271, 2435, 0, '515', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2272, 2436, 0, '483', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2273, 2437, 0, '509', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2274, 2438, 0, '442', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2275, 2439, 0, '508', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2276, 2440, 0, '514', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2277, 2441, 0, '512', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2278, 2442, 0, '491', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2279, 2443, 0, '521', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2280, 2444, 0, '551', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2281, 2445, 0, '454', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2282, 2446, 0, '522', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2283, 2452, 0, '662', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2284, 2453, 0, '663', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2285, 2467, 0, '10136', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2286, 2468, 0, '10135', 0, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2287, 2464, 0, '661', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2288, 2465, 0, '640', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2289, 2466, 0, '647', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2290, 2454, 0, '654', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2291, 2455, 0, '655', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2292, 2456, 0, '656', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2293, 2457, 0, '653', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2294, 2458, 0, '657', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2295, 2459, 0, '660', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2296, 2460, 0, '658', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2297, 2461, 0, '659', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2298, 2469, 0, '10139', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2299, 2470, 0, '10139', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2300, 2472, 0, '10139', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2301, 2471, 0, '10139', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2302, 2473, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2303, 2447, 0, '649', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2304, 2448, 0, '650', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2305, 2449, 0, '651', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2306, 2450, 0, '652', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2307, 2451, 0, '648', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2308, 2474, 0, '10131', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2309, 2475, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2310, 2476, 0, '10086', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2311, 2478, 0, '10119', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2312, 2477, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2313, 2479, 0, '10138', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2314, 2481, 0, '10137', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2315, 2482, 0, '10137', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2316, 2483, 0, '10092', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2317, 2484, 0, '10098', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2318, 2485, 0, '10118', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2319, 2480, 0, '10138', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2320, 2486, 0, '10099', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2321, 2488, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2322, 2489, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2323, 2490, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2324, 2491, 0, '10001', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2325, 2487, 0, '18', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2326, 2492, 0, '666', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2327, 2495, 0, '668', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2328, 2493, 0, '667', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2329, 2494, 0, '667', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2330, 2496, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2331, 2497, 0, '10093', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2332, 2498, 0, '670', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2333, 2499, 0, '671', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2334, 2500, 0, '672', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2335, 2501, 0, '673', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2336, 2502, 0, '674', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2337, 2503, 0, '677', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2338, 2504, 0, '675', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2339, 2505, 0, '676', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2340, 2506, 0, '10142', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2341, 2507, 0, '10141', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2342, 2508, 0, '10140', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2343, 2509, 0, '576', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2344, 2510, 0, '661', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2345, 2511, 0, '680', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2346, 2512, 0, '681', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2347, 2513, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2348, 2514, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2349, 2515, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2350, 2516, 0, '10047', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2351, 2517, 0, '678', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2352, 2518, 0, '679', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2353, 2519, 0, '10021', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2354, 2520, 1, '', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2355, 2520, 0, '10015', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2356, 539, 0, '682', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2357, 2521, 0, '10020', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2358, 2526, 0, '684', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2359, 2522, 0, '689', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2360, 2523, 0, '688', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2361, 2525, 0, '687', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2362, 2524, 0, '690', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2363, 2527, 0, '683', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2364, 2528, 0, '10143', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2365, 2529, 0, '10144', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2366, 2530, 0, '10145', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2367, 2531, 0, '10146', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2368, 2532, 0, '10147', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2369, 2533, 0, '10148', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2370, 2534, 0, '10149', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2371, 2535, 0, '10150', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2372, 2536, 0, '10', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2373, 2537, 0, '68', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2374, 2538, 0, '85', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2375, 2539, 0, '86', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2376, 2541, 0, '304', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2377, 2540, 0, '303', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2378, 2544, 0, '309', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2379, 2543, 0, '313', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2380, 2542, 0, '310', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2381, 2545, 0, '312', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2382, 2546, 0, '314', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2383, 2547, 0, '316', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2384, 2548, 0, '305', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2385, 2549, 0, '519', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2386, 2550, 0, '518', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2387, 2551, 0, '520', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2388, 2552, 0, '517', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2389, 2553, 0, '523', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2390, 2554, 0, '685', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2391, 2555, 0, '686', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2392, 2566, 0, '661', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2393, 2559, 0, '521', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2394, 2562, 0, '576', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2395, 2560, 0, '491', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2396, 2563, 0, '582', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2397, 2564, 0, '578', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2398, 2558, 0, '565', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2399, 2557, 0, '407', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2400, 2556, 0, '410', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2401, 2565, 0, '581', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2402, 2561, 0, '577', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2403, 2567, 0, '640', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2404, 2568, 0, '647', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2405, 2569, 0, '9', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2406, 2570, 0, '17', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2407, 2571, 0, '302', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2408, 2572, 0, '317', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2409, 2573, 0, '315', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2410, 2574, 0, '318', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2411, 2575, 0, '300', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2412, 2576, 0, '680', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2413, 2577, 0, '681', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2414, 2578, 0, '10073', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2415, 2579, 0, '10151', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2416, 2581, 0, '484', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2417, 2582, 0, '577', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2418, 2583, 0, '565', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2419, 2584, 0, '489', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2420, 2585, 0, '581', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2421, 2586, 0, '509', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2422, 2587, 0, '507', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2423, 2588, 0, '514', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2424, 2589, 0, '499', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2425, 2590, 0, '582', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2426, 2591, 0, '647', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2427, 2592, 0, '640', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2428, 2593, 0, '508', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2429, 2594, 0, '512', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2430, 2595, 0, '578', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2431, 2580, 0, '40013', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2432, 2596, 0, '693', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2433, 2597, 0, '696', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2434, 2598, 0, '699', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2435, 2599, 0, '694', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2436, 2600, 0, '695', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2437, 2601, 0, '697', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2438, 2602, 0, '698', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2439, 2603, 0, '376', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2440, 2604, 0, '379', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2441, 2605, 0, '691', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2442, 2606, 0, '692', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2443, 2607, 0, '10152', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2444, 2608, 0, '10025', NULL, NULL);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2445, 2609, 0, '10092', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2446, 2610, 0, '742', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2447, 2611, 0, '741', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2448, 2612, 0, '743', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2449, 2613, 0, '744', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2450, 2614, 0, '737', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2451, 2615, 0, '736', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2452, 2616, 0, '738', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2453, 2617, 0, '739', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2454, 2618, 0, '745', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2455, 2619, 0, '740', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2456, 2620, 0, '751', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2457, 2621, 0, '751', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2458, 2622, 0, '750', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2459, 2623, 0, '750', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2460, 2624, 0, '538', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2461, 2625, 0, '532', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2462, 2626, 0, '10154', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2463, 2627, 0, '10154', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2464, 2628, 0, '10154', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2465, 2629, 0, '10153', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2466, 2630, 0, '10153', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2467, 2631, 0, '10153', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2468, 2632, 0, '747', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2469, 2633, 0, '746', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2470, 2634, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2471, 2635, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2472, 2636, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2473, 2637, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2474, 2638, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2475, 2639, 0, '10017', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2476, 2640, 0, '712', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2477, 2641, 0, '715', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2478, 2642, 0, '730', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2479, 2643, 0, '733', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2480, 2644, 0, '700', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2481, 2645, 0, '703', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2482, 2646, 0, '706', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2483, 2647, 0, '709', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2484, 2648, 0, '718', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2485, 2649, 0, '721', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2486, 2650, 0, '724', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2487, 2651, 0, '727', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2488, 2652, 0, '713', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2489, 2653, 0, '716', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2490, 2654, 0, '731', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2491, 2655, 0, '734', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2492, 2656, 0, '701', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2493, 2657, 0, '704', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2494, 2658, 0, '707', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2495, 2659, 0, '710', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2496, 2660, 0, '719', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2497, 2661, 0, '722', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2498, 2662, 0, '725', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2499, 2663, 0, '728', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2500, 2665, 0, '747', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2501, 2666, 0, '746', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2502, 2664, 0, '10017', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2503, 2667, 0, '21', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2504, 2668, 0, '10051', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2505, 2669, 0, '748', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2506, 2670, 0, '749', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2507, 2671, 0, '10065', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2508, 2672, 0, '752', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2509, 2673, 0, '753', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2510, 2674, 0, '756', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2511, 2675, 0, '754', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2512, 2676, 0, '755', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2513, 2677, 0, '757', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2514, 2678, 0, '758', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2515, 2679, 0, '761', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2516, 2680, 0, '759', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2517, 2681, 0, '760', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2518, 2682, 0, '10088', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2519, 2683, 0, '10000', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2520, 2684, 0, '10105', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2521, 2685, 0, '10086', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2522, 2686, 0, '10047', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2523, 2687, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2524, 2688, 0, '10047', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2525, 2689, 0, '10083', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2526, 2690, 0, '10092', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2527, 2691, 0, '10155', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2528, 2692, 0, '781', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2529, 2693, 0, '782', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2530, 2694, 0, '783', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2531, 2695, 0, '784', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2532, 2696, 0, '785', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2533, 2697, 0, '786', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2534, 2698, 0, '788', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2535, 2699, 0, '789', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2536, 2700, 0, '787', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2537, 2701, 0, '787', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2538, 2702, 0, '780', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2539, 2703, 0, '779', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2540, 2704, 0, '776', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2541, 2705, 0, '777', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2542, 2706, 0, '775', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2543, 2707, 0, '774', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2544, 2708, 0, '778', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2546, 2709, 0, '790', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2547, 2710, 0, '791', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2548, 2711, 0, '792', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2549, 2712, 0, '793', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2550, 2713, 0, '794', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2551, 2714, 0, '795', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2552, 2715, 0, '798', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2553, 2716, 0, '798', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2554, 2717, 0, '796', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2555, 2718, 0, '797', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2556, 2720, 0, '790', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2557, 2721, 0, '791', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2558, 2722, 0, '792', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2559, 2723, 0, '793', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2560, 2724, 0, '794', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2561, 2725, 0, '795', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2562, 2726, 0, '798', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2563, 2727, 0, '798', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2564, 2728, 0, '796', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2565, 2729, 0, '797', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2566, 2730, 0, '799', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2567, 2731, 0, '10020', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2568, 2732, 0, '10156', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2569, 2733, 0, '10156', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2570, 2734, 0, '791', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2572, 2735, 0, '714', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2573, 2736, 0, '717', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2574, 2737, 0, '732', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2575, 2738, 0, '735', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2576, 2739, 0, '702', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2577, 2740, 0, '705', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2578, 2741, 0, '708', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2579, 2742, 0, '711', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2580, 2743, 0, '720', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2581, 2744, 0, '723', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2582, 2745, 0, '726', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2583, 2746, 0, '729', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2584, 2747, 0, '800', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2585, 2748, 0, '10151', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2586, 2749, 0, '762', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2587, 2750, 0, '768', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2588, 2751, 0, '762', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2589, 2752, 0, '768', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2590, 2773, 0, '762', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2591, 2774, 0, '768', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2592, 2775, 0, '762', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2593, 2776, 0, '768', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2594, 2753, 0, '763', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2595, 2754, 0, '769', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2596, 2755, 0, '763', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2597, 2756, 0, '769', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2598, 2777, 0, '763', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2599, 2778, 0, '769', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2600, 2779, 0, '763', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2601, 2780, 0, '769', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2602, 2757, 0, '767', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2603, 2758, 0, '773', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2604, 2759, 0, '767', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2605, 2760, 0, '773', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2606, 2781, 0, '767', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2607, 2782, 0, '773', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2608, 2783, 0, '767', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2609, 2784, 0, '773', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2610, 2761, 0, '766', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2611, 2762, 0, '772', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2612, 2763, 0, '766', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2613, 2764, 0, '772', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2614, 2785, 0, '766', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2615, 2786, 0, '772', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2616, 2787, 0, '766', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2617, 2788, 0, '772', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2618, 2765, 0, '764', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2619, 2766, 0, '770', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2620, 2767, 0, '764', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2621, 2768, 0, '770', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2622, 2789, 0, '764', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2623, 2790, 0, '770', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2624, 2791, 0, '764', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2625, 2792, 0, '770', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2626, 2769, 0, '765', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2627, 2770, 0, '771', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2628, 2771, 0, '765', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2629, 2772, 0, '771', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2630, 2793, 0, '765', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2631, 2794, 0, '771', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2632, 2795, 0, '765', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2633, 2796, 0, '771', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2636, 2799, 0, '616', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2637, 2800, 0, '611', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2638, 2719, 0, '799', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2639, 2801, 0, '404', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2640, 2802, 0, '399', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2641, 2803, 0, '746', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2642, 2804, 0, '747', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2643, 2805, 0, '10157', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2644, 2812, 0, '132', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2645, 2813, 0, '104', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2646, 2814, 0, '424', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2647, 2815, 0, '114', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2648, 2816, 0, '453', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2649, 2817, 0, '223', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2650, 2818, 0, '158', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2651, 2819, 0, '254', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2652, 2820, 0, '130', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2653, 2821, 0, '684', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2654, 2822, 0, '476', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2655, 2823, 0, '682', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2656, 2824, 0, '132', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2657, 2825, 0, '476', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2658, 2826, 0, '689', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2659, 2827, 0, '688', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2660, 2828, 0, '690', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2661, 2829, 0, '687', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2662, 2830, 0, '10149', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2663, 2831, 0, '10150', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2664, 2832, 0, '131', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2665, 2833, 0, '127', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2666, 2834, 0, '215', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2667, 2835, 0, '79', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2668, 2836, 0, '92', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2669, 2837, 0, '216', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2670, 2838, 0, '230', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2671, 2839, 0, '103', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2672, 2840, 0, '54', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2673, 2841, 0, '67', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2674, 2842, 0, '54', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2675, 2843, 0, '237', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2676, 2844, 0, '445', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2677, 2845, 0, '440', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2678, 2846, 0, '54', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2679, 2847, 0, '67', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2680, 2848, 0, '439', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2681, 2849, 0, '205', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2682, 2850, 0, '172', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2683, 2851, 0, '123', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2684, 2852, 0, '182', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2685, 2853, 0, '217', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2686, 2854, 0, '54', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2687, 2855, 0, '211', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2688, 2806, 0, '10117', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2689, 2807, 0, '10001', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2690, 2808, 0, '10008', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2691, 2809, 0, '10008', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2692, 2810, 0, '10010', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2693, 2811, 0, '10010', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2694, 2856, 0, '516', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2695, 2857, 0, '10092', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2696, 2858, 0, '815', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2697, 2859, 0, '816', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2698, 2860, 0, '813', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2699, 2861, 0, '814', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2700, 2862, 0, '817', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2701, 2863, 0, '818', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2702, 2864, 0, '819', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2703, 2865, 0, '820', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2704, 2866, 0, '813', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2705, 2867, 0, '814', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2706, 2868, 0, '813', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2707, 2869, 0, '814', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2708, 2870, 0, '817', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2709, 2871, 0, '813', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2710, 2872, 0, '714', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2711, 2873, 0, '717', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2712, 2874, 0, '732', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2713, 2875, 0, '735', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2714, 2876, 0, '702', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2715, 2877, 0, '705', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2716, 2878, 0, '708', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2717, 2879, 0, '711', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2718, 2880, 0, '720', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2719, 2881, 0, '723', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2720, 2882, 0, '726', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2721, 2883, 0, '729', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2722, 2884, 0, '714', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2723, 2885, 0, '717', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2724, 2886, 0, '732', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2725, 2887, 0, '735', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2726, 2888, 0, '702', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2727, 2889, 0, '705', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2728, 2890, 0, '708', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2729, 2891, 0, '711', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2730, 2892, 0, '720', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2731, 2893, 0, '723', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2732, 2894, 0, '726', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2733, 2895, 0, '729', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2734, 2896, 0, '801', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2735, 2897, 0, '802', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2736, 2898, 0, '801', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2737, 2899, 0, '802', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2738, 2900, 0, '803', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2739, 2901, 0, '804', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2740, 2902, 0, '803', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2741, 2903, 0, '804', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2742, 2904, 0, '809', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2743, 2905, 0, '810', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2744, 2906, 0, '809', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2745, 2907, 0, '810', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2746, 2908, 0, '811', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2747, 2909, 0, '812', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2748, 2910, 0, '811', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2749, 2911, 0, '812', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2750, 2912, 0, '807', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2751, 2913, 0, '808', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2752, 2914, 0, '807', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2753, 2915, 0, '808', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2754, 2916, 0, '805', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2755, 2917, 0, '806', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2756, 2918, 0, '805', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2757, 2919, 0, '806', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2758, 2920, 0, '801', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2759, 2921, 0, '802', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2760, 2922, 0, '801', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2761, 2923, 0, '802', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2762, 2924, 0, '803', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2763, 2925, 0, '804', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2764, 2926, 0, '803', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2765, 2927, 0, '804', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2766, 2928, 0, '809', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2767, 2929, 0, '810', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2768, 2930, 0, '809', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2769, 2931, 0, '810', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2770, 2932, 0, '811', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2771, 2933, 0, '812', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2772, 2934, 0, '811', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2773, 2935, 0, '812', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2774, 2936, 0, '807', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2775, 2937, 0, '808', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2776, 2938, 0, '807', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2777, 2939, 0, '808', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2778, 2940, 0, '805', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2779, 2941, 0, '806', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2780, 2942, 0, '805', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2781, 2943, 0, '806', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2782, 2944, 0, '822', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2783, 2945, 0, '824', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2784, 2946, 0, '10156', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (2785, 2947, 0, '814', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (107244, 107242, 0, '640', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (107245, 107243, 0, '407', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (107269, 107267, 0, '813', 0, 0);
GO

INSERT INTO [dbo].[DT_MonsterResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (107270, 107268, 0, '814', 0, 0);
GO

