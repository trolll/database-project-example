/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_BeadEffect
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (2, '추가 발동1', 1, 3, 0.1, 1, 767, 768, 767, 769, 770);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (3, '추가 발동2', 1, 3, 0.2, 1, 767, 768, 767, 769, 770);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (4, '추가 발동3', 1, 3, 0.3, 1, 767, 768, 767, 769, 770);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (5, '추가 타격1', 3, 15, 10.0, 1, 1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (6, '추가 타격2', 3, 15, 14.0, 1, 2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (7, '추가 타격3', 3, 15, 17.0, 1, 3, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (8, '효과 삭제1', 2, 3, 0.6, 1, 721, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (9, '효과 삭제2', 2, 3, 0.7, 1, 721, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (10, '효과 삭제3', 2, 3, 0.8, 1, 721, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (11, '상승 타격1', 2, 3, 0.1, 0, 712, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (12, '상승 타격2', 2, 3, 0.2, 0, 712, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (13, '상승 타격3', 2, 3, 0.3, 0, 712, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (14, '치명 공격1', 2, 3, 0.1, 0, 713, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (15, '치명 공격2', 2, 3, 0.2, 0, 713, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (16, '치명 공격3', 2, 3, 0.3, 0, 713, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (17, '완전한 회피1', 4, 7, 0.1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (18, '완전한 회피2', 4, 7, 0.2, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (19, '완전한 회피3', 4, 7, 0.3, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (20, '타격 제한1', 3, 10, 100.0, 1, -1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (21, '타격 제한2', 3, 10, 100.0, 1, -2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (22, '타격 제한3', 3, 10, 100.0, 1, -3, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, '용기1', 2, 14, 100.0, 0, 764, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, '용기2', 2, 14, 100.0, 0, 765, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, '용기3', 2, 14, 100.0, 0, 766, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (26, '정신 분열1', 2, 4, 0.1, 1, 722, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (27, '정신 분열2', 2, 4, 0.5, 1, 722, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (28, '정신 분열3', 2, 4, 1.0, 1, 722, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (29, '각성1', 2, 4, 0.1, 0, 723, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (30, '각성2', 2, 4, 0.2, 0, 723, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (31, '각성3', 2, 4, 0.3, 0, 723, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (32, '근거리 타격 절감1', 3, 11, 1.0, 0, -1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (33, '근거리 타격 절감2', 3, 11, 1.0, 0, -2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (34, '근거리 타격 절감3', 3, 11, 1.0, 0, -3, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (35, '지속 절감1', 2, 5, 5.0, 0, 724, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (36, '지속 절감2', 2, 5, 5.0, 0, 725, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (37, '지속 절감3', 2, 5, 5.0, 0, 726, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (38, '시간 단축1', 5, 14, 100.0, 0, -1000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (39, '시간 단축2', 5, 14, 100.0, 0, -2000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (40, '시간 단축3', 5, 14, 100.0, 0, -3000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, '근거리 추가 방어1', 2, 14, 100.0, 0, 761, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, '근거리 추가 방어2', 2, 14, 100.0, 0, 762, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, '근거리 추가 방어3', 2, 14, 100.0, 0, 763, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, '생존시간1', 5, 14, 100.0, 0, 10000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, '생존시간2', 5, 14, 100.0, 0, 15000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, '생존시간3', 5, 14, 100.0, 0, 20000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (68, '원거리 타격 절감1', 3, 13, 5.0, 0, -1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (69, '원거리 타격 절감2', 3, 13, 5.0, 0, -2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (70, '원거리 타격 절감3', 3, 13, 5.0, 0, -3, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (71, '연쇄 공격1', 2, 1, 0.1, 0, 727, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (72, '연쇄 공격2', 2, 1, 0.1, 0, 728, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (73, '연쇄 공격3', 2, 1, 0.1, 0, 729, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (74, '집중 공격1', 2, 1, 0.1, 0, 730, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (75, '집중 공격2', 2, 1, 0.1, 0, 731, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (76, '집중 공격3', 2, 1, 0.1, 0, 732, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (77, '연쇄 반격1', 2, 4, 0.1, 0, 733, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (78, '연쇄 반격2', 2, 4, 0.1, 0, 734, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (79, '연쇄 반격3', 2, 4, 0.1, 0, 735, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (80, '집중 반격1', 2, 4, 0.1, 0, 736, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (81, '집중 반격2', 2, 4, 0.1, 0, 737, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (82, '집중 반격3', 2, 4, 0.1, 0, 738, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (83, '마법 타격 절감1', 3, 12, 0.5, 0, -2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (84, '마법 타격 절감2', 3, 12, 0.5, 0, -4, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (85, '마법 타격 절감3', 3, 12, 0.5, 0, -6, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, '지구력 향상1', 5, 14, 100.0, 0, 60000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, '지구력 향상2', 5, 14, 100.0, 0, 120000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, '지구력 향상3', 5, 14, 100.0, 0, 180000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, '질주1', 2, 14, 100.0, 0, 739, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, '질주2', 2, 14, 100.0, 0, 740, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, '질주3', 2, 14, 100.0, 0, 741, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (92, '자유의지1', 4, 7, 0.1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (93, '자유의지2', 4, 7, 0.2, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (94, '자유의지3', 4, 7, 0.3, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (95, '민첩한 움직임1', 4, 8, 0.1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (96, '민첩한 움직임2', 4, 8, 0.2, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (97, '민첩한 움직임3', 4, 8, 0.3, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (98, '은둔자1', 2, 17, 100.0, 0, 774, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (99, '은둔자2', 2, 17, 100.0, 0, 775, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (100, '은둔자3', 2, 17, 100.0, 0, 776, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (101, '구원의 손길1', 2, 4, 1.0, 0, 742, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (102, '구원의 손길2', 2, 4, 1.0, 0, 743, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (103, '구원의 손길3', 2, 4, 1.0, 0, 744, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (104, '현란한 움직임1', 2, 4, 1.0, 0, 745, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (105, '현란한 움직임2', 2, 4, 1.0, 0, 746, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (106, '현란한 움직임3', 2, 4, 1.0, 0, 747, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (107, '빠른 습득1', 2, 4, 0.4, 2, 748, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (108, '빠른 습득2', 2, 4, 0.8, 2, 748, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (109, '빠른 습득3', 2, 4, 1.2, 2, 748, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, '은둔자의 보폭1', 2, 17, 100.0, 0, 749, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, '은둔자의 보폭2', 2, 17, 100.0, 0, 750, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, '은둔자의 보폭3', 2, 17, 100.0, 0, 751, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (113, '역공 타격1', 2, 6, 1.0, 1, 752, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (114, '역공 타격2', 2, 6, 1.0, 1, 753, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (115, '역공 타격3', 2, 6, 1.0, 1, 754, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (116, '역공 마법1', 2, 5, 5.0, 1, 755, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (117, '역공 마법2', 2, 5, 5.0, 1, 756, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (118, '역공 마법3', 2, 5, 5.0, 1, 757, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (119, '분발1', 2, 4, 0.6, 0, 771, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (120, '분발2', 2, 4, 0.7, 0, 772, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (121, '분발3', 2, 4, 0.8, 0, 773, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (122, '분산1', 3, 16, 5.0, 0, -2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (123, '분산2', 3, 16, 5.0, 0, -4, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (124, '분산3', 3, 16, 5.0, 0, -6, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (125, '최종 타격 절감1', 3, 9, 0.1, 0, -3, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (126, '최종 타격 절감2', 3, 9, 0.1, 0, -6, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (127, '최종 타격 절감3', 3, 9, 0.1, 0, -9, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (128, '추가 발동4', 1, 3, 0.4, 1, 767, 768, 767, 769, 770);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (129, '추가 발동5', 1, 3, 0.5, 1, 767, 768, 767, 769, 770);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (130, '추가 타격4', 3, 15, 20.0, 1, 4, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (131, '추가 타격5', 3, 15, 25.0, 1, 5, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (132, '효과 삭제4', 2, 3, 0.9, 1, 721, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (133, '효과 삭제5', 2, 3, 1.0, 1, 721, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (134, '상승 타격4', 2, 3, 0.4, 0, 712, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (135, '상승 타격5', 2, 3, 0.5, 0, 712, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (136, '치명 공격4', 2, 3, 0.4, 0, 713, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (137, '치명 공격5', 2, 3, 0.5, 0, 713, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (138, '완전한 회피4', 4, 7, 0.4, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (139, '완전한 회피5', 4, 7, 0.5, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (140, '타격 제한4', 3, 10, 100.0, 1, -4, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (141, '타격 제한5', 3, 10, 100.0, 1, -5, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, '용기4', 2, 14, 100.0, 0, 777, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, '용기5', 2, 14, 100.0, 0, 778, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (144, '정신 분열4', 2, 4, 1.5, 1, 722, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (145, '정신 분열5', 2, 4, 2.0, 1, 722, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (146, '각성4', 2, 4, 0.4, 0, 723, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (147, '각성5', 2, 4, 0.5, 0, 723, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (148, '근거리 타격 절감4', 3, 11, 1.0, 0, -4, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (149, '근거리 타격 절감5', 3, 11, 1.0, 0, -5, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (150, '지속 절감4', 2, 5, 5.0, 0, 779, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (151, '지속 절감5', 2, 5, 5.0, 0, 780, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (152, '시간 단축4', 5, 14, 100.0, 0, -4000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (153, '시간 단축5', 5, 14, 100.0, 0, -5000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, '근거리 추가 방어4', 2, 14, 100.0, 0, 781, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, '근거리 추가 방어5', 2, 14, 100.0, 0, 782, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, '생존시간4', 5, 14, 100.0, 0, 25000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, '생존시간5', 5, 14, 100.0, 0, 30000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (158, '원거리 타격 절감4', 3, 13, 5.0, 0, -4, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (159, '원거리 타격 절감5', 3, 13, 5.0, 0, -5, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (160, '연쇄 반격4', 2, 4, 0.1, 0, 787, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (161, '연쇄 반격5', 2, 4, 0.1, 0, 788, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (162, '연쇄 공격4', 2, 1, 0.1, 0, 783, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (163, '연쇄 공격5', 2, 1, 0.1, 0, 784, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (164, '집중 공격4', 2, 1, 0.1, 0, 785, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (165, '집중 공격5', 2, 1, 0.1, 0, 786, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (166, '집중 반격4', 2, 4, 0.1, 0, 789, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (167, '집중 반격5', 2, 4, 0.1, 0, 790, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (168, '마법 타격 절감4', 3, 12, 0.5, 0, -8, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (169, '마법 타격 절감5', 3, 12, 0.5, 0, -10, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, '지구력 향상4', 5, 14, 100.0, 0, 240000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, '지구력 향상5', 5, 14, 100.0, 0, 300000, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, '질주4', 2, 14, 100.0, 0, 791, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, '질주5', 2, 14, 100.0, 0, 792, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (174, '자유의지4', 4, 7, 0.4, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (175, '자유의지5', 4, 7, 0.5, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (176, '민첩한 움직임4', 4, 8, 0.4, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (177, '민첩한 움직임5', 4, 8, 0.5, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (178, '은둔자4', 2, 17, 100.0, 0, 793, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (179, '은둔자5', 2, 17, 100.0, 0, 794, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, '은둔자의 보폭4', 2, 17, 100.0, 0, 799, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, '은둔자의 보폭5', 2, 17, 100.0, 0, 800, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (182, '구원의 손길4', 2, 4, 1.0, 0, 795, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (183, '구원의 손길5', 2, 4, 1.0, 0, 796, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (184, '현란한 움직임4', 2, 4, 1.0, 0, 797, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (185, '현란한 움직임5', 2, 4, 1.0, 0, 798, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (186, '빠른 습득4', 2, 4, 1.6, 2, 748, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (187, '빠른 습득5', 2, 4, 2.0, 2, 748, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (188, '역공 타격4', 2, 6, 1.0, 1, 801, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (189, '역공 타격5', 2, 6, 1.0, 1, 802, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (190, '역공 마법4', 2, 5, 5.0, 1, 803, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (191, '역공 마법5', 2, 5, 5.0, 1, 804, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (192, '분발4', 2, 4, 0.9, 0, 758, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (193, '분발5', 2, 4, 1.0, 0, 759, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (194, '분산4', 3, 16, 5.0, 0, -8, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (195, '분산5', 3, 16, 5.0, 0, -10, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (196, '최종 타격 절감4', 3, 9, 0.1, 0, -12, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (197, '최종 타격 절감5', 3, 9, 0.1, 0, -15, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (198, '피격시 데미지 감소 Ⅰ', 3, 9, 1.0, 0, -1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (199, '피격시 데미지 감소 Ⅱ', 3, 9, 3.0, 0, -2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffect] ([mBeadNo], [mName], [mBeadType], [mChkGroup], [mPercent], [mApplyTarget], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (200, '피격시 데미지 감소 Ⅲ', 3, 9, 5.0, 0, -3, 0, 0, 0, 0);
GO

