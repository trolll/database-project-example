/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_PShopClass
Date                  : 2023-10-07 09:08:50
*/


INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (1, 'DEV');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (2, 'KR??-????');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (3, 'KR??-??');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (4, 'KR???-????');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (5, 'KR???-??');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (6, 'CN');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (7, 'RU');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (8, 'TW');
GO

INSERT INTO [dbo].[TP_PShopClass] ([PShopClass], [PShop]) VALUES (9, 'JP');
GO

