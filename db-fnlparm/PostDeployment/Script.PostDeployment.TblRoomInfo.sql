/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblRoomInfo
Date                  : 2023-10-07 09:08:58
*/


INSERT INTO [dbo].[TblRoomInfo] ([mID], [MName], [mType], [mMapNo], [mKeyItem]) VALUES (1, '라이칸의 방', 0, 87, 3468);
GO

INSERT INTO [dbo].[TblRoomInfo] ([mID], [MName], [mType], [mMapNo], [mKeyItem]) VALUES (2, '캐스퍼의 방', 0, 87, 3469);
GO

INSERT INTO [dbo].[TblRoomInfo] ([mID], [MName], [mType], [mMapNo], [mKeyItem]) VALUES (3, '블레어의 방', 0, 87, 3470);
GO

INSERT INTO [dbo].[TblRoomInfo] ([mID], [MName], [mType], [mMapNo], [mKeyItem]) VALUES (4, '노예의 방', 0, 87, 3471);
GO

