/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_ServantExp
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (1, 65, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (2, 156, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (3, 250, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (4, 374, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (5, 512, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (6, 866, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (7, 1367, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (8, 2228, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (9, 3284, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (10, 4283, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (11, 5617, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (12, 7839, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (13, 10727, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (14, 14173, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (15, 18662, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (16, 22728, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (17, 27062, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (18, 32178, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (19, 38562, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (20, 46172, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (21, 55617, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (22, 70616, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (23, 90616, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (24, 110616, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (25, 137126, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (26, 159066, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (27, 182926, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (28, 210364, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (29, 239815, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (30, 273389, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (31, 308929, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (32, 349090, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (33, 390980, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (34, 437898, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (35, 486066, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (36, 539533, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (37, 598882, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (38, 658770, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (39, 724646, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (40, 1412889, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (41, 1555857, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (42, 1709409, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (43, 1874057, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (44, 2050321, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (45, 2415001, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (46, 3169209, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (47, 4916841, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (48, 7416841, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (49, 10286419, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (50, 12425677, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (51, 15028209, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (52, 23043212, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (53, 25347533, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (54, 27882286, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (55, 30670515, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (56, 33737566, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (57, 37111323, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (58, 40822455, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (59, 44904700, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (60, 49395170, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (61, 54334687, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (62, 59768156, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (63, 65744972, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (64, 72319469, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (65, 79551416, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (66, 87506557, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (67, 96257213, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (68, 105882934, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (69, 116471228, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (70, 128118351, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (71, 140930186, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (72, 155023204, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (73, 170525525, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (74, 187578077, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (75, 206335885, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (76, 226969473, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (77, 249666421, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (78, 274633063, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (79, 302096369, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (80, 332306006, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (81, 365536606, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (82, 402090267, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (83, 442299294, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (84, 486529223, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (85, 535182145, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (86, 588700360, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (87, 647570396, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (88, 712327436, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (89, 783560179, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (90, 861916197, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (91, 948107817, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (92, 1042918598, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (93, 1147210458, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (94, 1261931504, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (95, 1388124654, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (96, 1526937120, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (97, 1679630832, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (98, 1847593915, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (99, 2032353307, 0);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (1, 38, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (2, 90, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (3, 145, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (4, 217, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (5, 297, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (6, 502, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (7, 793, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (8, 1292, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (9, 1905, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (10, 2484, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (11, 3258, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (12, 4547, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (13, 6221, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (14, 8220, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (15, 10824, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (16, 13182, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (17, 15696, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (18, 18663, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (19, 22366, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (20, 26779, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (21, 32258, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (22, 40957, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (23, 52557, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (24, 64157, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (25, 79533, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (26, 92258, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (27, 106097, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (28, 122011, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (29, 139092, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (30, 158565, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (31, 179179, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (32, 202472, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (33, 226768, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (34, 253981, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (35, 281918, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (36, 312929, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (37, 347351, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (38, 382086, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (39, 420295, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (40, 819475, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (41, 902397, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (42, 991457, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (43, 1086953, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (44, 1189186, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (45, 1400701, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (46, 1838141, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (47, 2851767, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (48, 4301767, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (49, 5966123, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (50, 7206892, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (51, 8716361, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (52, 13365063, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (53, 14701569, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (54, 16171726, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (55, 17788898, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (56, 19567788, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (57, 21524567, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (58, 23677024, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (59, 26044726, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (60, 28649199, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (61, 31514119, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (62, 34665531, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (63, 38132084, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (64, 41945292, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (65, 46139821, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (66, 50753803, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (67, 55829184, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (68, 61412102, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (69, 67553312, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (70, 74308643, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (71, 81739508, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (72, 89913458, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (73, 98904804, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (74, 108795285, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (75, 119674813, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (76, 131642294, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (77, 144806524, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (78, 159287176, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (79, 175215894, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (80, 192737483, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (81, 212011232, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (82, 233212355, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (83, 256533590, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (84, 282186949, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (85, 310405644, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (86, 341446209, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (87, 375590830, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (88, 413149913, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (89, 454464904, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (90, 499911394, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (91, 549902534, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (92, 604892787, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (93, 665382066, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (94, 731920272, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (95, 805112300, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (96, 885623530, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (97, 974185882, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (98, 1071604471, 1);
GO

INSERT INTO [dbo].[DT_ServantExp] ([ELevel], [EExp], [EType]) VALUES (99, 1178764918, 1);
GO

