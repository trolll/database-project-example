/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_BeadEffectParm
Date                  : 2023-10-07 09:08:57
*/


INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (5, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (6, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (7, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (17, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (18, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (19, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (20, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (21, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (22, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (38, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (38, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (39, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (39, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (40, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (40, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 1, 89, 508, 509, 510, 1829);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 2, 236, 1818, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 3, 87, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 1, 89, 508, 510, 1829, 236);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 2, 1818, 509, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 3, 87, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (92, 1, 127, 179, 378, 18, 737);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (92, 2, 738, 739, 740, 394, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (93, 1, 127, 179, 378, 18, 737);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (93, 2, 738, 739, 740, 394, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (94, 1, 127, 179, 378, 18, 737);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (94, 2, 738, 739, 740, 394, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 1, 89, 508, 509, 510, 1829);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 2, 236, 1818, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 3, 87, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 1, 21, 27, 89, 90, 111);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 2, 112, 113, 122, 128, 138);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 3, 190, 191, 192, 205, 246);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 4, 260, 265, 301, 302, 303);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 5, 369, 407, 542, 729, 731);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 6, 760, 761, 833, 835, 837);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 7, 841, 910, 911, 982, 983);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 8, 1043, 1044, 1079, 1080, 1081);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 9, 1082, 1411, 1412, 1413, 1414);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (143, 10, 2616, 2621, 2626, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (5, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (6, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (7, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (17, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (18, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (19, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (20, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (21, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 5, 762, 817, 1410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 5, 762, 817, 1410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 4, 364, 450, 542, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 5, 817, 1410, 1727, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 6, 362, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 7, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 8, 1375, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 5, 762, 817, 1410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 7, 1375, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 5, 762, 817, 1410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (130, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (131, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (138, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (139, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (140, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (141, 1, 120, 126, 318, 377, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 1, 21, 27, 89, 90, 111);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 2, 112, 113, 122, 128, 138);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 3, 190, 191, 192, 205, 246);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 4, 260, 265, 301, 302, 303);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 5, 369, 407, 542, 729, 731);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 6, 760, 761, 833, 837, 841);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 7, 910, 911, 982, 983, 1043);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 8, 1044, 1079, 1080, 1081, 1082);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 9, 1411, 1412, 1413, 1414, 835);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 1, 21, 27, 89, 90, 111);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 2, 112, 113, 122, 128, 138);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 3, 190, 191, 192, 205, 246);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 4, 260, 265, 301, 302, 303);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 5, 369, 407, 542, 729, 731);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 6, 760, 761, 833, 835, 837);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 7, 841, 910, 911, 982, 983);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 8, 1043, 1044, 1079, 1080, 1081);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 9, 1082, 1411, 1412, 1413, 1414);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (22, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (130, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (131, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (138, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (139, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (140, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (141, 2, 2615, 2620, 2625, 2637, 2638);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 2, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 3, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 4, 364, 450, 542, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 5, 817, 1410, 1727, 362, 6);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 7, 1375, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, 1, 89, 508, 509, 510, 1829);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 4, 364, 450, 542, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 5, 817, 1410, 1727, 362, 6);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 6, 174, 380, 300, 234, 1375);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, 2, 236, 1818, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, 1, 89, 508, 509, 510, 1829);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, 2, 236, 1818, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 4, 2809, 2810, 2811, 2812, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 4, 2809, 2810, 2811, 2812, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 4, 2809, 2810, 2811, 2812, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, 3, 2809, 2810, 2811, 2812, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, 3, 2809, 2810, 2811, 2812, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 4, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 5, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 6, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 7, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (41, 8, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 2, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 3, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 4, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 5, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 6, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 7, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (42, 8, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 2, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 3, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 4, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 5, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 6, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 7, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (43, 8, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 2, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 3, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 4, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 5, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 6, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 7, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 8, 619, 2448, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (44, 9, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 2, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 3, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 4, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 5, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 6, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 7, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 8, 619, 2448, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (45, 9, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 2, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 3, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 4, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 5, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 6, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 7, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 8, 619, 2448, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (46, 9, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 1, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 2, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 3, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 4, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 5, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 6, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 7, 619, 2448, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (156, 8, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 1, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 2, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 3, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 4, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 5, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 6, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 7, 619, 2448, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (157, 8, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 1, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 2, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 3, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 4, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 5, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 6, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (154, 7, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 1, 298, 304, 324, 364, 370);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 2, 406, 440, 448, 542, 708);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 3, 709, 710, 711, 752, 762);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 4, 817, 1119, 1120, 1121, 1122);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 5, 1123, 1410, 1727, 1744, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 6, 64, 121, 152, 235, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (155, 7, 2617, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (23, 10, 2616, 2621, 2626, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 1, 21, 27, 89, 90, 111);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 2, 112, 113, 122, 128, 138);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 3, 190, 191, 192, 205, 246);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 4, 260, 265, 301, 302, 303);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 5, 369, 407, 542, 729, 731);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 6, 760, 761, 833, 835, 837);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 7, 841, 910, 911, 982, 983);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 8, 1043, 1044, 1079, 1080, 1081);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 9, 1082, 1411, 1412, 1413, 1414);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (24, 10, 2616, 2621, 2626, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 1, 21, 27, 89, 90, 111);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (35, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (35, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 8, 174, 380, 300, 234, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 2, 112, 113, 122, 128, 138);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 3, 190, 191, 192, 205, 246);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 4, 260, 265, 301, 302, 303);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 5, 369, 407, 542, 729, 731);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 6, 760, 761, 833, 835, 837);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 7, 841, 910, 911, 982, 983);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 8, 1043, 1044, 1079, 1080, 1081);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 9, 1082, 1411, 1412, 1413, 1414);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (25, 10, 2616, 2621, 2626, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (142, 10, 2616, 2621, 2626, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (92, 4, 2622, 2636, 2640, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (93, 3, 2622, 2636, 2640, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (94, 3, 2622, 2636, 2640, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (174, 1, 127, 179, 378, 18, 737);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (174, 2, 738, 739, 740, 394, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (174, 3, 2622, 2636, 2640, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (175, 1, 127, 179, 378, 18, 737);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (175, 2, 738, 739, 740, 394, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (175, 3, 2622, 2636, 2640, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (35, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (36, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (36, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (36, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (37, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (37, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (37, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (38, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (39, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (40, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (86, 7, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (87, 7, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (88, 7, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (89, 9, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 5, 762, 817, 410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 7, 1375, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (90, 8, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (91, 8, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (150, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (150, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (150, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (151, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (151, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (151, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (152, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (152, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (152, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (153, 1, 14, 1151, 1152, 1153, 1154);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (153, 2, 1155, 177, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (153, 3, 2634, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 5, 762, 817, 1410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (170, 7, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 1, 7, 61, 152, 233, 235);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 2, 288, 289, 290, 291, 292);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 3, 293, 298, 299, 360, 361);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 4, 362, 364, 450, 542, 752);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 5, 762, 817, 1410, 1727, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 6, 6, 174, 380, 300, 234);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (171, 7, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (172, 9, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (173, 7, 2631, 2641, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 5, 3096, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 5, 3096, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 5, 3096, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, 4, 3096, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, 4, 3096, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 6, 2233, 2234, 2235, 2236, 2237);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 6, 2233, 2234, 2235, 2236, 2237);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (110, 7, 3243, 3244, 3245, 3246, 3247);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (111, 7, 3243, 3244, 3245, 3246, 3247);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 6, 2233, 2234, 2235, 2236, 2237);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (112, 7, 3243, 3244, 3245, 3246, 3247);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, 5, 2233, 2234, 2235, 2236, 2237);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (180, 6, 3243, 3244, 3245, 3246, 3247);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, 5, 2233, 2234, 2235, 2236, 2237);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (181, 6, 3243, 3244, 3245, 3246, 3247);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (98, 1, 512, 513, 514, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (98, 2, 507, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (99, 1, 512, 513, 514, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (99, 2, 507, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (100, 1, 512, 513, 514, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (100, 2, 507, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (178, 1, 512, 513, 514, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (178, 2, 507, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (179, 1, 512, 513, 514, 0, 0);
GO

INSERT INTO [dbo].[DT_BeadEffectParm] ([mBeadNo], [mOrderNo], [mParamA], [mParamB], [mParamC], [mParamD], [mParamE]) VALUES (179, 2, 507, 0, 0, 0, 0);
GO

