/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblLevelupCoin
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (0, 10651947430, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (1, 21303894859, 5, 3, 1);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (2, 34086231775, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (3, 46868568690, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (4, 63911684577, 11, 6, 1);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (5, 78824410979, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (6, 93737137380, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (7, 110780253267, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (8, 127823369155, 23, 12, 1);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (9, 144866485042, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (10, 161909600929, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (11, 178952716816, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (12, 195995832704, 4, 2, 0);
GO

INSERT INTO [dbo].[TblLevelupCoin] ([mSection], [mExp], [mRewardCoin], [mMinimumCoin], [mType]) VALUES (13, 213038948591, 49, 25, 1);
GO

