/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblTeleportTower
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblTeleportTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc]) VALUES ('2013-05-20 14:58:47.513', 300, 671089.5625, 17333.304688, 858826.6875, 268.196411, 4.607669, 'c1_battlefield_battletower_01.r3m                 ');
GO

INSERT INTO [dbo].[TblTeleportTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc]) VALUES ('2013-05-20 14:58:47.517', 301, 666856.625, 17278.470703, 866559.5625, 268.196411, 3.874631, 'c1_battlefield_battletower_01.r3m                 ');
GO

INSERT INTO [dbo].[TblTeleportTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc]) VALUES ('2013-05-20 14:58:47.520', 302, 670911.0, 17346.548828, 874315.0, 268.196411, 4.799655, 'c1_battlefield_battletower_01.r3m                 ');
GO

