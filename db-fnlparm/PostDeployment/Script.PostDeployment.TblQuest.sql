/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblQuest
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1006, '暗杀团的委托
', 255, 1, 0, 0, 1, '암살단 스크롤 제공 일퀘', 1, 3, 1006, 0, 322, 136993.0, 27444.0, 64551.0, 1, 2, 0, 2870, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1005, '伊莉娜的委托
', 255, 1, 0, 865, 1, '서번트 획득 퀘스트 (일일 퀘스트)', 1, 1, 1005, 0, 76, 381854.0, 20598.0, 164182.0, 1, 2, 0, 2748, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1004, '<大宗师>金的试验 (V)
', 255, 100, 0, 1003, 0, '<그랜드마스터>진의 시험 (V)', 1, 5, 1004, 0, 0, 0.0, 0.0, 0.0, 1, 51, 1000, 2691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1003, '<大宗师>金的试验 (IV)
', 255, 100, 0, 1002, 0, '<그랜드마스터>진의 시험 (IV)', 1, 5, 1003, 0, 0, 0.0, 0.0, 0.0, 1, 41, 1000, 2691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1002, '<大宗师>金的试验 (III)
', 255, 100, 0, 1001, 0, '<그랜드마스터>진의 시험 (III)', 1, 5, 1002, 0, 0, 0.0, 0.0, 0.0, 1, 31, 1000, 2691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1001, '<大宗师>金的试验 (II)
', 255, 100, 0, 1000, 0, '<그랜드마스터>진의 시험 (II)', 1, 5, 1001, 0, 0, 0.0, 0.0, 0.0, 1, 21, 1000, 2691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (1000, '<大宗师>金的试验 (I)
', 255, 100, 0, 0, 0, '<그랜드마스터>진의 시험 (I)', 1, 5, 1000, 0, 0, 0.0, 0.0, 0.0, 1, 10, 0, 2691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (914, '团长的委托(XV)', 255, 70, 0, 0, 0, '자경 단장의 부탁(XV)(스킬퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 42, 0, 2105, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (913, '团长的委托(XIV)', 255, 68, 0, 0, 0, '자경 단장의 부탁(XIV)(스킬퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 32, 0, 2105, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (912, '团长的委托(XIII)', 255, 66, 0, 0, 0, '자경 단장의 부탁(XIII)(스킬퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 22, 0, 2105, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (911, '团长的委托(XII)', 255, 64, 0, 0, 0, '자경 단장의 부탁(XII)(스킬퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 12, 0, 2105, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (910, '团长的委托(XI)', 255, 62, 0, 0, 0, '자경 단장의 부탁(XI)(스킬퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 2105, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (909, '团长的委托 (X)', 255, 60, 0, 0, 0, '자경 단장의 부탁 (X)(스킬퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 32, 0, 1692, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (908, '团长的委托 (IX)', 255, 55, 0, 0, 0, '자경 단장의 부탁 (IX)(스킬퀘스트)', 1, 4, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 22, 0, 1692, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (907, '团长的委托 (VIII)', 255, 50, 0, 0, 0, '자경 단장의 부탁 (VIII)(스킬퀘스트)', 1, 4, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 0, 1694, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (906, '团长的委托 (VII)', 255, 45, 0, 0, 0, '자경 단장의 부탁 (VII)(스킬퀘스트)', 1, 3, 0, 1, 111, 102029.0, 23709.0, 133186.0, 1, 2, 0, 1694, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (905, '团长的委托 (VI)', 255, 40, 0, 0, 0, '자경 단장의 부탁 (VI)(스킬퀘스트)', 1, 3, 0, 1, 75, 436945.0, 16299.0, 211363.0, 1, 2, 0, 1693, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (904, '团长的委托 (V)', 255, 35, 0, 0, 0, '자경 단장의 부탁 (V)(스킬퀘스트)', 1, 2, 0, 1, 46, 160510.0, 17506.0, 325407.0, 1, 12, 0, 1692, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (903, '团长的委托 (IV)', 255, 30, 0, 0, 0, '자경 단장의 부탁 (IV)(스킬퀘스트)', 1, 2, 0, 1, 58, 228146.0, 19789.0, 189716.0, 1, 2, 0, 1692, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (902, '团长的委托 (III)', 255, 25, 0, 0, 0, '자경 단장의 부탁 (III)(스킬퀘스트)', 1, 1, 0, 1, 21, 281512.0, 15152.0, 237431.0, 1, 22, 0, 1691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (901, '团长的委托 (II)', 255, 20, 0, 0, 0, '자경 단장의 부탁 (II)(스킬퀘스트)', 1, 1, 0, 1, 11, 378123.0, 15212.0, 234397.0, 1, 12, 0, 1691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (900, '团长的委托 (I)', 255, 15, 0, 0, 0, '자경 단장의 부탁 (I)(스킬퀘스트)', 1, 1, 0, 1, 16, 419366.0, 18374.0, 276226.0, 1, 2, 0, 1691, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (899, '装备支持
', 255, 1, 0, 0, 0, '특화서버 장비 지원', 0, 1, 0, 1, 0, 0.0, 0.0, 0.0, 0, 0, 0, 2857, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (898, '装备支持
', 255, 1, 0, 0, 0, '특화서버 장비 지원', 0, 1, 0, 1, 0, 0.0, 0.0, 0.0, 0, 0, 0, 2690, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (897, '消灭地下城怪物', 255, 79, -79, 0, 0, '[지하광장] 모든 몬스터 500마리 처치', 1, 4, 897, 0, 304, 1149979.0, 13932.0, 781331.0, 1, 16, 0, 2685, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (896, '消灭眼镜蛇战士', 255, 78, -79, 0, 0, '[지하광장] 코브라 병사 100마리 처치', 1, 4, 896, 0, 304, 1149979.0, 13932.0, 781331.0, 1, 15, 0, 2685, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (895, '消灭黑暗的追踪者', 255, 77, -79, 0, 0, '[지하광장] 어둠의 추적자 50마리 처치', 1, 4, 895, 0, 304, 1149979.0, 13932.0, 781331.0, 1, 14, 0, 2685, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (894, '消灭章鱼巫师', 255, 75, -79, 0, 0, '[지하광장] 문어 주술사 30마리 처치', 1, 4, 894, 0, 304, 1149979.0, 13932.0, 781331.0, 1, 13, 0, 2685, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (893, '消灭大地之剑', 255, 73, -79, 0, 0, '[지하광장] 대지의 검 50마리 처치', 1, 4, 893, 0, 304, 1149979.0, 13932.0, 781331.0, 1, 12, 0, 2685, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (892, '消灭大地之守护者', 255, 71, -79, 0, 0, '[지하광장] 대지의 수호자 30마리 처치', 1, 4, 892, 0, 304, 1149979.0, 13932.0, 781331.0, 1, 11, 0, 2685, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (891, '封印次元的缝隙 (III)', 255, 78, -79, 890, 0, '[지하광장] 시나리오 퀘스트 5번', 1, 4, 891, 1, 304, 1149979.0, 13932.0, 781331.0, 1, 15, 887, 2684, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (890, '封印次元的缝隙 (II)', 255, 76, -79, 889, 0, '[지하광장] 시나리오 퀘스트 4번', 1, 4, 890, 1, 304, 1149979.0, 13932.0, 781331.0, 1, 14, 887, 2684, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (889, '封印次元的缝隙 (I)', 255, 74, -79, 888, 0, '[지하광장] 시나리오 퀘스트 3번', 1, 4, 889, 1, 304, 1149979.0, 13932.0, 781331.0, 1, 13, 887, 2684, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (888, '调查次元的缝隙 (II)', 255, 72, -79, 887, 0, '[지하광장] 시나리오 퀘스트 2번', 1, 4, 888, 1, 304, 1149979.0, 13932.0, 781331.0, 1, 12, 887, 2684, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (887, '调查次元的缝隙 (I)', 255, 70, -79, 0, 0, '[지하광장] 시나리오 퀘스트 1번', 1, 4, 887, 1, 304, 1149979.0, 13932.0, 781331.0, 1, 11, 0, 2684, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (886, '消灭空中地下城怪物', 255, 69, -69, 0, 0, '[공중던전] 모든 몬스터 500마리 처치', 1, 3, 886, 0, 303, 1175072.0, 13899.0, 935780.0, 1, 16, 0, 2683, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (885, '消灭岩石巨人', 255, 68, -69, 0, 0, '[공중던전] 암석거인 100마리 처치', 1, 3, 885, 0, 303, 1175072.0, 13899.0, 935780.0, 1, 15, 0, 2683, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (884, '消灭天空之鬼', 255, 67, -69, 0, 0, '[공중던전] 하늘의 귀신 50마리 처치', 1, 3, 884, 0, 303, 1175072.0, 13899.0, 935780.0, 1, 14, 0, 2683, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (883, '消灭守护石壁', 255, 65, -69, 0, 0, '[공중던전] 수호비석 30마리 처치', 1, 3, 883, 0, 303, 1175072.0, 13899.0, 935780.0, 1, 13, 0, 2683, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (882, '消灭天空之剑', 255, 63, -69, 0, 0, '[공중던전] 천공의 검 50마리 처치', 1, 3, 882, 0, 303, 1175072.0, 13899.0, 935780.0, 1, 12, 0, 2683, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (881, '消灭天空之守护者', 255, 61, -69, 0, 0, '[공중던전] 천공의 수호자 30마리 처치', 1, 3, 881, 0, 303, 1175072.0, 13899.0, 935780.0, 1, 11, 0, 2683, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (880, '能力证明', 255, 68, -69, 879, 0, '[공중던전] 시나리오 퀘스트 5번', 1, 3, 880, 1, 303, 1175072.0, 13899.0, 935780.0, 1, 15, 876, 2682, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (879, '不停的挑战 (II)', 255, 66, -69, 878, 0, '[공중던전] 시나리오 퀘스트 4번', 1, 3, 879, 1, 303, 1175072.0, 13899.0, 935780.0, 1, 14, 876, 2682, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (878, '不停的挑战 (I)', 255, 64, -69, 877, 0, '[공중던전] 시나리오 퀘스트 3번', 1, 3, 878, 1, 303, 1175072.0, 13899.0, 935780.0, 1, 13, 876, 2682, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (877, '新的挑战 (II)', 255, 62, -69, 876, 0, '[공중던전] 시나리오 퀘스트 2번', 1, 3, 877, 1, 303, 1175072.0, 13899.0, 935780.0, 1, 12, 876, 2682, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (876, '新的挑战 (I)', 255, 60, -69, 0, 0, '[공중던전] 시나리오 퀘스트 1번', 1, 3, 876, 1, 303, 1175072.0, 13899.0, 935780.0, 1, 11, 0, 2682, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (875, '消灭拉尔卡', 255, 1, 0, 874, 0, '[만월 던전] 하드코어 퀘스트 5 (밤)', 1, 5, 875, 0, 302, 0.0, 0.0, 0.0, 1, 51, 871, 2624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (874, '扫荡盈月遗址地平线(II)', 255, 1, 0, 873, 0, '[만월 던전] 하드코어 퀘스트 4 (밤)', 1, 5, 874, 0, 301, 0.0, 0.0, 0.0, 1, 41, 871, 2624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (873, '消灭拉尔卡高级骑士', 255, 1, 0, 872, 0, '[만월 던전] 하드코어 퀘스트 3 (밤)', 1, 5, 873, 0, 301, 0.0, 0.0, 0.0, 1, 31, 871, 2624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (872, '消灭拉尔卡长枪兵', 255, 1, 0, 871, 0, '[만월 던전] 하드코어 퀘스트 2 (밤)', 1, 5, 872, 0, 301, 0.0, 0.0, 0.0, 1, 21, 871, 2624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (871, '消灭拉尔卡近卫骑士', 255, 1, 0, 0, 0, '[만월 던전] 하드코어 퀘스트 1 (밤)', 1, 5, 871, 0, 301, 0.0, 0.0, 0.0, 1, 10, 0, 2624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (870, '消灭塔纳托斯', 255, 1, 0, 869, 0, '[만월 던전] 하드코어 퀘스트 5 (낮)', 1, 5, 870, 0, 302, 0.0, 0.0, 0.0, 1, 51, 866, 2625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (869, '扫荡盈月遗址地平线', 255, 1, 0, 868, 0, '[만월 던전] 하드코어 퀘스트 4 (낮)', 1, 5, 869, 0, 301, 0.0, 0.0, 0.0, 1, 41, 866, 2625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (868, '消灭塔纳托斯锤子兵', 255, 1, 0, 867, 0, '[만월 던전] 하드코어 퀘스트 3 (낮)', 1, 5, 868, 0, 301, 0.0, 0.0, 0.0, 1, 31, 866, 2625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (867, '消灭塔纳托斯狙击手', 255, 1, 0, 866, 0, '[만월 던전] 하드코어 퀘스트 2 (낮)', 1, 5, 867, 0, 301, 0.0, 0.0, 0.0, 1, 21, 866, 2625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (866, '消灭塔纳托斯盾牌兵', 255, 1, 0, 0, 0, '[만월 던전] 하드코어 퀘스트 1 (낮)', 1, 5, 866, 0, 301, 0.0, 0.0, 0.0, 1, 10, 0, 2625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (865, '伊莉娜的委托 (II)', 255, 1, 0, 856, 0, '서번트 획득 퀘스트 (II)', 1, 1, 865, 0, 18, 338111.0, 18304.0, 240464.0, 1, 22, 0, 2579, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (864, '调查光荣圣殿', 255, 1, 0, 0, 1, '[일루미나의 성지] 퀘코 ', 1, 5, 0, 1, 228, 598660.0, 16523.0, 381050.0, 1, 20, 0, 2608, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (863, '巴林的备忘录', 255, 1, 0, 862, 0, '[일루미나의 성지] 퀘코 선행퀘6', 1, 5, 0, 1, 227, 599868.0, 15205.0, 395409.0, 1, 5, 858, 2608, 2608, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (862, '调查混乱的洞窟', 255, 1, 0, 861, 0, '[일루미나의 성지] 퀘코 선행퀘5', 1, 5, 0, 1, 231, 572839.0, 17759.0, 384638.0, 1, 3, 858, 2609, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (861, '转达标本', 255, 1, 0, 860, 0, '[일루미나의 성지] 퀘코 선행퀘4', 1, 5, 0, 1, 230, 576297.0, 18818.0, 375525.0, 1, 16, 858, 2609, 2609, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (860, '调查变种动物岩石地带', 255, 1, 0, 859, 0, '[일루미나의 성지] 퀘코 선행퀘3', 1, 5, 0, 1, 230, 578696.0, 18731.0, 370938.0, 1, 7, 858, 2608, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (859, '调查德拉古兰的巢穴', 255, 1, 0, 858, 0, '[일루미나의 성지] 퀘코 선행퀘2', 1, 5, 0, 1, 229, 620306.0, 19079.0, 374607.0, 1, 4, 858, 2608, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (858, '整理变质的沼泽', 255, 1, 0, 0, 0, '[일루미나의 성지] 퀘코 선행퀘1', 1, 5, 0, 1, 228, 598660.0, 16523.0, 381050.0, 1, 2, 0, 2608, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (857, '为了最好而努力', 255, 1, 0, 0, 1, '신기아이템 기간 연장 퀘스트', 1, 1, 857, 0, 14, 389871.0, 16944.0, 278642.0, 1, 2, 0, 2607, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (856, '伊莉娜的委托 (I)', 255, 1, 0, 0, 0, '서번트 획득 퀘스트', 1, 1, 856, 0, 11, 391501.0, 15929.0, 294409.0, 1, 12, 0, 2579, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (855, '天国之门', 255, 60, 0, 854, 0, '[엘테르] 유피테르 던전 시나리오 퀘스트 4번', 1, 5, 855, 0, 294, 841073.0, 27731.0, 1043882.0, 1, 13, 815, 2486, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (854, '侦察队的任务', 255, 60, 0, 853, 0, '[엘테르] 유피테르 던전 시나리오 퀘스트 3번', 1, 5, 854, 1, 294, 841073.0, 27731.0, 1043882.0, 1, 77, 815, 2486, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (853, '天使基地', 255, 60, 0, 852, 0, '[엘테르] 유피테르 던전 시나리오 퀘스트 2번', 1, 5, 853, 1, 294, 841073.0, 27731.0, 1043882.0, 1, 5, 815, 2486, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (852, '朱庇特遗迹', 255, 60, 0, 822, 0, '[엘테르] 유피테르 던전 시나리오 퀘스트 1번', 1, 5, 852, 1, 283, 841073.0, 27731.0, 1043882.0, 1, 4, 815, 2486, 2486, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (851, '地狱之门', 255, 60, 0, 850, 0, '[엘테르] 바알베크 던전 시나리오 퀘스트 4번', 1, 5, 851, 0, 295, 752525.0, 47503.0, 1163741.0, 1, 13, 807, 2484, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (850, '敢死队的任务', 255, 60, 0, 849, 0, '[엘테르] 바알베크 던전 시나리오 퀘스트 3번', 1, 5, 850, 1, 295, 752525.0, 47503.0, 1163741.0, 1, 77, 807, 2484, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (849, '恶魔基地', 255, 60, 0, 848, 0, '[엘테르] 바알베크 던전 시나리오 퀘스트 2번', 1, 5, 849, 1, 295, 752525.0, 47503.0, 1163741.0, 1, 5, 807, 2484, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (848, '巴贝克遗迹', 255, 60, 0, 814, 0, '[엘테르] 바알베크 던전 시나리오 퀘스트 1번', 1, 5, 848, 1, 287, 751132.0, 50914.0, 1145936.0, 1, 4, 807, 2484, 2484, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (847, '巴贝克的残骸(4)', 255, 60, 0, 846, 0, '바알베크 던전 하드코어 변신 퀘스트 4번', 1, 5, 847, 0, 295, 752525.0, 47503.0, 1163741.0, 1, 12, 844, 2482, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (846, '巴贝克的残骸(3)', 255, 60, 0, 845, 0, '바알베크 던전 하드코어 변신 퀘스트 3번', 1, 5, 0, 1, 295, 752525.0, 47503.0, 1163741.0, 1, 77, 844, 2482, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (845, '巴贝克的残骸(2)', 255, 60, 0, 844, 0, '바알베크 던전 하드코어 변신 퀘스트 2번', 1, 5, 845, 0, 295, 752525.0, 47503.0, 1163741.0, 1, 5, 844, 2482, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (844, '巴贝克的残骸(1)', 255, 60, 0, 0, 0, '바알베크 던전 하드코어 변신 퀘스트 1번', 1, 5, 0, 1, 287, 751132.0, 50914.0, 1145936.0, 1, 2, 0, 2482, 2482, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (843, '朱庇特的痕迹(4)', 255, 60, 0, 842, 0, '유피테르 던전 하드코어 변신 퀘스트 4번', 1, 5, 843, 0, 294, 841073.0, 27731.0, 1043882.0, 1, 12, 840, 2480, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (842, '朱庇特的痕迹(3)', 255, 60, 0, 841, 0, '유피테르 던전 하드코어 변신 퀘스트 3번', 1, 5, 0, 1, 294, 841073.0, 27731.0, 1043882.0, 1, 77, 840, 2480, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (841, '朱庇特的痕迹(2)', 255, 60, 0, 840, 0, '유피테르 던전 하드코어 변신 퀘스트 2번', 1, 5, 841, 0, 294, 841073.0, 27731.0, 1043882.0, 1, 5, 840, 2480, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (840, '朱庇特的痕迹(1)', 255, 60, 0, 0, 0, '유피테르 던전 하드코어 변신 퀘스트 1번', 1, 5, 0, 1, 283, 849289.0, 29496.0, 1051503.0, 1, 2, 0, 2480, 2480, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (839, '火焰与雷电的协奏曲(4)', 255, 60, 0, 838, 0, '화탑 봉인지 하드코어 퀘스트 4번 ', 1, 5, 839, 0, 0, 0.0, 0.0, 0.0, 1, 5, 836, 2478, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (838, '火焰与雷电的协奏曲(3)', 255, 60, 0, 837, 0, '화탑 봉인지 하드코어 퀘스트 3번 ', 1, 5, 0, 1, 0, 0.0, 0.0, 0.0, 1, 12, 836, 2478, 2478, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (837, '火焰与雷电的协奏曲(2)', 255, 60, 0, 836, 0, '화탑 봉인지 하드코어 퀘스트 2번 ', 1, 5, 837, 0, 0, 0.0, 0.0, 0.0, 1, 8, 836, 2477, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (836, '火焰与雷电的协奏曲(1)', 255, 60, 0, 0, 0, '화탑 봉인지 하드코어 퀘스트 1번 ', 1, 5, 0, 1, 0, 0.0, 0.0, 0.0, 1, 4, 0, 2477, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (835, '获得赛欧托的魔力石', 255, 60, 0, 0, 1, '화탑 봉인지 입장 퀘스트(보스처치)', 1, 5, 835, 0, 17, 464098.0, 21358.0, 306728.0, 1, 8, 0, 2476, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (834, '隐藏的上层(3)', 255, 60, 0, 833, 1, '화탑 봉인지 입장 아이템 퀘스트 3번', 1, 5, 0, 0, 0, 0.0, 0.0, 0.0, 1, 11, 0, 2475, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (833, '隐藏的上层(2)', 255, 60, 0, 832, 0, '화탑 봉인지 입장 아이템 퀘스트 2번', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 832, 2475, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (832, '隐藏的上层(1)', 255, 60, 0, 0, 0, '화탑 봉인지 입장 아이템 퀘스트 1번', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 2475, 2475, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (831, '获得战利品', 255, 1, 0, 0, 1, '전리품 획득 퀘스트', 1, 1, 831, 0, 14, 389871.0, 16944.0, 278642.0, 1, 2, 0, 2314, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (830, '没有永远的清晨', 255, 60, 0, 0, 1, '[엘테르] 유피테르 진영 일일퀘스트', 1, 5, 0, 1, 283, 849289.0, 29496.0, 1051503.0, 1, 2, 0, 2251, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (829, '他们只剩使命', 255, 60, 0, 0, 1, '[엘테르] 유피테르 진영 일일퀘스트', 1, 5, 0, 1, 282, 829564.0, 24734.0, 1026421.0, 1, 2, 0, 2250, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (828, '扭曲的命运', 255, 60, 0, 0, 1, '[엘테르] 유피테르 진영 일일퀘스트', 1, 5, 0, 1, 281, 806185.0, 22821.0, 1050885.0, 1, 2, 0, 2249, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (827, '黑暗追踪者', 255, 60, 0, 0, 1, '[엘테르] 유피테르 진영 일일퀘스트', 1, 5, 0, 1, 280, 841692.0, 25053.0, 1107432.0, 1, 2, 0, 2248, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (826, '黑暗一族', 255, 60, 0, 0, 1, '[엘테르] 바알베크 진영 일일퀘스트', 1, 5, 0, 1, 287, 751132.0, 50914.0, 1145936.0, 1, 2, 0, 2247, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (825, '净化怨恨', 255, 60, 0, 0, 1, '[엘테르] 바알베크 진영 일일퀘스트', 1, 5, 0, 1, 286, 782713.0, 43991.0, 1166259.0, 1, 2, 0, 2246, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (824, '防止疫病', 255, 60, 0, 0, 1, '[엘테르] 바알베크 진영 일일퀘스트', 1, 5, 0, 1, 285, 774313.0, 41310.0, 1142619.0, 1, 2, 0, 2245, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (823, '光的追踪者', 255, 60, 0, 0, 1, '[엘테르] 바알베크 진영 일일퀘스트', 1, 5, 0, 1, 284, 719329.0, 33950.0, 1112264.0, 1, 2, 0, 2244, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (822, '他们想要的', 255, 60, 0, 821, 0, '[엘테르] 시나리오 퀘스트 26번', 1, 5, 822, 0, 283, 849289.0, 29496.0, 1051503.0, 1, 3, 815, 2243, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (821, '研究所长哈伯斯', 255, 60, 0, 820, 0, '[엘테르] 시나리오 퀘스트 25번', 1, 5, 821, 0, 283, 855689.0, 25290.0, 1028863.0, 1, 6, 815, 2243, 2243, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (820, '他们的使命', 255, 60, 0, 819, 0, '[엘테르] 시나리오 퀘스트 24번', 1, 5, 820, 0, 282, 829564.0, 24734.0, 1026421.0, 1, 3, 815, 2242, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (819, '调查员拉夏', 255, 60, 0, 818, 0, '[엘테르] 시나리오 퀘스트 23번', 1, 5, 819, 0, 282, 829564.0, 24734.0, 1026421.0, 1, 6, 815, 2242, 2242, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (818, '收集魔力', 255, 60, 0, 817, 0, '[엘테르] 시나리오 퀘스트 22번', 1, 5, 818, 0, 281, 806185.0, 22821.0, 1050885.0, 1, 3, 815, 2241, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (817, '支援艾比昂', 255, 60, 0, 816, 0, '[엘테르] 시나리오 퀘스트 21번', 1, 5, 817, 0, 280, 841692.0, 25053.0, 1107432.0, 1, 6, 815, 2241, 2241, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (816, '森林守护者', 255, 60, 0, 815, 0, '[엘테르] 시나리오 퀘스트 20번', 1, 5, 816, 0, 280, 841692.0, 25053.0, 1107432.0, 1, 3, 815, 2240, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (815, '朱庇特阵营支援', 255, 60, 0, 806, 0, '[엘테르] 시나리오 퀘스트 19번', 1, 5, 815, 0, 280, 816911.0, 25478.0, 1095250.0, 1, 9, 0, 2240, 2240, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (814, '恶魔的回归', 255, 60, 0, 813, 0, '[엘테르] 시나리오 퀘스트 18번', 1, 5, 814, 0, 287, 751132.0, 50914.0, 1145936.0, 1, 3, 807, 2239, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (813, '团长贝洛克', 255, 60, 0, 812, 0, '[엘테르] 시나리오 퀘스트 17번', 1, 5, 813, 0, 287, 747535.0, 49077.0, 1183957.0, 1, 6, 807, 2239, 2239, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (812, '怨恨留下来的黑暗', 255, 60, 0, 811, 0, '[엘테르] 시나리오 퀘스트 16번', 1, 5, 812, 0, 286, 782713.0, 43991.0, 1166259.0, 1, 3, 807, 2238, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (811, '侦察兵拉米罗斯', 255, 60, 0, 810, 0, '[엘테르] 시나리오 퀘스트 15번', 1, 5, 811, 0, 286, 782713.0, 43991.0, 1166259.0, 1, 6, 807, 2238, 2238, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (810, '地狱疫病', 255, 60, 0, 809, 0, '[엘테르] 시나리오 퀘스트 14번', 1, 5, 810, 0, 285, 774313.0, 41310.0, 1142619.0, 1, 3, 807, 2237, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (809, '搭救阿扎尔', 255, 60, 0, 808, 0, '[엘테르] 시나리오 퀘스트 13번', 1, 5, 809, 0, 285, 774313.0, 41310.0, 1142619.0, 1, 6, 807, 2237, 2237, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (808, '巢穴里的巨大生命体', 255, 60, 0, 807, 0, '[엘테르] 시나리오 퀘스트 12번', 1, 5, 808, 0, 284, 719329.0, 33950.0, 1112264.0, 1, 3, 807, 2236, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (807, '巴贝克阵营支援', 255, 60, 0, 806, 0, '[엘테르] 시나리오 퀘스트 11번', 1, 5, 807, 0, 284, 730230.0, 32821.0, 1116370.0, 1, 8, 0, 2236, 2236, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (806, '返回城镇', 255, 60, 0, 805, 0, '[엘테르] 시나리오 퀘스트 10번', 1, 5, 806, 0, 274, 792955.0, 29996.0, 1100546.0, 1, 6, 797, 2231, 2231, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (805, '强力的存在', 255, 60, 0, 804, 0, '[엘테르] 시나리오 퀘스트 9번', 1, 5, 805, 0, 291, 778890.0, 23059.0, 1066186.0, 1, 3, 797, 2235, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (804, '给哈尔曼的信', 255, 60, 0, 803, 0, '[엘테르] 시나리오 퀘스트 8번', 1, 5, 804, 0, 291, 770176.0, 21273.0, 1061904.0, 1, 6, 797, 2235, 2235, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (803, '阿贝尔森的记忆', 255, 60, 0, 802, 0, '[엘테르] 시나리오 퀘스트 7번', 1, 5, 803, 0, 290, 772194.0, 15402.0, 1021046.0, 1, 3, 797, 2234, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (802, '阿贝尔森的徒弟伽罗', 255, 60, 0, 801, 0, '[엘테르] 시나리오 퀘스트 6번', 1, 5, 802, 0, 290, 759493.0, 18204.0, 1021266.0, 1, 6, 797, 2234, 2234, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (801, '阿贝尔森的教导', 255, 60, 0, 800, 0, '[엘테르] 시나리오 퀘스트 5번', 1, 5, 801, 0, 289, 730783.0, 19394.0, 1034017.0, 1, 3, 797, 2233, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (800, '海利的呼救', 255, 60, 0, 799, 0, '[엘테르] 시나리오 퀘스트 4번', 1, 5, 800, 0, 289, 727820.0, 19600.0, 1032765.0, 1, 6, 797, 2233, 2233, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (799, '为了存活', 255, 60, 0, 798, 0, '[엘테르] 시나리오 퀘스트 3번', 1, 5, 799, 0, 288, 736399.0, 29311.0, 1087626.0, 1, 3, 797, 2232, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (798, '访问呃齐温', 255, 60, 0, 797, 0, '[엘테르] 시나리오 퀘스트 2번', 1, 5, 798, 0, 288, 751676.0, 29618.0, 1086068.0, 1, 5, 797, 2232, 2232, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (797, '艾泰尔的引导', 255, 60, 0, 0, 0, '[엘테르] 시나리오 퀘스트 1번', 1, 5, 797, 0, 274, 776659.0, 29924.0, 1098783.0, 1, 2, 0, 2231, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (796, '无名洞窟的主人(3)', 255, 1, 0, 795, 1, '[에르테스]퀘스트 코인 퀘스트 6번', 1, 1, 796, 0, 115, 180001.0, 25926.0, 63013.0, 1, 6, 0, 2139, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (795, '无名洞窟的主人(2)', 255, 1, 0, 794, 0, '[에르테스]퀘스트 코인 퀘스트 5번', 1, 1, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 3, 791, 2139, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (794, '无名洞窟的主人(1)', 255, 1, 0, 793, 0, '[에르테스]퀘스트 코인 퀘스트 4번', 1, 1, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 12, 791, 2139, 2139, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (793, '美丽的无名洞窟(3)', 255, 1, 0, 792, 0, '[에르테스]퀘스트 코인 퀘스트 3번', 1, 1, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 9, 791, 2138, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (792, '美丽的无名洞窟(2)', 255, 1, 0, 791, 0, '[에르테스]퀘스트 코인 퀘스트 2번', 1, 1, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 5, 791, 2138, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (791, '美丽的无名洞窟(1)', 255, 1, 0, 0, 0, '[에르테스]퀘스트 코인 퀘스트 1번', 1, 1, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 2, 0, 2138, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (790, '制作埃吉尔, 石像魔(4)', 255, 1, 0, 789, 1, '[에기르]퀘스트 코인 퀘스트 6번', 1, 1, 790, 0, 76, 348582.0, 11214.0, 153010.0, 1, 9, 0, 2137, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (789, '制作埃吉尔, 石像魔(3)', 255, 1, 0, 788, 0, '[에기르]퀘스트 코인 퀘스트 5번', 1, 1, 0, 1, 76, 348582.0, 11214.0, 153010.0, 1, 6, 785, 2137, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (788, '制作埃吉尔, 石像魔(2)', 255, 1, 0, 787, 0, '[에기르]퀘스트 코인 퀘스트 4번', 1, 1, 0, 1, 76, 348582.0, 11214.0, 153010.0, 1, 3, 785, 2137, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (787, '制作埃吉尔, 石像魔(1)', 255, 1, 0, 786, 0, '[에기르]퀘스트 코인 퀘스트 3번', 1, 1, 0, 1, 76, 348582.0, 11214.0, 153010.0, 1, 9, 785, 2137, 2137, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (786, '埃吉尔的秘密研究(2)', 255, 1, 0, 785, 0, '[에기르]퀘스트 코인 퀘스트 2번', 1, 1, 0, 1, 76, 348582.0, 11214.0, 153010.0, 1, 5, 785, 2136, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (785, '埃吉尔的秘密研究(1)', 255, 1, 0, 0, 0, '[에기르]퀘스트 코인 퀘스트 1번', 1, 1, 0, 1, 76, 348582.0, 11214.0, 153010.0, 1, 2, 0, 2136, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (784, '巴力毗珥的复活(2)', 255, 50, 0, 783, 0, '', 1, 1, 784, 0, 46, 160510.0, 17506.0, 325407.0, 1, 8, 777, 2135, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (783, '巴力毗珥的复活(1)', 255, 50, 0, 782, 0, '', 1, 1, 0, 1, 46, 160510.0, 17506.0, 325407.0, 1, 3, 777, 2135, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (782, '巴力毗珥藏身处', 255, 50, 0, 781, 0, '', 1, 1, 0, 1, 46, 160510.0, 17506.0, 325407.0, 1, 12, 777, 2135, 2135, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (781, '烈火匹斯的复活(2)', 255, 50, 0, 780, 0, '', 1, 1, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 8, 777, 2134, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (780, '烈火匹斯的复活(1)', 255, 50, 0, 779, 0, '', 1, 1, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 3, 777, 2134, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (779, '烈火匹斯藏身处', 255, 50, 0, 778, 0, '', 1, 1, 779, 0, 17, 464098.0, 21358.0, 306728.0, 1, 9, 777, 2134, 2134, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (778, '巫妖王的复活(2)', 255, 50, 0, 777, 0, '', 1, 1, 0, 1, 58, 228146.0, 19789.0, 189716.0, 1, 5, 777, 2133, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (777, '巫妖王的复活(1)', 255, 50, 0, 0, 0, '', 1, 1, 0, 1, 58, 228146.0, 19789.0, 189716.0, 1, 2, 0, 2133, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (776, '奥秘之地(4)', 255, 40, 0, 775, 0, '[로덴]원거리 하드코어 퀘스트 7번', 1, 1, 776, 0, 111, 48406.0, 19981.0, 85761.0, 1, 12, 770, 2131, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (775, '奥秘之地(3)', 255, 40, 0, 774, 0, '[로덴]원거리 하드코어 퀘스트 6번', 1, 1, 0, 1, 111, 48406.0, 19981.0, 85761.0, 1, 12, 770, 2131, 2131, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (774, '奥秘之地(2)', 255, 40, 0, 773, 0, '[로덴]원거리 하드코어 퀘스트 5번', 1, 1, 0, 1, 118, 45032.0, 16594.0, 167260.0, 1, 8, 770, 2132, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (773, '奥秘之地(1)', 255, 40, 0, 772, 0, '[로덴]원거리 하드코어 퀘스트 4번', 1, 1, 0, 1, 119, 71383.0, 20417.0, 220155.0, 1, 3, 770, 2132, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (772, '移居计划', 255, 40, 0, 771, 0, '[로덴]원거리 하드코어 퀘스트 3번', 1, 1, 772, 0, 119, 71383.0, 20417.0, 220155.0, 1, 9, 770, 2132, 2132, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (771, '最后的住民(2)', 255, 40, 0, 770, 0, '[로덴]원거리 하드코어 퀘스트 2번', 1, 1, 0, 1, 116, 133793.0, 20095.0, 136485.0, 1, 5, 770, 2131, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (770, '最后的住民(1)', 255, 40, 0, 0, 0, '[로덴]원거리 하드코어 퀘스트 1번', 1, 1, 0, 1, 126, 228276.0, 21072.0, 76963.0, 1, 2, 0, 2131, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (769, '被污染之地(3)', 255, 30, 0, 768, 0, '[블랙랜드]원거리 하드코어 퀘스트 7번', 1, 1, 769, 0, 0, 0.0, 0.0, 0.0, 1, 8, 763, 2130, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (768, '被污染之地(2)', 255, 30, 0, 767, 0, '[블랙랜드]원거리 하드코어 퀘스트 6번', 1, 1, 0, 1, 44, 85846.0, 11329.0, 261108.0, 1, 4, 763, 2130, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (767, '被污染之地(1)', 255, 30, 0, 766, 0, '[블랙랜드]원거리 하드코어 퀘스트 5번', 1, 1, 0, 1, 44, 85846.0, 11329.0, 261108.0, 1, 9, 763, 2130, 2130, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (766, '红头发女士', 255, 30, 0, 765, 0, '[블랙랜드]원거리 하드코어 퀘스트 4번', 1, 1, 0, 1, 50, 192836.0, 16526.0, 156537.0, 1, 5, 763, 2129, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (765, '不知去向的叔叔', 255, 30, 0, 764, 0, '[블랙랜드]원거리 하드코어 퀘스트 3번', 1, 1, 0, 1, 50, 192836.0, 16526.0, 156537.0, 1, 14, 763, 2129, 2129, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (764, '阴森之地', 255, 30, 0, 763, 0, '[블랙랜드]원거리 하드코어 퀘스트 2번', 1, 1, 0, 1, 58, 220679.0, 20245.0, 192661.0, 1, 10, 763, 2128, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (763, '愉快的时间', 255, 30, 0, 0, 0, '[블랙랜드]원거리 하드코어 퀘스트 1번', 1, 1, 0, 1, 21, 281512.0, 15152.0, 237431.0, 1, 4, 0, 2128, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (762, '获得阿特努斯魔力石', 255, 60, 0, 0, 1, '왕의 무덤 봉인지 입장 퀘스트 (보스처치)', 1, 1, 762, 0, 46, 160510.0, 17506.0, 325407.0, 1, 8, 0, 2123, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (761, '赫尔墨斯的魔力石', 255, 60, 0, 0, 1, '흑룡의 늪 봉인지 입장 퀘스트 (보스처치)', 1, 1, 761, 0, 44, 88862.0, 11431.0, 270346.0, 1, 8, 0, 2120, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (760, '隐藏的上层(3) - 帝王墓穴', 255, 60, 0, 759, 1, '왕의 무덤 봉인지 입장 퀘스트 3번', 1, 1, 760, 0, 0, 0.0, 0.0, 0.0, 1, 11, 0, 2122, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (759, '隐藏的上层(2) - 帝王墓穴', 255, 60, 0, 758, 0, '왕의 무덤 봉인지 입장 퀘스트 2번', 1, 1, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 758, 2122, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (758, '隐藏的上层(1) - 帝王墓穴', 255, 60, 0, 0, 0, '왕의 무덤 봉인지 입장 퀘스트 1번', 1, 1, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2122, 2122, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (757, '隐藏的上层(3) - 黑龙沼泽', 255, 60, 0, 756, 1, '흑룡의 늪 봉인지 입장 퀘스트 3번', 1, 1, 757, 0, 0, 0.0, 0.0, 0.0, 1, 11, 0, 2119, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (756, '隐藏的上层(2) - 黑龙沼泽', 255, 60, 0, 755, 0, '흑룡의 늪 봉인지 입장 퀘스트 2번', 1, 1, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 755, 2119, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (755, '隐藏的上层(1) - 黑龙沼泽', 255, 60, 0, 0, 0, '흑룡의 늪 봉인지 입장 퀘스트 1번', 1, 1, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 0, 2119, 2119, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (754, '堕落之王的残骸(4)', 255, 60, 0, 753, 0, '왕의 무덤 봉인지 하드코어 퀘스트 4번', 1, 1, 754, 0, 0, 0.0, 0.0, 0.0, 1, 4, 751, 2127, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (753, '堕落之王的残骸(3)', 255, 60, 0, 752, 0, '왕의 무덤 봉인지 하드코어 퀘스트 3번', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 10, 751, 2127, 2127, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (752, '堕落之王的残骸(2)', 255, 60, 0, 751, 0, '왕의 무덤 봉인지 하드코어 퀘스트 2번', 1, 1, 752, 0, 0, 0.0, 0.0, 0.0, 1, 5, 751, 2126, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (751, '堕落之王的残骸(1)', 255, 60, 0, 0, 0, '왕의 무덤 봉인지 하드코어 퀘스트 1번', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 2, 0, 2126, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (750, '污染的缘由(4)', 255, 60, 0, 749, 0, '흑룡의 늪 봉인지 하드코어 퀘스트 4번', 1, 1, 750, 0, 0, 0.0, 0.0, 0.0, 1, 4, 747, 2125, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (749, '污染的缘由(3)', 255, 60, 0, 748, 0, '흑룡의 늪 봉인지 하드코어 퀘스트 3번', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 9, 747, 2125, 2125, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (748, '污染的缘由(2)', 255, 60, 0, 747, 0, '흑룡의 늪 봉인지 하드코어 퀘스트 2번', 1, 1, 748, 0, 0, 0.0, 0.0, 0.0, 1, 5, 747, 2124, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (747, '污染的缘由(1)', 255, 60, 0, 0, 0, '흑룡의 늪 봉인지 하드코어 퀘스트 1번', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 2, 0, 2124, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (746, '伊罗尼的嗜血骷髅骑士', 255, 15, 0, 0, 1, '이로닌의 스켈레톤 나이트', 1, 1, 746, 0, 58, 220417.0, 20264.0, 193260.0, 1, 21, 0, 1722, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (745, '新的英雄诞生', 255, 35, 0, 744, 0, '[로덴] 리뉴얼 퀘스트 17번', 1, 1, 745, 0, 120, 202207.0, 24591.0, 95324.0, 1, 6, 729, 1771, 1771, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (744, '崩溃的开始', 255, 35, 0, 743, 0, '[로덴] 리뉴얼 퀘스트 16번', 1, 1, 744, 0, 111, 48406.0, 19981.0, 85761.0, 1, 3, 729, 2117, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (743, '巨大异性生物', 255, 35, 0, 742, 0, '[로덴] 리뉴얼 퀘스트 15번', 1, 1, 743, 0, 118, 45032.0, 16594.0, 167260.0, 1, 6, 729, 2117, 2117, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (742, '强力的存在', 255, 35, 0, 741, 0, '[로덴] 리뉴얼 퀘스트 14번', 1, 1, 742, 0, 119, 71383.0, 20417.0, 220155.0, 1, 3, 729, 2116, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (741, '牛角魔宫支援', 255, 35, 0, 740, 0, '[로덴] 리뉴얼 퀘스트 13번', 1, 1, 741, 0, 117, 96463.0, 19739.0, 163556.0, 1, 6, 729, 2116, 2116, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (740, '石碑的记忆', 255, 35, 0, 739, 0, '[로덴] 리뉴얼 퀘스트 12번', 1, 1, 740, 0, 117, 96463.0, 19739.0, 163556.0, 1, 3, 729, 2115, 2115, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (739, '遗忘的石碑', 255, 35, 0, 738, 0, '[로덴] 리뉴얼 퀘스트 11번', 1, 1, 739, 0, 117, 96463.0, 19739.0, 163556.0, 1, 3, 729, 1770, 1770, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (738, '时间之湖', 255, 35, 0, 737, 0, '[로덴] 리뉴얼 퀘스트 10번', 1, 1, 738, 0, 117, 96463.0, 19739.0, 163556.0, 1, 7, 729, 2115, 2115, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (737, '英雄探测器', 255, 35, 0, 736, 0, '[로덴] 리뉴얼 퀘스트 9번', 1, 1, 737, 0, 116, 133793.0, 20095.0, 136485.0, 1, 3, 729, 2114, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (736, '英雄的线索', 255, 35, 0, 735, 0, '[로덴] 리뉴얼 퀘스트 8번', 1, 1, 736, 0, 116, 133793.0, 20095.0, 136485.0, 1, 3, 729, 2114, 2114, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (735, '寻找英雄', 255, 35, 0, 734, 0, '[로덴] 리뉴얼 퀘스트 7번', 1, 1, 735, 0, 120, 202207.0, 24591.0, 95324.0, 1, 6, 729, 1769, 1769, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (734, '解析书记期间', 255, 35, 0, 733, 0, '[로덴] 리뉴얼 퀘스트 6번', 1, 1, 734, 0, 113, 200478.0, 25898.0, 50858.0, 1, 3, 729, 2113, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (733, '解析专家艾伯特', 255, 35, 0, 732, 0, '[로덴] 리뉴얼 퀘스트 5번', 1, 1, 733, 0, 113, 212732.0, 24693.0, 45974.0, 1, 3, 729, 2113, 2113, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (732, '钢铁兽人族调查', 255, 35, 0, 731, 0, '[로덴] 리뉴얼 퀘스트 4번', 1, 1, 732, 0, 126, 228276.0, 21072.0, 76963.0, 1, 6, 729, 1772, 1772, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (731, '重逢的两势力', 255, 35, 0, 730, 0, '[로덴] 리뉴얼 퀘스트 3번', 1, 1, 731, 0, 120, 202207.0, 24591.0, 95324.0, 1, 4, 729, 1766, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (730, '伊克伦营地', 255, 35, 0, 729, 0, '[로덴] 리뉴얼 퀘스트 2번', 1, 1, 730, 0, 120, 202207.0, 24591.0, 95324.0, 1, 7, 729, 1766, 1766, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (729, '野生动物保护法', 255, 35, 0, 0, 0, '[로덴] 리뉴얼 퀘스트 1번', 1, 1, 729, 0, 94, 184550.0, 22079.0, 121212.0, 1, 3, 0, 761, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (728, '给罗敦转达紧急状况', 255, 28, 0, 727, 0, '[바이런] 리뉴얼 퀘스트 17번', 1, 1, 728, 0, 77, 253942.0, 11344.0, 120093.0, 1, 10, 712, 761, 761, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (727, '石锤镇的叛乱', 255, 28, 0, 726, 0, '[바이런] 리뉴얼 퀘스트 16번', 1, 1, 727, 0, 79, 465915.0, 21973.0, 135072.0, 1, 5, 712, 1765, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (726, '转达证件', 255, 28, 0, 725, 0, '[바이런] 리뉴얼 퀘스트 15번', 1, 1, 726, 0, 73, 423514.0, 21911.0, 135993.0, 1, 8, 712, 1765, 1765, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (725, '与混血豺狼的约定', 255, 28, 0, 724, 0, '[바이런] 리뉴얼 퀘스트 14번', 1, 1, 725, 0, 81, 327999.0, 19556.0, 112082.0, 1, 11, 712, 2111, 2111, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (724, '混血豺狼的主食', 255, 28, 0, 723, 0, '[바이런] 리뉴얼 퀘스트 13번', 1, 1, 724, 0, 81, 324324.0, 22912.0, 97011.0, 1, 6, 712, 2112, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (723, '意识主导者', 255, 28, 0, 722, 0, '[바이런] 리뉴얼 퀘스트 12번', 1, 1, 723, 0, 81, 327305.0, 23155.0, 83284.0, 1, 4, 712, 2112, 2112, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (722, '堵住不详的意识', 255, 28, 0, 721, 0, '[바이런] 리뉴얼 퀘스트 11번', 1, 1, 722, 0, 81, 325185.0, 19695.0, 115395.0, 1, 8, 712, 2111, 2111, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (721, '守护弗波塔', 255, 28, 0, 720, 0, '[바이런] 리뉴얼 퀘스트 10번', 1, 1, 721, 0, 76, 339022.0, 11793.0, 159264.0, 1, 4, 712, 1764, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (720, '石化蜥蜴的心脏', 255, 28, 0, 719, 0, '[바이런] 리뉴얼 퀘스트 9번', 1, 1, 720, 0, 76, 339022.0, 11793.0, 159264.0, 1, 11, 712, 1764, 1764, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (719, '粗暴的指挥者', 255, 28, 0, 718, 0, '[바이런] 리뉴얼 퀘스트 8번', 1, 1, 719, 0, 82, 362237.0, 16640.0, 191014.0, 1, 5, 712, 2110, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (718, '兽人占领地', 255, 28, 0, 717, 0, '[바이런] 리뉴얼 퀘스트 7번', 1, 1, 718, 0, 82, 362237.0, 16640.0, 191014.0, 1, 11, 712, 2110, 2110, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (717, '让平静下来的粉末', 255, 28, 0, 716, 0, '[바이런] 리뉴얼 퀘스트 6번', 1, 1, 717, 0, 78, 397644.0, 23606.0, 184861.0, 1, 5, 712, 2109, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (716, '丑陋的精灵', 255, 28, 0, 715, 0, '[바이런] 리뉴얼 퀘스트 5번', 1, 1, 716, 0, 78, 397644.0, 23606.0, 184861.0, 1, 9, 712, 2109, 2109, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (715, '食肉甲虫类', 255, 28, 0, 714, 0, '[바이런] 리뉴얼 퀘스트 4번', 1, 1, 715, 0, 75, 445311.0, 16009.0, 201282.0, 1, 5, 712, 1763, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (714, '贤明的老人', 255, 28, 0, 713, 0, '[바이런] 리뉴얼 퀘스트 3번', 1, 1, 714, 0, 75, 441146.0, 18857.0, 191391.0, 1, 14, 712, 1763, 1763, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (713, '刺杀变异哥布林', 255, 28, 0, 712, 0, '[바이런] 리뉴얼 퀘스트 2번', 1, 1, 713, 0, 79, 427516.0, 21640.0, 174995.0, 1, 9, 712, 1762, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (712, '异常征兆', 255, 28, 0, 0, 0, '[바이런] 리뉴얼 퀘스트 1번', 1, 1, 712, 0, 73, 423514.0, 21911.0, 135993.0, 1, 4, 0, 1762, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (711, '前往拜伦', 255, 11, 0, 710, 0, '[블랙랜드] 리뉴얼 퀘스트 20번', 1, 1, 711, 0, 73, 423514.0, 21911.0, 135993.0, 1, 12, 692, 1762, 1762, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (710, '返回黑暗之地', 255, 11, 0, 709, 0, '[블랙랜드] 리뉴얼 퀘스트 19번', 1, 1, 710, 0, 39, 205930.0, 11535.0, 231052.0, 1, 8, 692, 1755, 1755, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (709, '异性鱼人', 255, 11, 0, 708, 0, '[블랙랜드] 리뉴얼 퀘스트 18번', 1, 1, 709, 0, 47, 193545.0, 11559.0, 301823.0, 1, 4, 692, 2108, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (708, '失落的港口', 255, 11, 0, 707, 0, '[블랙랜드] 리뉴얼 퀘스트 17번', 1, 1, 708, 0, 47, 193545.0, 11559.0, 301823.0, 1, 8, 692, 2108, 2108, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (707, '古代文献', 255, 11, 0, 706, 0, '[블랙랜드] 리뉴얼 퀘스트 16번', 1, 1, 707, 0, 46, 163007.0, 17506.0, 313301.0, 1, 4, 692, 1759, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (706, '墓穴调查员', 255, 11, 0, 705, 0, '[블랙랜드] 리뉴얼 퀘스트 15번', 1, 1, 706, 0, 46, 163007.0, 17506.0, 313301.0, 1, 8, 692, 1759, 1759, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (705, '沼泽污染的缘由', 255, 11, 0, 704, 0, '[블랙랜드] 리뉴얼 퀘스트 14번', 1, 1, 705, 0, 44, 85846.0, 11329.0, 261108.0, 1, 4, 692, 1757, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (704, '污染的沼泽', 255, 11, 0, 703, 0, '[블랙랜드] 리뉴얼 퀘스트 13번', 1, 1, 704, 0, 44, 88862.0, 11431.0, 270346.0, 1, 8, 692, 1757, 1757, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (703, '玷污神殿的生物', 255, 11, 0, 702, 0, '[블랙랜드] 리뉴얼 퀘스트 12번', 1, 1, 703, 0, 43, 121425.0, 16723.0, 189520.0, 1, 4, 692, 1758, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (702, '神殿守卫', 255, 11, 0, 701, 0, '[블랙랜드] 리뉴얼 퀘스트 11번', 1, 1, 702, 0, 41, 160013.0, 16751.0, 195280.0, 1, 8, 692, 1758, 1758, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (701, '极乐鸟的记忆', 255, 11, 0, 700, 0, '[블랙랜드] 리뉴얼 퀘스트 10번', 1, 1, 701, 0, 50, 192836.0, 16526.0, 156537.0, 1, 4, 692, 2107, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (700, '极乐之巢', 255, 11, 0, 699, 0, '[블랙랜드] 리뉴얼 퀘스트 9번', 1, 1, 700, 0, 50, 192235.0, 17297.0, 153121.0, 1, 22, 692, 2107, 2107, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (699, '研究日志里的预言', 255, 11, 0, 698, 0, '[블랙랜드] 리뉴얼 퀘스트 8번', 1, 1, 699, 0, 58, 228146.0, 19789.0, 189716.0, 1, 8, 692, 2106, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (698, '尸体攻击', 255, 11, 0, 697, 0, '[블랙랜드] 리뉴얼 퀘스트 7번', 1, 1, 698, 0, 58, 228146.0, 19789.0, 189716.0, 1, 4, 692, 2106, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (697, '副本调查员', 255, 11, 0, 696, 0, '[블랙랜드] 리뉴얼 퀘스트 6번', 1, 1, 697, 0, 58, 228146.0, 19789.0, 189716.0, 1, 8, 692, 2106, 2106, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (696, '与兽人的势力之争', 255, 11, 0, 695, 0, '[블랙랜드] 리뉴얼 퀘스트 5번', 1, 1, 696, 0, 21, 281512.0, 15152.0, 237431.0, 1, 4, 692, 1756, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (695, '流浪者镇', 255, 11, 0, 694, 0, '[블랙랜드] 리뉴얼 퀘스트 4번', 1, 1, 695, 0, 45, 222837.0, 12742.0, 226269.0, 1, 8, 692, 1756, 1756, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (694, '地精携带的碎片是？', 255, 11, 0, 693, 0, '[블랙랜드] 리뉴얼 퀘스트 3번', 1, 1, 694, 0, 49, 177237.0, 16758.0, 240461.0, 1, 3, 692, 1755, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (693, '伊克伦的邀请', 255, 11, 0, 692, 0, '[블랙랜드] 리뉴얼 퀘스트 2번', 1, 1, 693, 0, 39, 215981.0, 11541.0, 235380.0, 1, 8, 692, 1755, 1755, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (692, '黑暗之地的试验', 255, 11, 0, 0, 0, '[블랙랜드] 리뉴얼 퀘스트 1번', 1, 1, 692, 0, 39, 230145.0, 12941.0, 231304.0, 1, 4, 0, 2095, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (691, '获得狱特斯的魔力石', 255, 40, 0, 0, 1, '헬테스의 불완전한 마력석 수집.', 1, 1, 691, 0, 58, 228146.0, 19789.0, 189716.0, 1, 8, 0, 2086, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (690, '获得赛诺的魔力石', 255, 40, 0, 0, 1, '세오눔의 불완전한 마력석 수집.', 1, 1, 690, 0, 19, 316705.0, 12396.0, 260777.0, 1, 8, 0, 2083, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (689, '隐藏的封印之路(3) - 不死地牢', 255, 40, 0, 688, 1, '혼돈의 파괴자 150마리 사냥.', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 11, 0, 2085, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (688, '隐藏的封印之路(2) - 不死地牢', 255, 40, 0, 661, 0, '마력의 조합제 100개 수집.', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 2, 661, 2085, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (687, '死者的回音(4)', 255, 40, 0, 686, 0, '깨진 수정조각 400개 수집.', 1, 1, 687, 0, 0, 0.0, 0.0, 0.0, 1, 4, 684, 2099, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (686, '死者的回音(3)', 255, 40, 0, 685, 0, '글룸 워리어/리치킹의 하수인 150마리 사냥', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 10, 684, 2099, 2099, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (685, '死者的回音(2)', 255, 40, 0, 684, 0, '칠흑의 지팡이 150개 수집.', 1, 1, 685, 0, 0, 0.0, 0.0, 0.0, 1, 5, 684, 2098, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (684, '死者的回音(1)', 255, 40, 0, 0, 0, '절망의 네크로맨서 150마리 사냥.', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 1, 0, 2098, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (683, '暗中传来的声音(4)', 255, 40, 0, 682, 0, '마모된 돌조각 400개 수집.', 1, 1, 683, 0, 0, 0.0, 0.0, 0.0, 1, 5, 680, 2097, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (682, '暗中传来的声音(3)', 255, 40, 0, 681, 0, '블러드 트레이서/가디언의 사냥개 150마리 사냥.', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 11, 680, 2097, 2097, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (681, '暗中传来的声音(2)', 255, 40, 0, 680, 0, '드루이드의 마법구 150개 수집.', 1, 1, 681, 0, 0, 0.0, 0.0, 0.0, 1, 8, 680, 2096, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (680, '暗中传来的声音(1)', 255, 40, 0, 0, 0, '공포의 나이트메어 150마리 사냥.', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 4, 0, 2096, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (679, '黑暗之地的邀请', 255, 10, 0, 678, 0, '[애쉬번] 리뉴얼 퀘스트 18번', 1, 1, 679, 0, 39, 214538.0, 11689.0, 234717.0, 1, 4, 662, 2095, 2095, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (678, '返回艾斯本', 255, 10, 0, 677, 0, '[애쉬번] 리뉴얼 퀘스트 17번', 1, 1, 678, 0, 9, 357740.0, 15759.0, 302961.0, 1, 7, 662, 1754, 1754, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (677, '遗失的秘密文件', 255, 10, 0, 676, 0, '[애쉬번] 리뉴얼 퀘스트 16번', 1, 1, 677, 0, 20, 304332.0, 13889.0, 311195.0, 1, 4, 662, 2094, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (676, '传达文件', 255, 10, 0, 675, 0, '[애쉬번] 리뉴얼 퀘스트 15번', 1, 1, 676, 0, 20, 304332.0, 13889.0, 311195.0, 1, 7, 662, 2094, 2094, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (675, '粗暴的巨魔', 255, 10, 0, 674, 0, '[애쉬번] 리뉴얼 퀘스트 14번', 1, 1, 675, 0, 19, 297031.0, 13598.0, 262347.0, 1, 3, 662, 1753, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (674, '危机的莱特恩', 255, 10, 0, 673, 0, '[애쉬번] 리뉴얼 퀘스트 13번', 1, 1, 674, 0, 19, 315394.0, 12723.0, 266025.0, 1, 7, 662, 1753, 1753, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (673, '蜘蛛狩猎', 255, 10, 0, 672, 0, '[애쉬번] 리뉴얼 퀘스트 12번', 1, 1, 673, 0, 18, 339693.0, 18307.0, 238978.0, 1, 2, 662, 2093, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (672, '蜘蛛狩猎者麦森', 255, 10, 0, 671, 0, '[애쉬번] 리뉴얼 퀘스트 11번', 1, 1, 672, 0, 18, 343313.0, 18283.0, 240302.0, 1, 10, 662, 2093, 2093, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (671, '讨伐不死地牢', 255, 10, 0, 670, 0, '[애쉬번] 리뉴얼 퀘스트 10번', 1, 1, 671, 0, 16, 419366.0, 18374.0, 276226.0, 1, 5, 662, 1752, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (670, '亡者之地', 255, 10, 0, 669, 0, '[애쉬번] 리뉴얼 퀘스트 9번', 1, 1, 670, 0, 16, 419366.0, 18374.0, 276226.0, 1, 8, 662, 1752, 1752, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (669, '哥布林的侵略', 255, 10, 0, 668, 0, '[애쉬번] 리뉴얼 퀘스트 8번', 1, 1, 669, 0, 11, 396629.0, 15832.0, 293163.0, 1, 3, 662, 2092, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (668, '支援任务', 255, 10, 0, 667, 0, '[애쉬번] 리뉴얼 퀘스트 7번', 1, 1, 668, 0, 11, 393169.0, 15839.0, 295370.0, 1, 4, 662, 2092, 2092, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (667, '受托', 255, 10, 0, 666, 0, '[애쉬번] 리뉴얼 퀘스트 6번', 1, 1, 667, 0, 9, 358171.0, 15610.0, 301206.0, 1, 11, 662, 1751, 1751, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (666, '消除拜德.莱特的小矮人', 255, 10, 0, 665, 0, '[애쉬번] 리뉴얼 퀘스트 5번', 1, 1, 666, 0, 15, 367941.0, 17474.0, 261714.0, 1, 2, 662, 166, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (665, '真正的任务', 255, 10, 0, 664, 0, '[애쉬번] 리뉴얼 퀘스트 4번', 1, 1, 665, 0, 9, 361196.0, 15610.0, 292939.0, 1, 11, 662, 166, 166, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (664, '警卫兵的试验', 255, 10, 0, 663, 0, '[애쉬번] 리뉴얼 퀘스트 3번', 1, 1, 664, 0, 9, 370930.0, 15726.0, 283210.0, 1, 3, 662, 155, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (663, '艾斯本第一个任务', 255, 10, 0, 662, 0, '[애쉬번] 리뉴얼 퀘스트 2번', 1, 1, 663, 0, 9, 365238.0, 14507.0, 304671.0, 1, 40, 662, 155, 155, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (662, '冒险的开始', 255, 10, 0, 0, 0, '[애쉬번] 리뉴얼 퀘스트 1번', 1, 1, 662, 0, 9, 368877.0, 14507.0, 304671.0, 1, 2, 0, 668, 668, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (661, '隐藏的封印之路(1) - 不死地牢', 255, 40, 0, 0, 0, '헬테스의 부서진 마력조각 50개 수집.', 1, 1, 0, 1, 228, 604544.0, 16533.0, 384416.0, 1, 2, 0, 2085, 2085, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (660, '隐藏的封印之路(3) - 黑暗洞窟', 255, 40, 0, 659, 1, '강철의 심판자 150마리 사냥.', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 11, 0, 2082, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (659, '隐藏的封印之路(2) - 黑暗洞窟', 255, 40, 0, 658, 0, '마력의 접착제 100개 수집.', 1, 1, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 658, 2082, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (658, '隐藏的封印之路(1) - 黑暗洞窟', 255, 40, 0, 0, 0, '세오눔의 부서진 마력조각 50개 수집.', 1, 1, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 2082, 2082, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (657, '营地猎人(拜伦)', 255, 55, 0, 0, 1, '타락한 지배자 100마리 사냥(스팟퀘스트)', 1, 5, 657, 0, 0, 0.0, 0.0, 0.0, 1, 2, 0, 2063, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (656, '营地猎人(黑暗大陆)', 255, 55, 0, 0, 1, '혼돈의 파괴자 100마리 사냥(스팟퀘스트)', 1, 5, 656, 0, 0, 0.0, 0.0, 0.0, 1, 2, 0, 2062, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (655, '营地猎人(复仇之城)', 255, 55, 0, 0, 1, '강철의 심판자 100마리 사냥(스팟퀘스트)', 1, 5, 655, 0, 0, 0.0, 0.0, 0.0, 1, 2, 0, 2061, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (654, '访问勒奥斯', 20, 1, 0, 607, 0, '레우스를 찾아가라(아크라)', 1, 1, 654, 0, 145, 796814.0, 15039.0, 661044.0, 1, 11, 652, 1030, 1030, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (653, '访问西尼奥斯', 20, 1, 0, 606, 0, '제네스를 찾아가라(아크라)', 1, 1, 653, 0, 147, 792824.0, 17223.0, 716764.0, 1, 11, 652, 1027, 1027, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (652, '访问伊比尔亚', 20, 1, 0, 605, 0, '이베아를 찾아가라(아크라)', 1, 1, 652, 0, 148, 768753.0, 13711.0, 706534.0, 1, 30, 0, 1044, 1044, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (651, '访问蓝瑟尔', 20, 1, 0, 612, 0, '란셀을 찾아가라(아크라)', 1, 1, 651, 0, 143, 789156.0, 14014.0, 687926.0, 1, 11, 647, 1043, 1043, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (650, '访问克斯', 20, 1, 0, 611, 0, '케이스를 찾아가라(아크라)', 1, 1, 650, 0, 146, 821025.0, 11457.0, 653937.0, 1, 11, 647, 1076, 1076, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (649, '访问甄辉', 20, 1, 0, 610, 0, '진휘를 찾아가라(아크라)', 1, 1, 649, 0, 145, 793480.0, 15324.0, 665099.0, 1, 11, 647, 1042, 1042, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (648, '访问比尔杰斯', 20, 1, 0, 609, 0, '베텔스를 찾아가라(아크라)', 1, 1, 648, 0, 147, 792947.0, 17135.0, 715220.0, 1, 11, 647, 1041, 1041, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (647, '访问雷克斯', 20, 1, 0, 605, 0, '렉스를 찾아가라(아크라)', 1, 1, 647, 0, 142, 765377.0, 16182.0, 716781.0, 1, 24, 0, 1035, 1035, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (646, '访问巴以塔', 20, 1, 0, 604, 0, '베이터를 찾아가라(아크라)', 1, 1, 646, 0, 145, 805525.0, 16329.0, 690887.0, 1, 11, 603, 1046, 1046, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (645, '访问伊诺', 20, 1, 0, 603, 0, '이노를 찾아가라(아크라)', 1, 1, 645, 0, 142, 799354.0, 15176.0, 682676.0, 1, 11, 603, 1045, 1045, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (644, '访问凯尔力', 8, 1, 0, 596, 0, '켈리를 찾아가라(기네아)', 1, 1, 644, 0, 241, 660483.0, 13854.0, 67250.0, 1, 11, 642, 351, 351, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (643, '访问阿玛斯', 8, 1, 0, 595, 0, '아마스를 찾아가라(기네아)', 1, 1, 643, 0, 242, 668419.0, 13773.0, 86605.0, 1, 11, 642, 387, 387, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (642, '访问乌尔莉娅', 8, 1, 0, 636, 0, '올리비아를 찾아가라(기네아)', 1, 1, 642, 0, 238, 677528.0, 14073.0, 91190.0, 1, 30, 0, 345, 345, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (641, '访问罗', 8, 1, 0, 600, 0, '로를 찾아가라(기네아)', 1, 1, 641, 0, 241, 670848.0, 16648.0, 58479.0, 1, 11, 638, 338, 338, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (640, '访问阿尔洛玛', 8, 1, 0, 599, 0, '알로마를 찾아가라(기네아)', 1, 1, 640, 0, 242, 674037.0, 13370.0, 87188.0, 1, 11, 638, 386, 386, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (639, '访问普斯特', 8, 1, 0, 598, 0, '포스트를 찾아가라(기네아)', 1, 1, 639, 0, 238, 668381.0, 11334.0, 100209.0, 1, 10, 638, 372, 372, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (638, '访问博恩森', 8, 1, 0, 636, 0, '폰슨을 찾아가라(기네아)', 1, 1, 638, 0, 238, 686158.0, 12896.0, 97716.0, 1, 24, 0, 340, 340, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (637, '访问玛丽', 8, 1, 0, 634, 0, '마리를 찾아가라(기네아)', 1, 1, 637, 0, 236, 694543.0, 13275.0, 106402.0, 1, 11, 634, 2067, 2067, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (636, '吉内亚特制烤肉3(刺客)', 8, 1, 0, 635, 0, '어쌔신 멧돼지 고기 수집(기네아)', 1, 1, 636, 0, 236, 695745.0, 13109.0, 102090.0, 1, 22, 634, 2065, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (635, '吉内亚特制烤肉2(刺客)', 8, 1, 0, 637, 0, '어쌔신 너구리 털 수집(기네아)', 1, 1, 635, 0, 236, 695745.0, 13109.0, 102090.0, 1, 2, 634, 2067, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (634, '吉内亚特制烤肉1(刺客)', 8, 1, 0, 0, 0, '어쌔신 토끼고기 수집(기네아)', 1, 1, 634, 0, 236, 695745.0, 13109.0, 102090.0, 1, 2, 0, 2065, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (633, '丹尼斯', 2, 1, 0, 589, 0, '베니스터 찾아가기(기네아)', 1, 1, 633, 0, 241, 688973.0, 14053.0, 70515.0, 1, 11, 631, 339, 339, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (632, '访问米尔', 2, 1, 0, 588, 0, '모이어를 찾아가라(기네아)', 1, 1, 632, 0, 243, 703781.0, 13828.0, 71242.0, 1, 11, 631, 374, 374, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (631, '访问莱娅', 2, 1, 0, 625, 0, '레아를 찾아가라(기네아)', 1, 1, 631, 0, 245, 727331.0, 15630.0, 75635.0, 1, 11, 0, 384, 384, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (630, '访问克莱肯', 2, 1, 0, 593, 0, '클레이튼을 찾아가라(기네아)', 1, 1, 630, 0, 241, 688644.0, 14064.0, 71568.0, 1, 11, 627, 353, 353, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (629, '访问莱克', 2, 1, 0, 592, 0, '레이커를 찾아가라(기네아)', 1, 1, 629, 0, 243, 704790.0, 13898.0, 71140.0, 1, 11, 627, 379, 379, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (628, '访问艾凯琳', 2, 1, 0, 591, 0, '아코스타를 찾아가라(기네아)', 1, 1, 628, 0, 239, 734463.0, 11964.0, 96504.0, 1, 10, 627, 383, 383, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (627, '访问艾伦', 2, 1, 0, 625, 0, '오스왈트를 찾아가라(기네아)', 1, 1, 627, 0, 245, 725965.0, 15599.0, 58464.0, 1, 24, 0, 341, 341, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (626, '访问芭比', 2, 1, 0, 623, 0, '바비를 찾아가라(기네아)', 1, 1, 626, 0, 235, 738802.0, 20809.0, 52737.0, 1, 11, 623, 2066, 2066, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (625, '吉内亚特制烤肉3(游侠)', 2, 1, 0, 624, 0, '레인저 멧돼지 고기 수집(기네아)', 1, 1, 625, 0, 246, 729924.0, 17739.0, 40879.0, 1, 22, 623, 2064, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (624, '吉内亚特制烤肉2(游侠)', 2, 1, 0, 626, 0, '레인저 너구리 털 수집(기네아)', 1, 1, 624, 0, 246, 729924.0, 17739.0, 40879.0, 1, 2, 623, 2066, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (623, '吉内亚特制烤肉1(游侠)', 2, 1, 0, 0, 0, '레인저 토끼고기 수집(기네아)', 1, 1, 623, 0, 246, 728691.0, 17746.0, 41334.0, 1, 2, 0, 2064, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (622, '访问哈伍德', 1, 1, 0, 582, 0, '하워드를 찾아가라(기네아)', 1, 1, 622, 0, 7, 663020.0, 12890.0, 39765.0, 1, 11, 617, 349, 349, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (621, '访问沃斯坦', 1, 1, 0, 581, 0, '워스턴을 찾아가라(기네아)', 1, 1, 621, 0, 244, 713426.0, 12924.0, 28006.0, 1, 11, 617, 350, 350, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (620, '访问摩尔', 1, 1, 0, 586, 0, '무어를 찾아가라(기네아)', 1, 1, 620, 0, 241, 670905.0, 16757.0, 57893.0, 1, 11, 616, 381, 381, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (619, '访问佩尼', 1, 1, 0, 585, 0, '페니를 찾아가라(기네아)', 1, 1, 619, 0, 244, 714265.0, 12978.0, 29364.0, 1, 11, 616, 343, 343, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (618, '访问莱姆.金', 1, 1, 0, 584, 0, '램킨을 찾아가라(기네아)', 1, 1, 618, 0, 240, 718700.0, 11808.0, 16172.0, 1, 11, 616, 378, 378, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (617, '访问沃尔特', 1, 1, 0, 580, 0, '월터를 찾아가라(기네아)', 1, 1, 617, 0, 240, 710207.0, 13255.0, 25582.0, 1, 30, 0, 352, 352, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (616, '访问巴登', 1, 1, 0, 580, 0, '바튼을 찾아가라(기네아)', 1, 1, 616, 0, 247, 705983.0, 12607.0, 29081.0, 1, 24, 0, 382, 382, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (615, '访问玛提', 1, 1, 0, 578, 0, '마비를 찾아가라(기네아)', 1, 1, 615, 0, 234, 689260.0, 12672.0, 17365.0, 1, 11, 578, 376, 376, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (614, '遗失的圣诞礼物', 255, 1, 0, 0, 0, '크리스마스 이벤트', 1, 1, 0, 1, 0, 0.0, 0.0, 0.0, 1, 2, 0, 717, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (613, '古代特殊魔法', 20, 1, 0, 651, 0, '엘프 서모너 푸드크리에이션, 고폭 습득(아크라)', 1, 1, 0, 1, 143, 789146.0, 14025.0, 687811.0, 1, 2, 0, 1043, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (612, '开出海路', 20, 1, 0, 650, 0, '엘프 서모너 솔리더 사냥(아크라)', 1, 1, 612, 0, 146, 821001.0, 11457.0, 653955.0, 1, 2, 647, 1076, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (611, '降伏原住民', 20, 1, 0, 649, 0, '엘프 서모너 카니발 마스크 사냥(아크라)', 1, 1, 611, 0, 145, 793442.0, 15336.0, 664933.0, 1, 2, 647, 1042, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (610, '降伏鱼', 20, 1, 0, 648, 0, '엘프 서모너 크랩 피쉬 사냥(아크라)', 1, 1, 610, 0, 147, 792891.0, 17133.0, 715050.0, 1, 2, 647, 1041, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (609, '降伏狐狸', 20, 1, 0, 647, 0, '엘프 서모너 여우 사냥(아크라)', 1, 1, 609, 0, 142, 765410.0, 16196.0, 716941.0, 1, 2, 647, 1035, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (608, '解决问题', 20, 1, 0, 654, 0, '엘프 서모너 수정구 수집(아크라)', 1, 1, 608, 0, 145, 797032.0, 15039.0, 660845.0, 1, 2, 652, 1030, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (607, '结界缝隙调查', 20, 1, 0, 653, 0, '엘프 서모너 혼의 조각 수집(아크라)', 1, 1, 607, 0, 147, 792791.0, 17224.0, 716765.0, 1, 2, 652, 1027, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (606, '土壤调查', 20, 1, 0, 652, 0, '엘프 서모너 천년삼 뿌리 수집(아크라)', 1, 1, 606, 0, 148, 768748.0, 13714.0, 706564.0, 1, 2, 652, 1044, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (605, '阿克拉魔法药剂(3)', 20, 1, 0, 646, 0, '엘프 서모너 플라밍고 부리 수집(아크라)', 1, 1, 605, 0, 145, 805571.0, 16335.0, 691034.0, 1, 2, 603, 1046, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (604, '阿克拉魔法药剂(2)', 20, 1, 0, 645, 0, '엘프 서모너 냄새나는 꽃 수집(아크라)', 1, 1, 604, 0, 142, 799587.0, 15218.0, 682575.0, 1, 2, 603, 1045, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (603, '阿克拉魔法药剂(1)', 20, 1, 0, 0, 0, '엘프 서모너 토끼고기 수집(아크라)', 1, 1, 603, 0, 142, 790846.0, 13975.0, 689236.0, 1, 2, 0, 1039, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (602, '古代精灵的特别魔法', 4, 1, 0, 601, 0, '엘프 푸드크리에이선 습득(기네아)', 1, 1, 602, 0, 241, 670751.0, 16772.0, 58048.0, 1, 20, 0, 338, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (601, '全部根源', 12, 1, 0, 641, 0, '엘프 어쌔신 고대 감시자의 눈 수집(기네아)', 1, 1, 601, 0, 241, 670751.0, 16772.0, 58048.0, 1, 2, 638, 338, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (600, '变残暴的吉内亚双足翼龙', 12, 1, 0, 640, 0, '엘프 어쌔신 기네아 와이번의 발톱 수집(기네아)', 1, 1, 600, 0, 242, 673955.0, 13378.0, 87250.0, 1, 2, 638, 386, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (599, '残暴的蟾蜍', 12, 1, 0, 639, 0, '엘프 어쌔신 뿔거북 두꺼비 독 수집(기네아)', 1, 1, 599, 0, 238, 668195.0, 11326.0, 100264.0, 1, 2, 638, 372, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (598, '兽人迁海的缘由', 12, 1, 0, 638, 0, '엘프 어쌔신 어린 오크 도시락 수집(기네아)', 1, 1, 598, 0, 238, 686018.0, 12883.0, 97672.0, 1, 2, 638, 340, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (597, '胆量', 12, 1, 0, 644, 0, '엘프 어쌔신 기네아 와이번 사냥(기네아)', 1, 1, 597, 0, 241, 660480.0, 13886.0, 67055.0, 1, 2, 642, 351, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (596, '一击', 12, 1, 0, 643, 0, '엘프 어쌔신 박쥐 사냥(기네아)', 1, 1, 596, 0, 242, 668377.0, 13781.0, 86702.0, 1, 2, 642, 387, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (595, '突袭', 12, 1, 0, 642, 0, '엘프 어쌔신 리틀 고블린 사냥(기네아)', 1, 1, 595, 0, 238, 677523.0, 14067.0, 91156.0, 1, 2, 642, 345, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (594, '异常力量的来源', 2, 1, 0, 630, 0, '레인저 고대 감시자의 눈 수집(기네아)', 1, 1, 594, 0, 241, 688454.0, 14111.0, 71541.0, 1, 2, 627, 353, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (593, '守护遗迹的吉内亚双足翼龙', 2, 1, 0, 629, 0, '레인저 기네아 와이번의 발톱 수집(기네아)', 1, 1, 593, 0, 243, 704821.0, 13926.0, 70964.0, 1, 2, 627, 379, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (592, '海岸可疑动物', 2, 1, 0, 628, 0, '레인저 뿔거북 두꺼비 독 수집(기네아)', 1, 1, 592, 0, 239, 734288.0, 12001.0, 96524.0, 1, 2, 627, 383, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (591, '旷野可疑兽人', 2, 1, 0, 627, 0, '레인저 어린 오크 도시락 수집(기네아)', 1, 1, 591, 0, 245, 725803.0, 15624.0, 58508.0, 1, 2, 627, 341, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (590, '狩猎大会(2)', 2, 1, 0, 633, 0, '레인저 기네아 와이번 사냥(기네아)', 1, 1, 590, 0, 241, 688799.0, 14079.0, 70549.0, 1, 2, 631, 339, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (589, '狩猎大会(1)', 2, 1, 0, 632, 0, '레인저 박쥐 사냥(기네아)', 1, 1, 589, 0, 243, 703675.0, 13836.0, 71089.0, 1, 2, 631, 374, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (588, '竞争者', 2, 1, 0, 631, 0, '레인저 리틀 고블린 사냥(기네아)', 1, 1, 588, 0, 245, 727216.0, 15633.0, 75626.0, 1, 2, 631, 384, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (587, '遗迹徘徊者', 1, 1, 0, 620, 0, '나이트 고대 감시자의 눈 수집(기네아)', 1, 1, 587, 0, 241, 670719.0, 16815.0, 57834.0, 1, 2, 616, 381, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (586, '突变的吉内亚双足翼龙', 1, 1, 0, 619, 0, '나이트 기네아 와이번의 발톱 수집(기네아)', 1, 1, 586, 0, 244, 714262.0, 12992.0, 29527.0, 1, 2, 616, 343, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (585, '海岸的异常征兆', 1, 1, 0, 618, 0, '나이트 뿔거북 두꺼비 독 수집(기네아)', 1, 1, 585, 0, 240, 718544.0, 11820.0, 16215.0, 1, 2, 616, 378, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (584, '河岸可疑兽人', 1, 1, 0, 616, 0, '나이트 어린 오크 도시락 수집(기네아)', 1, 1, 584, 0, 247, 705697.0, 12568.0, 29404.0, 1, 2, 616, 382, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (583, '最后的训练', 1, 1, 0, 622, 0, '나이트 기네아 와이번 사냥(기네아)', 1, 1, 583, 0, 241, 663015.0, 12937.0, 39950.0, 1, 2, 617, 349, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (582, '爆发力测试', 1, 1, 0, 621, 0, '나이트 박쥐 사냥(기네아)', 1, 1, 582, 0, 244, 713532.0, 12956.0, 27887.0, 1, 2, 617, 350, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (581, '第一次战斗', 1, 1, 0, 617, 0, '나이트 리틀 고블린 사냥(기네아)', 1, 1, 581, 0, 240, 710143.0, 13254.0, 25549.0, 1, 2, 617, 352, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (580, '吉内亚特制烤肉3(骑士)', 1, 1, 0, 579, 0, '나이트 멧돼지고기 수집(기네아)', 1, 1, 580, 0, 234, 687484.0, 12742.0, 23803.0, 1, 22, 578, 375, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (579, '吉内亚特制烤肉2(骑士)', 1, 1, 0, 615, 0, '나이트 너구리 털 수집(기네아)', 1, 1, 579, 0, 234, 687484.0, 12742.0, 23803.0, 1, 2, 578, 376, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (578, '吉内亚特制烤肉1(骑士)', 1, 1, 0, 0, 0, '나이트 토끼고기 수집(기네아)', 1, 1, 578, 0, 234, 687484.0, 12742.0, 23803.0, 1, 2, 0, 375, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (576, '符文槽初始化', 255, 1, 0, 0, 1, '룬 슬롯 받기', 0, 1, 0, 1, 0, 0.0, 0.0, 0.0, 0, 1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (575, '高官们的命令', 255, 1, 0, 0, 1, '[만월]일퀘 4', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2034, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (574, '现实很残酷', 255, 1, 0, 0, 1, '[만월]일퀘 3', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2033, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (573, '顺利收集资源 II', 255, 1, 0, 0, 1, '[만월]일퀘 2', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2032, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (572, '顺利收集资源 I', 255, 1, 0, 0, 1, '[만월]일퀘 1', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2031, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (571, '关于盈月', 255, 1, 0, 0, 1, '[안식]일퀘 4', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2028, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (570, '无论如何要守住', 255, 1, 0, 0, 1, '[안식]일퀘 3', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2027, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (569, '光荣的死亡', 255, 1, 0, 0, 1, '[안식]일퀘 2', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2026, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (568, '确保退路', 255, 1, 0, 0, 1, '[안식]일퀘 1', 1, 5, 0, 1, 260, 433166.0, 20763.0, 278577.0, 1, 2, 0, 2025, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (567, '肮脏的交易(3)', 255, 60, 0, 566, 0, '하드코어 만월의 유적지6번퀘', 1, 5, 567, 0, 260, 433166.0, 20763.0, 278577.0, 1, 21, 565, 2030, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (566, '肮脏的交易(2)', 255, 60, 0, 565, 0, '하드코어 만월의 유적지5번퀘', 1, 5, 566, 0, 260, 433166.0, 20763.0, 278577.0, 1, 11, 565, 2030, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (565, '肮脏的交易(1)', 255, 60, 0, 0, 0, '하드코어 만월의 유적지4번퀘', 1, 5, 565, 0, 260, 433166.0, 20763.0, 278577.0, 1, 1, 0, 2030, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (564, '为了报仇(3)', 255, 60, 0, 563, 0, '하드코어 만월의 유적지3번퀘', 1, 5, 564, 0, 260, 433166.0, 20763.0, 278577.0, 1, 21, 562, 2024, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (563, '为了报仇(2)', 255, 60, 0, 562, 0, '하드코어 만월의 유적지2번퀘', 1, 5, 563, 0, 260, 433166.0, 20763.0, 278577.0, 1, 11, 562, 2024, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (562, '为了报仇(1)', 255, 60, 0, 0, 0, '하드코어 만월의 유적지1번퀘', 1, 5, 562, 0, 260, 433166.0, 20763.0, 278577.0, 1, 1, 0, 2024, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (561, '请求宽恕(2)', 255, 60, 0, 560, 0, '하드코어 원시해적의동굴5번퀘', 1, 5, 561, 0, 167, 291255.0, 15707.0, 81418.0, 1, 21, 557, 2014, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (560, '请求宽恕(1)', 255, 60, 0, 559, 0, '하드코어 원시해적의동굴4번퀘', 1, 5, 560, 0, 167, 291255.0, 15707.0, 81418.0, 1, 11, 557, 2014, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (559, '为了赎罪(3)', 255, 60, 0, 558, 0, '하드코어 원시해적의동굴3번퀘', 1, 5, 559, 0, 167, 291255.0, 15707.0, 81418.0, 1, 21, 557, 2014, 2014, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (558, '为了赎罪(2)', 255, 60, 0, 557, 0, '하드코어 원시해적의동굴2번퀘', 1, 5, 558, 0, 167, 291255.0, 15707.0, 81418.0, 1, 11, 557, 2013, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (557, '为了赎罪(1)', 255, 60, 0, 0, 0, '하드코어 원시해적의동굴1번퀘', 1, 5, 557, 0, 167, 291255.0, 15707.0, 81418.0, 1, 1, 0, 2013, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (556, '为了找回荣光圣殿(5)', 255, 55, 0, 555, 0, '하드코어 일루미나의 성지5번퀘', 1, 5, 556, 0, 0, 0.0, 0.0, 0.0, 1, 51, 552, 1835, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (555, '为了找回荣光圣殿(4)', 255, 55, 0, 554, 0, '하드코어 일루미나의 성지4번퀘', 1, 5, 555, 1, 231, 579451.0, 16601.0, 386555.0, 1, 41, 552, 1835, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (554, '为了找回荣光圣殿(3)', 255, 55, 0, 553, 0, '하드코어 일루미나의 성지3번퀘', 1, 5, 554, 0, 230, 581574.0, 20569.0, 358454.0, 1, 31, 552, 1835, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (553, '为了找回荣光圣殿(2)', 255, 55, 0, 552, 0, '하드코어 일루미나의 성지2번퀘', 1, 5, 553, 0, 229, 621779.0, 18562.0, 379567.0, 1, 21, 552, 1835, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (552, '为了找回荣光圣殿(1)', 255, 55, 0, 0, 0, '하드코어 일루미나의 성지1번퀘', 1, 5, 552, 0, 228, 605150.0, 16514.0, 385778.0, 1, 10, 0, 1835, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (551, '米泰欧斯与精灵的关系(5)', 255, 50, 0, 550, 0, '하드코어 메테오스의 탑5번퀘', 1, 5, 551, 0, 138, 46921.0, 16827.0, 265927.0, 1, 51, 547, 1719, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (550, '米泰欧斯与精灵的关系(4)', 255, 50, 0, 549, 0, '하드코어 메테오스의 탑4번퀘', 1, 5, 550, 1, 138, 46921.0, 16827.0, 265927.0, 1, 41, 547, 1719, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (549, '米泰欧斯与精灵的关系(3)', 255, 50, 0, 548, 0, '하드코어 메테오스의 탑3번퀘', 1, 5, 549, 0, 138, 46921.0, 16827.0, 265927.0, 1, 31, 547, 1719, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (548, '米泰欧斯与精灵的关系(2)', 255, 50, 0, 547, 0, '하드코어 메테오스의 탑2번퀘', 1, 5, 548, 0, 138, 46921.0, 16827.0, 265927.0, 1, 21, 547, 1719, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (547, '米泰欧斯与精灵的关系(1)', 255, 50, 0, 0, 0, '하드코어 메테오스의 탑1번퀘', 1, 5, 547, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 0, 1719, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (546, '封锁米泰欧斯的气力(7)', 255, 45, 0, 545, 0, '하드코어 메테오스7번퀘', 1, 5, 546, 0, 138, 46921.0, 16827.0, 265927.0, 1, 31, 540, 2012, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (545, '封锁米泰欧斯的气力(6)', 255, 45, 0, 544, 0, '하드코어 메테오스6번퀘', 1, 5, 545, 0, 138, 46921.0, 16827.0, 265927.0, 1, 21, 540, 2012, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (544, '封锁米泰欧斯的气力(5)', 255, 45, 0, 543, 0, '하드코어 메테오스5번퀘', 1, 5, 544, 0, 138, 46921.0, 16827.0, 265927.0, 1, 11, 540, 2012, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (543, '封锁米泰欧斯的气力(4)', 255, 45, 0, 542, 0, '하드코어 메테오스4번퀘', 1, 5, 543, 1, 138, 46921.0, 16827.0, 265927.0, 1, 31, 540, 2012, 2012, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (542, '封锁米泰欧斯的气力(3)', 255, 45, 0, 541, 0, '하드코어 메테오스3번퀘', 1, 5, 542, 0, 138, 46921.0, 16827.0, 265927.0, 1, 21, 540, 425, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (541, '封锁米泰欧斯的气力(2)', 255, 45, 0, 540, 0, '하드코어 메테오스2번퀘', 1, 5, 541, 0, 138, 46921.0, 16827.0, 265927.0, 1, 11, 540, 425, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (540, '封锁米泰欧斯的气力(1)', 255, 45, 0, 0, 0, '하드코어 메테오스1번퀘', 1, 5, 540, 0, 138, 46921.0, 16827.0, 265927.0, 1, 1, 0, 425, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (536, '前往巴普麦特巢穴', 1, 1, 0, 535, 1, '바포메트 둥지 입장 퀘 마지막', 1, 5, 0, 1, 0, 0.0, 0.0, 0.0, 1, 0, 0, 1893, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (535, '为了圣殿的未来 III', 1, 1, 0, 534, 1, '바포메트 둥지 입장 퀘 3', 1, 5, 0, 1, 0, 0.0, 0.0, 0.0, 1, 1, 0, 1892, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (534, '为了圣殿的未来 II', 1, 1, 0, 533, 1, '바포메트 둥지 입장 퀘 2', 1, 5, 0, 1, 0, 0.0, 0.0, 0.0, 1, 1, 0, 1891, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (533, '为了圣殿的未来', 1, 1, 0, 0, 1, '바포메트 둥지 입장 퀘 1', 1, 5, 0, 1, 0, 0.0, 0.0, 0.0, 1, 7, 0, 1818, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (532, '古代祭坛净化 III', 255, 45, 0, 531, 0, '하드코어 퀘스트 라이논 (던전IV) 6', 1, 5, 532, 0, 112, 82905.0, 44213.0, 83377.0, 1, 31, 527, 766, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (531, '古代祭坛净化 II', 255, 45, 0, 530, 0, '하드코어 퀘스트 라이논 (던전IV) 5', 1, 5, 531, 1, 112, 82905.0, 44213.0, 83377.0, 1, 21, 527, 766, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (530, '古代祭坛净化 I', 255, 45, 0, 529, 0, '하드코어 퀘스트 라이논 (던전IV) 4', 1, 5, 530, 0, 112, 82905.0, 44213.0, 83377.0, 1, 11, 527, 766, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (529, '受信任的冒险家', 255, 45, 0, 528, 0, '하드코어 퀘스트 라이논 (던전IV) 3', 1, 5, 529, 0, 112, 82905.0, 44213.0, 83377.0, 1, 31, 527, 766, 766, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (528, '无名洞窟内部净化', 255, 45, 0, 527, 0, '하드코어 퀘스트 라이논 (던전IV) 2', 1, 5, 528, 1, 115, 180001.0, 25926.0, 63013.0, 1, 21, 527, 948, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (527, '无名洞窟外部净化', 255, 45, 0, 0, 0, '하드코어 퀘스트 라이논 (던전IV) 1', 1, 5, 527, 0, 115, 180001.0, 25926.0, 63013.0, 1, 11, 0, 948, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (526, '不要问理由 II', 255, 45, 0, 525, 0, '하드코어 퀘스트 베테르 (던전 III) 5', 1, 5, 526, 0, 75, 436945.0, 16299.0, 211363.0, 1, 6, 522, 625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (525, '不要问理由 I', 255, 45, 0, 524, 0, '하드코어 퀘스트 베테르 (던전 III) 4', 1, 5, 525, 0, 75, 436945.0, 16299.0, 211363.0, 1, 1, 522, 625, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (524, '很受欢迎的冒险家', 255, 45, 0, 523, 0, '하드코어 퀘스트 베테르 (던전 III) 3', 1, 5, 524, 0, 75, 436945.0, 16299.0, 211363.0, 1, 9, 522, 625, 625, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (523, '突破入口 II', 255, 45, 0, 522, 0, '하드코어 퀘스트 샤샤 (던전 III) 2', 1, 5, 523, 1, 76, 348582.0, 11214.0, 153010.0, 1, 6, 522, 624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (522, '突破入口 I', 255, 45, 0, 0, 0, '하드코어 퀘스트 샤샤 (던전 III) 1', 1, 5, 522, 0, 76, 348582.0, 11214.0, 153010.0, 1, 1, 0, 624, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (521, '卖药水 II', 255, 45, 0, 520, 0, '하드코어 퀘스트  카브카브 (던전II) 8', 1, 5, 521, 0, 46, 160510.0, 17506.0, 325407.0, 1, 21, 514, 623, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (520, '卖药水 I', 255, 45, 0, 519, 0, '하드코어 퀘스트  카브카브 (던전II) 7', 1, 5, 520, 0, 46, 160510.0, 17506.0, 325407.0, 1, 11, 514, 623, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (519, '寻找冒险家 II', 255, 45, 0, 518, 0, '하드코어 퀘스트  모튼 (던전II) 6', 1, 5, 519, 0, 46, 160510.0, 17506.0, 325407.0, 1, 21, 514, 623, 623, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (518, '火焰II', 255, 45, 0, 517, 0, '하드코어 퀘스트  모튼 (던전II) 5', 1, 5, 518, 1, 17, 464098.0, 21358.0, 306728.0, 1, 11, 514, 275, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (517, '火焰I', 255, 45, 0, 516, 0, '하드코어 퀘스트  모튼 (던전II) 4', 1, 5, 517, 0, 17, 464098.0, 21358.0, 306728.0, 1, 2, 514, 275, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (516, '寻找冒险家', 255, 45, 0, 515, 0, '하드코어 퀘스트  시어너 (던전II) 3', 1, 5, 516, 0, 9, 369650.0, 15858.0, 288830.0, 1, 21, 514, 275, 275, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (515, '卖药剂 II', 255, 45, 0, 514, 0, '하드코어 퀘스트  시어너 (던전II) 2', 1, 5, 515, 0, 58, 220679.0, 20245.0, 192661.0, 1, 11, 514, 707, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (514, '卖药剂 I', 255, 45, 0, 0, 0, '하드코어 퀘스트  시어너 (던전II) 1', 1, 5, 514, 0, 58, 220679.0, 20245.0, 192661.0, 1, 3, 0, 707, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (513, '气势汹汹', 255, 45, 0, 512, 0, '하드코어 퀘스트 젠디 (던전I) 5', 1, 5, 513, 0, 44, 97351.0, 11246.0, 263949.0, 1, 21, 509, 626, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (512, '丢失的设计图', 255, 45, 0, 511, 0, '하드코어 퀘스트 젠디 (던전I) 4', 1, 5, 512, 0, 44, 97351.0, 11246.0, 263949.0, 1, 11, 509, 626, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (511, '新的任务', 255, 45, 0, 510, 0, '하드코어 퀘스트 시퍼플 (던전I) 3', 1, 5, 511, 0, 44, 97351.0, 11246.0, 263949.0, 1, 21, 509, 626, 626, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (510, '罪恶之源', 255, 45, 0, 509, 0, '하드코어 퀘스트 시퍼플 (던전I) 2', 1, 5, 510, 1, 19, 316286.0, 12349.0, 261447.0, 1, 11, 509, 708, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (509, '过于黑暗', 255, 45, 0, 0, 0, '하드코어 퀘스트 시퍼플 (던전I) 1', 1, 5, 509, 0, 19, 316286.0, 12349.0, 261447.0, 1, 1, 0, 708, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (508, '利用特产成为富翁 IV', 255, 30, 0, 507, 0, '하드코어 퀘스트 루우 (로덴) 8', 1, 4, 508, 0, 119, 85757.0, 20778.0, 215197.0, 1, 41, 501, 751, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (507, '利用特产成为富翁 III', 255, 30, 0, 506, 0, '하드코어 퀘스트 루우 (로덴) 7', 1, 4, 507, 0, 118, 58714.0, 18938.0, 164232.0, 1, 31, 501, 751, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (506, '利用特产成为富翁 II', 255, 30, 0, 505, 0, '하드코어 퀘스트 루우 (로덴) 6', 1, 4, 506, 0, 111, 97912.0, 37963.0, 87468.0, 1, 21, 501, 751, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (505, '利用特产成为富翁 I', 255, 30, 0, 504, 0, '하드코어 퀘스트 루우 (로덴) 5', 1, 4, 505, 0, 113, 191126.0, 26900.0, 58033.0, 1, 11, 501, 751, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (504, '访问卡恩', 255, 30, 0, 503, 0, '하드코어 퀘스트 루우 (로덴) 4', 1, 4, 504, 0, 94, 189770.0, 23780.0, 98685.0, 1, 41, 501, 751, 751, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (503, '罗欧的喜好', 255, 30, 0, 502, 0, '하드코어 퀘스트 루우 (로덴) 3', 1, 4, 503, 0, 118, 58714.0, 18938.0, 164232.0, 1, 31, 501, 750, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (502, '送货途中 II', 255, 30, 0, 501, 0, '하드코어 퀘스트 루우 (로덴) 2', 1, 4, 502, 0, 111, 97912.0, 37963.0, 87468.0, 1, 21, 501, 750, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (501, '送货途中 I', 255, 30, 0, 0, 0, '하드코어 퀘스트 루우 (로덴) 1', 1, 4, 501, 0, 113, 191126.0, 26900.0, 58033.0, 1, 11, 0, 750, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (500, '詹姆普的委托 IV', 255, 35, 0, 499, 0, '하드코어 퀘스트 경비병 잠보 (바이런) 7', 1, 4, 500, 0, 0, 0.0, 0.0, 0.0, 1, 14, 494, 675, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (499, '詹姆普的委托 III', 255, 35, 0, 498, 0, '하드코어 퀘스트 경비병 잠보 (바이런) 6', 1, 4, 499, 0, 77, 237247.0, 11333.0, 117316.0, 1, 10, 494, 675, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (498, '詹姆普的委托 II', 255, 35, 0, 497, 0, '하드코어 퀘스트 경비병 잠보 (바이런) 5', 1, 4, 498, 0, 81, 331171.0, 23155.0, 88267.0, 1, 6, 494, 675, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (497, '詹姆普的委托 I', 255, 35, 0, 496, 0, '하드코어 퀘스트 경비병 잠보 (바이런) 4', 1, 4, 497, 0, 79, 470855.0, 22738.0, 132524.0, 1, 1, 494, 675, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (496, '又一个委托', 255, 35, 0, 495, 0, '하드코어 퀘스트 경비병 루커 (바이런) 3', 1, 4, 496, 0, 73, 424431.0, 23123.0, 131187.0, 1, 9, 494, 675, 675, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (495, '畏怯的警卫兵 II', 255, 35, 0, 494, 0, '하드코어 퀘스트 경비병 루커 (바이런) 2', 1, 4, 495, 0, 82, 353270.0, 15756.0, 195039.0, 1, 6, 494, 678, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (494, '畏怯的警卫兵 I', 255, 35, 0, 0, 0, '하드코어 퀘스트 경비병 루커 (바이런) 1', 1, 4, 494, 0, 78, 391922.0, 25015.0, 195407.0, 1, 1, 0, 678, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (493, '齐尔姆的委托', 255, 30, 0, 492, 0, '하드코어 퀘스트 길모어 (블랙랜드) 7', 1, 4, 493, 0, 0, 0.0, 0.0, 0.0, 1, 11, 487, 294, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (492, '塞娜的委托 III', 255, 30, 0, 491, 0, '하드코어 퀘스트 세나 (블랙랜드) 6', 1, 4, 492, 0, 39, 203507.0, 12628.0, 218605.0, 1, 31, 487, 294, 294, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (491, '塞娜的委托 II', 255, 30, 0, 490, 0, '하드코어 퀘스트 세나 (블랙랜드) 5', 1, 4, 491, 1, 44, 97492.0, 11246.0, 263916.0, 1, 21, 487, 297, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (490, '塞娜的委托 I', 255, 30, 0, 489, 0, '하드코어 퀘스트 세나 (블랙랜드) 4', 1, 4, 490, 0, 50, 199090.0, 16747.0, 154929.0, 1, 11, 487, 297, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (489, '寻找委托人', 255, 30, 0, 488, 0, '하드코어 퀘스트 엘렌 (블랙랜드) 3', 1, 4, 489, 0, 39, 203507.0, 12628.0, 218605.0, 1, 31, 487, 297, 297, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (488, '做礼物', 255, 30, 0, 487, 0, '하드코어 퀘스트 엘렌 (블랙랜드) 2', 1, 4, 488, 0, 44, 97351.0, 11246.0, 263949.0, 1, 21, 487, 288, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (487, '想去郊游', 255, 30, 0, 0, 0, '하드코어 퀘스트 엘렌 (블랙랜드) 1', 1, 4, 487, 0, 49, 176627.0, 16710.0, 240165.0, 1, 11, 0, 288, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (486, '阿勒图的委托 III', 255, 20, 0, 485, 0, '하드코어 퀘스트 아르투 (푸리에) 6', 1, 4, 486, 0, 0, 0.0, 0.0, 0.0, 1, 31, 481, 279, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (485, '阿勒图的委托 II', 255, 20, 0, 484, 0, '하드코어 퀘스트 아르투 (푸리에) 5', 1, 4, 485, 1, 21, 286660.0, 15192.0, 239653.0, 1, 21, 481, 279, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (484, '阿勒图的委托 I', 255, 20, 0, 483, 0, '하드코어 퀘스트 아르투 (푸리에) 4', 1, 4, 484, 0, 18, 336366.0, 18305.0, 239915.0, 1, 10, 481, 279, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (483, '访问阿勒图', 255, 20, 0, 482, 0, '하드코어 퀘스트 크레아 (푸리에) 3', 1, 4, 483, 0, 9, 358652.0, 15582.0, 285808.0, 1, 21, 481, 279, 279, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (482, '骷髅残骸收集', 255, 20, 0, 481, 0, '하드코어 퀘스트 크레아 (푸리에) 2', 1, 4, 482, 0, 16, 419411.0, 17999.0, 269871.0, 1, 11, 481, 264, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (481, '豺狼人的奇袭', 255, 20, 0, 0, 0, '하드코어 퀘스트 크레아 (푸리에) 1', 1, 4, 481, 0, 14, 390523.0, 16756.0, 279675.0, 1, 4, 0, 264, 0, 1);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (480, '怪物研究(IV)', 255, 50, 0, 479, 0, '몬스터 연구(IV)', 1, 5, 480, 0, 138, 46921.0, 16827.0, 265927.0, 1, 31, 477, 1815, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (479, '怪物研究(III)', 255, 50, 0, 478, 0, '몬스터 연구(III)', 1, 5, 479, 0, 138, 46921.0, 16827.0, 265927.0, 1, 21, 477, 1815, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (478, '怪物研究(II)', 255, 50, 0, 477, 0, '몬스터 연구(II)', 1, 5, 478, 0, 138, 46921.0, 16827.0, 265927.0, 1, 11, 477, 1815, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (477, '怪物研究(I)', 255, 50, 0, 0, 0, '몬스터 연구(I)', 1, 5, 477, 0, 138, 46921.0, 16827.0, 265927.0, 1, 1, 0, 1815, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (476, '丢失的头盔', 255, 45, -59, 475, 0, '잃어버린 투구', 1, 4, 476, 0, 165, 433039.0, 22181.0, 247528.0, 1, 31, 474, 1814, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (475, '丢失的腰带', 255, 45, -59, 474, 0, '잃어버린 벨트', 1, 4, 475, 0, 165, 433039.0, 22181.0, 247528.0, 1, 11, 474, 1814, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (474, '丢失的戒指', 255, 45, -59, 0, 0, '잃어버린 반지', 1, 4, 474, 0, 165, 433039.0, 22181.0, 247528.0, 1, 1, 0, 1814, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (473, '独眼兽的实际身份', 255, 30, 0, 472, 0, '사이클롭스의 정체', 1, 3, 473, 0, 112, 82905.0, 44213.0, 83377.0, 1, 31, 471, 1813, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (472, '牵线木偶的实际身份', 255, 30, 0, 471, 0, '마리오네트의 정체', 1, 3, 472, 0, 112, 82905.0, 44213.0, 83377.0, 1, 11, 471, 1813, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (471, '匹埃罗的实际身份', 255, 30, 0, 0, 0, '피에로의 정체', 1, 3, 471, 0, 112, 82905.0, 44213.0, 83377.0, 1, 1, 0, 1813, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (470, '消灭黄泉军团', 255, 1, 0, 0, 1, '[일루미나의 성지] 황천의 동굴 퀘스트', 1, 5, 0, 1, 231, 617607.0, 17662.0, 390149.0, 1, 1, 0, 1866, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (469, '消灭变种动物', 255, 1, 0, 0, 1, '[일루미나의 성지] 변종 동물 암석 지대 퀘스트', 1, 5, 0, 1, 230, 581574.0, 20569.0, 358454.0, 1, 1, 0, 1821, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (468, '消灭龙族德拉古兰', 255, 1, 0, 0, 1, '[일루미나의 성지] 드라그람 둥지 퀘스트', 1, 5, 0, 1, 229, 617722.0, 19111.0, 373184.0, 1, 1, 0, 1820, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (467, '抓住背叛的精灵', 255, 1, 0, 0, 1, '[일루미나의 성지] 변질된 늪 퀘스트', 1, 5, 0, 1, 228, 604544.0, 16533.0, 384416.0, 1, 1, 0, 1819, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (453, '10点的邀请 (II)
', 255, 1, 0, 0, 1, '', 0, 1, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0, 2805, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (452, '10点的邀请 (I)
', 255, 1, 0, 0, 1, '', 0, 1, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0, 2805, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (451, '回收垃圾[II]', 255, 60, 0, 450, 0, '쓰레기 수거[II]', 1, 6, 451, 0, 167, 291255.0, 15707.0, 81418.0, 1, 10, 450, 1797, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (450, '回收垃圾[I]', 255, 60, 0, 0, 0, '쓰레기 수거[I]', 1, 6, 450, 0, 167, 291255.0, 15707.0, 81418.0, 1, 10, 0, 1796, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (449, '确认新人', 255, 40, 0, 448, 0, '신원 확인', 1, 5, 449, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 447, 1795, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (448, '获得侦察兵的日志', 255, 40, 0, 447, 0, '정찰병의 일지 확보', 1, 5, 448, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 447, 1794, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (447, '获得侦察兵勋章', 255, 40, 0, 0, 0, '정찰병의 증표 확보', 1, 5, 447, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 0, 1793, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (446, '收集云游商人漏税调查书及证物', 255, 40, 0, 445, 0, '떠돌이 상인 탈세 조사서 및 증거물 수집 ', 1, 5, 446, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 444, 1792, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (445, '收集卢比伽角质', 255, 40, 0, 444, 0, '루비그의 피부샘플 수집', 1, 5, 445, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 444, 1791, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (444, '收集监视者的日志碎片', 255, 40, 0, 0, 0, '감시자의 일지 조각 수집', 1, 5, 444, 0, 138, 46921.0, 16827.0, 265927.0, 1, 10, 0, 1790, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (443, '回收阿克拉亡灵的灵魂和魂之火焰', 255, 40, -59, 442, 0, '아크라 망령 영혼과 혼 불 회수 ', 1, 4, 443, 0, 165, 433039.0, 22181.0, 247528.0, 1, 10, 440, 1789, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (442, '收集浩努克的触须和根', 255, 40, -59, 441, 0, '호노크 촉수와 뿌리 수집', 1, 4, 442, 0, 165, 433039.0, 22181.0, 247528.0, 1, 10, 440, 1788, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (441, '收集查克拉石头碎块和魔力结晶体', 255, 40, -59, 440, 0, '차크라 스톤 조각과 마력 결정체 수집', 1, 4, 441, 0, 165, 433039.0, 22181.0, 247528.0, 1, 10, 440, 1787, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (440, '收集长螯蟹的钳子和背壳', 255, 40, -59, 0, 0, '소라게 집게발과 등껍질 수집', 1, 4, 440, 0, 165, 433039.0, 22181.0, 247528.0, 1, 10, 0, 1786, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (439, '收集粮食', 255, 40, 0, 438, 0, '식량 수집', 1, 4, 439, 0, 112, 82905.0, 44213.0, 83377.0, 1, 10, 438, 1785, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (438, '收集药水材料', 255, 40, 0, 0, 0, '물약 재료 수집', 1, 4, 438, 0, 112, 82905.0, 44213.0, 83377.0, 1, 10, 0, 1784, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (437, '收集解封破灭之剑封印的材料', 255, 40, 0, 436, 0, '파멸의 검 봉인 해제 재료 수집', 1, 4, 437, 0, 112, 82905.0, 44213.0, 83377.0, 1, 10, 436, 1783, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (436, '收集破灭之剑', 255, 40, 0, 0, 0, '파멸의 검 수집 ', 1, 4, 436, 0, 112, 82905.0, 44213.0, 83377.0, 1, 10, 0, 1782, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (433, '强大魔力的被害者', 255, 60, 0, 432, 1, '강한 마력의 피해자(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 11, 0, 1490, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (432, '强大魔力的受惠者', 255, 60, 0, 0, 0, '강한 마력의 수혜자(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 1, 0, 1490, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (431, '瞬间变强的破坏者', 255, 60, 0, 430, 1, '급격히 강해진 파괴자(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 11, 0, 1489, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (430, '瞬间变强的弓箭手', 255, 60, 0, 0, 0, '급격히 강해진 레인저(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 1, 0, 1489, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (428, '肮脏的锁链', 255, 55, 0, 427, 1, '더러운 쉐도우(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 31, 0, 1748, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (427, '世界上最短的腿', 255, 55, 0, 426, 0, '세상에서 가장 짧은 다리(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 21, 425, 1748, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (426, '我觉得最可爱的怪物', 255, 55, 0, 425, 0, '내가 가장 귀여워하는 몬스터(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 11, 425, 1748, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (425, '这个真的是螳螂吗？', 255, 55, 0, 0, 0, '이것이 과연 사마귀인가?(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 1, 0, 1748, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (424, '头和脚只有一个的家伙', 255, 50, 0, 423, 1, '다리와 머리가 하나인 녀석(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 31, 0, 1747, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (423, '可爱的小鳄鱼们', 255, 50, 0, 422, 0, '귀여운 악어 녀석들(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 21, 421, 1747, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (422, '这样的也叫龙？', 255, 50, 0, 421, 0, '이것도 용이라고 불러야하나?(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 11, 421, 1747, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (421, '虫子真讨厌', 255, 50, 0, 0, 0, '벌레들은 정말 싫다(던전별퀘스트)', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 1, 0, 1747, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (420, '洛利肯的复仇 (III)', 255, 63, 0, 419, 1, '라이칸의 복수 (III)(던전별퀘스트)', 1, 6, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 22, 0, 1750, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (419, '洛利肯的复仇 (II)', 255, 63, 0, 418, 0, '라이칸의 복수 (II)(던전별퀘스트)', 1, 6, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 12, 418, 1750, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (418, '洛利肯的复仇 (I)', 255, 63, 0, 0, 0, '라이칸의 복수 (I)(던전별퀘스트)', 1, 6, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 0, 1750, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (417, '连怪物都欺负我 (III)', 255, 62, 0, 416, 1, '괴물조차 날 괴롭힌다. (III)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 43, 0, 1296, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (416, '连怪物都欺负我 (II)', 255, 62, 0, 415, 0, '괴물조차 날 괴롭힌다. (II)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 32, 413, 1296, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (415, '连怪物都欺负我 (I)', 255, 62, 0, 414, 0, '괴물조차 날 괴롭힌다. (I)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 22, 413, 1296, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (414, '连鬼都欺负我 (II)', 255, 62, 0, 413, 0, '귀신조차 날 괴롭힌다 (II)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 12, 413, 1296, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (413, '连鬼都欺负我 (I)', 255, 62, 0, 0, 0, '귀신조차 날 괴롭힌다 (I)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 0, 1296, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (412, '完全是土匪 (III)', 255, 60, 0, 411, 1, '완전 도둑놈들 (III)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 22, 0, 1297, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (411, '完全是土匪 (II)', 255, 60, 0, 410, 0, '완전 도둑놈들 (II)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 12, 410, 1297, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (410, '完全是土匪 (I)', 255, 60, 0, 0, 0, '완전 도둑놈들 (I)(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 0, 1297, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (409, '爽口的冷菜', 255, 60, 0, 408, 1, '시원한 냉채(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 32, 0, 1307, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (408, '最棒的寿司材料', 255, 60, 0, 407, 0, '최고의 횟감(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 22, 406, 1307, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (407, '蜈蚣是良药', 255, 60, 0, 406, 0, '지네가 보약(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 12, 406, 1307, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (406, '料理', 255, 60, 0, 0, 0, '랍스타 요리(던전별퀘스트)', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 0, 1307, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (405, '哭泣的亡灵', 255, 40, -59, 404, 1, '울고있는 망령(던전별퀘스트)', 1, 3, 0, 1, 165, 433039.0, 22181.0, 247528.0, 1, 22, 0, 1749, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (404, '畏缩的亡灵', 255, 40, -59, 403, 0, '주눅든 망령(던전별퀘스트)', 1, 3, 0, 1, 165, 433039.0, 22181.0, 247528.0, 1, 12, 403, 1749, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (403, '看起来很饿的亡灵', 255, 40, -59, 0, 0, '배고파 보이는 망령(던전별퀘스트)', 1, 3, 0, 1, 165, 433039.0, 22181.0, 247528.0, 1, 2, 0, 1749, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (402, '亡灵的起源', 255, 55, -59, 0, 1, '망령의 기원(던전별퀘스트)', 1, 3, 0, 1, 165, 433039.0, 22181.0, 247528.0, 1, 51, 0, 1293, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (401, '浩努克的真面目', 255, 50, -59, 0, 1, '호노크의 정체(던전별퀘스트)', 1, 3, 0, 1, 165, 433039.0, 22181.0, 247528.0, 1, 51, 0, 1292, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (400, '查克拉石头的坚硬的外壳', 255, 45, -59, 0, 1, '차크라스톤의 단단한 껍질(던전별퀘스트)', 1, 3, 0, 1, 165, 433039.0, 22181.0, 247528.0, 1, 51, 0, 1291, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (399, '想飞', 255, 45, 0, 398, 1, '날고 싶다.(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 0, 1746, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (398, '比半兽人更坏的家伙', 255, 45, 0, 394, 0, '가스트보다 더 나쁜녀석(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 2, 0, 1746, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (397, '最坏的独眼兽', 255, 45, 0, 396, 1, '가장 나쁜 사이클롭스(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 0, 1745, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (396, '太想知道的(II)', 255, 45, 0, 392, 0, '너무 궁금한 것 (II)(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 2, 0, 1745, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (395, '那家伙看起来强大的理由', 255, 40, 0, 394, 1, '그놈이 강해보이는 이유(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 0, 1744, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (394, '比匹埃罗还坏的家伙', 255, 40, 0, 390, 0, '피에로보다 더 나쁜녀석(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 2, 0, 1744, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (393, '不良独眼兽', 255, 40, 0, 392, 1, '불량 사이클롭스(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 0, 1743, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (392, '太想知道的(I)', 255, 40, 0, 386, 0, '너무 궁금한 것 (I)(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 2, 0, 1743, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (391, '莱本的脊椎是良药', 255, 35, 0, 390, 1, '레이번의 척추가 보약!(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 22, 0, 1742, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (390, '毛头小孩(II)', 255, 35, 0, 389, 0, '싹수가 노란 녀석 (II)(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 389, 1742, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (389, '毛头小孩(I)', 255, 35, 0, 386, 0, '싹수가 노란녀석 (I)(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 2, 0, 1742, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (388, '坏蛋得受到惩罚', 255, 30, 0, 387, 1, '나쁜녀석은 혼나야 해(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 22, 0, 1741, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (387, '坏蛋匹埃罗', 255, 30, 0, 386, 0, '악당 피에로(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 12, 386, 1741, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (386, '丢失的木偶', 255, 30, 0, 0, 0, '잃어버린 목각인형(던전별퀘스트)', 1, 3, 0, 1, 112, 82905.0, 44213.0, 83377.0, 1, 2, 0, 1741, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (382, '发放幸运精华', 255, 98, -99, 0, 1, 'PC방 혜택 행운의 매터리얼 지급 일일 퀘스트', 1, 1, 382, 0, 0, 0.0, 0.0, 0.0, 1, 18, 0, 680, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (338, '迪诺比克的极乐鸟的羽毛', 255, 15, 0, 0, 1, '디노비코의 하피의 깃털', 1, 2, 49, 1, 50, 197541.0, 16639.0, 149231.0, 1, 11, 0, 1723, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (337, '迪诺比克的蜘蛛网', 255, 15, 0, 0, 1, '디노비코의 스컬스파이더', 1, 2, 44, 0, 39, 205269.0, 12543.0, 212411.0, 1, 1, 0, 1723, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (336, '伊罗尼的嗜血骷髅', 255, 15, 0, 0, 1, '이로닌의 블러드 스켈레톤', 1, 2, 49, 1, 58, 220417.0, 20264.0, 193260.0, 1, 11, 0, 1722, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (335, '伊罗尼的骸骨弓箭手', 255, 15, 0, 0, 1, '이로닌의 스켈레톤 아처', 1, 2, 43, 0, 58, 220417.0, 20264.0, 193260.0, 1, 1, 0, 1722, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (334, '马尔德的死亡克鲁巫师', 255, 15, 0, 0, 1, '마일던의 데스크로우 샤먼', 1, 2, 42, 0, 11, 374962.0, 15071.0, 233083.0, 1, 11, 0, 1721, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (333, '马尔德的死亡克鲁', 255, 15, 0, 0, 1, '마일던의 데스크로우', 1, 2, 49, 1, 11, 374962.0, 15071.0, 233083.0, 1, 1, 0, 1721, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (332, '卡可图的小矮人的警告', 255, 15, 0, 0, 1, '[초보자 일퀘] 카크투의 그렘린에 경고', 1, 2, 41, 0, 15, 367578.0, 17249.0, 258595.0, 1, 11, 0, 1720, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (331, '卡可图的豺狼人袭击', 255, 15, 0, 0, 1, '[초보자 일퀘] 카크투의 놀 습격', 1, 2, 40, 0, 14, 389445.0, 19364.0, 270923.0, 1, 1, 0, 1720, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (325, '黑龙沼泽之精华', 255, 1, 0, 0, 1, '[자경단] 흑룡의늪 퀘스트', 1, 1, 0, 1, 44, 98735.0, 11498.0, 263789.0, 1, 3, 0, 1698, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (324, '遗弃的村镇之精华', 255, 1, 0, 0, 1, '[자경단] 버려진마을 퀘스트', 1, 1, 0, 1, 126, 221883.0, 22925.0, 76527.0, 1, 3, 0, 1707, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (323, '深红峡谷之精华', 255, 1, 0, 0, 1, '[자경단] 붉은계곡 퀘스트', 1, 1, 0, 1, 114, 173378.0, 20736.0, 86914.0, 1, 3, 0, 1708, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (322, '石锤镇之精华', 255, 1, 0, 0, 1, '[자경단] 스톤해머 퀘스트', 1, 1, 0, 1, 79, 470855.0, 22738.0, 132524.0, 1, 3, 0, 1704, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (321, '扎坦盆地之精华', 255, 1, 0, 0, 1, '[자경단] 자라탄 퀘스트', 1, 1, 0, 1, 116, 134879.0, 20459.0, 146006.0, 1, 3, 0, 1709, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (320, '牛角迷宫之精华', 255, 1, 0, 0, 1, '[자경단] 마녹의미궁 퀘스트', 1, 1, 0, 1, 119, 74822.0, 20710.0, 216122.0, 1, 3, 0, 1699, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (319, '极乐之巢之精华', 255, 1, 0, 0, 1, '[자경단] 하피의둥지 퀘스트', 1, 1, 0, 1, 50, 197541.0, 16639.0, 149231.0, 1, 3, 0, 1702, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (318, '秩序神殿之精华', 255, 1, 0, 0, 1, '[자경단] 질서의신전 퀘스트', 1, 1, 0, 1, 43, 115463.0, 17184.0, 182043.0, 1, 3, 0, 1700, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (316, '古城那勒图之精华', 255, 1, 0, 0, 1, '[자경단] 나르투 퀘스트', 1, 1, 0, 1, 77, 237247.0, 11333.0, 117316.0, 1, 3, 0, 1706, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (315, '精灵神殿之精华', 255, 1, 0, 0, 1, '[자경단] 엘프의신전 퀘스트', 1, 1, 0, 1, 41, 176292.0, 17391.0, 183368.0, 1, 3, 0, 1701, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (314, '哈斯特神殿之精华', 255, 1, 0, 0, 1, '[자경단] 하스트의제단 퀘스트', 1, 1, 0, 1, 81, 327316.0, 23178.0, 86913.0, 1, 3, 0, 1705, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (312, '妖精岭之精华', 255, 1, 0, 0, 1, '[자경단]요정의언덕 퀘스트', 1, 1, 0, 1, 78, 391922.0, 25015.0, 195407.0, 1, 3, 0, 1703, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (311, '半人鱼栖息地之精华', 255, 1, 0, 0, 1, '[자경단] 머맨의 서식지 퀘스트', 1, 1, 0, 1, 20, 300216.0, 12078.0, 333667.0, 1, 3, 0, 1696, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (304, '太阳之精华', 255, 1, 0, 0, 1, '[카오스 보상 던전] 태양의 층 퀘스트', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1713, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (303, '大海之精华', 255, 1, 0, 0, 1, '[카오스 보상던전] 대해의 층 퀘스트', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1712, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (302, '大气之精华', 255, 1, 0, 0, 1, '[카오스 보상던전] 대기의 층 퀘스트', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1711, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (301, '大地之精华', 255, 1, 0, 0, 1, '[카오스 보상던전] 대지의 층 퀘스트', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1710, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (251, '古代龙的残影 (2)', 255, 1, 0, 211, 0, '[블랙퀘]흑룡의늪2층3번퀘', 1, 2, 0, 1, 44, 98735.0, 11498.0, 263789.0, 1, 22, 250, 1462, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (250, '古代龙的残影 (1)', 255, 1, 0, 0, 0, '[블랙퀘]흑룡의늪2층1번퀘', 1, 2, 0, 1, 44, 98735.0, 11498.0, 263789.0, 1, 2, 0, 1462, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (249, '[可重复任务] 兽人营地的秘密', 255, 1, 0, 0, 1, '[애쉬번퀘]오캠 반복퀘', 1, 1, 0, 1, 21, 281919.0, 15141.0, 237451.0, 1, 3, 0, 1456, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (248, '米泰欧斯的巢穴(7) - 防御营地的真相', 255, 1, 0, 247, 0, '[메테퀘]엘프쪽 메테 4번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 31, 245, 1485, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (247, '米泰欧斯的巢穴(6) - 防御营地的真相', 255, 1, 0, 246, 0, '[메테퀘]엘프쪽 메테 3번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 22, 245, 1485, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (246, '米泰欧斯的巢穴(5) - 防御营地的真相', 255, 1, 0, 245, 0, '[메테퀘]엘프쪽 메테 2번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 12, 245, 1485, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (245, '米泰欧斯的巢穴(4) - 防御营地的真相', 255, 1, 0, 0, 0, '[메테퀘]엘프쪽 메테 1번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1485, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (244, '[可重复任务] 双刃剑！ 魔力之树', 255, 1, 0, 0, 1, '[메테퀘]메테오스반복퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1487, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (243, '米泰欧斯的巢穴(3) - 被发现的的真相', 255, 1, 0, 242, 0, '[메테퀘]유물쪽 메테 3번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 22, 241, 1484, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (242, '米泰欧斯的巢穴(2) - 防御的原理', 255, 1, 0, 241, 0, '[메테퀘]유물쪽 메테 2번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 12, 241, 1484, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (241, '米泰欧斯的巢穴(1) - 巨大的魔力', 255, 1, 0, 0, 0, '[메테퀘]유물쪽 메테 1번퀘', 1, 5, 0, 1, 138, 46921.0, 16827.0, 265927.0, 1, 2, 0, 1484, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (239, '[可重复任务] 米泰欧斯的邪恶', 255, 1, 0, 238, 1, '[로덴퀘]고대 반복퀘', 1, 4, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 2, 0, 1483, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (238, '古代的遗物,雷电时代 (3)', 255, 1, 0, 237, 0, '[로덴퀘]고대의계단2층3번퀘', 1, 4, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 22, 233, 1482, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (237, '古代的遗物,雷电时代 (2)', 255, 1, 0, 236, 0, '[로덴퀘]고대의계단2층2번퀘', 1, 4, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 12, 233, 1482, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (236, '古代的遗物,雷电时代 (1)', 255, 1, 0, 235, 0, '[로덴퀘]고대의계단2층1번퀘', 1, 4, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 2, 233, 1482, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (235, '古代祭坛,太古的秘密 (3)', 255, 1, 0, 234, 0, '[로덴퀘]고대의계단1층3번퀘', 1, 3, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 22, 233, 1481, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (234, '古代祭坛,太古的秘密 (2)', 255, 1, 0, 233, 0, '[로덴퀘]고대의계단1층2번퀘', 1, 3, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 12, 233, 1481, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (233, '古代祭坛,太古的秘密 (1)', 255, 1, 0, 0, 0, '[로덴퀘]고대의계단1층1번퀘', 1, 3, 0, 1, 112, 82887.0, 44186.0, 83251.0, 1, 2, 0, 1481, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (229, '杀戮女王, 阿特拉茜娅 (1)', 255, 1, 0, 0, 0, '[로덴퀘][바이런퀘]연계퀘스트 에르테스1번퀘', 1, 3, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 2, 0, 1479, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (228, '真实就在迷宫里', 255, 1, 0, 227, 0, '[로덴퀘][바이런퀘]연계퀘스트 마녹미궁', 1, 1, 0, 1, 119, 74822.0, 20710.0, 216122.0, 1, 7, 224, 1476, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (227, '巨人的大地,背叛者之土', 255, 1, 0, 226, 0, '[로덴퀘][바이런퀘]연계퀘스트 거인병', 1, 2, 0, 1, 118, 44612.0, 16594.0, 162710.0, 1, 2, 224, 1474, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (226, '没落的那勒图 (3) - 那勒图大屠杀', 255, 1, 0, 225, 0, '[바이런퀘]연계퀘스트 나르투3번퀘', 1, 3, 0, 1, 77, 237247.0, 11333.0, 117316.0, 1, 22, 224, 1471, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (225, '没落的那勒图 (2)', 255, 1, 0, 224, 0, '[바이런퀘]연계퀘스트 나르투2번퀘', 1, 3, 0, 1, 76, 348582.0, 11214.0, 153010.0, 1, 12, 224, 1471, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (224, '没落的那勒图 (1)', 255, 1, 0, 0, 0, '[바이런퀘]연계퀘스트 나르투1번퀘', 1, 1, 0, 1, 77, 237247.0, 11333.0, 117316.0, 1, 2, 0, 1471, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (223, '封印？阿特拉茜娅！', 255, 1, 0, 222, 0, '[로덴퀘][바이런퀘]연계퀘스트 에르테스4번퀘', 1, 3, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 31, 229, 1479, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (222, '杀戮女王, 阿特拉茜娅 (3)', 255, 1, 0, 221, 0, '[로덴퀘][바이런퀘]연계퀘스트 에르테스3번퀘', 1, 3, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 22, 229, 1479, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (221, '杀戮女王, 阿特拉茜娅 (2)', 255, 1, 0, 229, 0, '[로덴퀘][바이런퀘]연계퀘스트 에르테스2번퀘', 1, 3, 0, 1, 115, 180001.0, 25926.0, 63013.0, 1, 12, 229, 1479, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (220, '[可重复任务] 哈斯特的祭品', 255, 1, 0, 0, 1, '[바이런퀘]하스트 반복퀘', 1, 3, 0, 1, 81, 327316.0, 23178.0, 86913.0, 1, 2, 0, 1470, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (219, '复活！哈斯特！？', 255, 1, 0, 0, 0, '[바이런퀘]요정-딱던-스톤 스팟순환퀘', 1, 1, 0, 1, 75, 436945.0, 16299.0, 211363.0, 1, 2, 0, 1468, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (218, '[可重复任务] 召唤的代价', 255, 1, 0, 217, 1, '[블랙퀘]왕무 반복퀘', 1, 4, 0, 1, 46, 162938.0, 17952.0, 319566.0, 1, 2, 0, 1466, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (217, '召唤的代价 (4)', 255, 1, 0, 216, 0, '[블랙퀘]왕의무덤2층2번퀘', 1, 3, 0, 1, 46, 162938.0, 17952.0, 319566.0, 1, 12, 214, 1465, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (216, '召唤的代价 (3)', 255, 1, 0, 215, 0, '[블랙퀘]왕의무덤2층1번퀘', 1, 3, 0, 1, 46, 162938.0, 17952.0, 319566.0, 1, 2, 214, 1465, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (215, '召唤的代价 (2)', 255, 1, 0, 214, 0, '[블랙퀘]왕의무덤1층2번퀘', 1, 2, 0, 1, 46, 162938.0, 17952.0, 319566.0, 1, 12, 214, 1464, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (214, '召唤的代价 (1)', 255, 1, 0, 0, 0, '[블랙퀘]왕의무덤1층1번퀘', 1, 1, 0, 1, 46, 162938.0, 17952.0, 319566.0, 1, 2, 0, 1464, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (213, '[可重复任务] 古代龙的残影 ', 255, 1, 0, 251, 1, '[블랙퀘]흑룡 반복퀘', 1, 4, 0, 1, 44, 98735.0, 11498.0, 263789.0, 1, 2, 0, 1463, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (211, '古代龙的残影 (2)', 255, 1, 0, 250, 0, '[블랙퀘]흑룡의늪2층2번퀘', 1, 2, 0, 1, 44, 98735.0, 11498.0, 263789.0, 1, 12, 250, 1462, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (209, '被熏黑的痕迹 (3)', 255, 1, 0, 208, 0, '[블랙퀘]버려진항구3번퀘', 1, 1, 0, 1, 47, 200066.0, 11663.0, 300320.0, 1, 22, 207, 1461, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (208, '被熏黑的痕迹 (2)', 255, 1, 0, 207, 0, '[블랙퀘]버려진항구2번퀘', 1, 1, 0, 1, 47, 181895.0, 15905.0, 290690.0, 1, 12, 207, 1461, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (207, '被熏黑的痕迹 (1)', 255, 1, 0, 0, 0, '[블랙퀘]버려진항구1번퀘', 1, 2, 0, 1, 47, 200066.0, 11663.0, 300320.0, 1, 1, 0, 1461, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (206, '[可重复任务] 火种回收', 255, 1, 0, 205, 1, '[애쉬번퀘]화탑 반복퀘', 1, 4, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 2, 0, 1459, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (205, '烈焰塔, 图兰卡的秘密 (2)', 255, 1, 0, 204, 0, '[애쉬번퀘]화염의탑3층2번퀘', 1, 3, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 12, 201, 1458, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (204, '烈焰塔, 图兰卡的秘密 (1)', 255, 1, 0, 203, 0, '[애쉬번퀘]화염의탑3층1번퀘', 1, 3, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 2, 201, 1458, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (203, '火焰精灵的异常增加 (3)', 255, 1, 0, 202, 0, '[애쉬번퀘]화염의탑2층3번퀘', 1, 2, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 22, 201, 1457, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (202, '火焰精灵的异常增加 (2)', 255, 1, 0, 201, 0, '[애쉬번퀘]화염의탑2층2번퀘', 1, 2, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 12, 201, 1457, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (201, '火焰精灵的异常增加 (1)', 255, 1, 0, 0, 0, '[애쉬번퀘]화염의탑2층1번퀘', 1, 2, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 2, 0, 1457, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (199, '陌生的不速之客 (3)', 255, 1, 0, 198, 1, '[애쉬번퀘]어두운동굴4층3번퀘', 1, 3, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 22, 0, 1454, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (198, '陌生的不速之客 (2)', 255, 1, 0, 197, 0, '[애쉬번퀘]어두운동굴4층2번퀘', 1, 3, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 12, 191, 1454, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (197, '陌生的不速之客 (1)', 255, 1, 0, 196, 0, '[애쉬번퀘]어두운동굴4층1번퀘', 1, 3, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 2, 191, 1454, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (196, '背叛之洞窟 (3)', 255, 1, 0, 195, 0, '[애쉬번퀘]어두운동굴3층3번퀘', 1, 2, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 22, 191, 1453, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (195, '背叛之洞窟 (2)', 255, 1, 0, 194, 0, '[애쉬번퀘]어두운동굴3층2번퀘', 1, 2, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 12, 191, 1453, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (194, '背叛之洞窟 (1)', 255, 1, 0, 193, 0, '[애쉬번퀘]어두운동굴3층1번퀘', 1, 2, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 2, 191, 1453, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (193, '黑暗洞窟的哥布林(3)', 255, 1, 0, 192, 0, '[애쉬번퀘]어두운동굴2층3번퀘', 1, 1, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 22, 191, 1452, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (192, '黑暗洞窟的哥布林(2)', 255, 1, 0, 191, 0, '[애쉬번퀘]어두운동굴2층2번퀘', 1, 1, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 12, 191, 1452, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (191, '黑暗洞窟的哥布林(1)', 255, 1, 0, 0, 0, '[애쉬번퀘]어두운동굴2층1번퀘', 1, 1, 0, 1, 19, 316705.0, 12396.0, 260777.0, 1, 2, 0, 1452, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (190, '死者站起来了(3)', 255, 1, 0, 189, 0, '[애쉬번퀘]망자의대지3번퀘', 1, 1, 0, 1, 16, 427631.0, 18375.0, 264611.0, 1, 21, 188, 1451, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (189, '死者站起来了(2)', 255, 1, 0, 188, 0, '[애쉬번퀘]망자의대지2번퀘', 1, 1, 0, 1, 16, 427631.0, 18375.0, 264611.0, 1, 11, 188, 1451, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (188, '死者站起来了(1)', 255, 1, 0, 0, 0, '[애쉬번퀘]망자의대지1번퀘', 1, 1, 0, 1, 16, 427631.0, 18375.0, 264611.0, 1, 2, 0, 1451, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (173, '洛利肯的报答', 255, 60, 0, 172, 0, '[원시해적] 라이칸 4번', 1, 5, 29, 0, 167, 291255.0, 15707.0, 81418.0, 1, 4, 170, 1295, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (172, '洛利肯的复仇', 255, 60, 0, 171, 0, '[원시해적] 라이칸 3번', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 3, 170, 1295, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (171, '洛利肯的爱人', 255, 60, 0, 170, 0, '[원시해적] 라이칸 2번', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 2, 170, 1295, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (170, '洛利肯的委托', 255, 60, 0, 0, 0, '[원시해적] 라이칸 1번', 1, 5, 0, 1, 167, 291255.0, 15707.0, 81418.0, 1, 1, 0, 1295, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (169, '石墨岛的守护者', 255, 59, -59, 168, 0, '[발레포르] 레언 2번', 1, 3, 28, 0, 165, 433039.0, 22181.0, 247528.0, 1, 2, 168, 1293, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (168, '召唤师的协调', 255, 58, -59, 167, 0, '[발레포르] 레언 1번', 1, 3, 27, 0, 165, 433039.0, 22181.0, 247528.0, 1, 1, 0, 1293, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (167, '华利弗的勇士', 255, 56, -59, 166, 0, '[발레포르] 아이웬 4번', 1, 3, 26, 0, 165, 433039.0, 22181.0, 247528.0, 1, 4, 164, 1292, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (166, '精灵的力量', 255, 55, -59, 165, 0, '[발레포르] 아이웬 3번', 1, 3, 25, 0, 165, 433039.0, 22181.0, 247528.0, 1, 3, 164, 1292, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (165, '精灵的智慧', 255, 53, -59, 164, 0, '[발레포르] 아이웬 2번', 1, 3, 24, 0, 165, 433039.0, 22181.0, 247528.0, 1, 2, 164, 1292, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (164, '精灵的知识', 255, 52, -59, 163, 0, '[발레포르] 아이웬 1번', 1, 3, 23, 0, 165, 433039.0, 22181.0, 247528.0, 1, 1, 0, 1292, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (163, '石墨岛的勇士', 255, 49, -59, 162, 0, '[발레포르] 카슈 4번', 1, 3, 22, 0, 165, 433039.0, 22181.0, 247528.0, 1, 4, 160, 1291, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (162, '骑士的力量', 255, 46, -59, 161, 0, '[발레포르] 카슈 3번', 1, 3, 21, 0, 165, 433039.0, 22181.0, 247528.0, 1, 3, 160, 1291, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (161, '骑士的韧性', 255, 44, -59, 160, 0, '[발레포르] 카슈 2번', 1, 3, 20, 0, 165, 433039.0, 22181.0, 247528.0, 1, 2, 160, 1291, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (160, '骑士的勇气', 255, 42, -59, 0, 0, '[발레포르] 카슈 1번', 1, 3, 19, 0, 165, 433039.0, 22181.0, 247528.0, 1, 1, 0, 1291, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (158, '最强的技能(II)', 16, 50, 0, 157, 0, '[50레벨 스킬북 퀘스트] 서모너2', 1, 5, 36, 0, 0, 0.0, 0.0, 0.0, 1, 16, 157, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (157, '最强的技能', 16, 50, 0, 0, 0, '[50레벨 스킬북 퀘스트] 서모너1', 1, 5, 35, 0, 0, 0.0, 0.0, 0.0, 1, 15, 0, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (136, '调查结界缝隙(3)', 20, 13, 0, 135, 0, '[아크라 중랩] 결계의 틈 조사(3) (카라얀)', 1, 1, 13, 0, 147, 798994.0, 16831.0, 716557.0, 1, 21, 134, 1036, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (135, '调查结界缝隙(2)', 20, 13, 0, 134, 0, '[아크라 중랩] 결계의 틈 조사(2) (카라얀)', 1, 1, 12, 0, 147, 798994.0, 16831.0, 716557.0, 1, 11, 134, 1036, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (134, '调查结界缝隙(1)', 20, 13, 0, 0, 0, '[아크라 중랩] 결계의 틈 조사(1) (헌터마스터)', 1, 1, 12, 0, 147, 798994.0, 16831.0, 716557.0, 1, 1, 0, 1036, 1036, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (133, '向狩猎导师报告', 20, 13, 0, 132, 0, '[아크라 중랩] 헌터마스터에게 보고 (이상한 돌무더기)', 1, 1, 16, 0, 142, 777063.0, 12861.0, 689254.0, 1, 21, 134, 1049, 1049, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (132, '奇怪的石堆(2)', 20, 13, 0, 131, 0, '[아크라 중랩] 이상한 돌무더기(2) (헌터마스터)', 1, 1, 15, 0, 149, 720154.0, 11354.0, 717882.0, 1, 11, 134, 1052, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (131, '奇怪的石堆(1)', 20, 13, 0, 127, 0, '[아크라 중랩] 이상한 돌무더기(1) (헌터마스터)', 1, 1, 14, 0, 149, 720154.0, 11354.0, 717882.0, 1, 21, 134, 1052, 1052, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (130, '野性的树海(3)', 20, 13, 0, 129, 0, '[아크라 중랩] 야성수해의 비밀보고 (람단)', 1, 2, 18, 0, 142, 777063.0, 12861.0, 689254.0, 1, 20, 134, 1049, 1049, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (129, '野性的树海(2)', 20, 13, 0, 128, 0, '[아크라 중랩] 야성의 수해(2) (람단)', 1, 2, 17, 0, 152, 736375.0, 15850.0, 641509.0, 1, 10, 134, 1029, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (128, '野性的树海(1)', 20, 13, 0, 133, 0, '[아크라 중랩] 야성의 수해에 숨겨진 비밀(1)', 1, 2, 17, 0, 152, 736375.0, 15850.0, 641509.0, 1, 32, 134, 1029, 1029, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (127, '保卫者的秘密(3)', 20, 13, 0, 126, 0, '[아크라 중레벨 퀘스트]마두 3번 퀘스트', 1, 1, 13, 0, 150, 752303.0, 16895.0, 700608.0, 1, 21, 134, 1028, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (126, '保卫者的秘密(2)', 20, 13, 0, 125, 0, '[아크라 중레벨 퀘스트]마두 2번 퀘스트', 1, 1, 12, 0, 150, 752303.0, 16895.0, 700608.0, 1, 11, 134, 1028, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (125, '保卫者的秘密(1)', 20, 13, 0, 136, 0, '[아크라 중레벨 퀘스트] 헌터 마스터 시작(마두1번퀘)', 1, 1, 12, 0, 150, 752303.0, 16895.0, 700608.0, 1, 11, 134, 1028, 1028, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (120, '最强的技能', 8, 50, 0, 0, 0, '[50레벨 스킬북 퀘스트] 어쌔신', 1, 5, 34, 0, 0, 0.0, 0.0, 0.0, 1, 14, 0, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (119, '最强的技能(II)', 4, 50, 0, 118, 0, '[50레벨 스킬북 퀘스트] 엘프2', 1, 5, 33, 0, 0, 0.0, 0.0, 0.0, 1, 13, 118, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (118, '最强的技能', 4, 50, 0, 0, 0, '[50레벨 스킬북 퀘스트] 엘프1', 1, 5, 32, 0, 0, 0.0, 0.0, 0.0, 1, 12, 0, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (117, '最强的技能', 2, 50, 0, 0, 0, '[50레벨 스킬북 퀘스트] 레인저', 1, 5, 31, 0, 0, 0.0, 0.0, 0.0, 1, 11, 0, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (116, '最强的技能', 1, 50, 0, 0, 0, '[50레벨 스킬북 퀘스트] 나이트', 1, 5, 30, 0, 0, 0.0, 0.0, 0.0, 1, 10, 0, 953, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (115, '秘银铠甲III', 255, 60, 0, 114, 0, '[미스릴 갑옷 강화 퀘스트 3번] 경비병 코이', 1, 4, 0, 1, 112, 60162.0, 23193.0, 53528.0, 1, 2, 113, 760, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (114, '强化秘银铠甲II', 255, 60, 0, 113, 0, '[미스릴 갑옷 강화 퀘스트 2번] 경비병 다루', 1, 4, 0, 1, 116, 134879.0, 20459.0, 146006.0, 1, 2, 113, 758, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (113, '强化秘银铠甲', 255, 60, 0, 0, 0, '[미스릴 갑옷 강화 퀘스트 1번] 칸다루', 1, 4, 0, 1, 117, 100575.0, 19948.0, 164429.0, 1, 2, 0, 754, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (112, '名誉项链', 255, 45, 0, 0, 0, '[명예의 망토] 로덴 리', 1, 3, 0, 1, 113, 212642.0, 22360.0, 43217.0, 1, 1, 0, 756, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (111, '名誉披风', 255, 35, 0, 0, 0, '[명예의 망토] 바이런 경비병 진/코라/코코/톰', 1, 3, 0, 1, 81, 336753.0, 22733.0, 92868.0, 1, 3, 0, 677, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (99, '小礼物', 255, 55, 0, 98, 0, '[55레벨 퀘스트 8번] <경비병>폰챠카 3번', 1, 4, 47, 0, 19, 316705.0, 12396.0, 260777.0, 1, 31, 92, 570, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (98, '冶炼用钳子', 255, 55, 0, 97, 0, '[55레벨 퀘스트 7번] 대장장이 아내 2번(집게)', 1, 4, 0, 1, 46, 162938.0, 17952.0, 319566.0, 1, 5, 92, 816, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (97, '冶炼用锤子', 255, 55, 0, 96, 0, '[55레벨 퀘스트 6번] 대장장이 아내 1번(망치)', 1, 4, 23, 0, 46, 162938.0, 17952.0, 319566.0, 1, 2, 92, 816, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (96, '兰恩特曼的担心', 255, 55, 0, 95, 0, '[55레벨 퀘스트 5번] 대장장이 랜들맨', 1, 2, 21, 0, 39, 217376.0, 11930.0, 228429.0, 1, 21, 92, 816, 816, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (95, '生气的兰恩特曼', 255, 55, 0, 94, 0, '[55레벨 퀘스트 4번] <경비병>폰챠카 2번', 1, 2, 21, 0, 39, 217106.0, 11923.0, 227691.0, 1, 20, 92, 600, 600, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (94, '修理好的断剑', 255, 55, 0, 93, 0, '[55레벨 퀘스트 3번] 대장장이 러스 2번', 1, 2, 21, 0, 39, 219220.0, 11561.0, 236482.0, 1, 15, 92, 570, 570, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (93, '不会熄灭的火种', 255, 55, 0, 92, 0, '[55레벨 퀘스트 2번] 대장장이 러스 1번', 1, 4, 22, 0, 17, 464098.0, 21358.0, 306728.0, 1, 11, 92, 160, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (92, '修理断剑', 255, 55, 0, 0, 0, '[55레벨 퀘스트 1번] <경비병>폰챠카 1번', 1, 2, 21, 0, 9, 362047.0, 15721.0, 289696.0, 1, 3, 0, 160, 160, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (90, '准备讨伐伊芙利特', 1, 50, 0, 87, 0, '[50레벨 이프리트 퀘스트 3번이후] <촌장>브랜든', 1, 4, 0, 1, 0, 0.0, 0.0, 0.0, 1, 10, 0, 135, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (88, '古代书籍', 255, 50, 0, 87, 0, '[50레벨 이프리트 퀘스트 4번] <봉인술사>반데르2', 1, 3, 0, 1, 17, 464098.0, 21358.0, 306728.0, 1, 3, 85, 814, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (87, '伊芙利特的复活', 255, 50, 0, 86, 0, '[50레벨 이프리트 퀘스트 3번] <봉인술사>반데르1', 1, 3, 23, 0, 0, 0.0, 0.0, 0.0, 1, 2, 85, 814, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (86, '封印的磐石', 255, 50, 0, 85, 0, '[50레벨 이프리트 퀘스트 2번] 올렌버그', 1, 1, 22, 0, 108, 449122.0, 18645.0, 272261.0, 1, 1, 85, 311, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (85, '伊芙利特的封印', 255, 50, 0, 0, 0, '[50레벨 이프리트 퀘스트 1번] 파이론', 1, 4, 22, 0, 17, 464098.0, 21358.0, 306728.0, 1, 1, 0, 622, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (56, '制造熔岩铸剑', 255, 12, 0, 0, 0, '[용아검 연속 퀘스트 6번(끝)] <경비병>셀비', 1, 2, 39, 0, 0, 0.0, 0.0, 0.0, 1, 1, 0, 321, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (47, '制造暗红之装甲2', 255, 12, 0, 46, 0, '[크림슨갑옷 연속 퀘스트7번(끝)] 오크대장장이', 1, 2, 0, 1, 0, 0.0, 0.0, 0.0, 1, 1, 2, 240, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (46, '哈特的最后考验', 255, 12, 0, 45, 0, '[크림슨갑옷 연속 퀘스트 6번] 하트 여섯번째', 1, 2, 14, 0, 19, 316705.0, 12396.0, 260777.0, 1, 6, 2, 165, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (45, '哈特的猎捕火骷髅考验', 255, 12, 0, 44, 0, '[크림슨갑옷 연속 퀘스트 5번] 하트 다섯번째', 1, 2, 12, 0, 17, 464098.0, 21358.0, 306728.0, 1, 5, 2, 165, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (44, '哈特的猎捕兽人战士考验', 255, 12, 0, 43, 0, '[크림슨갑옷 연속 퀘스트 4번] 하트 네번째', 1, 2, 12, 0, 21, 281919.0, 15141.0, 237451.0, 1, 4, 2, 165, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (43, '哈特的猎捕蜥蜴人考验', 255, 12, 0, 42, 0, '[크림슨갑옷 연속 퀘스트 3번] 하트 세번째', 1, 2, 12, 0, 44, 96151.0, 11312.0, 279591.0, 1, 3, 2, 165, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (42, '哈特的猎捕极乐之鸟考验', 255, 12, 0, 0, 0, '[크림슨갑옷 연속 퀘스트 2번] 하트 두번째', 1, 2, 38, 0, 50, 197541.0, 16639.0, 149231.0, 1, 2, 2, 165, 0, 0);
GO

INSERT INTO [dbo].[TblQuest] ([mQuestNo], [mQuestNm], [mClass], [mLevel1], [mLevel2], [mPreQuestNo], [mIsOverlap], [mQuestDesc], [mAbandonment], [mDifficulty], [mRewardNo], [mScriptType], [mPlace], [mPosX], [mPosY], [mPosZ], [mVisible], [mTextNo], [mParentNo], [mFindNPC], [mCompletionNPC], [mQuestKind]) VALUES (2, '制造暗红之装甲(1)', 255, 12, 0, 0, 0, '[크림슨갑옷 연속 퀘스트 1번] 하트 첫번째', 1, 2, 37, 0, 20, 308646.0, 13669.0, 315567.0, 1, 1, 0, 165, 0, 0);
GO

