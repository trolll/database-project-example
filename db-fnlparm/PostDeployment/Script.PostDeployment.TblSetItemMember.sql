/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblSetItemMember
Date                  : 2023-10-07 09:08:58
*/


INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (1, 40030);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (1, 40150);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (1, 40270);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (1, 40390);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (1, 40510);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (2, 40031);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (2, 40151);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (2, 40271);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (2, 40391);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (2, 40511);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (3, 40032);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (3, 40152);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (3, 40272);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (3, 40392);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (3, 40512);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (4, 40033);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (4, 40153);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (4, 40273);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (4, 40393);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (4, 40513);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (5, 40034);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (5, 40154);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (5, 40274);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (5, 40394);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (5, 40514);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (6, 40035);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (6, 40155);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (6, 40275);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (6, 40395);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (6, 40515);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (7, 40036);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (7, 40156);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (7, 40276);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (7, 40396);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (7, 40516);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (8, 40037);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (8, 40157);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (8, 40277);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (8, 40397);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (8, 40517);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (9, 40038);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (9, 40158);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (9, 40278);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (9, 40398);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (9, 40518);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (10, 40039);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (10, 40159);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (10, 40279);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (10, 40399);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (10, 40519);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (11, 40040);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (11, 40160);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (11, 40280);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (11, 40400);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (11, 40520);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (12, 40041);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (12, 40161);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (12, 40281);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (12, 40401);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (12, 40521);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (13, 40042);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (13, 40162);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (13, 40282);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (13, 40402);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (13, 40522);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (14, 40043);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (14, 40163);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (14, 40283);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (14, 40403);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (14, 40523);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (15, 40044);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (15, 40164);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (15, 40284);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (15, 40404);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (15, 40524);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (16, 40045);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (16, 40165);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (16, 40285);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (16, 40405);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (16, 40525);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (17, 40046);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (17, 40166);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (17, 40286);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (17, 40406);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (17, 40526);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (18, 40047);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (18, 40167);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (18, 40287);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (18, 40407);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (18, 40527);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (19, 40048);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (19, 40168);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (19, 40288);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (19, 40408);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (19, 40528);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (20, 40049);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (20, 40169);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (20, 40289);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (20, 40409);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (20, 40529);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (21, 40050);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (21, 40170);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (21, 40290);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (21, 40410);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (21, 40530);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (22, 40051);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (22, 40171);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (22, 40291);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (22, 40411);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (22, 40531);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (23, 40052);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (23, 40172);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (23, 40292);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (23, 40412);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (23, 40532);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (24, 40053);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (24, 40173);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (24, 40293);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (24, 40413);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (24, 40533);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (25, 40054);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (25, 40174);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (25, 40294);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (25, 40414);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (25, 40534);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (26, 40055);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (26, 40175);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (26, 40295);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (26, 40415);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (26, 40535);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (27, 40056);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (27, 40176);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (27, 40296);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (27, 40416);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (27, 40536);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (28, 40057);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (28, 40177);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (28, 40297);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (28, 40417);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (28, 40537);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (29, 40058);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (29, 40178);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (29, 40298);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (29, 40418);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (29, 40538);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (30, 40059);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (30, 40179);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (30, 40299);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (30, 40419);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (30, 40539);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (31, 40060);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (31, 40180);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (31, 40300);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (31, 40420);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (31, 40540);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (32, 40061);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (32, 40181);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (32, 40301);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (32, 40421);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (32, 40541);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (33, 40062);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (33, 40182);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (33, 40302);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (33, 40422);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (33, 40542);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (34, 40063);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (34, 40183);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (34, 40303);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (34, 40423);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (34, 40543);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (35, 40064);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (35, 40184);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (35, 40304);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (35, 40424);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (35, 40544);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (36, 40065);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (36, 40185);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (36, 40305);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (36, 40425);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (36, 40545);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (37, 40066);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (37, 40186);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (37, 40306);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (37, 40426);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (37, 40546);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (38, 40067);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (38, 40187);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (38, 40307);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (38, 40427);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (38, 40547);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (39, 40068);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (39, 40188);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (39, 40308);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (39, 40428);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (39, 40548);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (40, 40069);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (40, 40189);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (40, 40309);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (40, 40429);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (40, 40549);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (41, 40070);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (41, 40190);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (41, 40310);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (41, 40430);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (41, 40550);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (42, 40071);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (42, 40191);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (42, 40311);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (42, 40431);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (42, 40551);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (43, 40072);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (43, 40192);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (43, 40312);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (43, 40432);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (43, 40552);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (44, 40073);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (44, 40193);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (44, 40313);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (44, 40433);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (44, 40553);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (45, 40074);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (45, 40194);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (45, 40314);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (45, 40434);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (45, 40554);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (46, 40075);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (46, 40195);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (46, 40315);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (46, 40435);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (46, 40555);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (47, 40076);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (47, 40196);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (47, 40316);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (47, 40436);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (47, 40556);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (48, 40077);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (48, 40197);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (48, 40317);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (48, 40437);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (48, 40557);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (49, 40078);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (49, 40198);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (49, 40318);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (49, 40438);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (49, 40558);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (50, 40079);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (50, 40199);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (50, 40319);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (50, 40439);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (50, 40559);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (51, 40080);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (51, 40200);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (51, 40320);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (51, 40440);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (51, 40560);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (52, 40081);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (52, 40201);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (52, 40321);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (52, 40441);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (52, 40561);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (53, 40082);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (53, 40202);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (53, 40322);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (53, 40442);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (53, 40562);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (54, 40083);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (54, 40203);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (54, 40323);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (54, 40443);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (54, 40563);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (55, 40084);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (55, 40204);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (55, 40324);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (55, 40444);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (55, 40564);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (56, 40085);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (56, 40205);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (56, 40325);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (56, 40445);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (56, 40565);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (57, 40086);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (57, 40206);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (57, 40326);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (57, 40446);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (57, 40566);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (58, 40087);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (58, 40207);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (58, 40327);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (58, 40447);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (58, 40567);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (59, 40088);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (59, 40208);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (59, 40328);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (59, 40448);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (59, 40568);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (60, 40089);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (60, 40209);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (60, 40329);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (60, 40449);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (60, 40569);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (61, 40090);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (61, 40210);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (61, 40330);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (61, 40450);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (61, 40570);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (62, 40091);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (62, 40211);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (62, 40331);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (62, 40451);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (62, 40571);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (63, 40092);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (63, 40212);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (63, 40332);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (63, 40452);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (63, 40572);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (64, 40093);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (64, 40213);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (64, 40333);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (64, 40453);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (64, 40573);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (65, 40094);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (65, 40214);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (65, 40334);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (65, 40454);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (65, 40574);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (66, 40095);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (66, 40215);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (66, 40335);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (66, 40455);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (66, 40575);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (67, 40096);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (67, 40216);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (67, 40336);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (67, 40456);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (67, 40576);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (68, 40097);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (68, 40217);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (68, 40337);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (68, 40457);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (68, 40577);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (69, 40098);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (69, 40218);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (69, 40338);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (69, 40458);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (69, 40578);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (70, 40099);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (70, 40219);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (70, 40339);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (70, 40459);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (70, 40579);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (71, 40100);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (71, 40220);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (71, 40340);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (71, 40460);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (71, 40580);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (72, 40101);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (72, 40221);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (72, 40341);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (72, 40461);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (72, 40581);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (73, 40102);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (73, 40222);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (73, 40342);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (73, 40462);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (73, 40582);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (74, 40103);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (74, 40223);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (74, 40343);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (74, 40463);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (74, 40583);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (75, 40104);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (75, 40224);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (75, 40344);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (75, 40464);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (75, 40584);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (76, 40105);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (76, 40225);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (76, 40345);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (76, 40465);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (76, 40585);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (77, 40106);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (77, 40226);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (77, 40346);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (77, 40466);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (77, 40586);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (78, 40107);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (78, 40227);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (78, 40347);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (78, 40467);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (78, 40587);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (79, 40108);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (79, 40228);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (79, 40348);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (79, 40468);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (79, 40588);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (80, 40109);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (80, 40229);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (80, 40349);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (80, 40469);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (80, 40589);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (81, 40110);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (81, 40230);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (81, 40350);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (81, 40470);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (81, 40590);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (82, 40111);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (82, 40231);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (82, 40351);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (82, 40471);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (82, 40591);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (83, 40112);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (83, 40232);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (83, 40352);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (83, 40472);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (83, 40592);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (84, 40113);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (84, 40233);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (84, 40353);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (84, 40473);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (84, 40593);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (85, 40114);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (85, 40234);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (85, 40354);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (85, 40474);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (85, 40594);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (86, 40115);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (86, 40235);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (86, 40355);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (86, 40475);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (86, 40595);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (87, 40116);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (87, 40236);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (87, 40356);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (87, 40476);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (87, 40596);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (88, 40117);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (88, 40237);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (88, 40357);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (88, 40477);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (88, 40597);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (89, 40118);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (89, 40238);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (89, 40358);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (89, 40478);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (89, 40598);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (90, 40119);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (90, 40239);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (90, 40359);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (90, 40479);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (90, 40599);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (91, 40611);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (91, 40612);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (91, 40613);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (91, 40614);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (91, 40615);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (92, 3149);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (92, 3215);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (92, 3226);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (92, 3237);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (93, 3150);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (93, 3216);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (93, 3227);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (93, 3238);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (94, 3151);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (94, 3217);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (94, 3228);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (94, 3239);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (95, 3152);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (95, 3218);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (95, 3229);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (95, 3240);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (96, 3153);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (96, 3219);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (96, 3230);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (96, 3241);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (97, 3154);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (97, 3220);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (97, 3231);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (97, 3242);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (98, 3155);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (98, 3221);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (98, 3232);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (98, 3243);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (99, 3156);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (99, 3222);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (99, 3233);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (99, 3244);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (100, 3157);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (100, 3223);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (100, 3234);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (100, 3245);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (101, 3158);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (101, 3224);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (101, 3235);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (101, 3246);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (102, 3159);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (102, 3225);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (102, 3236);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (102, 3247);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (104, 3961);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (104, 3962);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (104, 3963);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (105, 3964);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (105, 3965);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (105, 3966);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (106, 3967);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (106, 3968);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (106, 3969);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (108, 4612);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (108, 4628);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (108, 4632);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (108, 4636);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (109, 4613);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (109, 4629);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (109, 4633);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (109, 4637);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (110, 4614);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (110, 4630);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (110, 4634);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (110, 4638);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (111, 4615);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (111, 4631);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (111, 4635);
GO

INSERT INTO [dbo].[TblSetItemMember] ([mSetType], [IID]) VALUES (111, 4639);
GO

