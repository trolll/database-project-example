/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblEventQuestResultDraw
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (1, 90.0, 8.0, 2.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (2, 90.0, 8.0, 2.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (3, 90.0, 8.0, 2.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (4, 90.0, 8.0, 2.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (5, 70.0, 20.0, 10.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (6, 70.0, 20.0, 10.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (7, 70.0, 20.0, 10.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (8, 70.0, 20.0, 10.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (9, 30.0, 40.0, 30.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (10, 30.0, 40.0, 30.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (11, 30.0, 40.0, 30.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (12, 30.0, 40.0, 30.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (13, 20.0, 40.0, 40.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (14, 20.0, 40.0, 40.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (15, 20.0, 40.0, 40.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (16, 20.0, 40.0, 40.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (17, 10.0, 40.0, 50.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (18, 10.0, 40.0, 50.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (19, 10.0, 40.0, 50.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (20, 10.0, 40.0, 50.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (21, 10.0, 40.0, 50.0);
GO

INSERT INTO [dbo].[TblEventQuestResultDraw] ([mMonClass], [mFair], [mGood], [mExcellent]) VALUES (22, 10.0, 40.0, 50.0);
GO

