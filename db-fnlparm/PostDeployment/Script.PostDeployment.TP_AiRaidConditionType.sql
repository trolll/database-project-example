/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AiRaidConditionType
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (1, '??/?? ???? ??', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (2, 'HP ?? ??', '??HP%', '??HP%', NULL);
GO

INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (3, '???? ??', '', '', '');
GO

INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (4, '?? ???? PC??', '??? ???', NULL, NULL);
GO

INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (5, '????', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (6, '????', '??? ??', '??1', '??2');
GO

INSERT INTO [dbo].[TP_AiRaidConditionType] ([mCID], [mDesc], [mCAParamName], [mCBParamName], [mCCParamName]) VALUES (7, 'HP? ????', '??HP%', '??HP%', '??1');
GO

