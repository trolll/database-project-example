/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMaterialEnchantProbability
Date                  : 2023-10-07 09:08:48
*/


INSERT INTO [dbo].[TblMaterialEnchantProbability] ([MEnchant], [MAddValue], [MReinforceFail]) VALUES (1, 0, 0);
GO

INSERT INTO [dbo].[TblMaterialEnchantProbability] ([MEnchant], [MAddValue], [MReinforceFail]) VALUES (2, 12, 0);
GO

INSERT INTO [dbo].[TblMaterialEnchantProbability] ([MEnchant], [MAddValue], [MReinforceFail]) VALUES (3, 14, 1);
GO

INSERT INTO [dbo].[TblMaterialEnchantProbability] ([MEnchant], [MAddValue], [MReinforceFail]) VALUES (4, 16, 2);
GO

INSERT INTO [dbo].[TblMaterialEnchantProbability] ([MEnchant], [MAddValue], [MReinforceFail]) VALUES (5, 18, 3);
GO

INSERT INTO [dbo].[TblMaterialEnchantProbability] ([MEnchant], [MAddValue], [MReinforceFail]) VALUES (6, 20, 0);
GO

