/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblPlaceInvalidList
Date                  : 2023-10-07 09:07:32
*/


INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (1, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (2, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (8, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (9, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (23, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (24, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (25, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (26, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1619);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 2, 39);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (27, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (28, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (29, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (30, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (31, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (32, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (33, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (34, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (35, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (36, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (37, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (38, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (39, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (45, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (51, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (52, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (53, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (54, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (55, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (56, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (57, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (59, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (60, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (61, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (62, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (63, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (64, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (65, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (66, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (67, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (68, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (69, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (70, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (71, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (72, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (73, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (83, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (84, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (85, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (86, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (87, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (88, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (89, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (90, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (91, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (92, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (93, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (94, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (96, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (97, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (98, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (99, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (100, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (101, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (102, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (103, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (104, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (105, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (106, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (107, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (110, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (110, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (110, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (110, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (121, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (122, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (123, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (124, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (125, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 1619);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (127, 2, 39);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1619);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 2, 39);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (128, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (129, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (130, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (131, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (132, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (133, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (134, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (135, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (135, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (135, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (135, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (135, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (136, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (137, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (137, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (137, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (137, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (137, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (139, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (140, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (141, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (143, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (166, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (168, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (169, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (170, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (171, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (172, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (173, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (174, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (175, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (176, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (177, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (178, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (179, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (181, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (181, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (181, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (181, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (181, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (181, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (184, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (184, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (184, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (184, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (184, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (184, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (187, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (187, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (187, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (187, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (187, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (187, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (190, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (190, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (190, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (190, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (190, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (190, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (193, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (193, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (193, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (193, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (193, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (193, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (196, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (196, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (196, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (196, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (196, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (196, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (199, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (199, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (199, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (199, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (199, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (199, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (202, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (202, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (202, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (202, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (202, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (202, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (205, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (205, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (205, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (205, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (205, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (205, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (208, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (208, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (208, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (208, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (208, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (208, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (211, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (211, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (211, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (211, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (211, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (211, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (214, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (214, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (214, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (214, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (214, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (214, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (217, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (217, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (217, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (217, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (217, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (217, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (220, 1, 59);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (220, 1, 555);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (220, 1, 556);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (220, 1, 557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (220, 1, 558);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (220, 1, 559);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (226, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (227, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (228, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (229, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (230, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (231, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (232, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 1575);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (233, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (234, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (235, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (236, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (248, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (249, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (250, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (251, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (252, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (253, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (254, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (255, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (256, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (257, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (258, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (259, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (265, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (266, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (267, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (268, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (269, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (270, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (271, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (272, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 357);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1123);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1130);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1131);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1135);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1136);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1499);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1582);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1583);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1584);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1615);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1616);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1618);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1941);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1985);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2001);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2441);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2557);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2586);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2820);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2821);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2822);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2823);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2824);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2825);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2826);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 2835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 3352);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 3359);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 4125);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 4167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 4890);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 5682);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 0, 5683);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (273, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 489);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 492);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 494);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 737);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 738);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 828);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2606);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2607);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2608);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2609);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2610);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2611);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2612);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2613);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 2795);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 3358);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 3430);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 3431);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 3432);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 3433);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 0, 3434);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1151);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1152);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1153);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1154);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1155);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1167);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1168);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1169);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1170);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1171);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1208);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1209);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1210);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1211);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1212);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1213);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1214);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1215);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1216);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1217);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1376);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1377);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1378);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1383);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1384);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1385);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1386);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1387);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1391);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1392);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1393);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1394);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1395);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1396);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1397);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1398);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1400);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 1, 1684);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (274, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (275, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (276, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (277, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (278, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 490);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (279, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (280, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (281, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (282, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (283, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (284, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (285, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (286, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (287, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (288, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (289, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (290, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (291, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (292, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (294, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (295, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (296, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (297, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (298, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (299, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 425);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 1, 73);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 1575);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (300, 3, 2231);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (301, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (302, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (303, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (304, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (305, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (306, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (308, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (309, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (310, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (311, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (312, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (313, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (314, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (315, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (316, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (317, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (318, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 860);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 1023);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 1399);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 1481);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 1998);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 2, 31);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 2, 46);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 107);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 173);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 207);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 309);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 317);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 318);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 319);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 485);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 500);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 834);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (319, 3, 1576);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (320, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (320, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (320, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (320, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (320, 3, 288);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (321, 0, 362);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (321, 0, 835);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (321, 0, 2058);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (321, 3, 80);
GO

INSERT INTO [dbo].[TblPlaceInvalidList] ([mPlace], [mType], [mValue]) VALUES (321, 3, 288);
GO

