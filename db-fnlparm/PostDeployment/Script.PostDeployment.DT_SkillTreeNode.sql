/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillTreeNode
Date                  : 2023-10-07 09:08:47
*/


INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (1, 1, '血之盟誓', 5, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (2, 1, '----------', 0, 1, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (3, 1, '----------', 0, 1, 1, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (4, 1, '----------', 0, 1, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (5, 1, '神之祝福', 5, 0, 1, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (6, 1, '勇士的祝福', 3, 0, 1, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (7, 1, '灵魂之盟誓', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (8, 1, '----------', 0, 1, 2, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (9, 1, '----------', 0, 1, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (10, 1, '----------', 0, 1, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (11, 1, '灵魂的祝福', 5, 0, 2, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (12, 1, '贤者的祝福', 3, 0, 2, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (14, 1, '巨人之力', 5, 0, 4, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (15, 1, '不灭之魂', 2, 0, 3, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (16, 1, '指定目标', 1, 0, 4, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (17, 1, '荣誉提升', 5, 0, 3, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (18, 1, '技能列表专用荣誉勋章', 1, 0, 3, 4, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (19, 1, '召唤', 1, 0, 4, 4, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (20, 1, '团结的力量(I)', 3, 0, 1, 7, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (21, 1, '团结的敏捷(I)', 3, 0, 2, 7, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (22, 1, '----------', 0, 1, 2, 8, 1, 0, 0, 2, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (23, 1, '团结的智力', 3, 0, 3, 7, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (24, 1, '猛虎之爪', 5, 0, 1, 8, 1, 1, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (26, 1, '猛虎之力', 5, 0, 1, 9, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (28, 1, '朱庇特守护', 5, 0, 4, 9, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (29, 1, '近战防御精通(I)', 5, 0, 1, 10, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (30, 1, '远程防御精通(I)', 5, 0, 2, 10, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (31, 1, '魔法防御精通(I)', 5, 0, 3, 10, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (32, 2, '近战防御精通(II)', 3, 0, 1, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (33, 2, '远程防御精通(II)', 3, 0, 2, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (34, 2, '魔法防御精通(II)', 3, 0, 3, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (35, 2, '团结之力(I)', 5, 0, 1, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (36, 2, '飓风之力(I)', 5, 0, 2, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (37, 2, '魔法之力(I)', 5, 0, 3, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (38, 2, '猎豹之眼(I)', 3, 0, 1, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (39, 2, '飓风之眼(I)', 3, 0, 2, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (40, 2, '魔法之手(I)', 3, 0, 3, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (41, 3, '团结的力量(II)', 2, 0, 1, 1, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (42, 3, '团结的敏捷(II)', 2, 0, 2, 1, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (43, 3, '团结的智力(II)', 2, 0, 3, 1, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (44, 3, '贤者之祝福(II)', 2, 0, 4, 1, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (45, 3, '近战防御精通(III)', 2, 0, 1, 2, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (46, 3, '远程防御精通(III)', 2, 0, 2, 2, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (47, 3, '魔法防御精通(III)', 2, 0, 3, 2, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (48, 3, '猎豹之眼(II)', 2, 0, 1, 3, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (49, 3, '飓风之眼(II)', 2, 0, 2, 3, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (50, 3, '魔法之手(II)', 2, 0, 3, 3, 0, 0, 0, 0, 7);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (51, 4, '体力增加', 5, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (52, 4, '精神力增加', 5, 0, 3, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (53, 4, '法力增加', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (54, 4, '----------', 0, 1, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (55, 4, '----------', 0, 1, 2, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (56, 4, '体力恢复', 5, 0, 1, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (57, 4, '法力恢复', 5, 0, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (58, 4, '----------', 0, 1, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (59, 4, '----------', 0, 1, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (60, 4, '小矮人魔法', 5, 0, 4, 4, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (61, 4, '体力恢复(II)', 3, 0, 1, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (62, 4, '法力恢复(II)', 3, 0, 2, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (63, 4, '变身精通', 5, 0, 4, 6, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (64, 4, '英雄之力', 3, 0, 1, 7, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (65, 4, '猎人敏捷', 3, 0, 2, 7, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (66, 4, '贤者智慧', 3, 0, 3, 7, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (67, 5, '重击精通', 3, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (68, 5, '攻击精通(I)', 5, 0, 4, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (69, 5, '重击强化', 5, 0, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (70, 5, '猛力攻击精通', 3, 0, 2, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (71, 5, '攻击精通(II)', 5, 0, 4, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (72, 5, '猛力攻击强化', 3, 0, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (73, 5, '暴走精通', 3, 0, 3, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (74, 5, '攻击精通(III)', 5, 0, 4, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (75, 5, '三阶攻击', 5, 0, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (76, 5, '----------', 0, 1, 1, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (77, 5, '暴走强化', 4, 0, 3, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (78, 6, '痛苦麻痹精通', 5, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (79, 6, '防御精通(I)', 5, 0, 4, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (80, 6, '痛苦麻痹强化', 3, 0, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (81, 6, '吸收', 3, 0, 2, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (82, 6, '防御精通(II)', 5, 0, 4, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (83, 6, '生命绽放精通', 5, 0, 2, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (84, 6, '最后的抵抗', 5, 0, 3, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (85, 6, '防御精通(III)', 5, 0, 4, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (86, 6, '生命绽放强化', 5, 0, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (87, 6, '魔法抗体', 1, 0, 3, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (88, 7, '瞄准射击精通', 3, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (89, 7, '远程攻击精通(I)', 5, 0, 4, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (90, 7, '瞄准射击强化', 5, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (91, 7, '音速屏障精通', 3, 0, 2, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (92, 7, '火焰箭', 5, 0, 1, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (93, 7, '音速屏障强化', 5, 0, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (95, 7, '----------', 0, 1, 4, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (96, 7, '远程攻击精通', 3, 0, 4, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (97, 7, '爆炸', 5, 0, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (99, 8, '牵制', 5, 0, 1, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (100, 8, '移动妨碍回避', 5, 0, 4, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (101, 8, '振荡射击精通', 3, 0, 1, 2, 0, 2, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (102, 8, '----------', 0, 1, 2, 2, 0, 0, 2, 1, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (103, 8, '巨鹰之眼', 3, 0, 3, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (104, 8, '无法移动抵抗', 5, 0, 4, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (105, 8, '振荡射击瞄准', 5, 0, 1, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (106, 8, '振荡射击强化', 5, 0, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (107, 8, '钢铁陷阱', 5, 0, 3, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (108, 8, '粘性陷阱', 5, 0, 3, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (109, 9, '强化魔法盾', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (110, 9, '灵魂之力', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (111, 9, '剑术精通强化', 5, 0, 3, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (112, 9, '石化皮肤强化', 2, 0, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (113, 9, '精神之力', 5, 0, 2, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (114, 9, '强化元素铠甲', 5, 0, 1, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (115, 9, '精神力', 3, 0, 2, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (116, 9, '强化魔法驱散', 3, 0, 2, 4, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (117, 9, '法力转换HP', 3, 0, 4, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (118, 9, '强化群体拯救', 5, 0, 3, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (119, 9, '法力转换攻击', 3, 0, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (120, 10, '灵魂之力', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (121, 10, '强化诅咒', 3, 0, 3, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (122, 10, '强化厄运之触', 3, 0, 1, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (123, 10, '精神之力', 5, 0, 2, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (124, 10, '强化沉默', 3, 0, 3, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (125, 10, '强化剧毒之云', 3, 0, 1, 3, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (126, 10, '魔力增加', 5, 0, 2, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (127, 10, '强化烈焰风暴', 5, 0, 3, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (128, 10, '增加集中力', 3, 0, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (129, 10, '魔力吸食', 5, 0, 3, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (130, 11, '回避精通', 5, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (131, 11, '暴击', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (132, 11, '攻击精通(I)', 5, 0, 4, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (133, 11, '回避强化', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (134, 11, '----------', 0, 1, 2, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (135, 11, '毒牙精通', 5, 0, 3, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (136, 11, '攻击精通(II)', 5, 0, 4, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (137, 11, '暴击攻击', 5, 0, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (138, 11, '攻击精通(III)', 5, 0, 4, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (139, 11, '强化要害攻击', 5, 0, 1, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (140, 12, '昏厥攻击精通', 5, 0, 1, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (141, 12, '强化疾行', 5, 0, 3, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (143, 12, '昏厥攻击强化', 3, 0, 1, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (144, 12, '暗杀精通', 5, 0, 2, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (146, 12, '解毒', 4, 0, 2, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (147, 12, '强化暗杀', 5, 0, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (148, 12, '强化隐身术', 3, 0, 3, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (149, 12, '消灭', 1, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (150, 13, '强化哥布林爆破兵', 3, 0, 1, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (151, 13, '灵魂之力', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (152, 13, '强化猛力攻击', 5, 0, 3, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (153, 13, '强化曼陀罗', 3, 0, 1, 2, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (154, 13, '精神之力', 5, 0, 2, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (155, 13, '敏捷转换力量', 3, 0, 4, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (156, 13, '强化流星火', 0, 1, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (157, 13, '强化海神之怒', 5, 0, 2, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (158, 13, '近战攻击集中', 3, 0, 4, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (159, 13, '狂暴', 1, 0, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (160, 14, '战斗用生命图腾', 3, 0, 1, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (161, 14, '灵魂之力', 5, 0, 2, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (162, 14, '强化防御图腾', 1, 0, 3, 1, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (163, 14, '精神之力', 5, 0, 2, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (164, 14, '力量转化敏捷', 3, 0, 4, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (165, 14, '强化奥利爱德图腾', 5, 0, 1, 3, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (166, 14, '远程攻击集中', 3, 0, 4, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (167, 14, '召唤攻击图腾', 5, 0, 1, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (168, 14, '召唤防御图腾', 5, 0, 2, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (169, 7, '疾速射击精通', 3, 0, 3, 2, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (170, 7, '强化疾速射击', 5, 0, 3, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (171, 5, '重击精通（II）', 3, 0, 1, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (172, 5, '重击增幅', 1, 0, 1, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (173, 5, '猛力攻击强化（II）', 5, 0, 2, 4, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (174, 5, '----------', 0, 1, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (175, 5, '暴走强化（II）', 3, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (176, 5, '攻击精通（Ⅳ）', 3, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (177, 6, '---------', 0, 1, 1, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (178, 6, '痛苦麻痹强化（II）', 3, 0, 1, 4, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (179, 6, '--------', 0, 1, 2, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (180, 6, '生命绽放增幅', 1, 0, 2, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (181, 6, '魔法抵抗强化', 3, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (182, 6, '--------', 0, 1, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (183, 6, '防御精通（Ⅳ）', 3, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (184, 9, '----------', 0, 1, 1, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (185, 9, '---------', 0, 1, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (186, 9, '神圣铠甲', 1, 0, 1, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (187, 9, '祝福光环精通', 3, 0, 3, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (188, 9, '凝神术强化', 5, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (189, 9, '法力转换强化', 3, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (190, 10, '侵蚀火焰强化', 3, 0, 1, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (191, 10, '专注', 3, 0, 2, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (192, 10, '死亡之手', 1, 0, 2, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (193, 10, '魔力吸食强化', 3, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (194, 10, '侦查强化', 1, 0, 4, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (195, 10, '-------', 0, 1, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (196, 10, '蛛网术强化', 3, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (197, 7, '-------', 0, 1, 1, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (198, 7, '火焰箭强化', 1, 0, 1, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (199, 7, '------', 0, 1, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (200, 7, '音速屏障强化(II)', 3, 0, 2, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (201, 7, '疾速射击强化（II）', 3, 0, 3, 4, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (202, 7, '--------', 0, 1, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (203, 7, '远程攻击精通（III）', 3, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (204, 8, '振荡射击强化（II）', 5, 0, 2, 4, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (205, 8, '陷阱强化', 3, 0, 3, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (206, 8, '陷阱强化（II）', 1, 0, 3, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (207, 8, '无视护甲强化（I）', 3, 0, 4, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (208, 8, '无视护甲强化（II）', 2, 0, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (209, 8, '无视护甲强化（III）', 3, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (210, 11, '要害攻击精通', 5, 0, 1, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (211, 11, '--------', 0, 1, 2, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (212, 11, '暴击强化', 5, 0, 2, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (214, 11, '强效毒瓶生成[短剑]', 1, 0, 3, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (215, 11, '---------', 0, 1, 3, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (216, 11, '毒牙强化', 1, 0, 3, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (217, 11, '强效毒瓶生成[拳剑]', 1, 0, 3, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (219, 12, '------', 0, 1, 1, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (220, 12, '昏厥攻击强化（II）', 3, 0, 1, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (221, 12, '------', 0, 1, 2, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (222, 12, '暗杀（II）', 1, 0, 2, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (224, 12, '闪避精通（I）', 5, 0, 4, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (225, 12, '闪避精通（II）', 5, 0, 4, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (226, 12, '闪避精通（III）', 5, 0, 4, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (227, 12, '--------', 0, 1, 4, 4, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (229, 12, '回避强化（II）', 1, 0, 4, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (230, 13, '流星火强化（II）', 3, 0, 1, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (231, 13, '海神之怒强化（II）', 3, 0, 2, 4, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (233, 13, '地狱烈焰强化（II）', 3, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (234, 13, '-------', 1, 0, 3, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (235, 13, '狂暴强化（I）', 3, 0, 4, 5, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (236, 13, '狂暴强化（II）', 1, 0, 4, 6, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (237, 14, '攻击图腾强化', 3, 0, 1, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (238, 14, '防御图腾强化', 3, 0, 2, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (239, 14, '疾风图腾', 5, 0, 3, 4, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (240, 14, '疾风图腾强化', 3, 0, 3, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (241, 14, '图腾强化', 1, 0, 2, 6, 0, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (242, 12, '----------', 0, 1, 2, 3, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (243, 12, '-------', 0, 1, 3, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (244, 12, '疾行精通', 3, 0, 3, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (245, 10, '强化烈焰风暴（II）', 3, 0, 1, 5, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (246, 15, '宠物基本HP技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (247, 15, '宠物基本HP技能 2', 2, 0, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (248, 15, '宠物基本HP技能 3', 3, 0, 1, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (249, 15, '宠物基本HP技能 4', 2, 0, 3, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (250, 15, '宠物基本HP技能 5', 2, 0, 3, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (251, 15, '宠物基本HP技能 6', 3, 0, 3, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (252, 16, '宠物基本MP技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (253, 16, '宠物基本MP技能 2', 2, 0, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (254, 16, '宠物基本MP技能 3', 3, 0, 1, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (255, 16, '宠物基本MP技能 4', 2, 0, 3, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (256, 16, '宠物基本MP技能 5', 2, 0, 3, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (257, 16, '宠物基本MP技能 6', 3, 0, 3, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (258, 17, '宠物基本其他技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (259, 17, '宠物基本其他技能 2', 2, 0, 1, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (260, 17, '宠物基本其他技能 3', 3, 0, 1, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (261, 17, '宠物基本其他技能 4', 2, 0, 3, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (262, 17, '宠物基本其他技能 5', 2, 0, 3, 2, 1, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (263, 17, '宠物基本其他技能 6', 3, 0, 3, 3, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (264, 18, '宠物1阶HP型技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (265, 18, '宠物1阶HP型技能 2', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (266, 19, '宠物1阶HP型技能 3', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (267, 19, '宠物1阶HP型技能 4', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (268, 20, '宠物1阶MP型技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (269, 20, '宠物1阶MP型技能 2', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (270, 21, '宠物1阶MP型技能 3', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (271, 21, '宠物1阶MP型技能 4', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (272, 22, '宠物1阶其他型技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (273, 22, '宠物1阶其他型技能 2', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (274, 23, '宠物1阶其他型技能 3', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (275, 23, '宠物1阶其他型技能 4', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (276, 24, '宠物1阶HPMP型技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (277, 24, '宠物1阶HPMP型技能 2', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (278, 25, '宠物1阶HPMP型技能 3', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (279, 25, '宠物1阶HPMP型技能 4', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (280, 26, '宠物1阶HP其他型技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (281, 26, '宠物1阶HP其他型技能 2', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (282, 27, '宠物1阶HP其他型技能 3', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (283, 27, '宠物1阶HP其他型技能 4', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (284, 28, '宠物1阶MP其他型技能 1', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (285, 28, '宠物1阶MP其他型技能 2', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (286, 29, '宠物1阶MP其他型技能 3', 2, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (287, 29, '宠物1阶MP其他型技能 4', 2, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (288, 30, '2阶 HP技能 1', 3, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (289, 30, '2阶 HP技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (290, 31, '2阶 HP恢复技能 1', 3, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (291, 31, '2阶 HP恢复技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (292, 32, '2阶 MP技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (293, 32, '2阶 MP技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (294, 33, '2阶 MP恢复技能 2', 3, 0, 1, 1, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (295, 33, '2阶 MP恢复技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (296, 34, '2阶 负重技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (297, 34, '2阶 负重技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (298, 35, '2阶 变身技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (299, 35, '2阶 变身技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (300, 36, '2阶 HPMP技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (301, 36, '2阶 HPMP技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (302, 37, '2阶 HPMP恢复技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (303, 37, '2阶 HPMP恢复技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (304, 38, '2阶 HP负重技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (305, 38, '2阶 HP负重技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (306, 39, '2阶 HP恢复变身技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (307, 39, '2阶 HP恢复变身技能 2', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (308, 40, '2阶 MP负重技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (309, 40, '2阶 MP负重技能 1', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (310, 41, '2阶 MP恢复变身技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (311, 41, '2阶 MP恢复变身技能 1', 4, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (312, 42, '3阶 HP技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (313, 42, '3阶 HP技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (314, 43, '3阶 HP恢复技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (315, 43, '3阶 HP恢复技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (316, 44, '3阶 MP技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (317, 44, '3阶 MP技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (318, 45, '3阶 MP恢复技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (319, 45, '3阶 MP恢复技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (320, 46, '3阶 负重技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (321, 46, '3阶 负重技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (322, 47, '3阶 变身技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (323, 47, '3阶 变身技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (324, 48, '3阶 HPMP技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (325, 48, '3阶 HPMP技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (326, 49, '3阶 HPMP恢复技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (327, 49, '3阶 HPMP恢复技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (328, 50, '3阶 HP负重技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (329, 50, '3阶 HP负重技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (330, 51, '3阶 HP恢复变身技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (331, 51, '3阶 HP恢复变身技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (332, 52, '3阶 MP负重技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (333, 52, '3阶 MP负重技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (334, 53, '3阶 MP恢复变身技能 1', 3, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (335, 53, '3阶 MP恢复变身技能 2', 3, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (336, 13, '恢复变身技能
', 5, 0, 1, 3, 0, 0, 2, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (347, 54, '4阶HP技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (348, 54, '4阶HP技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (349, 55, '4阶HP恢复技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (350, 55, '4阶HP恢复技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (351, 56, '4阶MP技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (352, 56, '4阶MP技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (353, 57, '4阶MP恢复技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (354, 57, '4阶MP恢复技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (355, 58, '4阶负重技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (356, 58, '4阶负重技能1
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (357, 59, '4阶变身技能 1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (358, 59, '4阶变身技能 2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (359, 60, '4阶HPMP技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (360, 60, '4阶HPMP技能1
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (361, 61, '4阶HPMP恢复技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (362, 61, '4阶HPMP恢复技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (363, 62, '4阶HP负重技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (364, 62, '4阶HP负重技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (365, 63, '4阶HP恢复变身技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (366, 63, '4阶HP恢复变身技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (367, 64, '4阶MP负重技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (368, 64, '4阶MP负重技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (369, 65, '4阶MP恢复变身技能1
', 1, 0, 1, 1, 0, 0, 1, 0, 10000);
GO

INSERT INTO [dbo].[DT_SkillTreeNode] ([mSTNID], [mSTID], [mName], [mMaxLevel], [mNodeType], [mIconSlotX], [mIconSlotY], [mLineN], [mLineE], [mLineS], [mLineW], [mTermOfValidity]) VALUES (370, 65, '4阶MP恢复变身技能2
', 1, 0, 1, 2, 1, 0, 0, 0, 10000);
GO

