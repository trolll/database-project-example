/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblParmSvr
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblParmSvr] ([mRegDate], [mIsValid], [mDispOrder], [mSvrNo], [mType], [mMajorIp], [mMinorIp], [mWorldNo], [mMajorVer], [mMinorVer], [mThdWkCnt], [mThdTmCnt], [mThdDbCnt], [mThdLogCnt], [mSessionCnt], [mSendCnt], [mTcpPort], [mUdpPort], [mDesc], [mIsSiege], [mEvtStx], [mEvtEtx], [mIsCheckRSC], [mNationID], [mBullet], [mSupportType], [mLastSupportDate], [mSvrInfo], [mIsInputDlg], [mSmallSendCnt]) VALUES ('2007-12-21 16:18:38.830', 1, 1, 1, 0, '192.168.1.210  ', NULL, 0, 70309001, 2019082921, 100, 1, 10, 2, 4000, 60000, 11004, 11004, 'Channel server                                                                                      ', 0, '2000-01-01 00:00:00.000', '2000-01-01 00:00:00.000', 0, 1, 50000, 2, '2020-01-01 00:00:00.000', 0, 0, 40000);
GO

INSERT INTO [dbo].[TblParmSvr] ([mRegDate], [mIsValid], [mDispOrder], [mSvrNo], [mType], [mMajorIp], [mMinorIp], [mWorldNo], [mMajorVer], [mMinorVer], [mThdWkCnt], [mThdTmCnt], [mThdDbCnt], [mThdLogCnt], [mSessionCnt], [mSendCnt], [mTcpPort], [mUdpPort], [mDesc], [mIsSiege], [mEvtStx], [mEvtEtx], [mIsCheckRSC], [mNationID], [mBullet], [mSupportType], [mLastSupportDate], [mSvrInfo], [mIsInputDlg], [mSmallSendCnt]) VALUES ('2007-07-20 16:08:05.550', 1, 1, 999, 2, '192.168.1.210  ', NULL, 0, 61122001, 2019082921, 20, 1, 5, 2, 500, 60000, 11006, 11006, 'Manager server                                                                                      ', 0, '2007-07-20 16:08:05.000', '2007-07-20 16:08:05.000', 1, 1, 50000, 2, '2020-01-01 00:00:00.000', 0, 0, 40000);
GO

INSERT INTO [dbo].[TblParmSvr] ([mRegDate], [mIsValid], [mDispOrder], [mSvrNo], [mType], [mMajorIp], [mMinorIp], [mWorldNo], [mMajorVer], [mMinorVer], [mThdWkCnt], [mThdTmCnt], [mThdDbCnt], [mThdLogCnt], [mSessionCnt], [mSendCnt], [mTcpPort], [mUdpPort], [mDesc], [mIsSiege], [mEvtStx], [mEvtEtx], [mIsCheckRSC], [mNationID], [mBullet], [mSupportType], [mLastSupportDate], [mSvrInfo], [mIsInputDlg], [mSmallSendCnt]) VALUES ('2018-04-03 00:00:00.000', 1, 22, 1164, 1, '192.168.1.210  ', NULL, 1164, 70309002, 2019082921, 17, 1, 5, 3, 5000, 60000, 11005, 11005, 'Game 1164                                                                                           ', 1, '2008-01-31 10:00:00.000', '2008-02-14 10:00:00.000', 1, 2, 100000, 2, '2023-10-03 11:01:00.000', 0, 0, 40000);
GO

INSERT INTO [dbo].[TblParmSvr] ([mRegDate], [mIsValid], [mDispOrder], [mSvrNo], [mType], [mMajorIp], [mMinorIp], [mWorldNo], [mMajorVer], [mMinorVer], [mThdWkCnt], [mThdTmCnt], [mThdDbCnt], [mThdLogCnt], [mSessionCnt], [mSendCnt], [mTcpPort], [mUdpPort], [mDesc], [mIsSiege], [mEvtStx], [mEvtEtx], [mIsCheckRSC], [mNationID], [mBullet], [mSupportType], [mLastSupportDate], [mSvrInfo], [mIsInputDlg], [mSmallSendCnt]) VALUES ('2019-02-20 00:00:00.000', 0, 20, 1167, 1, '192.168.1.211  ', NULL, 1163, 70309002, 2019082921, 17, 1, 5, 3, 5000, 60000, 11005, 11005, 'Game 1167                                                                                           ', 1, '2008-01-31 10:00:00.000', '2008-02-14 10:00:00.000', 1, 2, 100000, 2, '2020-02-05 01:02:00.000', 0, 0, 40000);
GO

INSERT INTO [dbo].[TblParmSvr] ([mRegDate], [mIsValid], [mDispOrder], [mSvrNo], [mType], [mMajorIp], [mMinorIp], [mWorldNo], [mMajorVer], [mMinorVer], [mThdWkCnt], [mThdTmCnt], [mThdDbCnt], [mThdLogCnt], [mSessionCnt], [mSendCnt], [mTcpPort], [mUdpPort], [mDesc], [mIsSiege], [mEvtStx], [mEvtEtx], [mIsCheckRSC], [mNationID], [mBullet], [mSupportType], [mLastSupportDate], [mSvrInfo], [mIsInputDlg], [mSmallSendCnt]) VALUES ('2019-05-21 11:09:45.997', 0, 1, 3000, 1, '192.168.1.220  ', NULL, 3000, 70309002, 2019082921, 17, 1, 5, 3, 5000, 60000, 11005, 11005, 'Battle Server                                                                                       ', 0, '2008-01-31 10:00:00.000', '2008-02-14 10:00:00.000', 1, 2, 100000, 2, '2020-02-05 01:04:00.000', 1, 0, 40000);
GO

