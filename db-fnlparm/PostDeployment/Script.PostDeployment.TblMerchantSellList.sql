/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMerchantSellList
Date                  : 2023-10-07 09:09:23
*/


INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (60, 1, 355, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (45, 1, 356, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (52, 1, 357, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (7, 1, 358, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (6, 1, 359, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (129, 1, 360, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (44, 1, 362, 80, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (4, 1, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1, 1, 411, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (5, 1, 425, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2, 1, 714, 65, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (3, 1, 715, 230, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (124, 1, 721, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (125, 1, 725, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1518, 1, 937, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (497, 1, 1023, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1270, 1, 2802, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1271, 1, 2820, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1284, 1, 2835, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1581, 1, 4832, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1592, 1, 5247, 750, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1652, 1, 5687, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1658, 1, 5690, 850, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2368, 1, 8006, 100000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (9, 2, 41, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (11, 2, 61, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (10, 2, 101, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (146, 2, 153, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (602, 2, 213, 30000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (603, 2, 218, 30000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1210, 2, 231, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1203, 2, 232, 25000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1206, 2, 234, 30000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (601, 2, 236, 40000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (100, 2, 271, 5500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1207, 2, 542, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (391, 2, 779, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1637, 2, 783, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (181, 2, 796, 5500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1636, 2, 845, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1202, 2, 898, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1201, 2, 900, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1209, 2, 942, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1208, 2, 944, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1204, 2, 945, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1205, 2, 946, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (595, 2, 1248, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (596, 2, 1292, 5500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (600, 2, 1342, 50000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2059, 2, 1412, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2063, 2, 1422, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2060, 2, 1430, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2061, 2, 1442, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2062, 2, 1452, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1261, 2, 2691, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2064, 2, 2737, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1259, 2, 2747, 5500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1281, 2, 2827, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (164, 3, 417, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (182, 3, 418, 10, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (183, 3, 419, 10, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (165, 3, 420, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (184, 3, 421, 200, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (185, 3, 702, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (209, 3, 703, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (190, 3, 704, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (205, 3, 705, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (208, 3, 706, 15, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (207, 3, 828, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (194, 3, 829, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (193, 3, 830, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (192, 3, 831, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (195, 3, 832, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (196, 3, 833, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (197, 3, 834, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (186, 3, 835, 80, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (198, 3, 836, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (199, 3, 837, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (200, 3, 838, 50, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (189, 3, 839, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (202, 3, 840, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (203, 3, 841, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (204, 3, 842, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (405, 3, 915, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (406, 3, 916, 30, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (498, 3, 1024, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (501, 3, 1025, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1214, 3, 2497, 50, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1213, 3, 2498, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1215, 3, 2499, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1216, 3, 2500, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1217, 3, 2501, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1218, 3, 2502, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1219, 3, 2503, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1220, 3, 2504, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1221, 3, 2505, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1222, 3, 2506, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1223, 3, 2507, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1224, 3, 2508, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1225, 3, 2509, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1226, 3, 2510, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1227, 3, 2511, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1228, 3, 2512, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1229, 3, 2513, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1230, 3, 2514, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1231, 3, 2515, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1232, 3, 2516, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1233, 3, 2517, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1234, 3, 2518, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1235, 3, 2519, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1236, 3, 2520, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1237, 3, 2521, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1238, 3, 2522, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1239, 3, 2523, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1240, 3, 2524, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1241, 3, 2525, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1242, 3, 2526, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1243, 3, 2527, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1244, 3, 2528, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1245, 3, 2529, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1246, 3, 2530, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1247, 3, 2531, 200, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1248, 3, 2532, 200, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1249, 3, 2533, 200, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1250, 3, 2534, 200, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1520, 3, 3430, 70, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1521, 3, 3431, 100, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1522, 3, 3432, 120, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1523, 3, 3433, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1519, 3, 3434, 50, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1531, 3, 4446, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1532, 3, 4447, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1533, 3, 4448, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1534, 3, 4449, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1535, 3, 4450, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1536, 3, 4451, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1537, 3, 4452, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1538, 3, 4453, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1539, 3, 4454, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1540, 3, 4455, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1541, 3, 4456, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1542, 3, 4457, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1543, 3, 4459, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1544, 3, 4460, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1545, 3, 4461, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1546, 3, 4462, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1547, 3, 4479, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1548, 3, 4480, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1549, 3, 4481, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1550, 3, 4482, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1551, 3, 4483, 50, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1552, 3, 4484, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1553, 3, 4485, 150, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1554, 3, 4486, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1555, 3, 4487, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1556, 3, 4488, 300, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1557, 3, 4489, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1558, 3, 4490, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1559, 3, 4491, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1560, 3, 4492, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1561, 3, 4508, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1562, 3, 4509, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1563, 3, 4510, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1564, 3, 4511, 20, 2, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (169, 12, 411, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (170, 12, 778, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (230, 13, 355, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (232, 13, 356, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (234, 13, 357, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (233, 13, 360, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (242, 13, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (235, 13, 425, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (366, 13, 865, 5, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (971, 13, 1065, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (972, 13, 1069, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (590, 13, 1164, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (973, 13, 1241, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1092, 13, 2053, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1269, 13, 2601, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1280, 13, 2826, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (214, 14, 41, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (219, 14, 61, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (218, 14, 101, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (220, 14, 153, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (217, 14, 778, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (392, 14, 779, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (215, 14, 780, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1635, 14, 783, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (388, 14, 794, 1400, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1634, 14, 845, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (283, 15, 41, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (365, 15, 58, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (310, 15, 60, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (307, 15, 61, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (309, 15, 62, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (311, 15, 63, 80000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (312, 15, 71, 60000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (412, 15, 82, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (362, 15, 101, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (361, 15, 119, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (757, 15, 130, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (284, 15, 148, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (285, 15, 149, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (278, 15, 154, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (277, 15, 155, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (786, 15, 164, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (800, 15, 166, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (805, 15, 167, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (785, 15, 168, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (808, 15, 169, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (803, 15, 173, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (797, 15, 175, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (781, 15, 176, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (795, 15, 177, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (784, 15, 179, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (783, 15, 183, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (363, 15, 187, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (812, 15, 188, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (819, 15, 189, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (813, 15, 192, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (816, 15, 197, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (818, 15, 199, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (815, 15, 201, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (817, 15, 206, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (899, 15, 210, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (315, 15, 212, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (605, 15, 213, 15000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (754, 15, 216, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (313, 15, 217, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (318, 15, 218, 15000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (753, 15, 220, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (317, 15, 221, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (314, 15, 223, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (321, 15, 228, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (322, 15, 229, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (323, 15, 231, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (324, 15, 232, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (320, 15, 233, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (325, 15, 234, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (326, 15, 236, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (755, 15, 242, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (486, 15, 253, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (821, 15, 254, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (826, 15, 255, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (824, 15, 256, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (825, 15, 257, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (820, 15, 258, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (827, 15, 259, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (289, 15, 261, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (286, 15, 271, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (293, 15, 281, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (294, 15, 291, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (295, 15, 301, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (292, 15, 302, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (290, 15, 312, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (291, 15, 322, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (282, 15, 323, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (280, 15, 324, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (281, 15, 325, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (288, 15, 326, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (902, 15, 327, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (743, 15, 328, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (898, 15, 371, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (900, 15, 465, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (806, 15, 498, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (796, 15, 501, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (308, 15, 542, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (316, 15, 555, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (319, 15, 608, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (823, 15, 713, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (279, 15, 716, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (483, 15, 717, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (485, 15, 718, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (897, 15, 727, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (822, 15, 739, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (814, 15, 740, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (810, 15, 741, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (303, 15, 757, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (301, 15, 767, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (484, 15, 782, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (305, 15, 784, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (398, 15, 794, 700, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (304, 15, 795, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (302, 15, 796, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (306, 15, 797, 75000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (393, 15, 898, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (364, 15, 899, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (394, 15, 900, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (426, 15, 913, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (434, 15, 938, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (749, 15, 939, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (479, 15, 940, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (747, 15, 941, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (427, 15, 942, 250, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (748, 15, 943, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (431, 15, 944, 250, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (432, 15, 945, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (429, 15, 946, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (811, 15, 949, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (779, 15, 950, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (759, 15, 977, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (774, 15, 978, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (760, 15, 988, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (478, 15, 1003, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (565, 15, 1006, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (804, 15, 1072, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (791, 15, 1073, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (787, 15, 1074, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (802, 15, 1075, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (809, 15, 1076, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (789, 15, 1077, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (794, 15, 1078, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (799, 15, 1079, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (792, 15, 1080, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (780, 15, 1082, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (793, 15, 1084, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (764, 15, 1096, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (775, 15, 1112, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (768, 15, 1132, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (769, 15, 1133, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (771, 15, 1134, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (761, 15, 1176, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (730, 15, 1180, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (737, 15, 1181, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (729, 15, 1182, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (731, 15, 1183, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (738, 15, 1184, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (728, 15, 1185, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (732, 15, 1186, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (739, 15, 1187, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (896, 15, 1188, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (725, 15, 1189, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (746, 15, 1190, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (745, 15, 1191, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (734, 15, 1192, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (727, 15, 1193, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (724, 15, 1194, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (733, 15, 1195, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (726, 15, 1196, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (723, 15, 1197, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (837, 15, 1221, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (833, 15, 1222, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (832, 15, 1223, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (835, 15, 1224, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (829, 15, 1225, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (828, 15, 1226, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (836, 15, 1227, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (831, 15, 1228, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (830, 15, 1229, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (834, 15, 1230, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (773, 15, 1231, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (606, 15, 1248, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (776, 15, 1258, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (770, 15, 1268, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (766, 15, 1278, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (607, 15, 1292, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (767, 15, 1302, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (778, 15, 1312, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (772, 15, 1322, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (758, 15, 1332, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (604, 15, 1342, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (756, 15, 1352, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (777, 15, 1364, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (765, 15, 1374, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (903, 15, 1541, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (904, 15, 1551, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (905, 15, 1561, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (906, 15, 1571, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1260, 15, 2747, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (399, 16, 355, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (400, 16, 356, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (477, 16, 357, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (403, 16, 358, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (402, 16, 359, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (401, 16, 360, 25, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (420, 16, 362, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (328, 16, 411, 12, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (327, 16, 425, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (355, 16, 489, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (356, 16, 490, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (329, 16, 491, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (435, 16, 492, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (357, 16, 494, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (419, 16, 495, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (358, 16, 496, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (872, 16, 708, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (480, 16, 714, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (481, 16, 715, 110, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (354, 16, 721, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (351, 16, 722, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (352, 16, 724, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (353, 16, 725, 2, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (422, 16, 737, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (499, 16, 738, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (482, 16, 777, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (338, 16, 820, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (421, 16, 860, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (474, 16, 869, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (875, 16, 902, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (475, 16, 910, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (476, 16, 911, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (503, 16, 1023, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (890, 16, 1288, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (891, 16, 1289, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (892, 16, 1290, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (893, 16, 1291, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (860, 16, 1362, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (861, 16, 1363, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1276, 16, 2820, 60, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1287, 16, 2835, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1582, 16, 4832, 240, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1593, 16, 5247, 375, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (516, 17, 411, 38, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (517, 17, 714, 98, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (518, 17, 715, 368, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1577, 17, 4832, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1584, 17, 5247, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1659, 17, 5690, 1560, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (519, 18, 1033, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (560, 19, 918, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (561, 19, 919, 400, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (562, 19, 920, 700, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (563, 19, 921, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (564, 19, 922, 1500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (597, 19, 923, 800, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (598, 19, 924, 800, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (599, 19, 925, 800, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (613, 20, 1288, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (614, 20, 1289, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (615, 20, 1290, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (616, 20, 1291, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1585, 20, 5243, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1586, 20, 5244, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1587, 20, 5245, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1588, 20, 5246, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1626, 20, 5515, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1627, 20, 5516, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1628, 20, 5519, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1629, 20, 5520, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (580, 21, 1055, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (581, 21, 1057, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (582, 21, 1131, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1279, 21, 2822, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (583, 22, 1056, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (584, 22, 1058, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (585, 22, 1130, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1278, 22, 2821, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (592, 23, 1050, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (593, 23, 1051, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (594, 23, 1053, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (976, 23, 2047, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (977, 23, 2048, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (978, 23, 2049, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (919, 24, 1390, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (920, 24, 1391, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (921, 24, 1392, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (911, 24, 1403, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (912, 24, 1407, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (907, 24, 1855, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (913, 24, 1867, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2233, 24, 7035, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (619, 25, 1397, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (620, 26, 1395, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (621, 27, 1396, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (622, 28, 1398, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (623, 29, 1394, 600, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (624, 30, 1392, 600, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (625, 31, 1402, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (626, 32, 1404, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (627, 33, 1403, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (628, 34, 1400, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (629, 35, 1401, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (631, 36, 1405, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (632, 37, 1408, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (633, 37, 1409, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (634, 38, 1406, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (635, 39, 1407, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (636, 40, 1410, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (916, 41, 1833, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (915, 41, 1838, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (917, 41, 1857, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (918, 41, 1860, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (914, 41, 1863, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2234, 41, 7036, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (637, 42, 1386, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (638, 42, 1387, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (640, 42, 1388, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (641, 42, 1389, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (642, 42, 1390, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (643, 42, 1391, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (644, 42, 1392, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (639, 42, 1399, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (645, 43, 1386, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (646, 43, 1387, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (648, 43, 1388, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (649, 43, 1389, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (650, 43, 1390, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (651, 43, 1391, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (652, 43, 1392, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (647, 43, 1399, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (653, 44, 1386, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (654, 44, 1387, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (656, 44, 1388, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (657, 44, 1389, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (658, 44, 1390, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (659, 44, 1391, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (660, 44, 1392, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (655, 44, 1399, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (661, 45, 1386, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (662, 45, 1387, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (664, 45, 1388, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (665, 45, 1389, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (666, 45, 1390, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (667, 45, 1391, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (668, 45, 1392, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (663, 45, 1399, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (669, 46, 355, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (670, 46, 356, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (671, 46, 357, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (672, 46, 358, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (673, 46, 359, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (674, 46, 360, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (675, 46, 362, 80, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (676, 46, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (677, 46, 411, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (679, 46, 425, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (680, 46, 714, 65, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (681, 46, 715, 230, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (682, 46, 721, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (683, 46, 725, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (689, 46, 1023, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (718, 46, 1481, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1273, 46, 2820, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1285, 46, 2835, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1578, 46, 4832, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1589, 46, 5247, 750, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1684, 46, 5687, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1657, 46, 5690, 850, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2369, 46, 8006, 100000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (719, 47, 1508, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (721, 47, 1509, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (720, 47, 1510, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1211, 47, 2443, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1212, 47, 2444, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1649, 47, 5577, 4000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1650, 47, 5578, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1671, 47, 5694, 6000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (928, 48, 101, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (933, 48, 213, 30000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (931, 48, 218, 30000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (934, 48, 236, 40000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (929, 48, 271, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (937, 48, 779, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1641, 48, 783, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1640, 48, 845, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (930, 48, 1342, 50000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1282, 48, 2691, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1283, 48, 2827, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (962, 49, 355, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (963, 49, 356, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (966, 49, 357, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (960, 49, 358, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (932, 49, 359, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (964, 49, 360, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (961, 49, 425, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (968, 49, 428, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (969, 49, 429, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (967, 49, 708, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (974, 49, 714, 156, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (965, 49, 715, 596, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1275, 49, 2820, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1579, 49, 4832, 1295, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1590, 49, 5247, 1940, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1655, 49, 5690, 2587, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (942, 50, 355, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (944, 50, 356, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (970, 50, 357, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (958, 50, 358, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (959, 50, 359, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (946, 50, 360, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (952, 50, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (940, 50, 425, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (953, 50, 428, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (954, 50, 429, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (955, 50, 708, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (956, 50, 714, 98, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (957, 50, 715, 368, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (941, 50, 721, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (947, 50, 1023, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (948, 50, 1288, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (949, 50, 1289, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (950, 50, 1290, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (951, 50, 1291, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1274, 50, 2820, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1286, 50, 2835, 23000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1580, 50, 4832, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1619, 50, 5243, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1620, 50, 5244, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1621, 50, 5245, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1622, 50, 5246, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1591, 50, 5247, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1630, 50, 5515, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1631, 50, 5516, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1632, 50, 5519, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1633, 50, 5520, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1653, 50, 5687, 15, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1656, 50, 5690, 1250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2370, 50, 8006, 100000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (979, 51, 355, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (980, 51, 356, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (981, 51, 357, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (982, 51, 358, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (983, 51, 359, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (984, 51, 360, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (985, 51, 362, 80, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (986, 51, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (987, 51, 411, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (988, 51, 417, 400, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (989, 51, 425, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (990, 51, 714, 65, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (992, 51, 721, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (993, 51, 725, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (994, 51, 839, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (995, 51, 915, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (996, 51, 937, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (997, 51, 972, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (998, 51, 1001, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (999, 51, 1023, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1000, 51, 1065, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1001, 51, 1066, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1002, 51, 1067, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1003, 51, 1068, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1004, 51, 1069, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1005, 51, 1070, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1006, 51, 1071, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1007, 51, 1088, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1008, 51, 1091, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1009, 51, 1092, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1010, 51, 1093, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1011, 51, 1094, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1012, 51, 1241, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1013, 51, 1242, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1014, 51, 1243, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1015, 51, 1244, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1016, 51, 1245, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1017, 51, 1247, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1018, 51, 2051, 230, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1277, 51, 2820, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1020, 52, 1065, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1060, 52, 1066, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1061, 52, 1067, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1062, 52, 1068, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1063, 52, 1069, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1065, 52, 1070, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1064, 52, 1071, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1069, 52, 1091, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1067, 52, 1092, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1068, 52, 1093, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1066, 52, 1094, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1070, 52, 1241, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1071, 52, 1242, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1072, 52, 1243, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1073, 52, 1244, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1074, 52, 1245, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1075, 52, 1247, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1262, 52, 2601, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1263, 52, 2602, 10000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1264, 52, 2603, 5000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1265, 52, 2604, 20000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1022, 53, 709, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1031, 53, 799, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1021, 53, 800, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1030, 53, 802, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1025, 53, 819, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1026, 53, 847, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1032, 53, 901, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1037, 53, 937, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1036, 53, 957, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1033, 53, 958, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1027, 53, 970, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1028, 53, 971, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1024, 53, 972, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1034, 53, 975, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1035, 53, 976, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1029, 53, 1000, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1023, 53, 1001, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1038, 53, 1088, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1043, 54, 709, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1139, 54, 710, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1159, 54, 711, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1093, 54, 712, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1137, 54, 748, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1130, 54, 750, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1132, 54, 751, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1131, 54, 752, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1094, 54, 798, 15, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1051, 54, 799, 20, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1039, 54, 800, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1052, 54, 802, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1095, 54, 803, 15, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1096, 54, 806, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1136, 54, 807, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1148, 54, 808, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1133, 54, 809, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1157, 54, 810, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1140, 54, 811, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1147, 54, 812, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1041, 54, 813, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1042, 54, 817, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1046, 54, 819, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1134, 54, 822, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1152, 54, 826, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1102, 54, 827, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1047, 54, 847, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1149, 54, 848, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1156, 54, 849, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1129, 54, 850, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1150, 54, 851, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1151, 54, 852, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1158, 54, 853, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1141, 54, 854, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1153, 54, 855, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1053, 54, 901, 150, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1097, 54, 903, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1098, 54, 904, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1138, 54, 905, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1099, 54, 907, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1100, 54, 909, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1101, 54, 912, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1058, 54, 937, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1119, 54, 951, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1120, 54, 952, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1121, 54, 953, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1122, 54, 954, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1123, 54, 955, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1124, 54, 956, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1057, 54, 957, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1054, 54, 958, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1103, 54, 959, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1104, 54, 960, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1105, 54, 961, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1106, 54, 962, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1107, 54, 963, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1108, 54, 964, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1109, 54, 965, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1110, 54, 966, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1111, 54, 967, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1112, 54, 968, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1113, 54, 969, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1048, 54, 970, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1049, 54, 971, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1045, 54, 972, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1114, 54, 973, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1115, 54, 974, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1055, 54, 975, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1056, 54, 976, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1116, 54, 999, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1050, 54, 1000, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1044, 54, 1001, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1117, 54, 1002, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1118, 54, 1004, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1125, 54, 1010, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1135, 54, 1034, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1126, 54, 1048, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1127, 54, 1049, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1059, 54, 1088, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1128, 54, 1170, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1146, 54, 1212, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1144, 54, 1213, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1143, 54, 1214, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1142, 54, 1215, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1145, 54, 1216, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1154, 54, 1477, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1155, 54, 1478, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1076, 55, 2053, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1077, 55, 2054, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1078, 55, 2055, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1079, 55, 2056, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1080, 55, 2057, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1081, 55, 2058, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1082, 55, 2059, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1083, 55, 2060, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1084, 55, 2061, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1085, 55, 2062, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1086, 55, 2063, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1087, 55, 2064, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1088, 55, 2065, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1089, 55, 2066, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1090, 55, 2067, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1091, 55, 2068, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1268, 55, 2795, 3500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1160, 56, 417, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1174, 56, 418, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1173, 56, 419, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1172, 56, 420, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1182, 56, 421, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1180, 56, 702, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1179, 56, 703, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1177, 56, 704, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1186, 56, 705, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1171, 56, 706, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1176, 56, 828, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1162, 56, 829, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1185, 56, 830, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1170, 56, 831, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1163, 56, 832, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1167, 56, 833, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1178, 56, 834, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1165, 56, 835, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1190, 56, 836, 700, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1166, 56, 837, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1181, 56, 838, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1164, 56, 839, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1168, 56, 840, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1169, 56, 841, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1175, 56, 842, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1189, 56, 915, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1191, 56, 916, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1183, 56, 1024, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1184, 56, 1025, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1187, 56, 1122, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1188, 56, 1246, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1524, 56, 3434, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1192, 57, 1067, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1193, 57, 1068, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1194, 57, 1070, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1197, 57, 1091, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1196, 57, 1093, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1195, 57, 1094, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1198, 57, 1244, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1199, 57, 1245, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1200, 57, 1247, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1288, 57, 2602, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1267, 57, 2604, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1251, 58, 2564, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1252, 58, 2565, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1253, 58, 2566, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1254, 59, 2568, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1255, 59, 2569, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1257, 60, 2573, 1100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1256, 60, 2574, 1100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1258, 60, 2575, 1100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1295, 61, 355, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1296, 61, 356, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1294, 61, 357, 150, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1293, 61, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1289, 61, 2842, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1290, 61, 2843, 65, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1292, 61, 2845, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1468, 61, 4127, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1651, 61, 5687, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1321, 62, 3300, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1401, 62, 3304, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1402, 62, 3305, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1403, 62, 3306, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1404, 62, 3307, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1405, 62, 3308, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1406, 62, 3309, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1407, 62, 3310, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1425, 63, 751, 45, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1431, 63, 951, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1424, 63, 952, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1432, 63, 954, 5, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1426, 63, 955, 5, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1429, 63, 966, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1428, 63, 967, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1427, 63, 968, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1430, 63, 1004, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1420, 63, 2305, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1423, 63, 2315, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1421, 63, 2325, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1422, 63, 2335, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1433, 63, 2669, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1304, 63, 3269, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1305, 63, 3270, 40, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1308, 63, 3275, 40, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1311, 63, 3280, 40, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1314, 63, 3285, 40, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1317, 63, 3290, 40, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1320, 63, 3295, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1513, 63, 3296, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1434, 63, 3961, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1435, 63, 3962, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1436, 63, 3963, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1437, 63, 3964, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1438, 63, 3965, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1439, 63, 3966, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1440, 63, 3967, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1441, 63, 3968, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1442, 63, 3969, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1503, 63, 4060, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1504, 63, 4070, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1508, 63, 4075, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1509, 63, 4082, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1506, 63, 4087, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1507, 63, 4094, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1510, 63, 4099, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1511, 63, 4106, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1512, 63, 4111, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1505, 63, 4118, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1623, 63, 5399, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1624, 63, 5400, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1324, 64, 2944, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1325, 64, 2947, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1327, 64, 2955, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1328, 64, 2958, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1330, 64, 2966, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1331, 64, 2969, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1333, 64, 2977, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1334, 64, 2980, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1336, 64, 2988, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1337, 64, 2991, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1339, 64, 3020, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1340, 64, 3023, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1342, 64, 3031, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1343, 64, 3034, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1345, 64, 3042, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1346, 64, 3045, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1348, 64, 3053, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1349, 64, 3056, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1351, 64, 3064, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1352, 64, 3067, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1354, 64, 3075, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1355, 64, 3078, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1357, 64, 3086, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1358, 64, 3089, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1396, 64, 3097, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1397, 64, 3100, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1399, 64, 3108, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1400, 64, 3111, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1360, 64, 3119, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1361, 64, 3122, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1363, 64, 3130, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1364, 64, 3133, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1366, 64, 3141, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1367, 64, 3144, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1369, 64, 3152, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1370, 64, 3155, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1372, 64, 3163, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1373, 64, 3166, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1375, 64, 3174, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1376, 64, 3177, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1378, 64, 3207, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1379, 64, 3210, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1381, 64, 3218, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1382, 64, 3221, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1384, 64, 3229, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1385, 64, 3232, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1387, 64, 3240, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1388, 64, 3243, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1408, 65, 3346, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1410, 65, 3347, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1409, 65, 3348, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1411, 65, 3349, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1413, 65, 3351, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1452, 65, 3973, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1453, 65, 3974, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1455, 65, 3975, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1454, 65, 3976, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1514, 65, 4014, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1515, 65, 4015, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1516, 65, 4016, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1517, 65, 4017, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1525, 65, 4365, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1526, 65, 4366, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1625, 66, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1417, 66, 411, 37, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1418, 66, 714, 97, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1419, 66, 715, 345, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1638, 66, 779, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1639, 66, 2691, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1680, 67, 5898, 450, 1, 44, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1681, 67, 5899, 700, 1, 45, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1682, 67, 5900, 1200, 1, 46, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1675, 67, 5908, 40, 1, 50, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1676, 67, 5909, 40, 1, 52, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1677, 67, 5910, 40, 1, 51, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1678, 67, 5911, 40, 1, 53, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1683, 67, 6231, 700, 1, 59, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2070, 67, 6331, 500, 1, 36, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2071, 67, 6332, 500, 1, 38, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2260, 67, 7291, 1000, 1, 5, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2261, 67, 7292, 2000, 1, 6, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2262, 67, 7293, 5000, 1, 7, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1463, 68, 355, 30, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1462, 68, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1458, 68, 2842, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1459, 68, 2843, 65, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1493, 68, 2845, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1461, 68, 4125, 150, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1464, 68, 4126, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1465, 68, 4127, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1466, 68, 4128, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1473, 69, 355, 27, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1472, 69, 403, 9, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1467, 69, 2842, 23, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1469, 69, 2843, 59, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1492, 69, 2845, 450, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1471, 69, 4125, 135, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1495, 69, 4126, 225, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1475, 69, 4128, 900, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1481, 70, 355, 26, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1480, 70, 403, 8, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1476, 70, 2842, 22, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1477, 70, 2843, 56, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1494, 70, 2845, 425, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1479, 70, 4125, 128, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1482, 70, 4126, 213, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1483, 70, 4128, 850, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1484, 71, 355, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1488, 71, 356, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1490, 71, 357, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1485, 71, 358, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1486, 71, 359, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1489, 71, 425, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1491, 71, 428, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1496, 71, 429, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1497, 71, 708, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1487, 71, 715, 600, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1576, 71, 4832, 1300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1583, 71, 5247, 1950, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1654, 71, 5690, 2590, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1608, 72, 1390, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1609, 72, 1391, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1610, 72, 1392, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1605, 72, 1403, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1606, 72, 1407, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1604, 72, 1855, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1607, 72, 1867, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2330, 72, 7036, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2328, 72, 7514, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1613, 73, 1833, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1612, 73, 1838, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1614, 73, 1857, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1615, 73, 1860, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1611, 73, 1863, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2329, 73, 7515, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1602, 74, 355, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1603, 74, 356, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1601, 74, 358, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1600, 74, 359, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1594, 74, 411, 25, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1599, 74, 425, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1595, 74, 714, 65, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1596, 74, 715, 230, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1597, 74, 4832, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1598, 74, 5247, 750, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1660, 74, 5690, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1664, 75, 1508, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1666, 75, 1509, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1665, 75, 1510, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1667, 75, 2443, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1668, 75, 2444, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1669, 75, 5577, 4000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1670, 75, 5578, 3000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1795, 76, 41, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1837, 76, 58, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1816, 76, 60, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1813, 76, 61, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1815, 76, 62, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1817, 76, 63, 80000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1818, 76, 71, 60000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1841, 76, 82, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1834, 76, 101, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1833, 76, 119, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1884, 76, 130, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1796, 76, 148, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1797, 76, 149, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1790, 76, 154, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1789, 76, 155, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1911, 76, 164, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1925, 76, 166, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1930, 76, 167, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1910, 76, 168, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1932, 76, 169, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1928, 76, 173, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1922, 76, 175, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1906, 76, 176, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1920, 76, 177, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1909, 76, 179, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1908, 76, 183, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1835, 76, 187, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1936, 76, 188, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1943, 76, 189, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1937, 76, 192, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1940, 76, 197, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1942, 76, 199, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1939, 76, 201, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1941, 76, 206, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1964, 76, 210, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1821, 76, 212, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1856, 76, 213, 15000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1881, 76, 216, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1819, 76, 217, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1824, 76, 218, 15000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1880, 76, 220, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1823, 76, 221, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1820, 76, 223, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1827, 76, 228, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1828, 76, 229, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1829, 76, 231, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1830, 76, 232, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2067, 76, 233, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1831, 76, 234, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1832, 76, 236, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1882, 76, 242, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1853, 76, 253, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1945, 76, 254, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1950, 76, 255, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1948, 76, 256, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1949, 76, 257, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1944, 76, 258, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1951, 76, 259, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1800, 76, 261, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1798, 76, 271, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1804, 76, 281, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1805, 76, 291, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1806, 76, 301, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1803, 76, 302, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1801, 76, 312, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1802, 76, 322, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1794, 76, 323, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2066, 76, 324, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1793, 76, 325, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1799, 76, 326, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1966, 76, 327, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1874, 76, 328, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1748, 76, 355, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1749, 76, 356, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1761, 76, 357, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1752, 76, 358, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1751, 76, 359, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1750, 76, 360, 25, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1754, 76, 362, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1963, 76, 371, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1737, 76, 411, 12, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1685, 76, 417, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1698, 76, 418, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1697, 76, 419, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1696, 76, 420, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1706, 76, 421, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1736, 76, 425, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1965, 76, 465, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1744, 76, 489, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1745, 76, 490, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1738, 76, 491, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1757, 76, 492, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1746, 76, 494, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1753, 76, 495, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1747, 76, 496, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1921, 76, 501, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1814, 76, 542, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1822, 76, 555, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1825, 76, 608, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1704, 76, 702, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1703, 76, 703, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1701, 76, 704, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1710, 76, 705, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1695, 76, 706, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1769, 76, 708, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1975, 76, 709, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2038, 76, 710, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2058, 76, 711, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1992, 76, 712, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1947, 76, 713, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1762, 76, 714, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1763, 76, 715, 110, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1791, 76, 716, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1850, 76, 717, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1852, 76, 718, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1743, 76, 721, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1740, 76, 722, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1741, 76, 724, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1742, 76, 725, 2, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1962, 76, 727, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1756, 76, 737, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1765, 76, 738, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1946, 76, 739, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1938, 76, 740, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1934, 76, 741, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2069, 76, 748, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2029, 76, 750, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2031, 76, 751, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2030, 76, 752, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1809, 76, 757, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1807, 76, 767, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1764, 76, 777, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1851, 76, 782, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1811, 76, 784, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1840, 76, 794, 700, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1810, 76, 795, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1808, 76, 796, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1812, 76, 797, 75000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1993, 76, 798, 15, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1983, 76, 799, 20, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1972, 76, 800, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1984, 76, 802, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1994, 76, 803, 15, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1995, 76, 806, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2035, 76, 807, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2047, 76, 808, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2032, 76, 809, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2056, 76, 810, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2039, 76, 811, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2046, 76, 812, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1973, 76, 813, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1974, 76, 817, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1978, 76, 819, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1739, 76, 820, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2033, 76, 822, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2051, 76, 826, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2001, 76, 827, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1700, 76, 828, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1686, 76, 829, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1709, 76, 830, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1694, 76, 831, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1687, 76, 832, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1691, 76, 833, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1702, 76, 834, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1689, 76, 835, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1714, 76, 836, 700, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1690, 76, 837, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1705, 76, 838, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1688, 76, 839, 40, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1692, 76, 840, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1693, 76, 841, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1699, 76, 842, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1979, 76, 847, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2048, 76, 848, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2055, 76, 849, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2028, 76, 850, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2049, 76, 851, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2050, 76, 852, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2057, 76, 853, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2040, 76, 854, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2052, 76, 855, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1755, 76, 860, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1758, 76, 869, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1838, 76, 898, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1836, 76, 899, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1839, 76, 900, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1985, 76, 901, 150, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1770, 76, 902, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1996, 76, 903, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1997, 76, 904, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2037, 76, 905, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1998, 76, 907, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1999, 76, 909, 13, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1759, 76, 910, 300, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1760, 76, 911, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2000, 76, 912, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1842, 76, 913, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1713, 76, 915, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1715, 76, 916, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1728, 76, 918, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1729, 76, 919, 400, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1730, 76, 920, 700, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1731, 76, 921, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1732, 76, 922, 1500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1733, 76, 923, 800, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1734, 76, 924, 800, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1735, 76, 925, 800, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1990, 76, 937, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1847, 76, 938, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1879, 76, 939, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1849, 76, 940, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1877, 76, 941, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1843, 76, 942, 250, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1878, 76, 943, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1845, 76, 944, 250, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1846, 76, 945, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1844, 76, 946, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1935, 76, 949, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1904, 76, 950, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2018, 76, 951, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2019, 76, 952, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2020, 76, 953, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2021, 76, 954, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2022, 76, 955, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2023, 76, 956, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1989, 76, 957, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1986, 76, 958, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2002, 76, 959, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2003, 76, 960, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2004, 76, 961, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2005, 76, 962, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2006, 76, 963, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2007, 76, 964, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2008, 76, 965, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2009, 76, 966, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2010, 76, 967, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2011, 76, 968, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2012, 76, 969, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1980, 76, 970, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1981, 76, 971, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1977, 76, 972, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2013, 76, 973, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2014, 76, 974, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1987, 76, 975, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1988, 76, 976, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1886, 76, 977, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1899, 76, 978, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1887, 76, 988, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2015, 76, 999, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1982, 76, 1000, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1976, 76, 1001, 30, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2016, 76, 1002, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1848, 76, 1003, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2017, 76, 1004, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1854, 76, 1006, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2024, 76, 1010, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1766, 76, 1023, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1707, 76, 1024, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1708, 76, 1025, 500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2034, 76, 1034, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2025, 76, 1048, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2026, 76, 1049, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1717, 76, 1067, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1718, 76, 1068, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1719, 76, 1070, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2068, 76, 1072, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1916, 76, 1073, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1912, 76, 1074, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1927, 76, 1075, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1933, 76, 1076, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1914, 76, 1077, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1919, 76, 1078, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1924, 76, 1079, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1917, 76, 1080, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1905, 76, 1082, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1918, 76, 1084, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1991, 76, 1088, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2065, 76, 1091, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1721, 76, 1093, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1720, 76, 1094, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1889, 76, 1096, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1900, 76, 1112, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1711, 76, 1122, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1893, 76, 1132, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1894, 76, 1133, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1896, 76, 1134, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2027, 76, 1170, 10, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1888, 76, 1176, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1866, 76, 1180, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1871, 76, 1181, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1865, 76, 1182, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1867, 76, 1183, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1872, 76, 1184, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1864, 76, 1185, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1868, 76, 1186, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1873, 76, 1187, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1961, 76, 1188, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1861, 76, 1189, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1876, 76, 1190, 35000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1875, 76, 1191, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1870, 76, 1192, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1863, 76, 1193, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1860, 76, 1194, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1869, 76, 1195, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1862, 76, 1196, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1859, 76, 1197, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2045, 76, 1212, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2043, 76, 1213, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2042, 76, 1214, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2041, 76, 1215, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2044, 76, 1216, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1957, 76, 1222, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1956, 76, 1223, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1959, 76, 1224, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1953, 76, 1225, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1952, 76, 1226, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1960, 76, 1227, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1955, 76, 1228, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1954, 76, 1229, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1958, 76, 1230, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1898, 76, 1231, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1723, 76, 1244, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1724, 76, 1245, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1712, 76, 1246, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1725, 76, 1247, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1857, 76, 1248, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1901, 76, 1258, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1895, 76, 1268, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1891, 76, 1278, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1771, 76, 1288, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1772, 76, 1289, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1773, 76, 1290, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1774, 76, 1291, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1858, 76, 1292, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1892, 76, 1302, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1903, 76, 1312, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1897, 76, 1322, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1885, 76, 1332, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1855, 76, 1342, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1883, 76, 1352, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1767, 76, 1362, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1768, 76, 1363, 1000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1902, 76, 1364, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1890, 76, 1374, 100000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2053, 76, 1477, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2054, 76, 1478, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1967, 76, 1541, 40000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1968, 76, 1551, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1969, 76, 1561, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1970, 76, 1571, 25000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1727, 76, 2602, 3000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1726, 76, 2604, 6000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2230, 76, 2615, 50, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1971, 76, 2747, 2500, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1775, 76, 2820, 60, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1776, 76, 2835, 200, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1716, 76, 3433, 2000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1777, 76, 4832, 240, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1779, 76, 5243, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1780, 76, 5244, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1781, 76, 5245, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1782, 76, 5246, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1778, 76, 5247, 375, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1783, 76, 5515, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1784, 76, 5516, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1785, 76, 5517, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1786, 76, 5518, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1787, 76, 5519, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (1788, 76, 5520, 100, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2074, 76, 6340, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2075, 76, 6341, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2076, 76, 6342, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2077, 76, 6343, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2078, 76, 6344, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2079, 76, 6345, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2080, 76, 6346, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2081, 76, 6347, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2082, 76, 6348, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2083, 76, 6349, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2084, 76, 6350, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2085, 76, 6351, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2086, 76, 6352, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2087, 76, 6353, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2088, 76, 6354, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2089, 76, 6355, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2090, 76, 6356, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2091, 76, 6357, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2092, 76, 6358, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2093, 76, 6359, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2094, 76, 6360, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2095, 76, 6361, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2096, 76, 6362, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2097, 76, 6363, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2098, 76, 6364, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2099, 76, 6365, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2100, 76, 6366, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2101, 76, 6367, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2102, 76, 6368, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2103, 76, 6369, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2104, 76, 6370, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2105, 76, 6371, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2106, 76, 6372, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2107, 76, 6373, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2108, 76, 6374, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2109, 76, 6375, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2110, 76, 6376, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2111, 76, 6377, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2112, 76, 6378, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2113, 76, 6379, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2114, 76, 6380, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2115, 76, 6381, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2116, 76, 6382, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2117, 76, 6383, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2118, 76, 6384, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2119, 76, 6385, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2120, 76, 6386, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2121, 76, 6387, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2122, 76, 6388, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2123, 76, 6389, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2124, 76, 6390, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2125, 76, 6391, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2126, 76, 6392, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2127, 76, 6393, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2128, 76, 6394, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2129, 76, 6395, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2130, 76, 6396, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2131, 76, 6397, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2132, 76, 6398, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2133, 76, 6399, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2134, 76, 6400, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2135, 76, 6401, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2136, 76, 6402, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2137, 76, 6403, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2138, 76, 6404, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2139, 76, 6405, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2140, 76, 6406, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2141, 76, 6407, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2142, 76, 6408, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2143, 76, 6409, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2144, 76, 6410, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2145, 76, 6411, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2146, 76, 6412, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2147, 76, 6413, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2148, 76, 6414, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2149, 76, 6415, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2150, 76, 6416, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2151, 76, 6417, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2152, 76, 6418, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2153, 76, 6419, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2154, 76, 6420, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2155, 76, 6421, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2156, 76, 6422, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2157, 76, 6423, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2158, 76, 6424, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2159, 76, 6425, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2160, 76, 6426, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2161, 76, 6427, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2162, 76, 6428, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2163, 76, 6429, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2164, 76, 6430, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2165, 76, 6431, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2166, 76, 6432, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2167, 76, 6433, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2168, 76, 6434, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2169, 76, 6435, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2170, 76, 6436, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2171, 76, 6437, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2172, 76, 6438, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2173, 76, 6439, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2174, 76, 6440, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2175, 76, 6441, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2176, 76, 6442, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2177, 76, 6443, 20000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2178, 76, 6444, 30000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2242, 76, 7223, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2243, 76, 7224, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2244, 76, 7225, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2245, 76, 7226, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2246, 76, 7227, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2247, 76, 7228, 5000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2248, 76, 7229, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2249, 76, 7230, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2250, 76, 7231, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2251, 76, 7232, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2252, 76, 7233, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2253, 76, 7234, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2254, 76, 7235, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2255, 76, 7236, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2256, 76, 7237, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2257, 76, 7238, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2258, 76, 7239, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2259, 76, 7240, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2352, 76, 7925, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2353, 76, 7926, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2354, 76, 7927, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2355, 76, 7928, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2356, 76, 7929, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2357, 76, 7930, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2358, 76, 7931, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2359, 76, 7932, 50000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2360, 76, 7933, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2361, 76, 7934, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2362, 76, 7935, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2363, 76, 7936, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2364, 76, 7937, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2365, 76, 7938, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2366, 76, 7939, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2367, 76, 7940, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2372, 76, 8368, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2373, 76, 8369, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2374, 76, 8370, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2375, 76, 8371, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2376, 76, 8372, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2377, 76, 8373, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2378, 76, 8374, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2379, 76, 8375, 10000, 0, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2179, 77, 5615, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2180, 77, 5616, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2181, 77, 5617, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2182, 77, 5618, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2183, 77, 5619, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2184, 77, 5620, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2185, 77, 5621, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2186, 77, 5622, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2187, 77, 5623, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2188, 77, 5624, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2189, 77, 5625, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2190, 77, 5626, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2191, 77, 5627, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2192, 77, 5628, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2193, 77, 5629, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2194, 77, 5630, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2195, 77, 5631, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2196, 77, 6172, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2197, 77, 6173, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2198, 77, 6174, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2199, 77, 6175, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2200, 77, 6176, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2201, 77, 6177, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2202, 77, 6178, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2203, 77, 6179, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2204, 77, 6180, 250, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2205, 77, 6277, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2225, 78, 6683, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2226, 78, 6684, 300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2227, 78, 6685, 400, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2228, 78, 6686, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2229, 78, 6687, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2265, 79, 355, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2266, 79, 356, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2276, 79, 357, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2274, 79, 358, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2275, 79, 359, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2267, 79, 360, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2272, 79, 403, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2263, 79, 425, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2273, 79, 715, 368, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2264, 79, 721, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2268, 79, 1288, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2269, 79, 1289, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2270, 79, 1290, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2271, 79, 1291, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2277, 79, 2820, 500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2278, 79, 2835, 23000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2279, 79, 4832, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2281, 79, 5243, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2282, 79, 5244, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2283, 79, 5245, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2284, 79, 5246, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2326, 79, 5247, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2285, 79, 5515, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2286, 79, 5516, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2287, 79, 5519, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2288, 79, 5520, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2289, 79, 5687, 15, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2327, 79, 5690, 1600, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2294, 80, 358, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2295, 80, 359, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2293, 80, 715, 368, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2296, 80, 4832, 800, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2322, 80, 5247, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2323, 80, 5690, 1600, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2300, 80, 7425, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2336, 80, 7794, 130, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2337, 80, 7795, 3900, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2309, 81, 355, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2314, 81, 357, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2310, 81, 358, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2311, 81, 359, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2313, 81, 425, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2315, 81, 428, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2316, 81, 429, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2317, 81, 708, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2312, 81, 715, 600, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2318, 81, 4832, 1300, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2324, 81, 5247, 1950, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2325, 81, 5690, 2590, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2333, 82, 7569, 150, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2334, 82, 7570, 200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2335, 82, 7571, 400, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2338, 83, 355, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2341, 83, 356, 700, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2343, 83, 357, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2339, 83, 358, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2340, 83, 359, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2342, 83, 425, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2344, 83, 428, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2345, 83, 429, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2346, 83, 708, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2347, 83, 714, 120, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2348, 83, 715, 450, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2349, 83, 4832, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2350, 83, 5247, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2351, 83, 5690, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2380, 84, 8384, 100000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2381, 84, 8387, 50000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2382, 84, 8388, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2383, 84, 8389, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2385, 85, 8385, 100000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2386, 85, 8387, 50000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2387, 85, 8388, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2388, 85, 8389, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2389, 86, 8386, 100000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2390, 86, 8387, 50000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2391, 86, 8388, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2392, 86, 8389, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2393, 87, 8387, 50000000, 1, 0, 1);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2394, 87, 8388, 10000000, 1, 0, 1);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2395, 87, 8389, 10000000, 1, 0, 1);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2396, 87, 8390, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2397, 87, 8391, 2000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2398, 87, 8392, 1000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2399, 88, 8387, 50000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2400, 88, 8388, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2401, 88, 8389, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2402, 88, 8390, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2403, 88, 8391, 2000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2404, 88, 8392, 1000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2405, 89, 8387, 50000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2406, 89, 8388, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2407, 89, 8389, 10000000, 1, 0, 2);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2408, 89, 8390, 10000000, 1, 0, 1);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2409, 89, 8391, 2000000, 1, 0, 1);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2410, 89, 8392, 1000000, 1, 0, 1);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2411, 90, 355, 100, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2415, 90, 357, 2500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2412, 90, 358, 1200, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2413, 90, 359, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2414, 90, 425, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2416, 90, 428, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2417, 90, 429, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2418, 90, 708, 50, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2419, 90, 715, 450, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2420, 90, 4832, 1000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2421, 90, 5247, 1500, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2422, 90, 5690, 2000, 1, 0, 0);
GO

INSERT INTO [dbo].[TblMerchantSellList] ([mIndex], [ListID], [ItemID], [Price], [Flag], [SortKey], [LimitCnt]) VALUES (2423, 91, 1621, 700, 1, 0, 0);
GO

