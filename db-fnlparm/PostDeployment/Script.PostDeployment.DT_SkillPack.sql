/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillPack
Date                  : 2023-10-07 09:08:57
*/


INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1, '火球术(I)', 12, 2, 0, 10000, '向敌人施展用魔法制作的火球.', '火焰开始聚集在你的手指尖上.', 1500, 4, 1, 'skillicon02.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (2, '蛛网术(I)', 12, 2, 0, 10000, '向敌人施展蛛网术让敌人无法移动.', '粘稠的液体从手上开始分泌出来.', 1500, 4, 30, 'skillicon02.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (3, '厄运之触(I)', 12, 2, 0, 10000, '用黑暗之力让敌人处于痛苦和恐惧的状态.', '邪恶的力量开始聚集在手指尖上.', 1500, 4, 30, 'skillicon02.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (4, '雷电召唤(I)', 12, 2, 0, 10000, '乌云开始聚集在敌人的头上方.', '乌云开始聚集在敌人的头上方.', 1500, 4, 18, 'skillicon02.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (5, '烈焰风暴(I)', 12, 2, 0, 10000, '用强烈的火焰燃烧敌人.\n\n周边15区域', '大地的烈火开始聚集在敌人的脚底下.', 1500, 4, 48, 'skillicon03.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (6, '冰风暴(I)', 12, 2, 0, 10000, '使用强力的冷气将敌人冰冻住.', '周围瞬间被冰冻住了.', 1500, 4, 42, 'skillicon02.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (7, '剧毒之云', 12, 2, 0, 10000, '将带有毒和麻痹效果的气体发射给敌人.', '绿色烟雾开始布满周围.', 1500, 4, 42, 'skillicon03.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (8, '侵蚀火焰(I)', 12, 2, 0, 10000, '发射强力的酸性物体.', '带有酸性的雾开始布满周围.', 1500, 4, 36, 'skillicon02.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (9, '沉默', 12, 2, 0, 10000, '用魔法让敌人变成哑巴无法使用魔法.', '开始吸收噪音.', 1500, 4, 24, 'skillicon02.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (10, '诅咒', 12, 2, 0, 10000, '给敌人进行诅咒并造成伤害.', '黑色烟雾开始布满周围.', 1500, 4, 18, 'skillicon02.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (11, '魔法驱散', 12, 3, 0, 10000, '消除魔法力.', '去除魔力.', 1500, 4, 36, 'skillicon02.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (12, '加速', 12, 3, 0, 10000, '可以让身体变得更快的魔法.', '移动速度变快.', 1500, 4, 24, 'skillicon03.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (13, '解毒术', 12, 3, 0, 10000, '解毒.', '开始解毒.', 1500, 4, 18, 'skillicon02.dds', 96, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (14, '魔法盾(I)', 12, 3, 0, 10000, '用法力的力量提升防御力.', '魔力气息开始围绕了起来.', 1500, 4, 1, 'skillicon02.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (15, '石化皮肤(I)', 12, 3, 0, 10000, '用大地的力量提升防御力.', '皮肤开始变硬.', 1500, 4, 12, 'skillicon02.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (16, '祝福光环(I)', 12, 3, 0, 10000, '祝福对方并在战斗里提供帮助.', '祝福开始围绕着身边.', 1500, 4, 36, 'skillicon02.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (17, '剑术精通(II)', 12, 1, 0, 10000, '短时间内提高自身的攻击力和命中率.', '蓝色气息开始围绕在武器上.', 1500, 4, 24, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (18, '神圣铠甲(I)', 12, 3, 0, 10000, '保护不受到邪恶敌人的攻击.', '神圣气息开始围绕着身边.', 1500, 4, 6, 'skillicon03.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (19, '元素铠甲', 12, 3, 0, 10000, '用精灵的力量更好的抵抗魔法攻击.', '开始受到保护.', 1500, 4, 42, 'skillicon02.dds', 144, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (20, '冰箭术(I)', 12, 2, 0, 10000, '发射小型的冰块.', '冰冷的气息开始聚集在手指尖上.', 1500, 4, 6, 'skillicon02.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (21, '闪电箭(I)', 12, 2, 0, 10000, '发射魔法闪电.', '闪电开始聚集在手指尖上.', 1500, 4, 12, 'skillicon02.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (22, '强酸箭(I)', 12, 2, 0, 10000, '发射小型的酸性物体.', '产生强烈的爆炸.', 1500, 4, 12, 'skillicon02.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (23, '复活术', 12, 3, 0, 10000, '将生命力附加给尸体的魔法.短时间内提升自身的攻击力和命中率.', '新生命的光芒开始聚集了起来.', 1500, 4, 30, 'skillicon02.dds', 48, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (24, '剑术精通(I)', 12, 1, 0, 10000, '短时间内提高自身的攻击力和命中率.', '蓝色气息开始围绕在武器上.', 1500, 4, 1, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (25, '剑术精通(III)', 12, 1, 0, 10000, '短时间内提高自身的攻击力和命中率.', '蓝色气息开始围绕在武器上.', 1500, 4, 36, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (26, '法力恢复(I)', 12, 1, 0, 10000, '增加MP恢复量.但移动或攻击的话效果将会消失.', '魔力气息开始围绕了起来.', 1500, 4, 12, 'skillicon02.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (27, '增加负重(I)', 12, 3, 0, 10000, '提升可携带物品的负重.', '身体开始变轻.', 1500, 4, 30, 'skillicon02.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (28, '火球术(30天)', 12, 2, 0, 30, '向敌人施展用魔法制作的火球.', '火焰开始聚集在你的手指尖上.', 1500, 4, 1, 'skillicon02.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (29, '魔法盾(30天)', 12, 3, 0, 30, '用法力的力量提升防御力.', '魔力气息开始围绕了起来.', 1500, 4, 1, 'skillicon02.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (30, '剑术精通(I)(30天)', 12, 1, 0, 30, '短时间内提高自身的攻击力和命中率.', '蓝色气息开始围绕在武器上.', 1500, 4, 1, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (31, '冰箭术(30天)', 12, 2, 0, 30, '发射小型的冰块.', '冰冷的气息开始聚集在手指尖上.', 1500, 4, 6, 'skillicon02.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (32, '神圣铠甲(30天)', 12, 3, 0, 30, '保护不受到邪恶敌人的攻击.', '神圣气息开始围绕着身边.', 1500, 4, 6, 'skillicon03.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (33, '石化皮肤(30天)', 12, 3, 0, 30, '用大地的力量提升防御力.', '皮肤开始变硬.', 1500, 4, 12, 'skillicon02.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (34, '强酸箭(30天)', 12, 2, 0, 30, '发射小型的酸性物体.', '产生强烈的爆炸.', 1500, 4, 12, 'skillicon02.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (35, '魔法再生(30天)', 12, 1, 0, 30, '增加MP恢复量.但移动或攻击的话效果将会消失.', '魔力气息开始围绕了起来.', 1500, 4, 12, 'skillicon02.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (36, '解毒术(30天)', 12, 3, 0, 30, '解毒.', '开始解毒.', 1500, 4, 18, 'skillicon02.dds', 96, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (37, '诅咒(30天)', 12, 2, 0, 30, '给敌人进行诅咒并造成伤害.', '黑色烟雾开始布满周围.', 1500, 4, 18, 'skillicon02.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (38, '雷电召唤(30天)', 12, 2, 0, 30, '从天上召唤闪电并对敌人造成强烈的打击.', '乌云开始聚集在敌人的头上方.', 1500, 4, 18, 'skillicon02.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (39, '加速(30天)', 12, 3, 0, 30, '可以让身体变得更快的魔法.', '移动速度变快.', 1500, 4, 24, 'skillicon03.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (40, '沉默(30天)', 12, 2, 0, 30, '短时间内提高自身的攻击力和命中率.', '开始吸收噪音.', 1500, 4, 24, 'skillicon02.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (41, '剑术精通(II)(30天)', 12, 1, 0, 30, '让身体变的轻盈并让身体浮在空中.\n\n攻击速度及移动速度大幅度上升\n变身中可以使用', '蓝色气息开始围绕在武器上.', 1500, 4, 24, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (42, '漂浮术', 12, 8, 0, 10000, '集中自身的所有精力来凝聚魔力, 但背诵完咒文以后后遗症会有很长一段时间.\n\n有技能CD时间\n消耗10点声望值', '身体变的像羽毛一样轻盈.', 150, 4, 50, 'skillicon02.dds', 0, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (43, '魔法凝聚', 12, 1, 0, 10000, '短时间内提高自身的攻击力和命中率.', '将精神力集中在一处.', 150, 4, 50, 'skillicon02.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (44, '剑术精通(IV)', 12, 1, 0, 10000, '短时间内提高自身的攻击力和命中率.', '蓝色气息开始围绕在武器上.', 1500, 4, 48, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (45, '[区域]增加负重(I)', 12, 7, 0, 10000, '提升可携带物品的负重.\n\n区域效果', '3的身体开始变轻.', 1500, 4, 50, 'skillicon02.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (46, '[区域]魔法盾(I)', 12, 7, 0, 10000, '用法力的力量提升防御力.\n\n区域效果', '魔力气息开始围绕了起来.', 1500, 4, 51, 'skillicon02.dds', 194, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (47, '[区域]石化皮肤(I)', 12, 7, 0, 10000, '用大地的力量提升防御力.\n\n区域效果', '皮肤开始变硬.', 1500, 4, 52, 'skillicon02.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (48, '[区域]神圣铠甲(I)', 12, 7, 0, 10000, '保护不受到邪恶敌人的攻击.\n\n区域效果', '神圣气息开始围绕着身边.', 1500, 4, 53, 'skillicon02.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (49, '[区域]祝福光环(I)', 12, 7, 0, 10000, '祝福对方并在战斗里提供帮助.\n\n区域效果', '祝福开始围绕着身边.', 1500, 4, 54, 'skillicon02.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (50, '火球术(II)', 12, 2, 0, 10000, '向敌人施展用魔法制作的火球.', '火焰开始聚集在你的手指尖上.', 1500, 4, 10, 'skillicon02.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (51, '火球术(III)', 12, 2, 0, 10000, '向敌人施展用魔法制作的火球.', '火焰开始聚集在你的手指尖上.', 1500, 4, 20, 'skillicon02.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (52, '火球术(IV)', 12, 2, 0, 10000, '向敌人施展用魔法制作的火球.', '火焰开始聚集在你的手指尖上.', 1500, 4, 30, 'skillicon02.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (53, '火球术(V)', 12, 2, 0, 10000, '向敌人施展用魔法制作的火球.', '火焰开始聚集在你的手指尖上.', 1500, 4, 40, 'skillicon02.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (54, '冰箭术(II)', 12, 2, 0, 10000, '发射小型的冰块.', '冰冷的气息开始聚集在手指尖上.', 1500, 4, 15, 'skillicon02.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (55, '冰箭术(III)', 12, 2, 0, 10000, '发射小型的冰块.', '冰冷的气息开始聚集在手指尖上.', 1500, 4, 25, 'skillicon02.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (56, '冰箭术(IV)', 12, 2, 0, 10000, '发射小型的冰块.', '冰冷的气息开始聚集在手指尖上.', 1500, 4, 35, 'skillicon02.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (57, '冰箭术(V)', 12, 2, 0, 10000, '发射小型的冰块.', '冰冷的气息开始聚集在手指尖上.', 1500, 4, 45, 'skillicon02.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (58, '闪电箭(II)', 12, 2, 0, 10000, '发射魔法闪电.', '闪电开始聚集在手指尖上.', 1500, 4, 22, 'skillicon02.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (59, '闪电箭(III)', 12, 2, 0, 10000, '发射魔法闪电.', '闪电开始聚集在手指尖上.', 1500, 4, 32, 'skillicon02.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (60, '闪电箭(IV)', 12, 2, 0, 10000, '发射魔法闪电.', '闪电开始聚集在手指尖上.', 1500, 4, 42, 'skillicon02.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (61, '闪电箭(V)', 12, 2, 0, 10000, '发射魔法闪电.', '闪电开始聚集在手指尖上.', 1500, 4, 52, 'skillicon02.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (62, '强酸箭(II)', 12, 2, 0, 10000, '发射小型的酸性物体.', '产生强烈的爆炸.', 1500, 4, 22, 'skillicon02.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (63, '强酸箭(III)', 12, 2, 0, 10000, '发射小型的酸性物体.', '产生强烈的爆炸.', 1500, 4, 32, 'skillicon02.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (64, '强酸箭(IV)', 12, 2, 0, 10000, '发射小型的酸性物体.', '产生强烈的爆炸.', 1500, 4, 42, 'skillicon02.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (65, '强酸箭(V)', 12, 2, 0, 10000, '发射小型的酸性物体.', '产生强烈的爆炸.', 1500, 4, 52, 'skillicon02.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (66, '雷电召唤(II)', 12, 2, 0, 10000, '从天上召唤闪电并对敌人造成强烈的打击.', '乌云开始聚集在敌人的头上方.', 1500, 4, 28, 'skillicon02.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (67, '雷电召唤(III)', 12, 2, 0, 10000, '从天上召唤闪电并对敌人造成强烈的打击.', '乌云开始聚集在敌人的头上方.', 1500, 4, 38, 'skillicon02.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (68, '雷电召唤(IV)', 12, 2, 0, 10000, '从天上召唤闪电并对敌人造成强烈的打击.', '乌云开始聚集在敌人的头上方.', 1500, 4, 48, 'skillicon02.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (69, '雷电召唤(V)', 12, 2, 0, 10000, '从天上召唤闪电并对敌人造成强烈的打击.', '乌云开始聚集在敌人的头上方.', 1500, 4, 58, 'skillicon02.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (70, '侵蚀火焰(II)', 12, 2, 0, 10000, '发射强力的酸性物体.', '带有酸性的雾开始布满周围.', 1500, 4, 41, 'skillicon02.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (71, '侵蚀火焰(III)', 12, 2, 0, 10000, '发射强力的酸性物体.', '带有酸性的雾开始布满周围.', 1500, 4, 46, 'skillicon02.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (72, '侵蚀火焰(IV)', 12, 2, 0, 10000, '发射强力的酸性物体.', '带有酸性的雾开始布满周围.', 1500, 4, 51, 'skillicon02.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (73, '侵蚀火焰(V)', 12, 2, 0, 10000, '发射强力的酸性物体.', '带有酸性的雾开始布满周围.', 1500, 4, 56, 'skillicon02.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (74, '冰风暴(II)', 12, 2, 0, 10000, '使用强力的冷气将敌人冰冻住.', '周围瞬间被冰冻住了.', 1500, 4, 47, 'skillicon02.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (75, '冰风暴(III)', 12, 2, 0, 10000, '使用强力的冷气将敌人冰冻住.', '周围瞬间被冰冻住了.', 1500, 4, 52, 'skillicon02.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (76, '冰风暴(IV)', 12, 2, 0, 10000, '使用强力的冷气将敌人冰冻住.', '周围瞬间被冰冻住了.', 1500, 4, 57, 'skillicon02.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (77, '冰风暴(V)', 12, 2, 0, 10000, '使用强力的冷气将敌人冰冻住.', '周围瞬间被冰冻住了.', 1500, 4, 62, 'skillicon02.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (78, '烈焰风暴(II)', 12, 2, 0, 10000, '使用强烈的火焰燃烧敌人.\n\n区域魔法', '大地的烈火开始聚集在敌人的脚底下.', 1500, 4, 53, 'skillicon03.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (79, '烈焰风暴(III)', 12, 2, 0, 10000, '使用强烈的火焰燃烧敌人.\n\n区域魔法', '大地的烈火开始聚集在敌人的脚底下.', 1500, 4, 58, 'skillicon03.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (80, '烈焰风暴(IV)', 12, 2, 0, 10000, '使用强烈的火焰燃烧敌人.\n\n区域魔法', '大地的烈火开始聚集在敌人的脚底下.', 1500, 4, 63, 'skillicon03.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (81, '烈焰风暴(V)', 12, 2, 0, 10000, '使用强烈的火焰燃烧敌人.\n\n区域魔法', '大地的烈火开始聚集在敌人的脚底下.', 1500, 4, 68, 'skillicon03.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (82, '漂浮术(体验用)', 12, 8, 0, 10000, '让身体变的轻盈并让身体浮在空中.\n\n攻击速度及移动速度大幅度上升\n变身中可以使用', '身体变得像羽毛一样轻盈.', 150, 4, 50, 'skillicon02.dds', 0, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (83, '魔法凝聚(体验用)', 12, 1, 0, 10000, '集中自身的所有精力来凝聚魔力, 但背诵完咒文以后后遗症会有很长一段时间.\n\n有技能CD时间\n消耗10点声望值', '将精神力集中在一处.', 150, 4, 50, 'skillicon02.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (84, '魔力强化', 12, 1, 0, 10000, '提高对魔力流动的认知.', '', 150, 16, 20, 'skillicon02.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (85, '时间扭曲', 12, 1, 0, 10000, '时间开始在扭曲.', '', 150, 16, 40, 'skillicon02.dds', 144, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (86, '召唤哥布林爆破兵', 12, 1, 0, 10000, '召唤兽通过自爆来对敌人进行打击.', '', 150, 16, 15, 'skillicon02.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (87, '召唤嗜血曼陀罗', 12, 1, 0, 10000, '召唤减少生命力的曼陀罗.\n\n召唤兽设定窗口 : Z键', '', 150, 16, 20, 'skillicon02.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (88, '召唤魔力曼陀罗', 12, 1, 0, 10000, '召唤吸收魔力的大树.\n\n召唤兽设定窗口 : Z键', '', 150, 16, 20, 'skillicon02.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (89, '防御图腾', 12, 1, 0, 10000, '召唤出在特定情况下保护对象的图腾.', '', 150, 16, 25, 'skillicon02.dds', 432, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (90, '生命图腾', 12, 1, 0, 10000, '召唤出给对象传达生命力的图腾.', '', 150, 16, 35, 'skillicon02.dds', 432, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (91, '地狱烈焰', 12, 1, 0, 10000, '召唤出地狱火精灵燃烧周围.', '', 150, 16, 40, 'skillicon02.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (92, '海神之怒', 12, 1, 0, 10000, '召唤出深海精灵瞬间对周围造成伤害.', '', 150, 16, 40, 'skillicon02.dds', 432, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (93, '流星火', 12, 2, 0, 10000, '召唤出巨大陨石对周围的敌人给予异常状态.', '', 150, 16, 45, 'skillicon02.dds', 96, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (94, '召唤哥布林爆破兵(30天)', 12, 1, 0, 30, '召唤兽通过自爆来对敌人进行打击.', '', 150, 16, 15, 'skillicon02.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (95, '召唤奥利爱德图腾', 12, 1, 0, 10000, '召唤出吸收对方伤害的图腾.', '', 150, 16, 50, 'skillicon02.dds', 192, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (96, '魔法驱散 (II)', 12, 3, 0, 10000, '消除一部分魔法力.', '去除魔力.', 1500, 4, 42, 'skillicon02.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (97, '魔法驱散 (III)', 12, 3, 0, 10000, '消除一部分魔法力.', '去除魔力.', 1500, 4, 48, 'skillicon02.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (98, '魔法驱散 (IV)', 12, 3, 0, 10000, '消除一部分魔法力.', '去除魔力.', 1500, 4, 54, 'skillicon02.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (99, '魔法驱散 (V)', 12, 3, 0, 10000, '消除一部分魔法力.', '去除魔力.', 1500, 4, 60, 'skillicon02.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (100, '魔法驱散 (I)', 12, 3, 0, 10000, '消除一部分魔法力.', '去除魔力.', 1500, 4, 36, 'skillicon02.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (101, '增加负重(II)', 12, 3, 0, 10000, '负重增加的量比(I)等级更多.', '', 1500, 4, 50, 'skillicon02.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (102, '增加负重(III)', 12, 3, 0, 10000, '负重增加的量比(Ⅱ)等级更多.', '', 1500, 4, 55, 'skillicon02.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (103, '[区域]增加负重(II)', 12, 7, 0, 10000, '负重增加的量比(Ⅰ)等级更多.\n\n区域效果', '', 1500, 4, 55, 'skillicon02.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (104, '[区域]增加负重(III)', 12, 7, 0, 10000, '负重增加的量比(Ⅱ)等级更多.\n\n区域效果', '', 1500, 4, 60, 'skillicon02.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (105, '魔法盾(II)', 12, 3, 0, 10000, '用魔力提升防御力的增加量比(I)等级更多.', '', 1500, 4, 51, 'skillicon02.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (106, '魔法盾(III)', 12, 3, 0, 10000, '用魔力提升防御力的增加量比(II)等级更多.', '', 1500, 4, 55, 'skillicon02.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (107, '[区域]魔法盾(II)', 12, 7, 0, 10000, '用魔力提升防御力的增加量比(Ⅰ)等级更多.\n\n区域效果', '', 1500, 4, 55, 'skillicon02.dds', 194, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (108, '[区域]魔法盾(III)', 12, 7, 0, 10000, '用魔力提升防御力的增加量比(Ⅱ)等级更多.\n\n区域效果', '', 1500, 4, 60, 'skillicon02.dds', 194, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (109, '石化皮肤(II)', 12, 3, 0, 10000, '用大地的力量提升防御力的增加量比(Ⅰ)等级更多.', '', 1500, 4, 52, 'skillicon02.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (110, '石化皮肤(III)', 12, 3, 0, 10000, '用大地的力量提升防御力的增加量比(Ⅱ)等级更多.', '', 1500, 4, 55, 'skillicon02.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (111, '[区域]石化皮肤(II)', 12, 7, 0, 10000, '用大地的力量提升防御力的增加量比(Ⅰ)等级更多.\n\n区域效果', '', 1500, 4, 55, 'skillicon02.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (112, '[区域]石化皮肤(III)', 12, 7, 0, 10000, '用大地的力量提升防御力的增加量比(Ⅱ)等级更多.\n\n区域效果', '', 1500, 4, 60, 'skillicon02.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (113, '神圣铠甲(II)', 12, 3, 0, 10000, '保护不受到邪恶敌人的攻击.', '', 1500, 4, 53, 'skillicon03.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (114, '神圣铠甲(III)', 12, 3, 0, 10000, '保护不受到邪恶敌人的攻击.', '', 1500, 4, 55, 'skillicon03.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (115, '[区域]神圣铠甲(II)', 12, 7, 0, 10000, '保护不受到邪恶敌人的攻击.\n\n区域效果', '', 1500, 4, 55, 'skillicon02.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (116, '[区域]神圣铠甲(III)', 12, 7, 0, 10000, '保护不受到邪恶敌人的攻击.\n\n区域效果', '', 1500, 4, 60, 'skillicon02.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (117, '祝福光环(II)', 12, 3, 0, 10000, '祝福对方在战斗中比(I)级别提供更多的帮助.', '', 1500, 4, 54, 'skillicon02.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (118, '祝福光环(III)', 12, 3, 0, 10000, '祝福对方在战斗中比(II)级别提供更多的帮助.', '', 1500, 4, 55, 'skillicon02.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (119, '[区域]祝福光环(II)', 12, 7, 0, 10000, '祝福对方在战斗中比(I)级别提供更多的帮助.\n\n区域效果', '', 1500, 4, 55, 'skillicon02.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (120, '[区域]祝福光环(III)', 12, 7, 0, 10000, '祝福对方在战斗中比(II)级别提供更多的帮助.\n\n区域效果', '', 1500, 4, 60, 'skillicon02.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (121, '剑术精通(V)', 12, 1, 0, 10000, '短时间内提高自身的攻击力和命中率.', '蓝色气息开始围绕在武器上.', 1500, 4, 60, 'skillicon02.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (122, '魔法再生(II)', 12, 1, 0, 10000, '增加MP恢复量. 但移动或攻击时效果将会解除.', '', 1500, 4, 53, 'skillicon02.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (123, '魔法再生(III)', 12, 1, 0, 10000, '增加MP恢复量. 但移动或攻击时效果将会解除.', '', 1500, 4, 55, 'skillicon02.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (124, '[区域]魔法再生(I)', 12, 7, 0, 10000, '增加MP恢复量. 但移动或攻击时效果将会解除.\n\n区域效果', '', 1500, 4, 53, 'skillicon02.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (125, '[区域]魔法再生(II)', 12, 7, 0, 10000, '增加MP恢复量. 但移动或攻击时效果将会解除.\n\n区域效果', '', 1500, 4, 55, 'skillicon02.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (126, '[区域]魔法再生(III)', 12, 7, 0, 10000, '增加MP恢复量. 但移动或攻击时效果将会解除.\n\n区域效果', '', 1500, 4, 60, 'skillicon02.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (127, '厄运之触(II)', 12, 2, 0, 10000, '使用黑暗魔法的力量使敌人陷入无尽的痛苦与恐惧之中.', '', 1500, 4, 45, 'skillicon02.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (128, '厄运之触(III)', 12, 2, 0, 10000, '使用黑暗魔法的力量使敌人陷入无尽的痛苦与恐惧之中.', '', 1500, 4, 50, 'skillicon02.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (129, '厄运之触(IV)', 12, 2, 0, 10000, '使用黑暗魔法的力量使敌人陷入无尽的痛苦与恐惧之中.', '', 1500, 4, 55, 'skillicon02.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (130, '厄运之触(V)', 12, 2, 0, 10000, '使用黑暗魔法的力量使敌人陷入无尽的痛苦与恐惧之中.', '', 1500, 4, 60, 'skillicon02.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (131, '蛛网术(II)', 12, 2, 0, 10000, '用法力制造的蜘蛛网，攻击后使敌人无法移动.\n比蛛网术(I)有更高魔法成功率.', '', 1500, 4, 45, 'skillicon02.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (132, '蛛网术(III)', 12, 2, 0, 10000, '用法力制造的蜘蛛网，攻击后使敌人无法移动.\n比蛛网术(II)有更高魔法成功率.', '', 1500, 4, 50, 'skillicon02.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (133, '蛛网术(IV)', 12, 2, 0, 10000, '用法力制造的蜘蛛网，攻击后使敌人无法移动.\n比蛛网术(III)有更高魔法成功率.', '', 1500, 4, 55, 'skillicon02.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (134, '蛛网术(V)', 12, 2, 0, 10000, '用法力制造的蜘蛛网，攻击后使敌人无法移动.\n比蛛网术(IV)有更高魔法成功率.', '', 1500, 4, 60, 'skillicon02.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (135, '制造食物', 13, 1, 0, 10000, '制造魔法食物.', '', 1500, 4, 1, 'skillicon03.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (136, '传送术', 13, 1, 0, 10000, '随机瞬间移动到周围其他的地方的魔法.', '传送', 1500, 4, 6, 'skillicon02.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (137, '重击', 13, 8, 0, 10000, '重击伤害目标.', '', 150, 5, 10, 'skillicon03.dds', 144, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (138, '猛力攻击', 13, 8, 0, 10000, '一定时间内提升力量.', '', 150, 1, 20, 'skillicon02.dds', 288, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (139, '痛苦麻痹', 13, 8, 0, 10000, '瞬间提升防御力.', '', 150, 1, 30, 'skillicon02.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (140, '暴走', 13, 8, 0, 10000, '降低防御力，同时提高攻击速度提高.', '', 150, 1, 40, 'skillicon02.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (141, '瞄准射击', 13, 8, 0, 10000, '重击伤害目标.\n对同一个目标连续使用该技能，可产生额外追加伤害.', '', 1500, 2, 10, 'skillicon02.dds', 48, 384, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (142, '振荡射击', 13, 6, 0, 10000, '一定时间使目标的移动速度缓慢.', '', 1500, 2, 30, 'skillicon02.dds', 192, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (143, '音速屏障', 13, 8, 0, 10000, '一定时间提升敏捷，同时减伤防御提升.', '', 150, 2, 20, 'skillicon02.dds', 384, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (144, '群体拯救', 13, 7, 0, 10000, '使目标的防御力上升.', '', 150, 4, 40, 'skillicon02.dds', 144, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (145, '凝神术', 13, 8, 0, 10000, '一定时间提升智力.', '', 150, 4, 20, 'skillicon02.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (146, '魔法恢复', 13, 8, 0, 10000, '无视负重状态可恢复魔法值.', '', 150, 4, 30, 'skillicon02.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (147, '疾速射击', 13, 8, 0, 10000, '一定时间攻击速度提升.', '', 150, 2, 40, 'skillicon02.dds', 240, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (148, '疾行', 13, 1, 0, 10000, '一定时间移动提升.', '', 150, 8, 10, 'skillicon02.dds', 48, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (149, '隐身术', 13, 8, 0, 10000, '进入透明状态.', '', 150, 8, 20, 'skillicon02.dds', 384, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (150, '回避', 13, 8, 0, 10000, '回避率瞬间增加.', '', 150, 8, 20, 'skillicon02.dds', 336, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (151, '毒牙', 13, 1, 0, 10000, '对目标施毒.', '', 150, 8, 30, 'skillicon02.dds', 192, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (152, '暗杀', 13, 8, 0, 10000, '对目标进行强力攻击.\n\n未进入隐身状态下无法触发.', '', 150, 8, 40, 'skillicon02.dds', 0, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (153, '侦察', 13, 7, 0, 10000, '使处于隐身状态单位现形的魔法.', '', 150, 4, 12, 'skillicon02.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (154, '昏厥攻击', 13, 19, 0, 10000, '强力攻击使目标昏厥.', '', 150, 8, 40, 'skillicon02.dds', 0, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (155, '制造食物(30天)', 13, 1, 0, 30, '制造魔法食物.', '', 150, 4, 1, 'skillicon03.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (156, '传送术(30天)', 13, 1, 0, 30, '可瞬移的魔法.', '', 1500, 4, 6, 'skillicon02.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (157, '生命绽放', 13, 8, 0, 10000, '一定时间内大幅度提高自己的生命值上限.', '', 150, 1, 50, 'skillicon02.dds', 96, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (158, '无视护甲', 13, 20, 0, 10000, '一定时间降低目标的防御力，同时提高自身攻击力.', '', 1500, 2, 50, 'skillicon02.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (159, '要害攻击', 13, 8, 0, 10000, '对目标的弱点进行致命打击.', '', 150, 8, 50, 'skillicon02.dds', 48, 288, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (160, '生命绽放(体验用)', 13, 8, 0, 10000, '集中自身的所有精力来凝聚魔力, 但背诵完咒文以后会有很长一段时间的后遗症.\n\n有技能CD时间\n消耗10点声望值.', '', 150, 1, 50, 'skillicon02.dds', 96, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (161, '无视护甲(体验用)', 13, 20, 0, 10000, '对目标的弱点进行致命打击.', '', 1500, 2, 50, 'skillicon02.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (162, '要害攻击(体验用)', 13, 8, 0, 10000, '对目标的弱点进行致命打击.', '', 150, 8, 50, 'skillicon02.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (163, '猛力攻击', 13, 8, 0, 10000, '对目标进行重击.', '', 150, 16, 10, 'skillicon02.dds', 240, 432, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (164, '腐蚀术', 13, 6, 0, 10000, '腐蚀对方装备，无法装备、并解除异常状态.', '', 1500, 16, 30, 'skillicon02.dds', 336, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (165, '反射光环', 13, 1, 0, 10000, '一定几率免疫魔法，免疫效果触发时反弹给施法者.', '', 150, 16, 50, 'skillicon02.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (166, '暴走', 13, 8, 0, 10000, '减少防御力，同时提升攻击力.', '', 150, 1, 40, 'skillicon02.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (167, '血之盟誓 Lv1', 12, 0, 0, 10000, '增加HP. \n\nHP +5', '', 150, 255, 1, 'skillicon01.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (168, '血之盟誓 Lv2', 12, 0, 0, 10000, '增加HP. \n\nHP +10', '', 150, 255, 1, 'skillicon01.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (169, '血之盟誓 Lv3', 12, 0, 0, 10000, '增加HP. \n\nHP +15', '', 150, 255, 1, 'skillicon01.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (170, '血之盟誓 Lv4', 12, 0, 0, 10000, '增加HP. \n\nHP +20', '', 150, 255, 1, 'skillicon01.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (171, '血之盟誓 Lv5', 12, 0, 0, 10000, '增加HP. \n\nHP +25', '', 150, 255, 1, 'skillicon01.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (172, '灵魂之盟誓 Lv1', 12, 0, 0, 10000, '增加MP. \n\nMP +5', '', 150, 255, 1, 'skillicon01.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (173, '灵魂之盟誓 Lv2', 12, 0, 0, 10000, '增加MP. \n\nMP +10', '', 150, 255, 1, 'skillicon01.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (174, '灵魂之盟誓 Lv3', 12, 0, 0, 10000, '增加MP. \n\nMP +15', '', 150, 255, 1, 'skillicon01.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (175, '灵魂之盟誓 Lv4', 12, 0, 0, 10000, '增加MP. \n\nMP +20', '', 150, 255, 1, 'skillicon01.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (176, '灵魂之盟誓 Lv5', 12, 0, 0, 10000, '增加MP. \n\nMP +25', '', 150, 255, 1, 'skillicon01.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (177, '巨人之力 Lv1', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重增加+50', '', 150, 255, 1, 'skillicon01.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (178, '巨人之力 Lv2', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重增加+100', '', 150, 255, 1, 'skillicon01.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (179, '巨人之力 Lv3', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重增加+150', '', 150, 255, 1, 'skillicon01.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (180, '巨人之力 Lv4', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重增加+200', '', 150, 255, 1, 'skillicon01.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (181, '巨人之力 Lv5', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重增加+250', '', 150, 255, 1, 'skillicon01.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (182, '不灭之魂 Lv1', 12, 0, 0, 10000, '在公会议事厅下线时，将会产生休眠经验修炼期\n\n休眠经验修炼期间狩猎怪物获得的经验为2倍', '', 150, 255, 5, 'skillicon01.dds', 144, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (183, '不灭之魂 Lv2', 12, 0, 0, 10000, '在公会议事厅下线时，将会产生休眠经验修炼期\n\n休眠经验修炼期间狩猎怪物获得的经验为2倍', '', 150, 255, 5, 'skillicon01.dds', 144, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (184, '目标锁定', 13, 2, 0, 10000, '使指定目标更加清晰', '对方在闪闪发光.', 1500, 255, 5, 'skillicon01.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (185, '荣誉提升 Lv1', 12, 0, 0, 10000, '公会荣誉勋章制作数量增加.', '', 150, 255, 10, 'skillicon01.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (186, '荣誉提升 Lv2', 12, 0, 0, 10000, '公会荣誉勋章制作数量增加.', '', 150, 255, 10, 'skillicon01.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (187, '荣誉提升 Lv3', 12, 0, 0, 10000, '公会荣誉勋章制作数量增加.', '', 150, 255, 10, 'skillicon01.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (188, '荣誉提升 Lv4', 12, 0, 0, 10000, '公会荣誉勋章制作数量增加.', '', 150, 255, 10, 'skillicon01.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (189, '荣誉提升 Lv5', 12, 0, 0, 10000, '公会荣誉勋章制作数量增加.', '', 150, 255, 10, 'skillicon01.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (190, '技能专用荣誉卷轴', 12, 0, 0, 10000, '制作公会技能所需的经验值相应的公会荣誉勋章 \n\n 公会技能处于研究装填，研究该技能的成员才可使用', '', 150, 255, 15, 'skillicon01.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (191, '召唤', 13, 1, 0, 10000, '使成员传送至公会议事厅.', '', 150, 1, 15, 'skillicon01.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (192, '神之祝福 Lv1', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +1', '', 150, 255, 20, 'skillicon01.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (193, '神之祝福 Lv2', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +2', '', 150, 255, 20, 'skillicon01.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (194, '神之祝福 Lv3', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +3', '', 150, 255, 20, 'skillicon01.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (195, '神之祝福 Lv4', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +4', '', 150, 255, 20, 'skillicon01.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (196, '神之祝福 Lv5', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +5', '', 150, 255, 20, 'skillicon01.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (197, '灵魂的祝福 Lv1', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +1', '', 150, 255, 20, 'skillicon01.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (198, '灵魂的祝福 Lv2', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +2', '', 150, 255, 20, 'skillicon01.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (199, '灵魂的祝福 Lv3', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +3', '', 150, 255, 20, 'skillicon01.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (200, '灵魂的祝福 Lv4', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +4', '', 150, 255, 20, 'skillicon01.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (201, '灵魂的祝福 Lv5', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +5', '', 150, 255, 20, 'skillicon01.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (202, '勇士的祝福 Lv1', 12, 0, 0, 10000, '在负重过重的状态下恢复HP. \n\n无视负重的HP恢复 +1', '', 150, 255, 25, 'skillicon01.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (203, '勇士的祝福 Lv2', 12, 0, 0, 10000, '在负重过重的状态下恢复HP. \n\n无视负重的HP恢复 +2', '', 150, 255, 25, 'skillicon01.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (204, '勇士的祝福 Lv3', 12, 0, 0, 10000, '在负重过重的状态下恢复HP. \n\n无视负重的HP恢复 +3', '', 150, 255, 25, 'skillicon01.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (205, '贤者的祝福 Lv1', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +1', '', 150, 255, 25, 'skillicon01.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (206, '贤者的祝福 Lv2', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +2', '', 150, 255, 25, 'skillicon01.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (207, '贤者的祝福 Lv3', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +3', '', 150, 255, 25, 'skillicon01.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (208, '团结的力量(I) Lv1', 12, 0, 0, 10000, '增加力量. \n\n力量 +1', '', 150, 255, 30, 'skillicon01.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (209, '团结的力量(I) Lv2', 12, 0, 0, 10000, '增加力量. \n\n力量 +2', '', 150, 255, 30, 'skillicon01.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (210, '团结的力量(I) Lv3', 12, 0, 0, 10000, '增加力量. \n\n力量 +3', '', 150, 255, 30, 'skillicon01.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (211, '团结的敏捷(I) Lv1', 12, 0, 0, 10000, '增加敏捷. \n\n敏捷 +1', '', 150, 255, 30, 'skillicon01.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (212, '团结的敏捷(I) Lv2', 12, 0, 0, 10000, '增加敏捷. \n\n敏捷 +2', '', 150, 255, 30, 'skillicon01.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (213, '团结的敏捷(I) Lv3', 12, 0, 0, 10000, '增加敏捷. \n\n敏捷 +3', '', 150, 255, 30, 'skillicon01.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (214, '团结的智力(I) Lv1', 12, 0, 0, 10000, '增加智力. \n\n智力 +1', '', 150, 255, 30, 'skillicon01.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (215, '团结的智力(I) Lv2', 12, 0, 0, 10000, '增加智力. \n\n智力 +2', '', 150, 255, 30, 'skillicon01.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (216, '团结的智力(I) Lv3', 12, 0, 0, 10000, '增加智力. \n\n智力 +3', '', 150, 255, 30, 'skillicon01.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (217, '猛虎之爪 Lv1', 12, 0, 0, 10000, '增加暴击几率.', '', 150, 255, 35, 'skillicon01.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (218, '猛虎之爪 Lv2', 12, 0, 0, 10000, '增加暴击几率.', '', 150, 255, 35, 'skillicon01.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (219, '猛虎之爪 Lv3', 12, 0, 0, 10000, '增加暴击几率.', '', 150, 255, 35, 'skillicon01.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (220, '猛虎之爪 Lv4', 12, 0, 0, 10000, '增加暴击几率.', '', 150, 255, 35, 'skillicon01.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (221, '猛虎之爪 Lv5', 12, 0, 0, 10000, '增加暴击几率.', '', 150, 255, 35, 'skillicon01.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (222, '猛虎之力 Lv1', 12, 0, 0, 10000, '增加暴击伤害.', '', 150, 255, 40, 'skillicon01.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (223, '猛虎之力 Lv2', 12, 0, 0, 10000, '增加暴击伤害.', '', 150, 255, 40, 'skillicon01.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (224, '猛虎之力 Lv3', 12, 0, 0, 10000, '增加暴击伤害.', '', 150, 255, 40, 'skillicon01.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (225, '猛虎之力 Lv4', 12, 0, 0, 10000, '增加暴击伤害.', '', 150, 255, 40, 'skillicon01.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (226, '猛虎之力 Lv5', 12, 0, 0, 10000, '增加暴击伤害.', '', 150, 255, 40, 'skillicon01.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (227, '朱庇特守护 Lv1', 12, 0, 0, 10000, '减少声望值下降. \n\n增加获得声望值', '', 150, 255, 40, 'skillicon01.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (228, '朱庇特守护 Lv2', 12, 0, 0, 10000, '减少声望值下降. \n\n增加获得声望值', '', 150, 255, 40, 'skillicon01.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (229, '朱庇特守护 Lv3', 12, 0, 0, 10000, '减少声望值下降. \n\n增加获得声望值', '', 150, 255, 40, 'skillicon01.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (230, '朱庇特守护 Lv4', 12, 0, 0, 10000, '减少声望值下降. \n\n增加获得声望值', '', 150, 255, 40, 'skillicon01.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (231, '朱庇特守护 Lv5', 12, 0, 0, 10000, '减少声望值下降. \n\n增加获得声望值', '', 150, 255, 40, 'skillicon01.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (232, '近战防御精通(I) Lv1', 12, 0, 0, 10000, '提高近战攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (233, '近战防御精通(I) Lv2', 12, 0, 0, 10000, '提高近战攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (234, '近战防御精通(I) Lv3', 12, 0, 0, 10000, '提高近战攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (235, '近战防御精通(I) Lv4', 12, 0, 0, 10000, '提高近战攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (236, '近战防御精通(I) Lv5', 12, 0, 0, 10000, '提高近战攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (237, '远程防御精通(I) Lv1', 12, 0, 0, 10000, '提高远程攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (238, '远程防御精通(I) Lv2', 12, 0, 0, 10000, '提高远程攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (239, '远程防御精通(I) Lv3', 12, 0, 0, 10000, '提高远程攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (240, '远程防御精通(I) Lv4', 12, 0, 0, 10000, '提高远程攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (241, '远程防御精通(I) Lv5', 12, 0, 0, 10000, '提高远程攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (242, '魔法防御精通(I) Lv1', 12, 0, 0, 10000, '提高魔法攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (243, '魔法防御精通(I) Lv2', 12, 0, 0, 10000, '提高魔法攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (244, '魔法防御精通(I) Lv3', 12, 0, 0, 10000, '提高魔法攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (245, '魔法防御精通(I) Lv4', 12, 0, 0, 10000, '提高魔法攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (246, '魔法防御精通(I) Lv5', 12, 0, 0, 10000, '提高魔法攻击回避率.', '', 150, 255, 45, 'skillicon01.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (247, '近战防御精通(II) Lv1', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (248, '近战防御精通(II) Lv2', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (249, '近战防御精通(II) Lv3', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (250, '远程防御精通(II) Lv1', 12, 0, 0, 10000, '减少受到远程攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (251, '远程防御精通(II) Lv2', 12, 0, 0, 10000, '减少受到远程攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (252, '远程防御精通(II) Lv3', 12, 0, 0, 10000, '减少受到远程攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (253, '魔法防御精通(II) Lv1', 12, 0, 0, 10000, '减少受到魔法攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (254, '魔法防御精通(II) Lv2', 12, 0, 0, 10000, '减少受到魔法攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (255, '魔法防御精通(II) Lv3', 12, 0, 0, 10000, '减少受到魔法攻击伤害.', '', 150, 255, 50, 'skillicon01.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (256, '团结之力(I) Lv1', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 255, 55, 'skillicon01.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (257, '团结之力(I) Lv2', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 255, 55, 'skillicon01.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (258, '团结之力(I) Lv3', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 255, 55, 'skillicon01.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (259, '团结之力(I) Lv4', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 255, 55, 'skillicon01.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (260, '团结之力(I) Lv5', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 255, 55, 'skillicon01.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (261, '飓风之力(I) Lv1', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 255, 55, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (262, '飓风之力(I) Lv2', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 255, 55, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (263, '飓风之力(I) Lv3', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 255, 55, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (264, '飓风之力(I) Lv4', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 255, 55, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (265, '飓风之力(I) Lv5', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 255, 55, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (266, '魔法之力(I) Lv1', 12, 0, 0, 10000, '提高魔法攻击力.', '', 150, 255, 55, 'skillicon01.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (267, '魔法之力(I) Lv2', 12, 0, 0, 10000, '提高魔法攻击力.', '', 150, 255, 55, 'skillicon01.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (268, '魔法之力(I) Lv3', 12, 0, 0, 10000, '提高魔法攻击力.', '', 150, 255, 55, 'skillicon01.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (269, '魔法之力(I) Lv4', 12, 0, 0, 10000, '提高魔法攻击力.', '', 150, 255, 55, 'skillicon01.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (270, '魔法之力(I) Lv5', 12, 0, 0, 10000, '提高魔法攻击力.', '', 150, 255, 55, 'skillicon01.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (271, '猎豹之眼 Lv1', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (272, '猎豹之眼 Lv2', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (273, '猎豹之眼 Lv3', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (274, '巨鹰之眼 Lv1', 12, 0, 0, 10000, '提高远程攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (275, '巨鹰之眼 Lv2', 12, 0, 0, 10000, '提高远程攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (276, '巨鹰之眼 Lv3', 12, 0, 0, 10000, '提高远程攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (277, '魔法之手 Lv1', 12, 0, 0, 10000, '提高魔法攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (278, '魔法之手 Lv2', 12, 0, 0, 10000, '提高魔法攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (279, '魔法之手 Lv3', 12, 0, 0, 10000, '提高魔法攻击命中率.', '', 150, 255, 60, 'skillicon01.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (280, '团结的力量(II) Lv1', 12, 0, 0, 7, '提升力量. \n\n力量 +1', '', 150, 255, 1, 'skillicon01.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (281, '团结的力量(II) Lv2', 12, 0, 0, 7, '提升力量. \n\n力量 +2', '', 150, 255, 1, 'skillicon01.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (282, '团结的敏捷(II) Lv1', 12, 0, 0, 7, '提升敏捷. \n\n敏捷 +1', '', 150, 255, 1, 'skillicon01.dds', 288, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (283, '团结的敏捷(II) Lv2', 12, 0, 0, 7, '提升敏捷. \n\n敏捷 +2', '', 150, 255, 1, 'skillicon01.dds', 288, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (284, '团结的智力(II) Lv1', 12, 0, 0, 7, '提升智力. \n\n智力 +1', '', 150, 255, 1, 'skillicon01.dds', 384, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (285, '团结的智力(II) Lv2', 12, 0, 0, 7, '提升智力. \n\n智力 +2', '', 150, 255, 1, 'skillicon01.dds', 384, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (286, '贤者的祝福(II) Lv1', 12, 0, 0, 7, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +1', '', 150, 255, 1, 'skillicon01.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (287, '贤者的祝福(II) Lv2', 12, 0, 0, 7, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +2', '', 150, 255, 1, 'skillicon01.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (288, '近战防御精通(III) Lv1', 12, 0, 0, 7, '减少受到近战攻击伤害.', '', 150, 255, 1, 'skillicon01.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (289, '近战防御精通(III) Lv2', 12, 0, 0, 7, '减少受到近战攻击伤害.', '', 150, 255, 1, 'skillicon01.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (290, '远程防御精通(III) Lv1', 12, 0, 0, 7, '减少受到远程攻击伤害.', '', 150, 255, 1, 'skillicon01.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (291, '远程防御精通(III) Lv2', 12, 0, 0, 7, '减少受到远程攻击伤害.', '', 150, 255, 1, 'skillicon01.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (292, '魔法防御精通(III) Lv1', 12, 0, 0, 7, '减少受到魔法攻击伤害.', '', 150, 255, 1, 'skillicon01.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (293, '魔法防御精通(III) Lv2', 12, 0, 0, 7, '减少受到魔法攻击伤害.', '', 150, 255, 1, 'skillicon01.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (294, '猎豹之眼(II) Lv1', 12, 0, 0, 7, '提高近战攻击命中率.', '', 150, 255, 1, 'skillicon01.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (295, '猎豹之眼(II) Lv2', 12, 0, 0, 7, '提高近战攻击命中率.', '', 150, 255, 1, 'skillicon01.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (296, '飓风之眼(II) Lv1', 12, 0, 0, 7, '提高远程攻击命中率.', '', 150, 255, 1, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (297, '飓风之眼(II) Lv2', 12, 0, 0, 7, '提高远程攻击命中率.', '', 150, 255, 1, 'skillicon01.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (298, '魔法之手(II) Lv1', 12, 0, 0, 7, '提高魔法攻击命中率.', '', 150, 255, 1, 'skillicon01.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (299, '魔法之手(II) Lv2', 12, 0, 0, 7, '提高魔法攻击命中率.', '', 150, 255, 1, 'skillicon01.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (300, '体力增加 Lv1', 12, 0, 0, 10000, '增加HP. \n\nHP +5', '', 150, 255, 1, 'skillicon01.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (301, '体力增加 Lv2', 12, 0, 0, 10000, '增加HP. \n\nHP +10', '', 150, 255, 1, 'skillicon01.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (302, '体力增加 Lv3', 12, 0, 0, 10000, '增加HP. \n\nHP +15', '', 150, 255, 1, 'skillicon01.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (303, '体力增加 Lv4', 12, 0, 0, 10000, '增加HP. \n\nHP +20', '', 150, 255, 1, 'skillicon01.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (304, '体力增加 Lv5', 12, 0, 0, 10000, '增加HP. \n\nHP +25', '', 150, 255, 1, 'skillicon01.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (305, '法力增加 Lv1', 12, 0, 0, 10000, '增加MP. \n\nMP +5', '', 150, 255, 1, 'skillicon01.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (306, '法力增加 Lv2', 12, 0, 0, 10000, '增加MP. \n\nMP +10', '', 150, 255, 1, 'skillicon01.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (307, '法力增加 Lv3', 12, 0, 0, 10000, '增加MP. \n\nMP +15', '', 150, 255, 1, 'skillicon01.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (308, '法力增加 Lv4', 12, 0, 0, 10000, '增加MP. \n\nMP +20', '', 150, 255, 1, 'skillicon01.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (309, '法力增加 Lv5', 12, 0, 0, 10000, '增加MP. \n\nMP +25', '', 150, 255, 1, 'skillicon01.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (310, '精神力增加 Lv1', 12, 0, 0, 10000, '增加HP和MP. \n\nHP +2 \nMP +2', '', 150, 255, 5, 'skillicon01.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (311, '精神力增加 Lv2', 12, 0, 0, 10000, '增加HP和MP. \n\nHP +5 \nMP +5', '', 150, 255, 5, 'skillicon01.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (312, '精神力增加 Lv3', 12, 0, 0, 10000, '增加HP和MP. \n\nHP +7 \nMP +7', '', 150, 255, 5, 'skillicon01.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (313, '精神力增加 Lv4', 12, 0, 0, 10000, '增加HP和MP. \n\nHP +10 \nMP +10', '', 150, 255, 5, 'skillicon01.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (314, '精神力增加 Lv5', 12, 0, 0, 10000, '增加HP和MP. \n\nHP +12 \nMP +12', '', 150, 255, 5, 'skillicon01.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (315, '体力恢复 Lv1', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +1', '', 150, 255, 10, 'skillicon01.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (316, '体力恢复 Lv2', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +2', '', 150, 255, 10, 'skillicon01.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (317, '体力恢复 Lv3', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +3', '', 150, 255, 10, 'skillicon01.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (318, '体力恢复 Lv4', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +4', '', 150, 255, 10, 'skillicon01.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (319, '体力恢复 Lv5', 12, 0, 0, 10000, '增加HP恢复量. \n\nHP恢复 +5', '', 150, 255, 10, 'skillicon01.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (320, '法力恢复 Lv1', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +1', '', 150, 255, 10, 'skillicon01.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (321, '法力恢复 Lv2', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +2', '', 150, 255, 10, 'skillicon01.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (322, '法力恢复 Lv3', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +3', '', 150, 255, 10, 'skillicon01.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (323, '法力恢复 Lv4', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +4', '', 150, 255, 10, 'skillicon01.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (324, '法力恢复 Lv5', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +5', '', 150, 255, 10, 'skillicon01.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (325, '小矮人魔法 Lv1', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重 +25', '', 150, 255, 15, 'skillicon01.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (326, '小矮人魔法 Lv2', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重 +50', '', 150, 255, 15, 'skillicon01.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (327, '小矮人魔法 Lv3', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重 +75', '', 150, 255, 15, 'skillicon01.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (328, '小矮人魔法 Lv4', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重 +100', '', 150, 255, 15, 'skillicon01.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (329, '小矮人魔法 Lv5', 12, 0, 0, 10000, '提升可携带物品的负重. \n\n负重 +125', '', 150, 255, 15, 'skillicon01.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (330, '体力恢复(II) Lv1', 12, 0, 0, 10000, '在负重过重的状态下恢复HP. \n\n无视负重的HP恢复 +1', '', 150, 255, 20, 'skillicon01.dds', 192, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (331, '体力恢复(II) Lv2', 12, 0, 0, 10000, '在负重过重的状态下恢复HP. \n\n无视负重的HP恢复 +2', '', 150, 255, 20, 'skillicon01.dds', 192, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (332, '体力恢复(II) Lv3', 12, 0, 0, 10000, '在负重过重的状态下恢复HP. \n\n无视负重的HP恢复 +3', '', 150, 255, 20, 'skillicon01.dds', 192, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (333, '法力恢复(II) Lv1', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +1', '', 150, 255, 20, 'skillicon01.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (334, '法力恢复(II) Lv2', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +2', '', 150, 255, 20, 'skillicon01.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (335, '法力恢复(II) Lv3', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +3', '', 150, 255, 20, 'skillicon01.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (336, '变身精通 Lv1', 12, 0, 0, 10000, '延长变身效果时间 \n\n变身持续时间 +2分钟', '', 150, 255, 25, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (337, '变身精通 Lv2', 12, 0, 0, 10000, '延长变身效果时间 \n\n变身持续时间 +4分钟', '', 150, 255, 25, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (338, '变身精通 Lv3', 12, 0, 0, 10000, '延长变身效果时间 \n\n变身持续时间 +6分钟', '', 150, 255, 25, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (339, '变身精通 Lv4', 12, 0, 0, 10000, '延长变身效果时间 \n\n变身持续时间 +8分钟', '', 150, 255, 25, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (340, '变身精通 Lv5', 12, 0, 0, 10000, '延长变身效果时间 \n\n变身持续时间 +10分钟', '', 150, 255, 25, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (341, '英雄之力 Lv1', 12, 0, 0, 10000, '提升力量. \n\n力量 +1', '', 150, 255, 30, 'skillicon01.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (342, '英雄之力 Lv2', 12, 0, 0, 10000, '提升力量. \n\n力量 +2', '', 150, 255, 30, 'skillicon01.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (343, '英雄之力 Lv3', 12, 0, 0, 10000, '提升力量. \n\n力量 +3', '', 150, 255, 30, 'skillicon01.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (344, '猎人敏捷 Lv1', 12, 0, 0, 10000, '提升敏捷. \n\n敏捷 +1', '', 150, 255, 30, 'skillicon01.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (345, '猎人敏捷 Lv2', 12, 0, 0, 10000, '提升敏捷. \n\n敏捷 +2', '', 150, 255, 30, 'skillicon01.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (346, '猎人敏捷 Lv3', 12, 0, 0, 10000, '提升敏捷. \n\n敏捷 +3', '', 150, 255, 30, 'skillicon01.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (347, '贤者智慧 Lv1', 12, 0, 0, 10000, '提升智力 \n\n智力 +1', '', 150, 255, 30, 'skillicon01.dds', 432, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (348, '贤者智慧 Lv2', 12, 0, 0, 10000, '提升智力 \n\n智力 +2', '', 150, 255, 30, 'skillicon01.dds', 432, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (349, '贤者智慧 Lv3', 12, 0, 0, 10000, '提升智力 \n\n智力 +3', '', 150, 255, 30, 'skillicon01.dds', 432, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (350, '重击精通 Lv1', 12, 0, 0, 10000, '减少使用重击时的MP消耗量.\n\n使用重击消耗 MP -1', '', 150, 1, 35, 'skillicon01.dds', 0, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (351, '重击精通 Lv2', 12, 0, 0, 10000, '减少使用重击时的MP消耗量.\n\n使用重击消耗 MP -2', '', 150, 1, 35, 'skillicon01.dds', 0, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (352, '重击精通 Lv3', 12, 0, 0, 10000, '减少使用重击时的MP消耗量.\n\n使用重击消耗 MP -3', '', 150, 1, 35, 'skillicon01.dds', 0, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (353, '攻击精通(I) Lv1', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 1, 35, 'skillicon01.dds', 48, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (354, '攻击精通(I) Lv2', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 1, 35, 'skillicon01.dds', 48, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (355, '攻击精通(I) Lv3', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 1, 35, 'skillicon01.dds', 48, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (356, '攻击精通(I) Lv4', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 1, 35, 'skillicon01.dds', 48, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (357, '攻击精通(I) Lv5', 12, 0, 0, 10000, '提高近战攻击力.', '', 150, 1, 35, 'skillicon01.dds', 48, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (358, '重击强化 Lv1', 12, 0, 0, 10000, '使用重击时伤害值增加.', '', 150, 1, 40, 'skillicon01.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (359, '重击强化 Lv2', 12, 0, 0, 10000, '使用重击时伤害值增加.', '', 150, 1, 40, 'skillicon01.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (360, '重击强化 Lv3', 12, 0, 0, 10000, '使用重击时伤害值增加.', '', 150, 1, 40, 'skillicon01.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (361, '重击强化 Lv4', 12, 0, 0, 10000, '使用重击时伤害值增加.', '', 150, 1, 40, 'skillicon01.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (362, '重击强化 Lv5', 12, 0, 0, 10000, '使用重击时伤害值增加.', '', 150, 1, 40, 'skillicon01.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (363, '猛力攻击精通 Lv1', 12, 0, 0, 10000, '减少使用猛烈攻击时的MP消耗量.\n\n使用猛烈攻击消耗 MP -1', '', 150, 1, 40, 'skillicon01.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (364, '猛力攻击精通 Lv2', 12, 0, 0, 10000, '减少使用猛烈攻击时的MP消耗量.\n\n使用猛烈攻击消耗 MP -2', '', 150, 1, 40, 'skillicon01.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (365, '猛力攻击精通 Lv3', 12, 0, 0, 10000, '减少使用猛烈攻击时的MP消耗量.\n\n使用猛烈攻击消耗 MP -3', '', 150, 1, 40, 'skillicon01.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (366, '攻击精通(II) Lv1', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 1, 40, 'skillicon01.dds', 192, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (367, '攻击精通(II) Lv2', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 1, 40, 'skillicon01.dds', 192, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (368, '攻击精通(II) Lv3', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 1, 40, 'skillicon01.dds', 192, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (369, '攻击精通(II) Lv4', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 1, 40, 'skillicon01.dds', 192, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (370, '攻击精通(II) Lv5', 12, 0, 0, 10000, '提高近战攻击命中率.', '', 150, 1, 40, 'skillicon01.dds', 192, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (371, '强化猛力攻击 Lv1', 12, 0, 0, 10000, '使用猛力攻击时追加性的增加力量.\n\n使用猛力攻击时增加力量+1', '', 150, 1, 45, 'skillicon01.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (372, '强化猛力攻击 Lv2', 12, 0, 0, 10000, '使用猛力攻击时追加性的增加力量.\n\n使用猛力攻击时增加力量+2', '', 150, 1, 45, 'skillicon01.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (373, '强化猛力攻击 Lv3', 12, 0, 0, 10000, '使用猛力攻击时追加性的增加力量.\n\n使用猛力攻击时增加力量+3', '', 150, 1, 45, 'skillicon01.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (374, '暴走精通 Lv1', 12, 0, 0, 10000, '减少使用暴走时的MP消耗量. \n\n使用暴走消耗 MP -1', '', 150, 1, 45, 'skillicon01.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (375, '暴走精通 Lv2', 12, 0, 0, 10000, '减少使用暴走时的MP消耗量. \n\n使用暴走消耗 MP -2', '', 150, 1, 45, 'skillicon01.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (376, '暴走精通 Lv3', 12, 0, 0, 10000, '减少使用暴走时的MP消耗量. \n\n使用暴走消耗 MP -3', '', 150, 1, 45, 'skillicon01.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (377, '攻击精通(III) Lv1', 12, 0, 0, 10000, '增加力量.', '', 150, 1, 45, 'skillicon01.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (378, '攻击精通(III) Lv2', 12, 0, 0, 10000, '增加力量.', '', 150, 1, 45, 'skillicon01.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (379, '攻击精通(III) Lv3', 12, 0, 0, 10000, '增加力量.', '', 150, 1, 45, 'skillicon01.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (380, '攻击精通(III) Lv4', 12, 0, 0, 10000, '增加力量.', '', 150, 1, 45, 'skillicon01.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (381, '攻击精通(III) Lv5', 12, 0, 0, 10000, '增加力量.', '', 150, 1, 45, 'skillicon01.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (382, '三阶攻击 Lv1', 12, 0, 0, 10000, '使用重击技能时一定几率追加暴击攻击伤害值.', '', 150, 1, 50, 'skillicon01.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (383, '三阶攻击 Lv2', 12, 0, 0, 10000, '使用重击技能时一定几率追加暴击攻击伤害值.', '', 150, 1, 50, 'skillicon01.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (384, '三阶攻击 Lv3', 12, 0, 0, 10000, '使用重击技能时一定几率追加暴击攻击伤害值.', '', 150, 1, 50, 'skillicon01.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (385, '三阶攻击 Lv4', 12, 0, 0, 10000, '使用重击技能时一定几率追加暴击攻击伤害值.', '', 150, 1, 50, 'skillicon01.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (386, '三阶攻击 Lv5', 12, 0, 0, 10000, '使用重击技能时一定几率追加暴击攻击伤害值.', '', 150, 1, 50, 'skillicon01.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (387, '暴走强化 Lv1', 12, 0, 0, 10000, '使用暴走技能时将减少防御的效果降低1点.', '', 150, 1, 50, 'skillicon01.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (388, '暴走强化 Lv2', 12, 0, 0, 10000, '使用暴走技能时将减少防御的效果降低2点.', '', 150, 1, 50, 'skillicon01.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (389, '暴走强化 Lv3', 12, 0, 0, 10000, '使用暴走技能时将减少防御的效果降低3点.', '', 150, 1, 50, 'skillicon01.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (390, '暴走强化 Lv4', 12, 0, 0, 10000, '使用暴走技能时将减少防御的效果降低4点.', '', 150, 1, 50, 'skillicon01.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (391, '痛苦麻痹精通 Lv1', 12, 0, 0, 10000, '减少使用疼痛麻痹时的MP消耗量.\n\n使用疼痛麻痹消耗 -1', '', 150, 1, 35, 'skillicon01.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (392, '痛苦麻痹精通 Lv2', 12, 0, 0, 10000, '减少使用疼痛麻痹时的MP消耗量.\n\n使用疼痛麻痹消耗 -2', '', 150, 1, 35, 'skillicon01.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (393, '痛苦麻痹精通 Lv3', 12, 0, 0, 10000, '减少使用疼痛麻痹时的MP消耗量.\n\n使用疼痛麻痹消耗 -3', '', 150, 1, 35, 'skillicon01.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (394, '痛苦麻痹精通 Lv4', 12, 0, 0, 10000, '减少使用疼痛麻痹时的MP消耗量.\n\n使用疼痛麻痹消耗 -4', '', 150, 1, 35, 'skillicon01.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (395, '痛苦麻痹精通 Lv5', 12, 0, 0, 10000, '减少使用疼痛麻痹时的MP消耗量.\n\n使用疼痛麻痹消耗 -5', '', 150, 1, 35, 'skillicon01.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (396, '防御精通(I) Lv1', 12, 0, 0, 10000, '近战攻击回避率提高.', '', 150, 1, 35, 'skillicon01.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (397, '防御精通(I) Lv2', 12, 0, 0, 10000, '近战攻击回避率提高.', '', 150, 1, 35, 'skillicon01.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (398, '防御精通(I) Lv3', 12, 0, 0, 10000, '近战攻击回避率提高.', '', 150, 1, 35, 'skillicon01.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (399, '防御精通(I) Lv4', 12, 0, 0, 10000, '近战攻击回避率提高.', '', 150, 1, 35, 'skillicon01.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (400, '防御精通(I) Lv5', 12, 0, 0, 10000, '近战攻击回避率提高.', '', 150, 1, 35, 'skillicon01.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (401, '痛苦麻痹强化 Lv1', 12, 0, 0, 10000, '使用疼痛麻痹技能时增加防御力1.', '', 150, 1, 40, 'skillicon01.dds', 96, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (402, '痛苦麻痹强化 Lv2', 12, 0, 0, 10000, '使用疼痛麻痹技能时增加防御力2.', '', 150, 1, 40, 'skillicon01.dds', 96, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (403, '痛苦麻痹强化 Lv3', 12, 0, 0, 10000, '使用疼痛麻痹技能时增加防御力3.', '', 150, 1, 40, 'skillicon01.dds', 96, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (404, '吸收 Lv1', 12, 0, 0, 10000, '药剂恢复量提高1.', '', 150, 1, 40, 'skillicon01.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (405, '吸收 Lv2', 12, 0, 0, 10000, '药剂恢复量提高2.', '', 150, 1, 40, 'skillicon01.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (406, '吸收 Lv3', 12, 0, 0, 10000, '药剂恢复量提高3.', '', 150, 1, 40, 'skillicon01.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (407, '防御精通(II) Lv1', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 1, 40, 'skillicon01.dds', 192, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (408, '防御精通(II) Lv2', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 1, 40, 'skillicon01.dds', 192, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (409, '防御精通(II) Lv3', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 1, 40, 'skillicon01.dds', 192, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (410, '防御精通(II) Lv4', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 1, 40, 'skillicon01.dds', 192, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (411, '防御精通(II) Lv5', 12, 0, 0, 10000, '减少受到近战攻击伤害.', '', 150, 1, 40, 'skillicon01.dds', 192, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (412, '生命绽放精通 Lv1', 12, 0, 0, 10000, '减少使用生命绽放 时的MP消耗量.\n\n使用生命绽放消耗MP -1', '', 150, 1, 45, 'skillicon01.dds', 240, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (413, '生命绽放精通 Lv2', 12, 0, 0, 10000, '减少使用生命绽放 时的MP消耗量.\n\n使用生命绽放消耗MP -2', '', 150, 1, 45, 'skillicon01.dds', 240, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (414, '生命绽放精通 Lv3', 12, 0, 0, 10000, '减少使用生命绽放 时的MP消耗量.\n\n使用生命绽放消耗MP -3', '', 150, 1, 45, 'skillicon01.dds', 240, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (415, '生命绽放精通 Lv4', 12, 0, 0, 10000, '减少使用生命绽放 时的MP消耗量.\n\n使用生命绽放消耗MP -4', '', 150, 1, 45, 'skillicon01.dds', 240, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (416, '生命绽放精通 Lv5', 12, 0, 0, 10000, '减少使用生命绽放 时的MP消耗量.\n\n使用生命绽放消耗MP -5', '', 150, 1, 45, 'skillicon01.dds', 240, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (417, '最后的抵抗 Lv1', 13, 1, 0, 10000, '短时间内将防御力极速提升.', '感觉可以忍受住任何攻击.', 150, 1, 45, 'skillicon01.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (418, '最后的抵抗 Lv2', 13, 1, 0, 10000, '短时间内将防御力极速提升.', '感觉可以忍受住任何攻击.', 150, 1, 45, 'skillicon01.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (419, '最后的抵抗 Lv3', 13, 1, 0, 10000, '短时间内将防御力极速提升.', '感觉可以忍受住任何攻击.', 150, 1, 45, 'skillicon01.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (420, '最后的抵抗 Lv4', 13, 1, 0, 10000, '短时间内将防御力极速提升.', '感觉可以忍受住任何攻击.', 150, 1, 45, 'skillicon01.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (421, '最后的抵抗 Lv5', 13, 1, 0, 10000, '短时间内将防御力极速提升.', '感觉可以忍受住任何攻击.', 150, 1, 45, 'skillicon01.dds', 336, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (422, '防御精通(III) Lv1', 12, 0, 0, 10000, '提高回避率.', '', 150, 1, 45, 'skillicon01.dds', 384, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (423, '防御精通(III) Lv2', 12, 0, 0, 10000, '提高回避率.', '', 150, 1, 45, 'skillicon01.dds', 384, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (424, '防御精通(III) Lv3', 12, 0, 0, 10000, '提高回避率.', '', 150, 1, 45, 'skillicon01.dds', 384, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (425, '防御精通(III) Lv4', 12, 0, 0, 10000, '提高回避率.', '', 150, 1, 45, 'skillicon01.dds', 384, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (426, '防御精通(III) Lv5', 12, 0, 0, 10000, '提高回避率.', '', 150, 1, 45, 'skillicon01.dds', 384, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (427, '生命绽放强化 Lv1', 12, 0, 0, 10000, '增加生命绽放的持续时间1分钟.', '', 150, 1, 50, 'skillicon01.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (428, '生命绽放强化 Lv2', 12, 0, 0, 10000, '增加生命绽放的持续时间2分钟.', '', 150, 1, 50, 'skillicon01.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (429, '生命绽放强化 Lv3', 12, 0, 0, 10000, '增加生命绽放的持续时间3分钟.', '', 150, 1, 50, 'skillicon01.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (430, '生命绽放强化 Lv4', 12, 0, 0, 10000, '增加生命绽放的持续时间4分钟.', '', 150, 1, 50, 'skillicon01.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (431, '生命绽放强化 Lv5', 12, 0, 0, 10000, '增加生命绽放的持续时间5分钟.', '', 150, 1, 50, 'skillicon01.dds', 288, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (432, '魔法抗体', 13, 1, 0, 10000, '持续时间内无视所有魔法攻击.', '可以抵抗所有技能和魔法.', 150, 1, 50, 'skillicon01.dds', 432, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (433, '瞄准射击精通 Lv1', 12, 0, 0, 10000, '减少使用瞄准射击时的MP消耗量.\n\n 使用瞄准射击 消耗MP -1', '', 1500, 2, 35, 'skillicon01.dds', 0, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (434, '瞄准射击精通 Lv2', 12, 0, 0, 10000, '减少使用瞄准射击时的MP消耗量.\n\n 使用瞄准射击 消耗MP -2', '', 1500, 2, 35, 'skillicon01.dds', 0, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (435, '瞄准射击精通 Lv3', 12, 0, 0, 10000, '减少使用瞄准射击时的MP消耗量.\n\n 使用瞄准射击 消耗MP -3', '', 1500, 2, 35, 'skillicon01.dds', 0, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (436, '远程攻击精通(I) Lv1', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 2, 35, 'skillicon01.dds', 48, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (437, '远程攻击精通(I) Lv2', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 2, 35, 'skillicon01.dds', 48, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (438, '远程攻击精通(I) Lv3', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 2, 35, 'skillicon01.dds', 48, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (439, '远程攻击精通(I) Lv4', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 2, 35, 'skillicon01.dds', 48, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (440, '远程攻击精通(I) Lv5', 12, 0, 0, 10000, '提高远程攻击力.', '', 150, 2, 35, 'skillicon01.dds', 48, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (441, '瞄准射击强化 Lv1', 12, 0, 0, 10000, '强化瞄准射击的攻击力.', '', 1500, 2, 40, 'skillicon01.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (442, '瞄准射击强化 Lv2', 12, 0, 0, 10000, '强化瞄准射击的攻击力.', '', 1500, 2, 40, 'skillicon01.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (443, '瞄准射击强化 Lv3', 12, 0, 0, 10000, '强化瞄准射击的攻击力.', '', 1500, 2, 40, 'skillicon01.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (444, '瞄准射击强化 Lv4', 12, 0, 0, 10000, '强化瞄准射击的攻击力.', '', 1500, 2, 40, 'skillicon01.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (445, '瞄准射击强化 Lv5', 12, 0, 0, 10000, '强化瞄准射击的攻击力.', '', 1500, 2, 40, 'skillicon01.dds', 96, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (446, '音速屏障精通 Lv1', 12, 0, 0, 10000, '减少使用音速屏障时的MP消耗量.\n\n使用音速屏障消耗 MP -1', '', 150, 2, 40, 'skillicon01.dds', 144, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (447, '音速屏障精通 Lv2', 12, 0, 0, 10000, '减少使用音速屏障时的MP消耗量.\n\n使用音速屏障消耗 MP -2', '', 150, 2, 40, 'skillicon01.dds', 144, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (448, '音速屏障精通 Lv3', 12, 0, 0, 10000, '减少使用音速屏障时的MP消耗量.\n\n使用音速屏障消耗 MP -3', '', 150, 2, 40, 'skillicon01.dds', 144, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (449, '火焰箭 Lv1', 13, 20, 0, 10000, '将火焰箭的异常状态附加给对方. \n\n受到攻击目标每秒持续减少HP \n\n火焰箭1级可使用', '射出正在燃烧的箭.', 1500, 2, 45, 'skillicon01.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (450, '火焰箭 Lv2', 13, 20, 0, 10000, '将火焰箭的异常状态附加给对方. \n\n受到攻击目标每秒持续减少HP \n\n火焰箭2级可使用', '射出正在燃烧的箭.', 1500, 2, 45, 'skillicon01.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (451, '火焰箭 Lv3', 13, 20, 0, 10000, '将火焰箭的异常状态附加给对方. \n\n受到攻击目标每秒持续减少HP \n\n火焰箭3级可使用', '射出正在燃烧的箭.', 1500, 2, 45, 'skillicon01.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (452, '火焰箭 Lv4', 13, 20, 0, 10000, '将火焰箭的异常状态附加给对方. \n\n受到攻击目标每秒持续减少HP \n\n火焰箭4级可使用', '射出正在燃烧的箭.', 1500, 2, 45, 'skillicon01.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (453, '火焰箭 Lv5', 13, 20, 0, 10000, '将火焰箭的异常状态附加给对方. \n\n受到攻击目标每秒持续减少HP \n\n火焰箭5级可使用', '射出正在燃烧的箭.', 1500, 2, 45, 'skillicon01.dds', 192, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (454, '音速屏障强化 Lv1', 12, 0, 0, 10000, '增加音速屏障的持续时间.\n\n音速屏障持续时间 +10秒', '', 150, 2, 45, 'skillicon01.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (455, '音速屏障强化 Lv2', 12, 0, 0, 10000, '增加音速屏障的持续时间.\n\n音速屏障持续时间 +20秒', '', 150, 2, 45, 'skillicon01.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (456, '音速屏障强化 Lv3', 12, 0, 0, 10000, '增加音速屏障的持续时间.\n\n音速屏障持续时间 +30秒', '', 150, 2, 45, 'skillicon01.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (457, '音速屏障强化 Lv4', 12, 0, 0, 10000, '增加音速屏障的持续时间.\n\n音速屏障持续时间 +40秒', '', 150, 2, 45, 'skillicon01.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (458, '音速屏障强化 Lv5', 12, 0, 0, 10000, '增加音速屏障的持续时间.\n\n音速屏障持续时间 +50秒', '', 150, 2, 45, 'skillicon01.dds', 240, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (459, '疾速射击精通 Lv1', 12, 0, 0, 10000, '使用疾速射击时减少MP的消耗量.\n\n使用疾速射击消耗 MP -1', '', 150, 2, 40, 'skillicon01.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (460, '疾速射击精通 Lv2', 12, 0, 0, 10000, '使用疾速射击时减少MP的消耗量.\n\n使用疾速射击消耗 MP -2', '', 150, 2, 40, 'skillicon01.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (461, '疾速射击精通 Lv3', 12, 0, 0, 10000, '使用疾速射击时减少MP的消耗量.\n\n使用疾速射击消耗 MP -3', '', 150, 2, 40, 'skillicon01.dds', 288, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (462, '远程攻击精通(II) Lv1', 12, 0, 0, 10000, '提升远程攻击命中率.', '', 150, 2, 45, 'skillicon01.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (463, '远程攻击精通(II) Lv2', 12, 0, 0, 10000, '提升远程攻击命中率.', '', 150, 2, 45, 'skillicon01.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (464, '远程攻击精通(II) Lv3', 12, 0, 0, 10000, '提升远程攻击命中率.', '', 150, 2, 45, 'skillicon01.dds', 336, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (465, '爆炸 Lv1', 13, 20, 0, 10000, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人施加附加伤害.', '射出带有爆炸物的箭矢.', 1500, 2, 50, 'skillicon01.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (466, '爆炸 Lv2', 13, 20, 0, 10000, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人施加附加伤害.', '射出带有爆炸物的箭矢.', 1500, 2, 50, 'skillicon01.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (467, '爆炸 Lv3', 13, 20, 0, 10000, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人施加附加伤害.', '射出带有爆炸物的箭矢.', 1500, 2, 50, 'skillicon01.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (468, '爆炸 Lv4', 13, 20, 0, 10000, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人施加附加伤害.', '射出带有爆炸物的箭矢.', 1500, 2, 50, 'skillicon01.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (469, '爆炸 Lv5', 13, 20, 0, 10000, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人施加附加伤害.', '射出带有爆炸物的箭矢.', 1500, 2, 50, 'skillicon01.dds', 384, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (470, '强化疾速射击 Lv1', 12, 0, 0, 10000, '增加疾速射击的持续时间.\n\n疾速射击持续时间 +10秒', '', 150, 2, 45, 'skillicon01.dds', 432, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (471, '强化疾速射击 Lv2', 12, 0, 0, 10000, '增加疾速射击的持续时间.\n\n疾速射击持续时间 +20秒', '', 150, 2, 45, 'skillicon01.dds', 432, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (472, '强化疾速射击 Lv3', 12, 0, 0, 10000, '增加疾速射击的持续时间.\n\n疾速射击持续时间 +30秒', '', 150, 2, 45, 'skillicon01.dds', 432, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (473, '强化疾速射击 Lv4', 12, 0, 0, 10000, '增加疾速射击的持续时间.\n\n疾速射击持续时间 +40秒', '', 150, 2, 45, 'skillicon01.dds', 432, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (474, '强化疾速射击 Lv5', 12, 0, 0, 10000, '增加疾速射击的持续时间.\n\n疾速射击持续时间 +50秒', '', 150, 2, 45, 'skillicon01.dds', 432, 336, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (475, '牵制 Lv1', 12, 0, 0, 10000, '提高对远程攻击和魔法攻击的回避率.', '', 150, 2, 35, 'skillicon01.dds', 0, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (476, '牵制 Lv2', 12, 0, 0, 10000, '提高对远程攻击和魔法攻击的回避率.', '', 150, 2, 35, 'skillicon01.dds', 0, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (477, '牵制 Lv3', 12, 0, 0, 10000, '提高对远程攻击和魔法攻击的回避率.', '', 150, 2, 35, 'skillicon01.dds', 0, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (478, '牵制 Lv4', 12, 0, 0, 10000, '提高对远程攻击和魔法攻击的回避率.', '', 150, 2, 35, 'skillicon01.dds', 0, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (479, '牵制 Lv5', 12, 0, 0, 10000, '提高对远程攻击和魔法攻击的回避率.', '', 150, 2, 35, 'skillicon01.dds', 0, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (480, '移动速度减少抵抗 Lv1', 12, 0, 0, 10000, '提高躲避振荡射击的成功率.', '', 150, 2, 35, 'skillicon01.dds', 48, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (481, '移动速度减少抵抗 Lv2', 12, 0, 0, 10000, '提高躲避振荡射击的成功率.', '', 150, 2, 35, 'skillicon01.dds', 48, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (482, '移动速度减少抵抗 Lv3', 12, 0, 0, 10000, '提高躲避振荡射击的成功率.', '', 150, 2, 35, 'skillicon01.dds', 48, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (483, '移动速度减少抵抗 Lv4', 12, 0, 0, 10000, '提高躲避振荡射击的成功率.', '', 150, 2, 35, 'skillicon01.dds', 48, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (484, '移动速度减少抵抗 Lv5', 12, 0, 0, 10000, '提高躲避振荡射击的成功率.', '', 150, 2, 35, 'skillicon01.dds', 48, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (485, '振荡射击精通 Lv1', 12, 0, 0, 10000, '使用振荡射击时减少MP消耗量.\n\n使用振荡射击消耗 MP -1', '', 1500, 2, 40, 'skillicon01.dds', 96, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (486, '振荡射击精通 Lv2', 12, 0, 0, 10000, '使用振荡射击时减少MP消耗量.\n\n使用振荡射击消耗 MP -2', '', 1500, 2, 40, 'skillicon01.dds', 96, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (487, '振荡射击精通 Lv3', 12, 0, 0, 10000, '使用振荡射击时减少MP消耗量.\n\n使用振荡射击消耗 MP -3', '', 1500, 2, 40, 'skillicon01.dds', 96, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (488, '巨鹰之眼 Lv1', 12, 0, 0, 10000, '增加远程攻击的命中率', '', 150, 2, 40, 'skillicon01.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (489, '巨鹰之眼 Lv2', 12, 0, 0, 10000, '增加远程攻击的命中率', '', 150, 2, 40, 'skillicon01.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (490, '巨鹰之眼 Lv3', 12, 0, 0, 10000, '增加远程攻击的命中率', '', 150, 2, 40, 'skillicon01.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (491, '无法移动抵抗 Lv1', 12, 0, 0, 10000, '提高回避昏厥攻击和蛛网术的几率.', '', 150, 2, 40, 'skillicon01.dds', 192, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (492, '无法移动抵抗 Lv2', 12, 0, 0, 10000, '提高回避昏厥攻击和蛛网术的几率.', '', 150, 2, 40, 'skillicon01.dds', 192, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (493, '无法移动抵抗 Lv3', 12, 0, 0, 10000, '提高回避昏厥攻击和蛛网术的几率.', '', 150, 2, 40, 'skillicon01.dds', 192, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (494, '无法移动抵抗 Lv4', 12, 0, 0, 10000, '提高回避昏厥攻击和蛛网术的几率.', '', 150, 2, 40, 'skillicon01.dds', 192, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (495, '无法移动抵抗 Lv5', 12, 0, 0, 10000, '提高回避昏厥攻击和蛛网术的几率.', '', 150, 2, 40, 'skillicon01.dds', 192, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (496, '振荡射击瞄准 Lv1', 12, 0, 0, 10000, '提高振荡射击的命中率.', '', 1500, 2, 45, 'skillicon01.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (497, '振荡射击瞄准 Lv2', 12, 0, 0, 10000, '提高振荡射击的命中率.', '', 1500, 2, 45, 'skillicon01.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (498, '振荡射击瞄准 Lv3', 12, 0, 0, 10000, '提高振荡射击的命中率.', '', 1500, 2, 45, 'skillicon01.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (499, '振荡射击瞄准 Lv4', 12, 0, 0, 10000, '提高振荡射击的命中率.', '', 1500, 2, 45, 'skillicon01.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (500, '振荡射击瞄准 Lv5', 12, 0, 0, 10000, '提高振荡射击的命中率.', '', 1500, 2, 45, 'skillicon01.dds', 240, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (501, '振荡射击强化 Lv1', 12, 0, 0, 10000, '使用振荡射击时增大被击对象的移动速度的减少幅度.', '', 1500, 2, 45, 'skillicon01.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (502, '振荡射击强化 Lv2', 12, 0, 0, 10000, '使用振荡射击时增大被击对象的移动速度的减少幅度.', '', 1500, 2, 45, 'skillicon01.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (503, '振荡射击强化 Lv3', 12, 0, 0, 10000, '使用振荡射击时增大被击对象的移动速度的减少幅度.', '', 1500, 2, 45, 'skillicon01.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (504, '振荡射击强化 Lv4', 12, 0, 0, 10000, '使用振荡射击时增大被击对象的移动速度的减少幅度.', '', 1500, 2, 45, 'skillicon01.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (505, '振荡射击强化 Lv5', 12, 0, 0, 10000, '使用振荡射击时增大被击对象的移动速度的减少幅度.', '', 1500, 2, 45, 'skillicon01.dds', 288, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (506, '钢铁陷阱 Lv1', 13, 1, 0, 10000, '设置给予对象伤害的陷阱.', '设置陷阱.', 150, 2, 45, 'skillicon01.dds', 336, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (507, '钢铁陷阱 Lv2', 13, 1, 0, 10000, '设置给予对象伤害的陷阱.', '设置陷阱.', 150, 2, 45, 'skillicon01.dds', 336, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (508, '钢铁陷阱 Lv3', 13, 1, 0, 10000, '设置给予对象伤害的陷阱.', '设置陷阱.', 150, 2, 45, 'skillicon01.dds', 336, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (509, '钢铁陷阱 Lv4', 13, 1, 0, 10000, '设置给予对象伤害的陷阱.', '设置陷阱.', 150, 2, 45, 'skillicon01.dds', 336, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (510, '钢铁陷阱 Lv5', 13, 1, 0, 10000, '设置给予对象伤害的陷阱.', '设置陷阱.', 150, 2, 45, 'skillicon01.dds', 336, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (511, '粘性陷阱 Lv1', 13, 1, 0, 10000, '设置减少对象移动速度的陷阱.', '设置陷阱.', 150, 2, 50, 'skillicon01.dds', 384, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (512, '粘性陷阱 Lv2', 13, 1, 0, 10000, '设置减少对象移动速度的陷阱.', '设置陷阱.', 150, 2, 50, 'skillicon01.dds', 384, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (513, '粘性陷阱 Lv3', 13, 1, 0, 10000, '设置减少对象移动速度的陷阱.', '设置陷阱.', 150, 2, 50, 'skillicon01.dds', 384, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (514, '粘性陷阱 Lv4', 13, 1, 0, 10000, '设置减少对象移动速度的陷阱.', '设置陷阱.', 150, 2, 50, 'skillicon01.dds', 384, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (515, '粘性陷阱 Lv5', 13, 1, 0, 10000, '设置减少对象移动速度的陷阱.', '设置陷阱.', 150, 2, 50, 'skillicon01.dds', 384, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (516, '强化魔法盾 Lv1', 12, 0, 0, 10000, '使用魔法盾时额外增加防御力.\n\n使用魔法盾增加防御力 +1', '', 1500, 4, 35, 'skillicon01.dds', 432, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (517, '强化魔法盾 Lv2', 12, 0, 0, 10000, '使用魔法盾时额外增加防御力.\n\n使用魔法盾增加防御力 +2', '', 1500, 4, 35, 'skillicon01.dds', 432, 384, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (518, '灵魂之力 Lv1', 12, 0, 0, 10000, '增加MP. \n\nMP +5', '', 150, 4, 35, 'skillicon01.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (519, '灵魂之力 Lv2', 12, 0, 0, 10000, '增加MP. \n\nMP +10', '', 150, 4, 35, 'skillicon01.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (520, '灵魂之力 Lv3', 12, 0, 0, 10000, '增加MP. \n\nMP +15', '', 150, 4, 35, 'skillicon01.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (521, '灵魂之力 Lv4', 12, 0, 0, 10000, '增加MP. \n\nMP +20', '', 150, 4, 35, 'skillicon01.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (522, '灵魂之力 Lv5', 12, 0, 0, 10000, '增加MP. \n\nMP +25', '', 150, 4, 35, 'skillicon01.dds', 0, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (523, '熟练剑术精通 Lv1', 12, 0, 0, 10000, '增加剑术精通的持续时间.\n\n剑术精通持续时间 +1分钟', '', 150, 4, 35, 'skillicon01.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (524, '熟练剑术精通 Lv2', 12, 0, 0, 10000, '增加剑术精通的持续时间.\n\n剑术精通持续时间 +2分钟', '', 150, 4, 35, 'skillicon01.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (525, '熟练剑术精通 Lv3', 12, 0, 0, 10000, '增加剑术精通的持续时间.\n\n剑术精通持续时间 +3分钟', '', 150, 4, 35, 'skillicon01.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (526, '熟练剑术精通 Lv4', 12, 0, 0, 10000, '增加剑术精通的持续时间.\n\n剑术精通持续时间 +4分钟', '', 150, 4, 35, 'skillicon01.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (527, '熟练剑术精通 Lv5', 12, 0, 0, 10000, '增加剑术精通的持续时间.\n\n剑术精通持续时间 +5分钟', '', 150, 4, 35, 'skillicon01.dds', 48, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (528, '石化皮肤强化 Lv1', 12, 0, 0, 10000, '使用石化皮肤时额外增加防御力.\n\n使用石化皮肤增加防御力 +1', '', 1500, 4, 40, 'skillicon01.dds', 96, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (529, '石化皮肤强化 Lv2', 12, 0, 0, 10000, '使用石化皮肤时额外增加防御力.\n\n使用石化皮肤增加防御力 +2', '', 1500, 4, 40, 'skillicon01.dds', 96, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (530, '精神之力 Lv1', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +1', '', 150, 4, 40, 'skillicon01.dds', 144, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (531, '精神之力 Lv2', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +2', '', 150, 4, 40, 'skillicon01.dds', 144, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (532, '精神之力 Lv3', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +3', '', 150, 4, 40, 'skillicon01.dds', 144, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (533, '精神之力 Lv4', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +4', '', 150, 4, 40, 'skillicon01.dds', 144, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (534, '精神之力 Lv5', 12, 0, 0, 10000, '增加MP恢复量增加. \n\nMP恢复 +5', '', 150, 4, 40, 'skillicon01.dds', 144, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (535, '强化元素铠甲 Lv1', 12, 0, 0, 10000, '使用元素铠甲时增加对魔法攻击的回避几率.', '', 1500, 4, 45, 'skillicon01.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (536, '强化元素铠甲 Lv2', 12, 0, 0, 10000, '使用元素铠甲时增加对魔法攻击的回避几率.', '', 1500, 4, 45, 'skillicon01.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (537, '强化元素铠甲 Lv3', 12, 0, 0, 10000, '使用元素铠甲时增加对魔法攻击的回避几率.', '', 1500, 4, 45, 'skillicon01.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (538, '强化元素铠甲 Lv4', 12, 0, 0, 10000, '使用元素铠甲时增加对魔法攻击的回避几率.', '', 1500, 4, 45, 'skillicon01.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (539, '强化元素铠甲 Lv5', 12, 0, 0, 10000, '使用元素铠甲时增加对魔法攻击的回避几率.', '', 1500, 4, 45, 'skillicon01.dds', 192, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (540, '无视负重的MP增加 Lv1', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +1', '', 150, 4, 45, 'skillicon01.dds', 240, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (541, '无视负重的MP增加 Lv2', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +2', '', 150, 4, 45, 'skillicon01.dds', 240, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (542, '无视负重的MP增加 Lv3', 12, 0, 0, 10000, '在负重过重的状态下恢复MP. \n\n无视负重的MP恢复 +3', '', 150, 4, 45, 'skillicon01.dds', 240, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (543, '强化魔法驱散 Lv1', 12, 0, 0, 10000, '使用魔法驱散时一定几率多解除1个异常状态.', '', 1500, 4, 50, 'skillicon01.dds', 288, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (544, '强化魔法驱散 Lv2', 12, 0, 0, 10000, '使用魔法驱散时一定几率多解除2个异常状态.', '', 1500, 4, 50, 'skillicon01.dds', 288, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (545, '强化魔法驱散 Lv3', 12, 0, 0, 10000, '使用魔法驱散时一定几率多解除3个异常状态.', '', 1500, 4, 50, 'skillicon01.dds', 288, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (546, '法力转换 - HP Lv1', 13, 1, 0, 10000, '消耗自身的所有MP从而增加HP.', '消耗所有的法力值强化体力.', 150, 4, 45, 'skillicon01.dds', 336, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (547, '法力转换 - HP Lv2', 13, 1, 0, 10000, '消耗自身的所有MP从而增加HP.', '消耗所有的法力值强化体力.', 150, 4, 45, 'skillicon01.dds', 336, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (548, '法力转换 - HP Lv3', 13, 1, 0, 10000, '消耗自身的所有MP从而增加HP.', '消耗所有的法力值强化体力.', 150, 4, 45, 'skillicon01.dds', 336, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (549, '强化群体拯救 Lv1', 12, 0, 0, 10000, '使用群体拯救时增加持续时间.\n\n拯救持续时间增加 10秒', '', 150, 4, 50, 'skillicon01.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (550, '强化群体拯救 Lv2', 12, 0, 0, 10000, '使用群体拯救时增加持续时间.\n\n拯救持续时间增加 20秒', '', 150, 4, 50, 'skillicon01.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (551, '强化群体拯救 Lv3', 12, 0, 0, 10000, '使用群体拯救时增加持续时间.\n\n拯救持续时间增加 30秒', '', 150, 4, 50, 'skillicon01.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (552, '强化群体拯救 Lv4', 12, 0, 0, 10000, '使用群体拯救时增加持续时间.\n\n拯救持续时间增加 40秒', '', 150, 4, 50, 'skillicon01.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (553, '强化群体拯救 Lv5', 12, 0, 0, 10000, '使用群体拯救时增加持续时间.\n\n拯救持续时间增加 50秒', '', 150, 4, 50, 'skillicon01.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (554, '法力转换 - attack Lv1', 13, 1, 0, 10000, '消耗自身的所有MP从而增加攻击力.', '消耗所有的法力值提升攻击力.', 150, 4, 50, 'skillicon01.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (555, '法力转换 - attack Lv2', 13, 1, 0, 10000, '消耗自身的所有MP从而增加攻击力.', '消耗所有的法力值提升攻击力.', 150, 4, 50, 'skillicon01.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (556, '法力转换 - attack Lv3', 13, 1, 0, 10000, '消耗自身的所有MP从而增加攻击力.', '消耗所有的法力值提升攻击力.', 150, 4, 50, 'skillicon01.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (557, '强化诅咒 Lv1', 12, 0, 0, 10000, '使用诅咒时增加命中率.', '', 1500, 4, 35, 'skillicon02.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (558, '强化诅咒 Lv2', 12, 0, 0, 10000, '使用诅咒时增加命中率.', '', 1500, 4, 35, 'skillicon02.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (559, '强化诅咒 Lv3', 12, 0, 0, 10000, '使用诅咒时增加命中率.', '', 1500, 4, 35, 'skillicon02.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (560, '强化厄运之触 Lv1', 12, 0, 0, 10000, '使用厄运之触时增加命中率.', '', 1500, 4, 40, 'skillicon02.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (561, '强化厄运之触 Lv2', 12, 0, 0, 10000, '使用厄运之触时增加命中率.', '', 1500, 4, 40, 'skillicon02.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (562, '强化厄运之触 Lv3', 12, 0, 0, 10000, '使用厄运之触时增加命中率.', '', 1500, 4, 40, 'skillicon02.dds', 48, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (563, '强化沉默 Lv1', 12, 0, 0, 10000, '使用沉默时增加命中率.', '', 1500, 4, 40, 'skillicon02.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (564, '强化沉默 Lv2', 12, 0, 0, 10000, '使用沉默时增加命中率.', '', 1500, 4, 40, 'skillicon02.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (565, '强化沉默 Lv3', 12, 0, 0, 10000, '使用沉默时增加命中率.', '', 1500, 4, 40, 'skillicon02.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (566, '强化剧毒之云 Lv1', 12, 0, 0, 10000, '使用剧毒之云时增加命中率.', '', 1500, 4, 45, 'skillicon02.dds', 144, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (567, '强化剧毒之云 Lv2', 12, 0, 0, 10000, '使用剧毒之云时增加命中率.', '', 1500, 4, 45, 'skillicon02.dds', 144, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (568, '强化剧毒之云 Lv3', 12, 0, 0, 10000, '使用剧毒之云时增加命中率.', '', 1500, 4, 45, 'skillicon02.dds', 144, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (569, '魔力增加 Lv1', 12, 0, 0, 10000, '增加魔法攻击的伤害值.', '', 150, 4, 45, 'skillicon02.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (570, '魔力增加 Lv2', 12, 0, 0, 10000, '增加魔法攻击的伤害值.', '', 150, 4, 45, 'skillicon02.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (571, '魔力增加 Lv3', 12, 0, 0, 10000, '增加魔法攻击的伤害值.', '', 150, 4, 45, 'skillicon02.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (572, '魔力增加 Lv4', 12, 0, 0, 10000, '增加魔法攻击的伤害值.', '', 150, 4, 45, 'skillicon02.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (573, '魔力增加 Lv5', 12, 0, 0, 10000, '增加魔法攻击的伤害值.', '', 150, 4, 45, 'skillicon02.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (574, '强化烈焰风暴 Lv1', 12, 0, 0, 10000, '增加烈焰风暴的伤害值.', '', 1500, 4, 45, 'skillicon02.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (575, '强化烈焰风暴 Lv2', 12, 0, 0, 10000, '增加烈焰风暴的伤害值.', '', 1500, 4, 45, 'skillicon02.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (576, '强化烈焰风暴 Lv3', 12, 0, 0, 10000, '增加烈焰风暴的伤害值.', '', 1500, 4, 45, 'skillicon02.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (577, '强化烈焰风暴 Lv4', 12, 0, 0, 10000, '增加烈焰风暴的伤害值.', '', 1500, 4, 45, 'skillicon02.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (578, '强化烈焰风暴 Lv5', 12, 0, 0, 10000, '增加烈焰风暴的伤害值.', '', 1500, 4, 45, 'skillicon02.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (579, '增加集中力 Lv1', 12, 0, 0, 10000, '增加魔法攻击的命中率.', '', 150, 4, 50, 'skillicon02.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (580, '增加集中力 Lv2', 12, 0, 0, 10000, '增加魔法攻击的命中率.', '', 150, 4, 50, 'skillicon02.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (581, '增加集中力 Lv3', 12, 0, 0, 10000, '增加魔法攻击的命中率.', '', 150, 4, 50, 'skillicon02.dds', 288, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (582, '魔力吸食 Lv1', 12, 2, 0, 10000, '减少对方的MP.', '让对方的法力值燃烧掉且变成0.', 1500, 4, 50, 'skillicon02.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (583, '魔力吸食 Lv2', 12, 2, 0, 10000, '减少对方的MP.', '让对方的法力值燃烧掉且变成0.', 1500, 4, 50, 'skillicon02.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (584, '魔力吸食 Lv3', 12, 2, 0, 10000, '减少对方的MP.', '让对方的法力值燃烧掉且变成0.', 1500, 4, 50, 'skillicon02.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (585, '魔力吸食 Lv4', 12, 2, 0, 10000, '减少对方的MP.', '让对方的法力值燃烧掉且变成0.', 1500, 4, 50, 'skillicon02.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (586, '魔力吸食 Lv5', 12, 2, 0, 10000, '减少对方的MP.', '让对方的法力值燃烧掉且变成0.', 1500, 4, 50, 'skillicon02.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (587, '回避精通 Lv1', 12, 0, 0, 10000, '使用回避时减少MP消耗量.\n\n使用回避消耗 MP -1', '', 150, 8, 35, 'skillicon02.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (588, '回避精通 Lv2', 12, 0, 0, 10000, '使用回避时减少MP消耗量.\n\n使用回避消耗 MP -2', '', 150, 8, 35, 'skillicon02.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (589, '回避精通 Lv3', 12, 0, 0, 10000, '使用回避时减少MP消耗量.\n\n使用回避消耗 MP -3', '', 150, 8, 35, 'skillicon02.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (590, '回避精通 Lv4', 12, 0, 0, 10000, '使用回避时减少MP消耗量.\n\n使用回避消耗 MP -4', '', 150, 8, 35, 'skillicon02.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (591, '回避精通 Lv5', 12, 0, 0, 10000, '使用回避时减少MP消耗量.\n\n使用回避消耗 MP -5', '', 150, 8, 35, 'skillicon02.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (592, '暴击 Lv1', 12, 0, 0, 10000, '提高暴击几率.', '', 150, 8, 35, 'skillicon02.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (593, '暴击 Lv2', 12, 0, 0, 10000, '提高暴击几率.', '', 150, 8, 35, 'skillicon02.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (594, '暴击 Lv3', 12, 0, 0, 10000, '提高暴击几率.', '', 150, 8, 35, 'skillicon02.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (595, '暴击 Lv4', 12, 0, 0, 10000, '提高暴击几率.', '', 150, 8, 35, 'skillicon02.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (596, '暴击 Lv5', 12, 0, 0, 10000, '提高暴击几率.', '', 150, 8, 35, 'skillicon02.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (597, '回避强化 Lv1', 12, 0, 0, 10000, '使用回避时增加持续时间.\n\n增加持续时间 +5秒', '', 150, 8, 40, 'skillicon02.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (598, '回避强化 Lv2', 12, 0, 0, 10000, '使用回避时增加持续时间.\n\n增加持续时间 +10秒', '', 150, 8, 40, 'skillicon02.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (599, '回避强化 Lv3', 12, 0, 0, 10000, '使用回避时增加持续时间.\n\n增加持续时间 +15秒', '', 150, 8, 40, 'skillicon02.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (600, '毒牙精通 Lv1', 12, 0, 0, 10000, '使用毒牙时减少MP消耗量.\n\n使用毒牙消耗 MP -1', '', 150, 8, 40, 'skillicon02.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (601, '毒牙精通 Lv2', 12, 0, 0, 10000, '使用毒牙时减少MP消耗量.\n\n使用毒牙消耗 MP -2', '', 150, 8, 40, 'skillicon02.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (602, '毒牙精通 Lv3', 12, 0, 0, 10000, '使用毒牙时减少MP消耗量.\n\n使用毒牙消耗 MP -3', '', 150, 8, 40, 'skillicon02.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (603, '毒牙精通 Lv4', 12, 0, 0, 10000, '使用毒牙时减少MP消耗量.\n\n使用毒牙消耗 MP -4', '', 150, 8, 40, 'skillicon02.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (604, '毒牙精通 Lv5', 12, 0, 0, 10000, '使用毒牙时减少MP消耗量.\n\n使用毒牙消耗 MP -5', '', 150, 8, 40, 'skillicon02.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (605, '暴击伤害 Lv1', 12, 0, 0, 10000, '增加暴击伤害值.', '', 150, 8, 45, 'skillicon02.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (606, '暴击伤害 Lv2', 12, 0, 0, 10000, '增加暴击伤害值.', '', 150, 8, 45, 'skillicon02.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (607, '暴击伤害 Lv3', 12, 0, 0, 10000, '增加暴击伤害值.', '', 150, 8, 45, 'skillicon02.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (608, '暴击伤害 Lv4', 12, 0, 0, 10000, '增加暴击伤害值.', '', 150, 8, 45, 'skillicon02.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (609, '暴击伤害 Lv5', 12, 0, 0, 10000, '增加暴击伤害值.', '', 150, 8, 45, 'skillicon02.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (610, '强化要害攻击 Lv1', 12, 0, 0, 10000, '使用要害攻击时附加强力攻击.', '', 150, 8, 50, 'skillicon02.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (611, '强化要害攻击 Lv2', 12, 0, 0, 10000, '使用要害攻击时附加强力攻击.', '', 150, 8, 50, 'skillicon02.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (612, '强化要害攻击 Lv3', 12, 0, 0, 10000, '使用要害攻击时附加强力攻击.', '', 150, 8, 50, 'skillicon02.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (613, '强化要害攻击 Lv4', 12, 0, 0, 10000, '使用要害攻击时附加强力攻击.', '', 150, 8, 50, 'skillicon02.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (614, '强化要害攻击 Lv5', 12, 0, 0, 10000, '使用要害攻击时附加强力攻击.', '', 150, 8, 50, 'skillicon02.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (615, '昏厥攻击精通 Lv1', 12, 0, 0, 10000, '使用昏厥攻击时减少MP消耗量.\n\n使用昏厥攻击消耗 MP -1', '', 150, 8, 40, 'skillicon02.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (616, '昏厥攻击精通 Lv2', 12, 0, 0, 10000, '使用昏厥攻击时减少MP消耗量.\n\n使用昏厥攻击消耗 MP -2', '', 150, 8, 40, 'skillicon02.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (617, '昏厥攻击精通 Lv3', 12, 0, 0, 10000, '使用昏厥攻击时减少MP消耗量.\n\n使用昏厥攻击消耗 MP -3', '', 150, 8, 40, 'skillicon02.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (618, '昏厥攻击精通 Lv4', 12, 0, 0, 10000, '使用昏厥攻击时减少MP消耗量.\n\n使用昏厥攻击消耗 MP -4', '', 150, 8, 40, 'skillicon02.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (619, '昏厥攻击精通 Lv5', 12, 0, 0, 10000, '使用昏厥攻击时减少MP消耗量.\n\n使用昏厥攻击消耗 MP -5', '', 150, 8, 40, 'skillicon02.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (620, '强化疾行 Lv1', 12, 0, 0, 10000, '使用疾行时延长攻击速度增加的持续时.', '', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (621, '强化疾行 Lv2', 12, 0, 0, 10000, '使用疾行时延长攻击速度增加的持续时.', '', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (622, '强化疾行 Lv3', 12, 0, 0, 10000, '使用疾行时延长攻击速度增加的持续时.', '', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (623, '强化疾行 Lv4', 12, 0, 0, 10000, '使用疾行时延长攻击速度增加的持续时.', '', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (624, '强化疾行 Lv5', 12, 0, 0, 10000, '使用疾行时延长攻击速度增加的持续时.', '', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (625, '解毒 Lv1', 13, 1, 0, 10000, '解除曼陀铃中毒效果.', '解毒.', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (626, '解毒 Lv2', 13, 1, 0, 10000, '在解毒 Lv1的效果上可以追加性的解除狼蛛毒.', '解毒.', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (627, '解毒 Lv3', 13, 1, 0, 10000, '在解毒 Lv2的效果上可以追加性的解除猛毒.', '解毒.', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (628, '解毒 Lv4', 13, 1, 0, 10000, '在解毒 Lv3的效果上可以追加性的解除毒牙.', '解毒.', 150, 8, 35, 'skillicon02.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (629, '暗杀精通 Lv1', 12, 0, 0, 10000, '减少暗杀技能的MP消耗量.\n\n使用暗杀时 MP -1', '', 150, 8, 40, 'skillicon02.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (630, '暗杀精通 Lv2', 12, 0, 0, 10000, '减少暗杀技能的MP消耗量.\n\n使用暗杀时 MP -2', '', 150, 8, 40, 'skillicon02.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (631, '暗杀精通 Lv3', 12, 0, 0, 10000, '减少暗杀技能的MP消耗量.\n\n使用暗杀时 MP -3', '', 150, 8, 40, 'skillicon02.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (632, '暗杀精通 Lv4', 12, 0, 0, 10000, '减少暗杀技能的MP消耗量.\n\n使用暗杀时 MP -4', '', 150, 8, 40, 'skillicon02.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (633, '暗杀精通 Lv5', 12, 0, 0, 10000, '减少暗杀技能的MP消耗量.\n\n使用暗杀时 MP -5', '', 150, 8, 40, 'skillicon02.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (634, '昏厥攻击强化 Lv1', 12, 0, 0, 10000, '提升未隐身的状态下使用昏厥攻击的命中率.', '', 150, 8, 45, 'skillicon02.dds', 384, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (635, '昏厥攻击强化 Lv2', 12, 0, 0, 10000, '提升未隐身的状态下使用昏厥攻击的命中率.', '', 150, 8, 45, 'skillicon02.dds', 384, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (636, '昏厥攻击强化 Lv3', 12, 0, 0, 10000, '提升未隐身的状态下使用昏厥攻击的命中率.', '', 150, 8, 45, 'skillicon02.dds', 384, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (637, '强化暗杀 Lv1', 12, 0, 0, 10000, '使用暗杀后攻击时提升暴击几率.', '', 150, 8, 50, 'skillicon02.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (638, '强化暗杀 Lv2', 12, 0, 0, 10000, '使用暗杀后攻击时提升暴击几率.', '', 150, 8, 50, 'skillicon02.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (639, '强化暗杀 Lv3', 12, 0, 0, 10000, '使用暗杀后攻击时提升暴击几率.', '', 150, 8, 50, 'skillicon02.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (640, '强化暗杀 Lv4', 12, 0, 0, 10000, '使用暗杀后攻击时提升暴击几率.', '', 150, 8, 50, 'skillicon02.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (641, '强化暗杀 Lv5', 12, 0, 0, 10000, '使用暗杀后攻击时提升暴击几率.', '', 150, 8, 50, 'skillicon02.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (642, '强化隐身术 Lv1', 12, 0, 0, 10000, '增加隐身术状态下的移动速度.', '', 150, 8, 50, 'skillicon02.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (643, '强化隐身术 Lv2', 12, 0, 0, 10000, '增加隐身术状态下的移动速度.', '', 150, 8, 50, 'skillicon02.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (644, '强化隐身术 Lv3', 12, 0, 0, 10000, '增加隐身术状态下的移动速度.', '', 150, 8, 50, 'skillicon02.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (645, '消灭', 13, 1, 0, 10000, '解除所有异常状态的同时使用隐身术. \n\n在侦察的状态下也可以使用.', '将自己的脚步声隐藏起来让其他人无法找到.', 150, 8, 55, 'skillicon02.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (646, '强化哥布林爆破兵 Lv1', 12, 0, 0, 10000, '增加哥布林爆破兵的召唤等级.\n\n哥布林爆破兵 召唤等级 +1', '', 150, 16, 35, 'skillicon02.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (647, '强化哥布林爆破兵 Lv2', 12, 0, 0, 10000, '增加哥布林爆破兵的召唤等级.\n\n哥布林爆破兵 召唤等级 +2', '', 150, 16, 35, 'skillicon02.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (648, '强化哥布林爆破兵 Lv3', 12, 0, 0, 10000, '增加哥布林爆破兵的召唤等级.\n\n哥布林爆破兵 召唤等级 +3', '', 150, 16, 35, 'skillicon02.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (649, '强化猛力攻击 Lv1', 12, 0, 0, 10000, '增加猛力攻击的伤害.', '', 900, 16, 35, 'skillicon02.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (650, '强化猛力攻击 Lv2', 12, 0, 0, 10000, '增加猛力攻击的伤害.', '', 900, 16, 35, 'skillicon02.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (651, '强化猛力攻击 Lv3', 12, 0, 0, 10000, '增加猛力攻击的伤害.', '', 900, 16, 35, 'skillicon02.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (652, '强化猛力攻击 Lv4', 12, 0, 0, 10000, '增加猛力攻击的伤害.', '', 900, 16, 35, 'skillicon02.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (653, '强化猛力攻击 Lv5', 12, 0, 0, 10000, '增加猛力攻击的伤害.', '', 900, 16, 35, 'skillicon02.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (654, '强化曼陀罗 Lv1', 12, 0, 0, 10000, '增加曼陀罗的召唤等级.', '', 150, 16, 40, 'skillicon02.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (655, '强化曼陀罗 Lv2', 12, 0, 0, 10000, '增加曼陀罗的召唤等级.', '', 150, 16, 40, 'skillicon02.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (656, '强化曼陀罗 Lv3', 12, 0, 0, 10000, '增加曼陀罗的召唤等级.', '', 150, 16, 40, 'skillicon02.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (657, '敏捷转换力量 Lv1', 12, 0, 0, 10000, '减少2点敏捷且增加2点力量.', '', 150, 16, 40, 'skillicon02.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (658, '敏捷转换力量 Lv2', 12, 0, 0, 10000, '减少4点敏捷且增加4点力量.', '', 150, 16, 40, 'skillicon02.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (659, '敏捷转换力量 Lv3', 12, 0, 0, 10000, '减少6点敏捷且增加6点力量.', '', 150, 16, 40, 'skillicon02.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (660, '强化流星火 Lv1', 12, 0, 0, 10000, '增加流星火的伤害.', '', 150, 16, 45, 'skillicon02.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (661, '强化流星火 Lv2', 12, 0, 0, 10000, '增加流星火的伤害.', '', 150, 16, 45, 'skillicon02.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (662, '强化流星火 Lv3', 12, 0, 0, 10000, '增加流星火的伤害.', '', 150, 16, 45, 'skillicon02.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (663, '强化流星火 Lv4', 12, 0, 0, 10000, '增加流星火的伤害.', '', 150, 16, 45, 'skillicon02.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (664, '强化流星火 Lv5', 12, 0, 0, 10000, '增加流星火的伤害.', '', 150, 16, 45, 'skillicon02.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (665, '强化海神之怒 Lv1', 12, 0, 0, 10000, '增加海神之怒的攻击力.', '', 150, 16, 45, 'skillicon02.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (666, '强化海神之怒 Lv2', 12, 0, 0, 10000, '增加海神之怒的攻击力.', '', 150, 16, 45, 'skillicon02.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (667, '强化海神之怒 Lv3', 12, 0, 0, 10000, '增加海神之怒的攻击力.', '', 150, 16, 45, 'skillicon02.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (668, '强化海神之怒 Lv4', 12, 0, 0, 10000, '增加海神之怒的攻击力.', '', 150, 16, 45, 'skillicon02.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (669, '强化海神之怒 Lv5', 12, 0, 0, 10000, '增加海神之怒的攻击力.', '', 150, 16, 45, 'skillicon02.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (670, '近战攻击集中 Lv1', 12, 0, 0, 10000, '减少远程攻击力且增加同数值的近战攻击力.', '', 150, 16, 45, 'skillicon02.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (671, '近战攻击集中 Lv2', 12, 0, 0, 10000, '减少远程攻击力且增加同数值的近战攻击力.', '', 150, 16, 45, 'skillicon02.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (672, '近战攻击集中 Lv3', 12, 0, 0, 10000, '减少远程攻击力且增加同数值的近战攻击力.', '', 150, 16, 45, 'skillicon02.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (673, '狂暴', 13, 1, 0, 10000, '大幅度提升移动速度和攻击速度.', '移动速度和攻击速度大幅度上升.', 150, 16, 50, 'skillicon02.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (674, '战斗用生命图腾 Lv1', 12, 1, 0, 10000, '召唤出战斗用生命图腾.(瞬间释放)', '', 150, 16, 35, 'skillicon02.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (675, '战斗用生命图腾 Lv2', 12, 1, 0, 10000, '召唤出战斗用生命图腾.(瞬间释放)', '', 150, 16, 35, 'skillicon02.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (676, '战斗用生命图腾 Lv3', 12, 1, 0, 10000, '召唤出战斗用生命图腾.(瞬间释放)', '', 150, 16, 35, 'skillicon02.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (677, '强化防御图腾', 12, 1, 0, 10000, '召唤出提高对侵蚀火焰的抵抗力的防御图腾.', '', 150, 16, 35, 'skillicon02.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (678, '力量转化敏捷 Lv1', 12, 0, 0, 10000, '减少2点力量且增加2点敏捷.', '', 150, 16, 40, 'skillicon02.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (679, '力量转化敏捷 Lv2', 12, 0, 0, 10000, '减少4点力量且增加4点敏捷.', '', 150, 16, 40, 'skillicon02.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (680, '力量转化敏捷 Lv3', 12, 0, 0, 10000, '减少6点力量且增加6点敏捷.', '', 150, 16, 40, 'skillicon02.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (681, '强化奥利爱德图腾 Lv1', 12, 1, 0, 10000, '召唤出已强化的奥利爱德图腾. 比原有的奥利爱德图腾HP更多.', '', 150, 16, 45, 'skillicon02.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (682, '强化奥利爱德图腾 Lv2', 12, 1, 0, 10000, '召唤出已强化的奥利爱德图腾. 比原有的奥利爱德图腾HP更多.', '', 150, 16, 45, 'skillicon02.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (683, '强化奥利爱德图腾 Lv3', 12, 1, 0, 10000, '召唤出已强化的奥利爱德图腾. 比原有的奥利爱德图腾HP更多.', '', 150, 16, 45, 'skillicon02.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (684, '强化奥利爱德图腾 Lv4', 12, 1, 0, 10000, '召唤出已强化的奥利爱德图腾. 比原有的奥利爱德图腾HP更多.', '', 150, 16, 45, 'skillicon02.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (685, '强化奥利爱德图腾 Lv5', 12, 1, 0, 10000, '召唤出已强化的奥利爱德图腾. 比原有的奥利爱德图腾HP更多.', '', 150, 16, 45, 'skillicon02.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (686, '远程攻击集中 Lv1', 12, 0, 0, 10000, '在减少近战攻击力的同时增加同数值的远程攻击力.', '', 150, 16, 45, 'skillicon02.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (687, '远程攻击集中 Lv2', 12, 0, 0, 10000, '在减少近战攻击力的同时增加同数值的远程攻击力.', '', 150, 16, 45, 'skillicon02.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (688, '远程攻击集中 Lv3', 12, 0, 0, 10000, '在减少近战攻击力的同时增加同数值的远程攻击力.', '', 150, 16, 45, 'skillicon02.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (689, '攻击图腾 Lv1', 12, 1, 0, 10000, '召唤出提升攻击力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (690, '攻击图腾 Lv2', 12, 1, 0, 10000, '召唤出提升攻击力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (691, '攻击图腾 Lv3', 12, 1, 0, 10000, '召唤出提升攻击力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (692, '攻击图腾 Lv4', 12, 1, 0, 10000, '召唤出提升攻击力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (693, '攻击图腾 Lv5', 12, 1, 0, 10000, '召唤出提升攻击力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (694, '防御图腾 Lv1', 12, 1, 0, 10000, '召唤出提升防御力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (695, '防御图腾 Lv2', 12, 1, 0, 10000, '召唤出提升防御力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (696, '防御图腾 Lv3', 12, 1, 0, 10000, '召唤出提升防御力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (697, '防御图腾 Lv4', 12, 1, 0, 10000, '召唤出提升防御力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (698, '防御图腾 Lv5', 12, 1, 0, 10000, '召唤出提升防御力的图腾.', '召唤图腾.', 150, 16, 50, 'skillicon02.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (699, '元素爆炸', 12, 2, 0, 10000, '对目标施展元素力场。\n对目标使用元素魔法时，施展元素环绕。', '', 1500, 4, 50, 'skillicon03.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (700, '元素抵抗', 12, 3, 0, 10000, '无力化目标施展的元素力场。\n抵抗目标使用的元素爆炸。', '', 1500, 4, 50, 'skillicon03.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (701, '炼金术士变身', 12, 1, 0, 10000, '赋予炼金术士的力量。 \n使用技能后，装备变身项链时变身为炼金术士。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (702, '忍者少女变身', 12, 1, 0, 10000, '赋予忍者少女的力量。 \n使用技能后，装备变身项链时变身为忍者少女。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (703, '巴德变身', 12, 1, 0, 10000, '赋予巴德的力量。 \n使用技能后，装备变身项链时变身为巴德。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (704, '女王骑士变身', 12, 1, 0, 10000, '赋予女王骑士的力量。 \n使用技能后，装备变身项链时变身为女王骑士。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (705, '春丽变身', 12, 1, 0, 10000, '赋予春丽的力量。 \n使用技能后，装备变身项链时变身为春丽。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (706, '女海盗船长变身', 12, 1, 0, 10000, '赋予女海盗船长的力量。 \n使用技能后，装备变身项链时变身为女海盗船长。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (707, '修道士变身', 12, 1, 0, 10000, '赋予修道士的力量。 \n使用技能后，装备变身项链时变身为修道士。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (708, '阿修罗变身', 12, 1, 0, 10000, '赋予阿修罗的力量。 \n使用技能后，装备变身项链时变身为阿修罗。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (709, '4大精灵的灵魂', 12, 1, 0, 10000, '跨服战场变身时，外形保持不变，\n只赋予变身能力值，\n可进行远程攻击。', '精灵之魂缠绕着全身。', 150, 18, 1, 'skillicon01.dds', 48, 48, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (710, '透明', 16, 1, 20, 10000, '受到魔法之力，变成透明状态。\n穿上透明披风时，无法使用。', '使用透明技能。', 150, 255, 1, 'item26.dds', 384, 144, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (711, '解除透明', 12, 2, 21, 10000, '解除目标的透明状态\n未装备注视者的披风时，无法使用', '解除透明状态。', 1500, 255, 1, 'item26.dds', 432, 144, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (712, '所有攻击', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (713, '致命一击', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (714, '恶魔小丑变身', 12, 1, 0, 10000, '赋予恶魔小丑的力量。 \n使用技能后，装备变身项链时变身为恶魔小丑。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (715, '大力神变身', 12, 1, 0, 10000, '赋予大力神的力量。 \n使用技能后，装备变身项链时变身为大力神。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (716, '地狱魔兽变身', 12, 1, 0, 10000, '赋予地狱的力量。 \n使用技能后，装备变身项链时变身为地狱怪兽。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (717, '地狱弓箭手变身', 12, 1, 0, 10000, '赋予地狱弓箭手的力量。 \n使用技能后，装备变身项链时变身为地狱弓箭手。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (718, '女神咖莉', 12, 1, 0, 10000, '赋予女神咖莉的力量。 \n使用技能后，装备变身项链时变身为女神咖莉。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (719, '恶魔追猎者', 12, 1, 0, 10000, '赋予恶魔追猎者的力量。 \n使用技能后，装备变身项链时变身为恶魔追猎者。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (720, '狂战士', 12, 1, 0, 10000, '赋予狂战士的力量。 \n使用技能后，装备变身项链时变身为狂战士。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (721, '解除效果', 13, 2, 0, 10000, '', '', 1500, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (722, '精神分裂', 13, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (723, '觉醒', 13, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (724, '减少持续伤害1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (725, '减少持续伤害2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (726, '减少持续伤害3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (727, '疾速攻击1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (728, '疾速攻击2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (729, '疾速攻击3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (730, '集中攻击1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (731, '集中攻击2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (732, '集中攻击3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (733, '疾速反击1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (734, '疾速反击2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (735, '疾速反击3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (736, '集中反击1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (737, '集中反击2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (738, '集中反击3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (739, '疾速1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (740, '疾速2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (741, '疾速3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (742, '庇护1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (743, '庇护2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (744, '庇护3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (745, '闪避1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (746, '闪避2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (747, '闪避3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (748, '复制', 13, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (749, '潜行1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (750, '潜行2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (751, '潜行3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (752, '物理反击1', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (753, '物理反击2', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (754, '物理反击3', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (755, '魔法反击1', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (756, '魔法反击2', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (757, '魔法反击3', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (758, '奋发4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (759, '奋发5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (760, '奋发3（未使用）', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (761, '近程额外防御1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (762, '近程额外防御2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (763, '近程额外防御3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (764, '勇气1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (765, '勇气2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (766, '勇气3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (767, '重击[符文]', 13, 8, 0, 10000, '对目标进行重击。', '', 150, 1, 1, 'skillicon03.dds', 144, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (768, '瞄准射击[符文]', 13, 8, 0, 10000, '对目标进行重击。\n对同一个目标连续使用该技能，可产生额外追加伤害。', '', 1500, 2, 1, 'skillicon02.dds', 48, 384, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (769, '要害攻击[符文]', 13, 8, 0, 10000, '瞄准敌人的要害给予致命攻击。', '', 150, 8, 1, 'skillicon02.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (770, '猛力攻击[符文]', 13, 8, 0, 10000, '对目标进行重击。', '', 1500, 16, 1, 'skillicon02.dds', 240, 432, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (771, '奋发1', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (772, '奋发2', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (773, '奋发3', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (774, '潜伏者1', 13, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (775, '潜伏者2', 13, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (776, '潜伏者3', 13, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (777, '勇气4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (778, '勇气5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (779, '减少持续伤害4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (780, '减少持续伤害5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (781, '近程额外防御4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (782, '近程额外防御5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (783, '疾速攻击4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (784, '疾速攻击5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (785, '集中攻击4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (786, '集中攻击5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (787, '疾速反击4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (788, '疾速反击5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (789, '集中反击4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (790, '集中反击5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (791, '疾速4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (792, '疾速5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (793, '潜伏者4', 13, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (794, '潜伏者5', 13, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (795, '庇护4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (796, '庇护5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (797, '闪避4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (798, '闪避5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (799, '潜行4', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (800, '潜行5', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (801, '物理反击4', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (802, '物理反击5', 16, 2, 0, 10000, '', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (803, '魔法反击4', 16, 2, 0, 10000, '', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (804, '魔法反击5', 16, 2, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (805, '粉嫩护士变身', 12, 1, 0, 10000, '赋予粉嫩护士的力量。 \n使用技能后，装备变身项链时变身为粉嫩护士。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (806, '风精灵变身', 12, 1, 0, 10000, '赋予风精灵的力量。 \n使用技能后，装备变身项链时变身为风精灵。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (807, '防御BUFF（符文）', 16, 1, 0, 10000, '', '', 150, 255, 1, '', 0, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (808, '重击精通（II）', 12, 0, 0, 10000, '重击命中时，冷却时间会初始化。', '', 150, 1, 55, 'skillicon03.dds', 432, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (809, '重击精通（II） Lv2', 12, 0, 0, 10000, '重击命中时，冷却时间会初始化。', '', 150, 1, 55, 'skillicon03.dds', 432, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (810, '重击精通（II） Lv3', 12, 0, 0, 10000, '重击命中时，冷却时间会初始化。', '', 150, 1, 55, 'skillicon03.dds', 432, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (811, '重击增幅', 12, 0, 0, 10000, '重击命中时，降低目标的移动速度。', '', 150, 1, 60, 'skillicon03.dds', 0, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (812, '猛力攻击强化（II） Lv1', 12, 0, 0, 10000, '增加猛力攻击的持续时间。\n\n猛力攻击持续时间+5秒', '', 150, 1, 50, 'skillicon03.dds', 48, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (813, '猛力攻击强化（II） Lv2', 12, 0, 0, 10000, '增加猛力攻击的持续时间。\n\n猛力攻击持续时间+10秒', '', 150, 1, 50, 'skillicon03.dds', 48, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (814, '猛力攻击强化（II） Lv3', 12, 0, 0, 10000, '增加猛力攻击的持续时间。\n\n猛力攻击持续时间+15秒', '', 150, 1, 50, 'skillicon03.dds', 48, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (815, '猛力攻击强化（II） Lv4', 12, 0, 0, 10000, '增加猛力攻击的持续时间。\n\n猛力攻击持续时间+20秒', '', 150, 1, 50, 'skillicon03.dds', 48, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (816, '猛力攻击强化（II） Lv5', 12, 0, 0, 10000, '增加猛力攻击的持续时间。\n\n猛力攻击持续时间+25秒', '', 150, 1, 50, 'skillicon03.dds', 48, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (817, '暴走强化（II） Lv1', 12, 0, 0, 10000, '增加暴走的持续时间。\n\n暴走持续时间+1秒', '', 150, 1, 55, 'skillicon03.dds', 96, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (818, '暴走强化（II） Lv2', 12, 0, 0, 10000, '增加暴走的持续时间。\n\n暴走持续时间+3秒', '', 150, 1, 55, 'skillicon03.dds', 96, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (819, '暴走强化（II） Lv3', 12, 0, 0, 10000, '增加暴走的持续时间。\n\n暴走持续时间+5秒', '', 150, 1, 55, 'skillicon03.dds', 96, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (820, '攻击精通（Ⅳ） Lv1', 12, 0, 0, 10000, '增加近战攻击力。', '', 150, 1, 55, 'skillicon03.dds', 144, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (821, '攻击精通（Ⅳ） Lv2', 12, 0, 0, 10000, '增加近战攻击力。', '', 150, 1, 55, 'skillicon03.dds', 144, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (822, '攻击精通（Ⅳ） Lv3', 12, 0, 0, 10000, '增加近战攻击力。', '', 150, 1, 55, 'skillicon03.dds', 144, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (823, '痛苦麻痹强化（II） Lv1', 12, 0, 0, 10000, '增加疼痛麻痹的持续时间，追加额外防御。\n\n疼痛麻痹持续时间 +3秒', '', 150, 1, 50, 'skillicon03.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (824, '痛苦麻痹强化（II） Lv2', 12, 0, 0, 10000, '增加疼痛麻痹的持续时间，追加额外防御。\n\n疼痛麻痹持续时间 +6秒', '', 150, 1, 50, 'skillicon03.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (825, '痛苦麻痹强化（II） Lv3', 12, 0, 0, 10000, '增加疼痛麻痹的持续时间，追加额外防御。\n\n疼痛麻痹持续时间 +10秒', '', 150, 1, 50, 'skillicon03.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (826, '生命绽放增幅', 12, 0, 0, 10000, '额外增加生命绽放的生命值。', '', 150, 1, 60, 'skillicon03.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (827, '魔法抵抗强化 Lv1', 12, 0, 0, 10000, '减少魔法抵抗的冷却时间。\n\n魔法抵抗冷却时间减少+1秒', '', 150, 1, 55, 'skillicon03.dds', 288, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (828, '魔法抵抗强化 Lv2', 12, 0, 0, 10000, '减少魔法抵抗的冷却时间。\n\n魔法抵抗冷却时间减少+2秒', '', 150, 1, 55, 'skillicon03.dds', 288, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (829, '魔法抵抗强化 Lv3', 12, 0, 0, 10000, '减少魔法抵抗的冷却时间。\n\n魔法抵抗冷却时间减少+5秒', '', 150, 1, 55, 'skillicon03.dds', 288, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (830, '防御精通（Ⅳ） Lv1', 12, 0, 0, 10000, '提升防御力。', '', 150, 1, 55, 'skillicon03.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (831, '防御精通（Ⅳ） Lv2', 12, 0, 0, 10000, '提升防御力。', '', 150, 1, 55, 'skillicon03.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (832, '防御精通（Ⅳ） Lv3', 12, 0, 0, 10000, '提升防御力。', '', 150, 1, 55, 'skillicon03.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (833, '神圣铠甲', 12, 3, 0, 10000, '祝福目标提升魔法回避。', '受到祝福提升魔法回避。', 1500, 4, 60, 'skillicon03.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (834, '祝福光环精通 Lv1', 12, 0, 0, 10000, '使用祝福光环时，增加持续时间，额外增加命中率。\n\n祝福光环持续时间+20秒', '', 0, 4, 40, 'skillicon03.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (835, '祝福光环精通 Lv2', 12, 0, 0, 10000, '使用祝福光环时，增加持续时间，额外增加命中率。\n\n祝福光环持续时间+40秒', '', 0, 4, 40, 'skillicon03.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (836, '祝福光环精通 Lv3', 12, 0, 0, 10000, '使用祝福光环时，增加持续时间，额外增加命中率。\n\n祝福光环持续时间+60秒', '', 0, 4, 40, 'skillicon03.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (837, '凝神术强化 Lv1', 12, 0, 0, 10000, '增加凝神术的持续时间。\n\n凝神术持续时间+10秒', '', 0, 4, 55, 'skillicon03.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (838, '凝神术强化 Lv2', 12, 0, 0, 10000, '增加凝神术的持续时间。\n\n凝神术持续时间+20秒', '', 0, 4, 55, 'skillicon03.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (839, '凝神术强化 Lv3', 12, 0, 0, 10000, '增加凝神术的持续时间。\n\n凝神术持续时间+30秒', '', 0, 4, 55, 'skillicon03.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (840, '凝神术强化 Lv4', 12, 0, 0, 10000, '增加凝神术的持续时间。\n\n凝神术持续时间+40秒', '', 0, 4, 55, 'skillicon03.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (841, '凝神术强化 Lv5', 12, 0, 0, 10000, '增加凝神术的持续时间。\n\n凝神术持续时间+50秒', '', 0, 4, 55, 'skillicon03.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (842, '法力转换强化 Lv1', 12, 0, 0, 10000, '增加法力转换的持续时间。\n\n法力转换持续时间+20秒', '', 0, 4, 55, 'skillicon03.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (843, '法力转换强化 Lv2', 12, 0, 0, 10000, '增加法力转换的持续时间。\n\n法力转换持续时间+40秒', '', 0, 4, 55, 'skillicon03.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (844, '法力转换强化 Lv3', 12, 0, 0, 10000, '增加法力转换的持续时间。\n\n法力转换持续时间+60秒', '', 0, 4, 55, 'skillicon03.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (845, '侵蚀火焰强化 Lv1', 12, 0, 0, 10000, '侵蚀火焰的药水破坏数增加。', '', 0, 4, 50, 'skillicon03.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (846, '侵蚀火焰强化 Lv2', 12, 0, 0, 10000, '侵蚀火焰的药水破坏数增加。', '', 0, 4, 50, 'skillicon03.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (847, '侵蚀火焰强化 Lv3', 12, 0, 0, 10000, '侵蚀火焰的药水破坏数增加。', '', 0, 4, 50, 'skillicon03.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (848, '专注 Lv1', 12, 0, 0, 10000, '集中精神，减少所有技能的MP消耗量。\n\nMP消耗量 -1', '', 0, 4, 55, 'skillicon03.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (849, '专注 Lv2', 12, 0, 0, 10000, '集中精神，减少所有技能的MP消耗量。\n\nMP消耗量 -2', '', 0, 4, 55, 'skillicon03.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (850, '专注 Lv3', 12, 0, 0, 10000, '集中精神，减少所有技能的MP消耗量。\n\nMP消耗量 -3', '', 0, 4, 55, 'skillicon03.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (851, '死亡之手', 12, 1, 0, 10000, '增加自身魔法命中率。', '', 0, 4, 60, 'skillicon03.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (852, '魔力吸食强化 Lv1', 12, 0, 0, 10000, '恢复魔力吸食消耗的部分MP。', '', 0, 4, 55, 'skillicon03.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (853, '魔力吸食强化 Lv2', 12, 0, 0, 10000, '恢复魔力吸食消耗的部分MP。', '', 0, 4, 55, 'skillicon03.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (854, '魔力吸食强化 Lv3', 12, 0, 0, 10000, '恢复魔力吸食消耗的部分MP。', '', 0, 4, 55, 'skillicon03.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (855, '侦查强化', 12, 0, 0, 10000, '侦查范围增加。', '', 0, 4, 45, 'skillicon03.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (856, '蛛网术强化 Lv1', 12, 0, 0, 10000, '增加魔法蛛丝的持续时间。', '', 0, 4, 55, 'skillicon03.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (857, '蛛网术强化 Lv2', 12, 0, 0, 10000, '增加魔法蛛丝的持续时间。', '', 0, 4, 55, 'skillicon03.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (858, '蛛网术强化 Lv3', 12, 0, 0, 10000, '增加魔法蛛丝的持续时间。', '', 0, 4, 55, 'skillicon03.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (859, '火焰箭强化', 12, 0, 0, 10000, '增加火焰箭爆炸伤害。', '', 0, 2, 60, 'skillicon03.dds', 384, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (860, '音速屏障强化（II）Lv1', 12, 0, 0, 10000, '使用音速屏障时，增加最大生命力。', '', 0, 2, 55, 'skillicon03.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (861, '音速屏障强化（II）Lv2', 12, 0, 0, 10000, '使用音速屏障时，增加最大生命力。', '', 0, 2, 55, 'skillicon03.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (862, '音速屏障强化（II）Lv3', 12, 0, 0, 10000, '使用音速屏障时，增加最大生命力。', '', 0, 2, 55, 'skillicon03.dds', 432, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (863, '疾速射击强化（II） Lv1', 12, 0, 0, 10000, '使用疾速射击时，额外增加攻击速度。', '', 0, 2, 50, 'skillicon03.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (864, '疾速射击强化（II） Lv2', 12, 0, 0, 10000, '使用疾速射击时，额外增加攻击速度。', '', 0, 2, 50, 'skillicon03.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (865, '疾速射击强化（II） Lv3', 12, 0, 0, 10000, '使用疾速射击时，额外增加攻击速度。', '', 0, 2, 50, 'skillicon03.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (866, '远程攻击精通（III）Lv1', 12, 0, 0, 10000, '提升远程攻击力。', '', 0, 2, 55, 'skillicon03.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (867, '远程攻击精通（III）Lv2', 12, 0, 0, 10000, '提升远程攻击力。', '', 0, 2, 55, 'skillicon03.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (868, '远程攻击精通（III）Lv3', 12, 0, 0, 10000, '提升远程攻击力。', '', 0, 2, 55, 'skillicon03.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (869, '振荡射击强化（II） Lv1', 12, 0, 0, 10000, '增加振荡射击的减速持续时间。', '', 0, 2, 50, 'skillicon03.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (870, '振荡射击强化（II） Lv2', 12, 0, 0, 10000, '增加振荡射击的减速持续时间。', '', 0, 2, 50, 'skillicon03.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (871, '振荡射击强化（II） Lv3', 12, 0, 0, 10000, '增加振荡射击的减速持续时间。', '', 0, 2, 50, 'skillicon03.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (872, '陷阱强化（I） Lv1', 12, 0, 0, 10000, '增加陷阱的范围，立即释放。', '', 0, 2, 55, 'skillicon03.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (873, '陷阱强化（I） Lv2', 12, 0, 0, 10000, '增加陷阱的范围，立即释放。', '', 0, 2, 55, 'skillicon03.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (874, '陷阱强化（I） Lv3', 12, 0, 0, 10000, '增加陷阱的范围，立即释放。', '', 0, 2, 55, 'skillicon03.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (875, '陷阱强化（II）', 12, 0, 0, 10000, '增加粘性陷阱的减速效果。', '', 0, 2, 60, 'skillicon03.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (876, '无视护甲强化（I） Lv1', 12, 0, 0, 10000, '', '', 0, 2, 45, 'skillicon03.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (877, '无视护甲强化（I） Lv2', 12, 0, 0, 10000, '', '', 0, 2, 45, 'skillicon03.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (878, '无视护甲强化（I） Lv3', 12, 0, 0, 10000, '', '', 0, 2, 45, 'skillicon03.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (879, '无视护甲强化（II） Lv1', 12, 0, 0, 10000, '', '', 0, 2, 50, 'skillicon03.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (880, '无视护甲强化（II） Lv2', 12, 0, 0, 10000, '', '', 0, 2, 50, 'skillicon03.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (881, '不使用', 12, 0, 0, 10000, '', '', 0, 0, 0, 'skillicon03.dds', 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (882, '无视护甲强化（III） Lv1', 12, 0, 0, 10000, '', '', 0, 2, 55, 'skillicon03.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (883, '无视护甲强化（III） Lv2', 12, 0, 0, 10000, '', '', 0, 2, 55, 'skillicon03.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (884, '无视护甲强化（III） Lv3', 12, 0, 0, 10000, '', '', 0, 2, 55, 'skillicon03.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (885, '要害攻击精通 Lv1', 12, 0, 0, 10000, '使用要害攻击时，减少MP消耗量\n\n要害攻击消耗MP -1', '', 0, 8, 55, 'skillicon03.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (886, '要害攻击精通 Lv2', 12, 0, 0, 10000, '使用要害攻击时，减少MP消耗量\n\n要害攻击消耗MP -2', '', 0, 8, 55, 'skillicon03.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (887, '要害攻击精通 Lv3', 12, 0, 0, 10000, '使用要害攻击时，减少MP消耗量\n\n要害攻击消耗MP -3', '', 0, 8, 55, 'skillicon03.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (888, '要害攻击精通 Lv4', 12, 0, 0, 10000, '使用要害攻击时，减少MP消耗量\n\n要害攻击消耗MP -4', '', 0, 8, 55, 'skillicon03.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (889, '要害攻击精通 Lv5', 12, 0, 0, 10000, '使用要害攻击时，减少MP消耗量\n\n要害攻击消耗MP -5', '', 0, 8, 55, 'skillicon03.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (890, '暴击强化 Lv1', 12, 0, 0, 10000, '暴击命中后，下一次攻击时，暴击率上升。', '', 0, 8, 55, 'skillicon03.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (891, '暴击强化 Lv2', 12, 0, 0, 10000, '暴击命中后，下一次攻击时，暴击率上升。', '', 0, 8, 55, 'skillicon03.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (892, '暴击强化 Lv3', 12, 0, 0, 10000, '暴击命中后，下一次攻击时，暴击率上升。', '', 0, 8, 55, 'skillicon03.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (893, '暴击强化 Lv4', 12, 0, 0, 10000, '暴击命中后，下一次攻击时，暴击率上升。', '', 0, 8, 55, 'skillicon03.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (894, '暴击强化 Lv5', 12, 0, 0, 10000, '暴击命中后，下一次攻击时，暴击率上升。', '', 0, 8, 55, 'skillicon03.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (895, '强效毒瓶生成[短剑]', 13, 1, 0, 10000, '将猛毒的气息装入小瓶子里，致命的猛毒延长后遗症的持续时间。\n\n消耗声望 100', '', 0, 8, 50, 'skillicon03.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (896, '强效毒瓶生成[拳剑]', 13, 1, 0, 10000, '将猛毒的气息装入小瓶子里，致命的猛毒延长后遗症的持续时间。\n\n消耗声望 100', '', 0, 8, 45, 'skillicon03.dds', 48, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (897, '毒牙强化', 12, 0, 0, 10000, '增加毒牙的发动几率。', '', 0, 8, 60, 'skillicon03.dds', 96, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (898, '昏厥攻击强化（II） Lv1', 12, 0, 0, 10000, '使用昏厥攻击时，减少冷却时间。\n\n昏厥攻击冷却时间 -20秒', '', 0, 8, 55, 'skillicon03.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (899, '昏厥攻击强化（II） Lv2', 12, 0, 0, 10000, '使用昏厥攻击时，减少冷却时间。\n\n昏厥攻击冷却时间 -40秒', '', 0, 8, 55, 'skillicon03.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (900, '昏厥攻击强化（II） Lv3', 12, 0, 0, 10000, '使用昏厥攻击时，减少冷却时间。\n\n昏厥攻击冷却时间 -60秒', '', 0, 8, 55, 'skillicon03.dds', 144, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (901, '暗杀（II）', 12, 0, 0, 10000, '暗杀技能命中后，使目标中毒。', '', 0, 8, 60, 'skillicon03.dds', 192, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (902, '疾行精通 Lv1', 12, 0, 0, 10000, '使用疾行时，减少MP消耗量\n\n疾行消耗MP -1', '', 0, 8, 45, 'skillicon03.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (903, '疾行精通 Lv2', 12, 0, 0, 10000, '使用疾行时，减少MP消耗量\n\n疾行消耗MP -2', '', 0, 8, 45, 'skillicon03.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (904, '疾行精通 Lv3', 12, 0, 0, 10000, '使用疾行时，减少MP消耗量\n\n疾行消耗MP -3', '', 0, 8, 45, 'skillicon03.dds', 240, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (905, '闪避精通（I） Lv1', 12, 0, 0, 10000, '增加近程回避。', '', 0, 8, 35, 'skillicon03.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (906, '闪避精通（I） Lv2', 12, 0, 0, 10000, '增加近程回避。', '', 0, 8, 35, 'skillicon03.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (907, '闪避精通（I） Lv3', 12, 0, 0, 10000, '增加近程回避。', '', 0, 8, 35, 'skillicon03.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (908, '闪避精通（I） Lv4', 12, 0, 0, 10000, '增加近程回避。', '', 0, 8, 35, 'skillicon03.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (909, '闪避精通（I） Lv5', 12, 0, 0, 10000, '增加近程回避。', '', 0, 8, 35, 'skillicon03.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (910, '闪避精通（II） Lv1', 12, 0, 0, 10000, '增加远程回避。', '', 0, 8, 40, 'skillicon03.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (911, '闪避精通（II） Lv2', 12, 0, 0, 10000, '增加远程回避。', '', 0, 8, 40, 'skillicon03.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (912, '闪避精通（II） Lv3', 12, 0, 0, 10000, '增加远程回避。', '', 0, 8, 40, 'skillicon03.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (913, '闪避精通（II） Lv4', 12, 0, 0, 10000, '增加远程回避。', '', 0, 8, 40, 'skillicon03.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (914, '闪避精通（II） Lv5', 12, 0, 0, 10000, '增加远程回避。', '', 0, 8, 40, 'skillicon03.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (915, '闪避精通（III） Lv1', 12, 0, 0, 10000, '增加魔法回避。', '', 0, 8, 45, 'skillicon03.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (916, '闪避精通（III） Lv2', 12, 0, 0, 10000, '增加魔法回避。', '', 0, 8, 45, 'skillicon03.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (917, '闪避精通（III） Lv3', 12, 0, 0, 10000, '增加魔法回避。', '', 0, 8, 45, 'skillicon03.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (918, '闪避精通（III） Lv4', 12, 0, 0, 10000, '增加魔法回避。', '', 0, 8, 45, 'skillicon03.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (919, '闪避精通（III） Lv5', 12, 0, 0, 10000, '增加魔法回避。', '', 0, 8, 45, 'skillicon03.dds', 384, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (920, '回避强化（II）', 12, 0, 0, 10000, '使用回避技能时，额外增加回避率。', '', 0, 8, 55, 'skillicon03.dds', 0, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (921, '流星火强化（II） Lv1', 12, 0, 0, 10000, '增加陨石术的命中和伤害。', '', 0, 16, 55, 'skillicon03.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (922, '流星火强化（II） Lv2', 12, 0, 0, 10000, '增加陨石术的命中和伤害。', '', 0, 16, 55, 'skillicon03.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (923, '流星火强化（II） Lv3', 12, 0, 0, 10000, '增加陨石术的命中和伤害。', '', 0, 16, 55, 'skillicon03.dds', 96, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (924, '海神之怒强化（II） Lv1', 12, 0, 0, 10000, '增加海神之怒的命中和攻击速度降低效果。', '', 0, 16, 50, 'skillicon03.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (925, '海神之怒强化（II） Lv2', 12, 0, 0, 10000, '增加海神之怒的命中和攻击速度降低效果。', '', 0, 16, 50, 'skillicon03.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (926, '海神之怒强化（II） Lv3', 12, 0, 0, 10000, '增加海神之怒的命中和攻击速度降低效果。', '', 0, 16, 50, 'skillicon03.dds', 144, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (927, '地狱烈焰强化（I）', 12, 0, 0, 10000, '增加地狱烈焰的初始伤害。', '', 0, 16, 50, 'skillicon03.dds', 192, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (928, '地狱烈焰强化（II） Lv1', 12, 0, 0, 10000, '增加地狱烈焰的命中和持续时间。', '', 0, 16, 55, 'skillicon03.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (929, '地狱烈焰强化（II） Lv2', 12, 0, 0, 10000, '增加地狱烈焰的命中和持续时间。', '', 0, 16, 55, 'skillicon03.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (930, '地狱烈焰强化（II） Lv3', 12, 0, 0, 10000, '增加地狱烈焰的命中和持续时间。', '', 0, 16, 55, 'skillicon03.dds', 240, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (931, '狂暴强化（I） Lv1', 12, 0, 0, 10000, '使用狂暴时，增加持续时间。\n\n狂暴持续时间+3秒', '', 0, 16, 55, 'skillicon03.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (932, '狂暴强化（I） Lv2', 12, 0, 0, 10000, '使用狂暴时，增加持续时间。\n\n狂暴持续时间+6秒', '', 0, 16, 55, 'skillicon03.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (933, '狂暴强化（I） Lv3', 12, 0, 0, 10000, '使用狂暴时，增加持续时间。\n\n狂暴持续时间+10秒', '', 0, 16, 55, 'skillicon03.dds', 288, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (934, '狂暴强化（II）', 12, 0, 0, 10000, '使用狂暴时，增加攻击力和防御力。', '', 0, 16, 60, 'skillicon03.dds', 336, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (935, '攻击图腾强化 Lv1', 12, 0, 0, 10000, '攻击图腾附加命中。', '', 0, 16, 55, 'skillicon03.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (936, '攻击图腾强化 Lv2', 12, 0, 0, 10000, '攻击图腾附加命中。', '', 0, 16, 55, 'skillicon03.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (937, '攻击图腾强化 Lv3', 12, 0, 0, 10000, '攻击图腾附加命中。', '', 0, 16, 55, 'skillicon03.dds', 384, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (938, '防御图腾强化 Lv1', 12, 0, 0, 10000, '防御图腾附加回避。', '', 0, 16, 55, 'skillicon03.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (939, '防御图腾强化 Lv2', 12, 0, 0, 10000, '防御图腾附加回避。', '', 0, 16, 55, 'skillicon03.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (940, '防御图腾强化 Lv3', 12, 0, 0, 10000, '防御图腾附加回避。', '', 0, 16, 55, 'skillicon03.dds', 432, 240, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (941, '疾风图腾 Lv1', 12, 1, 0, 10000, '召唤提升攻速的图腾。', '召唤图腾。', 0, 16, 50, 'skillicon03.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (942, '疾风图腾 Lv2', 12, 1, 0, 10000, '召唤提升攻速的图腾。', '召唤图腾。', 0, 16, 50, 'skillicon03.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (943, '疾风图腾 Lv3', 12, 1, 0, 10000, '召唤提升攻速的图腾。', '召唤图腾。', 0, 16, 50, 'skillicon03.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (944, '疾风图腾 Lv4', 12, 1, 0, 10000, '召唤提升攻速的图腾。', '召唤图腾。', 0, 16, 50, 'skillicon03.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (945, '疾风图腾 Lv5', 12, 1, 0, 10000, '召唤提升攻速的图腾。', '召唤图腾。', 0, 16, 50, 'skillicon03.dds', 0, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (946, '疾风图腾强化 Lv1', 12, 0, 0, 10000, '疾风图腾的效力提升。', '', 0, 16, 55, 'skillicon03.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (947, '疾风图腾强化 Lv2', 12, 0, 0, 10000, '疾风图腾的效力提升。', '', 0, 16, 55, 'skillicon03.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (948, '疾风图腾强化 Lv3', 12, 0, 0, 10000, '疾风图腾的效力提升。', '', 0, 16, 55, 'skillicon03.dds', 48, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (949, '图腾强化', 12, 0, 0, 10000, '减少所有图腾的冷却时间。', '', 0, 16, 60, 'skillicon03.dds', 96, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (950, '振荡射击强化（II） Lv4', 12, 0, 0, 10000, '增加振荡射击的减速持续时间。', '', 0, 16, 50, 'skillicon03.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (951, '振荡射击强化（II） Lv5', 12, 0, 0, 10000, '增加振荡射击的减速持续时间。', '', 0, 16, 50, 'skillicon03.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (952, '名妓黄真伊变身', 12, 1, 0, 10000, '赋予名妓黄真伊的力量。 \n使用技能后，装备变身项链时变身为名妓黄真伊。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (953, '暴走机车女变身', 12, 1, 0, 10000, '赋予暴走机车女的力量。 \n使用技能后，装备变身项链时变身为暴走机车女。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (954, '男爵火枪手变身', 12, 1, 0, 10000, '赋予男爵火枪手的力量。 \n使用技能后，装备变身项链时变身为男爵火枪手。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (955, '惊艳女狙击手变身', 12, 1, 0, 10000, '赋予惊艳女狙击手的力量。 \n使用技能后，装备变身项链时变身为惊艳女狙击手。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (956, '暮色黑寡妇变身', 12, 1, 0, 10000, '赋予暮色黑寡妇的力量。 \n使用技能后，装备变身项链时变身为暮色黑寡妇。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (957, '强化烈焰风暴（II） Lv1', 12, 0, 0, 10000, '烈焰风暴的药水破坏数增加。', '', 0, 4, 55, 'skillicon03.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (958, '强化烈焰风暴（II） Lv2', 12, 0, 0, 10000, '烈焰风暴的药水破坏数增加。', '', 0, 4, 55, 'skillicon03.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (959, '强化烈焰风暴（II） Lv3', 12, 0, 0, 10000, '烈焰风暴的药水破坏数增加。', '', 0, 4, 55, 'skillicon03.dds', 144, 288, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (960, '侦查（II）', 13, 7, 0, 10000, '可以将处于隐身状态的敌人暴露出来的魔法。', '', 150, 4, 63, 'skillicon02.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (961, '侦查（III）', 13, 7, 0, 10000, '可以将处于隐身状态的敌人暴露出来的魔法。', '', 150, 4, 68, 'skillicon02.dds', 336, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (962, '粉嫩护士变身30天(RU)', 12, 1, 0, 30, '赋予粉嫩护士的力量。 \n使用技能后，装备变身项链时变身为粉嫩护士。', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (963, '男爵火枪手变身30天(RU)', 12, 1, 0, 30, '赋予男爵火枪手的力量。 \n使用技能后，装备变身项链时变身为男爵火枪手。', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (964, '惊艳女狙击手变身30天(RU)', 12, 1, 0, 30, '赋予惊艳女狙击手的力量。 \n使用技能后，装备变身项链时变身为惊艳女狙击手。', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (965, '暮色黑寡妇变身30天(RU)', 12, 1, 0, 30, '赋予暮色黑寡妇的力量。 \n使用技能后，装备变身项链时变身为暮色黑寡妇。', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (966, '暴走机车女技能书30天(RU)', 12, 1, 0, 30, '赋予暴走机车女的力量。 \n使用技能后，装备变身项链时变身为暴走机车女。', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (967, '名妓黄真伊技能书30天(RU)', 12, 1, 0, 30, '赋予名妓黄真伊的力量。 \n使用技能后，装备变身项链时变身为名妓黄真伊。', '', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (968, '女佣变身', 12, 1, 0, 10000, '赋予女佣的力量。 \n使用技能后，装备变身项链时变身为女佣。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (969, '暗黑侍卫变身', 12, 1, 0, 10000, '赋予暗黑侍卫的力量。 \n使用技能后，装备变身项链时变身为暗黑侍卫。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (970, '玲珑天使变身', 12, 1, 0, 10000, '赋予玲珑天使的力量。 \n使用技能后，装备变身项链时变身为玲珑天使。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (971, '变身朱庇特的邪念', 12, 1, 0, 10000, '赋予朱庇特的邪念的力量。\n使用技能后，佩戴变身项链时变身为朱庇特的邪念。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (972, '变身巴贝克的司令官', 12, 1, 0, 10000, '赋予巴贝克的司令官的力量。\n使用技能后，佩戴变身项链时变身为巴贝克的司令官。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (973, '变身阿努比斯', 12, 1, 0, 10000, '赋予阿努比斯的力量。\n使用技能后，佩戴变身项链时变身为阿努比斯。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (974, '变身月光精灵魔法师', 12, 1, 0, 10000, '赋予月光精灵魔法师的力量。\n使用技能后，佩戴变身项链时变身为月光精灵魔法师。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (975, '重击 - 控制时间
', 13, 8, 0, 10000, '暴击伤害攻击.\n使命中对象无法瞬间移动
', '', 150, 1, 10, 'skillicon03.dds', 288, 432, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (976, '猛力攻击 - 力量的集中
', 13, 8, 0, 10000, '一定时间内增加力量\n增加效果维持时间

', '', 150, 1, 20, 'skillicon03.dds', 336, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (977, '痛苦麻痹 - 铁的屏障
', 13, 8, 0, 10000, '瞬间提升防御力.\n增加效果维持时间
', '', 150, 1, 30, 'skillicon03.dds', 384, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (978, '暴走 - 突破极限
', 13, 8, 0, 10000, '降低防御力，同时提高攻击速度提高\n增加效果维持时间
', '', 150, 1, 40, 'skillicon03.dds', 432, 432, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (979, '生命绽放 - 坚强的意志
', 13, 8, 0, 10000, '一定时间内大幅度提高自己的生命值上限.\n增加效果维持时间
', '', 150, 1, 50, 'skillicon04.dds', 0, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (980, '瞄准射击 - 捕捉动作
', 13, 8, 0, 10000, '单体暴击伤害攻击.\n阻止命中对象隐身
', '', 1500, 2, 10, 'skillicon04.dds', 48, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (981, '音速屏障 - 风之增幅
', 13, 8, 0, 10000, '一定时间内敏捷增加，使用技能的状态下受到攻击时防御力上升。\n增加效果维持时间
', '', 150, 2, 20, 'skillicon04.dds', 96, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (982, '振荡射击- 熟练的猎人
', 13, 6, 0, 10000, '短时间内可降低敌人的移动速度..\n增加效果维持时间
', '', 1500, 2, 30, 'skillicon04.dds', 144, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (983, '疾速射击 - 迅速的动作
', 13, 8, 0, 10000, '瞬间攻击速度加快。\n增加效果维持时间。

', '', 150, 2, 40, 'skillicon04.dds', 192, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (984, '无视护甲 - 内心的破坏
', 13, 20, 0, 10000, '随机降低目标的防御力，增加释放者远距离命中率和暴击率、怪物伤害效果。\n 被命中对象的力量, 敏捷, 智力随机减少
', '', 1500, 2, 50, 'skillicon04.dds', 240, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (985, '重击 - 控制回避
', 13, 8, 0, 10000, '暴击伤害攻击。\n一定时间内减少被命中对象的魔法回避
', '', 150, 4, 10, 'skillicon04.dds', 288, 0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (986, '凝神术- 精神的铠甲
', 13, 8, 0, 10000, '一定时间内力量, 敏捷, 智力增加。\n增加效果维持时间
', '', 150, 4, 20, 'skillicon04.dds', 336, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (987, '魔法恢复 - 精灵的引导
', 13, 8, 0, 10000, '负重状态下恢复魔力\n增加了魔法恢复量。
', '', 150, 4, 30, 'skillicon04.dds', 384, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (988, '群体拯救 - 暴风的时间
', 13, 7, 0, 10000, '对方的防御力瞬间提升。\n增加效果维持时间
', '', 150, 4, 40, 'skillicon04.dds', 432, 0, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (989, '漂浮术 - 回避增加', 12, 8, 0, 10000, '浮在空中，移动速度及攻击速度大幅度增加。\n一定时间内自身的魔法回避增加。', '身子像羽毛一样变得轻飘飘
', 150, 4, 50, 'skillicon04.dds', 0, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (990, '魔法凝聚 - 精炼的魔力
', 12, 1, 0, 10000, '凝聚自身魔力生成魔晶石\n减少负重增加恢复量生成精练的魔晶石。
', '集中精神。
', 150, 4, 50, 'skillicon04.dds', 48, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (991, '疾行 - 饥饿的狼
', 13, 1, 0, 10000, '移动速度提升，暂时性提高攻击速度。\n比当前的攻击速度提升了维持时间。
', '', 150, 8, 10, 'skillicon04.dds', 96, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (992, '回避 - 回避本能
', 13, 8, 0, 10000, '瞬间回避率增加。\n增加效果维持时间
', '', 150, 8, 20, 'skillicon04.dds', 144, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (993, '隐身术发现者 - 安息的时间
', 13, 8, 0, 10000, '进入透明状态。\n增加效果维持时间
', '', 150, 8, 20, 'skillicon04.dds', 192, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (994, '毒牙 - 猛毒研究
', 13, 1, 0, 10000, '对目标施毒概率增加。\n增加效果维持时间
', '', 150, 8, 30, 'skillicon04.dds', 240, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (995, '暗杀技能- 巨鹰之眼
', 13, 8, 0, 10000, '对目标进行强力攻击。未进入隐身状态下无法触发。\n技能伤害提升。
', '', 150, 8, 40, 'skillicon04.dds', 288, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (996, '昏厥攻击 - 暗杀的气息
', 13, 19, 0, 10000, '强力攻击使目标昏厥。\n减少技能重复使用时间。
', '', 150, 8, 40, 'skillicon04.dds', 336, 48, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (997, '要害攻击 - 致命的刀锋
', 13, 8, 0, 10000, '瞄准敌人的要害给予致命攻击。\n 一定时间内降低目标武器攻击力的20%的伤害。
', '', 150, 8, 50, 'skillicon04.dds', 384, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (998, '猛力攻击 - 魔力强化
', 13, 8, 0, 10000, '暴击伤害攻击。
', '', 150, 16, 10, 'skillicon04.dds', 432, 48, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (999, '魔力强化 - 魔力的理解
', 12, 1, 0, 10000, '一定时间内提升魔法能力。\n增加魔法攻击力和命中率。
', '', 150, 16, 20, 'skillicon04.dds', 0, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1000, '腐蚀术- 幻想的监狱
', 13, 6, 0, 10000, '腐蚀敌人装备使一定时间内无法装备或者解除装备。
', '', 1500, 16, 30, 'skillicon04.dds', 48, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1001, '时间扭曲 - 次元的霸主
', 12, 1, 0, 10000, '扭曲周围时间，提升自身攻击速度。
', '', 150, 16, 40, 'skillicon04.dds', 96, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1002, '反射光环 - 魔力扩张
', 13, 1, 0, 10000, '一定几率使部分魔法对自己无效化，反射被施加的魔法。
', '', 150, 16, 50, 'skillicon04.dds', 144, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1003, '奥利爱德图腾召唤 - 吸收强化
', 12, 1, 0, 10000, '召唤出能够代替友军吸收一定量的伤害的图腾。
', '', 150, 16, 50, 'skillicon04.dds', 192, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1004, '嗜血 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +20
', '', 150, 255, 1, 'skillicon03.dds', 192, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1005, '嗜血 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +30
', '', 150, 255, 1, 'skillicon03.dds', 192, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1006, '嗜血提升 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +40
', '', 150, 255, 1, 'skillicon03.dds', 240, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1007, '嗜血提升 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +50
', '', 150, 255, 1, 'skillicon03.dds', 240, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1008, '强化嗜血提升 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +100
', '', 150, 255, 1, 'skillicon03.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1009, '强化嗜血提升 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +150
', '', 150, 255, 1, 'skillicon03.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1010, '强化嗜血提升 Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +200
', '', 150, 255, 1, 'skillicon03.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1011, '宠物灵魂 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +5
', '', 150, 255, 1, 'skillicon03.dds', 0, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1012, '宠物灵魂 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +7
', '', 150, 255, 1, 'skillicon03.dds', 0, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1013, '灵魂提升 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +9
', '', 150, 255, 1, 'skillicon03.dds', 48, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1014, '灵魂提升 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +11
', '', 150, 255, 1, 'skillicon03.dds', 48, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1015, '强化灵魂Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +13
', '', 150, 255, 1, 'skillicon03.dds', 96, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1016, '强化灵魂Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +15
', '', 150, 255, 1, 'skillicon03.dds', 96, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1017, '强化灵魂Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +17
', '', 150, 255, 1, 'skillicon03.dds', 96, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1018, '变形 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +100
', '', 150, 255, 1, 'skillicon03.dds', 288, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1019, '变形 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +130
', '', 150, 255, 1, 'skillicon03.dds', 288, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1020, '变形提升 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +150
', '', 150, 255, 1, 'skillicon03.dds', 336, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1021, '变形提升 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +200
', '', 150, 255, 1, 'skillicon03.dds', 336, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1022, '强化变形 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +250
', '', 150, 255, 1, 'skillicon03.dds', 384, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1023, '强化变形 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +300
', '', 150, 255, 1, 'skillicon03.dds', 384, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1024, '强化变形 Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +350
', '', 150, 255, 1, 'skillicon03.dds', 384, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1025, '嗜血治疗 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 336, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1026, '嗜血治疗 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 336, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1027, '血液恢复 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 384, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1028, '血液恢复 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 384, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1029, '强化血液恢复 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 432, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1030, '强化血液恢复 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 432, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1031, '强化血液恢复 Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的HP恢复量增加。\n\nHP恢复 +2
', '', 150, 255, 1, 'skillicon03.dds', 432, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1032, '魔法治疗Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 144, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1033, '魔法治疗Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 144, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1034, '魔法恢复 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 192, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1035, '魔法恢复 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 192, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1036, '强化魔法恢复 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 240, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1037, '强化魔法恢复 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 240, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1038, '强化魔法恢复 Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的MP恢复量增加。\n\nMP恢复 +2
', '', 150, 255, 1, 'skillicon03.dds', 240, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1039, '宠物蓄力 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon03.dds', 432, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1040, '宠物蓄力 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间''+1分
', '', 150, 255, 1, 'skillicon03.dds', 432, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1041, '蓄力提升 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon03.dds', 0, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1042, '蓄力提升 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon03.dds', 0, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1043, '强化蓄力 Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon03.dds', 48, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1044, '强化蓄力 Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon03.dds', 48, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1045, '强化蓄力 Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+2分
', '', 150, 255, 1, 'skillicon03.dds', 48, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1046, '二阶嗜血 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +210
', '', 150, 255, 1, 'skillicon03.dds', 96, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1047, '二阶嗜血 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +220
', '', 150, 255, 1, 'skillicon03.dds', 96, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1048, '二阶嗜血 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +240
', '', 150, 255, 1, 'skillicon03.dds', 144, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1049, '二阶嗜血 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +300
', '', 150, 255, 1, 'skillicon03.dds', 144, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1050, '二阶灵魂(I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +20
', '', 150, 255, 1, 'skillicon03.dds', 288, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1051, '二阶灵魂(I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +25
', '', 150, 255, 1, 'skillicon03.dds', 288, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1052, '二阶灵魂(II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +30
', '', 150, 255, 1, 'skillicon03.dds', 336, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1053, '二阶灵魂(II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +40
', '', 150, 255, 1, 'skillicon03.dds', 336, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1054, '二阶变形 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +380
', '', 150, 255, 1, 'skillicon03.dds', 0, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1055, '二阶变形 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +400
', '', 150, 255, 1, 'skillicon03.dds', 0, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1056, '二阶变形 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +420
', '', 150, 255, 1, 'skillicon03.dds', 48, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1057, '二阶变形 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +450
', '', 150, 255, 1, 'skillicon03.dds', 48, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1058, '嗜血 灵魂(I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP， MP增加。\n\nHP +10\nMP +10
', '', 150, 255, 1, 'skillicon03.dds', 192, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1059, '嗜血 灵魂(I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP， MP增加。\n\nHP +20\nMP +20
', '', 150, 255, 1, 'skillicon03.dds', 192, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1060, '嗜血 灵魂(II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP， MP增加。\n\nHP +30\nMP +30
', '', 150, 255, 1, 'skillicon03.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1061, '嗜血 灵魂(II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP， MP增加。\n\nHP +40\nMP +40
', '', 150, 255, 1, 'skillicon03.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1062, '二阶嗜血治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 192, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1063, '二阶嗜血治疗 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +2
', '', 150, 255, 1, 'skillicon03.dds', 192, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1064, '二阶嗜血治疗 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +2
', '', 150, 255, 1, 'skillicon03.dds', 240, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1065, '二阶嗜血治疗 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +3
', '', 150, 255, 1, 'skillicon03.dds', 240, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1066, '二阶魔法治疗(I) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon03.dds', 384, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1067, '二阶魔法治疗(I) Lv2
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +2
', '', 150, 255, 1, 'skillicon03.dds', 384, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1068, '二阶魔法治疗(II) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +2
', '', 150, 255, 1, 'skillicon03.dds', 432, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1069, '二阶魔法治疗(II) Lv2
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +3
', '', 150, 255, 1, 'skillicon03.dds', 432, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1070, '二阶蓄力 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon03.dds', 96, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1071, '二阶蓄力 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+2分
', '', 150, 255, 1, 'skillicon03.dds', 96, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1072, '二阶蓄力 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+2分
', '', 150, 255, 1, 'skillicon03.dds', 144, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1073, '二阶蓄力 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+3分
', '', 150, 255, 1, 'skillicon03.dds', 144, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1074, '变身恶魔小丑(II)', 12, 1, 0, 10000, '赋予恶魔小丑(II)的力量。\n使用技能后，佩戴变身项链时变身为恶魔小丑(II)。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1075, '变身女神咖莉(II)', 12, 1, 0, 10000, '赋予女神咖莉(II)的力量。\n使用技能后，佩戴变身项链时变身为女神咖莉(II)。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1076, '变身春丽(II)', 12, 1, 0, 10000, '赋予春丽(II)的力量。\n使用技能后，佩戴变身项链时变身为春丽(II)。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1077, '变身恶魔追猎者(II)', 12, 1, 0, 10000, '赋予恶魔追猎者(II)的力量。\n使用技能后，佩戴变身项链时变身为恶魔追猎者(II)。', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1078, '力量增加
', 13, 1, 0, 10000, '增加力量。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 240, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1079, '敏捷增加
', 13, 1, 0, 10000, '增加敏捷。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 288, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1080, '智力增加
', 13, 1, 0, 10000, '增加智力。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 336, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1081, '增加药水恢复量
', 13, 1, 0, 10000, '增加药水恢复量。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1082, '宠物的祈护 (I)
', 13, 1, 0, 10000, '减少被近战攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1083, '宠物的祈护 (II)

', 13, 1, 0, 10000, '减少被远程攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1084, '宠物的祈护 (III)', 13, 1, 0, 10000, '减少被魔法攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1085, '增加药水恢复量 (I)
', 13, 1, 0, 10000, '增加药水恢复量。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1086, '双倍暴击 (I)
', 13, 1, 0, 10000, '增加暴击几率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1087, '双倍伤害 (I)
', 13, 1, 0, 10000, '暴击时增加伤害值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1088, '双倍盾 (I)
', 13, 1, 0, 10000, '受到的受到的伤害量减少。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1089, '魔力的流动 (I)
', 13, 1, 0, 10000, '减少使用技能时需要的魔力消耗。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1090, '提升魔法命中率 (I)
', 13, 1, 0, 10000, '增加魔法命中率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1091, '双倍特效
', 13, 1, 0, 10000, '角色周围发生特效。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1092, '宠物的祝福 (I)
', 13, 1, 0, 10000, '增加狩猎时经验值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1093, '大地之德拉克
', 13, 1, 0, 10000, '佩戴德拉克戒指时德拉克外形变更及回避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1094, '太阳之德拉克
', 13, 1, 0, 10000, '佩戴德拉克戒指时德拉克外形变更及回避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1095, '风之德拉克
', 13, 1, 0, 10000, '佩戴德拉克戒指时德拉基外形变更及回避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1096, '三阶嗜血 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +50
', '', 150, 255, 1, 'skillicon04.dds', 48, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1097, '三阶嗜血 (I) Lv2', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +60
', '', 150, 255, 1, 'skillicon04.dds', 48, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1098, '三阶嗜血 (I) Lv3', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +90\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 48, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1099, '三阶嗜血 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +60
', '', 150, 255, 1, 'skillicon04.dds', 96, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1100, '三阶嗜血 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +70
', '', 150, 255, 1, 'skillicon04.dds', 96, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1101, '三阶嗜血 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +80
', '', 150, 255, 1, 'skillicon04.dds', 96, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1102, '三阶嗜血 (II) Lv4
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +110\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 96, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1103, '四阶嗜血 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +100
', '', 150, 255, 1, 'skillicon04.dds', 144, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1104, '四阶嗜血 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +110
', '', 150, 255, 1, 'skillicon04.dds', 144, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1105, '四阶嗜血 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +150\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 144, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1106, '四阶嗜血 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +110
', '', 150, 255, 1, 'skillicon04.dds', 192, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1107, '四阶嗜血 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +120
', '', 150, 255, 1, 'skillicon04.dds', 192, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1108, '四阶嗜血 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +160\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 192, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1109, '三阶嗜血治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1110, '三阶嗜血治疗 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1111, '三阶嗜血治疗 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1112, '三阶嗜血治疗 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1113, '三阶嗜血治疗 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1114, '三阶嗜血治疗 (II) Lv3
', 12, 0, 0, 10000, '', '', 150, 255, 1, 'skillicon04.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1115, '三阶嗜血治疗 (II) Lv4
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1116, '四阶嗜血治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 336, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1117, '四阶嗜血治疗 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 336, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1118, '四阶嗜血治疗 (I) Lv3
', 12, 0, 0, 10000, '', '', 150, 255, 1, 'skillicon04.dds', 336, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1119, '四阶嗜血治疗 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 384, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1120, '四阶嗜血治疗 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 384, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1121, '四阶嗜血治疗 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 384, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1122, '三阶灵魂(I) Lv1
', 12, 0, 0, 10000, '', '', 150, 255, 1, 'skillicon04.dds', 432, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1123, '三阶灵魂(I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +15
', '', 150, 255, 1, 'skillicon04.dds', 432, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1124, '三阶灵魂(I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +35\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 432, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1125, '三阶灵魂(II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +15
', '', 150, 255, 1, 'skillicon04.dds', 0, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1126, '三阶灵魂(II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +20
', '', 150, 255, 1, 'skillicon04.dds', 0, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1127, '三阶灵魂(II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +25
', '', 150, 255, 1, 'skillicon04.dds', 0, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1128, '三阶灵魂(II) Lv4
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +45\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 0, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1129, '四阶灵魂(I) Lv1
', 12, 0, 0, 10000, '', '', 150, 255, 1, 'skillicon04.dds', 48, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1130, '四阶灵魂(I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +40
', '', 150, 255, 1, 'skillicon04.dds', 48, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1131, '四阶灵魂(I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +60\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 48, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1132, '四阶灵魂(II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP+40
', '', 150, 255, 1, 'skillicon04.dds', 96, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1133, '四阶灵魂(II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP+45
', '', 150, 255, 1, 'skillicon04.dds', 96, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1134, '四阶灵魂(II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +65\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 96, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1135, '三阶魔法治疗(I) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 144, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1136, '三阶魔法治疗(I) Lv2
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 144, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1137, '三阶魔法治疗(I) Lv3
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 144, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1138, '三阶魔法治疗(II) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 192, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1139, '三阶魔法治疗(II) Lv2
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 192, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1140, '三阶魔法治疗(II) Lv3
', 12, 0, 0, 10000, '', '', 150, 255, 1, 'skillicon04.dds', 192, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1141, '三阶魔法治疗(II) Lv4
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 192, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1142, '四阶魔法治疗(I) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1143, '四阶魔法治疗(I) Lv2
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1144, '四阶魔法治疗(I) Lv3
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1145, '四阶魔法治疗(II) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1146, '四阶魔法治疗(II) Lv2
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1147, '四阶魔法治疗(II) Lv3
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1148, '三阶变形 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +130
', '', 150, 255, 1, 'skillicon04.dds', 336, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1149, '三阶变形 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +140
', '', 150, 255, 1, 'skillicon04.dds', 336, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1150, '三阶变形 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +170\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 336, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1151, '三阶变形 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +140
', '', 150, 255, 1, 'skillicon04.dds', 384, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1152, '三阶变形 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +150
', '', 150, 255, 1, 'skillicon04.dds', 384, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1153, '三阶变形 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +160
', '', 150, 255, 1, 'skillicon04.dds', 384, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1154, '三阶变形 (II) Lv4
', 12, 0, 0, 10000, '', '', 150, 255, 1, 'skillicon04.dds', 384, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1155, '四阶变形 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +180
', '', 150, 255, 1, 'skillicon04.dds', 432, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1156, '四阶变形 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +190
', '', 150, 255, 1, 'skillicon04.dds', 432, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1157, '四阶变形 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +210
', '', 150, 255, 1, 'skillicon04.dds', 432, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1158, '四阶变形 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +190
', '', 150, 255, 1, 'skillicon04.dds', 0, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1159, '四阶变形 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +200
', '', 150, 255, 1, 'skillicon04.dds', 0, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1160, '四阶变形 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +230\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 0, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1161, '三阶蓄力 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 48, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1162, '三阶蓄力 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 48, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1163, '三阶蓄力 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 48, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1164, '三阶蓄力 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 96, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1165, '三阶蓄力 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 96, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1166, '三阶蓄力 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 96, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1167, '三阶蓄力 (II) Lv4
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分\n宠物敏捷 +1
', '', 150, 255, 1, 'skillicon04.dds', 96, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1168, '四阶蓄力 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 144, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1169, '四阶蓄力 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+2分
', '', 150, 255, 1, 'skillicon04.dds', 144, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1170, '四阶蓄力 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+3分
', '', 150, 255, 1, 'skillicon04.dds', 144, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1171, '四阶蓄力 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+4分
', '', 150, 255, 1, 'skillicon04.dds', 192, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1172, '四阶蓄力 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+4分
', '', 150, 255, 1, 'skillicon04.dds', 192, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1173, '四阶蓄力 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+6分\n宠物智力 +1', '', 150, 255, 1, 'skillicon04.dds', 192, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1174, '魔法嗜血 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +20\nMP +20
', '', 150, 255, 1, 'skillicon04.dds', 240, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1175, '魔法嗜血 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +25\nMP +25
', '', 150, 255, 1, 'skillicon04.dds', 240, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1176, '魔法嗜血 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +35\nMP +35
', '', 150, 255, 1, 'skillicon04.dds', 240, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1177, '魔法嗜血 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +25\nMP +25
', '', 150, 255, 1, 'skillicon04.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1178, '魔法嗜血 (II) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +30\nMP +30
', '', 150, 255, 1, 'skillicon04.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1179, '魔法嗜血 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +35\nMP +35
', '', 150, 255, 1, 'skillicon04.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1180, '魔法嗜血 (II) Lv4
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +45\nMP +45\n宠物力量 +1
', '', 150, 255, 1, 'skillicon04.dds', 288, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1181, '魔法嗜血跳跃 (I) Lv1', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +35\nMP +35
', '', 150, 255, 1, 'skillicon04.dds', 336, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1182, '魔法嗜血跳跃 (I) Lv2
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +40\nMP +40', '', 150, 255, 1, 'skillicon04.dds', 336, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1183, '魔法嗜血跳跃 (I) Lv3

', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +50\nMP +50
', '', 150, 255, 1, 'skillicon04.dds', 336, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1184, '魔法嗜血跳跃 (II) Lv1', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +40\nMP +40
', '', 150, 255, 1, 'skillicon04.dds', 384, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1185, '魔法嗜血跳跃 (II) Lv2', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +45\nMP +45
', '', 150, 255, 1, 'skillicon04.dds', 384, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1186, '魔法嗜血跳跃 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP, MP增加。\n\nHP +55\nMP +55\n宠物智力+1', '', 150, 255, 1, 'skillicon04.dds', 384, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1187, '三阶嗜血治疗 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下HP恢复增加。\n\nHP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 240, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1188, '三阶魔法治疗(I) Lv3
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1
', '', 150, 255, 1, 'skillicon04.dds', 144, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1189, '三阶魔法治疗(II) Lv3
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物智力 +1
', '', 150, 255, 1, 'skillicon04.dds', 192, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1190, '三阶嗜血 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。.\n\nHP +90
', '', 150, 255, 1, 'skillicon04.dds', 48, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1191, '三阶变形 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +170
', '', 150, 255, 1, 'skillicon04.dds', 336, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1192, '三阶变形 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +160\n宠物敏捷 +1
', '', 150, 255, 1, 'skillicon04.dds', 384, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1193, '三阶蓄力 (I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n变身持续时间+1分
', '', 150, 255, 1, 'skillicon04.dds', 48, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1194, '三阶蓄力 (II) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间+1分\n宠物敏捷 +1
', '', 150, 255, 1, 'skillicon04.dds', 96, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1195, '三阶灵魂(I) Lv3
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +35
', '', 150, 255, 1, 'skillicon04.dds', 432, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1196, '宠物的守护 (I)
', 13, 1, 0, 10000, '减少被近战攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1197, '宠物的守护 (II)
', 13, 1, 0, 10000, '减少被远程攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1198, '宠物的守护 (III)
', 13, 1, 0, 10000, '减少被魔法攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1199, '增加药水恢复量 (II)
', 13, 1, 0, 10000, '增加药水恢复量。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1200, '双倍暴击 (II)
', 13, 1, 0, 10000, '增加暴击几率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1201, '双倍伤害 (II)
', 13, 1, 0, 10000, '暴击时增加伤害值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1202, '双倍盾 (II)
', 13, 1, 0, 10000, '受到的受到的伤害量减少。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1203, '宠物的庇护 (I)
', 13, 1, 0, 10000, '减少被近战攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1204, '宠物的庇护 (II)
', 13, 1, 0, 10000, '减少被远程攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1205, '宠物的庇护 (III)
', 13, 1, 0, 10000, '减少被魔法攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1206, '增加药水恢复量 (III)
', 13, 1, 0, 10000, '增加药水恢复量。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1207, '双倍暴击 (III)
', 13, 1, 0, 10000, '增加暴击几率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1208, '双倍伤害 (III)
', 13, 1, 0, 10000, '暴击时增加伤害值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1209, '双倍盾 (III)
', 13, 1, 0, 10000, '受到的受到的伤害量减少。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1210, '提升魔法命中率 (II)
', 13, 1, 0, 10000, '增加魔法命中率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1211, '魔力的流动 (II)
', 13, 1, 0, 10000, '减少使用技能时需要的魔力消耗。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1212, '提升魔法命中率 (III)
', 13, 1, 0, 10000, '增加魔法命中率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1213, '魔力的流动 (III)
', 13, 1, 0, 10000, '减少使用技能时需要的魔力消耗。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1214, '超强大地之德拉克 (I)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1215, '宠物的祝福 (II)
', 13, 1, 0, 10000, '增加狩猎时经验值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1216, '超强大地之德拉克 (II)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1217, '双倍特效 (II)
', 13, 1, 0, 10000, '角色周边产生特效，闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1218, '凶猛的大地之德拉克 (I)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1219, '宠物的祝福 (III)
', 13, 1, 0, 10000, '狩猎时经验值增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1220, '凶猛的大地之德拉克 (II)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1221, '双倍特效 (III)
', 13, 1, 0, 10000, '角色周边产生特效，闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1222, '超强太阳之德拉克 (I)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1223, '超强太阳之德拉克 (II)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1224, '凶猛的太阳之德拉克 (I)
', 13, 1, 0, 10000, '使用德拉克戒指时外形变更, 近距离攻击减少伤害, 闪避增加\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1225, '凶猛的太阳之德拉克 (II)
', 13, 1, 0, 10000, '使用德拉克戒指时外形变更, 近距离攻击减少伤害, 闪避增加\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1226, '超强风之德拉克 (I)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1227, '超强风之德拉克 (II)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1228, '凶猛的风之德拉克 (I)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及减少所受魔法伤害，回避增加。\n\n一次性技能', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1229, '凶猛的风之德拉克 (II)
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及减少所受魔法伤害，回避增加。\n\n一次性技能', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1230, '阿尔忒弥斯变身
', 12, 1, 0, 10000, '赋予阿尔忒弥斯的力量。 \n使用技能后，装备变身项链时变身为阿尔忒弥斯。', '使用了变身技能。
', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1231, '阿波罗变身
', 12, 1, 0, 10000, '赋予阿波罗的力量。 \n使用技能后，装备变身项链时变身为阿波罗。
', '使用了变身技能。
', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1232, '炼金术士(II) 变身
', 12, 1, 0, 10000, '赋予炼金术士(II)的力量。 \n使用技能后，装备变身项链时变身为炼金术士(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1233, '忍者少女(II) 变身
', 12, 1, 0, 10000, '赋予忍者少女(II)的力量。 \n使用技能后，装备变身项链时变身为忍者少女(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1234, '巴德(II) 变身
', 12, 1, 0, 10000, '赋予巴德(II)的力量。 \n使用技能后，装备变身项链时变身为巴德(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1235, '女王骑士(II) 变身
', 12, 1, 0, 10000, '赋予女王骑士(II)的力量。 \n使用技能后，装备变身项链时变身为女王骑士(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1236, '女海盗船长(II) 变身
', 12, 1, 0, 10000, '赋予女海盗船长(II)的力量。 \n使用技能后，装备变身项链时变身为女海盗船长(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1237, '修道士(II) 变身
', 12, 1, 0, 10000, '赋予修道士(II)的力量。 \n使用技能后，装备变身项链时变身为修道士(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1238, '阿修罗(II) 变身
', 12, 1, 0, 10000, '赋予阿修罗(II)的力量。 \n使用技能后，装备变身项链时变身为阿修罗(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1239, '大力神(II) 变身
', 12, 1, 0, 10000, '赋予大力神(II)的力量。 \n使用技能后，装备变身项链时变身为大力神(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1240, '狂战士(II) 变身
', 12, 1, 0, 10000, '赋予狂战士(II)的力量。 \n使用技能后，装备变身项链时变身为狂战士(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1241, '男爵火枪手(II) 变身
', 12, 1, 0, 10000, '赋予男爵火枪手(II)的力量。 \n使用技能后，装备变身项链时变身为男爵火枪手(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1242, '惊艳女狙击手(II) 变身
', 12, 1, 0, 10000, '赋予惊艳女狙击手(II)的力量。 \n使用技能后，装备变身项链时变身为惊艳女狙击手(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1243, '暮色黑寡妇(II) 变身
', 12, 1, 0, 10000, '赋予暮色黑寡妇(II)的力量。 \n使用技能后，装备变身项链时变身为暮色黑寡妇(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1244, '黑暗侍卫(II) 变身
', 12, 1, 0, 10000, '赋予暗黑侍卫(II)的力量。 \n使用技能后，装备变身项链时变身为暗黑侍卫(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1245, '玲珑天使(II) 变身
', 12, 1, 0, 10000, '赋予玲珑天使(II)的力量。 \n使用技能后，装备变身项链时变身为玲珑天使(II)。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1246, '骑士的力量
', 12, 1, 0, 10000, '可获得光之加护。', '提升光的力量进行魔法回避。', 150, 255, 100, 'skillicon04.dds', 432, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1247, '光之加护
', 12, 1, 0, 10000, '使用技能时，小幅提升近战攻击，命中。\nHP低于50%时，效果大幅提升。', '感受到骑士的力量。', 150, 1, 100, 'skillicon04.dds', 432, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1248, '骑士的韧性
', 12, 1, 0, 10000, '使用技能时，小幅提升药水恢复效果。\nHP低于50%时，效果大幅提升。', '感受到骑士的韧性。', 150, 1, 100, 'skillicon04.dds', 0, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1249, '游侠的力量
', 12, 1, 0, 10000, '使用技能时，小幅提升远程攻击，命中。\nHP低于50%时，效果大幅提升。', '感受到游侠的力量。', 150, 2, 100, 'skillicon04.dds', 48, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1250, '游侠的韧性
', 12, 1, 0, 10000, '使用技能时，小幅提升所有回避。\nHP低于50%时，效果大幅提升。', '感受到游侠的韧性。', 150, 2, 100, 'skillicon04.dds', 96, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1251, '精灵的力量
', 12, 1, 0, 10000, '使用技能时，小幅提升近战攻击，命中。\nHP低于50%时，效果大幅提升。', '感受到精灵的力量。', 150, 4, 100, 'skillicon04.dds', 336, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1252, '精灵的韧性
', 12, 1, 0, 10000, '使用技能时，小幅提升魔法命中。\nHP低于50%时，效果大幅提升。', '感受到精灵的韧性。', 150, 4, 100, 'skillicon04.dds', 384, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1253, '刺客的力量
', 12, 1, 0, 10000, '使用技能时，小幅提升近战命中， 暴击伤害。\nHP低于50%时，效果大幅提升。', '感受到刺客的力量。', 150, 8, 100, 'skillicon04.dds', 144, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1254, '刺客的韧性
', 12, 1, 0, 10000, '使用技能时，小幅提升所有回避。\nHP低于50%时，效果大幅提升。', '感受到刺客的韧性。', 150, 8, 100, 'skillicon04.dds', 192, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1255, '召唤师的力量
', 12, 1, 0, 10000, '使用技能时，小幅提升近战命中，暴击率。\nHP低于50%时，效果大幅提升。', '感受到召唤师的力量。', 150, 16, 100, 'skillicon04.dds', 240, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1256, '召唤师的韧性
', 12, 1, 0, 10000, '使用技能时，小幅提升所有防御力。\nHP低于50%时，效果大幅提升。', '感受到召唤师的韧性。', 150, 16, 100, 'skillicon04.dds', 288, 336, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1257, '五阶嗜血 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +110\n宠物力量 +3', '', 150, 255, 1, 'skillicon04.dds', 0, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1258, '五阶嗜血 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +130\n宠物力量 +3', '', 150, 255, 1, 'skillicon04.dds', 48, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1259, '五阶嗜血治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下恢复HP。\n\nHP恢复 +3\n宠物力量 +3', '', 150, 255, 1, 'skillicon04.dds', 96, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1260, '五阶嗜血治疗 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下恢复HP。\n\nHP恢复 +4\n宠物力量 +3', '', 150, 255, 1, 'skillicon04.dds', 144, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1261, '五阶灵魂 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +70\n宠物智力 +3', '', 150, 255, 1, 'skillicon04.dds', 192, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1262, '五阶灵魂 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +90\n宠物智力 +3', '', 150, 255, 1, 'skillicon04.dds', 240, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1263, '五阶魔法治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +2\n宠物智力 +3', '', 150, 255, 1, 'skillicon04.dds', 288, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1264, '五阶魔法治疗 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +3\n宠物智力 +3', '', 150, 255, 1, 'skillicon04.dds', 336, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1265, '五阶变形 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +200\n宠物的力量，敏捷，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 384, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1266, '五阶变形 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +230\n宠物的力量，敏捷，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 432, 384, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1267, '五阶蓄力 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间 +5分钟\n宠物的力量，敏捷，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 0, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1268, '五阶蓄力 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间 +5分钟\n宠物的力量，敏捷，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 48, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1269, '魔法嗜血冲刺 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP，MP增加。\n\nHP +45\nMP +45\n宠物的力量，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 96, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1270, '魔法嗜血冲刺 (II) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP， MP增加。\n\nHP +50\nMP +50\n宠物的力量，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 144, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1271, '四阶嗜血治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下恢复HP。\n\nHP恢复 +1\n宠物的力量，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 336, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1272, '四阶魔法治疗(I) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物的力量，智力 +3', '', 150, 255, 1, 'skillicon04.dds', 240, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1273, '四阶嗜血 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大HP增加。\n\nHP +100\n宠物的力量，敏捷 +3', '', 150, 255, 1, 'skillicon04.dds', 144, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1274, '四阶变形 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +180\n宠物力量，敏捷 +3', '', 150, 255, 1, 'skillicon04.dds', 432, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1275, '四阶嗜血治疗 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色在负重状态下恢复HP。\n\nHP恢复 +1\n宠物的力量，敏捷 +3', '', 150, 255, 1, 'skillicon04.dds', 336, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1276, '四阶蓄力 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间 +1分钟\n宠物的力量，敏捷 +3', '', 150, 255, 1, 'skillicon04.dds', 144, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1277, '四阶灵魂(I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大MP增加。\n\nMP +35\n宠物敏捷，智力 +3
', '', 150, 255, 1, 'skillicon04.dds', 48, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1278, '四阶变形 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的最大负重增加。\n\n负重 +180\n宠物敏捷，智力 +3
', '', 150, 255, 1, 'skillicon04.dds', 432, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1279, '四阶魔法治疗(I) Lv1
', 12, 0, 0, 10000, '宠物的力量在负重状态下可恢复角色的MP。\n\nMP恢复 +1\n宠物敏捷，智力 +3
', '', 150, 255, 1, 'skillicon04.dds', 240, 240, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1280, '四阶蓄力 (I) Lv1
', 12, 0, 0, 10000, '宠物的力量使角色的变身时间增加。\n\n变身持续时间 +1分钟\n宠物敏捷，智力 +3
', '', 150, 255, 1, 'skillicon04.dds', 144, 288, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1281, '宠物的奇迹 (I)
', 13, 1, 0, 10000, '减少被近战攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 96, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1282, '宠物的奇迹 (II)
', 13, 1, 0, 10000, '减少被远程攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1283, '宠物的奇迹 (III)
', 13, 1, 0, 10000, '减少被魔法攻击所受到的伤害。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 48, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1284, '增加药水恢复量 (IV)
', 13, 1, 0, 10000, '增加药水恢复量。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 96, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1285, '双倍暴击 (IV)
', 13, 1, 0, 10000, '增加暴击几率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 144, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1286, '双倍伤害 (IV)
', 13, 1, 0, 10000, '暴击时增加伤害值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 192, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1287, '双重盾 (IV)
', 13, 1, 0, 10000, '受到的伤害量减少。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 240, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1288, '提升魔法命中率 (IV)
', 13, 1, 0, 10000, '增加魔法命中率。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 336, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1289, '魔力的流动 (IV)
', 13, 1, 0, 10000, '减少使用技能时需要的魔力消耗。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 288, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1290, '荒野德拉克
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及所受到的远程攻击伤害减少，振荡射击回避率，回避增加。\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1291, '宠物的祝福 (IV)
', 13, 1, 0, 10000, '增加狩猎时经验值。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 0, 192, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1292, '平原德拉克
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及所受到的远程攻击伤害减少，振荡射击回避率，回避增加。\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1293, '双倍特效 (IV)
', 13, 1, 0, 10000, '角色周边产生特效，闪避增加。\n\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 432, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1294, '红焰德拉克
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及所受到的近战伤害减少，昏厥回避概率，回避增加。\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1295, '白光德拉克
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及所受到的近战伤害减少，昏厥回避概率，回避增加。\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1296, '疾风德拉克
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及减少所收到的魔法攻击伤害，蛛网术回避率，回避增加。\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1297, '旋风德拉克
', 13, 1, 0, 10000, '使用德拉克戒指时德拉克外形变更及所受到的魔法攻击伤害减少，蛛网术回避率，回避增加。\n一次性技能
', '', 150, 255, 1, 'skillicon04.dds', 384, 144, 1, 0, 1);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1298, '火焰伊格里特
', 12, 1, 0, 10000, '赋予火焰伊格里特的力量。\n使用技能后，装备变身项链时变身为火焰伊格里特。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1299, '火焰伊格里特弓箭手
', 12, 1, 0, 10000, '赋予火魔伊格里特弓箭手的力量。\n使用技能后，装备变身项链时变身为火魔伊格里特弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1300, '火魔伊格里特
', 12, 1, 0, 10000, '赋予火魔伊格里特的力量。\n使用技能后，装备变身项链时变身为火魔伊格里特。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1301, '火魔伊格里特弓箭手
', 12, 1, 0, 10000, '赋予火魔伊格里特弓箭手的力量。\n使用技能后，装备变身项链时变身为火魔伊格里特弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1302, '深海利维坦
', 12, 1, 0, 10000, '赋予深海利维坦的力量。\n使用技能后，装备变身项链时变身为深海利维坦。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1303, '深海利维坦弓箭手
', 12, 1, 0, 10000, '赋予深海利维坦弓箭手的力量。\n使用技能后，装备变身项链时变身为深海利维坦弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1304, '天海利维坦
', 12, 1, 0, 10000, '赋予天海利维坦的力量。\n使用技能后，装备变身项链时变身为天海利维坦。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1305, '天海利维坦弓箭手
', 12, 1, 0, 10000, '赋予天海利维坦弓箭手的力量。\n使用技能后，装备变身项链时变身为天海利维坦弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1306, '荒野忒亚
', 12, 1, 0, 10000, '赋予荒野忒亚的力量。\n使用技能后，装备变身项链时变身为荒野忒亚。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1307, '荒野忒亚弓箭手
', 12, 1, 0, 10000, '赋予荒野忒亚弓箭手的力量。\n使用技能后，装备变身项链时变身为荒野忒亚弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1308, '平原忒亚
', 12, 1, 0, 10000, '赋予平原忒亚的力量。\n使用技能后，装备变身项链时变身为平原忒亚。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1309, '平原忒亚弓箭手
', 12, 1, 0, 10000, '赋予平原忒亚弓箭手的力量。\n使用技能后，装备变身项链时变身为平原忒亚弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1310, '月光卡利戈
', 12, 1, 0, 10000, '赋予月光卡利戈的力量。\n使用技能后，装备变身项链时变身为月光卡利戈。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1311, '月光卡利戈弓箭手
', 12, 1, 0, 10000, '赋予月光卡利戈弓箭手的力量。\n使用技能后，装备变身项链时变身为月光卡利戈弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1312, '弦月卡利戈
', 12, 1, 0, 10000, '赋予弦月卡利戈的力量。\n使用技能后，装备变身项链时变身为弦月卡利戈。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1313, '弦月卡利戈弓箭手
', 12, 1, 0, 10000, '赋予弦月卡利戈弓箭手的力量。\n使用技能后，装备变身项链时变身为弦月卡利戈弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1314, '红焰鲁门
', 12, 1, 0, 10000, '赋予红焰鲁门的力量。\n使用技能后，装备变身项链时变身为红焰鲁门。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1315, '红焰鲁门弓箭手
', 12, 1, 0, 10000, '赋予红焰鲁门弓箭手的力量。\n使用技能后，装备变身项链时变身为红焰鲁门弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1316, '白光鲁门
', 12, 1, 0, 10000, '赋予白光鲁门的力量。\n使用技能后，装备变身项链时变身为白光鲁门。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1317, '白光鲁门弓箭手
', 12, 1, 0, 10000, '赋予白光鲁门弓箭手的力量。\n使用技能后，装备变身项链时变身为白光鲁门弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1318, '旋风文图斯
', 12, 1, 0, 10000, '赋予旋风文图斯的力量。\n使用技能后，装备变身项链时变身为旋风文图斯。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1319, '旋风文图斯弓箭手
', 12, 1, 0, 10000, '赋予旋风文图斯弓箭手的力量。\n使用技能后，装备变身项链时变身为旋风文图斯弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1320, '疾风文图斯
', 12, 1, 0, 10000, '赋予疾风文图斯的力量。\n使用技能后，装备变身项链时变身为疾风文图斯。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1321, '疾风文图斯弓箭手
', 12, 1, 0, 10000, '赋予疾风文图斯弓箭手的力量。\n使用技能后，装备变身项链时变身为疾风文图斯弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1322, '宠物力量
', 13, 1, 0, 10000, '使用技能时，确认范围内所有成员的宠物，并赋予特殊效果。\n所确认宠物变身种类越多，效果越高。
', '确认组员的宠物变身状态。', 150, 255, 1, 'skillicon04.dds', 192, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1323, '暗黑暗杀团变身
', 12, 1, 0, 10000, '赋予暗黑暗杀团的力量。\n使用技能后，装备变身项链时变身为暗黑。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1324, '漆黑暗杀团变身
', 12, 1, 0, 10000, '赋予漆黑暗杀团的力量。\n使用技能后，装备变身项链时变身为漆黑。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1325, '(真)火焰伊格里特
', 12, 1, 0, 10000, '赋予(真)火焰伊格里特的力量。\n使用技能后，装备变身项链时变身为(真)火焰伊格里特。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1326, '(真)火魔伊格里特弓箭手
', 12, 1, 0, 10000, '赋予(真)火魔伊格里特弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)火魔伊格里特弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1327, '(真)火魔伊格里特
', 12, 1, 0, 10000, '赋予(真)火魔伊格里特的力量。\n使用技能后，装备变身项链时变身为(真)火魔伊格里特。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1328, '(真)火魔伊格里特弓箭手
', 12, 1, 0, 10000, '赋予(真)火魔伊格里特弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)火魔伊格里特弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1329, '(真)深海利维坦
', 12, 1, 0, 10000, '赋予(真)深海利维坦的力量。\n使用技能后，装备变身项链时变身为(真)深海利维坦。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1330, '(真)深海利维坦弓箭手
', 12, 1, 0, 10000, '赋予(真)深海利维坦弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)深海利维坦弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1331, '(真)天海利维坦
', 12, 1, 0, 10000, '赋予(真)天海利维坦的力量。\n使用技能后，装备变身项链时变身为(真)天海利维坦。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1332, '(真)天海利维坦弓箭手
', 12, 1, 0, 10000, '赋予(真)天海利维坦弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)天海利维坦弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1333, '(真)荒野忒亚
', 12, 1, 0, 10000, '赋予(真)荒野忒亚的力量。\n使用技能后，装备变身项链时变身为(真)荒野忒亚。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1334, '(真)荒野忒亚弓箭手
', 12, 1, 0, 10000, '赋予(真)荒野忒亚弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)荒野忒亚弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1335, '(真)平原忒亚
', 12, 1, 0, 10000, '赋予(真)平原忒亚的力量。\n使用技能后，装备变身项链时变身为(真)平原忒亚。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1336, '(真)平原忒亚弓箭手
', 12, 1, 0, 10000, '赋予(真)平原忒亚弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)平原忒亚弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1337, '(真)月光卡利戈
', 12, 1, 0, 10000, '赋予(真)月光卡利戈的力量。\n使用技能后，装备变身项链时变身为(真)月光卡利戈。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1338, '(真)月光卡利戈弓箭手
', 12, 1, 0, 10000, '赋予(真)月光卡利戈弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)月光卡利戈弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1339, '(真)弦月卡利戈
', 12, 1, 0, 10000, '赋予(真)弦月卡利戈的力量。\n使用技能后，装备变身项链时变身为(真)弦月卡利戈。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1340, '(真)弦月卡利戈弓箭手
', 12, 1, 0, 10000, '赋予(真)弦月卡利戈弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)弦月卡利戈弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1341, '(真)红焰鲁门
', 12, 1, 0, 10000, '赋予(真)红焰鲁门的力量。\n使用技能后，装备变身项链时变身为(真)红焰鲁门。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1342, '(真)红焰鲁门弓箭手
', 12, 1, 0, 10000, '赋予(真)红焰鲁门弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)红焰鲁门弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1343, '(真)白光鲁门
', 12, 1, 0, 10000, '赋予(真)白光鲁门的力量。\n使用技能后，装备变身项链时变身为(真)白光鲁门。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1344, '(真)白光鲁门弓箭手
', 12, 1, 0, 10000, '赋予(真)白光鲁门弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)白光鲁门弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1345, '(真)旋风文图斯
', 12, 1, 0, 10000, '赋予(真)旋风文图斯的力量。\n使用技能后，装备变身项链时变身为(真)旋风文图斯。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1346, '(真)旋风文图斯弓箭手
', 12, 1, 0, 10000, '赋予(真)旋风文图斯弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)旋风文图斯弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1347, '(真)疾风文图斯
', 12, 1, 0, 10000, '赋予(真)疾风文图斯的力量。\n使用技能后，装备变身项链时变身为(真)疾风文图斯。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1348, '(真)疾风文图斯弓箭手
', 12, 1, 0, 10000, '赋予(真)疾风文图斯弓箭手的力量。\n使用技能后，装备变身项链时变身为(真)疾风文图斯弓箭手。
', '使用了变身技能。', 150, 255, 1, 'skillicon01.dds', 288, 192, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1349, '宠物的反力场
', 33, 2, 0, 10000, '对宠物的力场进行约束。
', '使用了宠物反力场。', 1200, 255, 1, 'skillicon04.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1350, '宠物的反力场
', 33, 2, 0, 10000, '对宠物的力场进行约束。
', '使用了宠物反力场。', 1200, 255, 1, 'skillicon04.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1351, '宠物的反力场
', 33, 2, 0, 10000, '对宠物的力场进行约束。
', '使用了宠物反力场。', 1200, 255, 1, 'skillicon04.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1352, '宠物的反力场
', 33, 2, 0, 10000, '对宠物的力场进行约束。
', '使用了宠物反力场。', 1200, 255, 1, 'skillicon04.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1353, '宠物的反力场
', 33, 2, 0, 10000, '对宠物的力场进行约束。
', '使用了宠物反力场。', 1200, 255, 1, 'skillicon04.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1354, '宠物的反力场
', 33, 2, 0, 10000, '对宠物的力场进行约束。
', '使用了宠物反力场。', 1200, 255, 1, 'skillicon04.dds', 240, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1355, '宠物的力场
', 33, 2, 0, 10000, '减少目标和周边宠物的能力值。
', '使用了宠物力场。', 1200, 255, 1, 'skillicon04.dds', 288, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1356, '宠物的力场
', 33, 2, 0, 10000, '减少目标和周边宠物的能力值。
', '使用了宠物力场。', 1200, 255, 1, 'skillicon04.dds', 288, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1357, '宠物的力场
', 33, 2, 0, 10000, '减少目标和周边宠物的能力值。
', '使用了宠物力场。', 1200, 255, 1, 'skillicon04.dds', 288, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1358, '宠物的力场
', 33, 2, 0, 10000, '减少目标和周边宠物的能力值。
', '使用了宠物力场。', 1200, 255, 1, 'skillicon04.dds', 288, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1359, '宠物的力场
', 33, 2, 0, 10000, '减少目标和周边宠物的能力值。
', '使用了宠物力场。', 1200, 255, 1, 'skillicon04.dds', 288, 432, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillPack] ([mSPID], [mName], [mIType], [mIUseType], [mISubType], [mTermOfValidity], [mDesc], [mUseMsg], [mUseRange], [mUseClass], [mUseLevel], [mSpriteFile], [mSpriteX], [mSpriteY], [mIsUseableUTGWSvr], [mUseInAttack], [mIsDrop]) VALUES (1360, '宠物的力场
', 33, 2, 0, 10000, '减少目标和周边宠物的能力值。
', '使用了宠物力场。', 1200, 255, 1, 'skillicon04.dds', 288, 432, 1, 0, 0);
GO

