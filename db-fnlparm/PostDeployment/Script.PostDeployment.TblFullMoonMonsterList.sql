/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblFullMoonMonsterList
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1983, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1984, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1985, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1986, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1987, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1988, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1989, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1990, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1991, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1992, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1993, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1994, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1995, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1996, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1997, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1998, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (1999, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2000, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2001, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2002, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2003, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2004, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2005, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2006, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2023, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2023, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2024, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2024, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2025, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2025, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2026, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2026, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2027, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2027, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2028, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2028, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2029, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2030, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2030, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2031, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2031, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2032, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2032, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2033, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2033, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2034, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2034, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2035, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2036, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2037, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2037, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2044, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2044, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2047, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2047, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2048, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2048, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2049, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2049, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2121, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2121, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2122, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2122, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2610, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2611, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2612, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2613, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2614, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2615, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2616, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2617, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2618, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2619, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2620, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2621, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2622, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2623, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2624, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2624, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2625, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2625, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2634, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2634, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2635, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2635, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2636, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2636, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2637, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2637, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2638, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2638, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2639, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2639, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2664, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2664, 1);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2667, 0);
GO

INSERT INTO [dbo].[TblFullMoonMonsterList] ([mMID], [mIsFullMoon]) VALUES (2667, 1);
GO

