/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Character
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (68, '01', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (69, '7검3셋', 1, 30, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (70, '6검3셋', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (71, '9검6셋30렙', 1, 30, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (72, '9검9셋', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (73, '1렙0검0셋', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (74, '1렙기본장비', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (75, 'Stargazer', 1, 30, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (76, '6검9갑', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (77, '본오오', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (78, '1렙0검0셋2', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (80, '생그지', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (68, '01', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (69, '7검3셋', 1, 30, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (70, '6검3셋', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (71, '9검6셋30렙', 1, 30, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (72, '9검9셋', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (73, '1렙0검0셋', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (74, '1렙기본장비', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (75, 'Stargazer', 1, 30, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (76, '6검9갑', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (77, '본오오', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (78, '1렙0검0셋2', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Character] ([CID], [CName], [CClass], [CLevel], [CExp], [CAttackRate], [CMoveRate], [CHP], [CMP], [CZoneID], [CPosX], [CPosY], [CPosZ], [CHead], [CFace], [CBody]) VALUES (80, '생그지', 1, 1, 0, 0, 500, 0, 0, 0, 0.0, 0.0, 0.0, 0, 0, 0);
GO

