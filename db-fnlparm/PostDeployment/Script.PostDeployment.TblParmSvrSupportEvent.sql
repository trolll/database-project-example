/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblParmSvrSupportEvent
Date                  : 2023-10-07 09:08:57
*/


INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 11, '2008-09-09 00:00:00.000', '2008-09-16 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 12, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 13, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 14, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 15, '2008-12-16 00:00:00.000', '2008-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 16, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 17, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 18, '2009-02-10 07:05:00.000', '2009-02-24 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 19, '2009-03-26 08:00:00.000', '2009-04-09 08:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 20, '2009-07-01 00:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 21, '2009-08-25 09:00:00.000', '2009-09-01 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 22, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 23, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 24, '2009-09-30 18:00:00.000', '2009-10-09 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 25, '2009-10-27 07:00:00.000', '2009-11-03 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 26, '2009-12-16 07:00:00.000', '2010-01-05 08:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 27, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 28, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 29, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 30, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 31, '2011-09-06 11:00:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 32, '2011-09-06 10:40:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 33, '2011-09-06 10:50:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 34, '2011-08-01 00:00:00.000', '2020-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 35, '2012-01-01 00:00:00.000', '2012-02-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 36, '2012-01-01 12:11:00.000', '2022-01-01 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 37, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 38, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 39, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 40, '2012-10-23 15:00:00.000', '2012-11-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 41, '2013-02-06 10:00:00.000', '2013-02-19 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 42, '2009-01-01 12:00:00.000', '2013-03-19 15:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 43, '2009-01-01 12:00:00.000', '2013-04-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 44, '2013-06-11 10:00:00.000', '2013-06-25 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 45, '2013-07-30 09:00:00.000', '2013-07-30 11:20:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 46, '2013-08-12 12:00:00.000', '2013-08-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 47, '2013-09-18 10:00:00.000', '2013-10-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 48, '2013-12-23 10:00:00.000', '2014-01-07 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 49, '2014-01-28 10:00:00.000', '2014-02-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 50, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 51, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 52, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 53, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 54, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 55, '2014-04-29 10:00:00.000', '2014-05-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 56, '2014-05-08 10:00:00.000', '2014-05-13 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 57, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 58, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 59, '2014-09-02 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 60, '2014-11-25 10:00:00.000', '2014-12-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 61, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 62, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 63, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 64, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 65, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 66, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 67, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 68, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 69, '2015-10-15 08:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 70, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 71, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 73, '2018-02-13 10:00:00.000', '2018-02-22 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 74, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 75, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 76, '2019-05-30 10:00:00.000', '2019-06-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 77, '2018-07-24 10:00:00.000', '2018-07-31 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 78, '2018-09-27 10:00:00.000', '2018-10-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 79, '2019-05-29 10:00:00.000', '2019-05-28 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 80, '2018-11-20 10:00:00.000', '2018-11-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 81, '2018-12-25 10:00:00.000', '2019-01-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 82, '2019-01-31 12:00:00.000', '2019-02-12 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 84, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 85, '2019-04-16 10:00:00.000', '2019-04-16 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 86, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1, 88, '2019-06-25 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 11, '2008-09-09 00:00:00.000', '2008-09-16 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 12, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 13, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 14, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 15, '2008-12-16 00:00:00.000', '2008-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 16, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 17, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 18, '2009-02-10 07:05:00.000', '2009-02-24 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 19, '2009-03-26 08:00:00.000', '2009-04-09 08:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 20, '2009-07-01 00:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 21, '2009-08-25 09:00:00.000', '2009-09-01 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 22, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 23, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 24, '2009-09-30 18:00:00.000', '2009-10-09 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 25, '2009-10-27 07:00:00.000', '2009-11-03 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 26, '2009-12-16 07:00:00.000', '2010-01-05 08:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 27, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 28, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 29, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 30, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 31, '2011-09-06 11:00:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 32, '2011-09-06 10:40:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 33, '2011-09-06 10:50:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 34, '2011-08-01 00:00:00.000', '2020-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 35, '2012-01-01 00:00:00.000', '2012-02-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 36, '2012-01-01 12:11:00.000', '2022-01-01 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 37, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 38, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 39, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 40, '2012-10-23 15:00:00.000', '2012-11-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 41, '2013-02-06 10:00:00.000', '2013-02-19 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 42, '2009-01-01 12:00:00.000', '2013-03-19 15:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 43, '2009-01-01 12:00:00.000', '2013-04-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 44, '2013-06-11 10:00:00.000', '2013-06-25 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 45, '2013-07-30 09:00:00.000', '2013-07-30 11:20:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 46, '2013-08-12 12:00:00.000', '2013-08-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 47, '2013-09-18 10:00:00.000', '2013-10-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 48, '2013-12-23 10:00:00.000', '2014-01-07 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 49, '2014-01-28 10:00:00.000', '2014-02-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 50, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 51, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 52, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 53, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 54, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 55, '2014-04-29 10:00:00.000', '2014-05-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 56, '2014-05-08 10:00:00.000', '2014-05-13 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 57, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 58, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 59, '2014-09-02 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 60, '2014-11-25 10:00:00.000', '2014-12-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 61, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 62, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 63, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 64, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 65, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 66, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 67, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 68, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 69, '2015-10-15 08:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 70, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 71, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 73, '2018-02-13 10:00:00.000', '2018-02-22 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 74, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 75, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 76, '2019-05-30 10:00:00.000', '2019-06-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 77, '2018-07-24 10:00:00.000', '2018-07-31 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 78, '2018-09-27 10:00:00.000', '2018-10-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 79, '2019-05-29 10:00:00.000', '2019-05-28 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 80, '2018-11-20 10:00:00.000', '2018-11-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 81, '2018-12-25 10:00:00.000', '2019-01-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 82, '2019-01-31 12:00:00.000', '2019-02-12 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 84, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 85, '2019-04-16 10:00:00.000', '2019-04-16 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 86, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (999, 88, '2019-06-25 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 7, '2008-01-29 10:00:00.000', '2008-02-13 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 8, '2008-05-07 07:00:00.000', '2008-06-11 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 9, '2008-05-01 09:34:00.000', '2070-12-31 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 10, '2008-06-11 07:06:00.000', '2079-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 11, '2008-09-09 00:00:00.000', '2008-09-16 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 12, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 13, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 14, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 15, '2008-12-16 00:00:00.000', '2008-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 16, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 17, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 18, '2009-02-10 07:05:00.000', '2009-02-24 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 19, '2009-03-26 08:00:00.000', '2009-04-09 08:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 20, '2009-07-01 00:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 21, '2009-08-25 09:00:00.000', '2009-09-01 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 22, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 23, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 24, '2009-09-30 18:00:00.000', '2009-10-09 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 25, '2009-10-27 07:00:00.000', '2009-11-03 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 26, '2009-12-16 07:00:00.000', '2010-01-05 08:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 27, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 28, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 29, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 30, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 31, '2011-09-06 11:00:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 32, '2011-09-06 10:40:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 33, '2011-09-06 10:50:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 34, '2011-08-01 00:00:00.000', '2020-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 35, '2012-01-01 00:00:00.000', '2012-02-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 36, '2012-01-01 12:11:00.000', '2022-01-01 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 37, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 38, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 39, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 40, '2012-10-23 15:00:00.000', '2012-11-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 41, '2013-02-06 10:00:00.000', '2013-02-19 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 42, '2009-01-01 12:00:00.000', '2013-03-19 15:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 43, '2009-01-01 12:00:00.000', '2013-04-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 44, '2013-06-11 10:00:00.000', '2013-06-25 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 45, '2013-07-30 09:00:00.000', '2013-07-30 11:20:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 46, '2013-08-12 12:00:00.000', '2013-08-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 47, '2013-09-18 10:00:00.000', '2013-10-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 48, '2013-12-23 10:00:00.000', '2014-01-07 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 49, '2014-01-28 10:00:00.000', '2014-02-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 50, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 51, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 52, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 53, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 54, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 55, '2014-04-29 10:00:00.000', '2014-05-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 56, '2014-05-08 10:00:00.000', '2014-05-13 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 57, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 58, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 59, '2014-09-02 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 60, '2014-11-25 10:00:00.000', '2014-12-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 61, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 62, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 63, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 64, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 65, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 66, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 67, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 68, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 69, '2015-10-15 08:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 70, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 71, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 73, '2018-02-13 10:00:00.000', '2018-02-22 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 74, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 75, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 76, '2019-05-30 10:00:00.000', '2019-06-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 77, '2018-07-31 12:00:00.000', '2018-08-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 78, '2018-09-27 10:00:00.000', '2018-10-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 79, '2019-05-29 10:00:00.000', '2019-05-28 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 80, '2018-11-20 10:00:00.000', '2018-11-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 81, '2018-12-25 10:00:00.000', '2019-01-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 82, '2019-01-30 10:00:00.000', '2019-01-30 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 84, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 85, '2019-04-16 10:00:00.000', '2019-04-16 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 86, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1164, 88, '2019-06-25 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 7, '2008-01-29 10:00:00.000', '2008-02-13 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 8, '2008-05-07 07:00:00.000', '2008-06-11 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 9, '2008-05-01 09:34:00.000', '2070-12-31 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 10, '2008-06-11 07:06:00.000', '2079-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 11, '2008-09-09 00:00:00.000', '2008-09-16 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 12, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 13, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 14, '2008-07-31 00:00:00.000', '2070-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 15, '2008-12-16 00:00:00.000', '2008-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 16, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 17, '2009-01-22 00:00:00.000', '2009-02-05 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 18, '2009-02-10 07:05:00.000', '2009-02-24 07:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 19, '2009-03-26 08:00:00.000', '2009-04-09 08:05:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 20, '2009-07-01 00:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 21, '2009-08-25 09:00:00.000', '2009-09-01 07:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 22, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 23, '2009-09-22 07:00:00.000', '2020-01-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 24, '2009-09-30 18:00:00.000', '2009-10-09 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 25, '2009-10-27 07:00:00.000', '2009-11-03 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 26, '2009-12-16 07:00:00.000', '2010-01-05 08:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 27, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 28, '2011-05-31 12:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 29, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 30, '2011-05-31 18:00:00.000', '2011-06-14 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 31, '2011-09-06 11:00:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 32, '2011-09-06 10:40:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 33, '2011-09-06 10:50:00.000', '2011-09-20 09:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 34, '2011-08-01 00:00:00.000', '2020-12-31 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 35, '2012-01-01 00:00:00.000', '2012-02-01 00:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 36, '2012-01-01 12:11:00.000', '2022-01-01 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 37, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 38, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 39, '2009-01-01 12:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 40, '2012-10-23 15:00:00.000', '2012-11-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 41, '2013-02-06 10:00:00.000', '2013-02-19 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 42, '2009-01-01 12:00:00.000', '2013-03-19 15:30:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 43, '2009-01-01 12:00:00.000', '2013-04-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 44, '2013-06-11 10:00:00.000', '2013-06-25 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 45, '2013-07-30 09:00:00.000', '2013-07-30 11:20:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 46, '2013-08-12 12:00:00.000', '2013-08-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 47, '2013-09-18 10:00:00.000', '2013-10-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 48, '2013-12-23 10:00:00.000', '2014-01-07 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 49, '2014-01-28 10:00:00.000', '2014-02-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 50, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 51, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 52, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 53, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 54, '2014-03-25 10:00:00.000', '2014-04-08 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 55, '2014-04-29 10:00:00.000', '2014-05-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 56, '2014-05-08 10:00:00.000', '2014-05-13 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 57, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 58, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 59, '2014-09-02 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 60, '2014-11-25 10:00:00.000', '2014-12-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 61, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 62, '2015-02-16 10:00:00.000', '2015-03-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 63, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 64, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 65, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 66, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 67, '2014-04-10 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 68, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 69, '2015-10-15 08:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 70, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 71, '2015-06-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 73, '2018-02-13 10:00:00.000', '2018-02-22 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 74, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 75, '2018-05-14 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 76, '2019-05-30 10:00:00.000', '2019-06-11 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 77, '2018-07-31 12:00:00.000', '2018-08-06 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 78, '2018-09-27 10:00:00.000', '2018-10-09 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 79, '2019-05-29 10:00:00.000', '2019-05-28 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 80, '2018-11-20 10:00:00.000', '2018-11-27 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 81, '2018-12-25 10:00:00.000', '2019-01-03 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 82, '2019-01-30 10:00:00.000', '2019-01-30 12:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 84, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 85, '2019-04-16 10:00:00.000', '2019-04-16 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 86, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (1167, 88, '2019-06-25 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (3000, 84, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (3000, 85, '2019-04-16 10:00:00.000', '2019-04-16 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (3000, 86, '2019-05-21 10:00:00.000', '2022-01-01 10:00:00.000');
GO

INSERT INTO [dbo].[TblParmSvrSupportEvent] ([mSvrNo], [mEventID], [mBeginDate], [mEndDate]) VALUES (3000, 88, '2019-06-25 10:00:00.000', '2022-01-01 10:00:00.000');
GO

