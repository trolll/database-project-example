/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AchieveGuildList
Date                  : 2023-10-07 09:08:48
*/


INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (1, '2013-04-30 13:40:31.183', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '셀린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK1', 312, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (2, '2013-04-30 13:40:31.183', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '브랜든', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK2', 309, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (3, '2013-04-30 13:40:31.187', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '브라운', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK3', 307, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (4, '2013-04-30 13:40:31.187', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '배리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK4', 305, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (5, '2013-04-30 13:40:31.187', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '클레멘트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK5', 303, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (6, '2013-04-30 13:40:31.190', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '메트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK6', 301, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (7, '2013-04-30 13:40:31.190', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '러스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK7', 299, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (8, '2013-04-30 13:40:31.190', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '워렐', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK8', 297, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (9, '2013-04-30 13:40:31.193', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '새미', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK9', 295, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (10, '2013-04-30 13:40:31.193', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '리나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK10', 293, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (11, '2013-04-30 13:40:31.193', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '슈만', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK11', 290, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (12, '2013-04-30 13:40:31.197', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '하트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK12', 288, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (13, '2013-04-30 13:40:31.197', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '베드넷', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK13', 286, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (14, '2013-04-30 13:40:31.197', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '포포', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK14', 284, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (15, '2013-04-30 13:40:31.200', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '에바', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK15', 282, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (16, '2013-04-30 13:40:31.200', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '토드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK16', 280, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (17, '2013-04-30 13:40:31.200', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '알프레드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK17', 278, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (18, '2013-04-30 13:40:31.203', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '루터', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK18', 276, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (19, '2013-04-30 13:40:31.203', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '마쉬', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK19', 274, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (20, '2013-04-30 13:40:31.203', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '크레아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK20', 272, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (21, '2013-04-30 13:40:31.207', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '케빈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK21', 269, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (22, '2013-04-30 13:40:31.207', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '세실', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK22', 267, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (23, '2013-04-30 13:40:31.210', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '짐머맨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK23', 265, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (24, '2013-04-30 13:40:31.210', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '던컨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK24', 263, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (25, '2013-04-30 13:40:31.213', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '로버슨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK25', 261, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (26, '2013-04-30 13:40:31.217', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '버드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK26', 259, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (27, '2013-04-30 13:40:31.217', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '필립스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK27', 257, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (28, '2013-04-30 13:40:31.220', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '키르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK28', 255, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (29, '2013-04-30 13:40:31.223', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '크저브', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK29', 253, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (30, '2013-04-30 13:40:31.223', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '모튼', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK30', 251, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (31, '2013-04-30 13:40:31.227', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '클라크', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK31', 248, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (32, '2013-04-30 13:40:31.227', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '녹스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK32', 246, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (33, '2013-04-30 13:40:31.230', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '아르투', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK33', 244, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (34, '2013-04-30 13:40:31.230', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '레오', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK34', 242, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (35, '2013-04-30 13:40:31.230', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '세바', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK35', 240, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (36, '2013-04-30 13:40:31.230', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '게일', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK36', 238, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (37, '2013-04-30 13:40:31.233', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '피셔', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK37', 236, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (38, '2013-04-30 13:40:31.233', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '보라스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK38', 234, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (39, '2013-04-30 13:40:31.237', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '우셔', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK39', 232, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (40, '2013-04-30 13:40:31.240', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '올렌버그', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK40', 230, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (41, '2013-04-30 13:40:31.243', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '타샤', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK41', 227, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (42, '2013-04-30 13:40:31.253', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '카잔타루', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK42', 225, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (43, '2013-04-30 13:40:31.253', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '로이터', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK43', 223, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (44, '2013-04-30 13:40:31.260', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '페티오', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK44', 221, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (45, '2013-04-30 13:40:31.260', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '라만', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK45', 219, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (46, '2013-04-30 13:40:31.270', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '스카라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK46', 217, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (47, '2013-04-30 13:40:31.270', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '잔쿠', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK47', 215, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (48, '2013-04-30 13:40:31.273', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '로마타', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK48', 213, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (49, '2013-04-30 13:40:31.277', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '우타루', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK49', 211, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (50, '2013-04-30 13:40:31.277', 1, '푸리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1', '몰린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK1_MEMBER_RANK50', 209, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (51, '2013-04-30 13:40:31.277', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '토마', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK1', 303, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (52, '2013-04-30 13:40:31.280', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '엘렌', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK2', 301, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (53, '2013-04-30 13:40:31.280', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '존슨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK3', 299, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (54, '2013-04-30 13:40:31.280', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '카트먼', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK4', 297, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (55, '2013-04-30 13:40:31.283', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '모드마', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK5', 295, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (56, '2013-04-30 13:40:31.283', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '길모어', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK6', 293, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (57, '2013-04-30 13:40:31.283', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '마리아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK7', 291, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (58, '2013-04-30 13:40:31.287', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '에밀리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK8', 289, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (59, '2013-04-30 13:40:31.287', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '세나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK9', 287, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (60, '2013-04-30 13:40:31.290', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '러츠', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK10', 285, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (61, '2013-04-30 13:40:31.290', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '에드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK11', 282, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (62, '2013-04-30 13:40:31.290', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '롭', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK12', 280, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (63, '2013-04-30 13:40:31.293', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '엘윈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK13', 278, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (64, '2013-04-30 13:40:31.293', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '로딘', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK14', 276, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (65, '2013-04-30 13:40:31.297', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '오웬스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK15', 274, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (66, '2013-04-30 13:40:31.300', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '에이츠', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK16', 272, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (67, '2013-04-30 13:40:31.300', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '피에르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK17', 270, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (68, '2013-04-30 13:40:31.300', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '스판', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK18', 268, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (69, '2013-04-30 13:40:31.303', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '시에라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK19', 266, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (70, '2013-04-30 13:40:31.303', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '셀비', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK20', 264, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (71, '2013-04-30 13:40:31.307', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '시어스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK21', 262, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (72, '2013-04-30 13:40:31.307', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '랩코', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK22', 260, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (73, '2013-04-30 13:40:31.310', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '리드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK23', 258, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (74, '2013-04-30 13:40:31.310', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '포그', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK24', 256, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (75, '2013-04-30 13:40:31.310', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '퀸튼', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK25', 254, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (76, '2013-04-30 13:40:31.310', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '레드키', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK26', 252, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (77, '2013-04-30 13:40:31.313', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '레스포', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK27', 250, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (78, '2013-04-30 13:40:31.320', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '제레미', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK28', 248, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (79, '2013-04-30 13:40:31.320', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '밀스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK29', 246, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (80, '2013-04-30 13:40:31.327', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '모리아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK30', 244, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (81, '2013-04-30 13:40:31.330', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '럭셔스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK31', 241, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (82, '2013-04-30 13:40:31.330', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '올가', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK32', 239, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (83, '2013-04-30 13:40:31.330', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '뮬러', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK33', 237, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (84, '2013-04-30 13:40:31.337', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '넬슨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK34', 235, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (85, '2013-04-30 13:40:31.340', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '코넬리아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK35', 233, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (86, '2013-04-30 13:40:31.340', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '카일', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK36', 231, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (87, '2013-04-30 13:40:31.343', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '켈스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK37', 229, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (88, '2013-04-30 13:40:31.343', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '빅스톤', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK38', 227, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (89, '2013-04-30 13:40:31.347', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '도노반', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK39', 225, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (90, '2013-04-30 13:40:31.350', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '크리에', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK40', 223, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (91, '2013-04-30 13:40:31.357', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '아로', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK41', 221, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (92, '2013-04-30 13:40:31.363', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '코난', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK42', 219, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (93, '2013-04-30 13:40:31.363', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '제피', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK43', 217, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (94, '2013-04-30 13:40:31.363', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '키라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK44', 215, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (95, '2013-04-30 13:40:31.367', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '라이코', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK45', 213, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (96, '2013-04-30 13:40:31.373', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '죠쉬', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK46', 211, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (97, '2013-04-30 13:40:31.387', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '보리시', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK47', 209, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (98, '2013-04-30 13:40:31.387', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '폰챠카', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK48', 207, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (99, '2013-04-30 13:40:31.387', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '사비나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK49', 205, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (100, '2013-04-30 13:40:31.390', 2, '블랙랜드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2', '샤라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK2_MEMBER_RANK50', 203, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (101, '2013-04-30 13:40:31.390', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '코덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK1', 295, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (102, '2013-04-30 13:40:31.390', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '제니', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK2', 293, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (103, '2013-04-30 13:40:31.393', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '코로니', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK3', 291, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (104, '2013-04-30 13:40:31.393', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '보시', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK4', 289, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (105, '2013-04-30 13:40:31.393', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '컬레이', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK5', 287, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (106, '2013-04-30 13:40:31.397', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '맥스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK6', 285, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (107, '2013-04-30 13:40:31.397', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '해머', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK7', 283, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (108, '2013-04-30 13:40:31.400', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '콴치', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK8', 281, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (109, '2013-04-30 13:40:31.400', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '보로샤', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK9', 279, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (110, '2013-04-30 13:40:31.400', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '보린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK10', 277, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (111, '2013-04-30 13:40:31.400', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '코라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK11', 275, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (112, '2013-04-30 13:40:31.403', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '브루노', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK12', 273, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (113, '2013-04-30 13:40:31.407', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '오튼', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK13', 271, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (114, '2013-04-30 13:40:31.407', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '호건', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK14', 269, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (115, '2013-04-30 13:40:31.407', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '캐미', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK15', 267, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (116, '2013-04-30 13:40:31.410', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '레미', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK16', 265, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (117, '2013-04-30 13:40:31.410', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '주다스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK17', 263, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (118, '2013-04-30 13:40:31.410', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '루니', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK18', 261, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (119, '2013-04-30 13:40:31.413', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '지코', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK19', 259, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (120, '2013-04-30 13:40:31.413', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '잠보', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK20', 257, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (121, '2013-04-30 13:40:31.413', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '코코', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK21', 255, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (122, '2013-04-30 13:40:31.417', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '루커', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK22', 253, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (123, '2013-04-30 13:40:31.417', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '울버린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK23', 251, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (124, '2013-04-30 13:40:31.417', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '트리거잭', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK24', 249, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (125, '2013-04-30 13:40:31.420', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '스탈린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK25', 247, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (126, '2013-04-30 13:40:31.420', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '실비아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK26', 245, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (127, '2013-04-30 13:40:31.420', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '민트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK27', 243, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (128, '2013-04-30 13:40:31.420', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '카멕', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK28', 241, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (129, '2013-04-30 13:40:31.423', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '더들리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK29', 239, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (130, '2013-04-30 13:40:31.423', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '크리스티나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK30', 237, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (131, '2013-04-30 13:40:31.423', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '크루거', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK31', 235, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (132, '2013-04-30 13:40:31.427', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '사티', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK32', 233, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (133, '2013-04-30 13:40:31.427', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '루나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK33', 231, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (134, '2013-04-30 13:40:31.427', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '켈린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK34', 229, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (135, '2013-04-30 13:40:31.430', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '에이엔', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK35', 227, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (136, '2013-04-30 13:40:31.430', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '릭서스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK36', 225, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (137, '2013-04-30 13:40:31.430', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '바투스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK37', 223, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (138, '2013-04-30 13:40:31.430', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '에아르윈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK38', 221, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (139, '2013-04-30 13:40:31.433', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '렌드에윈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK39', 219, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (140, '2013-04-30 13:40:31.433', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '케러백', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK40', 217, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (141, '2013-04-30 13:40:31.433', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '론젤', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK41', 215, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (142, '2013-04-30 13:40:31.437', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '켄트힐', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK42', 213, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (143, '2013-04-30 13:40:31.437', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '아노스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK43', 211, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (144, '2013-04-30 13:40:31.437', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '테츠', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK44', 209, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (145, '2013-04-30 13:40:31.440', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '시그네스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK45', 207, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (146, '2013-04-30 13:40:31.440', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '카사노아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK46', 205, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (147, '2013-04-30 13:40:31.443', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '테라노아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK7', 203, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (148, '2013-04-30 13:40:31.443', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '바르누이', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK48', 201, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (149, '2013-04-30 13:40:31.443', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '카르노스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK49', 199, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (150, '2013-04-30 13:40:31.447', 3, '바이런', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3', '엘라노미아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK3_MEMBER_RANK50', 197, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (151, '2013-04-30 13:40:31.447', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '코로', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK1', 286, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (152, '2013-04-30 13:40:31.447', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '로트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK2', 284, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (153, '2013-04-30 13:40:31.450', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '잼머', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK3', 282, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (154, '2013-04-30 13:40:31.450', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '로이', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK4', 280, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (155, '2013-04-30 13:40:31.450', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '루우', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK5', 278, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (156, '2013-04-30 13:40:31.453', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '카인', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK6', 276, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (157, '2013-04-30 13:40:31.453', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '파스파', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK7', 274, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (158, '2013-04-30 13:40:31.453', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '사루나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK8', 272, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (159, '2013-04-30 13:40:31.457', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '칸다루', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK9', 270, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (160, '2013-04-30 13:40:31.457', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '라마', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK10', 268, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (161, '2013-04-30 13:40:31.457', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '수잔', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK11', 267, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (162, '2013-04-30 13:40:31.460', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '다루', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK12', 265, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (163, '2013-04-30 13:40:31.460', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '빌로', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK13', 263, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (164, '2013-04-30 13:40:31.460', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '코이', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK14', 261, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (165, '2013-04-30 13:40:31.463', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '세키', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK15', 259, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (166, '2013-04-30 13:40:31.463', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '기드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK16', 257, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (167, '2013-04-30 13:40:31.463', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '찰시', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK17', 255, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (168, '2013-04-30 13:40:31.467', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '앨버스트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK18', 253, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (169, '2013-04-30 13:40:31.467', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '애비나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK19', 251, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (170, '2013-04-30 13:40:31.467', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '알바트론', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK20', 249, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (171, '2013-04-30 13:40:31.470', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '록센들', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK21', 247, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (172, '2013-04-30 13:40:31.470', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '앵글란드', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK22', 245, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (173, '2013-04-30 13:40:31.470', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '에델코', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK23', 243, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (174, '2013-04-30 13:40:31.470', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '레인다스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK24', 241, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (175, '2013-04-30 13:40:31.473', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '하르누보', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK25', 239, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (176, '2013-04-30 13:40:31.473', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '라곤다스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK26', 237, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (177, '2013-04-30 13:40:31.477', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '케르빔', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK27', 235, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (178, '2013-04-30 13:40:31.477', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '나르시스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK28', 233, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (179, '2013-04-30 13:40:31.477', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '에르스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK29', 231, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (180, '2013-04-30 13:40:31.480', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '마르스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK30', 229, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (181, '2013-04-30 13:40:31.480', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '하르네스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK31', 228, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (182, '2013-04-30 13:40:31.480', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '우르네', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK32', 226, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (183, '2013-04-30 13:40:31.480', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '바네슈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK33', 224, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (184, '2013-04-30 13:40:31.483', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '가데사', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK34', 222, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (185, '2013-04-30 13:40:31.483', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '세라핌', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK35', 220, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (186, '2013-04-30 13:40:31.487', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '스트라토스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK36', 218, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (187, '2013-04-30 13:40:31.487', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '바리오스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK37', 216, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (188, '2013-04-30 13:40:31.487', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '안티오스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK38', 214, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (189, '2013-04-30 13:40:31.490', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '세이버투스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK39', 212, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (190, '2013-04-30 13:40:31.490', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '딥스트로크', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK40', 210, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (191, '2013-04-30 13:40:31.490', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '지스파트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK41', 208, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (192, '2013-04-30 13:40:31.490', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '크레타리스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK42', 206, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (193, '2013-04-30 13:40:31.493', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '다이젠가', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK43', 204, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (194, '2013-04-30 13:40:31.493', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '다르투스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK44', 202, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (195, '2013-04-30 13:40:31.497', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '시난주', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK45', 200, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (196, '2013-04-30 13:40:31.497', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '소레스티안', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK46', 198, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (197, '2013-04-30 13:40:31.497', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '바르쿠쉬', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK47', 196, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (198, '2013-04-30 13:40:31.500', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '바이오넷', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK48', 194, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (199, '2013-04-30 13:40:31.500', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '다이모스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK49', 192, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (200, '2013-04-30 13:40:31.500', 4, '로덴', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4', '체르노스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK4_MEMBER_RANK50', 191, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (201, '2013-04-30 13:40:31.500', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '칼', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK1', 278, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (202, '2013-04-30 13:40:31.503', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '엘기온', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK2', 276, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (203, '2013-04-30 13:40:31.503', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '헬리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK3', 274, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (204, '2013-04-30 13:40:31.503', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '가이라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK4', 272, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (205, '2013-04-30 13:40:31.507', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '하르만', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK5', 270, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (206, '2013-04-30 13:40:31.507', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '부르만', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK6', 268, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (207, '2013-04-30 13:40:31.510', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '아르간', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK7', 266, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (208, '2013-04-30 13:40:31.510', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '라비로즈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK8', 264, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (209, '2013-04-30 13:40:31.510', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '알베르크', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK9', 262, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (210, '2013-04-30 13:40:31.513', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '엘리온느', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK10', 260, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (211, '2013-04-30 13:40:31.513', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '에비앙', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK11', 259, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (212, '2013-04-30 13:40:31.517', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '라쟈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK12', 257, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (213, '2013-04-30 13:40:31.517', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '할바스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK13', 255, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (214, '2013-04-30 13:40:31.517', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '아리엘', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK14', 253, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (215, '2013-04-30 13:40:31.520', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '엘미온', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK15', 251, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (216, '2013-04-30 13:40:31.520', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '리즈', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK16', 249, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (217, '2013-04-30 13:40:31.520', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '파미스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK17', 247, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (218, '2013-04-30 13:40:31.523', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '루디', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK18', 245, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (219, '2013-04-30 13:40:31.523', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '루젤', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK19', 243, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (220, '2013-04-30 13:40:31.523', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '라비앙', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK20', 241, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (221, '2013-04-30 13:40:31.527', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '벨라', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK21', 240, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (222, '2013-04-30 13:40:31.527', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '안델슨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK22', 238, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (223, '2013-04-30 13:40:31.527', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '델포르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK23', 236, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (224, '2013-04-30 13:40:31.530', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '헤이든', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK24', 234, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (225, '2013-04-30 13:40:31.530', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '단테스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK25', 232, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (226, '2013-04-30 13:40:31.530', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '델리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK26', 230, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (227, '2013-04-30 13:40:31.530', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '벨리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK27', 228, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (228, '2013-04-30 13:40:31.533', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '타렐', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK28', 226, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (229, '2013-04-30 13:40:31.533', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '일리언', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK29', 224, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (230, '2013-04-30 13:40:31.533', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '다오', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK30', 222, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (231, '2013-04-30 13:40:31.537', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '에일린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK31', 221, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (232, '2013-04-30 13:40:31.537', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '베이슨', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK32', 219, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (233, '2013-04-30 13:40:31.537', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '루지나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK33', 217, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (234, '2013-04-30 13:40:31.540', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '마이애나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK34', 215, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (235, '2013-04-30 13:40:31.540', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '그웬델시아', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK35', 213, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (236, '2013-04-30 13:40:31.540', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '벨리사', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK36', 211, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (237, '2013-04-30 13:40:31.543', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '오빌리언', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK37', 209, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (238, '2013-04-30 13:40:31.543', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '케일', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK38', 207, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (239, '2013-04-30 13:40:31.543', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '아그네스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK39', 205, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (240, '2013-04-30 13:40:31.547', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '엔지', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK40', 203, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (241, '2013-04-30 13:40:31.547', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '켄트', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK41', 202, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (242, '2013-04-30 13:40:31.547', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '램시', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK42', 200, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (243, '2013-04-30 13:40:31.550', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '콜린', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK43', 198, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (244, '2013-04-30 13:40:31.550', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '돌리', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK44', 196, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (245, '2013-04-30 13:40:31.550', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '엠마', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK45', 194, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (246, '2013-04-30 13:40:31.550', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '지나', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK46', 192, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (247, '2013-04-30 13:40:31.553', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '린다', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK47', 190, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (248, '2013-04-30 13:40:31.553', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '나니', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK48', 188, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (249, '2013-04-30 13:40:31.553', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '알렉스', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK49', 186, 0);
GO

INSERT INTO [dbo].[DT_AchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mGuildNamekey], [mMemberName], [mMemberNameKey], [mEquipPoint], [mUpdateIndex]) VALUES (250, '2013-04-30 13:40:31.557', 5, '엘테르', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5', '스티브', 'RCLEINT_MSG_ACHIEVE_GUILD_RANK5_MEMBER_RANK50', 185, 0);
GO

