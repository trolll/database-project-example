/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblServantAbilityDivisor
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (0, 4.5, 5.0, 5.5);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (1, 3.4000000953674316, 5.0, 5.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (2, 4.5, 5.0, 3.200000047683716);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (3, 4.199999809265137, 4.199999809265137, 4.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (4, 4.5, 5.0, 4.5);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (5, 4.5, 4.5, 5.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (6, 4.800000190734863, 4.5, 4.099999904632568);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (7, 3.4000000953674316, 5.0, 5.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (8, 3.4000000953674316, 5.0, 5.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (9, 4.5, 5.0, 3.200000047683716);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (10, 4.5, 5.0, 3.200000047683716);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (11, 4.199999809265137, 4.199999809265137, 4.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (12, 4.199999809265137, 4.199999809265137, 4.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (13, 4.5, 5.0, 4.5);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (14, 4.5, 5.0, 4.5);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (15, 4.5, 4.5, 5.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (16, 4.5, 4.5, 5.0);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (17, 4.800000190734863, 4.5, 4.099999904632568);
GO

INSERT INTO [dbo].[TblServantAbilityDivisor] ([SType], [SStrength], [SDexterity], [SInteligence]) VALUES (18, 4.800000190734863, 4.5, 4.099999904632568);
GO

