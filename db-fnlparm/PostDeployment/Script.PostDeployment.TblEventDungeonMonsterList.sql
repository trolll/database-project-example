/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblEventDungeonMonsterList
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (28, 2071, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (28, 2072, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (28, 2073, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (28, 2074, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (28, 2075, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (36, 2447, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (36, 2448, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (36, 2449, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (36, 2450, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (36, 2451, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (55, 2140, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (55, 2141, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (55, 2142, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (55, 2143, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (55, 2144, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (62, 2076, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (62, 2077, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (62, 2078, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (62, 2079, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (62, 2080, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (70, 2145, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (70, 2146, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (70, 2147, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (70, 2148, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (70, 2149, 2, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2709, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2710, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2711, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2712, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2713, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2714, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2715, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2716, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2717, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2718, 0, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2719, 0, 1);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2720, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2721, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2722, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2723, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2724, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2725, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2726, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2727, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2728, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2729, 1, 0);
GO

INSERT INTO [dbo].[TblEventDungeonMonsterList] ([mDungeonPlace], [mMID], [mCamp], [mIsBoss]) VALUES (306, 2730, 1, 1);
GO

