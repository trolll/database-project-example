/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_SkillTree
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (1, '公会基本技能树', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (2, '公会营地城池技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (3, '公会特殊技能树', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (4, '职业通用技能树', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (5, '骑士破坏技能树', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (6, '骑士防御技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (7, '游侠射击技能树', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (8, '游侠生存技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (9, '精灵光明技能树', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (10, '精灵黑暗技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (11, '刺客战斗技能树', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (12, '刺客突袭技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (13, '召唤师灵魂技能树', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (14, '召唤师图腾技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (15, 'HP技能树
', 5);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (16, 'MP技能树
', 5);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (17, '其他技能树
', 5);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (18, 'HP技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (19, 'HP恢复技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (20, 'MP技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (21, 'MP恢复技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (22, '负重技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (23, '变身时间技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (24, 'HPMP技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (25, 'HPMP恢复技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (26, 'HP负重技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (27, 'HP恢复变身时间技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (28, 'MP负重技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (29, 'MP恢复变身时间技能树
', 4);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (30, 'HP技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (31, 'HP恢复技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (32, 'MP技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (33, 'MP恢复技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (34, '负重技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (35, '变身时间技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (36, 'HPMP技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (37, 'HPMP恢复技能树', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (38, 'HP负重技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (39, 'HP恢复变身时间技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (40, 'MP负重技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (41, 'MP恢复变身时间技能树
', 3);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (42, 'HP技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (43, 'HP恢复技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (44, 'MP技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (45, 'MP恢复技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (46, '负重技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (47, '变身时间技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (48, 'HPMP技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (49, 'HPMP恢复技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (50, 'HP负重技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (51, 'HP恢复变身时间技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (52, 'MP负重技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (53, 'MP恢复变身时间技能树
', 2);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (54, 'HP技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (55, 'HP恢复技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (56, 'MP技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (57, 'MP恢复技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (58, '负重技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (59, '变身时间技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (60, 'HPMP技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (61, 'HPMP恢复技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (62, 'HP负重技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (63, 'HP恢复变身时间技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (64, 'MP负重技能树', 1);
GO

INSERT INTO [dbo].[TP_SkillTree] ([mSTID], [mName], [mDestroyOrder]) VALUES (65, 'MP恢复变身时间技能树', 1);
GO

