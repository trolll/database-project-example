/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblTransformAbilityByLevel
Date                  : 2023-10-07 09:07:22
*/


INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (0, 50, 30, 300, 700, 1000, 380, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (40, 60, 40, 400, 685, 990, 390, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (54, 75, 45, 500, 675, 975, 400, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (57, 100, 60, 750, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (60, 150, 90, 1250, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (63, 200, 120, 1500, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (66, 250, 150, 1750, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (69, 300, 180, 2000, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (72, 350, 210, 2250, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (75, 400, 240, 2500, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (0, 40, 20, 300, 800, 1100, 380, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (45, 50, 30, 300, 750, 1050, 350, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (48, 50, 30, 300, 700, 1000, 380, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (51, 75, 45, 500, 675, 975, 400, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (54, 100, 60, 750, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (57, 150, 90, 1250, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (60, 200, 120, 1500, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (63, 250, 150, 1750, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (66, 300, 180, 2000, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (69, 350, 210, 2250, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (72, 400, 240, 2500, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (75, 450, 270, 2750, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (78, 500, 300, 3000, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (81, 550, 330, 3250, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (84, 600, 360, 3500, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (87, 650, 390, 3750, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (90, 700, 420, 4000, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (93, 750, 450, 4250, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (96, 800, 480, 4500, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (99, 850, 510, 4750, 650, 950, 415, 2);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (78, 450, 270, 2750, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (81, 500, 300, 3000, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (84, 550, 330, 3250, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (87, 600, 360, 3500, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (0, 50, 30, 300, 700, 1000, 380, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (45, 75, 45, 500, 675, 975, 400, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (48, 100, 60, 750, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (51, 150, 90, 1250, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (54, 200, 120, 1500, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (57, 250, 150, 1750, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (60, 300, 180, 2000, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (63, 350, 210, 2250, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (66, 400, 240, 2500, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (69, 450, 270, 2750, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (72, 500, 300, 3000, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (75, 550, 330, 3250, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (78, 600, 360, 3500, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (81, 650, 390, 3750, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (84, 700, 420, 4000, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (87, 750, 450, 4250, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (90, 800, 480, 4500, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (93, 850, 510, 4750, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (96, 900, 540, 5000, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (99, 950, 570, 5250, 650, 950, 415, 3);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (90, 650, 390, 3750, 650, 950, 415, 0);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (0, 100, 60, 750, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (45, 150, 90, 1250, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (48, 200, 120, 1500, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (51, 250, 150, 1750, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (54, 300, 180, 2000, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (57, 350, 210, 2250, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (60, 400, 240, 2500, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (63, 450, 270, 2750, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (66, 500, 300, 3000, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (69, 550, 330, 3250, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (72, 600, 360, 3500, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (75, 650, 390, 3750, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (78, 700, 420, 4000, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (81, 750, 450, 4250, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (84, 800, 480, 4500, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (87, 850, 510, 4750, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (90, 900, 540, 5000, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (93, 950, 570, 5250, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (96, 1000, 600, 5500, 650, 950, 415, 4);
GO

INSERT INTO [dbo].[TblTransformAbilityByLevel] ([mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate], [mType]) VALUES (99, 1050, 630, 5750, 650, 950, 415, 4);
GO

