/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_ModuleType
Date                  : 2023-10-07 09:08:47
*/


INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (45, '은신모듈', '은신', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (46, '은신감지모듈', '은신과 함께 적용되면 은신무효', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (47, '1회용 추가데미지모듈 (절대값)', '한번만 적용되는 추가데미지 절대값 적용', '1최용 추가데미지 절대수치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (48, '1회용 추가데미지모듈 (상대값)', '한번만 적용되는 추가데미지 상대값 적용', '1최용 추가데미지 상대수치 (DD의 N/100)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (49, '무적모듈', '무적', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (50, '기절모듈', '기절', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (51, '마법캐스팅지연모듈', '아이템 사용 타입이 Self 가 아닌 책, 막대기의 스킬 사용 시 적용', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (1, '회복모듈', '', '회복량', '랜덤증감량(100%기준)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (2, '만복도 모듈', ' ', '증감량', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (3, '공격속도 모듈', ' ', '', '공격속도 변경치', '이동속도 변경치');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (4, '이동속도 모듈', ' ', '', '공격속도 변경치', '이동속도 변경치');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (5, '힘 모듈', ' ', '', '힘 변동치', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (6, '민첩 모듈', '', '', '민첩 변동치', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (7, '지능순간 모듈', ' ', '', '지능 변동치', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (8, '귀환 모듈', '귀환', '0:일반,1:아지트,2:홀귀환', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (9, '해독 모듈', '해당 등급 이하의 독 상태이상이 해독된다', '상태이상 아이디', '치료가능상태이상레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (10, 'HP 모듈', ' ', '', '변경주기', '변동량');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (11, '벙어리모듈', '지속 시간동안 마법 사용 금지', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (12, '발묶기모듈', '해당 지속시간동안 이동 불가', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (13, '마비모듈', '이동 불능 공격불능 텔레포트불가 아이템사용불가', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (14, 'HIT 모듈', ' ', '근접', '원거리 ', '마법');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (15, 'DV 모듈', '50Lv이상HDV', 'DDV변동치', 'MDV변동치', 'RDV변동치');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (16, 'DD 모듈', 'D', '근접', '원거리 ', '마법');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (17, 'PV 모듈', '50Lv이상HPV', 'DPV변동치', 'MPV변동치', 'RPV변동치');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (18, '부활모듈', '부활모듈', '경험치회복%', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (19, '푸드포션스크롤파괴모듈', '인벤에 있는 포션과 스크롤이 파괴됩니다', '성공시 파괴 가능한 %', '성공시 파괴가능한 아이템레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (20, '변신모듈', '몬스터로 변신한다', '변신리스트 번호', '변신리스트 표시 유무', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (21, '몬스터소환모듈', '랜덤한 몬스터가 소환된다', '소환 그룹 지정', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (22, '불 저항 강화', '강화', '', '레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (23, '냉기 저항 강화', '냉기저항을 가진다', '', '저항 레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (24, '전격 저항 강화', '', '', '저항레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (25, '혼란 저항강화', '레벨값', '레벨값', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (26, '산 저항 강화', '', '', '저항레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (27, 'HP 리젠양 UP 모듈', ' 90lv이상최대치증가', '변동량', '0:가벼움,1:무거움', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (28, 'MP 리젠양 UP 모듈', ' ', '변동량', '0:가벼움,1:무거움', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (29, 'MP즉시증감', 'MP를 즉시 증감시킴', 'MP증감량', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (30, '무게증가', '', '증가량', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (31, '크리티컬 공격', '현재 공격중인 상대에게 크리티컬 공격', '하드히트용', '에임드샷용', '파워샷용');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (32, '아이템 상태 모듈', ' 축복,저주 변경', '0:주저, 1:축복', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (33, '투명감지모듈', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (34, '확인모듈', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (35, '텔레포트모듈', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (36, '저주풀기', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (37, '투명', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (38, '텔레포트조종', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (39, '변신조종모듈', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (40, '푸드크리에이션', '마법으로 아이템을 만든다.', '아이템번호', '아이템갯수', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (41, '프로텍트모듈', '', '종족값', '프로텍트레벨', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (42, '디스펠매직', '지속시간있는모든 상태이상 무효화', '첫번째 우선순위상태이상 타입', '두번째 우선순위상태이상 타입', '세번째 우선순위상태이상 타입');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (43, '탈거', '탈거모듈 a가 스피드', 'a가 스피드', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (44, '경험치상승', '경험치 상승', '상승량', 'MAX레벨제한', '없음');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (52, '크리티컬 확률', '크리티컬 확률을 변동', '크리티컬 확률 변경 수치(-200 ~ 200)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (53, '자살모듈', '자살, 몬스터만 사용해야 함', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (54, '적 리스트 초기화모듈', '적 리스트 초기화, 몬스터만 사용해야 함', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (55, '익스펠', '몬스터가 PC를 시야범위 밖으로 강제 텔레포트시킴', '체크할 범위', '밀어낼 범위 (최소)', '밀어낼 범위 (최대)');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (56, '스폰스팟 텔레포트', '몬스터가 스폰스팟 간에 텔레포트', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (57, '비공식 프로그램 사용자 모듈', '공격력,방어력,모든 명중률 -10, 무게 -5000', '공격력, 방어력 -10', '모든 명중률 -10', '무게 -5000');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (58, '사냥시 경험치 증가 모듈', '사냥시 경험치 증가', '증가량%', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (59, '사냥시 아이템 드랍 증가 모듈', '사냥시 아이템 드랍 증가 모듈', '증가량%', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (60, '엔듀랜스', '자신의 최대 체력치가 상승한다', '최대 체력치 상승폭(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (61, '아머 브레이크(대상)', '상대의 DV/PV를 확률로 감소 시킨다.', 'DV/PV 변동량(50%)', 'DV/PV 변동량(30%)', 'DV/PV 변동량(30%)');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (62, '레비테이션', '이동속도와 공격속도가 소폭 상승한다.', NULL, '이동속도 증가량', '공격속도 증가량');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (63, '마나 콘덴싱', '자신의 MP를 마정석으로 응축시킨다.', '마정석ID', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (64, '모탈 블로우', '타켓에게 한방 찔러 큰 데미지를 입힌다.', '일반공격의 몇배의 데미지(%)', '최대 공격 데미지', NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (65, '파워워드 러스트', '장비를 장착/해제하지 못한다 / 스탯 감소(힘/민첩/지능)', '스탯 변동치', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (66, '소환수 소환', '소환수를 소환한다.', '소환수 타입(1:폭탄병,2:만드라고라, 3:루비그,4:토템)', '소환수 MID', '지속시간(초)');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (67, '소환수 공격', '몬스터가 광역으로 공격을 한다.', '', '공격범위', '공격 대상자 수');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (68, '절대 데미지 공격', '절대값(%) 데미지로 공격한다.', '절대값(%)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (69, '미티어 스웜', '상태이상을 %로 선택해서 건다.', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (70, '귀환(detach)', '귀환타입(0,1,2,3,4)', '0:일반,1:아지트,2:홀귀환,3:초청장(하우스),4:초청장(홀)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (71, 'HP 최대치 증가', 'HP 최대치가 증가한다', '증가량(절대값)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (79, '반격', '피격시 공격자에게 상태이상을 건다', '상태이상AID', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (80, '공격불가', '일반 공격을 할 수 없다.', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (89, '서몬 오리에드 토템', '토템에게 공격 데미지를 넘긴다.', '토템에게 넘길 데미지(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (90, '빙의 시도', '다른 몬스터로 빙의를 시도 HP 20% 증가', 'HP 증가량(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (72, 'MP 최대치 증가', 'MP 최대치가 증가한다', '증가량(절대값)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (73, '매직디스펠', '매직프로텍프 효과를 제거한다', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (74, '캔슬스크롤', '해당등급이하의 상태이상을 해제한다.', '해제등급', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (75, '상태이상 걸기', '모듈 적용시점에 상태이상을 건다', '상태이상 AID', '0:피격자, 1:시전자, 2: 장비형', '패시브 영향을 받는 스킬 SID');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (76, '스킬용 순간데미지', '순간데미지(절대값)을 준다.반복가능', '데미지', '0:내가 단다, 1:공격자가 단다', '반복주기');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (77, '데미지 감소', '받는 데미지를 감소시킨다.', '감소량(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (78, '스킬사용', '모듈 적용시점에 스킬을 사용한다.', '스킬ID(SID)', '0:자신에게, 1:공격자에게건다', '0:공격자/방어자가 다르다. 1:공격자/방어자가 같다.');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (81, '리플렉션 오오라', '특정상태이상에 반응한다', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (82, '데미지 회피', '일반 공격에 대해서 무조건 회피한다.', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (83, '감소대미지 회복', '데미지 감소모듈에서 감소된 데미지의 일정량을 HP 로 회복한다.', '회복량(감소된 데미지의%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (84, '스킬사용불가 모듈', '스킬을 사용할 수 없다.<셀프형은 가능>', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (85, '데미지 반사 모듈', '자신이 입은 데미지의 일정량을 공격자에게 돌려준다.(일반,스킬)', '반사될 데미지(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (86, '매직프로텍트', '매직프로텍트', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (87, '명예포인트 보호', '명예포인트를 보호한다', '보호될양(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (88, '명예포인트 증폭', '명예포인트 획득양을 증가시킨다', '증가될양(%)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (91, '몬스터 부활 모듈', '자신 또는 타NPC를 부활시킬수 있다', '자기 부활인지 여부(0:타인, 1:자신)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (92, '투명망토감지모듈', '투명망토만을 감지', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (93, '운영자 투명 모듈', '운영자 전용 투명 모듈', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (94, '선택적 상태이상 해제', '선택된 상태이상을 해제', '해제할 상태이상타입1', '해제할 상태이상타입2', '해제할 상태이상타입3');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (95, '빙의 성공', '빙의 성공에 의한 모듈', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (96, '몬스터 부활 성공모듈', '부활된 몬스터에게 적용되는 모듈', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (97, '몬스터 소환', '지정된 몬스터 소환', '몬스터 ID', '몬스터 ID', '몬스터 ID');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (98, '방문NPC활성화', '방문 NPC 활성화 성공 모듈', NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (99, '성향치 변동 모듈', '성향치를 가감 시킨다', '변동량(-30000~+30000)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (100, '포션 회복량 증가 모듈', '포션 회복량을 증가시킨다', '증가량(절대값)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (101, '스킬팩 생성 모듈', '스킬팩을 생성한다', '스킬팩ID', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (102, '스킬 파라미터 변경 모듈', '스킬 사용시 효과를 변화시킨다', '스킬번호', '변경타입', '변경값');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (103, 'Pc능력치 추가 모듈(%)', 'PC의 능력치 src에 비례하여 dst를 증가시킨다', 'src타입', 'dst타입', '변경값');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (104, 'Pc능력치 이동 모듈(value)', 'PC의 능력치의 일부를 src에서 dst로 이동시킨다', 'src타입', 'dst타입', '변경값');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (105, '체인 스킬 모듈', '체인 방식으로 스킬이 연결된다.', '시전자 사용 스킬ID', '피격자 사용 스킬ID(반사)', NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (106, 'DDV 모듈(+,-)', '50Lv이상Hidden', 'DDV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (107, 'MDV 모듈(+,-)', '50Lv이상Hidden', 'MDV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (108, 'RDV 모듈(+,-)', '50Lv이상Hidden', 'RDV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (109, 'DPV 모듈(+,-)', '50Lv이상Hidden', 'DPV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (110, 'MPV 모듈(+,-)', '50Lv이상Hidden', 'MPV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (111, 'RPV 모듈(+,-)', '50Lv이상Hidden', 'RPV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (112, '길드원 소환 모듈', '길드원 소환 모듈', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (113, '폭발 모듈', '폭발 모듈', '반경(cm단위)', '최대타겟개수', '상태이상 번호');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (114, '추가 데미지 모듈', '데미지의 %만큼 더 준다.', '데미지(%)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (115, '장비형 상태이상 모듈', '장비형 상태이상을 적용한다.', '장비형 상태이상 ID', '장비형 타입(모듈타입참조:서버)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (116, '트랩 모듈', '트랩을 생성한다.', '생성할 트랩의 MonId', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (117, '소멸 모듈', '소멸', '추가상태이상No1', '추가상태이상No2', '추가상태이상No3');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (118, '마나컨버전 모듈', '마나컨버전시작', '시간 변수', '상태이상번호', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (119, '마나컨버전HP모듈', '마나컨버전HP', 'HP 변수', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (120, '마나컨버전DDD모듈', '마나컨버전DDD', 'DDD변수', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (121, '마나번 모듈', '마나번', 'MP변수', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (122, '(길드)휴식경험치 모듈', '휴식 경험치 배율 세팅', '배수', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (123, '(길드)공명의 증가 모듈', '공명 생성량을 증가시킨다', '증가율(10000이 100퍼센트)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (124, '(길드)스킬트리 공명의 포상', '스킬트리용 공명의 포상을 생성할 수 있다', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (125, '크리티컬시 대미지 증가', '크리티컬시 대미지 증가', '증가값', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (126, '모든 변신의 지속시간 증가', '모든 변신의 지속시간 증가', '증가값(초단위)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (127, '경험치상승모듈(GkillTree)', '경험치상승모듈(GkillTree)', '상승량', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (128, '스킬트리 초기화모듈', '스킬트리를 초기화한다', '트리타입(1:PC스킬트리,2:길드스킬트리)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (129, '성향치 보호모듈', '성향치 하락 보호', '하락량감소비율(퍼센트값)', '증가량증가비율(퍼센트값)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (144, '슬레인 추가수치 모듈', '슬레인에 대한 추가 슬레인수치 제공.', '슬레인 아이디', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (145, '프로텍트 추가수치 모듈', '프로텍트에 대한 추가 슬레인수치 제공.', '프로텍트 아이디', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (151, '필요MP 증감모듈', '스킬사용시필요한MP 증감', '증감수치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (152, '스킬성공률모듈', '스킬적중률증감', '증감수치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (153, '크리티컬시 추가증가 모듈', '크리티컬시 크리티컬 추가 증가', '크리티컬 증감 상태이상번호', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (154, '투명망토 스킬', '투명망토 스킬', '삭제할 상태이상 타입', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (155, '주화주머니 사용 모듈', '미확인 주화를 생성한다', '미확인 주화 개수', NULL, NULL);
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (156, '논타겟스킬사용', '사용자기준 랜덤범위 내에 논타겟 스킬을 사용', '사용 스킬 번호', '사용자기준 랜덤 범위', '사용 횟수');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (157, '메테오스 스킬사용 알림', '메테오스 레이드 전용', '이벤트번호', '추가 동작 Flag', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (158, '몬스터 소환(메테오스 전용)', '메테오스 레이드 전용', '레이드 환경 타입', '소환 범위', '소환 횟수');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (159, '범위내 추가 데미지(메테오스 전용)', '범위내 추가 데미지', '데미지 범위', '데미지', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (160, '메테오스 블리자스(메테오스 전용)', '메테오스 레이드 전용', '블리자드 스킬 번호', '블리자드 스킬 사용 범위', '블리자드 지속시간(초)');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (161, '메테오스 토네이도(메테오스 전용)', '메테오스 레이드 전용', '토네이도 소환 범위', '토네이도 지속시간(초)', '소환 횟수');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (162, '근거리공격 완전회피', '근거리공격 완전회피', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (163, '메테오스 텔레포트', '메테오스 낙하공격 텔레포트', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (164, '현재 타겟에게 스킬사용', '현재 타겟에게 스킬사용(몬스터)', '스킬번호', '모듈에서 설정한 범위를 사용할지 여부', '모듈설정 스킬사용범위');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (165, '랜덤 광역 스킬사용', '랜덤 광역 스킬사용(몬스터)', '스킬번호', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (166, '아머 브레이크 - 내면의 파괴', '최소값+랜덤값', '최소값', '최대값', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (167, '모탈 블로우 - 치명적 칼날', '', '', '주기', '데미지값(%)');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (168, '파워 샷 - 마력 강화', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (169, '서번트 친밀도 변경', '서번트 친밀도 변경', '변동량', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (170, '둘중에 하나 지정된 몬스터 소환', '둘중에 하나 지정된 몬스터 소환', '몬스터 ID', '몬스터 ID', 'ParmA가 소환될 확률');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (174, '서번트 가변 HIT', '기본동작은 14번 모듈과 같고 파리미터는 배율로 사용된다.', '근접', '원거리', '마법');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (175, '서번트 가변 DV', '기본동작은 15번 모듈과 같고 파리미터는 배율로 사용된다.', 'DDV변동치', 'MDV변동치', 'RDV변동치');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (176, '서번트 가변 크리티컬 확률', '기본동작은 52번 모듈과 같고 파리미터는 배율로 사용된다.', '크리티컬 확률 변경 수치(-200 ~ 200)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (177, '서번트 가변 사냥시 경험치 증가', '기본동작은 58번 모듈과 같고 파리미터는 배율로 사용된다.', '증가량%', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (178, '서번트 가변 대미지 감소', '기본동작은 77번 모듈과 같고 파리미터는 배율로 사용된다.', '감소량(%)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (179, '서번트 가변 포션 회복량 증가', '기본동작은 100번 모듈과 같고 파리미터는 배율로 사용된다.', '증가량(절대값)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (180, '서번트 가변 DPV 모듈(+,-)', '기본동작은 109번 모듈과 같고 파리미터는 배율로 사용된다.', 'DPV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (181, '서번트 가변 MPV 모듈(+,-)', '기본동작은 110번 모듈과 같고 파리미터는 배율로 사용된다.', 'MPV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (182, '서번트 가변 RPV 모듈(+,-)', '기본동작은 111번 모듈과 같고 파리미터는 배율로 사용된다.', 'RPV변동치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (183, '서번트 가변 크리티컬시 대미지 증가', '기본동작은 125번 모듈과 같고 파리미터는 배율로 사용된다.', '증가값', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (184, '서번트 가변 필요MP 증감', '기본동작은 151번 모듈과 같고 파리미터는 배율로 사용된다.', '증감수치', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (146, '상태이상 전환 모듈', '상태이상 전환', '적용할 상태이상 번호', '상태이상 적용할 장비위치(장비가 아니면 -1)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (147, '쿨타임 초기화', '자신의 쿨타임을 전부 초기화한다.', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (148, '쿨타임 적용', '현재 공격 대상이 소유한 스킬팩 중의 하나에 쿨타임 강제 적용', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (149, '상태이상 복사or삭제', '상태이상을 복사해오거나 상대의 상태이상을 삭제한다.', '1:복사, 2:삭제', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (150, 'HP 도트대미지 감소', '도트 대미지가 일정량 감소한다.', '감소 수치(양수만 허용)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (171, '서번트 힘 수치 변동', '서번트 힘 수치 변동', '', '변동량(+, -)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (172, '서번트 민첩 수치 변동', '서번트 민첩 수치 변동', '', '변동량(+, -)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (173, '서번트 지능 수치 변동', '서번트 지능 수치 변동', '', '변동량(+, -)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (130, '엘리멘탈 앰프 주기적 체크', '엘리멘탈 앰프 주기적 체크', '최소시간', '최대시간(최대시간이 있는경우 최소에서 최대 랜덤)', '무기의%값');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (131, '랜덤HP 모듈', '랜덤HP 모듈', '변경주기', '최소값', '최대값');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (132, '모듈 종료시 상태이상 걸기', '모듈 종료시 상태이상 걸기', '상태이상번호', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (133, '모듈 적용시점에 상태이상 해제', '모듈 적용시점에 상태이상 해제', '상태이상번호', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (134, '블라스트 체크모듈', '블라스트 체크모듈', '사이클1 상태이상 번호', '사이클2 상태이상 번호', '사이클3 상태이상 번호');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (135, '엘리멘탈 사이클 데미지', '엘리멘탈 사이클 데미지', '사이클타입(1:파이어,2:아이스,3:라이트닝,4:애시드)', '증가율(%)', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (136, '엘리멘탈 사이클 디스펠', '엘리멘탈 사이클 디스펠(레벨만큼사라진다)', '첫번째 우선순위 상태이상 타입', '두번째 우선순위 상태이상 타입', '세번째 우선순위 상태이상 타입');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (137, '크리티컬 시 데미지 감소', '크리티컬 시 데미지 감소', '감소 수치(-100 ~ 0)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (138, '골렘 소환(바포메트 전용)', '바포메트 전용', '최대 소환 개수', '주기(간격 : 초)', '한번에 소환하는 골렘 수');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (139, '메테오 공격(랜덤 광역 공격)', '바포메트 전용', '스킬번호', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (140, '발구르기 공격(주변 광역 공격)', '바포메트 전용', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (141, '파이어 공격(주변 광역 공격)', '바포메트 전용', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (142, '상대방의 크리티컬 확률 감소', '상대방의 크리티컬 확률 감소', '감소 수치(0 ~ 100)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (143, '휴식경험치활성화모듈', '비활성휴식경험치활성화', '활성화율(10000이100%)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (185, 'HP 조건 상태이상 선택', 'HP 조건에 따라 상태이상을 선택하는 모듈', '기준HP', '상태이상1', '상태이상2');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (186, '서번트 모임 효과 확인 모듈', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (187, '서번트 모임 효과 모임장 모듈', '', '효과 적용 범위(cm)', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (188, '서번트 장착중 모듈', '상태이상 제거용', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (189, '아레나 무적', '', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (190, '암흑사제 사원 인식용 모듈', '암흑사제 사원 인식용 모듈', '', '', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (191, 'HP Percent 공격', 'HP Percent 공격', '기준 (0:전체 HP, 1:현재 HP)', '변경 percent', '');
GO

INSERT INTO [dbo].[TP_ModuleType] ([MType], [MName], [MDesc], [MAParamName], [MBParamName], [MCParamName]) VALUES (192, '서번트 합성 효과 트리거 모듈', '조건에 따라 상태이상을 걸어 준다', '적용 상태이상 ID', '적용 서번트 타입 1', '적용 서번트 타입 2');
GO

