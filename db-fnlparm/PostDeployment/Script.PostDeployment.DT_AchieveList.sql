/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AchieveList
Date                  : 2023-10-07 09:08:58
*/


INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (100, 1, '오토아님', 'RCLEINT_MSG_ACHIEVE_ID_1_NAME', '레벨 15달성', 'RCLEINT_MSG_ACHIEVE_ID_1_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (101, 1, '강력한 한방', 'RCLEINT_MSG_ACHIEVE_ID_2_NAME', '스킬 배우기', 'RCLEINT_MSG_ACHIEVE_ID_2_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (102, 1, '뚜벅이탈출', 'RCLEINT_MSG_ACHIEVE_ID_3_NAME', '드라코 타기', 'RCLEINT_MSG_ACHIEVE_ID_3_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (103, 100, '검지사냥꾼', 'RCLEINT_MSG_ACHIEVE_ID_4_NAME', '몬스터 100마리 사냥', 'RCLEINT_MSG_ACHIEVE_ID_4_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (104, 0, '바르는문서', 'RCLEINT_MSG_ACHIEVE_ID_5_NAME', '강화 성공', 'RCLEINT_MSG_ACHIEVE_ID_5_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (105, 1, '우린동업자', 'RCLEINT_MSG_ACHIEVE_ID_6_NAME', '길드 가입', 'RCLEINT_MSG_ACHIEVE_ID_6_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (106, 1, '소라게잡이', 'RCLEINT_MSG_ACHIEVE_ID_7_NAME', '발레포르 던전 방문', 'RCLEINT_MSG_ACHIEVE_ID_7_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (107, 1, '진짜 50렙', 'RCLEINT_MSG_ACHIEVE_ID_8_NAME', '50레벨 스킬 습득', 'RCLEINT_MSG_ACHIEVE_ID_8_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (108, 1, '떼쟁 사랑', 'RCLEINT_MSG_ACHIEVE_ID_9_NAME', '크루 컴뱃 참여', 'RCLEINT_MSG_ACHIEVE_ID_9_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (109, 1, '취급주의', 'RCLEINT_MSG_ACHIEVE_ID_10_NAME', '3강화 이상 성공', 'RCLEINT_MSG_ACHIEVE_ID_10_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (110, 1, '둥지관광', 'RCLEINT_MSG_ACHIEVE_ID_11_NAME', '메테오스의 레어 방문', 'RCLEINT_MSG_ACHIEVE_ID_11_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (111, 1, '변신합체', 'RCLEINT_MSG_ACHIEVE_ID_12_NAME', '변목 퀘 완료', 'RCLEINT_MSG_ACHIEVE_ID_12_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (112, 1, '고렙', 'RCLEINT_MSG_ACHIEVE_ID_13_NAME', '70레벨 달성', 'RCLEINT_MSG_ACHIEVE_ID_13_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (113, 1, '낮과밤', 'RCLEINT_MSG_ACHIEVE_ID_14_NAME', '만월의 유적지 방문', 'RCLEINT_MSG_ACHIEVE_ID_14_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (114, 1, '트리마스터', 'RCLEINT_MSG_ACHIEVE_ID_15_NAME', '55레벨 스킬 습득', 'RCLEINT_MSG_ACHIEVE_ID_15_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (115, 1, '러시앤강캐', 'RCLEINT_MSG_ACHIEVE_ID_16_NAME', '5강화 성공', 'RCLEINT_MSG_ACHIEVE_ID_16_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (116, 1, '합성대실패', 'RCLEINT_MSG_ACHIEVE_ID_17_NAME', '일루미나의 성지 방문', 'RCLEINT_MSG_ACHIEVE_ID_17_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (117, 1, '진화중독자', 'RCLEINT_MSG_ACHIEVE_ID_18_NAME', '매터리얼 착용', 'RCLEINT_MSG_ACHIEVE_ID_18_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (118, 1, '초보 서번트 조련사', 'RCLEINT_MSG_ACHIEVE_ID_19_NAME', '서번트 획득 퀘스트 완료', 'RCLEINT_MSG_ACHIEVE_ID_19_DESC');
GO

INSERT INTO [dbo].[DT_AchieveList] ([mID], [mValue], [mName], [mNameKey], [mDesc], [mDescKey]) VALUES (119, 1, '중급 서번트 조련사', 'RCLEINT_MSG_ACHIEVE_ID_20_NAME', '서번트 1차 진화', 'RCLEINT_MSG_ACHIEVE_ID_20_DESC');
GO

