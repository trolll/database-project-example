/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Racing
Date                  : 2023-10-07 09:07:31
*/


INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (1, 0, '司托雷德      ', 403, 33);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (2, 0, '博德尔        ', 78, 32);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (3, 0, '朗森          ', 410, 31);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (4, 0, '莱特          ', 104, 30);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (5, 0, '莱帕德        ', 38, 29);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (6, 0, '克莱思        ', 87, 28);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (7, 0, '巴克斯        ', 82, 27);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (8, 0, '托内德        ', 81, 26);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (9, 0, '杜兰德        ', 418, 25);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (10, 0, '海博          ', 409, 24);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (11, 0, '斯塔帕尔      ', 33, 23);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (12, 0, '阿卡希尔      ', 131, 22);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (13, 0, '莱欧纳德      ', 86, 21);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (14, 0, '洛基          ', 127, 20);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (15, 0, '塔拉          ', 85, 19);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (16, 0, '泽依          ', 92, 18);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (17, 0, '露茜          ', 436, 17);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (18, 0, '拜尔特        ', 145, 16);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (19, 0, '伍德罗尔      ', 83, 15);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (20, 0, '依克莱丝      ', 80, 14);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (21, 0, '玛丽安        ', 74, 13);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (22, 0, '莱恩          ', 115, 12);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (23, 0, '麦斯克        ', 148, 11);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (24, 0, '罗斯          ', 76, 10);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (25, 0, '泛影          ', 147, 9);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (26, 0, '莱克          ', 149, 8);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (27, 0, '欧利普        ', 393, 7);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (28, 0, '佩利昂        ', 79, 6);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (29, 0, '卢帕斯        ', 233, 5);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (30, 0, '奎克          ', 77, 4);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (31, 0, '斯莱尔        ', 394, 3);
GO

INSERT INTO [dbo].[DT_Racing] ([mNID], [mPlace], [mNpcNm], [mNpcNo], [mAbility]) VALUES (32, 0, '温特克        ', 550, 2);
GO

