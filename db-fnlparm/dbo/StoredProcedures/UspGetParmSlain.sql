/******************************************************************************  
**  File: 
**  Name: [UspGetParmSlain]  
**  Desc: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  20110603 辫锐档		浇饭牢狼 RHIT 客 RDD 肺爹阑 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmSlain]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SELECT a.[SID], a.[SType], a.[SLevel], a.[SHitPlus], a.[SDDPlus], a.[SRHitPlus], a.[SRDDPlus], RTRIM(b.[SName])
		FROM DT_Slain AS a INNER JOIN TP_SlainType AS b ON(a.[SType] = b.[SType])
	SET NOCOUNT OFF

GO

