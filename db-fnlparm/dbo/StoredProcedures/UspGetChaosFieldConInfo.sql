/******************************************************************************
**		File: 
**		Name: UspGetChaosFieldConInfo
**		Desc: 카오스 배틀 서버에서 접속이 가능한 필드 서버 정보를 받는다.
**
**		Auth: 김 광섭
**		Date: 2009-02-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetChaosFieldConInfo]
	@pChaosBattleSvrNo	SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  [mFieldSvrNo]
	FROM	dbo.TblChaosFieldConInfo
	WHERE	[mChaosBattleSvrNo] = @pChaosBattleSvrNo
			
	SET NOCOUNT OFF;

GO

