/******************************************************************************  
**  Name: UspListItemIncSysPossibleItem  
**  Desc: 殿废 啊瓷 酒捞袍 啊廉坷扁
**  
**                
**  Return values:  
**   饭内靛悸
**                
**  Author: 沥柳龋
**  Date: 2012-03-22
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**	2013.09.27	沥柳龋		mKind 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspListItemIncSysPossibleItem]
AS	
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		mIID,
		mStatus,
		mCubeType,
		mProb,
		mResource,
		mKind
	FROM dbo.TblItemIncSysPossibleItem
	
	SET NOCOUNT OFF

GO

