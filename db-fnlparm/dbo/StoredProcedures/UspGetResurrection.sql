/******************************************************************************
**		Name: UspGetResurrection
**		Desc: 何劝 包访 沥焊甫 啊廉柯促
**
**		Auth: 炼 技泅
**		Date: 2009-07-22
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetResurrection
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mMID, mRMID, mRHp, mIsItemDrop, mIsExp, mIsSelf
	FROM dbo.TblResurrection

GO

