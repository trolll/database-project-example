/******************************************************************************
**		Name: WspInsParmSvrSupportCalendarEvent
**		Desc: ÀÌº¥Æ® ´Þ·Â ¼­¹ö µî·Ï
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspInsParmSvrSupportCalendarEvent]  	
	@pSvrNo		smallint,
    @pEventNo	int,
    @pBeginDate smalldatetime,
    @pEndDate	smalldatetime
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	INSERT INTO dbo.TblParmSvrSupportCalendarEvent (mSvrNo, mEventNo, mBeginDate, mEndDate)
	SELECT @pSvrNo, @pEventNo, @pBeginDate, @pEndDate
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0 
		RETURN(1)

    RETURN(0)

GO

