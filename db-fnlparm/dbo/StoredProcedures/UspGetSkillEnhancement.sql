/******************************************************************************  
**  File: UspGetSkillEnhancement.sql
**  Name: UspGetSkillEnhancement
**  Desc: Á¦ÀÛ¾ÆÀÌÅÛ Á¤º¸ ¾ò±â
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2015.04.17	Á¤ÁøÈ£		»ý¼º
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetSkillEnhancement]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT
		T1.mESPID,
		T2.mOrderNo,
		T1.mUseClass,
		T2.mItemID,
		T2.mCnt,
		T1.mSPID
	FROM
		DT_SkillEnhancement AS T1 
		INNER JOIN DT_SkillEnhancementMaterial AS T2
		ON T1.mESPID = T2.mESPID
		
	
	SET NOCOUNT OFF

GO

