/******************************************************************************
**		Name: UspGetMaterialEnchantValue
**		Desc: 概磐府倔狼 牢忙飘 蔼阑 备茄促.
**
**		Auth: 沥备柳
**		Date: 2009-06-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩     
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMaterialEnchantValue]
	@pIID				INT		
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	MEnchant
	FROM	dbo.TblMaterialItemInfo
	WHERE	[IID] = @pIID

	IF @@ERROR <> 0
	BEGIN
		RETURN (1)
	END
	
	RETURN (0);

GO

