/******************************************************************************
**		Name: UspCreateJackpot
**		Desc: 黎铺 沥焊甫 积己茄促.
**
**		Auth: 沥柳宽
**		Date: 2013-10-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013.10.15	沥柳宽				积己    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCreateJackpot]
	@pSvrNo		SMALLINT	-- 辑滚 锅龋
	,@pCamp		TINYINT		-- 柳康	
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo	INT;
	SET 	@aErrNo = 0;
	
	INSERT INTO dbo.TblJackpot ([mSvrNo], [mCamp]) VALUES (@pSvrNo, @pCamp);

	IF (@@ERROR <> 0)
	BEGIN
		SET	@aErrNo = 1;	-- DB Error;
	END
		 
	RETURN(@aErrNo);

GO

