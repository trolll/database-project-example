CREATE PROCEDURE [dbo].[UspGetParmAbnormalResist]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT a.[AID], a.[AType], a.[ALevel], a.[APercent], RTRIM(b.[AName])
		FROM DT_AbnormalResist AS a INNER JOIN TP_AbnormalType AS b ON(a.[AType] = b.[AType])
		
	SET NOCOUNT OFF

GO

