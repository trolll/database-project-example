
CREATE PROCEDURE dbo.UspGetMaterialEvolutionResult
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		MEID,
		mPercent,
		IID
	FROM
		dbo.TblMaterialEvolutionResult

GO

