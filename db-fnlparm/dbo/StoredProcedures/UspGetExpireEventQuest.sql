/******************************************************************************
**		Name: UspGetExpireEventQuest
**		Desc: 辆丰等 捞亥飘 涅胶飘 沥焊甫 馆券
**
**		Auth: 沥柳龋
**		Date: 2010.06.10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetExpireEventQuest]  	
	@pSvrNo								SMALLINT
	,@pArrEventQuest	VARCHAR(3000)	OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SET @pArrEventQuest = '';	
		
	SELECT 
		@pArrEventQuest = CONVERT(VARCHAR,mObjID) + ',' + @pArrEventQuest
	FROM (
	SELECT mObjID 
	FROM dbo.TblParmSvrSupportEvent T1
		INNER JOIN dbo.TblEventObj T2
			ON T1.mEventID = T2.mEventID
	WHERE T1.mEndDate < getdate()		
		and T1.msvrno=@pSvrNo
		and T2.mObjType = 3	-- quest object 
	GROUP BY mObjID ) T1
	WHERE mObjID NOT IN (	
		SELECT mObjID			-- 柳青吝牢 捞亥飘绰 泅犁 矫埃栏肺 沥府
		FROM dbo.TblParmSvrSupportEvent T1
			INNER JOIN dbo.TblEventObj T2
				ON T1.mEventID = T2.mEventID
		WHERE T1.mBeginDate <= getdate()	
			and T1.mEndDate >= getdate()
			and T1.msvrno=@pSvrNo
			and T2.mObjType = 3	-- quest object 
		GROUP BY mObjID 	
	)

GO

