/******************************************************************************
**		Name: WspInsEventQuestReward
**		Desc: 捞亥飘 涅胶飘 焊惑 殿废
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROC [dbo].[WspInsEventQuestReward] 
    @pEQuestNo	INT,
	@pRank		TINYINT,
	@pIID		INT, 
    @pStatus	TINYINT,
	@pBinding	TINYINT,
	@pEffTime	INT,
	@pValTime	INT,
	@pCnt		INT
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	INSERT INTO dbo.TblEventQuestReward (mEQuestNo, mRank, mIID, mStatus, mBinding, mEffTime, mValTime, mCnt)
	SELECT @pEQuestNo, @pRank, @pIID, @pStatus, @pBinding, @pEffTime, @pValTime, @pCnt
	
	IF (@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

