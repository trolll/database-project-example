/******************************************************************************
**		Name: WspDelCalendarEvent
**		Desc: ÀÌº¥Æ® ´Þ·Â »èÁ¦
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspDelCalendarEvent]
	@pEventNo	INT
	, @pDelType	TINYINT	= 0	-- 0 : all, 1 : subtype  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	BEGIN TRAN
	
		DELETE dbo.TblParmSvrSupportCalendarEvent
		WHERE mEventNo = @pEventNo
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END 
		
		IF @pDelType = 0
		BEGIN
		
			DELETE dbo.TblCalendarEvent
			WHERE mEventNo = @pEventNo
			
			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
			
			IF @aErrNo <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(1)			
			END 		
		END 
		
		
	COMMIT TRAN	
    RETURN(0)

GO

