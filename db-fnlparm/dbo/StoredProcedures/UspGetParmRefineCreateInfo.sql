/******************************************************************************  
**  File: UspGetParmRefineCreateInfo.sql
**  Name: UspGetParmRefineCreateInfo
**  Desc: 力累酒捞袍 沥焊 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2012.09.10	沥柳龋		积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmRefineCreateInfo]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	SELECT 
		a.RID, b.RItemID, b.RNum, a.RItemID0, a.RItemID1,  
		a.RItemID2,a.RItemID3,a.RItemID4,a.RItemID5,a.RItemID6,a.RItemID7,a.RItemID8,
		a.RItemID9,a.RSuccess, a.RIsCreateCnt, c.mCost, c.mNationOp
	FROM 
		DT_Refine AS a INNER JOIN DT_RefineMaterial AS b ON a.RID = b.RID
		INNER JOIN DT_RefineCreateInfo AS c ON a.RID = c.mRID
		ORDER BY a.RID ASC, b.ROrderNo ASC
END

GO

