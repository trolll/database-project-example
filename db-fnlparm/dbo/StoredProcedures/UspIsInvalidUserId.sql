CREATE PROCEDURE dbo.UspIsInvalidUserId
	 @pUserId	CHAR(12)
--------WITH ENCRYPTION
AS
	DECLARE	@aCnt	INT
	SET @aCnt = dbo.UfnIsInvalidString(@pUserId)
	IF(0 = @aCnt)
	 BEGIN
		-- [String]가 VARCHAR이 아닌 CHAR이라면 RTRIM()을 해야 한다.
		--SELECT @aCnt=COUNT(*) FROM TblInvalidUserId WHERE @pUserId LIKE '%'+[mString]+'%'
		SELECT @aCnt=COUNT(*) FROM TblInvalidUserId WHERE @pUserId = [mString]
	 END
	 
	RETURN @aCnt

GO

