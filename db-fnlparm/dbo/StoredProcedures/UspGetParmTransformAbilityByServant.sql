/******************************************************************************
**		Name: UspGetParmTransformAbilityByServant
**		Desc: 函脚 矫 饭骇俊 蝶弗 瓷仿摹甫 啊瘤绰 抛捞喉阑 啊廉柯促. 
**              
**		Auth: 捞侩林
**		Date: 2016-04-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetParmTransformAbilityByServant]
	@pServantTransformType		SMALLINT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	SELECT mLevel, mMaxHP, mMaxMP, mMaxWeight, mShortAttackRate, mLongAttackRate, mMoveRate, mType
	FROM dbo.TblTransformAbilityByServant
	WHERE mServantTransformType = @pServantTransformType

	SET NOCOUNT OFF

GO

