/******************************************************************************
**		Name: UspGetSpecialScopeSkill
**		Desc: Æ¯¼ö¹üÀ§ ½ºÅ³ Á¤º¸ È¹µæ
**		Test:			
**		Auth: ³²¼º¸ð
**		Date: 2014/09/01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetSpecialScopeSkill
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @TblSid TABLE
	(
		mSid INT NOT NULL PRIMARY KEY
	)

	INSERT INTO @TblSid
	SELECT SID FROM dbo.Dt_Skill
	
	SELECT T1.mSkillNo, T1.mType, T1.mIsSpecialRadius, T1.mParamA, T1.mParamB, T1.mParamC
	FROM dbo.TblSpecialScopeSkill AS T1
		INNER JOIN @TblSid AS T2
	ON T1.mSkillNo = T2.mSid

GO

