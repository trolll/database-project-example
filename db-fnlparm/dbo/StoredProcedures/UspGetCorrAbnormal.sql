/******************************************************************************
**		Name: UspGetCorrAbnormal
**		Desc: 반응 상태이상 정보를 가져온다.
**
**		Auth: 조 세현
**		Date: 2009-04-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetCorrAbnormal]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	
	SELECT mOrigAID, mCorrAID, mType, mCorrSkill, mUsePerHp, mUsePerMp, mMaxCnt, mPercent 
	FROM dbo.TblCorrespondAbnormal;

GO

