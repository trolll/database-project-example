
CREATE PROCEDURE dbo.UspGetSetItemInfo
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		T1.mSetType,
		T1.mSetName,
		T2.IID
	FROM
		dbo.TP_SetItemInfo	T1
	INNER JOIN
		dbo.TblSetItemMember	T2
	ON
		T1.mSetType = T2.mSetType
	ORDER BY
		T1.mSetType
	ASC

GO

