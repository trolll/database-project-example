
CREATE PROCEDURE dbo.UspSetParmSvrLastSupportDate  	
	@pSvrNo	SMALLINT
	, @pSupportDate SMALLDATETIME = '2020-01-01 00:00:00'
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	UPDATE dbo.TblParmSvr
	SET
		mLastSupportDate = @pSupportDate
	WHERE mSvrNo = @pSvrNo

GO

