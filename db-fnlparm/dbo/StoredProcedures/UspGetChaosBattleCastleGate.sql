/******************************************************************************
**		File: 
**		Name: UspGetChaosBattleCastleGate
**		Desc: 카오스배틀용 성문 정보를 로드한다.
**
**		Auth: 김 광섭
**		Date: 2009-03-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetChaosBattleCastleGate]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  mNo
		, mPosX
		, mPosY
		, mPosZ
		, mWidth
		, mDir
		, mDesc
	FROM	dbo.TblChaosBattleCastleGate

GO

