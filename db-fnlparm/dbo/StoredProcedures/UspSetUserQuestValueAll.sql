CREATE PROCEDURE	dbo.UspSetUserQuestValueAll
	@pQuestNo		INT,		-- 퀘스트번호
	@pValue		INT,		-- 퀘스트값
	@pErrNoStr		VARCHAR(50) OUTPUT
As
Begin
	SET NOCOUNT ON
	DECLARE 	@pErr		INT

	SELECT	@pErrNoStr	= 'eErrNoSqlInternalError',
			@pErr = 0
		
	UPDATE dbo.TblUserQuest 
	SET mValue = @pValue
	WHERE mQuestNo = @pQuestNo

	SELECT @pErr  = @@ERROR	

	IF @pErr <> 0
	BEGIN
		SET	@pErrNoStr	= 'eErrNoSqlInternalError'			
		RETURN(@pErr)	
	END 				
	
	RETURN(0)	
END

GO

