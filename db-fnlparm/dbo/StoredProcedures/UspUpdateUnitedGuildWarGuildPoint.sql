/******************************************************************************
**		Name: UspUpdateUnitedGuildWarGuildPoint
**		Desc: 漂沥 辨靛狼 辨靛 器牢飘甫 函版茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-06-09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspUpdateUnitedGuildWarGuildPoint
	  @pSvrNo			SMALLINT		-- 辨靛啊 加茄 辑滚
	, @pGuildNo			INT				-- 辨靛 锅龋
	, @pGuildNm			CHAR(12)		-- 辨靛 疙
	, @pChgGuildPoint	INT		OUTPUT	-- 函版且 器牢飘樊 & 函版 饶 辨靛 器牢飘 荐摹
	
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aRowCnt 	INT;
	DECLARE	@aErrNo		INT
		, @aResultChgGuildPoint INT;
	
	SELECT @aResultChgGuildPoint = 0, @aRowCnt = 0, @aErrNo=0;
	
	UPDATE	dbo.TblUnitedGuildWarGuildInfo	
	SET 
			@aResultChgGuildPoint = [mGuildPoint] = [mGuildPoint] + @pChgGuildPoint	
	WHERE [mSvrNo] = @pSvrNo 
			AND [mGuildNo] = @pGuildNo;
	
	SELECT	@aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	
	IF(0 <> @aErrNo)
	 BEGIN
		RETURN(@aErrNo);
	 END
	 
	IF(0 = @aRowCnt)
	 BEGIN
		EXEC @aErrNo = dbo.UspCreateUnitedGuildWarGuildInfo @pSvrNo, @pGuildNo, @pGuildNm;
		IF(0 <> @aErrNo)
		 BEGIN
			RETURN(@aErrNo);
		 END
		 
		UPDATE	dbo.TblUnitedGuildWarGuildInfo	
		SET @aResultChgGuildPoint = [mGuildPoint] = [mGuildPoint] + @pChgGuildPoint	
		WHERE [mSvrNo] = @pSvrNo AND [mGuildNo] = @pGuildNo;
		
		IF(0 <> @@ERROR)
		 BEGIN
			RETURN(@@ERROR);
		 END
	 END
	
	SET @pChgGuildPoint = @aResultChgGuildPoint;
	
	RETURN(0);

GO

