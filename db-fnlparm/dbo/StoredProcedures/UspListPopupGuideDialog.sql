/******************************************************************************
**		Name: UspListPopupGuideDialog
**		Desc: TblPopupGuideDialog 甫 掘绢 坷绰 Procedure
**
**		Auth: 辫锐档
**		Date: 2009-09-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspListPopupGuideDialog]
AS
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT 
		[mGuideNo], RTRIM([mSubject]) AS 'mSubject', RTRIM([mDialog]) AS 'mDialog'
	FROM [dbo].[TblPopupGuideDialog]

GO

