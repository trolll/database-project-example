/******************************************************************************
**		Name: UspListPopupGuideCondition
**		Desc: TblPopupGuideDialog 甫 掘绢 坷绰 Procedure
**
**		Auth: 辫锐档
**		Date: 2009-09-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspListPopupGuideCondition]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT [mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]
		FROM [dbo].[TblPopupGuideCondition]
		WHERE [mConType] IN
		(
			SELECT [mConType] FROM TP_PopupGuideConditionType
		)

GO

