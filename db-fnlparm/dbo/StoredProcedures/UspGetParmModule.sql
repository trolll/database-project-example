CREATE PROCEDURE [dbo].[UspGetParmModule]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT a.[MID], a.[MType], a.[MLevel], a.[MAParam], a.[MBParam], a.[MCParam], RTRIM(b.[MName])
		FROM DT_Module AS a INNER JOIN TP_ModuleType AS b ON(a.[MType] = b.[MType])
		--WHERE (IsValid <> 0)
		
	SET NOCOUNT OFF

GO

