/******************************************************************************
**		Name: UspGetParmServantFriendlySkillTransform
**		Desc: 辑锅飘 模剐档 函脚 胶懦 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-04-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantFriendlySkillTransform]
	@pIID		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT SPID, MID
	FROM dbo.TblServantFriendlySkillTransform
	WHERE IID = @pIID

	SET NOCOUNT OFF;

GO

