/******************************************************************************
**		Name: UspGetQuestReward
**		Desc: 涅胶飘 焊惑沥焊 
**
**		Auth: 沥柳龋
**		Date: 2010-01-26
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetQuestReward]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT mRewardNo, mExp, mID, mCnt, mBinding, mStatus, mEffTime, mValTime
    FROM dbo.TblQuestReward

GO

