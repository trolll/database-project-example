/******************************************************************************
**		Name: UspInitUTGAchieveGuildList
**		Desc: 烹钦 辨靛傈 辨靛 单捞磐 诀利辨靛 府胶飘 檬扁拳.**
**		Auth: 巢扁豪
**		Date: 2013.06.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013/08/02	巢己葛				诀利辨靛鸥涝阑 涝仿栏肺 罐档废 荐沥. 
**										辨靛鸥涝捞 叼弃飘牢 版快俊档 檬扁拳 荐青窍档废 荐沥. 	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspInitUTGAchieveGuildList]
(
	@pGuildType			INT			-- 诀利辨靛鸥涝
)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	-- 烹钦 辨靛傈 辨靛 沥焊.
	IF @pGuildType = 2 OR @pGuildType = 0
	BEGIN
		IF NOT EXISTS(SELECT mAchieveGuildID FROM dbo.TblAchieveGuildList)
		BEGIN
			INSERT 
			INTO dbo.TblAchieveGuildList(mGuildRank, mGuildName, mMemberName, mEquipPoint, mUpdateIndex)
			SELECT mGuildRank, mGuildName, mMemberName, mEquipPoint, mUpdateIndex 
			FROM dbo.DT_AchieveGuildList 
			
			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
				RETURN 1;
		END
	END

	SET NOCOUNT OFF

	RETURN 0;

GO

