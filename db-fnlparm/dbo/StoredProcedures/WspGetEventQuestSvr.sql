/******************************************************************************
**		Name: WspGetEventQuestSvr
**		Desc: 捞亥飘 涅胶飘 辑滚
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetEventQuestSvr]  	
	@pSvrNo	SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T1.mEQuestNo
		,T1.mTitle
		,T1.mType
		,T2.mBeginDate
		,T2.mEndDate
		,mIsRunning =
			CASE WHEN mBeginDate <= GETDATE() AND mEndDate >= GETDATE() THEN 0
				ELSE 1
			END
	FROM dbo.TblEventQuest T1
		LEFT OUTER JOIN 
			(
				SELECT 
					  mSvrNo
					 ,mEQuestNo
					 ,mBeginDate
					 ,mEndDate
				FROM dbo.TblParmSvrSupportEventQuest
				WHERE mSvrNo = @pSvrNo
			) T2
				ON T1.mEQuestNo = T2.mEQuestNo

GO

