

CREATE  PROCEDURE dbo.WspUptLimitedResource
	@pResourceType	INT,
	@pMaxCnt		INT,
	@pRemainerCnt	INT,
	@pRandomVal		FLOAT
AS
	SET NOCOUNT ON
	
	UPDATE dbo.TblLimitedResource
	SET mMaxCnt = @pMaxCnt, mRemainerCnt = @pRemainerCnt, mRandomVal = @pRandomVal
	WHERE mResourceType = @pResourceType
	
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0
	BEGIN
		RETURN(1)	-- db error
	END

	RETURN(0)

GO

