/******************************************************************************  
**  Name: UspGetRegionQuestCondition  
**  Desc: Áö¿ª Äù½ºÆ® Á¶°Ç Á¤º¸¸¦ ¾ò¾î¿Â´Ù.
**  
**  Auth: Á¤Áø¿í  
**  Date: 2014-09-01  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**  2014-09-01  Á¤Áø¿í		»ý¼º 
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetRegionQuestCondition
AS
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT T1.mQuestNo, T1.mParmID, T1.mBoss, T1.mStepCnt, T1.mStep1, T1.mStep2, T1.mStep3, T1.mTotalCnt
	FROM dbo.TblRegionQuestCondition AS T1
		INNER JOIN dbo.TblRegionQuest AS T2
	ON T1.mQuestNo = T2.mQuestNo

GO

