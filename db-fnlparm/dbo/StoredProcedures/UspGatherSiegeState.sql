CREATE PROCEDURE dbo.UspGatherSiegeState
	 @pGamePath		VARCHAR(100)	-- '[223.234.255.212].[FnlGame]' 형태임.
	,@pParmPath		VARCHAR(100)	-- '[234.233.255.233].[FnlParm]' 형태임.
	,@pSvrNo		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE @aGameDb	VARCHAR(100)
	SET		@aGameDb = @pGamePath + '.[dbo]'
	DECLARE @aParmDb	VARCHAR(100)
	SET		@aParmDb = @pParmPath + '.[dbo]'	
	
	DECLARE	@aSql		VARCHAR(1024)
	SET @aSql = 'INSERT TblSiegeState([mRegDate], [mIsTower], [mSvrNo], [mSvrNm], [mPlaceNm], [mPlaceNo], [mGuildNo], [mGuildNm], [mGuildMark], [mGuildMasterNm], [mGuildCreateDate])'
			  + ' SELECT a.[mChgDate], a.[mIsTower], b.[mSvrNo], RTRIM(b.[mDesc]), c.[mPlaceNm], c.[mPlaceNo], d.[mGuildNo], d.[mGuildNm], d.[mGuildMark], f.[mNm], d.[mRegDate]'
			  + ' FROM ' + @aGameDb + '.[TblCastleTowerStone]  AS a'
			  + ' INNER JOIN ' + @aParmDb + '.[TblParmSvr]     AS b ON(b.[mSvrNo]	= ' + CONVERT(VARCHAR(20),@pSvrNo) + ')'
			  + ' INNER JOIN ' + @aParmDb + '.[TblPlace]       AS c ON(a.[mPlace]   = c.[mPlaceNo])'
			  + ' INNER JOIN ' + @aGameDb + '.[TblGuild]       AS d ON(a.[mGuildNo] = d.[mGuildNo])'
			  + ' INNER JOIN ' + @aGameDb + '.[TblGuildMember] AS e ON(a.[mGuildNo] = e.[mGuildNo])'
			  + ' INNER JOIN ' + @aGameDb + '.[TblPc]          AS f ON(e.[mPcNo]    = f.[mNo])'
			  + ' WHERE e.[mGuildGrade] = 0'
	
	--DELETE TblSiegeState
	EXEC(@aSql)	  
								
	SET NOCOUNT OFF

GO

