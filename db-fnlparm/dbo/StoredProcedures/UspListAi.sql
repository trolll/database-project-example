CREATE PROCEDURE [dbo].[UspListAi]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT RTRIM([mDesc]), [mAiId], [mFindItem], [mRageHp], [mFearHp], 
		   [mAngryItem1], [mAngryItem2], [mAngryItem3], [mAngryItem4],[mAngryItem5],[mAngryItem6],
		   [mRageItem1],  [mRageItem2],  [mRageItem3],  [mRageItem4], [mRageItem5], [mRageItem6]
		FROM TblAi
		
	SET NOCOUNT OFF

GO

