/******************************************************************************
**		Name: dbo.UspGetAbnormalAttrIgnore
**		Desc: DT_AbnormalAttrIgnore 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2012.12.04  沥柳龋				mConditionAID 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAbnormalAttrIgnore]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		T1.mOriginAID
		, T1.mConditionType
		, T1.mConditionAType
		, T1.mIsComplex
		, T1.mConditionAID
	FROM dbo.DT_AbnormalAttrIgnore AS T1
		INNER JOIN dbo.DT_Abnormal AS T2
	ON T1.mOriginAID = T2.AID
END

GO

