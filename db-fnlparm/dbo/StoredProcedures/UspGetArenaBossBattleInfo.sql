/******************************************************************************
**		Name: UspGetArenaBossBattleInfo
**		Desc: 焊胶傈 冀磐沥焊 
**
**		Auth: 沥柳龋
**		Date: 2016-09-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaBossBattleInfo]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		mPlace,
		mStartX,
		mStartZ,
		mEndX,
		mEndZ
	FROM dbo.TblArenaBossBattleInfo
	
	SET NOCOUNT OFF

GO

