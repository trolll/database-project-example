/******************************************************************************  
**  File: UspGetFullMoonSpotList.sql
**  Name: UspGetFullMoonSpotList 
**  Desc: 父岿狼 蜡利瘤俊辑 包府登绰 阁胶磐府胶飘 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2011.06.13	kirba		积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetFullMoonSpotList]
AS  
	SET NOCOUNT ON ;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	SELECT 
		T1.mGID,
		T2.mMID,
		T2.mIsFullMoon
	FROM dbo.TblMonsterSpot T1
	JOIN dbo.TblFullMoonMonsterList T2 On T1.mMID = T2.mMID
	
	SET NOCOUNT OFF

GO

