/******************************************************************************
**		Name: UspGetQuestAdd
**		Desc: 涅胶飘 沥焊(器扁 啊瓷咯何, 焊惑锅龋)甫 掘绢柯促. 
**
**		Auth: 沥柳龋
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010-05-12  沥柳龋              涅胶飘 力茄饭骇 眠啊
**		2013-08-28  沥柳龋				捞傈 涅胶飘 锅龋 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetQuestAdd]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT mQuestNo, mAbandonment, mRewardNo, mLevel1, mLevel2, mPreQuestNo
    FROM dbo.TblQuest

GO

