CREATE PROCEDURE dbo.UspGetTr
	 @pSvrNo	SMALLINT
--------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	DECLARE	@aType		TINYINT
	DECLARE	@aWorldNo	SMALLINT
	SELECT @aType=[mType], @aWorldNo=[mWorldNo] FROM TblParmSvr 
		WHERE ([mIsValid] <> 0) AND (@pSvrNo = [mSvrNo])
	IF(0 = @@ROWCOUNT)
	 BEGIN
		RAISERROR('Invalid SvrNo(%d)', 11, 1, @pSvrNo) WITH LOG
		GOTO LABEL_END
	 END

	SELECT RTRIM([mTrDesc]), [mWaitTm], [mPeriodTm], mWeekDay
		FROM TblTr
		WHERE (mIsValid <> 0) AND ((@aWorldNo = [mWorldNo]) OR (0 = [mWorldNo])) 
							  AND ((@aType = [mType])       OR (5 = [mType]))
							  AND ((@pSvrNo = [mSvrNo])	    OR (0 = [mSvrNo]))
		ORDER BY [mTrDesc] ASC, [mSvrNo] DESC, [mType] ASC, [mWorldNo] DESC

LABEL_END:		
	SET NOCOUNT OFF

GO

