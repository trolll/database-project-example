/******************************************************************************
**		Name: UspGetUnitedGuildWarKillRanking
**		Desc: 老扁配 珐欧 沥焊甫 掘绰促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetUnitedGuildWarKillRanking
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  [mSvrNo]
		, [mRanking]
		, RTRIM([mGuildNm])
		, [mKillCnt]
	FROM	dbo.TblUnitedGuildWarKillRanking
	ORDER BY [mRanking] ASC

GO

