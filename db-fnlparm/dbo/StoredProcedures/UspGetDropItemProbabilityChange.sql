/******************************************************************************
**		Name: UspGetDropItemProbabilityChange
**		Desc: µå·Ó ¾ÆÀÌÅÛ È®·ü º¯°æ
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-08-18
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetDropItemProbabilityChange
	@pSvrNo SMALLINT	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT DISTINCT
		T3.DDrop
		,T4.mMultiplication
		,T4.mAddition
		,t4.DGroup
		,T4.mItemID
	FROM dbo.DT_DropItem AS T3
		INNER JOIN (
						SELECT T1.mItemID, T2.DDrop, T1.mMultiplication, T1.mAddition, t2.DGroup
						FROM dbo.TblDropItemProbabilityChange AS T1
							INNER JOIN dbo.DT_DropGroup AS T2
						ON T1.mDGroup = T2.DGroup
						WHERE T1.mSvrNo = @pSvrNo
					) AS T4
	ON T3.DDrop = T4.DDrop AND T3.DItem = T4.mItemID

GO

