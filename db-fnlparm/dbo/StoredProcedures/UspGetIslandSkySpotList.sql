/******************************************************************************   
**  File: UspGetIslandSkySpotList.sql 
**  Name: UspGetIslandSkySpotList  
**  Desc: 玫傍狼级俊辑包府登绰阁胶磐府胶飘掘扁
*******************************************************************************   
**  Change History   
*******************************************************************************   
**  Date:  Author:  Description:  
**  ---------- ---------- ---------------------------------------   
** 2013.03.12     kongkong7 积己
*******************************************************************************/    
CREATE PROCEDURE [dbo].[UspGetIslandSkySpotList] 
AS   
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
   
	SELECT  
		T1.mGID
		, T2.mMID
		, T2.mCamp
		, T2.mIsEvent
		, T2.mIsHunting
	FROM dbo.TblMonsterSpot AS T1
		INNER JOIN dbo.TblIslandSkyMonsterList AS T2
	ON T1.mMID = T2. mMID

GO

