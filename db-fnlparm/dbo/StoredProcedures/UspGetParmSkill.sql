/******************************************************************************  
**  File: UspGetParmSkill  
**  Name: UspGetParmSkill  
**  Desc: UspGetParmSkill 函版  
**  
**  Auth: chose96  
**  Date: 2008.09.25  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:		Description:  
**  -------- --------   ---------------------------------------  
**  2009.07.21 炼技泅	胶懦 沥焊甫 啊廉柯促.  
**  2009-09-30 沥 备柳	亲格 眠啊. (mCoolTime)  
**  2009-09-30 沥 备柳	亲格 眠啊. (mCastingGroup)  
**  2009-09-30 沥 备柳	亲格 眠啊. (mCoolTimeGroup)  
**  2010-06-16 辫碍龋	mConsumeItem2, mConsumeItemCnt2 眠啊  
**  2010-11-09 沥柳龋	胶懦俊 浇饭牢阑 汲沥茄促.  
**  2011-07-22 辫碍龋	mIsAttack眠啊(傍拜胶懦牢瘤 咯何)
*******************************************************************************/   
CREATE    PROCEDURE [dbo].[UspGetParmSkill]  
--------WITH ENCRYPTION  
AS  
 SET NOCOUNT ON; -- Count-set搬苞甫 积己窍瘤 富酒扼. 
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
 SELECT a.[SID], a.[SHitPlus], a.[SMPPerUse], a.[SSpellNum], b.[AttrbuteID], RTRIM(a.[SName]),   
     a.[SHPPerUse], a.[SChaoUse], a.[mApplyRadius], a.[mApplyCnt], a.[mApplyRace], a.[SType], a.[mCastingDelay],   
  a.[mConsumeItem], a.[mConsumeItemCnt],   
  a.[mConsumeItem2], a.[mConsumeItemCnt2], -- 2010-06-16 added  
  a.[mActiveType], a.[mCoolTime], a.[mCastingGroup], a.[mCoolTimeGroup],
  a.[mIsAttack]
 FROM DBO.DT_Skill AS a   
  LEFT JOIN DT_SkillAttribute AS b   
   ON(a.[SID] = b.[SID])  
 ORDER BY a.[SID]  
    
 SELECT a.[SID], a.[SHitPlus], a.[SMPPerUse], a.[SSpellNum], b.[AbnormalID], RTRIM(a.[SName]),   
     a.[SHPPerUse], a.[SChaoUse], a.[mApplyRadius], a.[mApplyCnt], a.[mApplyRace], a.[SType], a.[mCastingDelay],   
  a.[mConsumeItem], a.[mConsumeItemCnt],   
  a.[mConsumeItem2], a.[mConsumeItemCnt2], -- 2010-06-16 added  
  a.[mActiveType], a.[mCoolTime], a.[mCastingGroup], a.[mCoolTimeGroup]  ,
  a.[mIsAttack]
 FROM DT_Skill AS a   
  LEFT JOIN DT_SkillAbnormal AS b   
   ON(a.[SID] = b.[SID])  
 ORDER BY a.[SID]  
  
 -- 2010-11-09 眠啊  
 SELECT a.[SID], a.[SHitPlus], a.[SMPPerUse], a.[SSpellNum], b.[SlainID], RTRIM(a.[SName]),   
     a.[SHPPerUse], a.[SChaoUse], a.[mApplyRadius], a.[mApplyCnt], a.[mApplyRace], a.[SType], a.[mCastingDelay],   
  a.[mConsumeItem], a.[mConsumeItemCnt],   
  a.[mConsumeItem2], a.[mConsumeItemCnt2], -- 2010-06-16 added  
  a.[mActiveType], a.[mCoolTime], a.[mCastingGroup], a.[mCoolTimeGroup]  ,
  a.[mIsAttack]
 FROM DT_Skill AS a   
  LEFT JOIN DT_SkillSlain AS b   
   ON(a.[SID] = b.[SkillID])  
 ORDER BY a.[SID]

GO

