CREATE   PROCEDURE [dbo].[WspListInvalidCharNm]
AS
	SET NOCOUNT ON

	SELECT
		mString
	FROM
		[dbo].[TblInvalidCharNm] WITH (NOLOCK)
	ORDER BY
		mRegDate

GO

