CREATE PROCEDURE [dbo].[UspGetParmNPC]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[SID], 100, RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCSlain AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[SID]	
		
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[SID], 100, RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCProtect AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[SID]	
		
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[DGroup], b.[DPercent], RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCDrop AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[DGroup]		
		
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[AID], 100, RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCAttributeAdd AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]	
		
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[AID], 100, RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCAttributeResist AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]					
		
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[AID], 100, RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCAbnormalAdd AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]		
		
	SELECT a.[MID], a.[MClass], a.[MExp], a.[MDV], a.[MPV],a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRate], a.[MMoveRate], a.[MHp], a.[MMp], a.[MRespawn], b.[AID], 100, RTRIM(a.[MName])
		FROM DT_NPC AS a LEFT JOIN DT_NPCAbnormalResist AS b ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]			
								
		
	SET NOCOUNT OFF

GO

