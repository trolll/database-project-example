/******************************************************************************  
**  Name: UspGetAbnormalAttrApply  
**  Desc: 惑怕捞惑 Apply

**	Auth: 沥备柳
**	Date: 10.12.06
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**	荐沥老		荐沥磊		荐沥郴侩    
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetAbnormalAttrApply]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		mOriginAID,
		mOriginAType,
		mConditionAID
	FROM	dbo.DT_AbnormalAttrApply
		
	SET NOCOUNT OFF

GO

