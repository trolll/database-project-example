/******************************************************************************  
**  File: UspGetEventDungeonInfo.sql
**  Name: UspGetEventDungeonInfo
**  Desc: 捞亥飘 带傈狼 沥焊 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2012.02.09	kirba		积己
**	2012.08.13	kirba		昏力 酒捞袍阑 焊胶侩 / 涅胶飘侩 2俺肺 盒府
**	2016.04.18	傍籍痹		[mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime]
							, [mAcceptTime], [mStartPosX2], [mStartPosY2]
							, [mStartPosZ2], [mTeleportPosRatio] 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetEventDungeonInfo]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		mDungeonType,
		mDungeonPlace,
		mBossMID,
		mIngressBossIID,
		mIngressQuestIID,
		mIngressAID,
		mMinArea,
		mPartyCnt,
		mPlayTime,
		mAcceptTime,
		mStartX,
		mStartZ,
		mEndX,
		mEndZ,
		mStartPosX,
		mStartPosY,
		mStartPosZ,
		mStartPosX2,
		mStartPosY2,
		mStartPosZ2,
		mTeleportPosRatio
	FROM dbo.TblEventDungeonInfo
	
	SET NOCOUNT OFF

GO

