CREATE     PROCEDURE [dbo].[WspListAdminServer]
AS
	SET NOCOUNT ON

	SELECT
		mSvrNo
		, mDesc

	FROM
		[dbo].[TblParmSvr] WITH (NOLOCK)
	WHERE
		mType = 1 or mType = 2
	ORDER BY
		mSvrNo

GO

