/******************************************************************************
**		Name: UspGetParmMonsterRole
**		Desc: NPC 开且 沥焊甫 啊廉柯促.
**
**		Auth: 辫碍龋
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmMonsterRole]
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	
	SELECT 
		MID, mRole 
	FROM dbo.DT_MonsterRole 
	ORDER BY MID, mRole

GO

