/******************************************************************************
**		File: UspGetParmMonsterEx.sql
**		Name: UspGetParmMonsterEx
**		Desc: 阁胶磐 
**			  1.Drop父 Percent啊 狼固啊 乐瘤父, 老包己阑 困秦辑 唱赣瘤浚 100阑 逞变促.	
**              
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2006-11-27	JUDY				函脚 HP, 函脚 MP
**		2006-12-08	JUDY				面傈侩 惑痢, 备涝, 魄概侩 惑痢 ID 眠啊 
**		2007-01-04	JUDY				函脚公霸 眠啊 
**		2007-05-17	JUDY				Scale 函版 
**		2007-05-28	JUDY				MP, HP 府哩樊 焊郴扁
**		2007-10-08	soundkey			mIsEventTest 眠啊
**		2008.02.20	JUDY				mIsShowHp 
**		2008.04.30	JUDY				mSupportType
**		2009.03.30	辫堡挤				疙抗狼 狼瘤 拿烦 SELECT 亲格 眠啊 
**		2009.12.28	辫堡挤				SELECT 亲格俊 眠啊等 拿烦阑 器窃茄促.
**		2010.06.14	沥备柳				mAttackType 眠啊
**		2010.06.14	沥备柳				mTransType 眠啊
**		2010.06.16	辫碍龋				mDPV, mMPV, mRPV 眠啊
										mDDV, mMDV, mRDV 眠啊
										MPV, MDV 力芭
**		2011.03.16	沥备柳				mSubDDWhenCritical 眠啊
										mEnemySubCriticalHit 眠啊
**		2013.03.11  沥柳龋				mEventQuest 眠啊
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetParmMonsterEx]
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT a.[MID], a.[MClass], a.[MExp], 
			--a.[MDV], a.[MPV],		-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],
			
			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[SID], 100, RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
		   a.[mIsEvent], a.[mLevel], a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID]
		   ,a.[mTransformWeight] ,a.[mNationOp], a.mScale, a.mHPRegen, a.mMPRegen, IContentsLv, a.mIsEventTest
		   ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterSlain AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[SID]	
		
	SELECT a.[MID], a.[MClass], a.[MExp], 

			--a.[MDV], a.[MPV],		-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],

			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[SID], 100, RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
		   a.[mIsEvent], a.[mLevel], a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID] 
		   ,a.[mTransformWeight]  ,a.[mNationOp], a.mScale, a.mHPRegen, a.mMPRegen, IContentsLv, a.mIsEventTest	   
		   ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterProtect AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[SID]	
		
	SELECT a.[MID], a.[MClass], a.[MExp], 

			--a.[MDV], a.[MPV],			-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],

			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[DGroup], b.[DPercent], RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
		   a.[mIsEvent], a.[mLevel], a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID] 
		   ,a.[mTransformWeight]   ,a.[mNationOp] , a.mScale, a.mHPRegen, a.mMPRegen, IContentsLv, a.mIsEventTest	   
		   ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterDrop AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[DGroup]		
		 
	SELECT a.[MID], a.[MClass], a.[MExp], 

			--a.[MDV], a.[MPV],			-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],

			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[AID], 100, RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
		   a.[mIsEvent], a.[mLevel],  a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID] 
		   ,a.[mTransformWeight] ,a.[mNationOp]	, a.mScale , a.mHPRegen, a.mMPRegen , IContentsLv, a.mIsEventTest 
		   ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterAttributeAdd AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]	
		
	SELECT a.[MID], a.[MClass], a.[MExp], 

			--a.[MDV], a.[MPV],		-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],

			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[AID], 100, RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
		   a.[mIsEvent], a.[mLevel],  a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID] 
		   ,a.[mTransformWeight]  ,a.[mNationOp]	  , a.mScale , a.mHPRegen, a.mMPRegen, IContentsLv, a.mIsEventTest
		   ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterAttributeResist AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]					
		
	SELECT a.[MID], a.[MClass], a.[MExp], 

			--a.[MDV], a.[MPV],		-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],

			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[AID], 100, RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
		   a.[mIsEvent], a.[mLevel],  a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID] 
		   ,a.[mTransformWeight]   ,a.[mNationOp]	   , a.mScale, a.mHPRegen, a.mMPRegen, IContentsLv, a.mIsEventTest
		   ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterAbnormalAdd AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]		
		
	SELECT a.[MID], a.[MClass], a.[MExp], 

			--a.[MDV], a.[MPV],		-- 昏力凳
			a.[mDDV], a.[mMDV], a.[mRDV],
			a.[mDPV], a.[mMPV], a.[mRPV],

			a.[MHIT], a.[MMinD], a.[MMaxD],
		   a.[MAttackRateOrg], a.[MMoveRateOrg], a.[MAttackRateNew], a.[MMoveRateNew], a.[MHp], a.[MMp], a.[MMoveRange], b.[AID], 100, RTRIM(a.[MName]),
		   a.[MGbjType], a.[MRaceType], a.[MAiType], a.[MAiEx], a.[MCastingDelay], a.[MChaotic],
		   a.[MSameRace1], a.[MSameRace2], a.[MSameRace3], a.[MSameRace4],
		   a.[mSightRange], a.[mAttackRange], a.[mSkillRange], a.[mBodySize], a.[mDetectTransF], a.[mDetectTransP], a.[mDetectChao], a.[mIsResistTransF],
           a.[mIsEvent], a.[mLevel],  a.[mIsTest], a.[mHPNew], a.[mMPNew], a.[mBuyMerchanID], a.[mSellMerchanID], a.[mChargeMerchanID] 
           ,a.[mTransformWeight]  ,a.[mNationOp]   , a.mScale     , a.mHPRegen, a.mMPRegen, IContentsLv, a.mIsEventTest
           ,a.mIsShowHp, a.mSupportType, a.mVolitionOfHonor, a.mIsAmpliableTermOfValidity, a.mAttackType, a.mTransType, a.mSubDDWhenCritical, a.mEnemySubCriticalHit, a.mEventQuest
		FROM dbo.DT_Monster AS a 
			LEFT JOIN dbo.DT_MonsterAbnormalResist AS b 
				ON(a.[MID] = b.[MID])
		ORDER BY a.[MID], b.[AID]

GO

