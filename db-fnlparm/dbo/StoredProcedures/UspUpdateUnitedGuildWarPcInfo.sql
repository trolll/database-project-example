/******************************************************************************    
**  Name: UspUpdateUnitedGuildWarPcInfo    
**  Desc: 烹钦 辨靛 措傈 某腐磐 何啊 沥焊甫 盎脚茄促.    
**    
**  Auth: 辫 堡挤    
**  Date: 2010-01-19    
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:  Description:    
**  -------- -------- ---------------------------------------    
**  2013.04.03 沥柳宽  [mGuildNm] 拿烦眠啊  
**  2013.05.06 巢扁豪  [mEquipPoint] 拿烦眠啊 
**  2013.07.22 巢己葛  EquipPoint 历厘苞沥 荐沥
*******************************************************************************/    
CREATE PROCEDURE [dbo].[UspUpdateUnitedGuildWarPcInfo]    
 @pSvrNo   SMALLINT,  -- 辑滚 锅龋    
 @pGuildNo  INT,   -- 辨靛 锅龋   
 @pGuildNm CHAR(12), -- 辨靛 疙   
 @pPcNo   INT,   -- 某腐磐 锅龋    
 @pPcNm   CHAR(12),  -- 某腐磐 疙    
 @pChgVictoryCnt INT,    -- 函版且 铰府 冉荐
 @pEquipPoint INT		 -- 某腐磐 厘厚 器牢飘
AS    
 SET NOCOUNT ON;    
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
     
 DECLARE @aErrNo INT    

 SET @aErrNo = 0;    

 UPDATE dbo.TblUnitedGuildWarPcInfo     
 SET     
   [mHerosBTVictoryCnt] = [mHerosBTVictoryCnt] + @pChgVictoryCnt,
	mEquipPoint = @pEquipPoint     
 WHERE [mSvrNo] = @pSvrNo     
   AND [mGuildNo] = @pGuildNo     
   AND [mPcNo] = @pPcNo;    
    
 IF(0 = @@ROWCOUNT)    
 BEGIN    
  -- 粮犁窍瘤 臼绰促搁 货肺 父电促.    
  EXEC @aErrNo = dbo.UspCreateUnitedGuildWarPcInfo @pSvrNo, @pGuildNo, @pGuildNm, @pPcNo, @pPcNm;    
  IF(0 <> @aErrNo)    
  BEGIN    
   RETURN(@aErrNo);    
  END    
       
  UPDATE dbo.TblUnitedGuildWarPcInfo     
  SET     
   [mHerosBTVictoryCnt] = [mHerosBTVictoryCnt] + @pChgVictoryCnt,
	mEquipPoint = @pEquipPoint     
  WHERE [mSvrNo] = @pSvrNo     
   AND [mGuildNo] = @pGuildNo     
   AND [mPcNo] = @pPcNo;    
    
  IF(0 = @@ROWCOUNT)    
   BEGIN    
   RETURN(10); -- 恐 捞繁老捞.ば_ば;    
   END    
 END    
     
 RETURN(@aErrNo);

GO

