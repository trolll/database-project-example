/******************************************************************************  
**  Name: UspGetParmItemNm
**  Desc: 酒捞袍ID, 酒捞袍疙, 酒捞袍疙俊 蝶扼 沥纺等 鉴困蔼(鉴困绰 吝汗登瘤 臼澜) 饭内靛悸阑 馆券茄促.
**  
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetParmItemNm  
AS 
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	 SELECT   		
		(SELECT count(*) 
		FROM dbo.DT_ITEM b
		WHERE b.IName < a.IName or (b.IName = a.IName and b.IID < a.IID))+1  as NmOrdNo		
		 , a.IID
		 , a.IName
		 , a.IPShopItemType   
	 FROM dbo.DT_ITEM a		 
	 ORDER BY NmOrdNo ASC;

GO

