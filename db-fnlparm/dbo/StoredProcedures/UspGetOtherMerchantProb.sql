/******************************************************************************
**		Name: UspGetOtherMerchantProb
**		Desc: ÀÌ°è »óÀÎ ¼ÒÈ¯ È®·ü
**
**		Auth: Á¤ÁøÈ£
**		Date: 2015-10-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetOtherMerchantProb]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT
		T1.mMonID,
		T1.mMerchantID,
		T1.mProb
	FROM
		dbo.TblOtherMerchantProbability AS T1 
		INNER JOIN dbo.TblOtherMerchantInfo AS T2
		ON T1.mMerchantID = T2.mMerchantID

	SET NOCOUNT OFF

GO

