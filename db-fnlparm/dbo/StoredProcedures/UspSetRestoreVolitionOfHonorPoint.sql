/******************************************************************************
**		File: 
**		Name: UspSetRestoreVolitionOfHonorPoint
**		Desc: 회복가능한 명예의 의지 포인트를 설정한다.
**
**		Auth: 김 광섭
**		Date: 2009-04-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: Author:	
**		Description: 
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspSetRestoreVolitionOfHonorPoint]
	   @pPcNo		INT
	 , @pSvrNO		SMALLINT
	 , @pRestore	SMALLINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	DECLARE	@aRowCnt	INT
	
	UPDATE	dbo.TblCommonPoint
	SET
		[mRestoreVolitionOfHonor]	= @pRestore
	WHERE	mSvrNo = @pSvrNo
				AND mPcNo = @pPcNo;
				
	IF(0 <> @aErrNo OR 1 <> @aRowCnt)
	 BEGIN
		RETURN(1);
	 END
	 
	RETURN(0);

GO

