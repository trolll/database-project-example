/******************************************************************************  
**  Name: UspGetParmAbnormal  
**  Desc:   
**  
**  Auth:  
**  Date:   
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- -----------------------------------------------  
**  2009-03-30 辫 辈挤    亲格 眠啊. (AGrade)  
**  2009-09-30 沥 备柳    亲格 眠啊. (TP_AbnormalType - ARemovable)  
**  2011-07-18 辫 碍龋    亲格 眠啊. (ACopyable)  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmAbnormal]  
AS  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
    
 SELECT   
  a.[AID], a.[AType], a.[ALevel], a.[APercent], b.[MID]  
  , RTRIM(c.[AName]), a.[ATime], a.[AGrade], c.[ARemovable], c.[ACopyable]
 FROM dbo.DT_Abnormal AS a   
  LEFT  JOIN dbo.DT_AbnormalModule AS b   
   ON(a.[AID] = b.[AID])  
  INNER JOIN dbo.TP_AbnormalType     
   AS c ON(a.[AType] = c.[AType])  
 ORDER BY a.[AID], b.[MID]

GO

