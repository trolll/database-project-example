/******************************************************************************
**		Name: UspGetParmPlaceInvalidList
**		Desc: 瘤开付促 荤侩 且 荐 绝绰 府胶飘(酒捞袍, 惑怕捞惑)阑 掘绢柯促.
**              
**		Auth: 沥备柳
**		Date: 10.06.22
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩   
*******************************************************************************/
CREATE  PROCEDURE dbo.UspGetParmPlaceInvalidList
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT [mPlace], [mType], [mValue]
	FROM [dbo].[TblPlaceInvalidList]
	ORDER BY [mPlace] ASC
	
	SET NOCOUNT OFF

GO

