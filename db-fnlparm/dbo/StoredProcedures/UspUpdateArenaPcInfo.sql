/******************************************************************************
**		Name: UspUpdateArenaPcInfo
**		Desc: 酒饭唱 PC沥焊 诀单捞飘
**
**		Auth: 沥柳龋
**		Date: 2016-09-029
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateArenaPcInfo]
	@pSvrNo				SMALLINT,	-- 辑滚 锅龋    
	@pPcNo					INT,		-- 某腐磐 锅龋    
	@pPlayTime				INT			-- 敲饭捞矫埃
AS    
	SET NOCOUNT ON;    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    

    DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT

	IF NOT EXISTS (SELECT * 
				FROM dbo.TblArenaPcInfo 
				WHERE mSvrNo = @pSvrNo AND mPcNo = @pPcNo)
	BEGIN
		INSERT INTO dbo.TblArenaPcInfo (mRegDate, mSvrNo, mPcNo, mPlayTime, mPanalty)
		VALUES (GETDATE(), @pSvrNo, @pPcNo, @pPlayTime, 0)
	END
	
	UPDATE dbo.TblArenaPcInfo 
	SET mPlayTime = @pPlayTime
	WHERE mSvrNo = @pSvrNo AND mPcNo = @pPcNo
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

