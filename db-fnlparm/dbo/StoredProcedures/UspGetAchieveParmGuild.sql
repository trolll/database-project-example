/******************************************************************************
**		Name: UspGetAchieveParmGuild
**		Desc: 诀利辨靛 扁夯 蔼.**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveParmGuild]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mAchieveGuildID
		, mGuildRank
		, mGuildName
		, mMemberName
		, mEquipPoint
		, mUpdateIndex
	FROM dbo.DT_AchieveGuildList

GO

