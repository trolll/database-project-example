/******************************************************************************
**		Name: UspGetAbnormalAttrChangeEquipAbnormal
**		Desc: 惑怕捞惑 加己 吝 厘厚俊 蝶扼 
**			   Attach 惑怕捞惑捞 函版登绰惑怕捞惑 沥焊积己  
**
**		Auth: 沥柳龋
**		Date: 2010-12-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAbnormalAttrChangeEquipAbnormal]
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT

		T1.mConditionAID,
		T1.mOriginAType,
		T1.mOriginAItemNo,
		T1.mEffectAID
		
	FROM	dbo.DT_AbnormalAttrChangeEquipAbnormal	T1
		INNER JOIN dbo.DT_Abnormal		T2
			ON	T1.mConditionAID = T2.AID

GO

