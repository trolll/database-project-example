/******************************************************************************
**		Name: WspUpdateCharResotreVolitionOfHonor
**		Desc: 雀汗啊瓷茄 疙抗狼 狼瘤 器牢飘 荐沥
**
**		Auth: 辫籍玫
**		Date: 2009-05-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE dbo.WspUpdateCharResotreVolitionOfHonor
	@pSvrNo		SMALLINT,		-- pc狼 鞘靛 辑滚 锅龋
	@pPcNo		INT,			-- pc狼 鞘靛 辑滚狼 pc锅龋
	@pRestoreVolitionOfHonor	SMALLINT		-- 荐沥且 雀汗啊瓷茄 疙抗狼 狼瘤 器牢飘
AS
	SET NOCOUNT ON			-- 汲疙 : 搬苞 饭内靛 悸阑 馆券 救 矫挪促.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	--- 函荐 急攫
	DECLARE @aErrNo INT,
			@aRowCnt INT

	--- 函荐 檬扁拳 
	SELECT @aErrNo = 0, @aRowCnt = 0   -- 茄临俊 檬扁拳 
	
	UPDATE dbo.TblCommonPoint
	SET mRestoreVolitionOfHonor = @pRestoreVolitionOfHonor
	WHERE mSvrNo = @pSvrNo
		AND mPcNo = @pPcNo

	SELECT @aErrNo = @@Error, @aRowCnt = @@RowCount

	IF @aRowCnt = 0	-- 某腐磐 沥焊啊 绝阑 锭, 货肺 眠啊
	BEGIN
		INSERT INTO dbo.TblCommonPoint(mSvrNo, mPcNo, mRestoreVolitionOfHonor)
		VALUES(@pSvrNo, @pPcNo, @pRestoreVolitionOfHonor)
	END

	SELECT @aErrNo = @@Error, @aRowCnt = @@RowCount
	
	RETURN @aErrNo

GO

