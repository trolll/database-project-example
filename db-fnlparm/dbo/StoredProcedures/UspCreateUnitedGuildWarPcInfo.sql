/******************************************************************************    
**  Name: UspCreateUnitedGuildWarPcInfo    
**  Desc: 烹钦 辨靛 措傈 某腐磐 何啊 沥焊甫 积己茄促.    
**    
**  Auth: 辫 堡挤    
**  Date: 2010-01-19    
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:  Description:    
**  -------- -------- ---------------------------------------    
**  2013.04.03 沥柳宽  [mGuildNm] 拿烦眠啊      
*******************************************************************************/    
CREATE PROCEDURE dbo.UspCreateUnitedGuildWarPcInfo    
	@pSvrNo  SMALLINT,  -- 辑滚 锅龋    
	@pGuildNo INT,   -- 辨靛 锅龋  
	@pGuildNm CHAR(12), -- 辨靛 疙     
	@pPcNo  INT,   -- 某腐磐 锅龋    
	@pPcNm  CHAR(12)  -- 某腐磐 疙    
AS    
	SET NOCOUNT ON;    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
	 
	DECLARE @aGuildNo INT;    
	SET  @aGuildNo = 0;    
	 
	SELECT     
		@aGuildNo = mGuildNo     
	FROM dbo.TblUnitedGuildWarPcInfo     
	WHERE mSvrNo = @pSvrNo     
	AND mPcNo = @pPcNo;    

	IF(0 <> @@ERROR)    
	BEGIN    
		RETURN(1); -- DB ERROR    
	END    

	IF ( @aGuildNo > 0 )    
	AND ( @aGuildNo <> @pGuildNo )    
	BEGIN    
		DELETE dbo.TblUnitedGuildWarPcInfo     
		WHERE mSvrNo = @pSvrNo     
		AND mGuildNo = @aGuildNo     
		AND mPcNo = @pPcNo;    

		IF(0 <> @@ERROR)    
		BEGIN    
			RETURN(1); -- DB ERROR    
		END    
	END     
	 
	IF ( @aGuildNo > 0 )
	BEGIN   
		RETURN(0); -- 捞固 殿废 沥焊
	END    

	INSERT INTO dbo.TblUnitedGuildWarPcInfo (mSvrNo, mGuildNo, mGuildNm, mPcNo, mPcNm)     
	VALUES (@pSvrNo, @pGuildNo, @pGuildNm, @pPcNo, @pPcNm);    

	IF(0 <> @@ERROR)    
	BEGIN     
		RETURN(2); -- DB ERROR    
	END    
	 
	RETURN(0);

GO

