/******************************************************************************
**		Name: UspGetServantAbilityDivisor
**		Desc: ¼­¹øÆ® ±âº» ´É·ÂÄ¡ °¡Á®¿À±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-10-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantAbilityDivisor]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT SType, SStrength, SDexterity, SInteligence
	FROM dbo.TblServantAbilityDivisor
	ORDER BY SType

	SET NOCOUNT OFF;

GO

