/******************************************************************************
**		Name: UspMerchantSellList
**		Desc:  »óÀÎÆÇ¸Å¸®½ºÆ®
**
**		Auth: ±è ±¤¼·
**		Date: 2007-11-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20121001	³²±âºÀ				[SortKey] ÄÃ·³ Ãß°¡	
**		20151013	Á¤ÁøÈ£				[LimitCnt] ÄÃ·³ Ãß°¡	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspMerchantSellList]
AS
	SET NOCOUNT ON;	-- Count-set°á°ú¸¦ »ý¼ºÇÏÁö ¸»¾Æ¶ó.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		ListID, 
		ItemID, 
		Price, 
		Flag,
		mShopType,
		mPaymentType,
		SortKey,
		LimitCnt
	FROM dbo.TblMerchantSellList T1
	INNER JOIN dbo.TblMerchantName T2
		ON T1.ListID = T2.mID	 
	ORDER BY ListID, Flag

GO

