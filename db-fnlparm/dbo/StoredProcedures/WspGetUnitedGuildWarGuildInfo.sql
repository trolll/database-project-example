/******************************************************************************
**		Name: WspGetUnitedGuildWarGuildInfo
**		Desc: 烹钦 辨靛 沥焊甫 啊廉柯促.
**
**		Auth: 眠槛
**		Date: 2010-03-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE dbo.WspGetUnitedGuildWarGuildInfo
	 @pSvrNo					SMALLINT
	,@pGuildNo				INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT 
		mGuildPoint, mKillCnt, mDieCnt, mVictoryCnt, mDrawCnt, mDefeatCnt, mVictoryHerosBT1, mDefeatHerosBT1, mVictoryHerosBT2, mDefeatHerosBT2, mVictoryHerosBT3, mDefeatHerosBT3, mRecentlyMatchSvrNo, mRecentlyMatchGuildNm, mRecentlyMatchResult
	FROM dbo.TblUnitedGuildWarGuildInfo
	WHERE mSvrNo = @pSvrNo
		AND mGuildNo = @pGuildNo

GO

