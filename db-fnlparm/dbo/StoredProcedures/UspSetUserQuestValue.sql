CREATE PROCEDURE	dbo.UspSetUserQuestValue
	@pUserNo		INT,		-- 사용자 계정번호
	@pQuestNo		INT,		-- 퀘스트번호
	@pValue		INT,			-- 퀘스트값
	@pErrNoStr		VARCHAR(50) OUTPUT
As
Begin
	SET NOCOUNT ON
	SET LOCK_TIMEOUT 2000		-- 2초 이내에 작업이 끝나지 않으면 1222 오류를 반환 처리 한다. 

	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@pErr = 0
		

	BEGIN TRANSACTION

		IF NOT EXISTS(	SELECT mValue 
						FROM dbo.TblUserQuest WITH (NOLOCK) 
						WHERE mUserNo = @pUserNo AND mQuestNo = @pQuestNo)
		BEGIN
			INSERT dbo.TblUserQuest(mUserNo, mQuestNo, mValue) 
			VALUES(	@pUserNo, 
					@pQuestNo, 
					@pValue)
		END
		ELSE
		BEGIN
			UPDATE dbo.TblUserQuest 
			SET mValue = @pValue, mUpdateDate = getdate()
			WHERE mUserNo = @pUserNo AND mQuestNo = @pQuestNo
		END
				
		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR	

		IF @pRowCnt = 0 OR @pErr <> 0
		BEGIN
			ROLLBACK TRANSACTION
			
			SET	@pErrNoStr	= 'eErrNoSqlInternalError'			
			RETURN(@pErr)	
		END 				
		
	COMMIT TRANSACTION
	RETURN(0)	
END

GO

