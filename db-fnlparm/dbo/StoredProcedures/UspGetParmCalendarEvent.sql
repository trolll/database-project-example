/******************************************************************************
**		Name: UspGetParmCalendarEvent
**		Desc: ´Þ·Â ÀÌº¥Æ® Á¤º¸
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmCalendarEvent]
(
	@pSvrNo	SMALLINT
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		T1.mEventNo
		, T2.mTitle
		, T2.mDesc
		, T2.mURL
		, T1.mBeginDate
		, T1.mEndDate
	FROM dbo.TblParmSvrSupportCalendarEvent AS T1
		INNER JOIN dbo.TblCalendarEvent AS T2
	ON T1.mEventNo = T2.mEventNo
	WHERE T1.mSvrNo = @pSvrNo
	AND T1.mEndDate > GETDATE()
END

GO

