/******************************************************************************
**		Name: UspGetMaterialDrawAddGroup
**		Desc: 概磐府倔 惶扁 眠啊 裙垫 沥焊甫 掘绢柯促.
**
**		Auth: 沥柳龋
**		Date: 2013.10.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMaterialDrawAddGroup]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		MDRD,
		mAddGroup,
		mResType,
		mMaxResCnt,
		mSuccess
	FROM
		dbo.TblMaterialDrawAddGroup

GO

