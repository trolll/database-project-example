
CREATE PROCEDURE dbo.UspGetSetItemAbnormalInfo
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		T1.mSetType,
		T2.AID
	FROM
		dbo.TP_SetItemInfo	T1
	INNER JOIN
		dbo.TblSetItemAbnormal	T2
	ON
		T1.mSetType = T2.mSetType
	ORDER BY
		T1.mSetType
	ASC

GO

