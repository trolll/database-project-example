-- 荐沥 橇肺矫廉 眠啊
/******************************************************************************
**		Name: UspListAiEx
**		Desc: TblAIEx 单捞磐 炼雀
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2016-11-11	巢己葛				Percent, CoolTime 拿烦 眠啊
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspListAiEx]
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	SELECT	 [mAiId]
			,[mSkillPercent]
			,[mSkillCoolTime]
			,[mSkillPerUpToWe0]
			,[mSkillPerUpToInside0]
			,[mSkillPerUpToOutside0]
			,[mSkillPerUpToWe1]
			,[mSkillPerUpToInside1]
			,[mSkillPerUpToOutside1]
			,[mSkillPerUpToWe2]
			,[mSkillPerUpToInside2]
			,[mSkillPerUpToOutside2]
			,[mSkillPerDownToWe0]
			,[mSkillPerDownToInside0]
			,[mSkillPerDownToOutside0]
			,[mSkillPerDownToWe1]
			,[mSkillPerDownToInside1]
			,[mSkillPerDownToOutside1]
			,[mSkillPerDownToWe2]
			,[mSkillPerDownToInside2]
			,[mSkillPerDownToOutside2]
			,[mItemPercent]
			,[mItemCoolTime]
			,[mItemPerUpBattle0]
			,[mItemPerUpChase0]
			,[mItemPerUpBattle1]
			,[mItemPerUpChase1]
			,[mItemPerUpBattle2]
			,[mItemPerUpChase2]			
			,[mItemPerDownBattle0]
			,[mItemPerDownChase0]
			,[mItemPerDownBattle1]
			,[mItemPerDownChase1]
			,[mItemPerDownBattle2]
			,[mItemPerDownChase2]
		FROM TblAiEx
		
	SET NOCOUNT OFF

GO

