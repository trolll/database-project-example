CREATE     PROCEDURE [dbo].[WspInsertInvalidUserId]
	@mString VARCHAR(12), -- 금칙어
	@mReturnCode SMALLINT OUTPUT -- 리턴코드
AS
	SET NOCOUNT ON

	IF (SELECT COUNT(*) FROM [dbo].[TblInvalidUserId] WHERE mString = @mString) = 0

	BEGIN

		BEGIN TRANSACTION

		INSERT INTO
			[dbo].[TblInvalidUserId]
			(mString)
		VALUES
			(@mString)

		IF @@ERROR <> 0
			ROLLBACK TRANSACTION
		ELSE
			COMMIT TRANSACTION

		SET @mReturnCode = @@ERROR
	END

	ELSE

		SET @mReturnCode = 1

GO

