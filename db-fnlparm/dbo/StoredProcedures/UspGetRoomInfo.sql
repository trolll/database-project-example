/******************************************************************************
**		Name: UspGetRoomInfo
**		Desc: 何劝 包访 沥焊甫 啊廉柯促
**
**		Auth: 炼 技泅
**		Date: 2009-07-22
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetRoomInfo
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT mID, mName, mType, mMapNo, mKeyItem
	FROM dbo.TblRoomInfo

GO

