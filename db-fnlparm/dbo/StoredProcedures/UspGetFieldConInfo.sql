/******************************************************************************
**		Name: UspGetFieldConInfo
**		Desc: 필드 서버에서 접속가능한 카오스배틀 서버 정보를 받는다.
**
**		Auth: 김 광섭
**		Date: 2009-02-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetFieldConInfo]
	@pSvrNo		SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  [mChaosBattleSvrNo]
	FROM	dbo.TblChaosFieldConInfo
	WHERE	[mFieldSvrNo] = @pSvrNo
			
	SET NOCOUNT OFF;

GO

