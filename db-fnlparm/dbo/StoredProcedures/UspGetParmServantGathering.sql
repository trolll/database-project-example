/******************************************************************************
**		Name: UspGetParmServantGathering
**		Desc: 辑锅飘 盲笼 沥焊 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-10-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantGathering]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT SServerType, SIsSpeed, SServantIID, SResultIID, SCount
	FROM dbo.TblServantGathering

	SET NOCOUNT OFF;

GO

