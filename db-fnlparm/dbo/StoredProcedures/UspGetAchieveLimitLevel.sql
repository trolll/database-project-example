/******************************************************************************
**		Name: UspGetAchieveLimitLevel
**		Desc: 傈府前 力茄 饭骇**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveLimitLevel]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mLimitID
		, mSRange
		, mERange
		, mProb
	FROM dbo.TP_AchieveLimitLevel

GO

