/******************************************************************************
**		Name: UspGetArenaBossBattleBossList
**		Desc: 焊胶傈 焊胶 阁胶磐 沥焊
**
**		Auth: 沥柳龋
**		Date: 2016-09-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaBossBattleBossList]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT DISTINCT mMID, mBossMID
	FROM dbo.TblArenaBossBattleMonList
	
	SET NOCOUNT OFF

GO

