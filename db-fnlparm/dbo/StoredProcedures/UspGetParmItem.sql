/******************************************************************************  
**  File: UspGetParmItem.sql  
**  Name: UspGetParmItem  
**  Desc: 酒捞袍 沥焊, 酒捞袍 胶懦  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**  2006.11.27	JUDY     	捞亥飘 傈侩 鞘靛 眠啊   
**  2006.12.18	JUDY     	捞亥飘 拿烦疙 函版 棺, 扁家 咯何 拿烦 眠啊  
**  2007.02.13	JUDY     	胶懦 酒捞袍 鉴辑 裹困 函版   
**  2007.03.02	JUDY     	蜡丰拳 咯何 扁瓷 眠啊    
**  2007.05.17	JUDY     	抛挤 咯何 眉农   
**  2007.06.12	JUDY     	涅胶飘 酒捞袍 鞘夸 肮荐   
**  2007.08.24	JUDY     	IIsConfirm 拿烦 眠啊   
**  2007.09.17	judy     	IIsSealable  
**  2007.10.05	soundkey 	IAddDDWhenCritical 拿烦 眠啊  
**  2007.12.10	judy     	豪牢秦力俊 鞘夸茄 拿烦 肮荐 眠啊   
**  2008.03.10	JUDY     	mIsPracticalPeriod <-- Pratical Item 咯何  
**  2008.03.21	judy     	付阑俊辑 酒捞袍阑 罐阑 荐 乐绰 可记 眠啊  
**  2008.04.07	辫堡挤   	眠啊等 拿烦阑 SELECT 府胶飘俊 眠啊  
**  2008.04.17	judy     	Support Type  
**  2008.05.16	JUDY     	俺牢惑痢 备盒蔼俊 蝶扼 load 贸府 函版  
**  2010.02.11	辫堡挤   	烹钦辨靛措傈 辑滚俊辑 荤侩啊瓷茄瘤 敲贰弊  
**  2010.01.27	辫碍龋   	[ITermOfValidityLv] 拿烦 眠啊  
**  2010.05.18	沥备柳   	[IAddShortAttackRange] 拿烦 眠啊  
**  2010.05.18	沥备柳   	[IAddLongAttackRange] 拿烦 眠啊  
**  2010.06.16	辫碍龋   	IDPV, IMPV, IRPV 眠啊  
							IDDV, IMDV, IRDV 眠啊  
							IHDPV, IHMPV, IHRPV 眠啊  
							IHDDV, IHMDV, IHRDV 眠啊  
							IPV, IDV, IHPV, IHDV 昏力  
**	2010.12.02	沥备柳		[ISubDDWhenCritical] 拿烦 眠啊
							[IEnemySubCriticalHit] 拿烦 眠啊
**	2012.01.11	傍籍痹		[ISubTypeOption] 拿烦 眠啊
**	2012.10.01	巢扁豪		[IMaxBeadHoleCount] 拿烦 眠啊
**	2013.10.15	沥柳宽		[ITermOfValidityMi] 拿烦 眠啊
**	2016.09.23	沥柳龋		[mIsDeleteArenaSvr] 拿烦 眠啊 ( 烹钦辨靛傈俊辑 荤侩啊瓷茄 酒捞袍吝 酒饭唱俊辑绰 瘤况具窍绰 酒捞袍)
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmItem]
	@pShopClass TINYINT  
AS  
 SET NOCOUNT ON     
    
 SELECT a.[IID], a.[IType], a.[ILevel],     
    
 --a.[IDV], a.[IPV], -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[SID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],      
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],  
     
 --,a.[IHDV], a.[IHPV], -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,  
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod,  a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform] , a.ISupportType , a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange], a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemSkill AS b       
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass       
 ORDER BY a.[IID], b.[SOrderNO],  b.[SID]       
         
 SELECT a.[IID], a.[IType], a.[ILevel],     
     
 --a.[IDV], a.[IPV], -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
   
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[AID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],       
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],  
     
 --,a.[IHDV], a.[IHPV], -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
  
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,  
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod, a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform] , a.ISupportType  , a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange]  , a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemAttributeAdd AS b       
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass          
 ORDER BY a.[IID], b.[AID]       
  
 SELECT a.[IID], a.[IType], a.[ILevel],     
     
 --a.[IDV], a.[IPV],   -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
  
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[AID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],       
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],  
  
 --,a.[IHDV], a.[IHPV],  -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
  
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,  
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod,  a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform]  , a.ISupportType , a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange] , a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemAttributeResist AS b       
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass        
 ORDER BY a.[IID], b.[AID]        
  
 SELECT a.[IID], a.[IType], a.[ILevel],     
  
 --a.[IDV], a.[IPV],  -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
  
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[SID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],       
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],  
  
 --,a.[IHDV], a.[IHPV], -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
  
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,  
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod, a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform]  , a.ISupportType , a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange]  , a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemSlain AS b       
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass       
 ORDER BY a.[IID], b.[SID]       
  
 SELECT a.[IID], a.[IType], a.[ILevel],     
  
 --a.[IDV], a.[IPV],  -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
  
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[PID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],       
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],      
  
 --,a.[IHDV], a.[IHPV], -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
  
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,   
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod, a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform]  , a.ISupportType , a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange]  , a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemProtect AS b       
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass       
 ORDER BY a.[IID], b.[PID]       
  
 SELECT a.[IID], a.[IType], a.[ILevel],     
  
 --a.[IDV], a.[IPV],    -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
  
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[AID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],       
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],  
  
 --,a.[IHDV], a.[IHPV],   -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
  
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,  
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod, a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform]  , a.ISupportType , a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange]  , a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemAbnormalAdd AS b      
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass       
 ORDER BY a.[IID], b.[AID]        
  
 SELECT a.[IID], a.[IType], a.[ILevel],     
  
 --a.[IDV], a.[IPV],   -- 昏力凳    
 a.[IDDV], a.[IMDV], a.[IRDV],    
 a.[IDPV], a.[IMPV], a.[IRPV],    
  
 a.[IDHIT], a.[IDDD], a.[IRHIT], a.[IRDD], a.[IMHIT], a.[IMDD],      
 a.[IHPPlus], a.[IMPPlus], a.[ISTR], a.[IDEX], a.[IINT], a.[IMaxStack], a.[IWeight], a.[IUseType],      
 a.[IUseNum], a.[IRecycle], a.[IHPRegen], a.[IMPRegen], a.[IAttackRate], a.[IMoveRate],      
 a.[ICritical], a.[ITermOfValidity], a.[ITermOfValidityMi], b.[AID], RTRIM(a.[IName]), a.[IStatus], a.[IFakeID], a.[IFakeName],       
 a.[IRange], a.[IUseClass], a.[IUseLevel], a.[IUseEternal], a.[IUseDelay], a.[IUseInAttack], a.[IIsEvent], a.[IIsIndict],  
  
 --,a.[IHDV], a.[IHPV],   -- 昏力凳    
 a.[IHDDV], a.[IHMDV], a.[IHRDV],    
 a.[IHDPV], a.[IHMPV], a.[IHRPV],    
  
 a.[IAddWeight], a.ISubType, a.IIsCharge,  
 a.[INationOp],  
 IIsPShop = CASE WHEN c.IID IS NULL THEN 0 ELSE 1 END,  
 a.IPShopItemType,  
 IQuestNo, a.IIsTest, a.IQuestNeedCnt, a.IContentsLv,  
 a.[IIsConfirm], a.[IIsSealable], a.[IAddDDWhenCritical],  
 a.mSealRemovalNeedCnt, a.mIsPracticalPeriod, a.mIsReceiveTown, a.[IIsReinforceDestroy], a.[IAddPotionRestore], a.[IAddMaxHpWhenTransform],  
 a.[IAddMaxMpWhenTransform], a.[IAddAttackRateWhenTransform], a.[IAddMoveRateWhenTransform], a.ISupportType, a.[ITermOfValidityLv],  
 a.[mIsUseableUTGWSvr], a.[IAddShortAttackRange], a.[IAddLongAttackRange], a.[IWeaponPoisonType], a.[ISubDDWhenCritical], a.[IEnemySubCriticalHit], a.[IIsPartyDrop],  
 a.[IMaxBeadHoleCount], a.[ISubTypeOption], a.[mIsDeleteArenaSvr]
 FROM dbo.DT_Item AS a       
  LEFT JOIN dbo.DT_ItemAbnormalResist AS b       
   ON(a.[IID] = b.[IID])      
  LEFT OUTER JOIN dbo.TblPShopSupportItem AS c      
   ON(a.IID = c.IID)      
  AND c.PShopClass = @pShopClass       
 ORDER BY a.[IID], b.[AID]                                
	      
	SET NOCOUNT OFF

GO

