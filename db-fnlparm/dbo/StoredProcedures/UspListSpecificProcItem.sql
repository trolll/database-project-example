
CREATE  PROCEDURE [dbo].[UspListSpecificProcItem]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT [mIID], [mProcNo], [mAParam], [mBParam], [mCParam],[mDParam] FROM TblSpecificProcItem

GO

