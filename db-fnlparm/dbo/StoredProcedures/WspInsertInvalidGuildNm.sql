CREATE     PROCEDURE [dbo].[WspInsertInvalidGuildNm]
	@mString VARCHAR(12), -- 금칙어
	@mReturnCode SMALLINT OUTPUT -- 리턴코드
AS
	SET NOCOUNT ON

	IF (SELECT COUNT(*) FROM [dbo].[TblInvalidGuildNm] WHERE mString = @mString) = 0

	BEGIN

		BEGIN TRANSACTION

		INSERT INTO
			[dbo].[TblInvalidGuildNm]
			(mString)
		VALUES
			(@mString)

		IF @@ERROR <> 0
			ROLLBACK TRANSACTION
		ELSE
			COMMIT TRANSACTION

		SET @mReturnCode = @@ERROR
	END

	ELSE

		SET @mReturnCode = 1

GO

