CREATE PROCEDURE dbo.UspSetCommonPoint
	@pSvrNo				SMALLINT,
	@pPcNo				INT,
	@pType				TINYINT,	-- 0 : 疙抗狼 狼瘤 器牢飘, 1 : 疙抗 器牢飘, 2 : 墨坷胶 器牢飘
	@pPoint				BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aRowCnt	INT;
	DECLARE	@aErr		INT
			, @aVolitionOfHonor	smallint
			, @aHonorPoint	int
			, @aChaosPoint	bigint
	
	SELECT @aRowCnt = 0, @aErr = 0, @aVolitionOfHonor = 0, @aHonorPoint = 0, @aChaosPoint = 0;

	UPDATE	dbo.TblCommonPoint
	SET
		 @aVolitionOfHonor = [mVolitionOfHonor] = 
			CASE 
				WHEN @pType = 0 THEN [mVolitionOfHonor] + CONVERT(INT, @pPoint)
					ELSE [mVolitionOfHonor]
			END
		, @aHonorPoint = [mHonorPoint]	= 
			CASE 
				WHEN @pType = 1 THEN [mHonorPoint] + CONVERT(INT, @pPoint)
					ELSE [mHonorPoint]
			END
		, @aChaosPoint = [mChaosPoint] = 
			CASE 
				WHEN @pType = 2 THEN [mChaosPoint] + @pPoint
					ELSE [mChaosPoint]
			END
	WHERE	[mSvrNo] = @pSvrNo
				AND [mPcNo] = @pPcNo;
				
	SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF((0 <> @aErr) OR (@aRowCnt <> 1))
	 BEGIN
		RETURN(1);	-- error
	 END


	SELECT mPoint  = 
			CASE
				WHEN @pType = 0 THEN @aVolitionOfHonor
				WHEN @pType = 1 THEN @aHonorPoint
				WHEN @pType = 2 THEN @aChaosPoint
			END
	
	RETURN(0);

GO

