/******************************************************************************
**		Name: UspGetArenaBossBattleBossGrade
**		Desc: 焊胶傈 焊胶 阁胶磐 殿鞭
**
**		Auth: 沥柳龋
**		Date: 2016-09-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaBossBattleBossGrade]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT DISTINCT mBossMID, mBossGrade
	FROM dbo.TblArenaBossBattleMonList
	
	SET NOCOUNT OFF

GO

