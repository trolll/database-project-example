CREATE     PROCEDURE [dbo].[WspListInvalidGuildNickNm]
AS
	SET NOCOUNT ON

	SELECT
		mString
	FROM
		[dbo].[TblInvalidGuildNickNm] WITH (NOLOCK)
	ORDER BY
		mRegDate

GO

