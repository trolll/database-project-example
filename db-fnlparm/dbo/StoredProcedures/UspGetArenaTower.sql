/******************************************************************************
**		Name: UspGetArenaTower
**		Desc: 拜傈侩 己巩 沥焊甫 肺靛茄促.
**
**		Auth: 沥柳龋
**		Date: 2016-09-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaTower]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  mNo
		, mPosX
		, mPosY
		, mPosZ
		, mWidth
		, mDir
		, mDesc
		, mGroup
	FROM	dbo.TblArenaTower

GO

