/******************************************************************************  
**  Name: UspGetRegionQuestPlace  
**  Desc: Áö¿ª Äù½ºÆ® ÇÃ·¹ÀÌ½º Á¤º¸¸¦ ¾ò¾î¿Â´Ù.
**  
**  Auth: Á¤Áø¿í  
**  Date: 2014-09-01  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**  2014-09-01  Á¤Áø¿í		»ý¼º 
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetRegionQuestPlace
AS
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT T1.mQuestNo, T1.mPlace
	FROM dbo.TblRegionQuestPlace AS T1
		INNER JOIN dbo.TblPlaceInfo AS T2
	ON T1.mPlace = T2.mPlaceNo

GO

