CREATE PROCEDURE [dbo].[UspGetTradeList]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT [mNo], [mMonID], [mRecvItem], [mRecvCnt], [mGiveItem], [mGiveCnt], [mTalkMsg] FROM TblTradeList
	SET NOCOUNT OFF

GO

