/******************************************************************************  
**  File: 
**  Name: UspGetSkillTreeNode
**  Desc: 胶懦 飘府 畴靛 沥焊甫 肺靛茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.13 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetSkillTreeNode
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  


	SELECT a.mSTNID, a.mSTID, a.mName, a.mMaxLevel, a.mTermOfValidity
	FROM dt_skilltreenode  a
	WHERE a.mNodeType=0

GO

