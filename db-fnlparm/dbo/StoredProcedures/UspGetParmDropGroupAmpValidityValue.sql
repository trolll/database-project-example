/******************************************************************************
**		Name: UspGetParmDropGroupAmpValidityValue
**		Desc: 靛而 弊缝喊 蜡瓤扁埃 刘气 蔼阑 掘绰促.
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetParmDropGroupAmpValidityValue
AS
	SET NOCOUNT ON
	
	SELECT 
		  mDGroup
		, mChaosBattleAdvLv
		, mAmpValidityValue
	FROM	dbo.DT_DropGroupChaosBattleAmpValidity
	ORDER BY mDGroup, mChaosBattleAdvLv

GO

