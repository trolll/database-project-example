/******************************************************************************
**		Name: UspGetJackpot
**		Desc: 黎铺 沥焊甫 啊廉柯促.
**
**		Auth: 沥柳宽
**		Date: 2013-10-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013.10.15	沥柳宽				积己    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetJackpot]
	@pSvrNo		SMALLINT	-- 辑滚 锅龋	
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT [mCamp], [mStackPoint] FROM dbo.TblJackpot WHERE [mSvrNo] = @pSvrNo ORDER BY [mCamp]
			
	SET NOCOUNT OFF;

GO

