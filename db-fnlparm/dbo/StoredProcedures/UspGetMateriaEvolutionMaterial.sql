
CREATE PROCEDURE dbo.UspGetMateriaEvolutionMaterial
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		MEID,
		MType,
		MGrade,
		MLevel,
		mSuccess
	FROM
		dbo.TblMaterialEvolutionMaterial

GO

