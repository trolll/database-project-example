/******************************************************************************
**		File: UspGetTransformList.sql
**		Name: UspGetTransformList
**		Desc: 酒捞袍 沥焊, 酒捞袍 胶懦
**
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2006.11.27	JUDY				捞亥飘 傈侩 鞘靛 眠啊 
**		2006.12.18	JUDY				捞亥飘 拿烦疙 函版 棺, 扁家 咯何 拿烦 眠啊
**		2007.02.13	JUDY				胶懦 酒捞袍 鉴辑 裹困 函版	
**		2007.03.02	JUDY				蜡丰拳 咯何 扁瓷 眠啊 	
**		2007.05.17	JUDY				抛挤 咯何 眉农 
**		2007.06.12	JUDY				涅胶飘 酒捞袍 鞘夸 肮荐 
**		2007.08.24	JUDY				IIsConfirm 拿烦 眠啊 
**		2007.09.17	judy				IIsSealable
**		2007.10.05	soundkey			IAddDDWhenCritical 拿烦 眠啊
**		2007.12.10	judy				豪牢秦力俊 鞘夸茄 拿烦 肮荐 眠啊 
**		2008.03.10	JUDY				mIsPracticalPeriod <-- Pratical Item 咯何
**		2008.03.21	judy				付阑俊辑 酒捞袍阑 罐阑 荐 乐绰 可记 眠啊
**		2008.04.07	辫堡挤				眠啊等 拿烦阑 SELECT 府胶飘俊 眠啊
**		2008.04.17	judy				Support Type
**		2008.05.16	JUDY				俺牢惑痢 备盒蔼俊 蝶扼 load 贸府 函版
**		2010.02.11  辫堡挤				烹钦辨靛措傈 辑滚俊辑 荤侩啊瓷茄瘤 敲贰弊
**		2010.01.27	辫碍龋				[ITermOfValidityLv] 拿烦 眠啊
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetTransformList
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT	[mNo], [mGroupID], [mMonID], [mLevel], [mEq0], [mEq1], [mEq2], [mEq3], [mEq4], [mEq5], [mEq6], [mEq7], [mEq8], [mEq9], [mEq10], 
			[mControl]
	FROM TblTransformList

	SET NOCOUNT OFF

GO

