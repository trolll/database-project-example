/******************************************************************************    
**  Name: UspUpdateTeamRankPcInfo  
**  Desc: 评珐农 某腐磐 何啊 沥焊甫 盎脚茄促.  
**    
**  Auth: 沥柳宽  
**  Date: 2013-04-03  
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:    Description:    
**  -------- --------   ---------------------------------------    
**        
*******************************************************************************/  
CREATE PROCEDURE dbo.UspUpdateTeamRankPcInfo  
	@pSvrNo  SMALLINT, -- 辑滚 锅龋  
	@pGuildNo INT,  -- 辨靛 锅龋  
	@pGuildNm CHAR(12), -- 辨靛 疙  
	@pPcNo  INT   -- 某腐磐 锅龋  
AS    
	SET NOCOUNT ON;    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
   
	IF NOT EXISTS (SELECT * FROM dbo.TblUnitedGuildWarPcInfo WHERE mSvrNo = @pSvrNo AND mPcNo = @pPcNo)  
	BEGIN  
		RETURN(0)
	END  
   
	UPDATE dbo.TblUnitedGuildWarPcInfo  
	SET mGuildNo = @pGuildNo,  
		mGuildNm = @pGuildNm  
	WHERE mSvrNo = @pSvrNo  
	AND mPcNo = @pPcNo  
    
	IF (@@ERROR <> 0  OR @@ROWCOUNT <= 0) 
	BEGIN  
		RETURN(1) -- db error  
	END  
   
	RETURN(0)

GO

