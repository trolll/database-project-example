/******************************************************************************  
**  Name: UspGetUnitedGuildWarGuildInfo  
**  Desc: 烹钦 辨靛 措傈 辨靛 沥焊甫 啊廉柯促. 绝绰 版快 货肺 积己 饶 啊廉柯促.  
**  
**  Auth: 辫 堡挤  
**  Date: 2010-01-19  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**  2013.06.11	沥柳宽		DB 狼 捞抚苞 霸烙辑滚狼 捞抚捞 促弗版快 TblUnitedGuildWarPcInfo狼 mGuildNm阑 霸烙辑滚狼 捞抚栏肺 函版茄促.
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetUnitedGuildWarGuildInfo  
	@pSvrNo  SMALLINT,  -- 辑滚 锅龋  
	@pGuildNo INT,   -- 辨靛 锅龋  
	@pGuildNm CHAR(12)  -- 辨靛 疙  
AS  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
   
	DECLARE @aErrNo     INT,  
			@aGuildNm    CHAR(12),  
			@aGuildPoint   INT,  
			@aKillCnt    INT,  
			@aDieCnt    INT,  
			@aVictoryCnt   INT,  
			@aDrawCnt    INT,  
			@aDefeatCnt    INT,  
			@aVictoryHBT1   INT,  
			@aDefeatHBT1   INT,  
			@aVictoryHBT2   INT,  
			@aDefeatHBT2   INT,  
			@aVictoryHBT3   INT,  
			@aDefeatHBT3   INT,  
			@aRecentlyMatchSvrNo SMALLINT,  
			@aRecentlyMatchGuildNm CHAR(12),  
			@aRecentlyMatchResult TINYINT;  
     
	-- 扁夯 函荐 汲沥  
	SELECT  @aErrNo = 0, @aGuildPoint = 0, @aKillCnt = 0, @aVictoryCnt = 0, @aDrawCnt = 0, @aDefeatCnt = 0,   
	@aVictoryHBT1 = 0, @aDefeatHBT1 = 0, @aVictoryHBT2 = 0, @aDefeatHBT2 = 0,@aVictoryHBT3 = 0, @aDefeatHBT3 = 0,  
	@aRecentlyMatchSvrNo = 0, @aRecentlyMatchGuildNm = '', @aRecentlyMatchResult = 3;  
   
	-- 单捞磐 八祸  
	SELECT  
		@aGuildNm     = [mGuildNm],  
		@aGuildPoint   = [mGuildPoint],  
		@aKillCnt    = [mKillCnt],  
		@aDieCnt    = [mDieCnt],  
		@aVictoryCnt   = [mVictoryCnt],  
		@aDrawCnt    = [mDrawCnt],  
		@aDefeatCnt    = [mDefeatCnt],  
		@aVictoryHBT1   = [mVictoryHerosBT1],  
		@aDefeatHBT1   = [mDefeatHerosBT1],  
		@aVictoryHBT2   = [mVictoryHerosBT2],  
		@aDefeatHBT2   = [mDefeatHerosBT2],  
		@aVictoryHBT3   = [mVictoryHerosBT3],  
		@aDefeatHBT3   = [mDefeatHerosBT3],  
		@aRecentlyMatchSvrNo = [mRecentlyMatchSvrNo],  
		@aRecentlyMatchGuildNm = [mRecentlyMatchGuildNm],  
		@aRecentlyMatchResult = [mRecentlyMatchResult]  
	FROM dbo.TblUnitedGuildWarGuildInfo  
	WHERE [mSvrNo] = @pSvrNo   
	AND [mGuildNo] = @pGuildNo;  
   
	-- 单捞磐啊 粮犁窍瘤 臼绰 版快  
	IF (0 = @@ROWCOUNT)
	BEGIN  
    
		-- 货肺款 单捞磐甫 积己茄促.  
		EXEC @aErrNo = dbo.UspCreateUnitedGuildWarGuildInfo @pSvrNo, @pGuildNo, @pGuildNm;  
   
		IF(0 <> @aErrNo)  
		BEGIN  
			RETURN(@aErrNo);  
		END  
   
		-- 积己等 单捞磐甫 官帕栏肺 促矫 八祸  
		SELECT  
		@aGuildNm     = [mGuildNm],  
		@aGuildPoint   = [mGuildPoint],  
		@aKillCnt    = [mKillCnt],  
		@aDieCnt    = [mDieCnt],  
		@aVictoryCnt   = [mVictoryCnt],  
		@aDrawCnt    = [mDrawCnt],  
		@aDefeatCnt    = [mDefeatCnt],  
		@aVictoryHBT1   = [mVictoryHerosBT1],  
		@aDefeatHBT1   = [mDefeatHerosBT1],  
		@aVictoryHBT2   = [mVictoryHerosBT2],  
		@aDefeatHBT2   = [mDefeatHerosBT2],  
		@aVictoryHBT3   = [mVictoryHerosBT3],  
		@aDefeatHBT3   = [mDefeatHerosBT3],  
		@aRecentlyMatchSvrNo = [mRecentlyMatchSvrNo],  
		@aRecentlyMatchGuildNm = [mRecentlyMatchGuildNm],  
		@aRecentlyMatchResult = [mRecentlyMatchResult]  
		FROM dbo.TblUnitedGuildWarGuildInfo  
		WHERE [mSvrNo] = @pSvrNo   
		AND [mGuildNo] = @pGuildNo;
	END  
   
	-- DB 狼 捞抚苞 霸烙辑滚狼 捞抚捞 促弗版快 霸烙辑滚狼 捞抚栏肺 DB 狼 单捞磐甫 函版茄促.  
	IF (@pGuildNm <> @aGuildNm)  
	BEGIN  
  
		UPDATE dbo.TblUnitedGuildWarGuildInfo   
		SET [mGuildNm] = @pGuildNm   
		WHERE [mSvrNo] = @pSvrNo   
		AND [mGuildNo] = @pGuildNo;  
    
		IF(0 <> @@ERROR)  
		BEGIN  
			RETURN(2); -- DB ERROR;  
		END
   
		UPDATE dbo.TblUnitedGuildWarPcInfo
		SET [mGuildNm] = @pGuildNm
		WHERE [mSvrNo] = @pSvrNo   
		AND [mGuildNo] = @pGuildNo;

	END  
   
	-- 搬苞 免仿  
	SELECT @aGuildPoint, @aKillCnt, @aDieCnt, @aVictoryCnt, @aDrawCnt, @aDefeatCnt, @aVictoryHBT1, @aDefeatHBT1, @aVictoryHBT2,   
	@aDefeatHBT2, @aVictoryHBT3, @aDefeatHBT3, @aRecentlyMatchSvrNo, @aRecentlyMatchGuildNm, @aRecentlyMatchResult;  
     
	RETURN(@aErrNo);

GO

