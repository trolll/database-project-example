/******************************************************************************
**		Name: WspInsCalendarEvent
**		Desc: ÀÌº¥Æ® ´Þ·Â µî·Ï
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspInsCalendarEvent] 
    @pTitle				VARCHAR(30),
    @pDesc				VARCHAR(512),
	@pURL				VARCHAR(200),
	@pEventNo			INT	= NULL,
	@pEventNoOutput		INT	OUTPUT
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0

	IF (@pEventNo IS NULL OR @pEventNo= 0)	
	BEGIN
		EXEC @pEventNo = dbo.WspGetSequence 'TblCalendarEvent', 'mEventNo'
		
		IF (@pEventNo = 0 OR @pEventNo < 0)
		BEGIN
			RETURN (1)	-- sql error 
		END 
	END 
	
	UPDATE dbo.TblCalendarEvent
	SET    mTitle = @pTitle
		  ,mDesc = @pDesc
		  ,mURL = @pURL
	WHERE  mEventNo = @pEventNo
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF (@aRowCnt <= 0)
	BEGIN
		INSERT INTO dbo.TblCalendarEvent (mEventNo, mTitle, mDesc, mURL)
		SELECT @pEventNo, @pTitle, @pDesc, @pURL

		IF (@@ERROR <> 0)
		BEGIN
			RETURN (1)
		END
	END 
	
	SET @pEventNoOutput = @pEventNo			
	RETURN(0)	-- none error

GO

