/******************************************************************************
**		Name: UspGetParmEventQuestResultDraw
**		Desc: 捞亥飘 涅胶飘 惑磊 瘤鞭 犬伏
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmEventQuestResultDraw]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT   
		 mMonClass
		, mFair
		, mGood
		, mExcellent
	FROM dbo.TblEventQuestResultDraw

GO

