/******************************************************************************  
**  Name: UspListBead
**  Desc: 备浇狼 何啊 沥焊 府胶泼
**  
**                
**  Return values:  
**   饭内靛悸
**                
**  Author: 辫碍龋
**  Date: 2011-06-24
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2012.10.01  巢扁豪     [mItemSubType] 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspListBead]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT IID, mTargetIPos, mProb, mGroup, mItemSubType
	FROM dbo.DT_Bead
END

GO

