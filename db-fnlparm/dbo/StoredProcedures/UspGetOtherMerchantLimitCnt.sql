/******************************************************************************
**		Name: UspGetOtherMerchantLimitCnt
**		Desc: ÀÌ°è »óÀÎ ¼ÒÈ¯ Á¦ÇÑ È½¼ö
**
**		Auth: Á¤ÁøÈ£
**		Date: 2015-10-16
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetOtherMerchantLimitCnt]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT
		mMerchantID,
		mMaxSummonCnt,
		mMaxBuyItemCnt
	FROM dbo.TblOtherMerchantInfo
	
	SET NOCOUNT OFF

GO

