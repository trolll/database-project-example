CREATE PROCEDURE [dbo].[UspGetParmRacing]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT mNID, mPlace, RTRIM(mNpcNm), mNpcNo, mAbility from DT_Racing
		
	SET NOCOUNT OFF

GO

