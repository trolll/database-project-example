/******************************************************************************
**		Name: WspUptCalendarEvent
**		Desc: ÀÌº¥Æ® ´Þ·Â ¾÷µ¥ÀÌÆ®
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROC [dbo].[WspUptCalendarEvent] 
    @pEventNo		int,
    @pTitle			varchar(30),
	@pDesc			varchar(512),
	@pURL			varchar(200)
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblCalendarEvent
	SET    mTitle = @pTitle, mDesc = @pDesc, mURL = @pURL
	WHERE  mEventNo = @pEventNo

	IF @@ERROR <> 0	
		RETURN(1)

	RETURN(0)

GO

