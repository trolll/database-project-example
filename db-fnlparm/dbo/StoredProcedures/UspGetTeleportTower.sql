/******************************************************************************
**		File: 
**		Name: UspGetTeleportTower
**		Desc: 이동포탈 정보를 로드한다.
**
**		Auth: 김 광섭
**		Date: 2009-03-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetTeleportTower]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  mNo
		, mPosX
		, mPosY
		, mPosZ
		, mWidth
		, mDir
		, mDesc
	FROM	dbo.TblTeleportTower

GO

