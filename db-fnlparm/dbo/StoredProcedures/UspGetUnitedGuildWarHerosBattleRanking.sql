/******************************************************************************    
**  Name: UspGetUnitedGuildWarHerosBattleRanking    
**  Desc: 老扁配 珐欧 沥焊甫 掘绰促.    
**    
**  Auth: 辫 堡挤    
**  Date: 2010-01-19    
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:  Description:    
**  -------- -------- ---------------------------------------    
**  2010-03-29 辫堡挤  辨靛疙俊 RTRIM 眠啊    
**	2010-04-08 辫堡挤  mPcNm TRIM 眠啊    
**	2013-04-04 沥柳宽  [mGuildNo], [mPcNo] 拿烦眠啊  
*******************************************************************************/    
CREATE PROCEDURE dbo.UspGetUnitedGuildWarHerosBattleRanking    
AS    
	SET NOCOUNT ON;    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
     
	SELECT    
		mSvrNo   
		, mRanking 
		, mGuildNo
		, RTRIM(mGuildNm)    
		, mVictoryCnt
		, mPcNo
		, RTRIM(mPcNm)    
	FROM dbo.TblUnitedGuildWarHerosBattleRanking    
	ORDER BY mRanking ASC

GO

