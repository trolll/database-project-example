CREATE PROCEDURE [dbo].[UspListSealStone]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT [mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc]
		FROM TblCastleSealStone
	SET NOCOUNT OFF

GO

