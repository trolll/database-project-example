/******************************************************************************
**		Name: dbo.UspGetParmRefine
**		Desc: °­È­ Å×ÀÌºí ·Îµå
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.12.02	°ø¼®±Ô				°­È­¿¡ ½ÇÆÐÇÏ¿© ÆÄ±«½Ã ºÒ¿ÏÀüÇÑ ½Å¼ºÂü
**										¼ÒÀ¯ÇÏ°í ÀÖÀ»°æ¿ì ÀÏÁ¤ È®·ü·Î -1µÊ
**										½ÇÆÐ½Ã µ¹¾Æ°¥ -1¾ÆÀÌÅÛ(RBeforeItemID)À» ·Îµå
**		2012.09.10  Á¤ÁøÈ£				RIsCreateCnt °ªÀÌ 0ÀÎ°Í(Á¦ÀÛÀÌ¾Æ´Ñ°Í)¸¸ ·ÎµåÇÏµµ·Ï ¼öÁ¤
**										Á¦·Ã°ú Á¦ÀÛÀ» ±¸ºÐÇÑ´Ù.
**		2016.01.12  Á¤Áø¿í				IType 9¹ø Ãß°¡,
**										ÀÎÃ¾Æ® ÀÌº¥Æ® ÄÁÅÙÃ÷ ON µÇ¾îÀÖÀ»¶§
**										+0¿¡¼­ +1·Î 100% ¼º°øÈ®·üÀÌ ¾Æ´Ñ ¾ÆÀÌÅÛÀ» °­È­¿¡ ½ÇÆÐÇØ¼­
**										Àç·á¾ÆÀÌÅÛÀ¸·Î µ¹¾Æ°¡Áö ¸øÇÏ°Ô ¿¹¿ÜÃ³¸®
**										IID : 7658,7663,7668,7673,7678 ¾ÆÀÌÅÛ RBeforeItemID¸¦ NULL·Î ¸¸µë
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmRefine]
AS
	SET NOCOUNT ON;

	SELECT a.RID, b.RItemID, b.RNum
		 , a.RItemID0, a.RItemID1, a.RItemID2
		 , a.RItemID3, a.RItemID4, a.RItemID5
		 , a.RItemID6, a.RItemID7, a.RItemID8
		 , a.RItemID9, a.RSuccess, a.RBeforeItemID
	FROM
	(
		SELECT t1.RID, t2.RItemID as 'RBeforeItemID', t1.RItemID, t1.RNum
			 , t1.RItemID0, t1.RItemID1, t1.RItemID2
			 , t1.RItemID3, t1.RItemID4, t1.RItemID5
			 , t1.RItemID6, t1.RItemID7, t1.RItemID8
			 , t1.RItemID9, t1.RSuccess, t1.RIsCreateCnt
		FROM
		(
			SELECT r.RID, r.RItemID0, r.RItemID1
				 , r.RItemID2, r.RItemID3, r.RItemID4
				 , r.RItemID5, r.RItemID6, r.RItemID7
				 , r.RItemID8, r.RItemID9, r.RSuccess
				 , t.RItemID, t.RNum, r.RIsCreateCnt
			FROM dbo.DT_Refine AS r LEFT OUTER JOIN
			(
				SELECT m.RID, m.RItemID, m.RNum
				FROM dbo.DT_RefineMaterial AS m INNER JOIN dbo.DT_ITEM AS i ON (m.RItemID = i.IID)
				WHERE i.IType IN (1, 2, 3, 6, 7, 8, 9, 17, 18, 20)
				  AND i.ITermOfValidity = 10000
			) AS t ON ( r.RID = t.RID )
		) AS t1 LEFT OUTER JOIN
		(
			SELECT r.RItemID0, m.RItemID, m.RNum
			FROM dbo.DT_Refine r INNER JOIN dbo.DT_RefineMaterial m ON (r.RID = m.RID) 
							 INNER JOIN dbo.DT_ITEM i ON (m.RItemID = i.IID)
			WHERE i.IType IN (1, 2, 3, 6, 7, 8, 9, 17, 18, 20)
			  AND i.ITermOfValidity = 10000
			  AND r.RItemID0 NOT IN(7658,7663,7668,7673,7678)
			GROUP BY r.RItemID0, m.RItemID, m.RNum
		) AS t2 ON (t1.RItemID = t2.RItemID0)
	) AS a INNER JOIN dbo.DT_RefineMaterial AS b ON (a.RID = b.RID)
	WHERE a.RIsCreateCnt < 1
	ORDER BY a.RID ASC, b.RItemID DESC;
		
	SET NOCOUNT OFF;

GO

