/******************************************************************************  
**  Name: UspGetParmItemMaxStack
**  Desc: 酒捞袍狼 胶琶 咯何甫 馆券茄促.
**  
**                
**  Author: 辫碍龋  
**  Date: 2011-02-08
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetParmItemMaxStack
 @pItemNo INT		-- 酒捞袍 锅龋
 ,@pMaxStack SMALLINT OUTPUT
AS 
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
	
	SELECT	@pMaxStack=IMaxStack 
	FROM dbo.DT_item 
	WHERE IID =  @pItemNo;

	IF @pMaxStack IS NULL
	BEGIN
		SET @pMaxStack = 0;
	END

	RETURN(0);

GO

