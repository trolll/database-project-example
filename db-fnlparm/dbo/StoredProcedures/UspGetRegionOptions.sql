CREATE PROCEDURE [dbo].[UspGetRegionOptions]
AS
	SET NOCOUNT ON;

	SELECT
		  mPlace
		, mIsSupport
		, mExpRate
		, mMonsterItemDropRate
		, mShowPlayerName

		, mMonSilverDropRate
		, mNoDropItemOnDeath
		, mNoExpDescOnDeath	
	FROM
		dbo.TblRegionOptions

GO

