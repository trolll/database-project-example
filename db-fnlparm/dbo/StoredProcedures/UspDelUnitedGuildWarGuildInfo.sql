/******************************************************************************
**		Name: UspDelUnitedGuildWarGuildInfo
**		Desc: 烹钦 辨靛 措傈 辨靛 沥焊甫 昏力茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspDelUnitedGuildWarGuildInfo
	@pSvrNo					SMALLINT,	-- 辑滚 锅龋
	@pGuildNo				INT			-- 辨靛 锅龋
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DELETE	dbo.TblUnitedGuildWarGuildInfo  
	WHERE [mSvrNo] = @pSvrNo 
		AND [mGuildNo] = @pGuildNo;

GO

