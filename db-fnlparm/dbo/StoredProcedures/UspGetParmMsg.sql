CREATE PROCEDURE dbo.UspGetParmMsg
AS	
	SET NOCOUNT ON
	SELECT 
		mMsgGroupID
		, mHashID
		, mHashStr
		, mDesc 
	FROM dbo.TblMsg
	WHERE mMsgGroupID < 20000

GO

