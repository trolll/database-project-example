/******************************************************************************
**		Name: UspIsInvalidCharNm
**		Desc: 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20150918	Á¤Áø¿í				ÀÌ¿ë ºÒ°¡´ÉÇÑ Ä³¸¯ÅÍ ¸í + ÇÑ±Û, ¿µ¹®, ¼ýÀÚ Á¶ÇÕÀÇ °æ¿ì Ä³¸¯ÅÍ »ý¼º ºÒ°¡´É ¹× ÀÌ¸§ º¯°æÀÌ ºÒ°¡´ÉÇÏµµ·Ï ¼öÁ¤
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspIsInvalidCharNm]
	 @pCharNm	CHAR(12)
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aCharNm	VARCHAR(12)
	SET @aCharNm = RTRIM(@pCharNm)
	
	DECLARE	@aCnt		INT
	SET @aCnt = dbo.UfnIsInvalidString(@aCharNm)
	IF(0 = @aCnt)
	 BEGIN
		-- [String]°¡ VARCHARÀÌ ¾Æ´Ñ CHARÀÌ¶ó¸é RTRIM()À» ÇØ¾ß ÇÑ´Ù.
		--SELECT @aCnt=COUNT(*) FROM TblInvalidCharNm WHERE @aCharNm LIKE '%'+[mString]+'%'
		--SELECT @aCnt=COUNT(*) FROM TblInvalidCharNm WHERE @aCharNm = [mString]
		SELECT @aCnt=COUNT(*) FROM dbo.TblInvalidCharNm WHERE @aCharNm LIKE [mString]+'%'
	 END
	 
	RETURN @aCnt

GO

