/******************************************************************************
**		Name: UspGetParmDropGroup 
**		Desc: 
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2009-03-20	김 광섭				SELECT 항목 추가
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetParmDropGroup
AS
	SET NOCOUNT ON
	
	SELECT a.[DGroup], a.[DDrop], a.[DPercent], RTRIM(b.[DName]), b.[DDropType]
	FROM DT_DropGroup AS a 
		INNER JOIN TP_DropGroup AS b 
			ON(a.[DGroup] = b.[DGroup])
	ORDER BY a.[DGroup]
		
	SET NOCOUNT OFF

GO

