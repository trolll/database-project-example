/******************************************************************************
**		Name: UspGetAchieveCoinItem
**		Desc: 林拳 酒捞袍 沥焊**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveCoinItem]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT
		 IID
		, mGrade
		, mRarity
	FROM dbo.DT_AchieveItemCoin

GO

