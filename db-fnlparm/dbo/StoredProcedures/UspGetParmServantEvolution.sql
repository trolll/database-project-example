/******************************************************************************
**		Name: UspGetParmServantEvolution
**		Desc: ¼­¹øÆ® ¾ÆÀÌÅÛ Å¸ÀÔ ¾ò±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantEvolution]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT IID, STID1, STID2, RID
	FROM dbo.TblServantEvolution
	ORDER BY IID, STID1, STID2

	SET NOCOUNT OFF;

GO

