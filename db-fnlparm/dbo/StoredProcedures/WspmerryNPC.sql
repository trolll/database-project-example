CREATE PROCEDURE WspmerryNPC
	 @status		int

as
set nocount on 
if (@status=1)   --close 메리 NPC 안나오게
begin
UPDATE DT_Monster
SET mNationOp = 1152921504606846971
WHERE MID = 609
--NPC 위치변경(이벤트 서버)
update tblMonsterSpotGroup
set mPosX=359877, mPosY=15251, mPosZ=295390
where mGid = 16349

update tblMonsterSpotGroup
set mPosX=359444, mPosY=15211, mPosZ=293527
where mGid = 16351

update tblMonsterSpotGroup
set mPosX=357677, mPosY=15252, mPosZ=292504
where mGid = 16347

update tblMonsterSpotGroup
set mPosX=355748, mPosY=15262, mPosZ=293025
where mGid = 16350

end

if (@status=2)    --open(recovery)  메리 NPC복구
begin
UPDATE DT_Monster
SET mNationOp = 1152921504606846975
WHERE MID = 609

update tblMonsterSpotGroup
set mPosX=265286.375, mPosY=20446.041015999999, mPosZ=220177.625
where mGid = 16349

update tblMonsterSpotGroup
set mPosX=261855.859375, mPosY=20179.199218999998, mPosZ=224588.53125
where mGid = 16351

update tblMonsterSpotGroup
set mPosX=261635.640625, mPosY=20384.324218999998, mPosZ=221387.234375
where mGid = 16347

update tblMonsterSpotGroup
set mPosX=265412.09375, mPosY=20358.623047000001, mPosZ=222736.015625
where mGid = 16350
end

set nocount off

GO

