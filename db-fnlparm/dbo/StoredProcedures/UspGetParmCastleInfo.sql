CREATE PROCEDURE [dbo].[UspGetParmCastleInfo]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT mID, RTRIM(mName)
		FROM tblCastleInfo
		
	SET NOCOUNT OFF

GO

