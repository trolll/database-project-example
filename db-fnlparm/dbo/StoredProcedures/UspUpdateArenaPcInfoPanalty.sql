/******************************************************************************
**		Name: UspUpdateArenaPcInfoPanalty
**		Desc: 酒饭唱 PC沥焊 诀单捞飘
**
**		Auth: 沥柳龋
**		Date: 2016-09-029
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateArenaPcInfoPanalty]
	@pSvrNo				SMALLINT,	-- 辑滚 锅龋    
	@pPcNo					INT,		-- 某腐磐 锅龋    
	@pPanalty				INT			-- 菩澄萍 汲沥
AS    
	SET NOCOUNT ON;    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    

    DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT

	IF NOT EXISTS (SELECT * 
				FROM dbo.TblArenaPcInfo 
				WHERE mSvrNo = @pSvrNo AND mPcNo = @pPcNo)
	BEGIN
		INSERT INTO dbo.TblArenaPcInfo (mRegDate, mSvrNo, mPcNo, mPlayTime, mPanalty)
		VALUES (GETDATE(), @pSvrNo, @pPcNo, 0, @pPanalty)
	END
	
	UPDATE dbo.TblArenaPcInfo 
	SET mPanalty = 
		CASE
			WHEN @pPanalty <> 0
				THEN mPanalty + @pPanalty
			ELSE mPanalty
		END 
		,mRegDate = 
		CASE
			WHEN @pPanalty = 1
				THEN GETDATE()
			ELSE mRegDate
		END 
	WHERE mSvrNo = @pSvrNo AND mPcNo = @pPcNo
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

