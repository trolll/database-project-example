CREATE PROCEDURE [dbo].[UspGetParmAttributeDice]
	 @pFlag		INT		-- EParmFlag.
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	IF(0 = @pFlag)		-- 정상
	 BEGIN
		SELECT a.[AID], a.[AType], a.[ALevel], a.[ADiceDamage], RTRIM(b.[AName]), a.[ADamage]
			FROM DT_Attribute AS a INNER JOIN TP_AttributeType AS b ON(a.[AType] = b.[AType])
	 END
	ELSE IF(1 = @pFlag)	-- 가중
	 BEGIN
		SELECT a.[AID], a.[AType], a.[ALevel], a.[ADiceDamage], RTRIM(b.[AName]), a.[ADamage]
			FROM DT_AttributeAdd AS a INNER JOIN TP_AttributeType AS b ON(a.[AType] = b.[AType])
	 END
	ELSE IF(2 = @pFlag)	-- 저항
	 BEGIN
		SELECT a.[AID], a.[AType], a.[ALevel], a.[ADiceDamage], RTRIM(b.[AName]), a.[ADamage]
			FROM DT_AttributeResist AS a INNER JOIN TP_AttributeType AS b ON(a.[AType] = b.[AType])	 
	 END
		
	SET NOCOUNT OFF

GO

