/******************************************************************************
**		Name: UspGetExpireQuest
**		Desc: 老老 力芭且 涅胶飘 沥焊甫 馆券
**
**		Auth: 沥柳龋
**		Date: 2010.04.05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetExpireQuest]  	
	@pSvrNo				SMALLINT
	,@pDate				SMALLDATETIME	
	,@pArrQuest			VARCHAR(3000) OUTPUT
	,@aSupportTime		INT OUTPUT
	,@aLastTime			INT OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aSupportDay INT;
	SET DATEFIRST 7; -- 老夸老何磐 矫累

	SELECT @aSupportDay = DATEPART(dw, DATEADD(DD, -1, @pDate)) 
			, @pArrQuest = ''	
			, @aSupportTime = 0
			, @aLastTime = 0;

	SELECT 
		@pArrQuest = CONVERT(VARCHAR, mQuestNo) + ',' + @pArrQuest
	FROM dbo.TblQuestInfo
	WHERE mType = 2 -- 扁埃 馆汗 涅胶飘
		AND (mParmA = @aSupportDay OR mParmA = 99 ); -- 秦寸夸老 捞芭唱 概老檬扁拳 眉农


	SELECT		-- 辑滚辆丰矫埃苞 矫累矫埃狼 瞒捞甫 备茄促.
			@aSupportTime = DATEDIFF( hh, mLastSupportDate, GETDATE() )
	FROM dbo.TblParmSvr
	WHERE mSvrNo = @pSvrNo

	SELECT @aLastTime = DATEPART(hh, GETDATE()) - @aSupportTime;

GO

