/******************************************************************************
**		Name: UspUpdatePopupGuideCondition
**		Desc: TblPopupGuideCondition 狼 诀单捞飘
**
**		Auth: 辫锐档
**		Date: 2009-10-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePopupGuideCondition]
	@pConID		INT,
	@pGuideNo	INT,
	@pConType	INT,
	@pAParm		INT,
	@pBParm		INT,
	@pCParm		INT,
	@pReadCnt	INT,
	@pForce		BIT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE @aErrNo		INT
	SET		@aErrNo		= 0
	 
	----------------------------------------------
	-- 粮犁窍瘤 臼促搁 INSERT 秦 霖促.
	----------------------------------------------
	IF EXISTS(SELECT * FROM TblPopupGuideCondition WHERE [mConID] = @pConID)
		BEGIN
			UPDATE dbo.TblPopupGuideCondition
			SET
				[mGuideNo]	= @pGuideNo,
				[mConType]	= @pConType,
				[mAParm]	= @pAParm,
				[mBParm]	= @pBParm,
				[mCParm]	= @pCParm,
				[mReadCnt]	= @pReadCnt,
				[mForce]	= @pForce
			WHERE [mConID] = @pConID
		END
	ELSE
		BEGIN
				INSERT INTO dbo.TblPopupGuideCondition([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce])
					VALUES(@pConID, @pGuideNo, @pConType, @pAParm, @pBParm, @pCParm, @pReadCnt, @pForce)
		END
		
	IF(@@ROWCOUNT <> 1)
		BEGIN
			SET @aErrNo = 1
			GOTO LABEL_END
		END

LABEL_END:
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

