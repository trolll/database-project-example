/******************************************************************************
**		Name: WspInsEventQuestCondition
**		Desc: 捞亥飘 涅胶飘 炼扒 殿废
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROC [dbo].[WspInsEventQuestCondition] 
    @pEQuestNo	INT,
	@pType		TINYINT,
	@pParmID	INT, 
    @pParmA		INT,
	@pParmB		INT,
	@pParmC		INT,
	@pParmD		INT
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	INSERT INTO dbo.TblEventQuestCondition (mEQuestNo, mType, mParmID, mParmA, mParmB, mParmC, mParmD)
	SELECT @pEQuestNo, @pType, @pParmID, @pParmA, @pParmB, @pParmC, @pParmD
	
	IF (@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

