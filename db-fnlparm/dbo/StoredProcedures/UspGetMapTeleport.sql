CREATE PROCEDURE [dbo].[UspGetMapTeleport]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT [mNo], [mPos1x], [mPos1y], [mPos1z], [mPos1Range], [mPos2x], [mPos2y], [mPos2z], [mPos2Range], [mDesc]
		FROM TblMapTeleport
	SET NOCOUNT OFF

GO

