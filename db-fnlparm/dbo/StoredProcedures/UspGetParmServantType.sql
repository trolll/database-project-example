/******************************************************************************
**		Name: UspGetParmServantType
**		Desc: ¼­¹øÆ® ¾ÆÀÌÅÛ Å¸ÀÔ ¾ò±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20151013	ÀÌ¿ëÁÖ				´ëºÐ·ù(SCategory) Ãß°¡
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantType]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT IID, SCategory, SEvolutionStep, TP_Servant.SType
	FROM dbo.TblServantType INNER JOIN dbo.TP_Servant ON TblServantType.SType = TP_Servant.SType
	ORDER BY SCategory, SEvolutionStep, TP_Servant.SType

	SET NOCOUNT OFF;

GO

