/******************************************************************************
**		Name: WspGetDropItemProbabilityChange
**		Desc: µå·Ó ¾ÆÀÌÅÛ È®·ü º¯°æ
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-08-13
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE dbo.WspGetDropItemProbabilityChange
	@pSvrNo SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		T1.mItemID
		,T2.IName
		,T1.mDGroup
		,T1.mMultiplication
		,T1.mAddition
	FROM dbo.TblDropItemProbabilityChange AS T1
		INNER JOIN dbo.DT_Item AS T2
	ON T1.mItemID = T2.IID
	WHERE T1.mSvrNo = @pSvrNo

GO

