/******************************************************************************  
**  File: UspGetSpotAttackerSector.sql
**  Name: UspGetSpotAttackerSector
**  Desc: ½ºÆÌ ½À°ÝÀÚ ¼½ÅÍÁ¤º¸
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2015.05.11	°ø¼®±Ô		»ý¼º
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetSpotAttackerSector]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		mSpotPlace,
		mStartX,
		mStartZ,
		mEndX,
		mEndZ
	FROM dbo.TblSpotAttackerSector
	
	SET NOCOUNT OFF

GO

