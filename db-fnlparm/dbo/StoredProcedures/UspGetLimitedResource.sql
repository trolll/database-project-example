/******************************************************************************  
**  File: UspGetLimitedResource.sql  
**  Name: UspGetLimitedResource  
**  Desc: 府家胶酒捞袍阑 啊廉柯促  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**	2012.06.11	沥柳龋		mIsIncSys 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetLimitedResource]
AS
	SET NOCOUNT ON
	
	SELECT 
		mRegDate,
		mResourceType,
		mMaxCnt,
		mRemainerCnt,
		mRandomVal,
		mUptDate,	
		mIsIncSys
	FROM dbo.TblLimitedResource

GO

