/******************************************************************************
**		Name: UspGetArenaBossGradeProb
**		Desc: 焊胶傈 焊胶 家券殿鞭 犬伏阑 啊廉柯促.
**
**		Auth: 沥柳龋
**		Date: 2016-09-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaBossGradeProb]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT   
			 mPointGrade
			,mMinPoint
			,mMaxPoint
			,mGrade1st
			,mGrade2nd
			,mGrade3rd
			,mGrade4th
	FROM
		dbo.TblArenaBossGradeProb

GO

