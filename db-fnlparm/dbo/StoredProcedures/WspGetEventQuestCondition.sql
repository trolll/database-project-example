/******************************************************************************
**		Name: WspGetEventQuestCondition
**		Desc: 捞亥飘 涅胶飘 炼扒
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetEventQuestCondition] 
    @pEQuestNo	INT,
	@pType		TINYINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	SELECT
		mEQuestNo
		,mType
		,mParmID
		,mParmA
		,mParmB
		,mParmC
		,mParmD
		,mParmnm = 
			CASE
				WHEN mType = 1 THEN 
					(	SELECT MName
						FROM dbo.DT_Monster
						WHERE MID = T1.mParmID )
				ELSE
					(	SELECT mPlaceNm
						FROM dbo.TblPlaceInfo
						WHERE mPlaceNo = T1.mParmID )
			END 		
	FROM dbo.TblEventQuestCondition AS T1
	WHERE mEQuestNo = @pEQuestNo
	AND mType = @pType

GO

