CREATE     PROCEDURE [dbo].[WspGetItemNameByTid]
	@IID	INT	-- TID
AS

	SELECT
		IName
	FROM	
		[dbo].[DT_Item]
	WITH (NOLOCK)
	WHERE
		IID = @IID

GO

