/******************************************************************************
**		Name: UspGetAchieveGuildPoint
**		Desc: 辨靛 珐欧喊 器牢飘客 珐欧喊 锐蓖档 犬伏**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveGuildPoint]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mRank
		, mPoint
		, mChoiceProb
		, mLegendProb
		, mEpicProb
		, mRareProb
		, mNormalProb
	FROM dbo.TP_AchieveGuildPoint

GO

