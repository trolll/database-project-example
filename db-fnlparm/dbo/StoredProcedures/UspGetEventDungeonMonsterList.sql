/******************************************************************************  
**  File: UspGetEventDungeonMonsterList.sql
**  Name: UspGetEventDungeonMonsterList 
**  Desc: 捞亥飘 带傈 瘤开狼 阁胶磐府胶飘 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2011.11.14	kirba		积己
**	2016.04.18	傍籍痹		[mCamp], [mIsBoss] 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetEventDungeonMonsterList]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT DISTINCT mMID, mCamp, mIsBoss
	FROM dbo.TblEventDungeonMonsterList
	
	SET NOCOUNT OFF

GO

