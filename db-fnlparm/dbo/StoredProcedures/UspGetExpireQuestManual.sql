/******************************************************************************
**		Name: UspGetExpireQuestManual
**		Desc: 老老 力芭且 涅胶飘 沥焊甫 馆券 (荐悼焊惑老版快父 荤侩)
**
**		Auth: 沥柳龋
**		Date: 2011.04.08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetExpireQuestManual]  	
	@pDate				SMALLDATETIME	
	,@pArrQuest			VARCHAR(3000) OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aSupportDay INT;
	SET DATEFIRST 7; -- 老夸老何磐 矫累

	SELECT @aSupportDay = DATEPART(dw, DATEADD(DD, -1, @pDate)) 
			, @pArrQuest = '';

	SELECT 
		@pArrQuest = CONVERT(VARCHAR, mQuestNo) + ',' + @pArrQuest
	FROM dbo.TblQuestInfo
	WHERE mType = 2 -- 扁埃 馆汗 涅胶飘
		AND (mParmA = @aSupportDay OR mParmA = 99); -- 秦寸夸老 捞芭唱 概老檬扁拳 眉农

GO

