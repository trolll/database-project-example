/******************************************************************************
**		Name: UspUpdateJackpot
**		Desc: 黎铺 沥焊甫 盎脚茄促.
**
**		Auth: 沥柳宽
**		Date: 2013-10-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013.10.15	沥柳宽				积己	   
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateJackpot]
	@pSvrNo		SMALLINT	-- 辑滚 锅龋
	,@pCamp		TINYINT		-- 柳康
	,@pPoint	INT			-- 函版且 器牢飘	
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE      @aErrNo      INT
				 , @aRowCnt INT

	SELECT @aErrNo = 0
				 , @aRowCnt = 0

	UPDATE dbo.TblJackpot SET [mStackPoint] = @pPoint WHERE [mSvrNo] = @pSvrNo AND [mCamp] = @pCamp;

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

	IF (@aErrNo <> 0)
	BEGIN
		   SET    @aErrNo = 1; -- DB ERROR;
	END

	IF (@aRowCnt = 0)
	BEGIN
		   -- 货肺款单捞磐甫积己茄促.
		   EXEC @aErrNo = dbo.UspCreateJackpot @pSvrNo, @pCamp;

		   IF(0 <> @aErrNo)
		   BEGIN
				 RETURN(@aErrNo);
		   END
	END

	RETURN(@aErrNo);

GO

