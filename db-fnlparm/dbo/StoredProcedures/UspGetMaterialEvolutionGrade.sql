/******************************************************************************
**		Name: UspGetMaterialEvolutionGrade
**		Desc: 概磐府倔 柳拳 矫 殿鞭俊 蝶弗 刘啊蔼阑 肺靛茄促.
**
**		Auth: 沥备柳
**		Date: 2009-06-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩     
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMaterialEvolutionGrade]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		MGrade, MProbabilityAdd
	FROM 
		dbo.TblMaterialEvolutionGrade;

GO

