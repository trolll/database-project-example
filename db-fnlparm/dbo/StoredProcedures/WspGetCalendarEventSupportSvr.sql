/******************************************************************************
**		Name: WspGetCalendarEventSupportSvr
**		Desc: ÀÌº¥Æ® ´Þ·Â Áö¿ø ¼­¹ö
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetCalendarEventSupportSvr]
	@pEventNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T1.mSvrNo
		,T1.mDesc as mSvrNm
		,T2.mBeginDate
		,T2.mEndDate
		,mIsRunning =
			CASE WHEN mBeginDate <= GETDATE() AND mEndDate >= GETDATE() THEN 0
				ELSE 1
			END
	FROM dbo.TblParmSvr T1
		LEFT OUTER JOIN 
			(
				SELECT 
					mSvrNo
					,mBeginDate
					,mEndDate
				FROM dbo.TblParmSvrSupportCalendarEvent
				WHERE mEventNo = @pEventNo
			) T2
			ON T1.mSvrNo = T2.mSvrNo
	WHERE mType = 1
	ORDER BY T1.mSvrNo

	RETURN

GO

