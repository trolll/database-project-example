/******************************************************************************
**		Name: UspCreateUnitedGuildWarGuildInfo
**		Desc: 烹钦 辨靛 措傈 辨靛 沥焊甫 积己茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspCreateUnitedGuildWarGuildInfo
	@pSvrNo		SMALLINT,		-- 辑滚 锅龋
	@pGuildNo	INT,			-- 辨靛 锅龋
	@pGuildNm	CHAR(12)		-- 辨靛 疙
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	SET 	@aErrNo = 0;
	
	INSERT INTO dbo.TblUnitedGuildWarGuildInfo ([mSvrNo], [mGuildNo], [mGuildNm]) 
	VALUES (@pSvrNo, @pGuildNo, @pGuildNm);

	IF(@@ERROR <> 0)
	 BEGIN
		SET	@aErrNo = 1;		-- DB Error;
	 END
		 
	RETURN(@aErrNo);

GO

