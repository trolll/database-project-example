/******************************************************************************  
**  Name: UspListBeadHoleProb
**  Desc: 备浇狼 浇吩 俺荐 犬伏 府胶泼
**  
**                
**  Return values:  
**   饭内靛悸
**                
**  Author: 辫碍龋
**  Date: 2011-07-14
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspListBeadHoleProb]
AS
	SET NOCOUNT ON	
	
	SELECT IID, mMaxHoleCount, mHoleCount, mProb
	FROM dbo.TblBeadHoleProb

GO

