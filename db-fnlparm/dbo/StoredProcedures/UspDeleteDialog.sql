CREATE PROCEDURE [dbo].[UspDeleteDialog]
	 @pMId			INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	DELETE TblDialog WHERE @pMId = [mMId]
		
LABEL_END:		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

