/******************************************************************************
**		Name: WspGetEventQuestReward
**		Desc: 捞亥飘 涅胶飘 焊惑
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetEventQuestReward] 
    @pEQuestNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	SELECT
		mEQuestNo
		,mRank
		,mIID
		,mStatus
		,mBinding
		,mEffTime
		,mValTime
		,mCnt
		,mIName = (SELECT IName FROM dbo.DT_ITEM WHERE IID = T1.mIID)
	FROM dbo.TblEventQuestReward T1
	WHERE mEQuestNo = @pEQuestNo

GO

