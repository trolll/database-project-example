CREATE        PROCEDURE [dbo].[WspListServer]
AS
	SET NOCOUNT ON

	SELECT
		mSvrNo
		, mDesc
	--	, mGameDbIp
	FROM
		[dbo].[TblParmSvr] WITH (NOLOCK)
	WHERE
		mType = 1 
	ORDER BY
		mSvrNo

	SET NOCOUNT OFF

GO

