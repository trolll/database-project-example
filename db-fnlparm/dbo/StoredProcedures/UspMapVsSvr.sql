CREATE PROCEDURE [dbo].[UspMapVsSvr]
	@pWorldNo	SMALLINT	 
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON		-- Count-set결과를 생성하지 말아라.
	
	DECLARE @aErrNo	INT
	SET @aErrNo = 0
	
	DECLARE @aCnt	INT
	SELECT @aCnt=COUNT(*) FROM TblMap
	
	SELECT b.[mSvrNo], b.[mMapNo] 
		FROM TblParmSvr AS a INNER JOIN TblMapVsSvr AS b ON(a.[mSvrNo] = b.[mSvrNo])
							 INNER JOIN TblMap      AS c ON(b.[mMapNo] = c.[mNo])
		WHERE (0 <> a.[mIsValid]) AND (@pWorldNo = a.[mWorldNo]) AND (2 = a.[mType])
	-- 등록되지 않은 Map이 있다.ㅠ.ㅠ
	IF(@aCnt <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END
	 END
LABEL_END:
	SET NOCOUNT OFF
	RETURN @aErrNo

GO

