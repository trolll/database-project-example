/******************************************************************************
**		Name: UspGetJackpotRewardList
**		Desc: 黎铺 焊惑 府胶飘甫 啊廉柯促.
**
**		Auth: 沥柳宽
**		Date: 2013-10-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013.10.15	沥柳宽				积己    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetJackpotRewardList]
AS
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
   
	SELECT [mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt] FROM dbo.TblJackpotRewardList
  
	SET NOCOUNT OFF

GO

