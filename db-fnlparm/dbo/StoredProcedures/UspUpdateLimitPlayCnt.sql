/******************************************************************************
**		Name: UspUpdateLimitPlayCnt
**		Desc: 
**
**		Auth: 沥柳宽
**		Date: 2016-09-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateLimitPlayCnt]
	@pType		TINYINT,	-- 鸥涝(烹辨傈 1, 评珐农 2)
	@pSvrNo		SMALLINT,	-- 辑滚 锅龋
	@pNo		INT,		-- 锅龋(pType=1 : 辨靛锅龋, pType=2 : 某腐磐锅龋)
	@pPlayCnt	INT			-- 函版且 敲饭捞 冉荐
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo	INT;
	SET 	@aErrNo = 0;
	
	UPDATE	dbo.TblLimitPlayCnt 
	SET 
		[mPlayCnt] = [mPlayCnt]	+ @pPlayCnt
	WHERE	[mType] = @pType
			AND [mSvrNo] = @pSvrNo
			AND [mNo] = @pNo;

	IF(0 = @@ROWCOUNT)
	 BEGIN
		-- 货肺款 单捞磐甫 积己茄促.
		EXEC @aErrNo = dbo.UspCreateLimitPlayCnt @pType, @pSvrNo, @pNo;
	
		IF(0 <> @aErrNo)
		 BEGIN
			RETURN(@aErrNo);
		 END
		 
		UPDATE	dbo.TblLimitPlayCnt 
		SET 
			[mPlayCnt] = [mPlayCnt]	+ @pPlayCnt
		WHERE	[mType] = @pType
				AND [mSvrNo] = @pSvrNo
				AND [mNo] = @pNo;
	 END
			
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo = 1;		-- DB ERROR;
	 END
		 
	RETURN(@aErrNo);

GO

