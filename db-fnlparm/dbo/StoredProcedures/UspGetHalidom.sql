/******************************************************************************
**		Name: UspGetHalidom
**		Desc: 墨坷胶硅撇侩 己巩 沥焊甫 肺靛茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-02-09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetHalidom
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  mNo
		, mPosX
		, mPosY
		, mPosZ
		, mWidth
		, mDir
		, mDesc
		, mGroup
	FROM	dbo.TblHalidom

GO

