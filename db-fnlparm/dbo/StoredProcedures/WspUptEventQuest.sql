/******************************************************************************
**		Name: WspUptEventQuest
**		Desc: 捞亥飘 涅胶飘 诀单捞飘
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROC [dbo].[WspUptEventQuest] 
    @pEQuestNo		INT,
    @pType			TINYINT,
    @pTitle			VARCHAR(100)
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblEventQuest
	SET    mTitle = @pTitle, mType = @pType
	WHERE  mEQuestNo = @pEQuestNo

	IF (@@ERROR <> 0)
	BEGIN	
		RETURN(1)
	END

	RETURN(0)

GO

