
CREATE PROCEDURE dbo.UspGetParmEventObj
	@pIsSupportEvent		BIT,
	@pSvrNo					SMALLINT
AS
	SET NOCOUNT ON;

	IF(0=@pIsSupportEvent)
	BEGIN
		SELECT 
			mObjType
			, mObjID
		FROM dbo.TblEventObj T1
			INNER JOIN dbo.TblEvent T2
				ON T1.mEventID = T2.mEventID
					AND T2.mIsEverEvent = 1	-- 惑矫 瘤盔 Option
		WHERE T1.mEventID IN (
			SELECT  mEventID
			FROM dbo.TblParmSvrSupportEvent
			WHERE mSvrNo = @pSvrNo
				AND mBeginDate <= GETDATE()
				AND mEndDate >= GETDATE() )
		GROUP BY mObjType, mObjID;	

		
		RETURN;
	END
	
	
	SELECT 
		mObjType
		, mObjID
	FROM dbo.TblEventObj
	WHERE mEventID IN (
		SELECT  mEventID
		FROM dbo.TblParmSvrSupportEvent
		WHERE mSvrNo = @pSvrNo
			AND mBeginDate <= GETDATE()
			AND mEndDate >= GETDATE() )
	GROUP BY mObjType, mObjID;

GO

