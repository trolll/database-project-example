/******************************************************************************
**		Name: WspGetCalendarEvent
**		Desc: ÀÌº¥Æ® ´Þ·Â
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetCalendarEvent]  
	@pEventNo	INT		= NULL	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF @pEventNo IS NULL
	BEGIN
	SELECT 
		mEventNo, mTitle, mDesc, mURL
	FROM dbo.TblCalendarEvent	
	
		RETURN
	END 
	
	SELECT 
		mEventNo, mTitle, mDesc, mURL
	FROM dbo.TblCalendarEvent	
	WHERE mEventNo = @pEventNo

GO

