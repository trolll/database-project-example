/******************************************************************************
**		Name: WspDelEventQuestCondition
**		Desc: 捞亥飘 涅胶飘 炼扒 昏力
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspDelEventQuestCondition]
	@pEQuestNo	INT
	, @pDelType	TINYINT	= 0	-- 0 : all, 1 : subtype  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	BEGIN TRAN
	
		DELETE dbo.TblParmSvrSupportEventQuest
		WHERE mEQuestNo = @pEQuestNo
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END 

		DELETE dbo.TblEventQuestReward
		WHERE mEQuestNo = @pEQuestNo
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END 
		
		DELETE dbo.TblEventQuestCondition
		WHERE mEQuestNo = @pEQuestNo
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END 
		
		IF @pDelType = 0
		BEGIN
		
			DELETE dbo.TblEventQuest
			WHERE mEQuestNo = @pEQuestNo
			
			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
			
			IF @aErrNo <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(1)			
			END 		
		END 
		
	COMMIT TRAN	
    RETURN(0)

GO

