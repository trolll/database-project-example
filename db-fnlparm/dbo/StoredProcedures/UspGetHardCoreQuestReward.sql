/******************************************************************************
**		Name: UspGetHardCoreQuestReward
**		Desc: 窍靛内绢 涅胶飘 焊惑 酒捞袍(函脚胶懦合)犁瘤鞭
**
**		Auth: 巢扁豪
**		Date: 2012-10-09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetHardCoreQuestReward]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		Q.mQuestNo		, QR.mRewardNo		, QR.mID		, QR.mCnt		, QR.mBinding		, QR.mStatus		, QR.mEffTime		, QR.mValTime		, DM.MAParam
	FROM dbo.TblQuestReward AS QR
		INNER JOIN dbo.TblQuest AS Q
	ON QR.mRewardNo = Q.mRewardNo
		INNER JOIN dbo.DT_Item AS DI
	ON QR.mID = DI.IID AND DI.IType = 12 -- 12 : 氓 酒捞袍
		INNER JOIN dbo.DT_ItemSkill AS DIS
	ON DI.IID = DIS.IID
		INNER JOIN dbo.DT_SkillAbnormal AS DSA
	ON DIS.SID = DSA.SID
		INNER JOIN dbo.DT_AbnormalModule AS DAM
	ON DSA.AbnormalID = DAM.AID
		INNER JOIN dbo.DT_Module AS DM
	ON DAM.MID = DM.MID AND DM.MType = 101 -- 101 : 胶懦蒲 积己 葛碘
	WHERE Q.mQuestKind = 1 and QR.mID <> 0 -- 1 : 窍靛内绢 涅胶飘
END

GO

