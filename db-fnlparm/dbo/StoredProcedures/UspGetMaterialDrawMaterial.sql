/******************************************************************************
**		Name: UspGetMaterialDrawMaterial
**		Desc:  메터리얼 뽑기 정보 중 재료 정보를 로드한다.
**
**		Auth: 김 광섭
**		Date: 2008-03-12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 	Author:	
**		Description: 
**		--------	--------			---------------------------------------
**		2009.03.30	김희도				mSuccess 컬럼 정보 삭제		
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMaterialDrawMaterial]
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		T1.MDID,
		T1.IID,
		T1.mCnt
	FROM
		dbo.TblMaterialDrawMaterial	T1
		INNER JOIN 	dbo.DT_ITEM			T2
			ON	T1.IID = T2.IID

GO

