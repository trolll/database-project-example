/******************************************************************************
**		Name: UspGetParmServantCombine
**		Desc: 辑锅飘 钦己 搬苞 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-10-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantCombine]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT IID, ResultIID
	FROM dbo.TblServantCombine

	SET NOCOUNT OFF;

GO

