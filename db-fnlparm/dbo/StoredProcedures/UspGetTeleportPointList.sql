CREATE  PROCEDURE [dbo].[UspGetTeleportPointList]	
--------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SELECT
		mPosX,
		mPosY,
		mPosZ
	FROM
		[dbo].[TblTeleportPointList]
		
	SET NOCOUNT OFF

GO

