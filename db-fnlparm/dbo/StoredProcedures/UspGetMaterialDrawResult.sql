/******************************************************************************
**		Name: UspGetMaterialDrawResult
**		Desc:  皋磐府倔 惶扁 沥焊 吝 搬苞拱 沥焊甫 肺靛茄促.
**
**		Auth: 辫 堡挤
**		Date: 2008-03-12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 	Author:	
**		Description: 
**		--------	--------			---------------------------------------
**		20080520	辫堡挤				酒捞袍 惑怕 沥焊 眠啊
**		20081201	辫堡挤				酒捞袍 裙垫 肮荐 眠啊
**		20090330	辫锐档				拿烦 沥焊 眠啊
**		20130930	沥柳龋				mAddGroup 拿烦 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMaterialDrawResult]
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		T1.MDRD,
		T1.IID,
		T1.mPerOrRate,
		T1.mItemStatus,
		T1.mCnt,
		T1.mBinding,
		T1.mEffTime,
		T1.mValTime,
		T1.mResource,
		T1.mAddGroup
	FROM
		dbo.TblMaterialDrawResult		T1
		INNER JOIN	dbo.DT_ITEM		T2
			ON	T1.IID = T2.IID

GO

