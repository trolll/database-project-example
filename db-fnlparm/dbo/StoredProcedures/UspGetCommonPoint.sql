CREATE PROCEDURE [dbo].[UspGetCommonPoint]
	@pSvrNo						SMALLINT,
	@pPcNo						INT,
	@pVolitionOfHonor			INT		OUTPUT,
	@pHonorPoint				INT		OUTPUT,
	@pChaosPoint				BIGINT	OUTPUT,
	@pDefaultVolitionOfHonor	SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	
	-- 존재하지 않는다면 새로 생성한다.
	IF NOT EXISTS (SELECT * FROM dbo.TblCommonPoint WHERE [mSvrNo] = @pSvrNo AND [mPcNo] = @pPcNo)
	 BEGIN
		INSERT INTO	dbo.TblCommonPoint (mSvrNo, mPcNo, mVolitionOfHonor) VALUES (@pSvrNo, @pPcNo, @pDefaultVolitionOfHonor);
		SELECT	@aErrNo = @@ERROR;
		IF(@aErrNo <> 0)
		 BEGIN
			RETURN(1);	-- DB ERROR
		 END
	 END
	
	SELECT
		  @pVolitionOfHonor	= [mVolitionOfHonor]
		, @pHonorPoint		= [mHonorPoint]
		, @pChaosPoint		= [mChaosPoint]
	FROM	dbo.TblCommonPoint
	WHERE	[mSvrNo] = @pSvrNo
				AND [mPcNo] = @pPcNo;
		
	
	RETURN(0);

GO

