/******************************************************************************
**		Name: UspUpdateUnitedGuildWarGuildInfo
**		Desc: 烹钦 辨靛 措傈 辨靛 沥焊甫 盎脚茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspUpdateUnitedGuildWarGuildInfo
	@pSvrNo					SMALLINT,	-- 辑滚 锅龋
	@pGuildNo				INT,		-- 辨靛 锅龋
	@pGuildNm				CHAR(12),	-- 辨靛 疙
	@pChgGuildPoint			INT,		-- 函版且 辨靛 器牢飘
	@pChgKillCnt			INT,		-- 函版且 磷牢 冉荐
	@pChgDieCnt				INT,		-- 函版且 磷篮 冉荐
	@pChgVictoryCnt			INT,		-- 函版且 铰府 冉荐 
	@pChgDrawCnt			INT,		-- 函版且 厚变 冉荐
	@pChgDefeatCnt			INT,		-- 函版且 菩硅 冉荐
	@pChgVictoryHBT1		INT,		-- 函版且 1VS1 老扁配 铰荐
	@pChgDefeatHBT1			INT,		-- 函版且 1VS1 老扁配 菩硅 荐
	@pChgVictoryHBT2		INT,		-- 函版且 2VS2 老扁配 铰荐
	@pChgDefeatHBT2			INT,		-- 函版且 2VS2 老扁配 菩硅 荐
	@pChgVictoryHBT3		INT,		-- 函版且 3VS3 老扁配 铰荐
	@pChgDefeatHBT3			INT,		-- 函版且 3VS3 老扁配 菩硅 荐
	@pRecentlyMatchSvrNo	SMALLINT,	-- 弥辟 版扁茄 辨靛啊 家加等 辑滚 锅龋
	@pRecentlyMatchGuildNm	CHAR(12),	-- 弥辟 版扁茄 辨靛 疙
	@pRecentlyMatchResult	TINYINT		-- 弥辟 版扁 搬苞
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	SET 	@aErrNo = 0;
	
	UPDATE	dbo.TblUnitedGuildWarGuildInfo 
	SET 
		[mGuildPoint]	= [mGuildPoint] 	+ @pChgGuildPoint,
		[mKillCnt]		= [mKillCnt] 		+ @pChgKillCnt,
		[mDieCnt]		= [mDieCnt]			+ @pChgDieCnt,
		[mVictoryCnt]	= [mVictoryCnt] 	+ @pChgVictoryCnt,
		[mDrawCnt]		= [mDrawCnt] 		+ @pChgDrawCnt,
		[mDefeatCnt]	= [mDefeatCnt] 		+ @pChgDefeatCnt,
		[mVictoryHerosBT1]	= [mVictoryHerosBT1]	+ @pChgVictoryHBT1,
		[mDefeatHerosBT1]	= [mDefeatHerosBT1]		+ @pChgDefeatHBT1,
		[mVictoryHerosBT2]	= [mVictoryHerosBT2]	+ @pChgVictoryHBT2,
		[mDefeatHerosBT2]	= [mDefeatHerosBT2]		+ @pChgDefeatHBT2,
		[mVictoryHerosBT3]	= [mVictoryHerosBT3]	+ @pChgVictoryHBT3,
		[mDefeatHerosBT3]	= [mDefeatHerosBT3]		+ @pChgDefeatHBT3,
		[mRecentlyMatchSvrNo]	= @pRecentlyMatchSvrNo,
		[mRecentlyMatchGuildNm]	= @pRecentlyMatchGuildNm,
		[mRecentlyMatchResult]	= @pRecentlyMatchResult
	WHERE	[mSvrNo] = @pSvrNo 
			AND [mGuildNo] = @pGuildNo;

	IF(0 = @@ROWCOUNT)
	 BEGIN
		-- 货肺款 单捞磐甫 积己茄促.
		EXEC @aErrNo = dbo.UspCreateUnitedGuildWarGuildInfo @pSvrNo, @pGuildNo, @pGuildNm;
	
		IF(0 <> @aErrNo)
		 BEGIN
			RETURN(@aErrNo);
		 END
		 
		UPDATE	dbo.TblUnitedGuildWarGuildInfo 
		SET 
		[mGuildPoint]	= [mGuildPoint] 	+ @pChgGuildPoint,
		[mKillCnt]		= [mKillCnt] 		+ @pChgKillCnt,
		[mDieCnt]		= [mDieCnt]			+ @pChgDieCnt,
		[mVictoryCnt]	= [mVictoryCnt] 	+ @pChgVictoryCnt,
		[mDrawCnt]		= [mDrawCnt] 		+ @pChgDrawCnt,
		[mDefeatCnt]	= [mDefeatCnt] 		+ @pChgDefeatCnt,
		[mVictoryHerosBT1]	= [mVictoryHerosBT1]	+ @pChgVictoryHBT1,
		[mDefeatHerosBT1]	= [mDefeatHerosBT1]		+ @pChgDefeatHBT1,
		[mVictoryHerosBT2]	= [mVictoryHerosBT2]	+ @pChgVictoryHBT2,
		[mDefeatHerosBT2]	= [mDefeatHerosBT2]		+ @pChgDefeatHBT2,
		[mVictoryHerosBT3]	= [mVictoryHerosBT3]	+ @pChgVictoryHBT3,
		[mDefeatHerosBT3]	= [mDefeatHerosBT3]		+ @pChgDefeatHBT3,
		[mRecentlyMatchSvrNo]	= @pRecentlyMatchSvrNo,
		[mRecentlyMatchGuildNm]	= @pRecentlyMatchGuildNm,
		[mRecentlyMatchResult]	= @pRecentlyMatchResult
		WHERE	[mSvrNo] = @pSvrNo 
				AND [mGuildNo] = @pGuildNo;
	 END
			
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo = 1;		-- DB ERROR;
	 END
		 
	RETURN(@aErrNo);

GO

