CREATE PROCEDURE dbo.UspIsInvalidGuildNickNm
	 @pGuildNickNm	CHAR(16)
--------WITH ENCRYPTION
AS
	DECLARE @aGuildNickNm	VARCHAR(16)
	SET @aGuildNickNm = RTRIM(@pGuildNickNm)
	
	DECLARE	@aCnt		INT
	SET @aCnt = dbo.UfnIsInvalidString(@aGuildNickNm)
	IF(0 = @aCnt)
	 BEGIN
		-- [String]가 VARCHAR이 아닌 CHAR이라면 RTRIM()을 해야 한다.
		SELECT @aCnt=COUNT(*) FROM TblInvalidGuildNickNm WHERE @aGuildNickNm = [mString]
	 END
	 
	RETURN @aCnt

GO

