CREATE PROCEDURE dbo.UspSetDialogScr
	 @pMId			INT
	,@pScriptText	VARCHAR(8000)	-- eSzDialogScriptText 와 연동
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	-- 스크립트를 넣는다
	IF EXISTS(SELECT * FROM TblDialogScript WHERE @pMId = [mMId])
	 BEGIN
		UPDATE TblDialogScript 
		SET 
			[mScriptText] = @pScriptText,
			[mUptDate]=GETDATE()
		WHERE @pMId = [mMId]	 
	 END
	ELSE
	 BEGIN
		INSERT TblDialogScript([mMId], [mScriptText])
			VALUES(@pMId, @pScriptText)	 
	 END 
	 	 
	IF(@@ROWCOUNT <> 1)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END	 
	 END
		
LABEL_END:		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

