CREATE   PROCEDURE [dbo].[WspListInvalidUserId]
AS
	SET NOCOUNT ON

	SELECT
		mString
	FROM
		[dbo].[TblInvalidUserId] WITH (NOLOCK)
	ORDER BY
		mRegDate

GO

