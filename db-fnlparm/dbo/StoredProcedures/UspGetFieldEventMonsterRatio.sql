/******************************************************************************
**		Name: UspGetFieldEventMonsterRatio
**		Desc: 鞘靛 捞亥飘 阁胶磐 厚啦
**		Test:			
**		Auth: 巢己葛
**		Date: 2013/10/31
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetFieldEventMonsterRatio]
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT	[mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster]
	FROM dbo.TblFieldEventMonsterRatio

	SET NOCOUNT OFF

GO

