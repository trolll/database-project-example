CREATE PROCEDURE [dbo].[UspListDialogScr]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT a.[mMID], RTRIM(a.[mClick]), RTRIM(a.[mDie]), RTRIM(a.[mAttacked]), RTRIM(a.[mTarget]), RTRIM(a.[mBear]),
		   RTRIM(a.[mGossip1]), RTRIM(a.[mGossip2]), RTRIM(a.[mGossip3]), RTRIM(a.[mGossip4]), RTRIM(b.[mScriptText])
		FROM TblDialog AS a LEFT JOIN TblDialogScript AS b ON(a.[mMID] = b.[mMID])
		
	SET NOCOUNT OFF

GO

