/******************************************************************************  
**  Name: UspGetParmMonsterSpot  
**  Desc: 阁胶磐 府胶迄 矫埃

**	Auth: 沥备柳
**	Date: 10.12.06
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**	2010.12.06	沥备柳		[mAtDay], [mAtHour], [mAtMin] 拿烦 昏力
**	2016.03.23	傍籍痹		[mIsEvent] 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmMonsterSpot]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SELECT	
			mGID, mMID, mCnt, mTick, 
			--mAtDay, mAtHour, mAtMin,	-- 昏力(扁裙评 狼斑)
			mDir, mVarRespawnTick, mIsEvent
	FROM	TblMonsterSpot
		
	SET NOCOUNT OFF

GO

