/******************************************************************************
**		Name: UspGetWorldMapInfo
**		Desc: TblWorldMapInfo 를 불러오는 프로시져
**
**		Auth: 김희도
**		Date: 2009.03.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                수정일           수정자                             수정내용    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetWorldMapInfo]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		mMapNo,
		mMapVisible,
		mPartyVisible,
		mListVisible
	FROM dbo.TblWorldMapInfo	
END

GO

