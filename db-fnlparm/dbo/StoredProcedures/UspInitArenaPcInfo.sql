/******************************************************************************
**		Name: UspInitArenaPcInfo
**		Desc: 酒饭唱 菩澄萍 檬扁拳
**
**		Auth: 沥柳龋
**		Date: 2016-09-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspInitArenaPcInfo]
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblArenaPcInfo
	SET mPanalty = 0

GO

