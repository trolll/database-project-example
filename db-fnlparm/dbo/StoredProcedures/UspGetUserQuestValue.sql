CREATE PROCEDURE	dbo.UspGetUserQuestValue
	@pUserNo		INT,		-- 사용자 계정번호
	@pQuestNo		INT,		-- 퀘스트번호
	@pValue			INT OUTPUT,		-- 퀘스트값 (출력)
	@pErrNoStr		VARCHAR(50) OUTPUT
As
Begin
	SET NOCOUNT ON

	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@pValue = 0
			
	
	----------------------------------------------------------------------------
	-- 퀘스트 정보 존재 체크 
	----------------------------------------------------------------------------
	SELECT @pValue = mValue 
	FROM dbo.TblUserQuest WITH (NOLOCK) 
	WHERE mUserNo = @pUserNo AND mQuestNo = @pQuestNo
	
	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR	

	IF @pRowCnt > 0
	BEGIN
		RETURN(0)		-- None Error
	END 

	----------------------------------------------------------------------------
	-- 유저 퀘스트 등록
	----------------------------------------------------------------------------	
	BEGIN TRAN

		INSERT dbo.TblUserQuest (mUserNo, mQuestNo) 
		VALUES(@pUserNo, @pQuestNo)
		
		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR			

		IF @pErr <> 0 OR @pRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			
			SET	@pErrNoStr	= 'eErrNoSqlInternalError'
			RETURN(@pErr)	-- SQL 내부 에러 					
		END 

	COMMIT TRAN
	RETURN(0)
End

GO

