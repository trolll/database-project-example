/******************************************************************************
**		Name: UspGetAchieveTrophyItem
**		Desc: 飘肺乔 酒捞袍 沥焊**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveTrophyItem]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 IID
		, mRarity
		, mEquipType
		, mEquipPos
		, mAbilityType
	FROM dbo.DT_AchieveItemTrophy

GO

