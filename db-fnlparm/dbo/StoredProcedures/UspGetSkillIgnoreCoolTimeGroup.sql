CREATE PROCEDURE [dbo].[UspGetSkillIgnoreCoolTimeGroup]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		SGroupNo, SIgnoreGroup
	FROM 
		dbo.DT_SkillIgnoreCoolTimeGroup
	ORDER BY 
		SGroupNo ASC

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN (0)

GO

