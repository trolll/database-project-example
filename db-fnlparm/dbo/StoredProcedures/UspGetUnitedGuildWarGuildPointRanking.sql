/******************************************************************************
**		Name: UspGetUnitedGuildWarGuildPointRanking
**		Desc: 辨靛 器牢飘 珐欧 沥焊甫 掘绰促. (50困)
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010-03-29	辫堡挤				辨靛疙俊 RTRIM 眠啊
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetUnitedGuildWarGuildPointRanking
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  [mSvrNo]
		, [mRanking]
		, RTRIM([mGuildNm])
		, [mGuildPoint]
		, [mGuildNo]
	FROM	dbo.TblUnitedGuildWarGuildPointRanking
	ORDER BY [mRanking] ASC

GO

