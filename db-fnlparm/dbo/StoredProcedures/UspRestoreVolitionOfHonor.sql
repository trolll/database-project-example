/******************************************************************************
**		Name: UspRestoreVolitionOfHonor
**		Desc: 명예의 의지 포인트를 회복한다. 초기화 한다.
**
**		Auth: 김 광섭
**		Date: 2009-04-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: Author:	
**		Description: 
**		--------	--------			---------------------------------------
**		20090512	JUDY				초기화 한다.
*******************************************************************************/
CREATE  PROCEDURE dbo.UspRestoreVolitionOfHonor
	   @pPcNo		INT
	 , @pSvrNO		SMALLINT	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	DECLARE	@aRowCnt	INT
			
	SELECT	@aRowCnt = 0, @aErrNo = 0;
	
	
	UPDATE	dbo.TblCommonPoint
	SET
		[mRestoreVolitionOfHonor]	= 0
	WHERE	mSvrNo = @pSvrNo
				AND mPcNo = @pPcNo;
				
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;			
				
	IF(0 <> @aErrNo OR 1 <> @aRowCnt)
	 BEGIN
		RETURN(1);
	 END
	 
	RETURN(0);

GO

