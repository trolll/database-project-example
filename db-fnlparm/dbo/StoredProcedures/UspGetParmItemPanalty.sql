/******************************************************************************
**		Name: UspGetParmItemPanalty
**		Desc: 流诀俊 蝶弗 酒捞袍狼 菩澄萍 沥焊甫 啊瘤绊 柯促.
**
**		Auth: 沥备柳
**		Date: 10.02.16
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.06.16	辫碍龋				IDDV, IMDV, IRDV 眠啊
										IDPV, IMPV, IRPV 眠啊	
										IHDDV, IHMDV, IHRDV 眠啊
										IHDPV, IHMPV, IHRPV 眠啊
										IDV, IPV, IHDV, IHPV 昏力
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmItemPanalty]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		IID, IUseClass, 

		--IDV, IPV, IHDV, IHPV,			-- 昏力凳
		IDDV, IMDV, IRDV,
		IDPV, IMPV, IRPV,
		IHDDV, IHMDV, IHRDV,
		IHDPV, IHMPV, IHRPV,

		IDHIT, IDDD, IRHIT, IRDD, IMHIT, IMDD, IHPPlus, IMPPlus, ISTR, IDEX, IINT, IHPRegen, IMPRegen, 
		IAttackRate, IMoveRate, ICritical, IRange, IAddWeight, IAddPotionRestore
	FROM 
		dbo.DT_ItemPanalty
	ORDER BY
		IID ASC

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN (0)

GO

