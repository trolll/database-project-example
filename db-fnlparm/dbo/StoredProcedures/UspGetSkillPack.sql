/******************************************************************************  
**  File: UspGetSkillPack.sql
**  Name: UspGetSkillPack  
**  Desc: 胶懦蒲 沥焊, 胶懦蒲狼 胶懦甸阑 肺靛茄促.
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**  2010.05.13	dmbkh		积己
**	2010.07.31	dmbkh		mUseInAttack 眠啊
**	2011.06.01	kirba		mIsDrop 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetSkillPack]
AS  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	SELECT  a.mSPID, a.mName, a.mIType, a.mIUseType, a.mISubType, a.mTermOfValidity, a.mDesc, a.mUseMsg, 
		a.mUseRange, a.mUseClass
		, a.mUseLevel, a.mIsUseableUTGWSvr, b.mSID, a.mUseInAttack, a.mIsDrop
	FROM dbo.DT_SkillPack AS a     
		LEFT JOIN dbo.DT_SkillPackSkill AS b     
		ON(a.[mSPID] = b.[mSPID])    
	ORDER BY a.[mSPID], b.[mSOrderNO],  b.[mSID]

GO

