/******************************************************************************
**		Name: UspGetServantCombineAddAbility
**		Desc: 辑锅飘 钦己 眠啊 瓷仿摹 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-10-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantCombineAddAbility]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	SStuffType,SIsCoreGold,SIsStuffGold
			,SStrMax,SDexMax,SIntMax,STotalMin,STotalMax
	FROM dbo.TblServantCombineAddAbility

	SET NOCOUNT OFF;

GO

