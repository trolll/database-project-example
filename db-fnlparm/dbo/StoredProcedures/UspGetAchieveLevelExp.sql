/******************************************************************************
**		Name: UspGetAchieveLevelExp
**		Desc: 飘肺乔 酒捞袍 版氰摹 喊 饭闺 抛捞喉**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveLevelExp]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mLevel
		, mExp
	FROM dbo.TP_AchieveItemLevel

GO

