/******************************************************************************
**		Name: WspGetCharCommPoint
**		Desc: 캐릭터 서버 공용 포인트 정보
**
**		Auth: 김석천
**		Date: 2009-04-02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                수정일           수정자                             수정내용    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetCharCommPoint]
	@pSvrNo SMALLINT,	-- 필드 서버 번호
	@pPcNo INT,			-- 필드 서버에서의 PcNo
	@pVolitionOfHonor SMALLINT OUTPUT,	-- 명예의 의지 포인트
	@pHonorPoint INT OUTPUT,	-- 명예 포인트
	@pChaosPoint BIGINT OUTPUT,	-- 카오스 포인트
	@pRestoreVolitionOfHonor SMALLINT OUTPUT	-- 복구 가능한 명예의 의지 포인트
AS
	SET NOCOUNT ON			-- 설명 : 결과 레코드 셋을 반환 안 시킨다.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT @pVolitionOfHonor = mVolitionOfHonor,
		@pHonorPoint = mHonorPoint,
		@pChaosPoint = mChaosPoint,
		@pRestoreVolitionOfHonor = mRestoreVolitionOfHonor
	FROM dbo.TblCommonPoint
	WHERE mSvrNo = @pSvrNo
		AND mPcNo = @pPcNo

GO

