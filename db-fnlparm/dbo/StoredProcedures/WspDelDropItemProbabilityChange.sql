/******************************************************************************
**		Name: WspDelDropItemProbabilityChange
**		Desc: µå·Ó ¾ÆÀÌÅÛ È®·ü º¯°æ »èÁ¦
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-08-13
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE dbo.WspDelDropItemProbabilityChange
	@pSvrNo		SMALLINT,
	@pItemID	INT,
	@pDGroup	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DELETE dbo.TblDropItemProbabilityChange
	WHERE mItemID = @pItemID AND mDGroup = @pDGroup AND @pSvrNo = mSvrNo

	IF (@@ERROR <> 0)
	BEGIN
		RETURN (1);
	END
	
	RETURN (0);

GO

