/******************************************************************************  
**  File: UspGetSpotAttackerList.sql
**  Name: UspGetSpotAttackerList
**  Desc: ½ºÆÌ ½À°ÝÀÚ ¸®½ºÆ® Á¤º¸
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2015.05.11	°ø¼®±Ô		»ý¼º
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetSpotAttackerList]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		T1.mGID,
		T2.mMID,
		T2.mGroup		
	FROM dbo.TblMonsterSpot T1
	JOIN dbo.TblSpotAttackerList T2 On T1.mMID = T2.mMID
	
	SET NOCOUNT OFF

GO

