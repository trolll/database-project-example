/******************************************************************************
**		Name: WspGetEventQuestSupportSvr
**		Desc: 捞亥飘 涅胶飘 瘤盔 辑滚
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetEventQuestSupportSvr]
	@pEQuestNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T1.mSvrNo
		,T1.mDesc as mSvrNm
		,T2.mBeginDate
		,T2.mEndDate
		,mIsRunning =
			CASE WHEN mBeginDate <= GETDATE() AND mEndDate >= GETDATE() THEN 0
				ELSE 1
			END
	FROM dbo.TblParmSvr T1
		LEFT OUTER JOIN 
			(
				SELECT 
					mSvrNo
					,mBeginDate
					,mEndDate
				FROM dbo.TblParmSvrSupportEventQuest
				WHERE mEQuestNo = @pEQuestNo
			) T2
			ON T1.mSvrNo = T2.mSvrNo
	WHERE mType = 1
	ORDER BY T1.mSvrNo

	RETURN

GO

