/******************************************************************************
**		Name: UspGetExpireMakingEventQuest
**		Desc: 辆丰等 捞亥飘 涅胶飘
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetExpireMakingEventQuest]
	@pSvrNo					SMALLINT
	, @pArrEQuest			VARCHAR(500) OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET @pArrEQuest = '';	

	SELECT @pArrEQuest = CONVERT(VARCHAR, mEQuestNo) + ',' + @pArrEQuest
	FROM dbo.TblEventQuestCondition 
	WHERE mEQuestNo NOT IN (
		SELECT  T1.mEQuestNo
		FROM dbo.TblParmSvrSupportEventQuest as T1
			INNER JOIN dbo.TblEventQuestCondition as T2
				ON T1.mEQuestNo = T2.mEQuestNo
			INNER JOIN dbo.TblEventQuest as T3
				ON T2.mEQuestNo = T3.mEQuestNo AND T2.mType = T3.mType
		WHERE T1.mSvrNo = @pSvrNo
			AND T1.mBeginDate <= GETDATE()
			AND T1.mEndDate >= GETDATE() )

GO

