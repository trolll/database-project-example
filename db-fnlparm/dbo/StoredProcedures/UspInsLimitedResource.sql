/******************************************************************************  
**  File: UspInsLimitedResource.sql  
**  Name: UspInsLimitedResource  
**  Desc: 府家胶酒捞袍 眠啊  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**	2012.06.11	沥柳龋		mIsIncSys 拿烦 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspInsLimitedResource]  
 @pResourceType INT,  
 @pMaxCnt  INT,  
 @pRemainerCnt INT,  
 @pRandomVal  FLOAT,
 @pIsIncSys BIT  
AS   
	SET NOCOUNT ON  
   
	IF EXISTS( SELECT *   
		FROM dbo.TblLimitedResource   
		WHERE mResourceType = @pResourceType )  
	BEGIN  
		RETURN(1)  
	END      
  
	INSERT INTO dbo.TblLimitedResource  
	VALUES(  
		GETDATE(),  
		@pResourceType,  
		@pMaxCnt,  
		@pRemainerCnt,  
		@pRandomVal,  
		GETDATE(),
		@pIsIncSys
	)  
    
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0  
	BEGIN  
		RETURN(1) -- db error  
	END  
     
	RETURN(0)

GO

