/******************************************************************************  
**  File: UspGetMonMatchMonList.sql
**  Name: UspGetMonMatchMonList 
**  Desc: 阁胶磐 概摹 阁胶磐府胶飘 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2012.10.09	沥柳龋		积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetMonMatchMonList]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	SELECT DISTINCT mMID
	FROM dbo.TblMonMatchMonList
END

GO

