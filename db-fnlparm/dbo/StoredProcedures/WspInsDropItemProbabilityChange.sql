/******************************************************************************
**		Name: WspInsDropItemProbabilityChange
**		Desc: µå·Ó ¾ÆÀÌÅÛ È®·ü º¯°æ µî·Ï
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-08-13
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE dbo.WspInsDropItemProbabilityChange
	@pSvrNo				SMALLINT,
	@pItemID			INT,
	@pDGroup			INT,
	@pMultiplication	REAL,
	@pAddition			REAL
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF NOT EXISTS ( SELECT  *
					FROM dbo.DT_Item
					WHERE IID = @pItemID)
	BEGIN
		RETURN(1);	-- ¾ÆÀÌÅÛ ID°¡ ¸ÂÁö ¾Ê½À´Ï´Ù.
	END
	
	IF NOT EXISTS ( SELECT *
					FROM dbo.DT_DropItem
					WHERE DDrop IN 	
					(
						SELECT  DDrop
						FROM dbo.DT_DropGroup
						WHERE DGroup = @pDGroup 
					)
					AND DItem = @pItemID )
	BEGIN
		RETURN(2);	-- µå¶ø±×·ì ID°¡ ¸ÂÁö ¾Ê½À´Ï´Ù.
	END
	
	IF (99.99 < @pMultiplication OR @pMultiplication < 0.01) 
	BEGIN
		RETURN(3);	-- ¹èÀ²°ªÀÌ ¸ÂÁö ¾Ê½À´Ï´Ù.
	END
	
	IF (99.99 < @pAddition OR @pAddition < 0.01) 
	BEGIN
		RETURN(4);	-- °¡»ê°ªÀÌ ¸ÂÁö ¾Ê½À´Ï´Ù.
	END

	INSERT INTO dbo.TblDropItemProbabilityChange (mItemID, mDGroup, mSvrNo, mMultiplication, mAddition)
	VALUES (@pItemID, @pDGroup, @pSvrNo, @pMultiplication, @pAddition)

	IF @@ERROR <> 0
	BEGIN
		RETURN (5);	-- sql error 
	END
		
	RETURN (0);

GO

