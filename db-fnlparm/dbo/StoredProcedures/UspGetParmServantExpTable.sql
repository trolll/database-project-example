/******************************************************************************
**		Name: UspGetParmServantExpTable
**		Desc: 辑锅飘 版氰摹 抛捞喉 掘扁
**
**		Auth: 捞侩林
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20160418	捞侩林				蜡丰 辑锅飘 版氰摹 鸥涝 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantExpTable]
	@pEExpType 	SMALLINT = 0
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT ELevel, EExp
	FROM dbo.DT_ServantExp
	WHERE EType = @pEExpType
	ORDER BY ELevel

	SET NOCOUNT OFF;

GO

