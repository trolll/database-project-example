CREATE PROCEDURE dbo.UspSetDiscipleExp
	 @pLevel		INT
	,@pExp			INT
	,@pMaxDiscipleCount	INT

AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	DECLARE @aRv	INT
	DECLARE @aErrNo	INT
	SET @aRv = 0

	IF EXISTS(SELECT mLevel FROM dbo.DT_DiscipleExp WHERE mLevel = @pLevel)
	BEGIN
		UPDATE dbo.DT_DiscipleExp SET mExp = @pExp, mMaxDiscipleCount = @pMaxDiscipleCount WHERE mLevel = @pLevel
		SET @aErrNo = @@ERROR
		IF @aErrNo <> 0
		BEGIN
			SET @aRv = 1		-- 1 : DB 내부적인 오류
		END
	END
	ELSE
	BEGIN
		SET @aRv = 2		-- 2 : 해당 사제모임 경험치 레벨이 존재하지 않음
	END

GO

