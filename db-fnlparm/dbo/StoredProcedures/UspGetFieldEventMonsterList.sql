/******************************************************************************
**		Name: UspGetFieldEventMonsterList
**		Desc: 鞘靛 捞亥飘 阁胶磐 mapping 沥焊 八祸
**		Test:			
**		Auth: 巢己葛
**		Date: 2013/10/31
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetFieldEventMonsterList]
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- 扁粮 阁胶磐 沥焊甫 历厘且 烙矫 抛捞喉
	DECLARE @aOrgMonTbl TABLE 
	(
		mKey			INT		NOT NULL	PRIMARY KEY,
		mEventType		INT		NOT NULL,
		mEventMonType	INT		NOT NULL,
		mOrgMonMid		INT		NOT NULL,
		mOrgMonGid		INT		NOT NULL
	);

	-- 老馆 阁胶磐 沥焊 (mEventMonType捞 0捞促)
	INSERT INTO @aOrgMonTbl
		SELECT T1.mKey, T1.mEventType, T1.mEventMonType, T1.mOrgMonMid, T3.mGID
		FROM dbo.TblFieldEventMonsterList AS T1 
		INNER JOIN dbo.TblMonsterSpotGroup AS T2 
		ON 
			T1.mOrgMonPosX = T2.mPosX AND 
			T1.mOrgMonPosY = T2.mPosY AND 
			T1.mOrgMonPosZ = T2.mPosZ 
		INNER JOIN dbo.TblMonsterSpot AS T3
		ON 
			T2.mGID		  = T3.mGID AND 
			T1.mOrgMonMid = T3.mMID
		WHERE 
			T1.mEventMonType = 0
		
	-- 鞘靛 焊胶 阁胶磐 沥焊 (mEventMonType捞 1捞促)
	INSERT INTO @aOrgMonTbl
		SELECT T1.mKey, T1.mEventType, T1.mEventMonType, 0, 0
		FROM dbo.TblFieldEventMonsterList AS T1 
		WHERE 
			T1.mEventMonType = 1
			
	-- 带怜&捞亥飘 焊胶 阁胶磐 沥焊 (mEventMonType捞 2捞促)
	INSERT INTO @aOrgMonTbl
		SELECT T1.mKey, T1.mEventType, T1.mEventMonType, T1.mOrgMonMid, 0
		FROM dbo.TblFieldEventMonsterList AS T1 
		WHERE 
			T1.mEventMonType = 2
			
	-- NPC 沥焊 (mEventMonType捞 3捞促)
	INSERT INTO @aOrgMonTbl
		SELECT T1.mKey, 0, T1.mEventMonType, 0, 0
		FROM dbo.TblFieldEventMonsterList AS T1 
		WHERE 
			T1.mEventMonType = 3
			
		
	-- 捞亥飘 阁胶磐 沥焊甫 历厘且 烙矫 抛捞喉
	DECLARE @aEvtMonTbl TABLE 
	(
		mKey			INT		NOT NULL	PRIMARY KEY,
		mEventMonMid	INT		NOT NULL,
		mEventMonGid	INT		NOT NULL
	);
	
	-- 阁胶磐 沥焊 (老馆 + 鞘靛 焊胶)
	INSERT INTO @aEvtMonTbl
		SELECT T1.mKey, T1.mEventMonMid, T3.mGID
		FROM dbo.TblFieldEventMonsterList AS T1 
		INNER JOIN dbo.TblMonsterSpotGroup AS T2
		ON 
			T1.mEventMonPosX = T2.mPosX AND 
			T1.mEventMonPosY = T2.mPosY AND 
			T1.mEventMonPosZ = T2.mPosZ
		INNER JOIN dbo.TblMonsterSpot AS T3
		ON 
			T2.mGID			 = T3.mGID AND 
			T1.mEventMonMid  = T3.mMID
		WHERE 
			T1.mEventMonType = 0 OR
			T1.mEventMonType = 1
	
	-- 带怜&捞亥飘 焊胶 阁胶磐 沥焊 (mEventMonType捞 2捞促)
	INSERT INTO @aEvtMonTbl
		SELECT T1.mKey, T1.mEventMonMid, 0
		FROM dbo.TblFieldEventMonsterList AS T1 
		WHERE 
			T1.mEventMonType = 2
	
	-- NPC 沥焊 (mEventMonType捞 3捞促)
	INSERT INTO @aEvtMonTbl
		SELECT T1.mKey, T1.mEventMonMid, 0
		FROM dbo.TblFieldEventMonsterList AS T1 
		WHERE 
			T1.mEventMonType = 3
			
	
	-- 扁粮 阁胶磐 烙矫 抛捞喉 沥焊客 捞亥飘 阁胶磐 烙矫 抛捞喉 沥焊甫 荤侩窍咯 搬苞甫 备茄促.
	SELECT S1.mEventType, S1.mEventMonType, S1.mOrgMonMid, S1.mOrgMonGid, S2.mEventMonMid, S2.mEventMonGid
	FROM @aOrgMonTbl AS S1
		INNER JOIN @aEvtMonTbl AS S2
		ON 
			S1.mKey = S2.mKey


	SET NOCOUNT OFF

GO

