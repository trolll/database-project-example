/******************************************************************************
**		Name: dbo.UspGetAbnormalAttrDetach
**		Desc: DT_AbnormalAttrDetach 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2012.11.22  沥柳龋				mEffectAID 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAbnormalAttrDetach]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		T1.mAppType
		, T1.mOriginAID
		, T1.mEffectAType
		, T1.mConditionType
		, T1.mConditionAType
		, T1.mEffectAID
	FROM dbo.DT_AbnormalAttrDetach AS T1
		INNER JOIN dbo.DT_Abnormal AS T2
	ON T1.mOriginAID = T2.AID
END

GO

