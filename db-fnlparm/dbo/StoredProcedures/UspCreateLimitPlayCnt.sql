/******************************************************************************
**		Name: UspCreateLimitPlayCnt
**		Desc: 
**
**		Auth: 沥柳宽
**		Date: 2016-09-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCreateLimitPlayCnt]
	@pType	TINYINT,	-- 鸥涝(烹辨傈 1, 评珐农 2)
	@pSvrNo	SMALLINT,	-- 辑滚 锅龋
	@pNo	INT			-- 某腐磐 锅龋
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo	INT;
	SET 	@aErrNo = 0;
	
	INSERT INTO dbo.TblLimitPlayCnt ([mType], [mSvrNo], [mNo]) 
	VALUES (@pType, @pSvrNo, @pNo);

	IF(@@ERROR <> 0)
	 BEGIN
		SET	@aErrNo = 1;		-- DB Error;
	 END
		 
	RETURN(@aErrNo);

GO

