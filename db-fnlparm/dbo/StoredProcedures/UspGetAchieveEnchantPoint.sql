/******************************************************************************
**		Name: UspGetAchieveList
**		Desc: 牢忙飘矫 眠啊登绰 瓷仿摹**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveEnchantPoint]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mRarityID
		, mRarityPoint
		, mLevelHP
		, mLevelMP
		, mLevelWP
		, mWeightHP
		, mWeightMP
		, mWeightWP
	FROM dbo.TP_AchieveRarityPoint

GO

