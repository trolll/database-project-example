CREATE PROCEDURE dbo.UspGetTreasureBox
	@pType 	SMALLINT
AS
	SET NOCOUNT ON

	SELECT 
		mMID
		, mIID
		, mAmount
		, mDice
		, mStatus 
	FROM dbo.TblTreasureBox  
	WHERE mType = @pType 
	ORDER BY mDice DESC

GO

