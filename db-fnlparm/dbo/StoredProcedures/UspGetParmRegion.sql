CREATE PROCEDURE [dbo].[UspGetParmRegion]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SELECT RegionID, Type, ID, RTRIM(Name)
		FROM DT_Region
		
	SET NOCOUNT OFF

GO

