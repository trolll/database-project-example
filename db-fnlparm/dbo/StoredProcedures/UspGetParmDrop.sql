CREATE PROCEDURE dbo.UspGetParmDrop
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	SELECT DDrop, DItem, DNumber,DStatus, DIsEvent
		FROM DT_DropItem
		ORDER BY DDrop
		
	SET NOCOUNT OFF

GO

