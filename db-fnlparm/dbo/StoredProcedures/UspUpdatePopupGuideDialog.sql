/******************************************************************************
**		Name: UspUpdatePopupGuideDialog
**		Desc: TblPopupGuideDialog 狼 诀单捞飘
**
**		Auth: 辫锐档
**		Date: 2009-10-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePopupGuideDialog]
	@pGuideNo		INT,
	@pSubject		VARCHAR(30),
	@pDialog		VARCHAR(7000)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE @aErrNo		INT,
			@aDate		SMALLDATETIME
	SET		@aErrNo		= 0
	SET		@aDate		= GETDATE()
	
	----------------------------------------------
	-- 粮犁窍瘤 臼促搁 INSERT 秦 霖促.
	----------------------------------------------
	IF EXISTS(SELECT * FROM TblPopupGuideDialog WHERE [mGuideNo] = @pGuideNo)
		BEGIN
			UPDATE dbo.TblPopupGuideDialog
			SET
				[mSubject]	= @pSubject,
				[mDialog]	= @pDialog,
				[mUptDate]	= @aDate
			WHERE [mGuideNo] = @pGuideNo
		END
	ELSE
		BEGIN
				INSERT INTO dbo.TblPopupGuideDialog([mGuideNo], [mSubject], [mDialog])
					VALUES(@pGuideNo, @pSubject, @pDialog)
		END
		
	IF(@@ROWCOUNT <> 1)
		BEGIN
			SET @aErrNo = 1
			GOTO LABEL_END
		END

LABEL_END:
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

