/******************************************************************************
**		Name: UspGetParmServantFriendlySkillCombine
**		Desc: 辑锅飘 钦己 胶懦 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-10-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantFriendlySkillCombine]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT IID, SPID
	FROM dbo.TblServantCombineSkill

	SET NOCOUNT OFF;

GO

