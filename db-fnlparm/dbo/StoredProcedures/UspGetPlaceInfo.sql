/******************************************************************************  
**  File: UspGetPlaceInfo.sql
**  Name: UspGetPlaceInfo
**  Desc: 瘤开 汲沥 沥焊 棺 啊廉坷扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2012.06.27	baek		积己
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetPlaceInfo
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT	[mPlaceNo]
			,[mPlaceNm]
			,[mTeleport]
			,[mTeleportSave]
			,[mCombat]
			,[mReturn]
			,[mIsCollision]
			,[mIsSiege]
			,[mRgbQuadRed]
			,[mRgbQuadGreen]
			,[mRgbQuadBlue]
			,[mIsTown]
			,[mIsSupport]
		FROM dbo.TblPlaceInfo		
	SET NOCOUNT OFF

GO

