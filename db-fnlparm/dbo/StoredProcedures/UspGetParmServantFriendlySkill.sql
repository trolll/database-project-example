/******************************************************************************
**		Name: UspGetParmServantFriendlySkill
**		Desc: ¼­¹øÆ® Ä£¹Ðµµ ½ºÅ³ ¾ò±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantFriendlySkill]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT IID, SPID
	FROM dbo.TblServantFriendlySkill
	ORDER BY IID, SPID

	SET NOCOUNT OFF;

GO

