CREATE PROCEDURE dbo.UspGetDiscipleExp
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	SELECT mLevel, mExp, mMaxDiscipleCount 
	FROM dbo.DT_DiscipleExp
	ORDER BY mLevel ASC

GO

