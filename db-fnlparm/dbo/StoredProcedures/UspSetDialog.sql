/******************************************************************************  
**  File: UspSetDialog.sql
**  Name: UspSetDialog
**  Desc: 促捞倔肺弊 汲沥
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2016.05.17	nsm			pClick 荤捞令 函版(7000 -> 8000)
**	
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspSetDialog]
	 @pMId			INT
	,@pClick		VARCHAR(8000)    -- eSzDialogClick 客 楷悼
	,@pDie			VARCHAR(100)
	,@pAttacked		VARCHAR(100)
	,@pTarget		VARCHAR(100)
	,@pBear			VARCHAR(100)
	,@pGossip1		VARCHAR(100)
	,@pGossip2		VARCHAR(100)
	,@pGossip3		VARCHAR(100)
	,@pGossip4		VARCHAR(100)
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	IF EXISTS(SELECT * FROM TblDialog WHERE @pMId = [mMId])
	 BEGIN
		UPDATE dbo.TblDialog 
		SET 
			[mClick]=@pClick, 
			[mDie]=@pDie, 
			[mAttacked]=@pAttacked, 
			[mTarget]=@pTarget, 
			[mBear]=@pBear, 
			[mGossip1]=@pGossip1, 
			[mGossip2]=@pGossip2, 
			[mGossip3]=@pGossip3, 
			[mGossip4]=@pGossip4,
			[mUptDate]=GETDATE()
		WHERE @pMId = [mMId]	 
	 END
	ELSE
	 BEGIN
		INSERT TblDialog([mMId], [mClick], [mDie], [mAttacked], [mTarget], [mBear], 
			   [mGossip1], [mGossip2], [mGossip3], [mGossip4])
			VALUES(@pMId, @pClick, @pDie, @pAttacked, @pTarget, @pBear, 
				   @pGossip1, @pGossip2, @pGossip3, @pGossip4)	 
	 END
	 
	IF(@@ROWCOUNT <> 1)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END	 
	 END
		
LABEL_END:		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

