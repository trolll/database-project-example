/******************************************************************************
**		Name: UspLogConCurInfo
**		Desc: 悼立 扁废.
**			  GS俊辑 5盒 付促 龋免(惫郴 何盒蜡丰拳 酒捞袍 磊悼 汗备)
**
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20100906	JUDY			PC规 悼立
*******************************************************************************/
CREATE PROCEDURE dbo.UspLogConCurInfo
	 @pSvrNo			SMALLINT
	,@pUserCnt			INT
	,@pPcCnt				INT = 0		-- PC规 悼立 荤侩樊 
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aRowCnt INT
		, @aErr INT
		
	SELECT 	@aRowCnt = 0, @aErr = 0;
	
	BEGIN TRAN

		-- 付瘤阜 辑滚 瘤盔 矫埃(扁废)
		UPDATE dbo.TblParmSvr
		SET
			mLastSupportDate = GETDATE()
		WHERE mSvrNo = @pSvrNo
		SELECT 	@aRowCnt = @@ROWCOUNT, @aErr = @@ERROR;
		
		IF @aErr <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1)
		END			

		-- 穿利侩 悼立肺弊
		INSERT dbo.TblConCurInfo([mSvrNo], [mUserCnt])
		VALUES(@pSvrNo, @pUserCnt)
		
		SELECT 	@aRowCnt = @@ROWCOUNT, @aErr = @@ERROR;

		IF @aErr <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1)
		END		

		-- 20060712(milkji):霸烙寇何俊辑 辑滚惑怕 免仿侩.
		UPDATE dbo.TblConCurInfoNow 
		SET 
			[mUserCnt]=@pUserCnt
			, [mLastChg]=GETDATE() 
			, mPcCnt = @pPcCnt
		WHERE [mSvrNo] = @pSvrNo;		
		
		SELECT 	@aRowCnt = @@ROWCOUNT, @aErr = @@ERROR;

		IF @aErr <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1)
		END			
		
		IF @aRowCnt > 0 
		BEGIN
			COMMIT TRAN;
			RETURN(0)
		END				 					
				
		-- 20060712(milkji):霸烙寇何俊辑 辑滚惑怕 免仿侩.
		INSERT dbo.TblConCurInfoNow([mSvrNo], [mUserCnt], [mLastChg], mPcCnt ) 
				VALUES(@pSvrNo, @pUserCnt, GETDATE(), @pPcCnt)

		SELECT 	@aRowCnt = @@ROWCOUNT, @aErr = @@ERROR;

		IF @aErr <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1)
		END					

	COMMIT TRAN;
	RETURN(0)

GO

