/******************************************************************************
**		Name: UspGetParmTransformAbilityByLevel
**		Desc: 函脚 矫 饭骇俊 蝶弗 瓷仿摹甫 啊瘤绰 抛捞喉阑 啊廉柯促. 
**              
**		Auth: 沥备柳
**		Date: 2010.05.18
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩   
**      2013/04/24	巢己葛				mType 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmTransformAbilityByLevel]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		mLevel
		, mMaxHP
		, mMaxMP
		, mMaxWeight
		, mShortAttackRate
		, mLongAttackRate
		, mMoveRate
		, mType
	FROM dbo.TblTransformAbilityByLevel

GO

