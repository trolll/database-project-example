CREATE PROCEDURE [dbo].[UspListAiRaid]
AS
	SET NOCOUNT ON	

	SELECT [mID], [mMID], [mCID], [mCPercent], [mCAParam], [mCBParam], [mCCParam], 
		[mSID1], [mSDelay1], [mSLoop1], [mSTargetType1], [mSMaxCount1], [mSFixMax1], 
		[mSID2], [mSDelay2], [mSLoop2], [mSTargetType2], [mSMaxCount2], [mSFixMax2], 
		[mSID3], [mSDelay3], [mSLoop3], [mSTargetType3], [mSMaxCount3], [mSFixMax3], 
		[mSID4], [mSDelay4], [mSLoop4], [mSTargetType4], [mSMaxCount4], [mSFixMax4], 
		[mSID5], [mSDelay5], [mSLoop5], [mSTargetType5], [mSMaxCount5], [mSFixMax5]
	FROM TblAiRaid
		
	SET NOCOUNT OFF

GO

