/******************************************************************************
**		Name: UspResetUnitedGuildWarRecord
**		Desc: 烹钦 辨靛 措傈 扁废阑 檬扁拳 茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspResetUnitedGuildWarRecord
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	BEGIN TRAN
		DELETE dbo.TblUnitedGuildWarGuildInfo;
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END 			

		DELETE dbo.TblUnitedGuildWarPcInfo;

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END 			

	COMMIT TRAN

	RETURN(0)

GO

