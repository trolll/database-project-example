/******************************************************************************
**		Name: UspDelUnitedGuildWarPcInfo
**		Desc: 烹钦 辨靛 措傈 某腐磐 何啊 沥焊甫 昏力茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE dbo.UspDelUnitedGuildWarPcInfo
	@pSvrNo			SMALLINT,		-- 辑滚 锅龋
	@pGuildNo		INT,			-- 辨靛 锅龋
	@pPcNo			INT				-- 某腐磐 锅龋 (0锅牢 版快 秦寸 辨靛盔 傈何 昏力茄促.)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	IF(0 = @pPcNo)
	 BEGIN
		DELETE	dbo.TblUnitedGuildWarPcInfo 
		WHERE [mSvrNo] = @pSvrNo 
			AND [mGuildNo] = @pGuildNo;
	 END
	ELSE
	 BEGIN
		DELETE	dbo.TblUnitedGuildWarPcInfo 
		WHERE [mSvrNo] = @pSvrNo 
			AND [mGuildNo] = @pGuildNo 
			AND [mPcNo] = @pPcNo;
	 END

GO

