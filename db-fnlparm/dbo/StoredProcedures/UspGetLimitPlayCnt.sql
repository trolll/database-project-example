/******************************************************************************
**		File: 
**		Name: UspGetLimitPlayCnt
**		Desc: 
**
**		Auth: 沥柳宽
**		Date: 2016-09-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetLimitPlayCnt]
	@pType	TINYINT	-- 鸥涝(烹辨傈 1, 评珐农 2)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT [mSvrNo], [mNo], [mPlayCnt] FROM dbo.TblLimitPlayCnt WHERE [mType] = @pType
			
	SET NOCOUNT OFF;

GO

