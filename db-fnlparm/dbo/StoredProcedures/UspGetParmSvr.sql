/******************************************************************************  
**  Name: UspGetParmSvr  
**  Desc: 阿 辑滚喊 汲沥沥焊甫 啊廉咳 -> 辑滚 殿废咯何 眉农  
**  Warn: 辑滚埃 矫埃悼扁拳绰 葛电 辑滚甸捞 龋免窍绰 捞镑俊辑 矫埃阑 逞败辑 悼扁拳茄促.  
**  
**  Auth:   
**  Date:   
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:     Description:   
**  -------- --------   ---------------------------------------  
**  2007-12-27 辫堡挤    SELECT 亲格俊 mSupportCoupon 拿烦 眠啊  
**  2008-01-02 辫堡挤    SELECT 亲格俊 mAppearItemLv  拿烦 眠啊  
**  2008.04.30 JUDY    mSupportType  
**  2009.01.30 辫堡挤    msvrinfo column 眠啊  
**  2009.12.09 辫锐档    mIsInputDlg column 眠啊  
**  2011.06.17 辫锐档    mSmallSendCnt column 眠啊  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmSvr]  
  @pType  TINYINT  
 ,@pMajorIp CHAR(15)  
AS  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
  
  
 DECLARE @aCur DATETIME  
    SET  @aCur = GETDATE()  
 SELECT [mSvrNo], [mWorldNo], [mThdWkCnt], [mThdTmCnt], [mThdDbCnt], [mThdLogCnt], [mSessionCnt], [mSendCnt], [mSmallSendCnt]  
    ,[mTcpPort], [mUdpPort], @aCur, [mMajorVer], [mMinorVer], RTRIM([mDesc]), [mIsSiege]  
    , CASE WHEN ([mEvtStx] <= @aCur) AND (@aCur < [mEvtEtx]) THEN 1 ELSE 0 END   
    , mIsCheckRSC  
    , mNationID  
    , mBullet  
    , mSupportType   
    , mSvrInfo  
    , mIsInputDlg  
 FROM dbo.TblParmSvr  
 WHERE (mIsValid <> 0)   
   AND (@pType = [mType])   
   AND (@pMajorIp = [mMajorIp]);

GO

