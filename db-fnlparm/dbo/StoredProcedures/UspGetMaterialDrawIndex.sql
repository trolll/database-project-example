/******************************************************************************
**		Name: UspGetMaterialDrawIndex
**		Desc: 매터리얼 뽑기 시스템의 목차 테이블을 얻는다
**
**		Auth: 김희도
**		Date: 2009.03.300
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                수정일           수정자                             수정내용    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMaterialDrawIndex]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		MDID,
		MDRD,
		mResType,
		mMaxResCnt,
		mSuccess,
		mDesc
	FROM
		dbo.TblMaterialDrawIndex

GO

