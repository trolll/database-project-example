/******************************************************************************
**		Name: UspGetExpireEventItem
**		Desc: 만료된 아이템 정보를 반환한다.
**			- UfnGetExpireEventItem --> 함수의 연결 서버 작동이 안 되는 문제 발생
**			- 연결서버가 접근할 수 있는 범위는 TABLE, VIEW, PROCEDURE 단위	
**
**		Auth: JUDY
**		Date: 2008.08.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20090423	JUDY				이벤트 전용 아이템만 삭제 가능    
**		20100211	JUDY				이벤트 기간 종료 버그로 인한 버그 수정      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetExpireEventItem]  	
	@pSvrNo	SMALLINT
	,@pDate	SMALLDATETIME	-- 현재일
	,@pArrItem	VARCHAR(8000) OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SET @pArrItem = '';	
		
	SELECT 
		@pArrItem = CONVERT(VARCHAR,mObjID) + ',' + @pArrItem
	FROM (
	SELECT mObjID			-- 이벤트 종료일은 입력값으로 정리 제한 
	FROM dbo.TblParmSvrSupportEvent T1
		INNER JOIN dbo.TblEventObj T2
			ON T1.mEventID = T2.mEventID
	WHERE T1.mEndDate < @pDate		
		and T1.msvrno=@pSvrNo
		and T2.mObjType = 1	-- item object 
	GROUP BY mObjID ) T1
	WHERE mObjID NOT IN (	
		SELECT mObjID			-- 진행중인 이벤트는 현재 시간으로 정리
		FROM dbo.TblParmSvrSupportEvent T1
			INNER JOIN dbo.TblEventObj T2
				ON T1.mEventID = T2.mEventID
		WHERE T1.mBeginDate <= getdate()	
			and T1.mEndDate >= getdate()
			and T1.msvrno=@pSvrNo
			and T2.mObjType = 1	-- item object 
		GROUP BY mObjID 	
		UNION ALL
		SELECT IID mObjID		-- 이벤트 전용 아이템
		FROM dbo.DT_ITEM
		WHERE IIsEvent = 0	
	)

GO

