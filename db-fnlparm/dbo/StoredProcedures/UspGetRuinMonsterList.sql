/******************************************************************************  
**  File: UspGetRuinMonsterList.sql
**  Name: UspGetRuinMonsterList 
**  Desc: 企倾拳 瘤开狼 阁胶磐府胶飘 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2011.11.14	kirba		积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetRuinMonsterList]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT DISTINCT mMID
	FROM dbo.TblRuinMonsterList
	
	SET NOCOUNT OFF

GO

