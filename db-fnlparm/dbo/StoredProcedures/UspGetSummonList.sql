CREATE PROCEDURE [dbo].[UspGetSummonList]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT [mNo], [mGroupID], [mMonID], [mLevel]
		FROM TblSummonList
	SET NOCOUNT OFF

GO

