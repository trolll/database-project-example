/******************************************************************************
**		File: 
**		Name: UspGetGuardianInfo
**		Desc: 수호신 정보를 로드한다.
**
**		Auth: 김 광섭
**		Date: 2009-03-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGuardianInfo]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  mNo
		, mPosX
		, mPosY
		, mPosZ
	FROM	dbo.TblGuardian

GO

