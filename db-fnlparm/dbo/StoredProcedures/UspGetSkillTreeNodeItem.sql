/******************************************************************************  
**  File: 
**  Name: UspGetSkillTreeNodeItem
**  Desc: 胶懦 飘府 畴靛俊 器窃等 阿 饭骇狼 畴靛酒捞袍阑 肺靛茄促. 
**        阿 畴靛酒捞袍狼 炼扒档 鞍捞 肺靛茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.13 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE dbo.UspGetSkillTreeNodeItem
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	 SELECT 
		a.mSTNIID, 
		a.mSTNID, 
		a.mSPID, 
		a.mLevel, 
		b.mSTNICType, 
		b.mParamA, 
		b.mParamB, 
		b.mParamC  
	 FROM DT_SkillTreeNodeItem  a  
		LEFT JOIN DT_SkillTreeNodeItemCondition b 
			ON a.mSTNIID = b.mSTNIID  
		inner join dt_skilltreenode c 
			on a.mSTNID=c.mSTNID
		inner join tp_skilltree d 
			on d.mSTID=c.mSTID
	order by d.mDestroyOrder desc, d.mSTID asc, c.mIconSlotY asc, c.mIconSlotX asc, a.mLevel asc

GO

