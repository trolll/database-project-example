/******************************************************************************
**		Name: UspGetLevelupCoin
**		Desc: ·¹º§ ¾÷ ÁÖÈ­ Á¤º¸ È¹µæ
**		Test:			
**		Auth: ³²¼º¸ð
**		Date: 2014/02/18
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetLevelupCoin]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		mSection
		, mExp,mRewardCoin
		, mMinimumCoin
		, mType
	FROM dbo.TblLevelupCoin

GO

