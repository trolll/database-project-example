CREATE PROCEDURE dbo.UspCheckEvent
	 @pSvrNo	SMALLINT
	,@pIsEvt	BIT			OUTPUT
--------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	SET	@pIsEvt = 0
	
	DECLARE @aCur	DATETIME
	SET		@aCur = GETDATE()
	
	DECLARE @aEvtStx	DATETIME
	DECLARE @aEvtEtx	DATETIME
	SELECT @aEvtStx=[mEvtStx], @aEvtEtx=[mEvtEtx] FROM TblParmSvr WHERE ([mIsValid] <> 0) AND (@pSvrNo = [mSvrNo])
	IF(1 = @@ROWCOUNT)
	 BEGIN
		IF(@aEvtStx <= @aCur) AND (@aCur < @aEvtEtx)
		 BEGIN
			SET	@pIsEvt = 1
		 END
	 END
	
	SET NOCOUNT OFF

GO

