/******************************************************************************  
**  Name: AP_CYCLE_RANKING  
**  Desc: RANKING »êÁ¤  
**    PM 11:30 SQL JOB AGENT (SQL2000 ¹öÁ¯±îÁö È£È¯ÀÌ µÇ¾î¾ß ÇÑ´Ù)  
**      
**      
**      
**  Auth: JUDY  
**  Date: 2010.02.24  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	--------------------------------------- 
**	2013.04.03	Á¤Áø¿í		TblUnitedGuildWarHerosBattleRankingÅ×ÀÌºí [mGuildNo], [mPcNo] ÄÃ·³Ãß°¡
*******************************************************************************/  
CREATE PROCEDURE [dbo].[AP_CYCLE_RANKING]     
AS  
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	-- ·©Å· Á¤º¸¸¦ ÃÊ±âÈ­ ÇÕ´Ï´Ù.  
	TRUNCATE TABLE dbo.TblUnitedGuildWarGuildPointRanking  
	TRUNCATE TABLE dbo.TblUnitedGuildWarVictoryRanking  
	TRUNCATE TABLE dbo.TblUnitedGuildWarKillRanking  
	TRUNCATE TABLE dbo.TblUnitedGuildWarHerosBattleRanking   
  
	if exists ( select * from sysobjects where name ='temp_ranking_pc' and type = 'u')  
	begin  
		drop table dbo.temp_ranking_pc  
	end   

	if exists ( select * from sysobjects where name ='temp_ranking_guild' and type = 'u')  
	begin  
		drop table dbo.temp_ranking_guild  
	end

	SELECT
		mSvrNo  
		, MAX(mGuildNo) AS mGuildNo
		, MAX(mGuildNm) AS mGuildNm
		, mPcNo  
		, mPcNm
		, SUM(mHerosBTVictoryCnt) AS mHerosBTVictoryCnt
	INTO dbo.temp_ranking_pc
	FROM dbo.TblUnitedGuildWarPcInfo
	GROUP BY mSvrNo, mPcNo, mPcNm
  
	SELECT   
		mSvrNo  
		, mGuildNo  
		, mGuildNm  
		, mGuildPoint  
		, mKillCnt  
		, mVictoryCnt  
		, (mGuildPoint + mKillCnt + mVictoryCnt) mSum  
	INTO dbo.temp_ranking_guild  
	FROM dbo.TblUnitedGuildWarGuildInfo
  
	---- ÀÎµ¦½º¸¦ »ý¼ºÇÕ´Ï´Ù.  
	create unique index  unc_TblUnitedGuildWarPcInfo_msvrno_mpcno on dbo.temp_ranking_pc ( mSvrNo, mPcNo )  
	create index nc_TblUnitedGuildWarPcInfo_mHerosBTVictoryCnt ON dbo.temp_ranking_pc( mHerosBTVictoryCnt DESC )   
	create unique index unc_temp_ranking_guild_msvrno_mguildno ON dbo.temp_ranking_guild( msvrno, mguildno) include (mSum)  

	create unique clustered index ucl_temp_ranking_guild_msvrno_mguildno ON dbo.temp_ranking_guild( msvrno, mguildno)  
	create index nc_temp_ranking_guild_mguildpoint on dbo.temp_ranking_guild( mGuildPoint DESC, mSum DESC )  
	create index nc_temp_ranking_guild_mKillCnt on dbo.temp_ranking_guild( mKillCnt DESC, mSum DESC )  
	create index nc_temp_ranking_guild_mVictoryCnt on dbo.temp_ranking_guild( mVictoryCnt DESC, mSum DESC )  
  
  -- 1 . WarHerosBattleRanking   
	INSERT INTO dbo.TblUnitedGuildWarHerosBattleRanking ( mRegDate, mSvrNo, mGuildNo, mGuildNm, mVictoryCnt, mPcNo, mPcNm )  
	SELECT   
		TOP 50  
		GETDATE() mRegDate  
		, T1.mSvrNo  
		, T1.mGuildNo  
		, T1.mGuildNm  
		, mHerosBTVictoryCnt  
		, mPcNo   
		, mPcNm  
	FROM (  
		SELECT   
			TOP 70   
			mSvrNo  
			, mGuildNo  
			, mGuildNm  
			, mPcNo  
			, mPcNm  
			, mHerosBTVictoryCnt    
		FROM dbo.temp_ranking_pc  
		WHERE mHerosBTVictoryCnt > 0  
		ORDER BY mHerosBTVictoryCnt DESC  
	) T1  
	LEFT OUTER JOIN dbo.temp_ranking_guild T2  
		ON T1.mSvrNo = T2.mSvrNo  
		AND T1.mGuildNo = T2.mGuildNo
	ORDER BY T1.mHerosBTVictoryCnt DESC, T2.mSum DESC  
  
	-- 2. UnitedGuildWarGuildPointRanking  
	INSERT INTO dbo.TblUnitedGuildWarGuildPointRanking( mRegDate, mSvrNo, mGuildNm, mGuildPoint, mGuildNo )  
	SELECT TOP 50   
		GETDATE() mRegDate, mSvrNo, mGuildNm, mGuildPoint, mGuildNo  
	FROM dbo.temp_ranking_guild with(index=nc_temp_ranking_guild_mguildpoint)  
	WHERE mGuildPoint > 0   
	ORDER BY mGuildPoint DESC, mSum DESC   
  
	-- 3. TblUnitedGuildWarKillRanking  
	INSERT INTO dbo.TblUnitedGuildWarKillRanking( mRegDate, mSvrNo, mGuildNm, mKillCnt )  
	SELECT TOP 50   
		GETDATE() mRegDate, mSvrNo, mGuildNm, mKillCnt  
	FROM dbo.temp_ranking_guild with(index=nc_temp_ranking_guild_mKillCnt)  
	WHERE mKillCnt > 0   
	ORDER BY mKillCnt DESC, mSum DESC   
  
	-- 4. dbo.TblUnitedGuildWarVictoryRanking  
	INSERT INTO dbo.TblUnitedGuildWarVictoryRanking( mRegDate, mSvrNo, mGuildNm, mVictoryCnt )  
	SELECT TOP 50   
		GETDATE() mRegDate, mSvrNo, mGuildNm, mVictoryCnt  
	FROM dbo.temp_ranking_guild with(index=nc_temp_ranking_guild_mVictoryCnt)  
	WHERE mVictoryCnt > 0   
	ORDER BY mVictoryCnt DESC, mSum DESC

GO

