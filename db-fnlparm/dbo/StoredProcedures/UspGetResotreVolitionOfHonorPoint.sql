/******************************************************************************
**		File: 
**		Name: UspGetResotreVolitionOfHonorPoint
**		Desc: 회복 가능한 명예의 의지 포인트 량을 얻어온다.
**
**		Auth: 김 광섭
**		Date: 2009-04-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: Author:	
**		Description: 
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetResotreVolitionOfHonorPoint]
	   @pPcNo		INT
	 , @pSvrNo		SMALLINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		[mRestoreVolitionOfHonor]
	FROM	dbo.TblCommonPoint
	WHERE	mSvrNo = @pSvrNo
				AND mPcNo = @pPcNo

GO

