CREATE PROCEDURE dbo.UspDelLimitedResource
	@pResourceType	INT
AS	
	SET NOCOUNT ON
	
	DELETE dbo.TblLimitedResource
	WHERE mResourceType = @pResourceType
	
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0
	BEGIN
		RETURN(1)	-- db error
	END

	RETURN(0)

GO

