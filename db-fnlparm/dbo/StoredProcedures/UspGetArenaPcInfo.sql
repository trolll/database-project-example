/******************************************************************************
**		Name: UspGetArenaPcInfo
**		Desc: 酒饭唱 PC沥焊
**
**		Auth: 沥柳龋
**		Date: 2016-09-29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaPcInfo]
	@pSvrNo				SMALLINT,	-- 辑滚 锅龋    
	@pPcNo				INT,		-- 某腐磐 锅龋
	@pPlayTime				INT				OUTPUT,			-- 敲饭捞矫埃    
	@pPanalty				INT				OUTPUT,			-- 菩澄萍 汲沥
	@pRegDate				SMALLDATETIME	OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SET		@pPlayTime = 0
	SET		@pPanalty = 0
	SET		@pRegDate = GETDATE()

	SELECT 
		@pPlayTime = mPlayTime,
		@pPanalty = mPanalty,
		@pRegDate = mRegDate
	FROM dbo.TblArenaPcInfo
	WHERE mSvrNo = @pSvrNo AND mPcNo = @pPcNo

	IF @@ROWCOUNT = 0 
	BEGIN
		RETURN(2)
	END

	RETURN(0)
	
	SET NOCOUNT OFF

GO

