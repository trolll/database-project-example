/******************************************************************************
**		Name: UspGetQuestCondition
**		Desc: 涅胶飘 沥焊(阁胶磐 割付府, 酒捞袍 割俺)甫 掘绢柯促. 
**
**		Auth: 沥柳龋
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetQuestCondition]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT mQuestNo, mType, mID, mCnt
    FROM dbo.TblQuestCondition

GO

