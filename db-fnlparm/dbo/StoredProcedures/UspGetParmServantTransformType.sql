/******************************************************************************
**		Name: UspGetParmServantTransformType
**		Desc: 辑锅飘 函脚 阁胶磐 鸥涝 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-04-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmServantTransformType]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT MID, TType
	FROM dbo.TblServantTransformType
	ORDER BY TType, MID

	SET NOCOUNT OFF;

GO

