
CREATE PROCEDURE dbo.UspGetParmSvrLastSupportDate  	
	@pSvrNo	SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aSupportMinute INT
	SELECT  @aSupportMinute = 0
	
	
	SELECT		
		@aSupportMinute = DATEDIFF( MI, mLastSupportDate, GETDATE() )
	FROM dbo.TblParmSvr
	WHERE mSvrNo = @pSvrNo
	
	IF @aSupportMinute IS NULL OR @aSupportMinute < 0		
		SET @aSupportMinute = 0

    RETURN(@aSupportMinute)

GO

