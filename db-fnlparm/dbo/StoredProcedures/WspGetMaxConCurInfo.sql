CREATE   PROCEDURE dbo.WspGetMaxConCurInfo
	@mYear			INT,
	@mMonth		INT,
	@mDay			INT
AS
	SET NOCOUNT ON
	DECLARE @mStart	DATETIME
	DECLARE @Sql		NVARCHAR(1000)
	DECLARE @mMaxUser	INT

	SET @mStart = STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay)

	EXEC @mMaxUser = UspGetMaxConCurinfo @mStart, 5	
	
	RETURN @mMaxUser
	SET NOCOUNT OFF

GO

