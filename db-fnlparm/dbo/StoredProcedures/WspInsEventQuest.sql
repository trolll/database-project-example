/******************************************************************************
**		Name: WspInsEventQuest
**		Desc: 捞亥飘 涅胶飘 殿废
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspInsEventQuest] 
    @pTitle				VARCHAR(100),
	@pType				TINYINT,
    @pDesc				VARCHAR(400),
	@pEQuestNo			INT	= NULL,
	@pEQuestNoOutput	INT	OUTPUT
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   

	IF (@pEQuestNo IS NULL OR @pEQuestNo= 0)
	BEGIN
		EXEC @pEQuestNo = dbo.WspGetSequence 'TblEventQuest', 'mEQuestNo'
		IF @pEQuestNo = 0 OR @pEQuestNo < 0
		BEGIN
			RETURN (1)	-- sql error 
		END 
	END 
	
	UPDATE dbo.TblEventQuest
	SET    mTitle = @pTitle
		  ,mType = @pType
		  ,mDesc = @pDesc
	WHERE  mEQuestNo = @pEQuestNo
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF (@aRowCnt <= 0)
	BEGIN
		INSERT INTO dbo.TblEventQuest (mEQuestNo, mTitle, mType, mDesc)
		SELECT @pEQuestNo, @pTitle, @pType, @pDesc

		IF (@@ERROR <> 0)
		BEGIN
			RETURN (1)
		END
	END 
	
	SET @pEQuestNoOutput = @pEQuestNo			
	RETURN(0)	-- none error

GO

