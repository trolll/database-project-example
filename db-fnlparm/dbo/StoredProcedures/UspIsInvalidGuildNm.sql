CREATE PROCEDURE dbo.UspIsInvalidGuildNm
	 @pGuildNm	CHAR(12)
--------WITH ENCRYPTION
AS
	DECLARE @aGuildNm	VARCHAR(12)
	SET @aGuildNm = RTRIM(@pGuildNm)
	
	DECLARE	@aCnt		INT
	SET @aCnt = dbo.UfnIsInvalidString(@aGuildNm)
	IF(0 = @aCnt)
	 BEGIN
		-- [String]가 VARCHAR이 아닌 CHAR이라면 RTRIM()을 해야 한다.
		SELECT @aCnt=COUNT(*) FROM TblInvalidGuildNm WHERE @aGuildNm = [mString]
	 END
	 
	RETURN @aCnt

GO

