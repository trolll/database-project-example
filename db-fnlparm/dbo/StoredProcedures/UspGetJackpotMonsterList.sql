/******************************************************************************
**		Name: UspGetJackpotMonsterList
**		Desc: 黎铺 阁胶磐 府胶飘甫 啊廉柯促.
**
**		Auth: 沥柳宽
**		Date: 2013-10-15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013.10.15	沥柳宽				积己    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetJackpotMonsterList]
AS
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
   
	SELECT [mMID], [mCamp], [mLevel], [mPoint] FROM dbo.TblJackpotMonsterList
  
	SET NOCOUNT OFF

GO

