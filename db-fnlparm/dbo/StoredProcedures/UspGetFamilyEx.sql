/******************************************************************************
**		File: UspGetFamilyEx.sql
**		Name: UspGetFamilyEx
**		Desc	: 월드번호 기준으로 같은 패밀리서버 (필드가 나머지를) 가져옴
**				: 1.Client와 Server의 역할은 서비스의 시작순서를 고려하여 결정한다.
**				:   *Field서버->채널서버 순으로 시작한다.
**				: 1.Field서버간에는 mesh형태로 연결을 하고
**				: 1.모든 Field는 모든 채널서버에 연결을 한다.
**		Warn	: 1.서버간 연결은 MinorIp가 있으면 MinorIp로 한다.
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2007.06.27	JUDY				Gateway 구성 옵션 추가 
**		2008.02.14	JUDY				mMinorIp -> mMajorIp 변경
**		2008.05.23	JUDy				mSupportType
**		2009.01.30	김광섭				msvrinfo column 추가
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetFamilyEx
	@pSvrNo	SMALLINT
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.

	DECLARE	@aType		TINYINT
	DECLARE	@aWorldNo	SMALLINT
	
	SELECT @aType=[mType], @aWorldNo=[mWorldNo] FROM TblParmSvr 
		WHERE ([mIsValid] <> 0) AND (@pSvrNo = [mSvrNo])
	IF(0 = @@ROWCOUNT)
	 BEGIN
		RAISERROR('Invalid SvrNo(%d)', 11, 1, @pSvrNo) WITH LOG
		GOTO LABEL_END
	 END

	-- ESvrType과 연결됨.
	DECLARE	@aChannel		TINYINT
	DECLARE	@aField			TINYINT	
	DECLARE	@aMgr			TINYINT
	DECLARE @aGateWay		TINYINT
	
	SET @aChannel	= 0
	SET @aField		= 1	
	SET	@aMgr		= 2
	SET @aGateWay	= 3
	
	IF(@aChannel = @aType)
	 BEGIN
		SELECT [mSvrNo], [mType], [mTcpPort], [mUdpPort], RTRIM([mMajorIp])
			  ,RTRIM([mMajorIp]), RTRIM([mDesc]), [mWorldNo], mSupportType, mSvrInfo
			FROM TblParmSvr
			WHERE ([mIsValid] <> 0) 
			  AND [mType] IN(@aMgr,@aField,@aGateWay)
			ORDER BY [mDispOrder], [mSvrNo]
	 END
	ELSE IF(@aField = @aType)
	 BEGIN
		SELECT [mSvrNo], [mType], [mTcpPort], [mUdpPort], RTRIM([mMajorIp])
			  ,RTRIM([mMajorIp]), RTRIM([mDesc]), [mWorldNo], mSupportType, mSvrInfo
			FROM TblParmSvr
			WHERE ([mIsValid] <> 0) 
			  AND (
					([mType] IN(@aMgr,@aChannel,@aGateWay))
				  OR
					(
						(@aWorldNo = [mWorldNo])
						AND 
						(@pSvrNo <> [mSvrNo])	--본인제외.
						AND
						([mType] IN(@aField))
					)
				  )
			ORDER BY [mDispOrder], [mSvrNo]
	 END
	ELSE IF(@aMgr = @aType)
	 BEGIN
		SELECT [mSvrNo], [mType], [mTcpPort], [mUdpPort], RTRIM([mMajorIp])
			  ,RTRIM([mMajorIp]), RTRIM([mDesc]), [mWorldNo], mSupportType, mSvrInfo
			FROM TblParmSvr
			WHERE ([mIsValid] <> 0)
			   AND [mType] <> @aMgr
			ORDER BY [mDispOrder], [mSvrNo]
	 END	
	ELSE	 
		RAISERROR('Invalid SvrType(%d)', 11, 1, @aType) WITH LOG		
	
LABEL_END:		
	SET NOCOUNT OFF

GO

