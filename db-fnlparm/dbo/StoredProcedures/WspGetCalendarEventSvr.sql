/******************************************************************************
**		Name: WspGetCalendarEventSvr
**		Desc: ÀÌº¥Æ® ´Þ·Â ¼­¹ö
**		Test:
			
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetCalendarEventSvr]  	
	@pSvrNo	SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T1.mEventNo
		,T1.mTitle
		,T2.mBeginDate
		,T2.mEndDate
		,mIsRunning =
			CASE WHEN mBeginDate <= GETDATE() AND mEndDate >= GETDATE() THEN 0
				ELSE 1
			END
	FROM dbo.TblCalendarEvent T1
		LEFT OUTER JOIN 
			(
				SELECT 
					  mSvrNo
					 ,mEventNo
					 ,mBeginDate
					 ,mEndDate
				FROM dbo.TblParmSvrSupportCalendarEvent
				WHERE mSvrNo = @pSvrNo
			) T2
				ON T1.mEventNo = T2.mEventNo

GO

