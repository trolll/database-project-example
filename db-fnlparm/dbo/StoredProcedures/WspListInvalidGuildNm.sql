CREATE    PROCEDURE [dbo].[WspListInvalidGuildNm]
AS
	SET NOCOUNT ON

	SELECT
		mString
	FROM
		[dbo].[TblInvalidGuildNm] WITH (NOLOCK)
	ORDER BY
		mRegDate

GO

