CREATE PROCEDURE [dbo].[WspGetMaxConCurInfo1]
	@in_date	CHAR(10)	-- 2006-10-10
AS
	SET NOCOUNT ON			-- јіён : °б°ъ ·№ДЪµе јВА» №ЭИЇ ѕИ ЅГЕІґЩ.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aSvrNoList nVARCHAR(4000),
			@aSql		nVARCHAR(4000),
			@aNewLine	VARCHAR(2),
			@aDate		DATETIME

	-- єЇјцµй ГК±вИ­
	SELECT @aSvrNoList = '',
		@aSql = '',
		@aNewline = NCHAR(13) + NCHAR(10),
		@aDate = CAST(@in_date AS DATETIME)

	-- °Л»цЗТ ј­№ц №шИЈµй °ЎБ®їА±в
	SELECT @aSvrNoList = @aSvrNoList + ', ISNULL(SUM( CASE WHEN T1.mSvrNo = ' + CAST(mSvrNo AS VARCHAR) + ' THEN mMaxUserCnt END ),0) AS ''' + CAST(mSvrNo AS VARCHAR) + ''''
	FROM
		(
			SELECT DISTINCT mSvrNo
			FROM dbo.TblConCurInfo
			WHERE mRegDate >= CAST(@in_date AS DATETIME)
				AND mRegDate <= DATEADD(DD, 1, CAST(@in_date AS DATETIME))
				
		) T1
	ORDER BY mSvrNo ASC

	-- ј­№ц №шИЈ ёс·ПїЎј­ ѕХАЗ ѕµёр ѕшґВ №®АЪїН, µЪАЗ ГСЗХ ї­ ГЯ°Ў
	SET @aSvrNoList = STUFF(@aSvrNoList, 1, 2, '') + ', ISNULL(SUM( mMaxUserCnt), 0) AS ''TOT'''

	-- µїАыSQLАЫјє
	SET @aSql = 
		N'SELECT mHour, ' + @aSvrNoList + @aNewline +
		N'FROM (' + @aNewline +
		N'		SELECT mCode AS mHour, mSvrNo, MAX(mUserCnt) AS mMaxUserCnt' + @aNewline +
		N'		FROM (' + @aNewline +
		N'			SELECT DATEPART(hh, mRegDate) AS mHour, mSvrNo, mUserCnt' + @aNewline +
		N'			FROM dbo.TblConCurInfo' + @aNewline +
		N'			WHERE mRegDate >= @in_Date' + @aNewline +
		N'				AND mRegDate <= DATEADD(DD, 1, @in_Date)' + @aNewline +
		N'			) T1' + @aNewline +
		N'			RIGHT OUTER JOIN dbo.TblCode T2' + @aNewline +
		N'				ON T1.mHour = T2.mCode' + @aNewline +
		N'		WHERE T2.mCodeType = 1' + @aNewline +
		N'		GROUP BY mCode, mSvrNo' + @aNewline +
		N'		) T1' + @aNewline +
		N'GROUP BY mHour WITH ROLLUP' + @aNewline +
		N'' + @aNewline +
		N''

	-- PRINT @aSql

	-- µїАы SQLЅЗЗа
	exec sp_executesql @aSql, N'@in_Date DATETIME', @in_Date = @aDate

GO

