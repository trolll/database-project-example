CREATE PROCEDURE dbo.UspGetTreasureBoxForOne
	@pMID		INT,
	@pType		SMALLINT
AS
	SET NOCOUNT ON

	SELECT 
		mIID
		, mAmount
		, mDice
		, mStatus 
	FROM dbo.TblTreasureBox 
	WHERE mMID = @pMID 
		AND mType = @pType 
	ORDER BY mDice DESC

GO

