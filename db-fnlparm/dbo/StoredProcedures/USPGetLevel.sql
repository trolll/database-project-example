CREATE PROCEDURE  USPGetLevel as 


select count(*) total ,mlevel from ( 

select case 
when mlevel>=1 and mlevel<=10  then '1-10'
when mlevel>=11 and mlevel<=20  then '11-20' 
when mlevel>=21 and mlevel<=30  then '21-30' 
when mlevel>=31 and mlevel<=40  then '31-40' 
when mlevel>=41 and mlevel<=50  then '41-50' 
when mlevel>=51 and mlevel<=60  then '51-60'
end as mlevel
from sta_tblpcstate where 
mno in 

( select mno from  sta_tblpc b, sta_tbluser c 
where b.mOwner=c.muserno and  ( c.muserid like 'cn%' or c.muserid like 'kr%' ) 
and  b.createdate='20070725' and c.createdate='20070725' 
and 
b.mdeldate is null and c.mdeldate='1900-01-01'
)

and 
createdate='20070725'

) a
group by mlevel

GO

