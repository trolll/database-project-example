/******************************************************************************  
**  File: UspGetRuinSpotSector.sql
**  Name: UspGetRuinSpotSector 
**  Desc: 企倾拳 瘤开狼 裹困 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2011.11.14	kirba		积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetRuinSpotSector]
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		mSpotPlace,
		mStartX,
		mStartZ,
		mEndX,
		mEndZ
	FROM dbo.TblRuinSpotRange
	
	SET NOCOUNT OFF

GO

