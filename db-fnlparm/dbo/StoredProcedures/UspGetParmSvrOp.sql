/******************************************************************************  
**  File: UspGetParmSvrOp.sql  
**  Name: UspGetParmSvrOp  
**  Desc: 辑滚 可记 扁瓷  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:			Description:  
**  --------	--------		---------------------------------------  
**  2007-11-07	soundkey		mOpValue2 ~ mOpValue5 add  
**  2008-01-15	Judy			mOpValue6 ~ mOpValue10 add   
**  2008.05.07	JUDY			辑滚喊 瘤盔 沥焊 眠啊  
**  20090520	辫 堡挤			眠啊等 可记拿烦阑 SELECT 亲格俊 眠啊.  
**  20130513	沥 柳宽			mOpValue21 ~ mOpValue30 眠啊, 眠啊等 可记拿烦阑 SELECT 亲格俊 眠啊.  
*******************************************************************************/   
CREATE	PROCEDURE [dbo].[UspGetParmSvrOp]
	@pSvrNo SMALLINT  
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	SELECT
		mOpNo  
		,mIsSetup  
		,mOpValue1  
		,mOpValue2  
		,mOpValue3  
		,mOpValue4  
		,mOpValue5  
		,mOpValue6  
		,mOpValue7  
		,mOpValue8  
		,mOpValue9  
		,mOpValue10  
		,mOpValue11  
		,mOpValue12  
		,mOpValue13  
		,mOpValue14  
		,mOpValue15  
		,mOpValue16  
		,mOpValue17  
		,mOpValue18  
		,mOpValue19  
		,mOpValue20
		,mOpValue21
		,mOpValue22
		,mOpValue23
		,mOpValue24
		,mOpValue25
		,mOpValue26
		,mOpValue27
		,mOpValue28
		,mOpValue29
		,mOpValue30  
  FROM dbo.TblParmSvrOp  
  WHERE mSvrNo = @pSvrNo

GO

