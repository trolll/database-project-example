/******************************************************************************  
**  Name: UspListBeadEffect  
**  Desc: 备浇 瓤苞 啊廉坷扁
**  
**                
**  Return values:  
**   饭内靛悸
**                
**  Author: 辫碍龋
**  Date: 2011-06-10
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspListBeadEffect]
AS
	SET NOCOUNT ON	
	
	SELECT 
	mBeadNo
	,mBeadType
	,mChkGroup
	,mPercent
	,mApplyTarget
	,mParamA
	,mParamB
	,mParamC
	,mParamD
	,mParamE
	FROM DT_BeadEffect
	UNION ALL
	SELECT 
	mBeadNo
	,0 as mBeadType
	,0 as mChkGroup
	,0 as mPercent
	,0 as mApplyTarget
	,mParamA
	,mParamB
	,mParamC
	,mParamD
	,mParamE
	FROM DT_BeadEffectParm 
	order by mBeadType DESC

GO

