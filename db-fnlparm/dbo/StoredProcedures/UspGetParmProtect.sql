/******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.06.16	辫碍龋				SDPV, SMPV, SRPV 眠啊
										SDDV, SMDV, SRDV 眠啊
										SHitPlus, SDDPlus 昏力
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmProtect]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON;
	
	SELECT 
		a.[SID], a.[SType], a.[SLevel], 
		-- a.[SHitPlus], a.[SDDPlus],	-- 昏力凳
		a.[SDDV], a.[SMDV], a.[SRDV],
		a.[SDPV], a.[SMPV], a.[SRPV],
		RTRIM(b.[SName])
	FROM dbo.DT_Protect AS a 
		INNER JOIN dbo.TP_SlainType AS b 
			ON(a.[SType] = b.[SType])

GO

