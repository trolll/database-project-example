CREATE PROCEDURE  USPGetmTotUseTm as


select count(*) total ,mTotUseTm from ( 

SELECT    case 
when mTotUseTm/60>=0 and mTotUseTm/60<1  then '0-1'
when mTotUseTm/60>=1 and mTotUseTm/60<2 then '1-2' 
when mTotUseTm/60>=2 and mTotUseTm/60<3  then '2-3' 
when mTotUseTm/60>=3 and mTotUseTm/60<4  then '3-4' 
when mTotUseTm/60>=4 and mTotUseTm/60<5  then '4-5' 
when mTotUseTm/60>=5 and mTotUseTm/60<6  then '5-6' 
when mTotUseTm/60>=6 and mTotUseTm/60<7  then '6-7'
when mTotUseTm/60>=7 and mTotUseTm/60<8  then '7-8'
when mTotUseTm/60>=8 and mTotUseTm/60<9  then '8-9'
when mTotUseTm/60>=9 and mTotUseTm/60<10  then '9-10'
when mTotUseTm/60>=10  then '10-more'

end as mTotUseTm
FROM         sta_TblUser where  mDelDate='1900-1-1' and createdate='20070725') a 

group by mTotUseTm
order by total

GO

