CREATE PROCEDURE dbo.UspGetParmExpTable
AS
	SET NOCOUNT ON;

	SELECT 
		ELevel, EExp, ERestExpRate
	FROM dbo.DT_Exp
	ORDER BY ELevel
		
	SET NOCOUNT OFF

GO

