CREATE PROCEDURE dbo.UspSetDiscipleEtcInfo
	 @pType		INT
	,@pValue		INT

AS
	SET NOCOUNT ON	

	DECLARE @aRv	INT
	DECLARE @aErrNo	INT
	SET @aRv = 0

	IF EXISTS(SELECT mType FROM dbo.DT_DiscipleEtcInfo WHERE mType = @pType)
	BEGIN
		UPDATE dbo.DT_DiscipleEtcInfo SET mValue = @pValue WHERE mType = @pType
		SET @aErrNo = @@ERROR
		IF @aErrNo <> 0
		BEGIN
			SET @aRv = 1		-- 1 : DB 내부적인 오류
		END
	END
	ELSE
	BEGIN
		SET @aRv = 2		-- 2 : 해당 사제모임 정보타입이 존재하지 않음
	END

GO

