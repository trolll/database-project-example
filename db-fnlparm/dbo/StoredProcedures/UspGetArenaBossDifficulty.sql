/******************************************************************************
**		Name: UspGetArenaBossDifficulty
**		Desc: 焊胶傈 焊胶 抄捞档
**
**		Auth: 沥柳龋
**		Date: 2016-10-04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaBossDifficulty]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT   
			 mPointGrade
			,mMinPoint
			,mMaxPoint
			,mMinD
			,mMaxD
			,mHIT
			,mHP
			,mDPV
			,mMPV
			,mRPV
			,mDDV
			,mMDV
			,mRDV
			,mSkillDmg
	FROM
		dbo.TblArenaBossDifficulty

GO

