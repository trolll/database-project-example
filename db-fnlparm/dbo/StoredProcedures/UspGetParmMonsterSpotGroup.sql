/******************************************************************************  
**  File: UspGetParmMonsterSpotGroup.sql
**  Name: UspGetParmMonsterSpotGroup 
**  Desc: 阁胶磐 胶铺 弊缝 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2011.11.14	kirba		MonsterID档 掘霸 函版
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetParmMonsterSpotGroup]
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT 
		T1.mGID, T1.mPosX, T1.mPosY, T1.mPosZ, T1.mRadius, T2.mMID
	FROM TblMonsterSpotGroup T1
	join TblMonsterSpot T2 on T1.mGID = T2.mGID
	
	
	SET NOCOUNT OFF

GO

