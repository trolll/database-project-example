/******************************************************************************
**		Name: UspGetServantSkillTree
**		Desc: ¼­¹øÆ® ½ºÅ³ Æ®¸® °¡Á®¿À±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-10-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantSkillTree]
	@pIID		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT SStep, STID1, STID2, STID3
	FROM dbo.TblServantSkillTree
	WHERE IID = @pIID
	ORDER BY SStep DESC

	SET NOCOUNT OFF;

GO

