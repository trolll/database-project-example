CREATE      PROCEDURE [dbo].[WspGetConCurInfo] 
	@mYear		INT,
	@mMonth	INT,
	@mDay		INT,
	@mCnt		INT
AS

SET NOCOUNT ON
	
	DECLARE @mStartDate	DATETIME
	DECLARE @mSvrCnt 	INT
	DECLARE @mEndDate	DATETIME
	DECLARE @mTempDate	DATETIME
	DECLARE @Sql		NVARCHAR(1000)
	DECLARE @mTable	VARCHAR(20)

	SET @mStartDate = STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay)
	SET @mEndDate = DATEADD(dd, 1, @mStartDate)

	CREATE TABLE #Tmp1 (mRegTi DATETIME, mTSvrNo INT, mUserCnt INT)	

	WHILE(@mStartDate < @mEndDate)
	 BEGIN
		INSERT INTO #Tmp1 (mRegTi, mTSvrNo, mUserCnt)
		SELECT
			mRegDate, ISNULL(mSvrNo, 0) AS mSvrNo, ISNULL(mUserCnt, 0) AS mUserCnt
		FROM
			TblConCurInfo WITH (NOLOCK)
		WHERE
			mRegDate BETWEEN @mStartDate AND DATEADD(MI, 5, @mStartDate) AND mSvrNo = (@mCnt + 100)

		SET @mStartDate = DATEADD(MI, 60, @mStartDate)
	 END

	SELECT mUserCnt FROM #Tmp1

	SET NOCOUNT OFF

GO

