/******************************************************************************
**		Name: WspGetConsumCategory
**		Desc: 
**
**		Auth: JUDY
**		Date: 2010-12-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetConsumCategory]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	
	SELECT 
		mCode				-- 墨抛绊府 锅龋
		, mCodeDesc		-- 墨抛绊府疙
	FROM dbo.TblCode
	WHERE mCodeType = 7

GO

