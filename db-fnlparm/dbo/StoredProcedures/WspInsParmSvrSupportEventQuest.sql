/******************************************************************************
**		Name: WspInsParmSvrSupportEventQuest
**		Desc: 捞亥飘 涅胶飘 辑滚 殿废
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspInsParmSvrSupportEventQuest]  	
	@pSvrNo		SMALLINT,
    @pEQuestNo	INT,
    @pBeginDate SMALLDATETIME,
    @pEndDate	SMALLDATETIME
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	INSERT INTO dbo.TblParmSvrSupportEventQuest (mSvrNo, mEQuestNo, mBeginDate, mEndDate)
	SELECT @pSvrNo, @pEQuestNo, @pBeginDate, @pEndDate
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF (@aErrNo <> 0)
	BEGIN 
		RETURN(1)
	END

    RETURN(0)

GO

