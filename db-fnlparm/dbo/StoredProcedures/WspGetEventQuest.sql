/******************************************************************************
**		Name: WspGetEventQuest
**		Desc: 捞亥飘 涅胶飘
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetEventQuest]  
	@pEQuestNo	INT		= NULL	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF @pEQuestNo IS NULL
	BEGIN
		SELECT 
			mEQuestNo, mTitle, mType, mDesc
		FROM dbo.TblEventQuest
	
		RETURN
	END
	
	SELECT 
		mEQuestNo, mTitle, mType, mDesc
	FROM dbo.TblEventQuest	
	WHERE mEQuestNo = @pEQuestNo

GO

