/******************************************************************************
**		Name: UspGetServantAbilityBase
**		Desc: ¼­¹øÆ® ±âº» ´É·ÂÄ¡ °¡Á®¿À±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-10-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantAbilityBase]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT SCategory, SStrength, SDexterity, SInteligence
	FROM dbo.TblServantAbilityBase
	ORDER BY SCategory

	SET NOCOUNT OFF;

GO

