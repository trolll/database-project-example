/******************************************************************************
**		Name: UspGetQuestInfo
**		Desc: 涅胶飘 沥焊(涅胶飘辆幅, 蔼)甫 掘绢柯促. 
**
**		Auth: 沥柳龋
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetQuestInfo]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT mQuestNo, mType, mParmA, mParmB, mParmC
    FROM dbo.TblQuestInfo

GO

