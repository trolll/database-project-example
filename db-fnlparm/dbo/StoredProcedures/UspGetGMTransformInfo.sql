
/******************************************************************************
**		Name: UspGetGMTransformInfo
**		Desc: GM 函脚府胶飘 概莫 惑怕捞惑
**
**		Auth: 傍籍痹
**		Date: 2016-11-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGMTransformInfo]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mGroupID, AID
	FROM dbo.TP_GMTransformInfo

	SET NOCOUNT OFF;

GO

