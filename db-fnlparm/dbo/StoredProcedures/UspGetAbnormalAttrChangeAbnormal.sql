CREATE PROCEDURE dbo.UspGetAbnormalAttrChangeAbnormal
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT

		T1.mOriginAID,
		T1.mEffectAID,
		T1.mConditionType,
		T1.mConditionAID,
		T1.mConditionAType
		
	FROM	dbo.DT_AbnormalAttrChangeAbnormal	T1
		INNER JOIN dbo.DT_Abnormal		T2
			ON	T1.mOriginAID = T2.AID

GO

