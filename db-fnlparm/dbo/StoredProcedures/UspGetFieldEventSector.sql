/******************************************************************************
**		Name: UspGetFieldEventSector
**		Desc: 鞘靛 捞亥飘 柳青 冀磐沥焊 八祸
**		Test:			
**		Auth: 巢己葛
**		Date: 2013/10/31
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetFieldEventSector]
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT	[mEventType], [mSectorType], [mX], [mZ]
	FROM dbo.TblFieldEventSector

	SET NOCOUNT OFF

GO

