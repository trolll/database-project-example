CREATE PROCEDURE dbo.UspGetMaxConCurInfo
	@in_Date  CHAR(10)	-- 2006-10-10
AS	
	SELECT 
		-- 시간대별 추출
		mHour,
		-- 추가되는 서버는 여기에서 등록 
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 101 THEN mMaxUserCnt END ),0) AS '101',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 102 THEN mMaxUserCnt END ), 0) AS '102',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 103 THEN mMaxUserCnt END ), 0) AS '103',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 104 THEN mMaxUserCnt END ), 0) AS '104',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 105 THEN mMaxUserCnt END ), 0) AS '105',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 106 THEN mMaxUserCnt END ), 0) AS '106',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 107 THEN mMaxUserCnt END ), 0) AS '107',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 108 THEN mMaxUserCnt END ), 0) AS '108',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 109 THEN mMaxUserCnt END ), 0) AS '109',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 110 THEN mMaxUserCnt END ), 0) AS '110',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 111 THEN mMaxUserCnt END ), 0) AS '111',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 121 THEN mMaxUserCnt END ), 0) AS '121',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 122 THEN mMaxUserCnt END ), 0) AS '122',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 123 THEN mMaxUserCnt END ), 0) AS '123',		
		ISNULL(SUM( mMaxUserCnt), 0) AS 'TOT'
	FROM (
		SELECT 
			T1.mSvrNo AS mSvrNo,
			T1.mCode AS mHour,
			ISNULL(T2.mMaxUserCnt, 0) AS mMaxUserCnt
		FROM (
			SELECT 
				T1.mCode, 
				mSvrNo
			FROM dbo.TblCode T1
				CROSS JOIN  (
					SELECT mSvrNo
					FROM dbo.TblConCurInfo WITH(NOLOCK)
						WHERE mRegDate >= CONVERT(DATETIME, @in_Date )
							AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_Date ) )
					GROUP BY mSvrNo	
				) T2
			WHERE mCodeType = 1 
		) T1 LEFT OUTER JOIN 
			(
				SELECT 
					mSvrNo, 
					DATEPART(hh, mRegDate) AS mHour,
					MAX(mUserCnt) AS mMaxUserCnt
				FROM dbo.TblConCurInfo WITH(NOLOCK)
					WHERE mRegDate >= CONVERT(DATETIME, @in_Date )
						AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_Date ) )
				GROUP BY mSvrNo, DATEPART(hh, mRegDate)
			) T2
		ON T1.mSvrNo = T2.mSvrNo AND T1.mCode = T2.mHour
	) T1	
	GROUP BY mHour  WITH ROLLUP

GO

