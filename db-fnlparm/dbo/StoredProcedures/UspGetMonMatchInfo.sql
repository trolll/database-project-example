/******************************************************************************  
**  File: UspGetMonMatchInfo.sql
**  Name: UspGetMonMatchInfo
**  Desc: 阁胶磐 概摹 沥焊 掘扁
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2012.10.09	沥柳龋		积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetMonMatchInfo]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	SELECT 
		mPlace,
		mStartX,
		mStartZ,
		mEndX,
		mEndZ
	FROM dbo.TblMonMatchInfo
END

GO

