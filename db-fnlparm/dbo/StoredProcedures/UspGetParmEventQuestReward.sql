/******************************************************************************
**		Name: UspGetParmEventQuestReward
**		Desc: 捞亥飘 涅胶飘 沥焊
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmEventQuestReward]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		mEQuestNo
		, mRank
		, mIID
		, mStatus
		, mBinding
		, mEffTime
		, mValTime
		, mCnt
	FROM dbo.TblEventQuestReward AS T1
		INNER JOIN	dbo.DT_ITEM AS T2
	ON	T1.mIID = T2.IID

GO

