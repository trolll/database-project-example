/******************************************************************************
**		File: 
**		Name: UspGetChaosBattleInfo
**		Desc: 카오스 배틀 서버에서 사용할 필드 서버별 정보를 로드한다.
**
**		Auth: 김 광섭
**		Date: 2009-02-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetChaosBattleInfo]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  [mSvrNo]
		, RTRIM([mCharNmPrefix]) mCharNmPrefix
		, RTRIM([mCharNickNm]) mCharNickNm
		, [mMark]
		, [mMarkSeq]
	FROM	dbo.TblChaosBattleInfo
			
	SET NOCOUNT OFF;

GO

