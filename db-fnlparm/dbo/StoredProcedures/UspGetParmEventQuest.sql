/******************************************************************************
**		Name: UspGetParmEventQuest
**		Desc: 捞亥飘 涅胶飘 沥焊
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetParmEventQuest]
	@pSvrNo					SMALLINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		mEQuestNo
		,mType
		,mParmID
		,mParmA
		,mParmB
		,mParmC
		,mParmD
	FROM dbo.TblEventQuestCondition
	WHERE mEQuestNo IN (
		SELECT  T1.mEQuestNo
		FROM dbo.TblParmSvrSupportEventQuest as T1
			INNER JOIN dbo.TblEventQuestCondition as T2
				ON T1.mEQuestNo = T2.mEQuestNo
			INNER JOIN dbo.TblEventQuest as T3
				ON T2.mEQuestNo = T3.mEQuestNo AND T2.mType = T3.mType
		WHERE T1.mSvrNo = @pSvrNo
			AND T1.mBeginDate <= GETDATE()
			AND T1.mEndDate >= GETDATE() )

GO

