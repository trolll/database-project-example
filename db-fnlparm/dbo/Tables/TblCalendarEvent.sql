CREATE TABLE [dbo].[TblCalendarEvent] (
    [mEventNo] INT           NOT NULL,
    [mTitle]   VARCHAR (30)  NOT NULL,
    [mDesc]    VARCHAR (512) NOT NULL,
    [mURL]     VARCHAR (200) NULL,
    CONSTRAINT [UCL_PK_TblCalendarEvent_mEventNo] PRIMARY KEY CLUSTERED ([mEventNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼³¸í', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarEvent', @level2type = N'COLUMN', @level2name = N'mDesc';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'ÀÌº¥Æ® ´Þ·Â', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarEvent';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarEvent', @level2type = N'COLUMN', @level2name = N'mURL';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÌº¥Æ® ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarEvent', @level2type = N'COLUMN', @level2name = N'mEventNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Á¦¸ñ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarEvent', @level2type = N'COLUMN', @level2name = N'mTitle';


GO

