CREATE TABLE [dbo].[TblUnitedGuildWarCastleGate] (
    [mRegDate]  DATETIME   DEFAULT (getdate()) NOT NULL,
    [mNo]       BIGINT     NOT NULL,
    [mPosX]     FLOAT (53) NOT NULL,
    [mPosY]     FLOAT (53) NOT NULL,
    [mPosZ]     FLOAT (53) NOT NULL,
    [mWidth]    FLOAT (53) NOT NULL,
    [mDir]      FLOAT (53) NOT NULL,
    [mDesc]     CHAR (50)  NOT NULL,
    [mGroup]    BIGINT     NOT NULL,
    [mGateType] BIGINT     CONSTRAINT [DF_TblUnitedGuildWarCastleGate_mGateType] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblUnitedGuildWarCastleGate] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

