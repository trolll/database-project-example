CREATE TABLE [dbo].[TblTransformList] (
    [mNo]      INT     NOT NULL,
    [mGroupID] INT     NOT NULL,
    [mMonID]   INT     NOT NULL,
    [mLevel]   INT     CONSTRAINT [DF_TblTransformList_mLevel] DEFAULT (0) NOT NULL,
    [mEq0]     TINYINT CONSTRAINT [DF_TblTransformList_mEq0] DEFAULT (0) NOT NULL,
    [mEq1]     TINYINT CONSTRAINT [DF_TblTransformList_mEq1] DEFAULT (0) NOT NULL,
    [mEq2]     TINYINT CONSTRAINT [DF_TblTransformList_mEq2] DEFAULT (0) NOT NULL,
    [mEq3]     TINYINT CONSTRAINT [DF_TblTransformList_mEq3] DEFAULT (0) NOT NULL,
    [mEq4]     TINYINT CONSTRAINT [DF_TblTransformList_mEq4] DEFAULT (0) NOT NULL,
    [mEq5]     TINYINT CONSTRAINT [DF_TblTransformList_mEq5] DEFAULT (0) NOT NULL,
    [mEq6]     TINYINT CONSTRAINT [DF_TblTransformList_mEq6] DEFAULT (0) NOT NULL,
    [mEq7]     TINYINT CONSTRAINT [DF_TblTransformList_mEq7] DEFAULT (0) NOT NULL,
    [mEq8]     TINYINT CONSTRAINT [DF_TblTransformList_mEq8] DEFAULT (0) NOT NULL,
    [mEq9]     TINYINT CONSTRAINT [DF_TblTransformList_mEq9] DEFAULT (0) NOT NULL,
    [mEq10]    TINYINT CONSTRAINT [DF_TblTransformList_mEq10] DEFAULT (0) NOT NULL,
    [mControl] INT     CONSTRAINT [DF_TblTransformList_mControl] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblTransformList] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

