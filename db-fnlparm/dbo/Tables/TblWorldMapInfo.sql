CREATE TABLE [dbo].[TblWorldMapInfo] (
    [mMapNo]        BIGINT         NOT NULL,
    [mMapVisible]   BIT            CONSTRAINT [DF_TblWorldMapInfo_mMapVisible] DEFAULT (0) NOT NULL,
    [mPartyVisible] BIT            CONSTRAINT [DF_TblWorldMapInfo_mPartyVisible] DEFAULT (0) NOT NULL,
    [mListVisible]  BIT            CONSTRAINT [DF_TblWorldMapInfo_mListVisible] DEFAULT (0) NOT NULL,
    [mDesc]         NVARCHAR (500) NULL,
    CONSTRAINT [UCL_TblWorldMapInfo_wMapNo] PRIMARY KEY CLUSTERED ([mMapNo] ASC) WITH (FILLFACTOR = 90)
);


GO

