CREATE TABLE [dbo].[TblOtherMerchantProbability] (
    [mMonID]      INT  NOT NULL,
    [mMerchantID] INT  NOT NULL,
    [mProb]       REAL NOT NULL,
    CONSTRAINT [UCL_PK_TblOtherMerchantProbability] PRIMARY KEY CLUSTERED ([mMonID] ASC, [mMerchantID] ASC),
    CONSTRAINT [CK_TblOtherMerchantProbability_mProb] CHECK ([mProb]<=(100.00) AND [mProb]>=(0.01))
);


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'ÀÌ°èÀÇ »óÀÎ ¼ÒÈ¯ È®·ü Å×ÀÌºí', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantProbability';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ó½ºÅÍ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantProbability', @level2type = N'COLUMN', @level2name = N'mMonID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»óÀÎID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantProbability', @level2type = N'COLUMN', @level2name = N'mMerchantID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'È®·ü', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantProbability', @level2type = N'COLUMN', @level2name = N'mProb';


GO

