CREATE TABLE [dbo].[TblParmSvrOp] (
    [mSvrNo]     SMALLINT   NOT NULL,
    [mOpNo]      INT        NOT NULL,
    [mIsSetup]   BIT        NOT NULL,
    [mOpValue1]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue1] DEFAULT (0) NOT NULL,
    [mOpValue2]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue2] DEFAULT (0) NOT NULL,
    [mOpValue3]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue3] DEFAULT (0) NOT NULL,
    [mOpValue4]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue4] DEFAULT (0) NOT NULL,
    [mOpValue5]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue5] DEFAULT (0) NOT NULL,
    [mOpValue6]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue6] DEFAULT (0) NOT NULL,
    [mOpValue7]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue7] DEFAULT (0) NOT NULL,
    [mOpValue8]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue8] DEFAULT (0) NOT NULL,
    [mOpValue9]  FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue9] DEFAULT (0) NOT NULL,
    [mOpValue10] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue10] DEFAULT (0) NOT NULL,
    [mOpValue11] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue11] DEFAULT (0) NOT NULL,
    [mOpValue12] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue12] DEFAULT (0) NOT NULL,
    [mOpValue13] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue13] DEFAULT (0) NOT NULL,
    [mOpValue14] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue14] DEFAULT (0) NOT NULL,
    [mOpValue15] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue15] DEFAULT (0) NOT NULL,
    [mOpValue16] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue16] DEFAULT (0) NOT NULL,
    [mOpValue17] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue17] DEFAULT (0) NOT NULL,
    [mOpValue18] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue18] DEFAULT (0) NOT NULL,
    [mOpValue19] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue19] DEFAULT (0) NOT NULL,
    [mOpValue20] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue20] DEFAULT (0) NOT NULL,
    [mOpValue21] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue21] DEFAULT (0) NOT NULL,
    [mOpValue22] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue22] DEFAULT (0) NOT NULL,
    [mOpValue23] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue23] DEFAULT (0) NOT NULL,
    [mOpValue24] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue24] DEFAULT (0) NOT NULL,
    [mOpValue25] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue25] DEFAULT (0) NOT NULL,
    [mOpValue26] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue26] DEFAULT (0) NOT NULL,
    [mOpValue27] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue27] DEFAULT (0) NOT NULL,
    [mOpValue28] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue28] DEFAULT (0) NOT NULL,
    [mOpValue29] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue29] DEFAULT (0) NOT NULL,
    [mOpValue30] FLOAT (53) CONSTRAINT [DF_TblParmSvrOp_mOpValue30] DEFAULT (0) NOT NULL,
    CONSTRAINT [NC_PK_TblParmSvrOp_1] PRIMARY KEY NONCLUSTERED ([mSvrNo] ASC, [mOpNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [NC_PK_TblParmSvrOp_2]
    ON [dbo].[TblParmSvrOp]([mOpNo] ASC) WITH (FILLFACTOR = 90);


GO

CREATE CLUSTERED INDEX [CL_TblParmSvrOp]
    ON [dbo].[TblParmSvrOp]([mSvrNo] ASC) WITH (FILLFACTOR = 90);


GO

