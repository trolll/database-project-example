CREATE TABLE [dbo].[TblEventQuestCondition] (
    [mEQuestNo] INT     NOT NULL,
    [mType]     TINYINT NOT NULL,
    [mParmID]   INT     NOT NULL,
    [mParmA]    INT     CONSTRAINT [DF_TblEventQuestCondition_mParmA] DEFAULT (0) NOT NULL,
    [mParmB]    INT     CONSTRAINT [DF_TblEventQuestCondition_mParmB] DEFAULT (0) NOT NULL,
    [mParmC]    INT     CONSTRAINT [DF_TblEventQuestCondition_mParmC] DEFAULT (0) NOT NULL,
    [mParmD]    INT     CONSTRAINT [DF_TblEventQuestCondition_mParmD] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblEventQuestCondition_mEQeustNo] PRIMARY KEY CLUSTERED ([mEQuestNo] ASC, [mType] ASC, [mParmID] ASC) WITH (FILLFACTOR = 90)
);


GO

