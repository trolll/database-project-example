CREATE TABLE [dbo].[TblAi] (
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF__TblAi__mRegDate__00FF1D08] DEFAULT (getdate()) NOT NULL,
    [mDesc]       CHAR (100)    NOT NULL,
    [mAiId]       SMALLINT      NOT NULL,
    [mFindItem]   SMALLINT      NOT NULL,
    [mRageHp]     SMALLINT      NOT NULL,
    [mFearHp]     SMALLINT      NOT NULL,
    [mAngryItem1] INT           NULL,
    [mAngryItem2] INT           NULL,
    [mAngryItem3] INT           NULL,
    [mAngryItem4] INT           NULL,
    [mAngryItem5] INT           NULL,
    [mAngryItem6] INT           NULL,
    [mRageItem1]  INT           NULL,
    [mRageItem2]  INT           NULL,
    [mRageItem3]  INT           NULL,
    [mRageItem4]  INT           NULL,
    [mRageItem5]  INT           NULL,
    [mRageItem6]  INT           NULL,
    CONSTRAINT [PkTblAi] PRIMARY KEY NONCLUSTERED ([mAiId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK__TblAi__mFearHp__03DB89B3] CHECK (0 <= [mFearHp] and [mFearHp] <= 100),
    CONSTRAINT [CK__TblAi__mFindItem__01F34141] CHECK (0 <= [mFindItem] and [mFindItem] <= 100),
    CONSTRAINT [CK__TblAi__mRageHp__02E7657A] CHECK (0 <= [mRageHp] and [mRageHp] <= 100)
);


GO

