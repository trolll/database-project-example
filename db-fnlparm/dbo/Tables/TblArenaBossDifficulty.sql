CREATE TABLE [dbo].[TblArenaBossDifficulty] (
    [mPointGrade] INT      NOT NULL,
    [mMinPoint]   INT      NOT NULL,
    [mMaxPoint]   INT      NOT NULL,
    [mMinD]       SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mMinD] DEFAULT ((100)) NOT NULL,
    [mMaxD]       SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mMaxD] DEFAULT ((100)) NOT NULL,
    [mHIT]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mHIT] DEFAULT ((100)) NOT NULL,
    [mHP]         SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mHP] DEFAULT ((100)) NOT NULL,
    [mDPV]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mDPV] DEFAULT ((100)) NOT NULL,
    [mMPV]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mMPV] DEFAULT ((100)) NOT NULL,
    [mRPV]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mRPV] DEFAULT ((100)) NOT NULL,
    [mDDV]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mDDV] DEFAULT ((100)) NOT NULL,
    [mMDV]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mMDV] DEFAULT ((100)) NOT NULL,
    [mRDV]        SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mRDV] DEFAULT ((100)) NOT NULL,
    [mSkillDmg]   SMALLINT CONSTRAINT [DF_TblArenaBossDifficulty_mSkillDmg] DEFAULT ((100)) NOT NULL,
    CONSTRAINT [UCL_PK_TblArenaBossDifficulty] PRIMARY KEY CLUSTERED ([mPointGrade] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付过雀乔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mMDV';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦Dmg', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mSkillDmg';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'焊胶傈 焊胶 抄捞档', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥家蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mMinPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥家傍拜仿', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mMinD';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'疙吝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mHIT';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辟立规绢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mDPV';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'盔芭府规绢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mRPV';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'盔芭府雀乔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mRDV';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'厘厚器牢飘殿鞭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mPointGrade';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mMaxPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措傍拜仿', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mMaxD';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mHP';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付过规绢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mMPV';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辟立雀乔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossDifficulty', @level2type = N'COLUMN', @level2name = N'mDDV';


GO

