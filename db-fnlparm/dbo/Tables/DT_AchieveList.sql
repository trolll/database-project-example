CREATE TABLE [dbo].[DT_AchieveList] (
    [mID]      INT            CONSTRAINT [DF_DT_AchieveList_mID] DEFAULT (0) NOT NULL,
    [mValue]   INT            CONSTRAINT [DF_DT_AchieveList_mValue] DEFAULT (0) NOT NULL,
    [mName]    NVARCHAR (50)  CONSTRAINT [DF_DT_AchieveList_mName] DEFAULT ('') NOT NULL,
    [mNameKey] NVARCHAR (128) CONSTRAINT [DF_DT_AchieveList_mNameKey] DEFAULT ('') NOT NULL,
    [mDesc]    NVARCHAR (50)  CONSTRAINT [DF_DT_AchieveList_mDesc] DEFAULT ('') NOT NULL,
    [mDescKey] NVARCHAR (128) CONSTRAINT [DF_DT_AchieveList_mDescKey] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_DT_AchieveList] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

