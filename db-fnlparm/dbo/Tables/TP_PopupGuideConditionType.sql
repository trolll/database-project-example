CREATE TABLE [dbo].[TP_PopupGuideConditionType] (
    [mConType] INT           NOT NULL,
    [mDesc]    VARCHAR (100) NULL,
    [mAParm]   VARCHAR (100) NULL,
    [mBParm]   VARCHAR (100) NULL,
    [mCParm]   VARCHAR (100) NULL,
    CONSTRAINT [UCL_TP_PopupGuideConditionType_mConType] PRIMARY KEY CLUSTERED ([mConType] ASC) WITH (FILLFACTOR = 90)
);


GO

