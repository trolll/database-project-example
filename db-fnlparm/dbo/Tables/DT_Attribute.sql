CREATE TABLE [dbo].[DT_Attribute] (
    [AID]         INT           NOT NULL,
    [AType]       INT           NULL,
    [ALevel]      TINYINT       NULL,
    [ADiceDamage] NVARCHAR (10) NULL,
    [ADamage]     SMALLINT      NULL
);


GO

