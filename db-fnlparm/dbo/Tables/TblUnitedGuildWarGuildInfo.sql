CREATE TABLE [dbo].[TblUnitedGuildWarGuildInfo] (
    [mRegDate]              SMALLDATETIME CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]                SMALLINT      NOT NULL,
    [mGuildNo]              INT           NOT NULL,
    [mGuildNm]              CHAR (12)     NOT NULL,
    [mGuildPoint]           INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mGuildPoint] DEFAULT (0) NOT NULL,
    [mKillCnt]              INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mKillCnt] DEFAULT (0) NOT NULL,
    [mDieCnt]               INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mDieCnt] DEFAULT (0) NOT NULL,
    [mVictoryCnt]           INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mVictoryCnt] DEFAULT (0) NOT NULL,
    [mDrawCnt]              INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mDrawCnt] DEFAULT (0) NOT NULL,
    [mDefeatCnt]            INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mDefeatCnt] DEFAULT (0) NOT NULL,
    [mVictoryHerosBT1]      INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mVictoryHerosBT1] DEFAULT (0) NOT NULL,
    [mDefeatHerosBT1]       INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mDefeatHerosBT1] DEFAULT (0) NOT NULL,
    [mVictoryHerosBT2]      INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mVictoryHerosBT2] DEFAULT (0) NOT NULL,
    [mDefeatHerosBT2]       INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mDefeatHerosBT2] DEFAULT (0) NOT NULL,
    [mVictoryHerosBT3]      INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mVictoryHerosBT3] DEFAULT (0) NOT NULL,
    [mDefeatHerosBT3]       INT           CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mDefeatHerosBT3] DEFAULT (0) NOT NULL,
    [mRecentlyMatchSvrNo]   SMALLINT      CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mRecentlyMatchSvrNo] DEFAULT (0) NOT NULL,
    [mRecentlyMatchGuildNm] CHAR (12)     CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mRecentlyMatchGuildNm] DEFAULT ('') NOT NULL,
    [mRecentlyMatchResult]  TINYINT       CONSTRAINT [DF_TblUnitedGuildWarGuildInfo_mRecentlyMatchResult] DEFAULT (3) NOT NULL,
    CONSTRAINT [UCL_TblUnitedGuildWarGuildInfo] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mGuildNo] ASC) WITH (FILLFACTOR = 90)
);


GO

