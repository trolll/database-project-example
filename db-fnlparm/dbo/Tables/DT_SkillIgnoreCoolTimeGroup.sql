CREATE TABLE [dbo].[DT_SkillIgnoreCoolTimeGroup] (
    [SGroupNo]     SMALLINT NOT NULL,
    [SIgnoreGroup] SMALLINT NOT NULL,
    CONSTRAINT [CL_PKIgnoreCoolTime] PRIMARY KEY CLUSTERED ([SGroupNo] ASC, [SIgnoreGroup] ASC) WITH (FILLFACTOR = 90)
);


GO

