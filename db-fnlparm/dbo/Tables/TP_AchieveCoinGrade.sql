CREATE TABLE [dbo].[TP_AchieveCoinGrade] (
    [mGrade]     TINYINT CONSTRAINT [DF_TP_AchieveCoinGrade_mGrade] DEFAULT (1) NOT NULL,
    [mCoinPoint] INT     CONSTRAINT [DF_TP_AchieveCoinGrade_mCoinPoint] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TP_AchieveCoinGrade] PRIMARY KEY CLUSTERED ([mGrade] ASC) WITH (FILLFACTOR = 90)
);


GO

