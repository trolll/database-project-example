CREATE TABLE [dbo].[DT_AchieveItemTrophy] (
    [IID]          INT     CONSTRAINT [DF_DT_AchieveItemTrophy_IID] DEFAULT (0) NOT NULL,
    [mRarity]      TINYINT CONSTRAINT [DF_DT_AchieveItemTrophy_mRarity] DEFAULT (0) NOT NULL,
    [mEquipType]   TINYINT CONSTRAINT [DF_DT_AchieveItemTrophy_mEquipType] DEFAULT (0) NOT NULL,
    [mEquipPos]    TINYINT CONSTRAINT [DF_DT_AchieveItemTrophy_mEquipPos] DEFAULT (0) NOT NULL,
    [mAbilityType] TINYINT CONSTRAINT [DF_DT_AchieveItemTrophy_mAbilityType] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_DT_AchieveItemTrophy] PRIMARY KEY CLUSTERED ([IID] ASC) WITH (FILLFACTOR = 90)
);


GO

