CREATE TABLE [dbo].[TP_MaterialType] (
    [MType]     SMALLINT      NOT NULL,
    [MTypeDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [UCL_PK_TP_MaterialType] PRIMARY KEY CLUSTERED ([MType] ASC) WITH (FILLFACTOR = 90)
);


GO

