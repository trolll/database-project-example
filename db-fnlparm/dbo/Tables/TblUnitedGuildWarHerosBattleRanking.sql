CREATE TABLE [dbo].[TblUnitedGuildWarHerosBattleRanking] (
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF_TblUnitedGuildWarHerosBattleRanking_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]      SMALLINT      NOT NULL,
    [mRanking]    SMALLINT      IDENTITY (1, 1) NOT NULL,
    [mGuildNo]    INT           NOT NULL,
    [mGuildNm]    CHAR (12)     NOT NULL,
    [mVictoryCnt] INT           NOT NULL,
    [mPcNo]       INT           NOT NULL,
    [mPcNm]       CHAR (12)     NOT NULL,
    CONSTRAINT [UNC_TblUnitedGuildWarHerosBattleRanking] PRIMARY KEY NONCLUSTERED ([mRanking] ASC) WITH (FILLFACTOR = 90)
);


GO

