CREATE TABLE [dbo].[DT_AbnormalAttrApply] (
    [mID]           INT            CONSTRAINT [DF_DT_AbnormalAttrApply_mID] DEFAULT (0) NOT NULL,
    [mOriginAID]    INT            CONSTRAINT [DF_DT_AbnormalAttrApply_mOriginAID] DEFAULT (0) NOT NULL,
    [mOriginAType]  TINYINT        CONSTRAINT [DF_DT_AbnormalAttrApply_mOriginAType] DEFAULT (0) NOT NULL,
    [mConditionAID] INT            CONSTRAINT [DF_DT_AbnormalAttrApply_mConditionAID] DEFAULT (0) NOT NULL,
    [mDesc]         NVARCHAR (200) CONSTRAINT [DF_DT_AbnormalAttrApply_mDesc] DEFAULT (N'') NULL,
    CONSTRAINT [UCL_PK_DT_AbnormalAttrApply] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

