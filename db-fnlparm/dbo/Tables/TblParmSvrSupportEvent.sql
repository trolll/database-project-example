CREATE TABLE [dbo].[TblParmSvrSupportEvent] (
    [mSvrNo]     SMALLINT      NOT NULL,
    [mEventID]   INT           NOT NULL,
    [mBeginDate] SMALLDATETIME NOT NULL,
    [mEndDate]   SMALLDATETIME NOT NULL,
    CONSTRAINT [UNC_PK_TblParmSvrSupportEvent] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mEventID] ASC) WITH (FILLFACTOR = 90)
);


GO

