CREATE TABLE [dbo].[TblEventQuestResultDraw] (
    [mMonClass]  TINYINT NOT NULL,
    [mFair]      REAL    CONSTRAINT [DF_TblEventQuestResultDraw_mFair] DEFAULT (0) NOT NULL,
    [mGood]      REAL    CONSTRAINT [DF_TblEventQuestResultDraw_mGood] DEFAULT (0) NOT NULL,
    [mExcellent] REAL    CONSTRAINT [DF_TblEventQuestResultDraw_mExcellent] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblEventQuestResultDraw_mMonClass] PRIMARY KEY CLUSTERED ([mMonClass] ASC) WITH (FILLFACTOR = 90)
);


GO

