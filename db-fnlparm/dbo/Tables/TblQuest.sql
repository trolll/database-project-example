CREATE TABLE [dbo].[TblQuest] (
    [mQuestNo]       INT           NOT NULL,
    [mQuestNm]       NVARCHAR (30) CONSTRAINT [DF_TblQuest_mQuestNm] DEFAULT (' ') NOT NULL,
    [mClass]         TINYINT       CONSTRAINT [DF_TblQuest_mClass] DEFAULT (0) NOT NULL,
    [mLevel1]        SMALLINT      CONSTRAINT [DF_TblQuest_mLevel1] DEFAULT (0) NOT NULL,
    [mLevel2]        SMALLINT      CONSTRAINT [DF_TblQuest_mLevel2] DEFAULT (0) NULL,
    [mPreQuestNo]    INT           CONSTRAINT [DF_TblQuest_mPreQuestNo] DEFAULT (0) NOT NULL,
    [mIsOverlap]     TINYINT       CONSTRAINT [DF_TblQuest_mIsOverlap] DEFAULT (0) NOT NULL,
    [mQuestDesc]     NVARCHAR (30) NULL,
    [mAbandonment]   BIT           CONSTRAINT [DF_TblQuest_mAbandonment] DEFAULT (0) NOT NULL,
    [mDifficulty]    TINYINT       CONSTRAINT [DF_TblQuest_mDifficulty] DEFAULT (1) NOT NULL,
    [mRewardNo]      INT           CONSTRAINT [DF_TblQuest_mRewardNo] DEFAULT (0) NOT NULL,
    [mScriptType]    TINYINT       CONSTRAINT [DF_TblQuest_mScriptType] DEFAULT (0) NOT NULL,
    [mPlace]         SMALLINT      CONSTRAINT [DF_TblQuest_mPlace] DEFAULT (0) NOT NULL,
    [mPosX]          FLOAT (53)    CONSTRAINT [DF_TblQuest_mPosX] DEFAULT (0) NOT NULL,
    [mPosY]          FLOAT (53)    CONSTRAINT [DF_TblQuest_mPosY] DEFAULT (0) NOT NULL,
    [mPosZ]          FLOAT (53)    CONSTRAINT [DF_TblQuest_mPosZ] DEFAULT (0) NOT NULL,
    [mVisible]       BIT           CONSTRAINT [DF_TblQuest_mInvisible] DEFAULT (0) NOT NULL,
    [mTextNo]        TINYINT       CONSTRAINT [DF_TblQuest_mTextNo] DEFAULT (1) NOT NULL,
    [mParentNo]      INT           CONSTRAINT [DF_TblQuest_mParentNo] DEFAULT (0) NOT NULL,
    [mFindNPC]       INT           CONSTRAINT [DF_TblQuest_mFindNPC] DEFAULT (0) NOT NULL,
    [mCompletionNPC] INT           CONSTRAINT [DF_TblQuest_mmCompletionNPC] DEFAULT (0) NOT NULL,
    [mQuestKind]     TINYINT       CONSTRAINT [DF_TblQuest_mQuestKind] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_CL_TblQuest] PRIMARY KEY CLUSTERED ([mQuestNo] DESC) WITH (FILLFACTOR = 90)
);


GO

