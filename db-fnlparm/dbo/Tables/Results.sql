CREATE TABLE [dbo].[Results] (
    [mRegDate]  DATETIME       NULL,
    [mMId]      INT            NULL,
    [mClick]    NTEXT          NULL,
    [mDie]      NVARCHAR (100) NULL,
    [mAttacked] NVARCHAR (100) NULL,
    [mTarget]   NVARCHAR (100) NULL,
    [mBear]     NVARCHAR (100) NULL,
    [mGossip1]  NVARCHAR (100) NULL,
    [mGossip2]  NVARCHAR (100) NULL,
    [mGossip3]  NVARCHAR (100) NULL,
    [mGossip4]  NVARCHAR (100) NULL,
    [mUptDate]  DATETIME       NULL
);


GO

