CREATE TABLE [dbo].[TblServantFriendlySkillTransform] (
    [IID]  INT NOT NULL,
    [SPID] INT NOT NULL,
    [MID]  INT NOT NULL,
    CONSTRAINT [UCL_PK_TblServantFriendlySkillTransform] PRIMARY KEY CLUSTERED ([IID] ASC, [SPID] ASC, [MID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 模剐档 函脚 阁胶磐 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkillTransform', @level2type = N'COLUMN', @level2name = N'MID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 酒捞袍 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkillTransform', @level2type = N'COLUMN', @level2name = N'IID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 模剐档 函脚 胶懦 蒲', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkillTransform', @level2type = N'COLUMN', @level2name = N'SPID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辑锅飘 模剐档 函脚 胶懦 蒲 格废', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkillTransform';


GO

