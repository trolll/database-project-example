CREATE TABLE [dbo].[DT_AbnormalAttrDetach] (
    [mID]             INT            NOT NULL,
    [mAppType]        TINYINT        CONSTRAINT [DF_DT_AbnormalAttrDetach_mAppType] DEFAULT (0) NOT NULL,
    [mOriginAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrDetach_mOriginAID] DEFAULT (0) NOT NULL,
    [mEffectAType]    INT            CONSTRAINT [DF_DT_AbnormalAttrDetach_mEffectAType] DEFAULT (0) NOT NULL,
    [mConditionType]  TINYINT        CONSTRAINT [DF_DT_AbnormalAttrDetach_mConditionType] DEFAULT (0) NOT NULL,
    [mConditionAType] INT            CONSTRAINT [DF_DT_AbnormalAttrDetach_mConditionAType] DEFAULT (0) NOT NULL,
    [mDesc]           NVARCHAR (200) CONSTRAINT [DF_DT_AbnormalAttrDetach_mDesc] DEFAULT (N'') NULL,
    [mEffectAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrDetach_mEffectAID] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_DT_AbnormalAttrDetach] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

