CREATE TABLE [dbo].[TP_SpecificProcItemType] (
    [mProcNo]     INT           NOT NULL,
    [mProcDesc]   VARCHAR (100) NULL,
    [mAParamDesc] VARCHAR (100) NULL,
    [mBParamDesc] VARCHAR (100) NULL,
    [mCParamDesc] VARCHAR (100) NULL,
    [mDParamDesc] VARCHAR (100) NULL,
    CONSTRAINT [PK_TP_SpecificProcItemType] PRIMARY KEY CLUSTERED ([mProcNo] ASC) WITH (FILLFACTOR = 90)
);


GO

