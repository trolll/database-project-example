CREATE TABLE [dbo].[TblAiRaid] (
    [mID]           INT     NOT NULL,
    [mMID]          INT     NOT NULL,
    [mCID]          INT     NOT NULL,
    [mCPercent]     INT     CONSTRAINT [DF_TblAiRaid_mCPercent] DEFAULT (0) NOT NULL,
    [mCAParam]      INT     CONSTRAINT [DF_TblAiRaid_mCVal1] DEFAULT (0) NOT NULL,
    [mCBParam]      INT     CONSTRAINT [DF_TblAiRaid_mCVal2] DEFAULT (0) NOT NULL,
    [mCCParam]      INT     CONSTRAINT [DF_TblAiRaid_mCVal3] DEFAULT (0) NOT NULL,
    [mSID1]         INT     CONSTRAINT [DF_TblAiRaid_mSID1] DEFAULT ((-1)) NOT NULL,
    [mSDelay1]      INT     CONSTRAINT [DF_TblAiRaid_mSDelay1] DEFAULT (0) NOT NULL,
    [mSLoop1]       INT     CONSTRAINT [DF_TblAiRaid_mSLoop1] DEFAULT (0) NOT NULL,
    [mSTargetType1] INT     CONSTRAINT [DF_TblAiRaid_mSTargetType1] DEFAULT (0) NOT NULL,
    [mSMaxCount1]   INT     CONSTRAINT [DF_TblAiRaid_mSMaxCount1] DEFAULT (999999999) NOT NULL,
    [mSFixMax1]     TINYINT CONSTRAINT [DF_TblAiRaid_mSFixMax1] DEFAULT (0) NOT NULL,
    [mSID2]         INT     CONSTRAINT [DF_TblAiRaid_mSID2] DEFAULT ((-1)) NOT NULL,
    [mSDelay2]      INT     CONSTRAINT [DF_TblAiRaid_mSDelay2] DEFAULT (0) NOT NULL,
    [mSLoop2]       INT     CONSTRAINT [DF_TblAiRaid_mSLoop2] DEFAULT (0) NOT NULL,
    [mSTargetType2] INT     CONSTRAINT [DF_TblAiRaid_mSTargetType2] DEFAULT (0) NOT NULL,
    [mSMaxCount2]   INT     CONSTRAINT [DF_TblAiRaid_mSMaxCount2] DEFAULT (999999999) NOT NULL,
    [mSFixMax2]     TINYINT CONSTRAINT [DF_TblAiRaid_mSFixMax11] DEFAULT (0) NOT NULL,
    [mSID3]         INT     CONSTRAINT [DF_TblAiRaid_mSID3] DEFAULT ((-1)) NOT NULL,
    [mSDelay3]      INT     CONSTRAINT [DF_TblAiRaid_mSDelay3] DEFAULT (0) NOT NULL,
    [mSLoop3]       INT     CONSTRAINT [DF_TblAiRaid_mSLoop3] DEFAULT (0) NOT NULL,
    [mSTargetType3] INT     CONSTRAINT [DF_TblAiRaid_mSTargetType3] DEFAULT (0) NOT NULL,
    [mSMaxCount3]   INT     CONSTRAINT [DF_TblAiRaid_mSMaxCount3] DEFAULT (999999999) NOT NULL,
    [mSFixMax3]     TINYINT CONSTRAINT [DF_TblAiRaid_mSFixMax11_1] DEFAULT (0) NOT NULL,
    [mSID4]         INT     CONSTRAINT [DF_TblAiRaid_mSID4] DEFAULT ((-1)) NOT NULL,
    [mSDelay4]      INT     CONSTRAINT [DF_TblAiRaid_mSDelay4] DEFAULT (0) NOT NULL,
    [mSLoop4]       INT     CONSTRAINT [DF_TblAiRaid_mSLoop4] DEFAULT (0) NOT NULL,
    [mSTargetType4] INT     CONSTRAINT [DF_TblAiRaid_mSTargetType4] DEFAULT (0) NOT NULL,
    [mSMaxCount4]   INT     CONSTRAINT [DF_TblAiRaid_mSMaxCount4] DEFAULT (999999999) NOT NULL,
    [mSFixMax4]     TINYINT CONSTRAINT [DF_TblAiRaid_mSFixMax11_2] DEFAULT (0) NOT NULL,
    [mSID5]         INT     CONSTRAINT [DF_TblAiRaid_mSID5] DEFAULT ((-1)) NOT NULL,
    [mSDelay5]      INT     CONSTRAINT [DF_TblAiRaid_mSDelay5] DEFAULT (0) NOT NULL,
    [mSLoop5]       INT     CONSTRAINT [DF_TblAiRaid_mSLoop5] DEFAULT (0) NOT NULL,
    [mSTargetType5] INT     CONSTRAINT [DF_TblAiRaid_mSTargetType5] DEFAULT (0) NOT NULL,
    [mSMaxCount5]   INT     CONSTRAINT [DF_TblAiRaid_mSMaxCount5] DEFAULT (999999999) NOT NULL,
    [mSFixMax5]     TINYINT CONSTRAINT [DF_TblAiRaid_mSFixMax11_3] DEFAULT (0) NOT NULL
);


GO

CREATE CLUSTERED INDEX [IX_TblAiRaid]
    ON [dbo].[TblAiRaid]([mMID] ASC, [mCID] ASC) WITH (FILLFACTOR = 90);


GO

