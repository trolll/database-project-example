CREATE TABLE [dbo].[TP_AchieveGuildPoint] (
    [mRank]       INT NOT NULL,
    [mPoint]      INT NOT NULL,
    [mChoiceProb] INT NOT NULL,
    [mLegendProb] INT NOT NULL,
    [mEpicProb]   INT NOT NULL,
    [mRareProb]   INT NOT NULL,
    [mNormalProb] INT NOT NULL,
    CONSTRAINT [PK_TP_AchieveGuildPoint] PRIMARY KEY CLUSTERED ([mRank] ASC) WITH (FILLFACTOR = 90)
);


GO

