CREATE TABLE [dbo].[DT_Exp] (
    [ELevel]       SMALLINT NULL,
    [EExp]         BIGINT   NOT NULL,
    [ERestExpRate] INT      CONSTRAINT [DF_DT_Exp_ERestExpRate] DEFAULT (1) NOT NULL
);


GO

