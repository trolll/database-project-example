CREATE TABLE [dbo].[TblArenaTower] (
    [mRegDate] DATETIME   DEFAULT (getdate()) NOT NULL,
    [mNo]      BIGINT     NOT NULL,
    [mPosX]    FLOAT (53) NOT NULL,
    [mPosY]    FLOAT (53) NOT NULL,
    [mPosZ]    FLOAT (53) NOT NULL,
    [mWidth]   FLOAT (53) NOT NULL,
    [mDir]     FLOAT (53) NOT NULL,
    [mDesc]    CHAR (50)  NOT NULL,
    [mGroup]   BIGINT     NOT NULL,
    CONSTRAINT [UCL_TblArenaTower] PRIMARY KEY CLUSTERED ([mNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'绊蜡 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己拱 府家胶 颇老 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mDesc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己瞪 困摹(Z谅钎)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mPosZ';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'酒饭唱 拜傈 己拱 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己瞪 困摹(X谅钎)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mPosX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弊缝 沥焊. (己拱 沥焊客 己巩 沥焊啊 弓咯乐绰 弊缝沥焊.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己拱 呈厚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mWidth';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'单捞磐 积己 老磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己拱 规氢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mDir';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己瞪 困摹(Y谅钎)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaTower', @level2type = N'COLUMN', @level2name = N'mPosY';


GO

