CREATE TABLE [dbo].[TblSummonList] (
    [mNo]      INT NOT NULL,
    [mGroupID] INT NOT NULL,
    [mMonID]   INT NOT NULL,
    [mLevel]   INT CONSTRAINT [DF_TblSummonList_mLevel] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblSummonList] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

