CREATE TABLE [dbo].[TblParmSvrSupportCalendarEvent] (
    [mSvrNo]     SMALLINT      NOT NULL,
    [mEventNo]   INT           NOT NULL,
    [mBeginDate] SMALLDATETIME NOT NULL,
    [mEndDate]   SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblParmSvrSupportCalendarEvent] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mEventNo] ASC),
    CONSTRAINT [FK_TblParmSvrSupportCalendarEvent_TblParmSvr] FOREIGN KEY ([mSvrNo]) REFERENCES [dbo].[TblParmSvr] ([mSvrNo])
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblParmSvrSupportCalendarEvent_mEventNo]
    ON [dbo].[TblParmSvrSupportCalendarEvent]([mEventNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÌº¥Æ® Á¾·áÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblParmSvrSupportCalendarEvent', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹öº° ÀÌº¥Æ® ´Þ·Â Ç¥½Ã', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblParmSvrSupportCalendarEvent';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹ö¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblParmSvrSupportCalendarEvent', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÌº¥Æ® ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblParmSvrSupportCalendarEvent', @level2type = N'COLUMN', @level2name = N'mEventNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÌº¥Æ® ½ÃÀÛÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblParmSvrSupportCalendarEvent', @level2type = N'COLUMN', @level2name = N'mBeginDate';


GO

