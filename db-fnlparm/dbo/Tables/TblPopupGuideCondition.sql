CREATE TABLE [dbo].[TblPopupGuideCondition] (
    [mConID]   INT NOT NULL,
    [mGuideNo] INT NOT NULL,
    [mConType] INT NOT NULL,
    [mAParm]   INT NOT NULL,
    [mBParm]   INT NOT NULL,
    [mCParm]   INT NOT NULL,
    [mReadCnt] INT CONSTRAINT [DF_TblPopupGuideCondition_mReadCnt] DEFAULT (1) NOT NULL,
    [mForce]   BIT CONSTRAINT [DF_TblPopupGuideCondition_mForce] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblPopupGuideCondition_mConID] PRIMARY KEY CLUSTERED ([mConID] ASC) WITH (FILLFACTOR = 90)
);


GO

