CREATE TABLE [dbo].[TblParmSvr] (
    [mRegDate]         DATETIME      CONSTRAINT [DF__TblParmSv__mRegD__12C8C788] DEFAULT (getdate()) NOT NULL,
    [mIsValid]         BIT           CONSTRAINT [DF__TblParmSv__mIsVa__13BCEBC1] DEFAULT (1) NOT NULL,
    [mDispOrder]       SMALLINT      CONSTRAINT [DF_TblParmSvr_mDispOrder] DEFAULT (1) NOT NULL,
    [mSvrNo]           SMALLINT      NOT NULL,
    [mType]            TINYINT       NOT NULL,
    [mMajorIp]         CHAR (15)     NOT NULL,
    [mMinorIp]         CHAR (15)     CONSTRAINT [DF__TblParmSv__mMino__1699586C] DEFAULT (null) NULL,
    [mWorldNo]         SMALLINT      NULL,
    [mMajorVer]        INT           CONSTRAINT [DF_TblParmSvr_mMajorVer] DEFAULT (1) NOT NULL,
    [mMinorVer]        INT           CONSTRAINT [DF_TblParmSvr_mMinorVer] DEFAULT (1) NOT NULL,
    [mThdWkCnt]        SMALLINT      NOT NULL,
    [mThdTmCnt]        SMALLINT      NOT NULL,
    [mThdDbCnt]        SMALLINT      NOT NULL,
    [mThdLogCnt]       SMALLINT      NOT NULL,
    [mSessionCnt]      SMALLINT      NOT NULL,
    [mSendCnt]         INT           NOT NULL,
    [mTcpPort]         SMALLINT      NOT NULL,
    [mUdpPort]         SMALLINT      NOT NULL,
    [mDesc]            CHAR (100)    NOT NULL,
    [mIsSiege]         BIT           CONSTRAINT [DF_TblParmSvr_mIsSiege] DEFAULT (0) NOT NULL,
    [mEvtStx]          DATETIME      CONSTRAINT [DF_TblParmSvr_mEvtStx] DEFAULT ('2000-1-1') NOT NULL,
    [mEvtEtx]          DATETIME      CONSTRAINT [DF_TblParmSvr_mEvtEtx] DEFAULT ('2000-1-1') NOT NULL,
    [mIsCheckRSC]      BIT           CONSTRAINT [DF_TblParmSvr_IsCheckRSC] DEFAULT (1) NOT NULL,
    [mNationID]        TINYINT       CONSTRAINT [DF_TblParmSvr_mNationID] DEFAULT (0) NOT NULL,
    [mBullet]          INT           NOT NULL,
    [mSupportType]     TINYINT       CONSTRAINT [DF_TblParmSvr_mSupportType] DEFAULT (2) NOT NULL,
    [mLastSupportDate] SMALLDATETIME DEFAULT ('2020-01-01') NOT NULL,
    [mSvrInfo]         TINYINT       CONSTRAINT [DF_TblParmSvr_mSvrInfo] DEFAULT (0) NOT NULL,
    [mIsInputDlg]      BIT           CONSTRAINT [DF_TblParmSvr_mIsInputDlg] DEFAULT (0) NOT NULL,
    [mSmallSendCnt]    INT           CONSTRAINT [DF_TblParmSvr_mSmallSendCnt] DEFAULT (0) NOT NULL,
    CONSTRAINT [PkTblParmSvr] PRIMARY KEY CLUSTERED ([mSvrNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK__TblParmSv__mSend__4C371A65] CHECK ([mSendCnt] > 0),
    CONSTRAINT [CK__TblParmSv__mSess__4D2B3E9E] CHECK ([mSessionCnt] > 0),
    CONSTRAINT [CK__TblParmSv__mSvrN__4E1F62D7] CHECK (0 <= [mSvrNo]),
    CONSTRAINT [CK__TblParmSv__mTcpP__4F138710] CHECK ([mTcpPort] > 0),
    CONSTRAINT [CK__TblParmSv__mThdD__5007AB49] CHECK ([mThdDbCnt] > 0),
    CONSTRAINT [CK__TblParmSv__mThdW__50FBCF82] CHECK ([mThdWkCnt] > 0),
    CONSTRAINT [CK__TblParmSv__mType__10966653] CHECK (0 <= [mType] and [mType] <= 3),
    CONSTRAINT [CK__TblParmSv__mUdpP__52E417F4] CHECK ([mUdpPort] > 0),
    CONSTRAINT [CK_TblParmSvr] CHECK ([mThdLogCnt] > 0),
    CONSTRAINT [CK_TblParmSvr_mSupportType] CHECK ([mSupportType] = 2 or [mSupportType] = 1)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblParmSvr]
    ON [dbo].[TblParmSvr]([mType] ASC, [mMajorIp] ASC) WITH (FILLFACTOR = 90);


GO

