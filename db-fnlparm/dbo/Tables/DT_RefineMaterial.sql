CREATE TABLE [dbo].[DT_RefineMaterial] (
    [RID]      INT     NULL,
    [RItemID]  INT     NULL,
    [RNum]     INT     NULL,
    [ROrderNo] TINYINT CONSTRAINT [DF_DT_RefineMaterial_ROrderNo] DEFAULT (1) NOT NULL
);


GO

