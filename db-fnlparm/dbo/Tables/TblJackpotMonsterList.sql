CREATE TABLE [dbo].[TblJackpotMonsterList] (
    [mMID]   INT     NOT NULL,
    [mCamp]  TINYINT CONSTRAINT [DF_TblJackpotMonsterList_mCamp] DEFAULT (0) NOT NULL,
    [mLevel] TINYINT CONSTRAINT [DF_TblJackpotMonsterList_mLevel] DEFAULT (0) NOT NULL,
    [mPoint] TINYINT CONSTRAINT [DF_TblJackpotMonsterList_mPoint] DEFAULT (0) NOT NULL
);


GO

