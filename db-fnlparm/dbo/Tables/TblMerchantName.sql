CREATE TABLE [dbo].[TblMerchantName] (
    [mID]          INT        NOT NULL,
    [mDesc]        NCHAR (50) NOT NULL,
    [mShopType]    TINYINT    DEFAULT (0) NOT NULL,
    [mPaymentType] TINYINT    CONSTRAINT [DF_TblMerchantName_mPaymentType] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblMerchantName] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

