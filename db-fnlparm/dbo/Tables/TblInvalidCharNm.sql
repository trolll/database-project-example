CREATE TABLE [dbo].[TblInvalidCharNm] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblInvali__mRegD__3AD6B8E2] DEFAULT (getdate()) NOT NULL,
    [mString]  VARCHAR (12) NOT NULL,
    CONSTRAINT [PkTblInvalidCharNm] PRIMARY KEY CLUSTERED ([mString] ASC) WITH (FILLFACTOR = 90)
);


GO

