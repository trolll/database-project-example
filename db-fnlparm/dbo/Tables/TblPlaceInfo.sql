CREATE TABLE [dbo].[TblPlaceInfo] (
    [mPlaceNo]      INT          NOT NULL,
    [mPlaceNm]      VARCHAR (50) NOT NULL,
    [mTeleport]     TINYINT      NOT NULL,
    [mTeleportSave] TINYINT      NOT NULL,
    [mCombat]       TINYINT      NOT NULL,
    [mReturn]       TINYINT      NOT NULL,
    [mIsCollision]  BIT          NOT NULL,
    [mIsSiege]      BIT          NOT NULL,
    [mRgbQuadRed]   TINYINT      NOT NULL,
    [mRgbQuadGreen] TINYINT      NOT NULL,
    [mRgbQuadBlue]  TINYINT      NOT NULL,
    [mIsTown]       BIT          NOT NULL,
    [mIsSupport]    BIT          NOT NULL,
    CONSTRAINT [PkTblPlaceInfo] PRIMARY KEY CLUSTERED ([mPlaceNo] ASC) WITH (FILLFACTOR = 90)
);


GO

