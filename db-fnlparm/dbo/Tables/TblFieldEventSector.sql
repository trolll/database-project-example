CREATE TABLE [dbo].[TblFieldEventSector] (
    [mEventType]  INT          CONSTRAINT [DF_TblFieldEventSector_mEventType] DEFAULT (0) NOT NULL,
    [mSectorType] INT          CONSTRAINT [DF_TblFieldEventSector_mSectorType] DEFAULT (0) NOT NULL,
    [mX]          INT          CONSTRAINT [DF_TblFieldEventSector_mX] DEFAULT (0) NOT NULL,
    [mZ]          INT          CONSTRAINT [DF_TblFieldEventSector_mZ] DEFAULT (0) NOT NULL,
    [mDesc]       VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TblFieldEventSector_mEventType_mSectorType_mX_mZ] PRIMARY KEY CLUSTERED ([mEventType] ASC, [mSectorType] ASC, [mX] ASC, [mZ] ASC) WITH (FILLFACTOR = 90)
);


GO

