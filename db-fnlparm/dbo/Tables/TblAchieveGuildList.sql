CREATE TABLE [dbo].[TblAchieveGuildList] (
    [mAchieveGuildID] INT           IDENTITY (1, 1) NOT NULL,
    [mRegDate]        DATETIME      CONSTRAINT [DF_TblAchieveGuildList_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mGuildRank]      TINYINT       CONSTRAINT [DF_TblAchieveGuildList_mGuildRank] DEFAULT (0) NOT NULL,
    [mGuildName]      NVARCHAR (50) CONSTRAINT [DF_TblAchieveGuildList_mGuildName] DEFAULT ('') NOT NULL,
    [mMemberName]     NVARCHAR (50) CONSTRAINT [DF_TblAchieveGuildList_mMemberName] DEFAULT ('') NOT NULL,
    [mEquipPoint]     INT           CONSTRAINT [DF_TblAchieveGuildList_mEquipPoint] DEFAULT (0) NOT NULL,
    [mUpdateIndex]    INT           CONSTRAINT [DF_TblAchieveGuildList_mUpdateIndex] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblAchieveGuildList] PRIMARY KEY CLUSTERED ([mAchieveGuildID] ASC) WITH (FILLFACTOR = 90)
);


GO

