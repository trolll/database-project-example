CREATE TABLE [dbo].[TblUnitedGuildWarPcInfo] (
    [mRegDate]           SMALLDATETIME CONSTRAINT [DF_TblUnitedGuildWarPcInfo_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]             SMALLINT      NOT NULL,
    [mGuildNo]           INT           NOT NULL,
    [mGuildNm]           CHAR (12)     CONSTRAINT [DF_TblUnitedGuildWarPcInfo_mGuildNm] DEFAULT ('') NOT NULL,
    [mPcNo]              INT           NOT NULL,
    [mPcNm]              CHAR (12)     NOT NULL,
    [mHerosBTVictoryCnt] INT           CONSTRAINT [DF_TblUnitedGuildWarPcInfo_mHerosBTVictoryCnt] DEFAULT (0) NOT NULL,
    [mEquipPoint]        INT           CONSTRAINT [DF_TblUnitedGuildWarPcInfo_mEquipPoint] DEFAULT (0) NOT NULL
);


GO

CREATE UNIQUE CLUSTERED INDEX [UCL_TblUnitedGuildWarPcInfo_mSvrNo_mGuildNo_mPcNo]
    ON [dbo].[TblUnitedGuildWarPcInfo]([mSvrNo] ASC, [mGuildNo] ASC, [mPcNo] ASC);


GO

