CREATE TABLE [dbo].[DT_MonsterRole] (
    [MID]   INT      NOT NULL,
    [mRole] SMALLINT NOT NULL,
    CONSTRAINT [PK_DT_MonsterRole_MID_mRole] PRIMARY KEY CLUSTERED ([MID] ASC, [mRole] ASC) WITH (FILLFACTOR = 90)
);


GO

