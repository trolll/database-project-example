CREATE TABLE [dbo].[TP_BeadType] (
    [mBeadType] INT            NOT NULL,
    [mDesc]     NVARCHAR (100) NULL,
    [mDescA]    NVARCHAR (40)  NULL,
    [mDescB]    NVARCHAR (40)  NULL,
    [mDescC]    NVARCHAR (40)  NULL,
    [mDescD]    NVARCHAR (40)  NULL,
    [mDescE]    NVARCHAR (40)  NULL,
    CONSTRAINT [CL_PK_TP_BeadType_mBeadType] PRIMARY KEY CLUSTERED ([mBeadType] ASC) WITH (FILLFACTOR = 90),
    CHECK ([mBeadType] <> 0)
);


GO

