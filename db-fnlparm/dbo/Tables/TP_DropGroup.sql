CREATE TABLE [dbo].[TP_DropGroup] (
    [DGroup]    INT           NOT NULL,
    [DName]     NVARCHAR (40) NULL,
    [DDropType] TINYINT       CONSTRAINT [DF_TP_DropGroup_DDropType] DEFAULT (0) NOT NULL
);


GO

