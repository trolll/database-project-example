CREATE TABLE [dbo].[TblServantFriendlySkill] (
    [IID]  INT NOT NULL,
    [SPID] INT NOT NULL,
    CONSTRAINT [UCL_PK_TblServantFriendlySkill] PRIMARY KEY CLUSTERED ([IID] ASC, [SPID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® Ä£¹Ðµµ ½ºÅ³ ÆÑ ¸ñ·Ï', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkill';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ¾ÆÀÌÅÛ ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkill', @level2type = N'COLUMN', @level2name = N'IID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Ä£¹Ðµµ ½ºÅ³ ÆÑ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantFriendlySkill', @level2type = N'COLUMN', @level2name = N'SPID';


GO

