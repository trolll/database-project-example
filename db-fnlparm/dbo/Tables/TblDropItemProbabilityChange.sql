CREATE TABLE [dbo].[TblDropItemProbabilityChange] (
    [mItemID]         INT      NOT NULL,
    [mDGroup]         INT      CONSTRAINT [DF_TblDropItemProbChg_mDGroup] DEFAULT ((0)) NOT NULL,
    [mSvrNo]          SMALLINT NOT NULL,
    [mMultiplication] REAL     NOT NULL,
    [mAddition]       REAL     NOT NULL,
    CONSTRAINT [UCL_PK_TblDropItemProbabilityChange] PRIMARY KEY CLUSTERED ([mItemID] ASC, [mDGroup] ASC, [mSvrNo] ASC),
    CONSTRAINT [CK_TblDropItemProbChg_mAdd] CHECK ([mAddition]>=(0.01) AND (99.99)>=[mAddition]),
    CONSTRAINT [CK_TblDropItemProbChg_mItemID] CHECK ([mItemID]>(0)),
    CONSTRAINT [CK_TblDropItemProbChg_mMulti] CHECK ([mMultiplication]>=(0.01) AND (99.99)>=[mMultiplication])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDropItemProbabilityChange', @level2type = N'COLUMN', @level2name = N'mItemID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°öÇÏ´Â °ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDropItemProbabilityChange', @level2type = N'COLUMN', @level2name = N'mMultiplication';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'´õÇÏ´Â °ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDropItemProbabilityChange', @level2type = N'COLUMN', @level2name = N'mAddition';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µå¶ø±×·ì ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDropItemProbabilityChange', @level2type = N'COLUMN', @level2name = N'mDGroup';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'µå·Ó ¾ÆÀÌÅÛ È®·ü º¯°æ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDropItemProbabilityChange';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹ö¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDropItemProbabilityChange', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

