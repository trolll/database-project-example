CREATE TABLE [dbo].[TblChaosBattleInfo] (
    [mSvrNo]        SMALLINT      NOT NULL,
    [mCharNmPrefix] CHAR (2)      NOT NULL,
    [mCharNickNm]   CHAR (16)     NOT NULL,
    [mMark]         BINARY (1784) NOT NULL,
    [mMarkSeq]      SMALLINT      DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblChaosBattleInfo] PRIMARY KEY CLUSTERED ([mSvrNo] ASC) WITH (FILLFACTOR = 90)
);


GO

