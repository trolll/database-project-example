CREATE TABLE [dbo].[TblQuestCondition] (
    [mQuestNo] INT           NOT NULL,
    [mType]    TINYINT       NOT NULL,
    [mID]      INT           CONSTRAINT [DF_TblQuestCondition_mID] DEFAULT (0) NOT NULL,
    [mCnt]     INT           CONSTRAINT [DF_TblQuestCondition_mCnt] DEFAULT (0) NOT NULL,
    [mDesc]    VARCHAR (100) NULL,
    [mSeqNo]   INT           NOT NULL,
    CONSTRAINT [UCL_PK_TblQuestCondition_mQuestNo] PRIMARY KEY CLUSTERED ([mQuestNo] ASC, [mType] ASC, [mID] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblQuestCondition_mSeqNo]
    ON [dbo].[TblQuestCondition]([mSeqNo] ASC) WITH (FILLFACTOR = 90);


GO

