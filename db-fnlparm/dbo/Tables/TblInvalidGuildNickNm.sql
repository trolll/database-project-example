CREATE TABLE [dbo].[TblInvalidGuildNickNm] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblInvali__mRegD__36670980] DEFAULT (getdate()) NOT NULL,
    [mString]  VARCHAR (16) NOT NULL,
    CONSTRAINT [PkTblInvalidGuildNickNm] PRIMARY KEY CLUSTERED ([mString] ASC) WITH (FILLFACTOR = 90)
);


GO

