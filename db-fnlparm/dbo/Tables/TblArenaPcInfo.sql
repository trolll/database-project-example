CREATE TABLE [dbo].[TblArenaPcInfo] (
    [mRegDate]  SMALLDATETIME CONSTRAINT [DF_TblArenaPcInfo_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]    SMALLINT      CONSTRAINT [DF_TblArenaPcInfo_mSvrNo] DEFAULT ((0)) NOT NULL,
    [mPcNo]     INT           CONSTRAINT [DF_TblArenaPcInfo_mPcNo] DEFAULT ((0)) NOT NULL,
    [mPlayTime] INT           CONSTRAINT [DF_TblArenaPcInfo_mPlayTime] DEFAULT ((0)) NOT NULL,
    [mPanalty]  INT           CONSTRAINT [DF_TblArenaPcInfo_mPanalty] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblArenaPcInfo] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mPcNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'酒饭唱 PC 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaPcInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'菩澄萍', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaPcInfo', @level2type = N'COLUMN', @level2name = N'mPanalty';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaPcInfo', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaPcInfo', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'敲饭捞矫埃(檬)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaPcInfo', @level2type = N'COLUMN', @level2name = N'mPlayTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaPcInfo', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

