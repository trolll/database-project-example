CREATE TABLE [dbo].[TblMonsterSpot] (
    [mGID]            INT        NOT NULL,
    [mMID]            INT        NOT NULL,
    [mCnt]            INT        NOT NULL,
    [mTick]           INT        NOT NULL,
    [mDir]            FLOAT (53) NOT NULL,
    [mVarRespawnTick] INT        CONSTRAINT [DF_TblMonsterSpot_mVarRespawnTick] DEFAULT (0) NOT NULL,
    [mIsEvent]        BIT        DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblMonsterRespwan] PRIMARY KEY CLUSTERED ([mGID] ASC, [mMID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'捞亥飘 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMonsterSpot', @level2type = N'COLUMN', @level2name = N'mIsEvent';


GO

