CREATE TABLE [dbo].[DT_AchieveGuildList] (
    [mAchieveGuildID] INT            NOT NULL,
    [mRegDate]        DATETIME       DEFAULT (getdate()) NOT NULL,
    [mGuildRank]      TINYINT        CONSTRAINT [DF_DT_AchieveGuildList_mGuildRank] DEFAULT (0) NOT NULL,
    [mGuildName]      NVARCHAR (50)  CONSTRAINT [DF_DT_AchieveGuildList_mGuildName] DEFAULT ('') NOT NULL,
    [mGuildNamekey]   NVARCHAR (128) CONSTRAINT [DF_DT_AchieveGuildList_mGuildNameKey] DEFAULT ('') NOT NULL,
    [mMemberName]     NVARCHAR (50)  CONSTRAINT [DF_DT_AchieveGuildList_mMemberName] DEFAULT ('') NOT NULL,
    [mMemberNameKey]  NVARCHAR (128) CONSTRAINT [DF_DT_AchieveGuildList_mMemberNameKey] DEFAULT ('') NOT NULL,
    [mEquipPoint]     INT            CONSTRAINT [DF_DT_AchieveGuildList_mEquipPoint] DEFAULT (0) NOT NULL,
    [mUpdateIndex]    INT            CONSTRAINT [DF_DT_AchieveGuildList_mUpdateIndex] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_DT_AchieveGuildList] PRIMARY KEY CLUSTERED ([mAchieveGuildID] ASC) WITH (FILLFACTOR = 90)
);


GO

