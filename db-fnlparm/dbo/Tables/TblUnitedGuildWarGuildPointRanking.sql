CREATE TABLE [dbo].[TblUnitedGuildWarGuildPointRanking] (
    [mRegDate]    SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mSvrNo]      SMALLINT      NOT NULL,
    [mRanking]    SMALLINT      IDENTITY (1, 1) NOT NULL,
    [mGuildNm]    CHAR (12)     NOT NULL,
    [mGuildPoint] INT           NOT NULL,
    [mGuildNo]    INT           NOT NULL,
    CONSTRAINT [UNC_TblUnitedGuildWarGuildPointRanking] PRIMARY KEY NONCLUSTERED ([mRanking] ASC) WITH (FILLFACTOR = 90)
);


GO

