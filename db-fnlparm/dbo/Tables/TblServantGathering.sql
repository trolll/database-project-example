CREATE TABLE [dbo].[TblServantGathering] (
    [SServerType] TINYINT NOT NULL,
    [SIsSpeed]    BIT     NOT NULL,
    [SServantIID] INT     NOT NULL,
    [SResultIID]  INT     NOT NULL,
    [SCount]      TINYINT NOT NULL,
    CONSTRAINT [PK_TblServantGathering] PRIMARY KEY CLUSTERED ([SServerType] ASC, [SIsSpeed] ASC, [SServantIID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 盲笼 焊惑 瘤鞭 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantGathering', @level2type = N'COLUMN', @level2name = N'SCount';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 IID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantGathering', @level2type = N'COLUMN', @level2name = N'SServantIID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 盲笼 焊惑 IID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantGathering', @level2type = N'COLUMN', @level2name = N'SResultIID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚 汲沥 胶乔靛(抛胶飘) 牢瘤 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantGathering', @level2type = N'COLUMN', @level2name = N'SIsSpeed';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 盲笼 焊惑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantGathering';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚 备盒 (坷府瘤澄 / 坷锹)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantGathering', @level2type = N'COLUMN', @level2name = N'SServerType';


GO

