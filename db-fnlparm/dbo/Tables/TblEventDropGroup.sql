CREATE TABLE [dbo].[TblEventDropGroup] (
    [DGroup]   INT  NOT NULL,
    [MClass]   INT  NOT NULL,
    [DPercent] REAL NOT NULL,
    CONSTRAINT [UNC_TblEventDropGroup_DGroup_MClass] UNIQUE NONCLUSTERED ([DGroup] ASC, [MClass] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 努贰胶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropGroup', @level2type = N'COLUMN', @level2name = N'MClass';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 犬伏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropGroup', @level2type = N'COLUMN', @level2name = N'DPercent';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'捞亥飘 靛酚 弊缝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 弊缝 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropGroup', @level2type = N'COLUMN', @level2name = N'DGroup';


GO

