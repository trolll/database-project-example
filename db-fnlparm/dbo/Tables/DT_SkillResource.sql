CREATE TABLE [dbo].[DT_SkillResource] (
    [RID]       INT           NOT NULL,
    [ROwnerID]  INT           NULL,
    [RType]     INT           NULL,
    [RFileName] NVARCHAR (50) NULL,
    [RPosX]     INT           NULL,
    [RPosY]     INT           NULL
);


GO

