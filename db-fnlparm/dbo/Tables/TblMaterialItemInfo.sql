CREATE TABLE [dbo].[TblMaterialItemInfo] (
    [IID]      INT      NOT NULL,
    [MType]    SMALLINT NOT NULL,
    [MGrade]   SMALLINT NOT NULL,
    [MLevel]   SMALLINT NOT NULL,
    [MEnchant] SMALLINT NOT NULL,
    CONSTRAINT [UCL_PK_TblMaterialItemInfo] PRIMARY KEY CLUSTERED ([IID] ASC) WITH (FILLFACTOR = 90)
);


GO

