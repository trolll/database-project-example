CREATE TABLE [dbo].[TblEventObj] (
    [mID]      INT     IDENTITY (1, 1) NOT NULL,
    [mEventID] INT     NOT NULL,
    [mObjType] TINYINT NOT NULL,
    [mObjID]   INT     NOT NULL,
    CONSTRAINT [CL_PKTblEventObj] PRIMARY KEY CLUSTERED ([mEventID] ASC, [mObjType] ASC, [mObjID] ASC) WITH (FILLFACTOR = 90)
);


GO

