CREATE TABLE [dbo].[TblSpotAttackerSector] (
    [mSpotPlace] INT          DEFAULT ((0)) NOT NULL,
    [mStartX]    FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mStartZ]    FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mEndX]      FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mEndZ]      FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mDesc]      VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TblSpotAttackerSector] PRIMARY KEY CLUSTERED ([mSpotPlace] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'³¡ XÁÂÇ¥', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector', @level2type = N'COLUMN', @level2name = N'mEndX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ÃÀÛ ZÁÂÇ¥', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector', @level2type = N'COLUMN', @level2name = N'mStartZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ÃÀÛ XÁÂÇ¥', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector', @level2type = N'COLUMN', @level2name = N'mStartX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Áö¿ª ÀÌ¸§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector', @level2type = N'COLUMN', @level2name = N'mDesc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SpotPlaceNo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector', @level2type = N'COLUMN', @level2name = N'mSpotPlace';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'½ºÆÌ ½À°ÝÀÚ Áö¿ª ¹üÀ§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'³¡ ZÁÂÇ¥', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpotAttackerSector', @level2type = N'COLUMN', @level2name = N'mEndZ';


GO

