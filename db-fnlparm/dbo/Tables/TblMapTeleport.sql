CREATE TABLE [dbo].[TblMapTeleport] (
    [mNo]        BIGINT     NOT NULL,
    [mPos1x]     FLOAT (53) NOT NULL,
    [mPos1y]     FLOAT (53) NOT NULL,
    [mPos1z]     FLOAT (53) NOT NULL,
    [mPos1Range] INT        NOT NULL,
    [mPos2x]     FLOAT (53) NOT NULL,
    [mPos2y]     FLOAT (53) NOT NULL,
    [mPos2z]     FLOAT (53) NOT NULL,
    [mPos2Range] INT        NOT NULL,
    [mDesc]      CHAR (50)  CONSTRAINT [DF_TblTeleportList_mDesc] DEFAULT ('') NOT NULL,
    [mRegDate]   DATETIME   CONSTRAINT [DF_TblMapTeleport_mRegDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_TblTeleportList] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

