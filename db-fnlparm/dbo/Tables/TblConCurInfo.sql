CREATE TABLE [dbo].[TblConCurInfo] (
    [mRegDate] DATETIME CONSTRAINT [DF__TblConCur__mRegD__619B8048] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]   SMALLINT NOT NULL,
    [mUserCnt] INT      NOT NULL,
    CONSTRAINT [PkTblConCurInfo] PRIMARY KEY CLUSTERED ([mRegDate] DESC, [mSvrNo] DESC) WITH (FILLFACTOR = 90)
);


GO

