CREATE TABLE [dbo].[TblMonMatchMonList] (
    [mPlace] INT DEFAULT (0) NOT NULL,
    [mMID]   INT DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblMonMatchMonList_mPlace] PRIMARY KEY CLUSTERED ([mPlace] ASC, [mMID] ASC) WITH (FILLFACTOR = 90)
);


GO

