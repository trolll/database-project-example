CREATE TABLE [dbo].[TblTradeList] (
    [mNo]       INT           NOT NULL,
    [mMonID]    INT           NOT NULL,
    [mRecvItem] INT           NOT NULL,
    [mRecvCnt]  INT           NOT NULL,
    [mGiveItem] INT           NOT NULL,
    [mGiveCnt]  INT           NOT NULL,
    [mTalkMsg]  VARCHAR (255) CONSTRAINT [DF_TblTradeList_mTalkMsg] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblTradeList] PRIMARY KEY CLUSTERED ([mMonID] ASC, [mRecvItem] ASC, [mRecvCnt] ASC) WITH (FILLFACTOR = 90)
);


GO

