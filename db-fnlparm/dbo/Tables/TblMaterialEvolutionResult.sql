CREATE TABLE [dbo].[TblMaterialEvolutionResult] (
    [MEID]     BIGINT NOT NULL,
    [IID]      INT    NOT NULL,
    [mPercent] REAL   NULL,
    CONSTRAINT [UCL_PK_TblMaterialEvolutionResult] PRIMARY KEY CLUSTERED ([MEID] ASC, [IID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblMaterialEvolutionResult_mPercent] CHECK ([mPercent] <= 100 and [mPercent] >= 0)
);


GO

