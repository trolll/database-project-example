CREATE TABLE [dbo].[TblOtherMerchantInfo] (
    [mMerchantID]    INT     NOT NULL,
    [mMaxSummonCnt]  TINYINT CONSTRAINT [DF_TblOtherMerchantInfo_mMaxSummonCnt] DEFAULT ((1)) NOT NULL,
    [mMaxBuyItemCnt] INT     CONSTRAINT [DF_TblOtherMerchantInfo_mMaxBuyItemCnt] DEFAULT ((0)) NOT NULL,
    [mMaxTrayCnt]    TINYINT CONSTRAINT [DF_TblOtherMerchantInfo_mMaxTrayCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblOtherMerchantInfo] PRIMARY KEY CLUSTERED ([mMerchantID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'ÀÌ°èÀÇ »óÀÎ Á¤º¸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»óÀÎID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantInfo', @level2type = N'COLUMN', @level2name = N'mMerchantID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»óÀÎÀÌ ¼ÒÈ¯µÇ´Â ÃÖ´ë È½¼ö', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantInfo', @level2type = N'COLUMN', @level2name = N'mMaxSummonCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÇÑ°³ÀÇ »óÇ°À» »ì¼öÀÖ´Â ÃÖ´ë°¹¼ö (0Àº ¹«Á¦ÇÑ)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantInfo', @level2type = N'COLUMN', @level2name = N'mMaxBuyItemCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»óÁ¡±¸¸Å½Ã tray ÃÖ´ë °¹¼ö (0Àº ±âº»°ª)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblOtherMerchantInfo', @level2type = N'COLUMN', @level2name = N'mMaxTrayCnt';


GO

