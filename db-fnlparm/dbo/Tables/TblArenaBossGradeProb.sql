CREATE TABLE [dbo].[TblArenaBossGradeProb] (
    [mPointGrade] INT  NOT NULL,
    [mMinPoint]   INT  NOT NULL,
    [mMaxPoint]   INT  NOT NULL,
    [mGrade1st]   REAL CONSTRAINT [DF_TblArenaBossGradeProb_1] DEFAULT ((0)) NOT NULL,
    [mGrade2nd]   REAL CONSTRAINT [DF_TblArenaBossGradeProb_2] DEFAULT ((0)) NOT NULL,
    [mGrade3rd]   REAL CONSTRAINT [DF_TblArenaBossGradeProb_3] DEFAULT ((0)) NOT NULL,
    [mGrade4th]   REAL CONSTRAINT [DF_TblArenaBossGradeProb_4] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblArenaBossGradeProb] PRIMARY KEY CLUSTERED ([mPointGrade] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mMaxPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2殿鞭犬伏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mGrade2nd';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'厘厚器牢飘殿鞭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mPointGrade';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3殿鞭犬伏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mGrade3rd';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1殿鞭犬伏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mGrade1st';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥家蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mMinPoint';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'焊胶傈 焊胶 家券殿鞭 犬伏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'4殿鞭犬伏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossGradeProb', @level2type = N'COLUMN', @level2name = N'mGrade4th';


GO

