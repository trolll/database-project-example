CREATE TABLE [dbo].[TP_SetItemInfo] (
    [mSetType] INT           NOT NULL,
    [mSetName] VARCHAR (100) NOT NULL,
    CONSTRAINT [UCL_PK_TP_SetItemInfo] PRIMARY KEY CLUSTERED ([mSetType] ASC) WITH (FILLFACTOR = 90)
);


GO

