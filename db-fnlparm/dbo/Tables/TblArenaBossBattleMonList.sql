CREATE TABLE [dbo].[TblArenaBossBattleMonList] (
    [mPlace]     INT CONSTRAINT [DF_TblArenaBossBattleMonList_mPlace] DEFAULT ((0)) NOT NULL,
    [mMID]       INT CONSTRAINT [DF_TblArenaBossBattleMonList_mMid] DEFAULT ((0)) NOT NULL,
    [mBossMID]   INT CONSTRAINT [DF_TblArenaBossBattleMonList_mBossMID] DEFAULT ((0)) NOT NULL,
    [mBossGrade] INT CONSTRAINT [DF_TblArenaBossBattleMonList_mBossGrade] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblArenaBossBattleMonList] PRIMARY KEY CLUSTERED ([mPlace] ASC, [mMID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PlaceNo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleMonList', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊胶 阁胶磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleMonList', @level2type = N'COLUMN', @level2name = N'mBossMID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'焊胶傈 阁胶磐 府胶飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleMonList';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleMonList', @level2type = N'COLUMN', @level2name = N'mMID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊胶 阁胶磐 殿鞭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleMonList', @level2type = N'COLUMN', @level2name = N'mBossGrade';


GO

