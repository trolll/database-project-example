CREATE TABLE [dbo].[DT_AttributeResist] (
    [AID]         INT           NOT NULL,
    [AType]       INT           NULL,
    [ALevel]      TINYINT       NULL,
    [ADamage]     SMALLINT      NULL,
    [ADiceDamage] NVARCHAR (10) NULL
);


GO

