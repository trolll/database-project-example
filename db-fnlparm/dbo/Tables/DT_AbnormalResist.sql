CREATE TABLE [dbo].[DT_AbnormalResist] (
    [AID]      INT     NOT NULL,
    [AType]    INT     NULL,
    [ALevel]   TINYINT NULL,
    [APercent] TINYINT NULL
);


GO

