CREATE TABLE [dbo].[TblFieldEventMonsterRatio] (
    [mEventType]     INT          NOT NULL,
    [mPerOrgMonster] INT          CONSTRAINT [DF_TblFieldEventMonsterRatio_mPerOrgMonster] DEFAULT (33) NOT NULL,
    [mPerChgMonster] INT          CONSTRAINT [DF_TblFieldEventMonsterRatio_mPerChgMonster] DEFAULT (33) NOT NULL,
    [mPerDelMonster] INT          CONSTRAINT [DF_TblFieldEventMonsterRatio_mPerDelMonster] DEFAULT (34) NOT NULL,
    [mDesc]          VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TblFieldEventSector_mEventType] PRIMARY KEY CLUSTERED ([mEventType] ASC) WITH (FILLFACTOR = 90)
);


GO

