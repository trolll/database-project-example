CREATE TABLE [dbo].[TblQuestRefMonster] (
    [mQuestNo]   INT NOT NULL,
    [mMonsterID] INT NOT NULL,
    CONSTRAINT [PK_CL_TblQuestRefMonster] PRIMARY KEY CLUSTERED ([mQuestNo] ASC, [mMonsterID] ASC) WITH (FILLFACTOR = 90)
);


GO

