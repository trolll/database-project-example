CREATE TABLE [dbo].[TblRegionQuestCondition] (
    [mQuestNo]  INT      NOT NULL,
    [mParmID]   INT      NOT NULL,
    [mBoss]     BIT      CONSTRAINT [DF_TblRegionQuestCondition_mBoss] DEFAULT ((0)) NOT NULL,
    [mStepCnt]  TINYINT  NOT NULL,
    [mStep1]    SMALLINT NOT NULL,
    [mStep2]    SMALLINT NOT NULL,
    [mStep3]    SMALLINT NOT NULL,
    [mTotalCnt] SMALLINT NOT NULL,
    CONSTRAINT [UCL_PK_TblRegionQuestCondition] PRIMARY KEY CLUSTERED ([mQuestNo] ASC, [mParmID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä«¿îÆ® ÇÕ°è', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mTotalCnt';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Áö¿ª Äù½ºÆ® Á¶°Ç', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Äù½ºÆ® ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mQuestNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'º¸½º ¿©ºÎ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mBoss';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ó½ºÅÍ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mParmID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1´Ü°è Ä«¿îÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mStep1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'´Ü°è Ä«¿îÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mStepCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3´Ü°è Ä«¿îÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mStep3';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2´Ü°è Ä«¿îÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestCondition', @level2type = N'COLUMN', @level2name = N'mStep2';


GO

