CREATE TABLE [dbo].[TblChaosFieldConInfo] (
    [mChaosBattleSvrNo] SMALLINT NOT NULL,
    [mFieldSvrNo]       SMALLINT NOT NULL,
    CONSTRAINT [UCL_TblChaosFieldConInfo] PRIMARY KEY CLUSTERED ([mChaosBattleSvrNo] ASC, [mFieldSvrNo] ASC) WITH (FILLFACTOR = 90)
);


GO

