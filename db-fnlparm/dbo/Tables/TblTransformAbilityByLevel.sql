CREATE TABLE [dbo].[TblTransformAbilityByLevel] (
    [mLevel]           INT      CONSTRAINT [DF_TblTransformAbilityByLevel_mLevel] DEFAULT (0) NOT NULL,
    [mMaxHP]           SMALLINT CONSTRAINT [DF_TblTransformAbilityByLevel_mMaxHP] DEFAULT (0) NOT NULL,
    [mMaxMP]           SMALLINT CONSTRAINT [DF_TblTransformAbilityByLevel_mMaxMP] DEFAULT (0) NOT NULL,
    [mMaxWeight]       SMALLINT CONSTRAINT [DF_TblTransformAbilityByLevel_mMaxWeight] DEFAULT (0) NOT NULL,
    [mShortAttackRate] SMALLINT CONSTRAINT [DF_TblTransformAbilityByLevel_mShortAttackRate] DEFAULT (0) NOT NULL,
    [mLongAttackRate]  SMALLINT CONSTRAINT [DF_TblTransformAbilityByLevel_mLongAttackRate] DEFAULT (0) NOT NULL,
    [mMoveRate]        SMALLINT CONSTRAINT [DF_TblTransformAbilityByLevel_mMoveRate] DEFAULT (0) NOT NULL,
    [mType]            INT      CONSTRAINT [DF_TblTransformAbilityByLevel_mType] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblTransformAbilityByLevel_mType_mLevel] PRIMARY KEY NONCLUSTERED ([mType] ASC, [mLevel] ASC) WITH (FILLFACTOR = 90)
);


GO

