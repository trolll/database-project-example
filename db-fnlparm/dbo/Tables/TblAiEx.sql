CREATE TABLE [dbo].[TblAiEx] (
    [mRegDate]                SMALLDATETIME CONSTRAINT [DF__TblAiEx__mRegDat__45F365D3] DEFAULT (getdate()) NOT NULL,
    [mAiId]                   INT           NOT NULL,
    [mDesc]                   CHAR (100)    NOT NULL,
    [mSkillPercent]           INT           CONSTRAINT [DF_TblAiEx_mSkillPercent] DEFAULT ((50)) NOT NULL,
    [mSkillCoolTime]          INT           CONSTRAINT [DF_TblAiEx_mSkillCoolTime] DEFAULT ((10)) NOT NULL,
    [mSkillPerUpToWe0]        INT           NULL,
    [mSkillPerUpToInside0]    INT           NULL,
    [mSkillPerUpToOutside0]   INT           NULL,
    [mSkillPerUpToWe1]        INT           NULL,
    [mSkillPerUpToInside1]    INT           NULL,
    [mSkillPerUpToOutside1]   INT           NULL,
    [mSkillPerUpToWe2]        INT           NULL,
    [mSkillPerUpToInside2]    INT           NULL,
    [mSkillPerUpToOutside2]   INT           NULL,
    [mSkillPerDownToWe0]      INT           NULL,
    [mSkillPerDownToInside0]  INT           NULL,
    [mSkillPerDownToOutside0] INT           NULL,
    [mSkillPerDownToWe1]      INT           NULL,
    [mSkillPerDownToInside1]  INT           NULL,
    [mSkillPerDownToOutside1] INT           NULL,
    [mSkillPerDownToWe2]      INT           NULL,
    [mSkillPerDownToInside2]  INT           NULL,
    [mSkillPerDownToOutside2] INT           NULL,
    [mItemPercent]            INT           CONSTRAINT [DF_TblAiEx_mItemPercent] DEFAULT ((50)) NOT NULL,
    [mItemCoolTime]           INT           CONSTRAINT [DF_TblAiEx_mItemCoolTime] DEFAULT ((60)) NOT NULL,
    [mItemPerUpBattle0]       INT           NULL,
    [mItemPerUpChase0]        INT           NULL,
    [mItemPerUpBattle1]       INT           NULL,
    [mItemPerUpChase1]        INT           NULL,
    [mItemPerUpBattle2]       INT           NULL,
    [mItemPerUpChase2]        INT           NULL,
    [mItemPerDownBattle0]     INT           NULL,
    [mItemPerDownChase0]      INT           NULL,
    [mItemPerDownBattle1]     INT           NULL,
    [mItemPerDownChase1]      INT           NULL,
    [mItemPerDownBattle2]     INT           NULL,
    [mItemPerDownChase2]      INT           NULL,
    CONSTRAINT [PkTblAiEx] PRIMARY KEY CLUSTERED ([mAiId] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToWe0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerUpChase2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToWe2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerDownBattle2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerUpChase1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToWe1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToInside0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 荤侩 扁霖 HP 厚啦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPercent';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 荤侩 扁霖 HP 厚啦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPercent';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Monster喊 AI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToWe0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'汲疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mDesc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToOutside0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerUpBattle1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerDownChase1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToOutside2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerUpBattle0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToWe2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerDownChase0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToWe1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToOutside1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToOutside1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 荤侩 林扁', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemCoolTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToOutside0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 荤侩 林扁', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillCoolTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AI ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mAiId';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerDownBattle0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToOutside2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerDownChase2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerUpBattle2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerDownBattle1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToInside2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerUpToInside1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToInside0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mItemPerUpChase0';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblAiEx', @level2type = N'COLUMN', @level2name = N'mSkillPerDownToInside2';


GO

