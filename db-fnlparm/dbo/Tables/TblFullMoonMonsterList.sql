CREATE TABLE [dbo].[TblFullMoonMonsterList] (
    [mMID]        INT CONSTRAINT [DF_TblFullMoonMonsterList_mMID] DEFAULT (0) NOT NULL,
    [mIsFullMoon] BIT DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblFullMoonMonsterList] PRIMARY KEY CLUSTERED ([mMID] ASC, [mIsFullMoon] ASC) WITH (FILLFACTOR = 90)
);


GO

