CREATE TABLE [dbo].[DT_AbnormalAttrChangeAbnormal] (
    [mID]             INT            NOT NULL,
    [mOriginAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrChangeAbnormal_mOriginAID] DEFAULT (0) NOT NULL,
    [mEffectAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrChangeAbnormal_mEffectAID] DEFAULT (0) NOT NULL,
    [mConditionType]  TINYINT        CONSTRAINT [DF_DT_AbnormalAttrChangeAbnormal_mConditionType] DEFAULT (0) NOT NULL,
    [mConditionAID]   INT            CONSTRAINT [DF_DT_AbnormalAttrChangeAbnormal_mConditionAID] DEFAULT (0) NOT NULL,
    [mDesc]           NVARCHAR (200) CONSTRAINT [DF_DT_AbnormalAttrChangeAbnormal_mDesc] DEFAULT (N'') NULL,
    [mConditionAType] INT            CONSTRAINT [DF_DT_AbnormalAttrChangeAbnormal_mConditionAType] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_DT_AbnormalAttrChangeAbnormal] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

