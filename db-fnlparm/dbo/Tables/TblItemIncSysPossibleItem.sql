CREATE TABLE [dbo].[TblItemIncSysPossibleItem] (
    [mIID]      INT     NOT NULL,
    [mStatus]   TINYINT CONSTRAINT [DF_TblItemIncSysPossibleItem_mStatus] DEFAULT (1) NOT NULL,
    [mCubeType] TINYINT CONSTRAINT [DF_TblItemIncSysPossibleItem_mCubeType] DEFAULT (0) NOT NULL,
    [mProb]     REAL    CONSTRAINT [DF_TblItemIncSysPossibleItem_mProb] DEFAULT (0) NOT NULL,
    [mResource] INT     CONSTRAINT [DF_TblItemIncSysPossibleItem_mResource] DEFAULT (0) NOT NULL,
    [mKind]     TINYINT CONSTRAINT [DF_TblItemIncSysPossibleItem_mKind] DEFAULT (1) NOT NULL,
    CONSTRAINT [UCL_PK_TblItemIncSysPossibleItem_mIID] PRIMARY KEY CLUSTERED ([mIID] ASC, [mStatus] ASC) WITH (FILLFACTOR = 90)
);


GO

