CREATE TABLE [dbo].[DT_SkillTreeNode] (
    [mSTNID]          INT           NOT NULL,
    [mSTID]           INT           NOT NULL,
    [mName]           NVARCHAR (40) CONSTRAINT [DF_DT_SkillTreeNode_mName] DEFAULT ('') NOT NULL,
    [mMaxLevel]       SMALLINT      NOT NULL,
    [mNodeType]       TINYINT       NOT NULL,
    [mIconSlotX]      SMALLINT      CONSTRAINT [DF_DT_SkillTreeNode_mIconSlotX] DEFAULT (0) NOT NULL,
    [mIconSlotY]      SMALLINT      CONSTRAINT [DF_DT_SkillTreeNode_mIconSlotY] DEFAULT (0) NOT NULL,
    [mLineN]          TINYINT       CONSTRAINT [DF_DT_SkillTreeNode_mLineN] DEFAULT (0) NOT NULL,
    [mLineE]          TINYINT       CONSTRAINT [DF_DT_SkillTreeNode_mLineE] DEFAULT (0) NOT NULL,
    [mLineS]          TINYINT       CONSTRAINT [DF_DT_SkillTreeNode_mLineS] DEFAULT (0) NOT NULL,
    [mLineW]          TINYINT       CONSTRAINT [DF_DT_SkillTreeNode_mLineW] DEFAULT (0) NOT NULL,
    [mTermOfValidity] SMALLINT      CONSTRAINT [DF_DT_SkillTreeNode_mTermOfValidity] DEFAULT (10000) NOT NULL,
    CONSTRAINT [UCL_PK_DT_SkillTreeNode_mSTNID] PRIMARY KEY CLUSTERED ([mSTNID] ASC) WITH (FILLFACTOR = 90)
);


GO

