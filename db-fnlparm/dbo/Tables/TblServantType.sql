CREATE TABLE [dbo].[TblServantType] (
    [IID]            INT      NOT NULL,
    [SCategory]      SMALLINT NOT NULL,
    [SEvolutionStep] TINYINT  NOT NULL,
    [SType]          SMALLINT NOT NULL,
    CONSTRAINT [UCL_PK_TblServantType] PRIMARY KEY CLUSTERED ([IID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® ¾ÆÀÌÅÛ Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantType', @level2type = N'COLUMN', @level2name = N'SType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ÁøÈ­ ´Ü°è', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantType', @level2type = N'COLUMN', @level2name = N'SEvolutionStep';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ´ëºÐ·ù', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantType', @level2type = N'COLUMN', @level2name = N'SCategory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ¾ÆÀÌÅÛ ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantType', @level2type = N'COLUMN', @level2name = N'IID';


GO

