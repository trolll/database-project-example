CREATE TABLE [dbo].[T_TblParmSvrOp] (
    [mOpNo]      INT        NOT NULL,
    [mIsSetup]   BIT        NOT NULL,
    [mOpValue1]  FLOAT (53) NOT NULL,
    [mOpValue2]  FLOAT (53) NOT NULL,
    [mOpValue3]  FLOAT (53) NOT NULL,
    [mOpValue4]  FLOAT (53) NOT NULL,
    [mOpValue5]  FLOAT (53) NOT NULL,
    [mOpValue6]  FLOAT (53) NOT NULL,
    [mOpValue7]  FLOAT (53) NOT NULL,
    [mOpValue8]  FLOAT (53) NOT NULL,
    [mOpValue9]  FLOAT (53) NOT NULL,
    [mOpValue10] FLOAT (53) NOT NULL
);


GO

