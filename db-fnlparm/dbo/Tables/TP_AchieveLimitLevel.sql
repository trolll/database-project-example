CREATE TABLE [dbo].[TP_AchieveLimitLevel] (
    [mLimitID] TINYINT    NOT NULL,
    [mSRange]  TINYINT    CONSTRAINT [DF_TP_AchieveLimitLevel_mSRange] DEFAULT (0) NOT NULL,
    [mERange]  TINYINT    CONSTRAINT [DF_TP_AchieveLimitLevel_mERange] DEFAULT (0) NOT NULL,
    [mProb]    FLOAT (53) CONSTRAINT [DF_TP_AchieveLimitLevel_mProb] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TP_AchieveLimitLevel] PRIMARY KEY CLUSTERED ([mLimitID] ASC) WITH (FILLFACTOR = 90)
);


GO

