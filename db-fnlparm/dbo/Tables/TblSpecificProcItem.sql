CREATE TABLE [dbo].[TblSpecificProcItem] (
    [mIID]    INT        NOT NULL,
    [mProcNo] INT        NOT NULL,
    [mAParam] BIGINT     CONSTRAINT [DF__TblSpecif__mAPar__3DBE1285] DEFAULT (0) NOT NULL,
    [mBParam] BIGINT     CONSTRAINT [DF__TblSpecif__mBPar__3EB236BE] DEFAULT (0) NOT NULL,
    [mCParam] BIGINT     CONSTRAINT [DF__TblSpecif__mCPar__3FA65AF7] DEFAULT (0) NOT NULL,
    [mDParam] FLOAT (53) DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblSpecificProcItem] PRIMARY KEY CLUSTERED ([mIID] ASC) WITH (FILLFACTOR = 90)
);


GO

