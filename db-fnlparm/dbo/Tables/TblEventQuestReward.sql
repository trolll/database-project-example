CREATE TABLE [dbo].[TblEventQuestReward] (
    [mEQuestNo] INT     NOT NULL,
    [mRank]     TINYINT NOT NULL,
    [mIID]      INT     NOT NULL,
    [mStatus]   TINYINT CONSTRAINT [DF_TblEventQuestReward_mStatus] DEFAULT (1) NOT NULL,
    [mBinding]  TINYINT CONSTRAINT [DF_TblEventQuestReward_mBinding] DEFAULT (0) NOT NULL,
    [mEffTime]  INT     CONSTRAINT [DF_TblEventQuestReward_mEffTime] DEFAULT (0) NOT NULL,
    [mValTime]  INT     CONSTRAINT [DF_TblEventQuestReward_mValTime] DEFAULT (0) NOT NULL,
    [mCnt]      INT     NOT NULL,
    CONSTRAINT [UCL_PK_TblEventQuestReward_mEQuestNo] PRIMARY KEY CLUSTERED ([mEQuestNo] ASC, [mRank] ASC, [mIID] ASC, [mStatus] ASC) WITH (FILLFACTOR = 90)
);


GO

