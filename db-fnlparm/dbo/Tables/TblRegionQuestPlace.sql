CREATE TABLE [dbo].[TblRegionQuestPlace] (
    [mQuestNo] INT NOT NULL,
    [mPlace]   INT NOT NULL,
    CONSTRAINT [UCL_PK_TblRegionQuestPlace] PRIMARY KEY CLUSTERED ([mQuestNo] ASC, [mPlace] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Áö¿ª Äù½ºÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Àå¼Ò', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestPlace', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Äù½ºÆ® ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuestPlace', @level2type = N'COLUMN', @level2name = N'mQuestNo';


GO

