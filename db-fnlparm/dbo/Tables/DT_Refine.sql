CREATE TABLE [dbo].[DT_Refine] (
    [RID]          INT      NOT NULL,
    [RItemID0]     INT      CONSTRAINT [DF__DT_Refine__RItem__4BEC364B] DEFAULT (0) NOT NULL,
    [RItemID1]     INT      CONSTRAINT [DF__DT_Refine__RItem__4CE05A84] DEFAULT (0) NOT NULL,
    [RItemID2]     INT      CONSTRAINT [DF__DT_Refine__RItem__4DD47EBD] DEFAULT (0) NOT NULL,
    [RItemID3]     INT      CONSTRAINT [DF__DT_Refine__RItem__4EC8A2F6] DEFAULT (0) NOT NULL,
    [RItemID4]     INT      CONSTRAINT [DF__DT_Refine__RItem__4FBCC72F] DEFAULT (0) NOT NULL,
    [RItemID5]     INT      CONSTRAINT [DF__DT_Refine__RItem__50B0EB68] DEFAULT (0) NOT NULL,
    [RItemID6]     INT      CONSTRAINT [DF__DT_Refine__RItem__51A50FA1] DEFAULT (0) NOT NULL,
    [RItemID7]     INT      CONSTRAINT [DF__DT_Refine__RItem__529933DA] DEFAULT (0) NOT NULL,
    [RItemID8]     INT      CONSTRAINT [DF__DT_Refine__RItem__538D5813] DEFAULT (0) NOT NULL,
    [RItemID9]     INT      CONSTRAINT [DF__DT_Refine__RItem__54817C4C] DEFAULT (0) NOT NULL,
    [RSuccess]     REAL     CONSTRAINT [DF__DT_Refine__RSucc__5575A085] DEFAULT (0) NULL,
    [RIsCreateCnt] SMALLINT CONSTRAINT [DF_DT_Refine_RIsCreateCnt] DEFAULT (0) NOT NULL,
    CONSTRAINT [CK_DT_Refine_RSuccess] CHECK ([RSuccess]<=(100) AND [RSuccess]>=(0))
);


GO

