CREATE TABLE [dbo].[DT_RestoreItem] (
    [iServerID]   TINYINT      NOT NULL,
    [strUserID]   VARCHAR (20) NOT NULL,
    [strChar]     VARCHAR (20) NOT NULL,
    [TID]         INT          NOT NULL,
    [strItemName] VARCHAR (50) NOT NULL,
    [Status]      VARCHAR (10) NULL,
    [iCount]      BIGINT       NOT NULL,
    [Confirm]     VARCHAR (10) NULL
);


GO

