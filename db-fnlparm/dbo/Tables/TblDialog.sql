CREATE TABLE [dbo].[TblDialog] (
    [mRegDate]  SMALLDATETIME  CONSTRAINT [DF_TblDialog_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMId]      INT            NOT NULL,
    [mClick]    VARCHAR (8000) NULL,
    [mDie]      VARCHAR (100)  NOT NULL,
    [mAttacked] VARCHAR (100)  NOT NULL,
    [mTarget]   VARCHAR (100)  NOT NULL,
    [mBear]     VARCHAR (100)  NOT NULL,
    [mGossip1]  VARCHAR (100)  NOT NULL,
    [mGossip2]  VARCHAR (100)  NOT NULL,
    [mGossip3]  VARCHAR (100)  NOT NULL,
    [mGossip4]  VARCHAR (100)  NOT NULL,
    [mUptDate]  SMALLDATETIME  CONSTRAINT [DF_TblDialog_mUptDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblDialog] PRIMARY KEY CLUSTERED ([mMId] ASC) WITH (FILLFACTOR = 90)
);


GO

