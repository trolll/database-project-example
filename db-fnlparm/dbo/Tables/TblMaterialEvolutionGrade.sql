CREATE TABLE [dbo].[TblMaterialEvolutionGrade] (
    [MGrade]          INT  NOT NULL,
    [MProbabilityAdd] REAL NULL,
    CONSTRAINT [UCL_TblMaterialEvolutionGrade_MGrade] PRIMARY KEY CLUSTERED ([MGrade] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblMaterialEvolutionGrade_MProbabilityAdd] CHECK ([MProbabilityAdd] <= 100 and [MProbabilityAdd] >= 0)
);


GO

