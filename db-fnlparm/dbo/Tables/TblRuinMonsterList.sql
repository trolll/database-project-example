CREATE TABLE [dbo].[TblRuinMonsterList] (
    [mSpotPlace] INT DEFAULT (0) NOT NULL,
    [mMID]       INT DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblRuinMonsterList] PRIMARY KEY CLUSTERED ([mSpotPlace] ASC, [mMID] ASC) WITH (FILLFACTOR = 90)
);


GO

