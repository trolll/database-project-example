CREATE TABLE [dbo].[TP_AchieveRarityPoint] (
    [mName]        NVARCHAR (50) NOT NULL,
    [mRarityID]    TINYINT       NOT NULL,
    [mRarityPoint] INT           NOT NULL,
    [mLevelHP]     FLOAT (53)    NOT NULL,
    [mLevelMP]     FLOAT (53)    NOT NULL,
    [mLevelWP]     FLOAT (53)    NOT NULL,
    [mWeightHP]    FLOAT (53)    NOT NULL,
    [mWeightMP]    FLOAT (53)    NOT NULL,
    [mWeightWP]    FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_TP_AchieveRarityPoint] PRIMARY KEY CLUSTERED ([mRarityID] ASC) WITH (FILLFACTOR = 90)
);


GO

