CREATE TABLE [dbo].[TblLevelupCoin] (
    [mSection]     INT    CONSTRAINT [DF_TblLevelupCoin_mSection] DEFAULT ((0)) NOT NULL,
    [mExp]         BIGINT CONSTRAINT [DF_TblLevelupCoin_mExp] DEFAULT ((0)) NOT NULL,
    [mRewardCoin]  INT    CONSTRAINT [DF_TblLevelupCoin_mRewardCoin] DEFAULT ((0)) NOT NULL,
    [mMinimumCoin] INT    CONSTRAINT [DF_TblLevelupCoin_mMinimumCoin] DEFAULT ((0)) NOT NULL,
    [mType]        INT    CONSTRAINT [DF_TblLevelupCoin_mType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblLevelupCoin_mSection] PRIMARY KEY CLUSTERED ([mSection] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°æÇèÄ¡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLevelupCoin', @level2type = N'COLUMN', @level2name = N'mExp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'º¸»ó ÁÖÈ­ÀÇ °³¼ö', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLevelupCoin', @level2type = N'COLUMN', @level2name = N'mRewardCoin';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'·¹º§ ¾÷ ÁÖÈ­', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLevelupCoin';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'º¸»ó ÁÖÈ­ÀÇ ÃÖ¼Ò °³¼ö', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLevelupCoin', @level2type = N'COLUMN', @level2name = N'mMinimumCoin';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLevelupCoin', @level2type = N'COLUMN', @level2name = N'mType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'±¸°£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLevelupCoin', @level2type = N'COLUMN', @level2name = N'mSection';


GO

