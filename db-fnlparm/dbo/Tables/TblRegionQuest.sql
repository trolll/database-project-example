CREATE TABLE [dbo].[TblRegionQuest] (
    [mQuestNo]    INT            NOT NULL,
    [mQuestNm]    NVARCHAR (50)  NOT NULL,
    [mQuestNmKey] NVARCHAR (128) CONSTRAINT [DF_TblRegionQuest_mQuestNmKey] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_CL_TblRegionQuest] PRIMARY KEY CLUSTERED ([mQuestNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Áö¿ª Äù½ºÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuest';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Äù½ºÆ® ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuest', @level2type = N'COLUMN', @level2name = N'mQuestNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÌ¸§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuest', @level2type = N'COLUMN', @level2name = N'mQuestNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Å°°ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRegionQuest', @level2type = N'COLUMN', @level2name = N'mQuestNmKey';


GO

