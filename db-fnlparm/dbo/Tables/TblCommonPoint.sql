CREATE TABLE [dbo].[TblCommonPoint] (
    [mSvrNo]                  SMALLINT NOT NULL,
    [mPcNo]                   INT      NOT NULL,
    [mVolitionOfHonor]        SMALLINT CONSTRAINT [DF__TblCommonPoint_mVolitionOfHonor] DEFAULT (500) NOT NULL,
    [mHonorPoint]             INT      DEFAULT (0) NOT NULL,
    [mChaosPoint]             BIGINT   DEFAULT (0) NOT NULL,
    [mRestoreVolitionOfHonor] SMALLINT CONSTRAINT [DF_TblCommonPoint_mRestoreVolitionOfHonor] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblCommonPoint] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mPcNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblCommonPoint_mChaosPoint] CHECK ([mChaosPoint] >= 0 and [mChaosPoint] <= 10000000000),
    CONSTRAINT [CK_TblCommonPoint_mHonorPoint] CHECK ([mHonorPoint] >= 0 and [mHonorPoint] <= 1000000000),
    CONSTRAINT [CK_TblCommonPoint_mVolitionOfHonor] CHECK ([mVolitionOfHonor] >= (-30000) and [mVolitionOfHonor] <= 30000)
);


GO

