CREATE TABLE [dbo].[TblEventDungeonInfo] (
    [mDungeonPlace]     INT          DEFAULT (0) NOT NULL,
    [mBossMID]          INT          NOT NULL,
    [mIngressBossIID]   INT          NOT NULL,
    [mIngressQuestIID]  INT          NOT NULL,
    [mIngressAID]       INT          NOT NULL,
    [mStartX]           FLOAT (53)   DEFAULT (0) NOT NULL,
    [mStartZ]           FLOAT (53)   DEFAULT (0) NOT NULL,
    [mEndX]             FLOAT (53)   DEFAULT (0) NOT NULL,
    [mEndZ]             FLOAT (53)   DEFAULT (0) NOT NULL,
    [mStartPosX]        FLOAT (53)   DEFAULT (0) NOT NULL,
    [mStartPosY]        FLOAT (53)   DEFAULT (0) NOT NULL,
    [mStartPosZ]        FLOAT (53)   DEFAULT (0) NOT NULL,
    [mDesc]             VARCHAR (50) NULL,
    [mDungeonType]      INT          DEFAULT ((0)) NOT NULL,
    [mMinArea]          INT          DEFAULT ((90000000)) NOT NULL,
    [mPartyCnt]         INT          DEFAULT ((20)) NOT NULL,
    [mPlayTime]         INT          DEFAULT ((30)) NOT NULL,
    [mAcceptTime]       INT          DEFAULT ((30)) NOT NULL,
    [mStartPosX2]       FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mStartPosY2]       FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mStartPosZ2]       FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [mTeleportPosRatio] INT          DEFAULT ((50)) NOT NULL,
    CONSTRAINT [UCL_PK_TblEventDungeonInfo] PRIMARY KEY CLUSTERED ([mDungeonPlace] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥家 承捞', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mMinArea';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'滴锅掳 矫累 困摹 X谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mStartPosX2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'力茄 牢盔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mPartyCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'滴锅掳 矫累 困摹 Y谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mStartPosY2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'滴锅掳 矫累 困摹 Z谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mStartPosZ2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'带傈 柳青矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mPlayTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'带傈 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mDungeonType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'涝厘困摹 厚啦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mTeleportPosRatio';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚没 柳青矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonInfo', @level2type = N'COLUMN', @level2name = N'mAcceptTime';


GO

