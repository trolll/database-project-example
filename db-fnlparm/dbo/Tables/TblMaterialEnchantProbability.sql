CREATE TABLE [dbo].[TblMaterialEnchantProbability] (
    [MEnchant]       INT NOT NULL,
    [MAddValue]      INT NULL,
    [MReinforceFail] INT NULL,
    CONSTRAINT [UCL_DT_MaterialReinforce_MEnchant] PRIMARY KEY CLUSTERED ([MEnchant] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblMaterialEnchantProbability_MAddValue] CHECK ([MAddValue] <= 100 and [MAddValue] >= 0),
    CONSTRAINT [CK_TblMaterialEnchantProbability_MReinforceFail] CHECK ([MReinforceFail] <= 100 and [MReinforceFail] >= 0)
);


GO

