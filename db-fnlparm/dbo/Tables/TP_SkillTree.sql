CREATE TABLE [dbo].[TP_SkillTree] (
    [mSTID]         INT           NOT NULL,
    [mName]         NVARCHAR (40) CONSTRAINT [DF_TP_SkillTree_mName] DEFAULT ('') NOT NULL,
    [mDestroyOrder] INT           NOT NULL,
    CONSTRAINT [UCL_PK_TP_SkillTree_mSTID] PRIMARY KEY CLUSTERED ([mSTID] ASC) WITH (FILLFACTOR = 90)
);


GO

