CREATE TABLE [dbo].[TP_AbnormalType] (
    [AType]      INT            NOT NULL,
    [AName]      NVARCHAR (40)  NULL,
    [AEffect]    INT            CONSTRAINT [DF_TP_AbnormalType_AEffect] DEFAULT (0) NOT NULL,
    [ARemovable] BIT            CONSTRAINT [DF_TP_ABNORMALTYPE_ARemovable] DEFAULT (0) NOT NULL,
    [AFileName]  NVARCHAR (100) CONSTRAINT [DF_TP_AbnormalType_AFileName] DEFAULT ('') NOT NULL,
    [AIconX]     SMALLINT       CONSTRAINT [DF_TP_AbnormalType_AIconX] DEFAULT (0) NOT NULL,
    [AIconY]     SMALLINT       CONSTRAINT [DF_TP_AbnormalType_AIconY] DEFAULT (0) NOT NULL,
    [ACopyable]  BIT            CONSTRAINT [DF_TP_AbnormalType_ACopyable] DEFAULT (0) NOT NULL
);


GO

CREATE CLUSTERED INDEX [NC_TP_AbnormalType]
    ON [dbo].[TP_AbnormalType]([AType] ASC) WITH (FILLFACTOR = 90);


GO

