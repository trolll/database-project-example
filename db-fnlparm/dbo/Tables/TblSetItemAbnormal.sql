CREATE TABLE [dbo].[TblSetItemAbnormal] (
    [mSetType] INT NOT NULL,
    [AID]      INT NOT NULL,
    CONSTRAINT [UCL_PK_TblSetItemAbnormal] PRIMARY KEY CLUSTERED ([mSetType] ASC, [AID] ASC) WITH (FILLFACTOR = 90)
);


GO

