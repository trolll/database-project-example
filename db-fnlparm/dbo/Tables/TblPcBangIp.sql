CREATE TABLE [dbo].[TblPcBangIp] (
    [SN]          INT           NOT NULL,
    [PSN]         INT           NOT NULL,
    [CSN]         INT           NOT NULL,
    [CustIP]      NVARCHAR (15) NOT NULL,
    [ServiceType] TINYINT       NULL,
    [DT_reg]      SMALLDATETIME NULL,
    [IsBlock]     BIT           NOT NULL
);


GO

