CREATE TABLE [dbo].[TblTr] (
    [mRegDate]  DATETIME  CONSTRAINT [DF__TblTr__mRegDate__2022C2A6] DEFAULT (getdate()) NOT NULL,
    [mIsValid]  BIT       CONSTRAINT [DF__TblTr__mIsValid__2116E6DF] DEFAULT (1) NOT NULL,
    [mWorldNo]  SMALLINT  NOT NULL,
    [mType]     TINYINT   NOT NULL,
    [mSvrNo]    SMALLINT  NOT NULL,
    [mTrDesc]   CHAR (50) NOT NULL,
    [mWaitTm]   INT       NULL,
    [mPeriodTm] INT       NULL,
    [mWeekDay]  INT       NULL,
    CONSTRAINT [PkTblTr] PRIMARY KEY CLUSTERED ([mWorldNo] ASC, [mType] ASC, [mSvrNo] ASC, [mTrDesc] ASC) WITH (FILLFACTOR = 90)
);


GO

