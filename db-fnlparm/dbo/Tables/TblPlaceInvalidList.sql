CREATE TABLE [dbo].[TblPlaceInvalidList] (
    [mPlace] INT      CONSTRAINT [DF_TblPlaceInvalidList_mPlace] DEFAULT (0) NOT NULL,
    [mType]  SMALLINT CONSTRAINT [DF_TblPlaceInvalidList_mType] DEFAULT (0) NOT NULL,
    [mValue] INT      CONSTRAINT [DF_TblPlaceInvalidList_mValue] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblPlaceInvalidList_mPlace] PRIMARY KEY CLUSTERED ([mPlace] ASC, [mType] ASC, [mValue] ASC) WITH (FILLFACTOR = 90)
);


GO

