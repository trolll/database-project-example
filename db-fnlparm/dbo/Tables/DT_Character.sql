CREATE TABLE [dbo].[DT_Character] (
    [CID]         INT           NOT NULL,
    [CName]       NVARCHAR (20) NULL,
    [CClass]      INT           NULL,
    [CLevel]      SMALLINT      NULL,
    [CExp]        INT           NULL,
    [CAttackRate] SMALLINT      NULL,
    [CMoveRate]   SMALLINT      NULL,
    [CHP]         SMALLINT      NULL,
    [CMP]         SMALLINT      NULL,
    [CZoneID]     INT           NULL,
    [CPosX]       REAL          NULL,
    [CPosY]       REAL          NULL,
    [CPosZ]       REAL          NULL,
    [CHead]       TINYINT       NULL,
    [CFace]       INT           NULL,
    [CBody]       INT           NULL
);


GO

