CREATE TABLE [dbo].[DT_SkillPack] (
    [mSPID]             INT            NOT NULL,
    [mName]             NVARCHAR (40)  CONSTRAINT [DF_DT_SkillPack_mName] DEFAULT ('') NOT NULL,
    [mIType]            INT            CONSTRAINT [DF_DT_SkillPack_mIType] DEFAULT (0) NOT NULL,
    [mIUseType]         INT            CONSTRAINT [DF_DT_SkillPack_mIUseType] DEFAULT (0) NOT NULL,
    [mISubType]         SMALLINT       CONSTRAINT [DF_DT_SkillPack_mISubType] DEFAULT (0) NOT NULL,
    [mTermOfValidity]   SMALLINT       CONSTRAINT [DF_DT_SkillPack_mTermOfValidity] DEFAULT (10000) NOT NULL,
    [mDesc]             NVARCHAR (200) CONSTRAINT [DF_DT_SkillPack_mDesc] DEFAULT ('') NULL,
    [mUseMsg]           NVARCHAR (50)  CONSTRAINT [DF_DT_SkillPack_mUseMsg] DEFAULT ('') NULL,
    [mUseRange]         SMALLINT       CONSTRAINT [DF_DT_SkillPack_mUseRange] DEFAULT (0) NOT NULL,
    [mUseClass]         TINYINT        CONSTRAINT [DF_DT_SkillPack_mUseClass] DEFAULT (0) NOT NULL,
    [mUseLevel]         SMALLINT       CONSTRAINT [DF_DT_SkillPack_mUseLevel] DEFAULT (0) NOT NULL,
    [mSpriteFile]       NVARCHAR (50)  CONSTRAINT [DF_DT_SkillPack_mSpriteFile] DEFAULT ('') NOT NULL,
    [mSpriteX]          INT            NOT NULL,
    [mSpriteY]          INT            NOT NULL,
    [mIsUseableUTGWSvr] BIT            NOT NULL,
    [mUseInAttack]      TINYINT        CONSTRAINT [DF_DT_SKILLPACK_mUseInAttack] DEFAULT (0) NOT NULL,
    [mIsDrop]           BIT            CONSTRAINT [DF_DT_SkillPack_mIsDrop] DEFAULT (1) NOT NULL,
    CONSTRAINT [UCL_PK_DT_SkillPack_mSPID] PRIMARY KEY CLUSTERED ([mSPID] ASC) WITH (FILLFACTOR = 90)
);


GO

