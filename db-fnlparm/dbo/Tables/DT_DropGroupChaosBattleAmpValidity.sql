CREATE TABLE [dbo].[DT_DropGroupChaosBattleAmpValidity] (
    [mDGroup]           INT     NOT NULL,
    [mChaosBattleAdvLv] TINYINT NOT NULL,
    [mAmpValidityValue] INT     NOT NULL,
    CONSTRAINT [UCL_DT_DropGroupChaosBattleAmpValidity] PRIMARY KEY CLUSTERED ([mDGroup] ASC, [mChaosBattleAdvLv] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_DT_DropGroupChaosBattleAmpValidity_mChaosBattleAdvLv] CHECK (0 < [mChaosBattleAdvLv])
);


GO

