CREATE TABLE [dbo].[DT_AbnormalAttrAttach] (
    [mID]             INT            NOT NULL,
    [mOriginAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrAttach_mOriginAID] DEFAULT (0) NOT NULL,
    [mEffectAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrAttach_mEffectAID] DEFAULT (0) NOT NULL,
    [mConditionType]  TINYINT        CONSTRAINT [DF_DT_AbnormalAttrAttach_mConditionType] DEFAULT (0) NOT NULL,
    [mConditionAType] INT            CONSTRAINT [DF_DT_AbnormalAttrAttach_mConditionAType] DEFAULT (0) NOT NULL,
    [mDesc]           NVARCHAR (200) CONSTRAINT [DF_DT_AbnormalAttrAttach_mDesc] DEFAULT (N'') NULL,
    CONSTRAINT [UCL_PK_DT_AbnormalAttrAttach] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

