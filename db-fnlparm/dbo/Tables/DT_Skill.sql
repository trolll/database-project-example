CREATE TABLE [dbo].[DT_Skill] (
    [SID]                    INT           NOT NULL,
    [SName]                  NVARCHAR (40) NULL,
    [SHitPlus]               SMALLINT      CONSTRAINT [DF_DT_Skill_SHitPlus] DEFAULT (0) NOT NULL,
    [SMPPerUse]              SMALLINT      CONSTRAINT [DF_DT_Skill_SMPPerUse] DEFAULT (0) NOT NULL,
    [SSpellNum]              SMALLINT      NULL,
    [SDesc]                  NVARCHAR (50) NULL,
    [SType]                  SMALLINT      CONSTRAINT [DF_DT_Skill_SType_1] DEFAULT (0) NULL,
    [SHPPerUse]              SMALLINT      CONSTRAINT [DF_DT_Skill_SHPPerUse] DEFAULT (0) NOT NULL,
    [SChaoUse]               SMALLINT      CONSTRAINT [DF_DT_Skill_SChaosUse] DEFAULT (0) NOT NULL,
    [mApplyRadius]           SMALLINT      CONSTRAINT [DF_DT_Skill_mApplyRadius] DEFAULT (0) NOT NULL,
    [mApplyCnt]              SMALLINT      CONSTRAINT [DF_DT_Skill_mApplyCnt] DEFAULT (0) NOT NULL,
    [mApplyRace]             TINYINT       CONSTRAINT [DF__DT_Skill__mApply__7211DF33] DEFAULT (0) NOT NULL,
    [mCastingDelay]          SMALLINT      CONSTRAINT [DF_DT_Skill_mCastingDelay] DEFAULT (0) NOT NULL,
    [mConsumeItem]           INT           CONSTRAINT [DF_DT_Skill_mConsumeItem] DEFAULT (0) NOT NULL,
    [mConsumeItemCnt]        TINYINT       CONSTRAINT [DF_DT_Skill_mConsumeItemCnt] DEFAULT (0) NOT NULL,
    [mActiveType]            SMALLINT      CONSTRAINT [DF_DT_Skill_mActiveType] DEFAULT (0) NOT NULL,
    [mAnimation]             NVARCHAR (30) CONSTRAINT [DF_DT_Skill_mAnimation] DEFAULT (N'') NULL,
    [mCastingSpeed]          SMALLINT      CONSTRAINT [DF_DT_Skill_mCastingSpeed] DEFAULT (0) NOT NULL,
    [mSkillEffect]           SMALLINT      CONSTRAINT [DF_DT_Skill_mSkillEffect] DEFAULT (0) NOT NULL,
    [mCAmShakeWhenHit]       INT           NOT NULL,
    [mCriticalEffectWhenHit] TINYINT       CONSTRAINT [DF_DT_Skill_mCriticalEffectWhenHit] DEFAULT (0) NOT NULL,
    [mActiveWeapon]          BIT           CONSTRAINT [DF_DT_Skill_mActiveWeapon] DEFAULT (0) NOT NULL,
    [mCoolTime]              INT           CONSTRAINT [DF_DT_SKILL_mCoolTime] DEFAULT (0) NOT NULL,
    [mCastingGroup]          SMALLINT      CONSTRAINT [DF_DT_SKILL_mCastingGroup] DEFAULT (0) NOT NULL,
    [mCoolTimeGroup]         SMALLINT      CONSTRAINT [DF_DT_SKILL_mCoolTimeGroup] DEFAULT (0) NOT NULL,
    [mConsumeItem2]          INT           CONSTRAINT [DF_DT_SKILL_mConsumeItem2] DEFAULT (0) NOT NULL,
    [mConsumeItemCnt2]       TINYINT       CONSTRAINT [DF_DT_SKILL_mConsumeItemCnt2] DEFAULT (0) NOT NULL,
    [mIsCancel]              BIT           CONSTRAINT [DF_DT_Skill_mIsCancel] DEFAULT (0) NOT NULL,
    [mIsAttack]              BIT           CONSTRAINT [DF_DT_SKILL_mIsAttack] DEFAULT (0) NOT NULL,
    CONSTRAINT [CK_DT_SKILL_CASTINGRANGE] CHECK ([mCastingDelay] <= 10000),
    CONSTRAINT [CK_DT_SKILL_COOLTIMERANGE] CHECK ([mCoolTime] <= 7200000)
);


GO

