CREATE TABLE [dbo].[DT_SkillTreeNodeItemCondition] (
    [mSTNIID]    INT NOT NULL,
    [mSTNICType] INT NOT NULL,
    [mParamA]    INT CONSTRAINT [DF_DT_SkillTreeNodeItemCondition_mParamA] DEFAULT (0) NOT NULL,
    [mParamB]    INT CONSTRAINT [DF_DT_SkillTreeNodeItemCondition_mParamB] DEFAULT (0) NOT NULL,
    [mParamC]    INT CONSTRAINT [DF_DT_SkillTreeNodeItemCondition_mParamC] DEFAULT (0) NOT NULL,
    CONSTRAINT [UNC_DT_SKILLTREENODEITEMCONDITION_mSTNIID_mSTNICType_mParamA_mParamB_mParamC] UNIQUE NONCLUSTERED ([mSTNIID] ASC, [mSTNICType] ASC, [mParamA] ASC, [mParamB] ASC, [mParamC] ASC) WITH (FILLFACTOR = 90)
);


GO

