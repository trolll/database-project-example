CREATE TABLE [dbo].[TblServantEvolution] (
    [IID]   INT NOT NULL,
    [STID1] INT NOT NULL,
    [STID2] INT NOT NULL,
    [RID]   INT NOT NULL,
    CONSTRAINT [UCL_PK_TblServantEvolution] PRIMARY KEY CLUSTERED ([IID] ASC, [RID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ÁøÈ­ ÇÊ¿ä ½ºÅ³ Æ®¸® ¾ÆÀÌµð 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantEvolution', @level2type = N'COLUMN', @level2name = N'STID1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ÁøÈ­ ÇÊ¿ä ½ºÅ³ Æ®¸® ¾ÆÀÌµð 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantEvolution', @level2type = N'COLUMN', @level2name = N'STID2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ÁøÈ­ °¡´É Á¦ÀÛ ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantEvolution', @level2type = N'COLUMN', @level2name = N'RID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® ¾ÆÀÌÅÛ ÁøÈ­ °¡´É ¸ñ·Ï', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantEvolution';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ¾ÆÀÌÅÛ ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantEvolution', @level2type = N'COLUMN', @level2name = N'IID';


GO

