CREATE TABLE [dbo].[TblLimitPlayCnt] (
    [mType]    TINYINT  NOT NULL,
    [mSvrNo]   SMALLINT NOT NULL,
    [mNo]      INT      NOT NULL,
    [mPlayCnt] INT      CONSTRAINT [DF_TblLimitPlayCnt_mPlayCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblLimitPlayCnt] PRIMARY KEY CLUSTERED ([mType] ASC, [mSvrNo] ASC, [mNo] ASC),
    CONSTRAINT [CK_LimitPlayCnt_mPlayCnt] CHECK ((0)<=[mPlayCnt])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鸥涝(烹辨傈 1, 评珐农 2)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayCnt', @level2type = N'COLUMN', @level2name = N'mType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayCnt', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'锅龋(mType=1 : 辨靛锅龋, mType=2 : 某腐磐锅龋)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayCnt', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'敲饭捞 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayCnt', @level2type = N'COLUMN', @level2name = N'mPlayCnt';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'敲饭捞冉荐 力茄沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayCnt';


GO

