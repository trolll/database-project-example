CREATE TABLE [dbo].[DT_Slain] (
    [SID]       INT      NOT NULL,
    [SType]     INT      NULL,
    [SLevel]    TINYINT  NULL,
    [SHitPlus]  SMALLINT NULL,
    [SDDPlus]   SMALLINT NULL,
    [SRDDPlus]  SMALLINT CONSTRAINT [DF_DT_Slain_SRDDPlus] DEFAULT (0) NOT NULL,
    [SRHitPlus] SMALLINT CONSTRAINT [DF_DT_Slain_SRHitPlus] DEFAULT (0) NOT NULL
);


GO

