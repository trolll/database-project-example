CREATE TABLE [dbo].[TP_MaterialGrade] (
    [MGrade]     SMALLINT      NOT NULL,
    [MGradeDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [UCL_PK_TP_MaterialGrade] PRIMARY KEY CLUSTERED ([MGrade] ASC) WITH (FILLFACTOR = 90)
);


GO

