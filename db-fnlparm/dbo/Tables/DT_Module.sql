CREATE TABLE [dbo].[DT_Module] (
    [MID]     INT      NOT NULL,
    [MType]   INT      NULL,
    [MLevel]  SMALLINT NULL,
    [MAParam] INT      NULL,
    [MBParam] INT      NULL,
    [MCParam] INT      NULL
);


GO

