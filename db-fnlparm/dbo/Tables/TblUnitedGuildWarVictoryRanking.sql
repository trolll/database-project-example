CREATE TABLE [dbo].[TblUnitedGuildWarVictoryRanking] (
    [mRegDate]    SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mSvrNo]      SMALLINT      NOT NULL,
    [mRanking]    SMALLINT      IDENTITY (1, 1) NOT NULL,
    [mGuildNm]    CHAR (12)     NOT NULL,
    [mVictoryCnt] INT           NOT NULL,
    CONSTRAINT [UNC_TblUnitedGuildWarVictoryRanking] PRIMARY KEY NONCLUSTERED ([mRanking] ASC) WITH (FILLFACTOR = 90)
);


GO

