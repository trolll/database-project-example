CREATE TABLE [dbo].[TblTeleportPointList] (
    [mRegDate] DATETIME   CONSTRAINT [DF__TblTelepo__mRegD__07EC11B9] DEFAULT (getdate()) NOT NULL,
    [mPosX]    FLOAT (53) NOT NULL,
    [mPosY]    FLOAT (53) NOT NULL,
    [mPosZ]    FLOAT (53) NOT NULL
);


GO

