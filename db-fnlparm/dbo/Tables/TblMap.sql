CREATE TABLE [dbo].[TblMap] (
    [mRegDate] DATETIME  CONSTRAINT [DF__TblMap__mRegDate__2C88998B] DEFAULT (getdate()) NOT NULL,
    [mNo]      INT       NOT NULL,
    [mDesc]    CHAR (40) NOT NULL,
    CONSTRAINT [PkTblMap] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90),
    CHECK (0 <= [mNo])
);


GO

