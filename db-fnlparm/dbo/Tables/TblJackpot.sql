CREATE TABLE [dbo].[TblJackpot] (
    [mSvrNo]      SMALLINT NOT NULL,
    [mCamp]       TINYINT  CONSTRAINT [DF_TblJackpot_mCamp] DEFAULT (0) NOT NULL,
    [mStackPoint] INT      CONSTRAINT [DF_TblJackpot_mStackPoint] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblJackpot_mSvrNo_mCamp] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mCamp] ASC) WITH (FILLFACTOR = 90)
);


GO

