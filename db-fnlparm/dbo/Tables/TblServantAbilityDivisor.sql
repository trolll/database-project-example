CREATE TABLE [dbo].[TblServantAbilityDivisor] (
    [SType]        SMALLINT NOT NULL,
    [SStrength]    REAL     NOT NULL,
    [SDexterity]   REAL     NOT NULL,
    [SInteligence] REAL     NOT NULL,
    CONSTRAINT [CL_PK_TblServantAbilityDivisor] PRIMARY KEY CLUSTERED ([SType] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityDivisor', @level2type = N'COLUMN', @level2name = N'SType';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® Å¸ÀÔº° ´É·ÂÄ¡ ºÐ¸ð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityDivisor';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Áö´É¿¡ ´ëÇÑ ºÐ¸ð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityDivisor', @level2type = N'COLUMN', @level2name = N'SInteligence';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Èû¿¡ ´ëÇÑ ºÐ¸ð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityDivisor', @level2type = N'COLUMN', @level2name = N'SStrength';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¹ÎÃ¸¿¡ ´ëÇÑ ºÐ¸ð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityDivisor', @level2type = N'COLUMN', @level2name = N'SDexterity';


GO

