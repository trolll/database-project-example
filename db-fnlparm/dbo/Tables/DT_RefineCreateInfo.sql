CREATE TABLE [dbo].[DT_RefineCreateInfo] (
    [mIDX]      INT     NOT NULL,
    [mRID]      INT     NOT NULL,
    [mGroup1]   TINYINT CONSTRAINT [DF_DT_RefineCreateInfo_mGroup1] DEFAULT (0) NOT NULL,
    [mGroup2]   TINYINT CONSTRAINT [DF_DT_RefineCreateInfo_mGroup2] DEFAULT (0) NOT NULL,
    [mSort]     TINYINT CONSTRAINT [DF_DT_RefineCreateInfo_mSort] DEFAULT (0) NOT NULL,
    [mItem0]    INT     CONSTRAINT [DF_DT_RefineCreateInfo_mItem0] DEFAULT (0) NOT NULL,
    [mItem1]    INT     CONSTRAINT [DF_DT_RefineCreateInfo_mItem1] DEFAULT (0) NOT NULL,
    [mItem2]    INT     CONSTRAINT [DF_DT_RefineCreateInfo_mItem2] DEFAULT (0) NOT NULL,
    [mItem3]    INT     CONSTRAINT [DF_DT_RefineCreateInfo_mItem3] DEFAULT (0) NOT NULL,
    [mCost]     INT     CONSTRAINT [DF_DT_RefineCreateInfo_mCost] DEFAULT (0) NOT NULL,
    [mNationOp] BIGINT  CONSTRAINT [DF_DT_RefineCreateInfo_mNationOp] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_mIDX] PRIMARY KEY CLUSTERED ([mIDX] ASC) WITH (FILLFACTOR = 90)
);


GO

