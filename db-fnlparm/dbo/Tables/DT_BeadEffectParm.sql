CREATE TABLE [dbo].[DT_BeadEffectParm] (
    [mBeadNo]  INT      NOT NULL,
    [mOrderNo] SMALLINT NOT NULL,
    [mParamA]  INT      NOT NULL,
    [mParamB]  INT      NOT NULL,
    [mParamC]  INT      NOT NULL,
    [mParamD]  INT      NOT NULL,
    [mParamE]  INT      NOT NULL,
    CHECK ([mBeadNo] <> 0),
    CONSTRAINT [UNC_DT_BeadEffectParm_mBeadNo_mOrderNo] UNIQUE NONCLUSTERED ([mBeadNo] ASC, [mOrderNo] ASC) WITH (FILLFACTOR = 90)
);


GO

