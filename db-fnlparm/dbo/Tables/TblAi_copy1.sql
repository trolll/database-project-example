CREATE TABLE [dbo].[TblAi_copy1] (
    [mRegDate]    SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mDesc]       CHAR (100)    NOT NULL,
    [mAiId]       SMALLINT      NOT NULL,
    [mFindItem]   SMALLINT      NOT NULL,
    [mRageHp]     SMALLINT      NOT NULL,
    [mFearHp]     SMALLINT      NOT NULL,
    [mAngryItem1] INT           NULL,
    [mAngryItem2] INT           NULL,
    [mAngryItem3] INT           NULL,
    [mAngryItem4] INT           NULL,
    [mAngryItem5] INT           NULL,
    [mAngryItem6] INT           NULL,
    [mRageItem1]  INT           NULL,
    [mRageItem2]  INT           NULL,
    [mRageItem3]  INT           NULL,
    [mRageItem4]  INT           NULL,
    [mRageItem5]  INT           NULL,
    [mRageItem6]  INT           NULL,
    PRIMARY KEY NONCLUSTERED ([mAiId] ASC) WITH (FILLFACTOR = 90),
    CHECK ((0)<=[mFearHp] AND [mFearHp]<=(100)),
    CHECK ((0)<=[mFindItem] AND [mFindItem]<=(100)),
    CHECK ((0)<=[mRageHp] AND [mRageHp]<=(100))
);


GO

