CREATE TABLE [dbo].[TblMerchantSellList] (
    [mIndex]   INT NOT NULL,
    [ListID]   INT NOT NULL,
    [ItemID]   INT NOT NULL,
    [Price]    INT NOT NULL,
    [Flag]     INT NOT NULL,
    [SortKey]  INT CONSTRAINT [DF_TblMerchantSellList_SortKey] DEFAULT (0) NOT NULL,
    [LimitCnt] INT CONSTRAINT [DF_TblMerchantSellList_LimitCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblMerchantSellList] PRIMARY KEY CLUSTERED ([ListID] ASC, [ItemID] ASC, [Flag] ASC, [SortKey] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Á¦ÇÑ¼ö·®(0ÀÌ¸é ¹«Á¦ÇÑ)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMerchantSellList', @level2type = N'COLUMN', @level2name = N'LimitCnt';


GO

