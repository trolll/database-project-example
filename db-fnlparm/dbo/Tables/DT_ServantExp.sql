CREATE TABLE [dbo].[DT_ServantExp] (
    [ELevel] SMALLINT NOT NULL,
    [EExp]   BIGINT   NOT NULL,
    [EType]  SMALLINT CONSTRAINT [DF_EType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_DT_ServantExp] PRIMARY KEY CLUSTERED ([EType] ASC, [ELevel] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'·¹º§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_ServantExp', @level2type = N'COLUMN', @level2name = N'ELevel';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版氰摹 鸥涝 (0:老馆, 1:蜡丰)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_ServantExp', @level2type = N'COLUMN', @level2name = N'EType';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® °æÇèÄ¡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_ServantExp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°æÇèÄ¡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_ServantExp', @level2type = N'COLUMN', @level2name = N'EExp';


GO

