CREATE TABLE [dbo].[TblSpecialScopeSkill] (
    [mSkillNo]         INT        CONSTRAINT [DF_TblSpecialScopeSkill_mSkillNo] DEFAULT ((0)) NOT NULL,
    [mType]            INT        CONSTRAINT [DF_TblSpecialScopeSkill_mType] DEFAULT ((0)) NOT NULL,
    [mIsSpecialRadius] INT        CONSTRAINT [DF_TblSpecialScopeSkill_mIsSpecialRadius] DEFAULT ((0)) NOT NULL,
    [mParamA]          FLOAT (53) CONSTRAINT [DF_TblSpecialScopeSkill_mParamA] DEFAULT ((0)) NOT NULL,
    [mParamB]          FLOAT (53) CONSTRAINT [DF_TblSpecialScopeSkill_mParamB] DEFAULT ((0)) NOT NULL,
    [mParamC]          FLOAT (53) CONSTRAINT [DF_TblSpecialScopeSkill_mParamC] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblSpecialScopeSkill_mSkillNo] PRIMARY KEY CLUSTERED ([mSkillNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill', @level2type = N'COLUMN', @level2name = N'mSkillNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÆÄ¶ó¹ÌÅÍ C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill', @level2type = N'COLUMN', @level2name = N'mParamC';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Æ¯¼ö¹üÀ§ ½ºÅ³', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÆÄ¶ó¹ÌÅÍ B', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill', @level2type = N'COLUMN', @level2name = N'mParamB';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÆÄ¶ó¹ÌÅÍ A', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill', @level2type = N'COLUMN', @level2name = N'mParamA';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Æ¯¼öÀû¿ë¹üÀ§ ½ºÅ³ ¿©ºÎ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill', @level2type = N'COLUMN', @level2name = N'mIsSpecialRadius';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Æ¯¼ö¹üÀ§½ºÅ³ Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSpecialScopeSkill', @level2type = N'COLUMN', @level2name = N'mType';


GO

