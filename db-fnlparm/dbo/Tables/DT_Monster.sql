CREATE TABLE [dbo].[DT_Monster] (
    [MID]                        INT           NOT NULL,
    [MName]                      NVARCHAR (40) NULL,
    [mLevel]                     INT           NULL,
    [MClass]                     INT           NULL,
    [MExp]                       INT           NULL,
    [MHIT]                       SMALLINT      NULL,
    [MMinD]                      SMALLINT      NULL,
    [MMaxD]                      SMALLINT      NULL,
    [MAttackRateOrg]             SMALLINT      NULL,
    [MMoveRateOrg]               SMALLINT      NULL,
    [MAttackRateNew]             SMALLINT      NULL,
    [MMoveRateNew]               SMALLINT      NULL,
    [MHP]                        SMALLINT      NULL,
    [MMP]                        SMALLINT      NULL,
    [MMoveRange]                 SMALLINT      NOT NULL,
    [MGbjType]                   SMALLINT      NOT NULL,
    [MRaceType]                  SMALLINT      NULL,
    [MAiType]                    SMALLINT      NOT NULL,
    [MCastingDelay]              SMALLINT      NOT NULL,
    [MChaotic]                   SMALLINT      NOT NULL,
    [MSameRace1]                 INT           NOT NULL,
    [MSameRace2]                 INT           NOT NULL,
    [MSameRace3]                 INT           NOT NULL,
    [MSameRace4]                 INT           NOT NULL,
    [mSightRange]                INT           NOT NULL,
    [mAttackRange]               INT           NOT NULL,
    [mSkillRange]                INT           NOT NULL,
    [mBodySize]                  INT           NOT NULL,
    [mDetectTransF]              SMALLINT      NOT NULL,
    [mDetectTransP]              SMALLINT      NOT NULL,
    [mDetectChao]                SMALLINT      NOT NULL,
    [mAiEx]                      INT           NULL,
    [mScale]                     FLOAT (53)    NOT NULL,
    [mIsResistTransF]            BIT           NOT NULL,
    [mIsEvent]                   BIT           NOT NULL,
    [mIsTest]                    BIT           NOT NULL,
    [mHPNew]                     SMALLINT      NOT NULL,
    [mMPNew]                     SMALLINT      NOT NULL,
    [mBuyMerchanID]              INT           NOT NULL,
    [mSellMerchanID]             INT           NOT NULL,
    [mChargeMerchanID]           INT           NOT NULL,
    [mTransformWeight]           SMALLINT      NOT NULL,
    [mNationOp]                  BIGINT        NOT NULL,
    [mHPRegen]                   SMALLINT      NOT NULL,
    [mMPRegen]                   SMALLINT      NOT NULL,
    [IContentsLv]                TINYINT       NOT NULL,
    [mIsEventTest]               BIT           NOT NULL,
    [mIsShowHp]                  BIT           NOT NULL,
    [mSupportType]               TINYINT       NOT NULL,
    [mVolitionOfHonor]           SMALLINT      NOT NULL,
    [mWMapIconType]              SMALLINT      NOT NULL,
    [mIsAmpliableTermOfValidity] BIT           NOT NULL,
    [mAttackType]                TINYINT       NOT NULL,
    [mTransType]                 TINYINT       NOT NULL,
    [mDPV]                       SMALLINT      NOT NULL,
    [mMPV]                       SMALLINT      NOT NULL,
    [mRPV]                       SMALLINT      NOT NULL,
    [mDDV]                       SMALLINT      NOT NULL,
    [mMDV]                       SMALLINT      NOT NULL,
    [mRDV]                       SMALLINT      NOT NULL,
    [mSubDDWhenCritical]         SMALLINT      CONSTRAINT [DF_DT_MONSTER_mSubDDWhenCritical] DEFAULT (0) NOT NULL,
    [mEnemySubCriticalHit]       SMALLINT      CONSTRAINT [DF_DT_MONSTER_mEnemySubCriticalHit] DEFAULT (0) NOT NULL,
    [mEventQuest]                TINYINT       CONSTRAINT [DF_DT_Monster_mEventQuest] DEFAULT (0) NOT NULL,
    [mEScale]                    FLOAT (53)    CONSTRAINT [DF_DT_Monster_mEScale] DEFAULT ((1.0)) NOT NULL,
    CONSTRAINT [PK_DT_Monster_SH] PRIMARY KEY CLUSTERED ([MID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Monster 捞棋飘 胶纳老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_Monster', @level2type = N'COLUMN', @level2name = N'mEScale';


GO

