CREATE TABLE [dbo].[TP_SkillTreeNodeItemConditionType] (
    [mSTNICType] INT          NOT NULL,
    [mDesc]      VARCHAR (50) NULL,
    [mParamA]    VARCHAR (50) NULL,
    [mParamB]    VARCHAR (50) NULL,
    [mParamC]    VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TP_SkillTreeNodeItemConditionType_mSTNICType] PRIMARY KEY CLUSTERED ([mSTNICType] ASC) WITH (FILLFACTOR = 90)
);


GO

