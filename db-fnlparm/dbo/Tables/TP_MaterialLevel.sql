CREATE TABLE [dbo].[TP_MaterialLevel] (
    [MLevel]     SMALLINT      NOT NULL,
    [MLevelDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [UCL_PK_TP_MaterialLevel] PRIMARY KEY CLUSTERED ([MLevel] ASC) WITH (FILLFACTOR = 90)
);


GO

