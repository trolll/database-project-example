CREATE TABLE [dbo].[DT_ItemBeadEffect] (
    [IID]     INT NOT NULL,
    [mBeadNo] INT NOT NULL,
    CONSTRAINT [UNC_DT_ItemBeadEffect_IID_mBeadNo] UNIQUE NONCLUSTERED ([IID] ASC, [mBeadNo] ASC) WITH (FILLFACTOR = 90)
);


GO

