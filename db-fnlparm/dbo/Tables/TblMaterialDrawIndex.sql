CREATE TABLE [dbo].[TblMaterialDrawIndex] (
    [MDID]             BIGINT         NOT NULL,
    [MDRD]             BIGINT         NOT NULL,
    [mResType]         INT            NOT NULL,
    [mMaxResCnt]       INT            NOT NULL,
    [mSuccess]         REAL           NULL,
    [mDesc]            NVARCHAR (500) NOT NULL,
    [mAddQuestionMark] SMALLINT       CONSTRAINT [DF_TblMaterialDrawIndex_mAddQuestionMark] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblMaterialDrawIndex_MDID] PRIMARY KEY CLUSTERED ([MDID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblMaterialDrawIndex_mSuccess] CHECK ([mSuccess] <= 100 and [mSuccess] >= 0)
);


GO

