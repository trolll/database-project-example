CREATE TABLE [dbo].[DT_AbnormalAttrIgnore] (
    [mID]             INT            NOT NULL,
    [mOriginAID]      INT            CONSTRAINT [DF_DT_AbnormalAttrIgnore_mOriginAID] DEFAULT (0) NOT NULL,
    [mConditionType]  TINYINT        CONSTRAINT [DF_DT_AbnormalAttrIgnore_mConditionType] DEFAULT (0) NOT NULL,
    [mConditionAType] INT            CONSTRAINT [DF_DT_AbnormalAttrIgnore_mConditionAType] DEFAULT (0) NOT NULL,
    [mIsComplex]      BIT            CONSTRAINT [DF_DT_AbnormalAttrIgnore_mIsComplex] DEFAULT (0) NOT NULL,
    [mDesc]           NVARCHAR (200) CONSTRAINT [DF_DT_AbnormalAttrIgnore_mDesc] DEFAULT (N'') NULL,
    [mConditionAID]   INT            CONSTRAINT [DF_DT_AbnormalAttrIgnore_mConditionAID] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_DT_AbnormalAttrIgnore] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

