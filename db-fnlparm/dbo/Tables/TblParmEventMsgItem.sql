CREATE TABLE [dbo].[TblParmEventMsgItem] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF_TblParmEventMsgItem_mRegDate] DEFAULT (getdate()) NOT NULL,
    [IID]      INT           NOT NULL,
    CONSTRAINT [UCL_TblParmEventMsgItem_IID] PRIMARY KEY CLUSTERED ([IID] ASC) WITH (FILLFACTOR = 90)
);


GO

