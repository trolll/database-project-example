CREATE TABLE [dbo].[TblRuinSpotRange] (
    [mSpotPlace] INT          DEFAULT (0) NOT NULL,
    [mStartX]    FLOAT (53)   DEFAULT (0) NOT NULL,
    [mStartZ]    FLOAT (53)   DEFAULT (0) NOT NULL,
    [mEndX]      FLOAT (53)   DEFAULT (0) NOT NULL,
    [mEndZ]      FLOAT (53)   DEFAULT (0) NOT NULL,
    [mDesc]      VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TblRuinSpotRange] PRIMARY KEY CLUSTERED ([mSpotPlace] ASC) WITH (FILLFACTOR = 90)
);


GO

