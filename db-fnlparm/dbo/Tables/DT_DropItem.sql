CREATE TABLE [dbo].[DT_DropItem] (
    [DDrop]    INT      NULL,
    [DItem]    INT      NULL,
    [DNumber]  SMALLINT NULL,
    [DStatus]  TINYINT  CONSTRAINT [DF_DT_DROPITEM_DStatus] DEFAULT (1) NOT NULL,
    [DIsEvent] BIT      CONSTRAINT [DF_DT_DropItem_DIsEvent] DEFAULT (0) NOT NULL
);


GO

