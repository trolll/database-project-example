CREATE TABLE [dbo].[TblCastleSealStone] (
    [mNo]      BIGINT     NOT NULL,
    [mPosx]    FLOAT (53) NOT NULL,
    [mPosy]    FLOAT (53) NOT NULL,
    [mPosz]    FLOAT (53) NOT NULL,
    [mWidth]   FLOAT (53) NOT NULL,
    [mDir]     FLOAT (53) NOT NULL,
    [mDesc]    CHAR (50)  NOT NULL,
    [mRegDate] DATETIME   NULL
);


GO

