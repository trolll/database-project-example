CREATE TABLE [dbo].[TblEvent] (
    [mEventID]     INT           NOT NULL,
    [mTitle]       VARCHAR (100) NOT NULL,
    [mIsEverEvent] BIT           CONSTRAINT [DF_TblEvent_mIsEverEvent] DEFAULT (0) NOT NULL,
    [mDesc]        VARCHAR (400) NULL,
    CONSTRAINT [CL_PKTblEvent] PRIMARY KEY CLUSTERED ([mEventID] ASC) WITH (FILLFACTOR = 90)
);


GO

