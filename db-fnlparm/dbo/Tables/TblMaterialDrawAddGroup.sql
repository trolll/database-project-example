CREATE TABLE [dbo].[TblMaterialDrawAddGroup] (
    [MDRD]       BIGINT        NOT NULL,
    [mAddGroup]  INT           NOT NULL,
    [mResType]   INT           NOT NULL,
    [mMaxResCnt] INT           NOT NULL,
    [mSuccess]   REAL          NOT NULL,
    [mDesc]      VARCHAR (500) NOT NULL,
    CONSTRAINT [UCL_TblMaterialDrawAddGroup_MDRD] PRIMARY KEY CLUSTERED ([MDRD] ASC, [mAddGroup] ASC) WITH (FILLFACTOR = 90)
);


GO

