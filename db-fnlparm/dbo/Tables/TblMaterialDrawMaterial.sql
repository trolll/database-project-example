CREATE TABLE [dbo].[TblMaterialDrawMaterial] (
    [mSeq] INT    NOT NULL,
    [MDID] BIGINT NOT NULL,
    [IID]  INT    NOT NULL,
    [mCnt] INT    NOT NULL
);


GO

CREATE UNIQUE CLUSTERED INDEX [UCL_TblMaterialDrawMaterial_mSeq]
    ON [dbo].[TblMaterialDrawMaterial]([mSeq] ASC) WITH (FILLFACTOR = 90);


GO

