CREATE TABLE [dbo].[DT_SkillIgnoreCastingDelayGroup] (
    [SGroupNo]     SMALLINT NOT NULL,
    [SIgnoreGroup] SMALLINT NOT NULL,
    CONSTRAINT [CL_PKIgnoreCasting] PRIMARY KEY CLUSTERED ([SGroupNo] ASC, [SIgnoreGroup] ASC) WITH (FILLFACTOR = 90)
);


GO

