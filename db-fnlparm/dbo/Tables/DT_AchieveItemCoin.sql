CREATE TABLE [dbo].[DT_AchieveItemCoin] (
    [IID]     INT     NOT NULL,
    [mGrade]  TINYINT CONSTRAINT [DF_DT_AchieveItemCoin_mGrade] DEFAULT (1) NOT NULL,
    [mRarity] TINYINT CONSTRAINT [DF_DT_AchieveItemCoin_mRarity] DEFAULT (1) NOT NULL,
    CONSTRAINT [PK_DT_AchieveItemCoin] PRIMARY KEY CLUSTERED ([IID] ASC) WITH (FILLFACTOR = 90)
);


GO

