CREATE TABLE [dbo].[DT_Region] (
    [RegNo]    INT        NOT NULL,
    [RegionID] INT        NOT NULL,
    [Type]     SMALLINT   NOT NULL,
    [ID]       SMALLINT   NOT NULL,
    [Name]     NCHAR (40) NOT NULL,
    PRIMARY KEY CLUSTERED ([RegNo] ASC) WITH (FILLFACTOR = 90)
);


GO

