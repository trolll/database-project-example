CREATE TABLE [dbo].[TP_AchieveItemLevel] (
    [mLevel] INT NOT NULL,
    [mExp]   INT NOT NULL,
    CONSTRAINT [PK_TP_AchieveItemLevel] PRIMARY KEY CLUSTERED ([mLevel] ASC) WITH (FILLFACTOR = 90)
);


GO

