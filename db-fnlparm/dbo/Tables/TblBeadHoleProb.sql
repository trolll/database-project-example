CREATE TABLE [dbo].[TblBeadHoleProb] (
    [IID]           INT     NOT NULL,
    [mMaxHoleCount] TINYINT NOT NULL,
    [mHoleCount]    TINYINT NOT NULL,
    [mProb]         TINYINT NOT NULL,
    CHECK (0 < [mHoleCount] and [mHoleCount] <= [mMaxHoleCount]),
    CHECK (0 < [mMaxHoleCount] and [mMaxHoleCount] <= 5),
    CHECK (0 <= [mProb] and [mProb] <= 100),
    CONSTRAINT [UNC_TblBeadHoleProb_IID_mMaxHoleCount_mHoleCount] UNIQUE NONCLUSTERED ([IID] ASC, [mMaxHoleCount] ASC, [mHoleCount] ASC) WITH (FILLFACTOR = 90)
);


GO

