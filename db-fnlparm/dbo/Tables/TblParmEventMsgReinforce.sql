CREATE TABLE [dbo].[TblParmEventMsgReinforce] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF_TblParmEventMsgReinforce_mRegDate] DEFAULT (getdate()) NOT NULL,
    [IID]      INT           NOT NULL,
    CONSTRAINT [UCL_TblParmEventMsgReinforce_IID] PRIMARY KEY CLUSTERED ([IID] ASC) WITH (FILLFACTOR = 90)
);


GO

