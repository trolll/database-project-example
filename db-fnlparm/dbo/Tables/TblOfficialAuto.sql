CREATE TABLE [dbo].[TblOfficialAuto] (
    [mIndex]     INT      NOT NULL,
    [mRegDate]   DATETIME NOT NULL,
    [mBeginSect] INT      NOT NULL,
    [mEndSect]   INT      NOT NULL,
    [mExp]       BIGINT   NOT NULL,
    [mItem]      INT      NOT NULL
);


GO

CREATE UNIQUE CLUSTERED INDEX [UCL_TblOfficialAuto_mIndex]
    ON [dbo].[TblOfficialAuto]([mIndex] ASC) WITH (FILLFACTOR = 90);


GO

