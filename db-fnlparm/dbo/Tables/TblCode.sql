CREATE TABLE [dbo].[TblCode] (
    [mSeq]      INT          NOT NULL,
    [mCodeType] SMALLINT     NOT NULL,
    [mCode]     SMALLINT     NOT NULL,
    [mCodeDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [UCL_PK_TblCode] PRIMARY KEY CLUSTERED ([mCodeType] DESC, [mCode] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblCode_1]
    ON [dbo].[TblCode]([mSeq] ASC) WITH (FILLFACTOR = 90);


GO

