CREATE TABLE [dbo].[TblMonMatchInfo] (
    [mPlace]  INT          DEFAULT (0) NOT NULL,
    [mStartX] FLOAT (53)   DEFAULT (0) NOT NULL,
    [mStartZ] FLOAT (53)   DEFAULT (0) NOT NULL,
    [mEndX]   FLOAT (53)   DEFAULT (0) NOT NULL,
    [mEndZ]   FLOAT (53)   DEFAULT (0) NOT NULL,
    [mDesc]   VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TblMonMatchInfo_mPlace] PRIMARY KEY CLUSTERED ([mPlace] ASC) WITH (FILLFACTOR = 90)
);


GO

