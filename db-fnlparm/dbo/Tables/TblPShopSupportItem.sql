CREATE TABLE [dbo].[TblPShopSupportItem] (
    [PShopClass] TINYINT NOT NULL,
    [IID]        INT     NOT NULL,
    CONSTRAINT [UCL_PK_TblPShopSupportItem] PRIMARY KEY CLUSTERED ([PShopClass] ASC, [IID] ASC) WITH (FILLFACTOR = 90)
);


GO

