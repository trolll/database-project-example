CREATE TABLE [dbo].[TblMaterialEvolutionMaterial] (
    [MEID]     BIGINT   NOT NULL,
    [MType]    SMALLINT NOT NULL,
    [MLevel]   SMALLINT NOT NULL,
    [MGrade]   SMALLINT NOT NULL,
    [mSuccess] REAL     NULL,
    CONSTRAINT [UCL_PK_TblMaterialEvolutionMaterial] PRIMARY KEY CLUSTERED ([MEID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblMaterialEvolutionMaterial_mSuccess] CHECK ([mSuccess] <= 100 and [mSuccess] >= 0)
);


GO

