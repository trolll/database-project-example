CREATE TABLE [dbo].[TblJackpotRewardList] (
    [mConfirmIID] INT     NOT NULL,
    [mIID]        INT     NOT NULL,
    [mRate]       REAL    NULL,
    [mItemStatus] TINYINT CONSTRAINT [DF_TblJackpotRewardList_mItemStatus] DEFAULT (1) NOT NULL,
    [mCnt]        INT     CONSTRAINT [DF_TblJackpotRewardList_mCnt] DEFAULT (1) NOT NULL
);


GO

