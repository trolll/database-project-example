CREATE TABLE [dbo].[TblGkillNode] (
    [mNode] INT           NOT NULL,
    [mDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [CL_PKTblGkillNode] PRIMARY KEY CLUSTERED ([mNode] ASC) WITH (FILLFACTOR = 90)
);


GO

