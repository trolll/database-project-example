CREATE TABLE [dbo].[TblPopupGuideDialog] (
    [mRegDate] SMALLDATETIME  CONSTRAINT [DF_TblPopupGuideDialog_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mGuideNo] INT            NOT NULL,
    [mSubject] VARCHAR (30)   NOT NULL,
    [mDialog]  VARCHAR (7000) NOT NULL,
    [mUptDate] SMALLDATETIME  CONSTRAINT [DF_TblPopupGuideDialog_mUptDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [UCL_TblPopupGuideDialog_mGuideNo] PRIMARY KEY CLUSTERED ([mGuideNo] ASC) WITH (FILLFACTOR = 90)
);


GO

