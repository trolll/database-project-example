CREATE TABLE [dbo].[TblConCurInfoNow] (
    [mRegDate] DATETIME CONSTRAINT [DF__TblConCur__mRegD__69E6AD86] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]   SMALLINT NOT NULL,
    [mUserCnt] INT      NOT NULL,
    [mLastChg] DATETIME NOT NULL,
    [mPcCnt]   INT      CONSTRAINT [DF_TblConCurInfoNow_mPcCnt] DEFAULT (0) NOT NULL,
    CONSTRAINT [CL_PK_TblConCurInfoNow] PRIMARY KEY CLUSTERED ([mSvrNo] DESC) WITH (FILLFACTOR = 90)
);


GO

