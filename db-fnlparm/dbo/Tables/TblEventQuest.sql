CREATE TABLE [dbo].[TblEventQuest] (
    [mEQuestNo] INT           NOT NULL,
    [mTitle]    VARCHAR (100) NOT NULL,
    [mType]     TINYINT       NOT NULL,
    [mDesc]     VARCHAR (400) NULL,
    CONSTRAINT [UCL_PK_TblEventQuest_mEQuestNo] PRIMARY KEY CLUSTERED ([mEQuestNo] ASC) WITH (FILLFACTOR = 90)
);


GO

