CREATE TABLE [dbo].[TblRegionOptions] (
    [mPlace]               INT        NOT NULL,
    [mIsSupport]           BIT        NOT NULL,
    [mExpRate]             FLOAT (53) NOT NULL,
    [mMonsterItemDropRate] FLOAT (53) NOT NULL,
    [mShowPlayerName]      BIT        NOT NULL,
    [mMonSilverDropRate]   FLOAT (53) CONSTRAINT [DF_TblRegionOptions_mMonSilverDropRate] DEFAULT (1) NOT NULL,
    [mNoDropItemOnDeath]   BIT        CONSTRAINT [DF_TblRegionOptions_mNoDropItemOnDeath] DEFAULT (0) NOT NULL,
    [mNoExpDescOnDeath]    BIT        CONSTRAINT [DF_TblRegionOptions_mNoExpDescOnDeath] DEFAULT (0) NOT NULL,
    PRIMARY KEY CLUSTERED ([mPlace] ASC) WITH (FILLFACTOR = 90)
);


GO

