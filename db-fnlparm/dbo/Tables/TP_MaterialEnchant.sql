CREATE TABLE [dbo].[TP_MaterialEnchant] (
    [MEnchant]     SMALLINT      NOT NULL,
    [MEnchantDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [UCL_PK_TP_MaterialEnchant] PRIMARY KEY CLUSTERED ([MEnchant] ASC) WITH (FILLFACTOR = 90)
);


GO

