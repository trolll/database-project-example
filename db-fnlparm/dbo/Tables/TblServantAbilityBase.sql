CREATE TABLE [dbo].[TblServantAbilityBase] (
    [SCategory]    SMALLINT NOT NULL,
    [SStrength]    SMALLINT NOT NULL,
    [SDexterity]   SMALLINT NOT NULL,
    [SInteligence] SMALLINT NOT NULL,
    CONSTRAINT [CL_PK_TblServantAbilityBase] PRIMARY KEY CLUSTERED ([SCategory] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Èû¿¡ ´ëÇÑ ±âº»°ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityBase', @level2type = N'COLUMN', @level2name = N'SStrength';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ´ëºÐ·ù', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityBase', @level2type = N'COLUMN', @level2name = N'SCategory';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® ´ëºÐ·ùº° ´É·ÂÄ¡ ±âº»°ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityBase';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Áö´É¿¡ ´ëÇÑ ±âº»°ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityBase', @level2type = N'COLUMN', @level2name = N'SInteligence';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¹ÎÃ¸¿¡ ´ëÇÑ ±âº»°ª', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantAbilityBase', @level2type = N'COLUMN', @level2name = N'SDexterity';


GO

