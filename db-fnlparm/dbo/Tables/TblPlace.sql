CREATE TABLE [dbo].[TblPlace] (
    [mPlaceNo]     INT          NOT NULL,
    [mPlaceNm]     VARCHAR (50) NOT NULL,
    [mTerritoryNm] VARCHAR (50) NULL,
    CONSTRAINT [PK_TblPlace] PRIMARY KEY CLUSTERED ([mPlaceNo] ASC) WITH (FILLFACTOR = 90)
);


GO

