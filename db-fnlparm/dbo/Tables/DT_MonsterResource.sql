CREATE TABLE [dbo].[DT_MonsterResource] (
    [RID]       INT           NOT NULL,
    [ROwnerID]  INT           NULL,
    [RType]     INT           NULL,
    [RFileName] NVARCHAR (50) NULL,
    [RPosX]     INT           NULL,
    [RPosY]     INT           NULL,
    CONSTRAINT [PK_DT_MonsterResource] PRIMARY KEY CLUSTERED ([RID] ASC) WITH (FILLFACTOR = 90)
);


GO

