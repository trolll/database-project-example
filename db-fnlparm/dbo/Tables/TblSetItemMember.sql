CREATE TABLE [dbo].[TblSetItemMember] (
    [mSetType] INT NOT NULL,
    [IID]      INT NOT NULL,
    CONSTRAINT [UCL_PK_TblSetItemMember] PRIMARY KEY CLUSTERED ([mSetType] ASC, [IID] ASC) WITH (FILLFACTOR = 90)
);


GO

