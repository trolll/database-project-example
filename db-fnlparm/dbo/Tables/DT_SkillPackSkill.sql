CREATE TABLE [dbo].[DT_SkillPackSkill] (
    [mSPID]     INT      NOT NULL,
    [mSID]      INT      NOT NULL,
    [mSOrderNO] SMALLINT NOT NULL,
    CONSTRAINT [UCL_PK_DT_SkillPackSkill_mSPID_mSID] PRIMARY KEY CLUSTERED ([mSPID] ASC, [mSID] ASC) WITH (FILLFACTOR = 90)
);


GO

