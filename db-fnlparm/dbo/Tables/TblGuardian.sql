CREATE TABLE [dbo].[TblGuardian] (
    [mRegDate] DATETIME   DEFAULT (getdate()) NOT NULL,
    [mNo]      BIGINT     NOT NULL,
    [mPosX]    FLOAT (53) NOT NULL,
    [mPosY]    FLOAT (53) NOT NULL,
    [mPosZ]    FLOAT (53) NOT NULL,
    CONSTRAINT [UCL_TblGuardian] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

