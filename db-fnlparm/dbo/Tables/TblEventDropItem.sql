CREATE TABLE [dbo].[TblEventDropItem] (
    [DDrop]    INT      NOT NULL,
    [DGroup]   INT      DEFAULT ((0)) NOT NULL,
    [DItem]    INT      DEFAULT ((0)) NOT NULL,
    [DNumber]  SMALLINT DEFAULT ((1)) NOT NULL,
    [DRate]    TINYINT  DEFAULT ((0)) NOT NULL,
    [DIsEvent] BIT      DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TblEventDropItem_DDrop] PRIMARY KEY CLUSTERED ([DDrop] ASC),
    CONSTRAINT [UNC_TblEventDropItem_DGroup_DItem] UNIQUE NONCLUSTERED ([DGroup] ASC, [DItem] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem', @level2type = N'COLUMN', @level2name = N'DDrop';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 厚吝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem', @level2type = N'COLUMN', @level2name = N'DRate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 弊缝 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem', @level2type = N'COLUMN', @level2name = N'DGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'捞亥飘 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem', @level2type = N'COLUMN', @level2name = N'DIsEvent';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 酒捞袍 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem', @level2type = N'COLUMN', @level2name = N'DItem';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'捞亥飘 靛酚 酒捞袍', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'靛酚 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDropItem', @level2type = N'COLUMN', @level2name = N'DNumber';


GO

