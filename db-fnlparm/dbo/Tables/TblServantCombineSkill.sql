CREATE TABLE [dbo].[TblServantCombineSkill] (
    [IID]  INT NOT NULL,
    [SPID] INT NOT NULL,
    CONSTRAINT [PK_TblServantCombineSkill] PRIMARY KEY CLUSTERED ([IID] ASC, [SPID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辑锅飘 钦己 胶懦 蒲 格废', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineSkill';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 酒捞袍 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineSkill', @level2type = N'COLUMN', @level2name = N'IID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 胶懦 蒲', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineSkill', @level2type = N'COLUMN', @level2name = N'SPID';


GO

