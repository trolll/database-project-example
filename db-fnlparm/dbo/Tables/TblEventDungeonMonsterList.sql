CREATE TABLE [dbo].[TblEventDungeonMonsterList] (
    [mDungeonPlace] INT DEFAULT (0) NOT NULL,
    [mMID]          INT DEFAULT (0) NOT NULL,
    [mCamp]         INT DEFAULT ((2)) NOT NULL,
    [mIsBoss]       BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblEventDungeonMonsterList] PRIMARY KEY CLUSTERED ([mDungeonPlace] ASC, [mMID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'柳康', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonMonsterList', @level2type = N'COLUMN', @level2name = N'mCamp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊胶咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblEventDungeonMonsterList', @level2type = N'COLUMN', @level2name = N'mIsBoss';


GO

