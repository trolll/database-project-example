CREATE TABLE [dbo].[TP_SkillCoolTimeGroup] (
    [SGroupNo] SMALLINT       NOT NULL,
    [SDesc]    NVARCHAR (100) NOT NULL,
    CONSTRAINT [UCL_TP_SkillCoolTimeGroup_SGroupNo] PRIMARY KEY CLUSTERED ([SGroupNo] ASC) WITH (FILLFACTOR = 90)
);


GO

