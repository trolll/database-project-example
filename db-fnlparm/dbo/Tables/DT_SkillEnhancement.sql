CREATE TABLE [dbo].[DT_SkillEnhancement] (
    [mESPID]    INT     NOT NULL,
    [mSPID]     INT     NOT NULL,
    [mOrderNo]  TINYINT NOT NULL,
    [mUseClass] TINYINT NOT NULL,
    CONSTRAINT [UCL_PK_DT_SkillEnhancement_mESPID] PRIMARY KEY CLUSTERED ([mESPID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°­È­µÈ ½ºÅ³ÆÑ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancement', @level2type = N'COLUMN', @level2name = N'mESPID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Æ¯¼º °­È­ ½ºÅ³', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancement';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»ç¿ë Å¬·¡½º', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancement', @level2type = N'COLUMN', @level2name = N'mUseClass';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼ø¼­', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancement', @level2type = N'COLUMN', @level2name = N'mOrderNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³ÆÑID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancement', @level2type = N'COLUMN', @level2name = N'mSPID';


GO

