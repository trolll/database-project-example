CREATE TABLE [dbo].[TP_SetItemEffectDesc] (
    [mSetType] INT           NOT NULL,
    [mDesc]    VARCHAR (200) NOT NULL,
    CONSTRAINT [UCL_PK_TP_SetItemEffectDesc] PRIMARY KEY CLUSTERED ([mSetType] ASC) WITH (FILLFACTOR = 90)
);


GO

