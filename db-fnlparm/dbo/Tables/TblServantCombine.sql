CREATE TABLE [dbo].[TblServantCombine] (
    [IID]       INT NOT NULL,
    [ResultIID] INT NOT NULL,
    CONSTRAINT [PK_TblServantCombine] PRIMARY KEY CLUSTERED ([IID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 饶 酒捞袍 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombine', @level2type = N'COLUMN', @level2name = N'ResultIID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 酒捞袍 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombine', @level2type = N'COLUMN', @level2name = N'IID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 搬苞', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombine';


GO

