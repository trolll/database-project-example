CREATE TABLE [dbo].[TP_AiRaidConditionType] (
    [mCID]         INT          NOT NULL,
    [mDesc]        VARCHAR (50) NOT NULL,
    [mCAParamName] VARCHAR (50) NULL,
    [mCBParamName] VARCHAR (50) NULL,
    [mCCParamName] VARCHAR (50) NULL,
    CONSTRAINT [PK_TP_AiRaidConditionType] PRIMARY KEY CLUSTERED ([mCID] ASC) WITH (FILLFACTOR = 90)
);


GO

