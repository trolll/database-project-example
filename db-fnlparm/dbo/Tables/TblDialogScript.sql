CREATE TABLE [dbo].[TblDialogScript] (
    [mRegDate]    SMALLDATETIME  CONSTRAINT [DF_TblDialogScript_mRegDate_1] DEFAULT (getdate()) NOT NULL,
    [mMId]        INT            NOT NULL,
    [mScriptText] VARCHAR (8000) CONSTRAINT [DF_TblDialogScript_mScriptText_1] DEFAULT ('') NOT NULL,
    [mUptDate]    SMALLDATETIME  CONSTRAINT [DF_TblDialogScript_mUptDate] DEFAULT (getdate()) NOT NULL
);


GO

