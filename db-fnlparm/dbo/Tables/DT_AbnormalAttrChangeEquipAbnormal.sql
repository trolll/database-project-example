CREATE TABLE [dbo].[DT_AbnormalAttrChangeEquipAbnormal] (
    [mID]            INT            NOT NULL,
    [mConditionAID]  INT            CONSTRAINT [DF_DT_AbnormalAttrChangeEquipAbnormal_mConditionAID] DEFAULT (0) NOT NULL,
    [mOriginAType]   INT            CONSTRAINT [DF_DT_AbnormalAttrChangeEquipAbnormal_mOriginAType] DEFAULT (0) NOT NULL,
    [mOriginAItemNo] INT            CONSTRAINT [DF_DT_AbnormalAttrChangeEquipAbnormal_mOriginAItemNo] DEFAULT (0) NOT NULL,
    [mEffectAID]     INT            CONSTRAINT [DF_DT_AbnormalAttrChangeEquipAbnormal_mEffectAID] DEFAULT (0) NOT NULL,
    [mDesc]          NVARCHAR (200) CONSTRAINT [DF_DT_AbnormalAttrChangeEquipAbnormal_mDesc] DEFAULT (N'') NULL,
    CONSTRAINT [UCL_PK_DT_AbnormalAttrChangeEquipAbnormal] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 90)
);


GO

