CREATE TABLE [dbo].[TblPcBangInfo] (
    [SN]            INT           NOT NULL,
    [PSN]           INT           NOT NULL,
    [CSN]           INT           NOT NULL,
    [GSN]           INT           NULL,
    [PCB_Level]     TINYINT       NULL,
    [ServiceType]   TINYINT       NULL,
    [RemainderTime] INT           NULL,
    [StartDate]     SMALLDATETIME NULL,
    [EndDate]       SMALLDATETIME NULL,
    [DT_reg]        SMALLDATETIME NULL
);


GO

