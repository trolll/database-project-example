CREATE TABLE [dbo].[DT_Bead] (
    [IID]          INT        NOT NULL,
    [mTargetIPos]  INT        NOT NULL,
    [mProb]        FLOAT (53) NOT NULL,
    [mGroup]       SMALLINT   NOT NULL,
    [mItemSubType] INT        CONSTRAINT [DF_DT_Bead_mItemSubType] DEFAULT (0) NOT NULL,
    CONSTRAINT [CL_PK_DT_Bead_IID] PRIMARY KEY CLUSTERED ([IID] ASC) WITH (FILLFACTOR = 90)
);


GO

