CREATE TABLE [dbo].[TblServantCombineAddAbility] (
    [SStuffType]   SMALLINT NOT NULL,
    [SIsCoreGold]  BIT      NOT NULL,
    [SIsStuffGold] BIT      NOT NULL,
    [SStrMax]      TINYINT  NOT NULL,
    [SDexMax]      TINYINT  NOT NULL,
    [SIntMax]      TINYINT  NOT NULL,
    [STotalMin]    TINYINT  NOT NULL,
    [STotalMax]    TINYINT  NOT NULL,
    CONSTRAINT [PK_TblServantCombineAddAbility] PRIMARY KEY CLUSTERED ([SStuffType] ASC, [SIsCoreGold] ASC, [SIsStuffGold] ASC),
    CONSTRAINT [CK_TblServantCombineAddAbility_Total] CHECK ([STotalMin]<=[STotalMax])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 刘啊 醚樊 弥措蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'STotalMax';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 刘啊 醚樊 弥家蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'STotalMin';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辑锅飘 钦己 犁丰俊 蝶弗 眠啊 瓷仿摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 犁丰 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'SStuffType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 林 犁丰啊 蜡丰牢瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'SIsCoreGold';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 刮酶 刘啊 弥措蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'SDexMax';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 犁丰啊 蜡丰牢瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'SIsStuffGold';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 塞 刘啊 弥措蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'SStrMax';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 瘤瓷 刘啊 弥措蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantCombineAddAbility', @level2type = N'COLUMN', @level2name = N'SIntMax';


GO

