CREATE TABLE [dbo].[TblMsg] (
    [mMsgGroupID] INT           NOT NULL,
    [mHashID]     BIGINT        NOT NULL,
    [mHashStr]    VARCHAR (100) NOT NULL,
    [mDesc]       VARCHAR (500) NOT NULL,
    CONSTRAINT [PK_CL_TblMsg] PRIMARY KEY CLUSTERED ([mMsgGroupID] ASC, [mHashID] ASC) WITH (FILLFACTOR = 90)
);


GO

