CREATE TABLE [dbo].[TblServantTransformType] (
    [MID]   INT      NOT NULL,
    [TType] SMALLINT NOT NULL,
    CONSTRAINT [UCL_PK_TblServantTransformType] PRIMARY KEY CLUSTERED ([MID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 函脚 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantTransformType', @level2type = N'COLUMN', @level2name = N'TType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 函脚 阁胶磐 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantTransformType', @level2type = N'COLUMN', @level2name = N'MID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辑锅飘 函脚 阁胶磐 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantTransformType';


GO

