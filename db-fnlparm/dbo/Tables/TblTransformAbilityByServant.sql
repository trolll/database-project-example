CREATE TABLE [dbo].[TblTransformAbilityByServant] (
    [mServantTransformType] SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mServantTransformType] DEFAULT ((0)) NOT NULL,
    [mType]                 INT      CONSTRAINT [DF_TblTransformAbilityByServant_mType] DEFAULT ((0)) NOT NULL,
    [mLevel]                INT      CONSTRAINT [DF_TblTransformAbilityByServant_mLevel] DEFAULT ((0)) NOT NULL,
    [mMaxHP]                SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mMaxHP] DEFAULT ((0)) NOT NULL,
    [mMaxMP]                SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mMaxMP] DEFAULT ((0)) NOT NULL,
    [mMaxWeight]            SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mMaxWeight] DEFAULT ((0)) NOT NULL,
    [mShortAttackRate]      SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mShortAttackRate] DEFAULT ((0)) NOT NULL,
    [mLongAttackRate]       SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mLongAttackRate] DEFAULT ((0)) NOT NULL,
    [mMoveRate]             SMALLINT CONSTRAINT [DF_TblTransformAbilityByServant_mMoveRate] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblTransformAbilityByServant] PRIMARY KEY NONCLUSTERED ([mServantTransformType] ASC, [mType] ASC, [mLevel] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'府胶飘 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刘啊登绰 弥措摹 公霸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mMaxWeight';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 函脚 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mServantTransformType';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辑锅飘 函脚 矫 汲沥登绰 瓷仿摹甫 曼炼窍绰 抛捞喉', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刘啊登绰 弥措摹 MP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mMaxMP';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刘啊登绰 捞悼加档', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mMoveRate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刘啊登绰 盔芭府 傍拜加档', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mLongAttackRate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刘啊登绰 弥措摹 HP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mMaxHP';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥家 饭骇', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mLevel';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刘啊登绰 辟芭府 傍拜加档', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblTransformAbilityByServant', @level2type = N'COLUMN', @level2name = N'mShortAttackRate';


GO

