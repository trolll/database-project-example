CREATE TABLE [dbo].[TblInvalidString] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblInvali__mRegD__351DDF8C] DEFAULT (getdate()) NOT NULL,
    [mString]  VARCHAR (12) NOT NULL,
    CONSTRAINT [PkTblInvalidString] PRIMARY KEY CLUSTERED ([mString] ASC) WITH (FILLFACTOR = 90)
);


GO

