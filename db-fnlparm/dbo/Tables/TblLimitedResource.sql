CREATE TABLE [dbo].[TblLimitedResource] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblLimitedResource_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mResourceType] INT           NOT NULL,
    [mMaxCnt]       INT           NOT NULL,
    [mRemainerCnt]  INT           NOT NULL,
    [mRandomVal]    FLOAT (53)    NOT NULL,
    [mUptDate]      SMALLDATETIME CONSTRAINT [DF_TblLimitedResource_mUptDate] DEFAULT (getdate()) NOT NULL,
    [mIsIncSys]     BIT           CONSTRAINT [DF_TblLimitedResource_mIsIncSys] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblLimitedResource_mResourceType] PRIMARY KEY CLUSTERED ([mResourceType] ASC) WITH (FILLFACTOR = 90)
);


GO

