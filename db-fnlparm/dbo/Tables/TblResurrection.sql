CREATE TABLE [dbo].[TblResurrection] (
    [mMID]        INT      NOT NULL,
    [mRMID]       INT      NOT NULL,
    [mRHp]        SMALLINT NOT NULL,
    [mIsItemDrop] BIT      CONSTRAINT [DF_TblResurrection_mIsItemDrop] DEFAULT (0) NOT NULL,
    [mIsExp]      BIT      CONSTRAINT [DF_TblResurrection_mIsExp] DEFAULT (0) NOT NULL,
    [mIsSelf]     BIT      CONSTRAINT [DF_TblResurrection_mIsSelf] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblResurrection] PRIMARY KEY CLUSTERED ([mMID] ASC, [mRMID] ASC) WITH (FILLFACTOR = 90)
);


GO

