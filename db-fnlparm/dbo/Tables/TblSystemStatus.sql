CREATE TABLE [dbo].[TblSystemStatus] (
    [mRegDate]       DATETIME CONSTRAINT [DF__TblSystem__mRegD__369CDF01] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]         SMALLINT NOT NULL,
    [mSessionFree]   INT      NOT NULL,
    [mSendFree]      INT      NOT NULL,
    [mMemoryUsage]   INT      NOT NULL,
    [mBulletFreeCnt] INT      NOT NULL,
    [mDbQueSz]       INT      NOT NULL,
    [mLogQueSz]      INT      NOT NULL,
    [mTmQueSz]       INT      NOT NULL,
    [mWkWaitCnt]     INT      NOT NULL
);


GO

