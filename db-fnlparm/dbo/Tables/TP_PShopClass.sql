CREATE TABLE [dbo].[TP_PShopClass] (
    [PShopClass] TINYINT      NOT NULL,
    [PShop]      VARCHAR (50) NOT NULL,
    CONSTRAINT [UCL_PK_TP_PShopClass] PRIMARY KEY CLUSTERED ([PShopClass] ASC) WITH (FILLFACTOR = 90)
);


GO

