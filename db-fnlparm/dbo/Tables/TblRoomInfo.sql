CREATE TABLE [dbo].[TblRoomInfo] (
    [mID]      INT           NULL,
    [MName]    NVARCHAR (40) NULL,
    [mType]    TINYINT       CONSTRAINT [DF_TblRoomInfo_mType] DEFAULT (0) NOT NULL,
    [mMapNo]   SMALLINT      CONSTRAINT [DF_TblRoomInfo_mMapNo] DEFAULT (0) NOT NULL,
    [mKeyItem] INT           CONSTRAINT [DF_TblRoomInfo_mKeyItem] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_TblRoomInfo] PRIMARY KEY CLUSTERED ([mType] ASC, [mMapNo] ASC, [mKeyItem] ASC) WITH (FILLFACTOR = 90)
);


GO

