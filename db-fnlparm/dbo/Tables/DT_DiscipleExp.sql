CREATE TABLE [dbo].[DT_DiscipleExp] (
    [mLevel]            INT NOT NULL,
    [mExp]              INT NOT NULL,
    [mMaxDiscipleCount] INT NOT NULL,
    CONSTRAINT [CL_PK_DT_DiscipleExp] PRIMARY KEY CLUSTERED ([mLevel] ASC) WITH (FILLFACTOR = 90)
);


GO

