CREATE TABLE [dbo].[TP_ModuleType] (
    [MType]       INT           NOT NULL,
    [MName]       NVARCHAR (40) NULL,
    [MDesc]       NVARCHAR (50) NULL,
    [MAParamName] NVARCHAR (40) NULL,
    [MBParamName] NVARCHAR (40) NULL,
    [MCParamName] NVARCHAR (40) NULL
);


GO

