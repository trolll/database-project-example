CREATE TABLE [dbo].[TblMaterialDrawResult] (
    [mSeq]        INT      NOT NULL,
    [MDRD]        BIGINT   NOT NULL,
    [IID]         INT      NOT NULL,
    [mPerOrRate]  REAL     NULL,
    [mItemStatus] TINYINT  CONSTRAINT [DF_TblMaterialDrawResult_mItemStatus] DEFAULT ((1)) NOT NULL,
    [mCnt]        INT      CONSTRAINT [CK_TblMaterialDrawResult_mCnt] DEFAULT ((1)) NOT NULL,
    [mBinding]    INT      CONSTRAINT [DF_TblMaterialDrawResult_mBinding] DEFAULT ((0)) NOT NULL,
    [mEffTime]    INT      CONSTRAINT [DF_TblMaterialDrawResult_mEffTime] DEFAULT ((0)) NOT NULL,
    [mValTime]    SMALLINT CONSTRAINT [DF_TblMaterialDrawResult_mValTime] DEFAULT ((10000)) NOT NULL,
    [mResource]   INT      CONSTRAINT [DF_TblMaterialDrawResult_mResource] DEFAULT ((0)) NOT NULL,
    [mAddGroup]   TINYINT  CONSTRAINT [DF_TblMaterialDrawResult_mAddGroup] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CK_TblMaterialDrawResult_mItemStatus] CHECK ((0)<=[mItemStatus] AND [mItemStatus]<(3)),
    CONSTRAINT [CK_TblMaterialDrawResult_mPerOrRate] CHECK ([mPerOrRate]<=(100) AND [mPerOrRate]>=(0))
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ °³¼ö (ºñ½ºÅÃÇüÀÏ °æ¿ì¿¡´Â ¹«Á¶°Ç 1)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'±Í¼Ó ¿©ºÎ (0:ÀÏ¹Ý, 1:±Í¼Ó)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mBinding';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mSeq';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°á°ú¹° Å° TblMaterialDrawIndex.MDRD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'MDRD';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'È¿°ú½Ã°£ (±âº»°ª 0)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mEffTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'À¯È¿±â°£ (±âº»°ª 10000)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mValTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°á°ú¹° ¾ÆÀÌÅÛ ID (DT_ITEM.IID)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'IID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼±ÅÃµÉ È®·ü (100000 = 100%, 1 = 0.001%)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mPerOrRate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸®¼Ò½º °ª (À¥ ¾îµå¹Î ÂüÁ¶°ª)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mResource';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'¸ÞÅÍ¸®¾ó »Ì±â ½Ã½ºÅÛ Áß °á°ú¹° Å×ÀÌºí', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ãß°¡È¹µæ ±×·ì', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mAddGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ »óÅÂ (0 : ÀúÁÖ, 1:ÀÏ¹Ý, 2: Ãàº¹)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMaterialDrawResult', @level2type = N'COLUMN', @level2name = N'mItemStatus';


GO

