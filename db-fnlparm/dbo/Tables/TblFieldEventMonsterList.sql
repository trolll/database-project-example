CREATE TABLE [dbo].[TblFieldEventMonsterList] (
    [mKey]          INT        NOT NULL,
    [mEventType]    INT        CONSTRAINT [DF_TblFieldEventMonsterList_mEventType] DEFAULT (0) NOT NULL,
    [mEventMonType] INT        CONSTRAINT [DF_TblFieldEventMonsterList_mEventMonType] DEFAULT (0) NOT NULL,
    [mOrgMonMid]    INT        CONSTRAINT [DF_TblFieldEventMonsterList_mOrgMonMid] DEFAULT (0) NOT NULL,
    [mOrgMonPosX]   FLOAT (53) CONSTRAINT [DF_TblFieldEventMonsterList_mOrgMonPosX] DEFAULT (0) NOT NULL,
    [mOrgMonPosY]   FLOAT (53) CONSTRAINT [DF_TblFieldEventMonsterList_mOrgMonPosY] DEFAULT (0) NOT NULL,
    [mOrgMonPosZ]   FLOAT (53) CONSTRAINT [DF_TblFieldEventMonsterList_mOrgMonPosZ] DEFAULT (0) NOT NULL,
    [mEventMonMid]  INT        CONSTRAINT [DF_TblFieldEventMonsterList_mEventMonMid] DEFAULT (0) NOT NULL,
    [mEventMonPosX] FLOAT (53) CONSTRAINT [DF_TblFieldEventMonsterList_mEventMonPosX] DEFAULT (0) NOT NULL,
    [mEventMonPosY] FLOAT (53) CONSTRAINT [DF_TblFieldEventMonsterList_mEventMonPosY] DEFAULT (0) NOT NULL,
    [mEventMonPosZ] FLOAT (53) CONSTRAINT [DF_TblFieldEventMonsterList_mEventMonPosZ] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblFieldEventMonsterList_mKey] PRIMARY KEY CLUSTERED ([mKey] ASC) WITH (FILLFACTOR = 90)
);


GO

