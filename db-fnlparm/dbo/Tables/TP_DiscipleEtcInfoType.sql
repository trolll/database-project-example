CREATE TABLE [dbo].[TP_DiscipleEtcInfoType] (
    [mType] INT        NOT NULL,
    [mNm]   CHAR (32)  CONSTRAINT [DF_TP_DiscipleEtcInfoType_mNm] DEFAULT ('') NOT NULL,
    [mDesc] CHAR (128) CONSTRAINT [DF_TP_DiscipleEtcInfoType_mDesc] DEFAULT ('') NOT NULL,
    CONSTRAINT [CL_PK_TP_DiscipleEtcInfoType] PRIMARY KEY CLUSTERED ([mType] ASC) WITH (FILLFACTOR = 90)
);


GO

