CREATE TABLE [dbo].[TblWorld] (
    [mRegDate] DATETIME  CONSTRAINT [DF__TblWorld__mRegDa__0C1BC9F9] DEFAULT (getdate()) NOT NULL,
    [mWorldNo] SMALLINT  NOT NULL,
    [mWorldNm] CHAR (50) NOT NULL,
    CONSTRAINT [PkTblWorld] PRIMARY KEY CLUSTERED ([mWorldNo] ASC) WITH (FILLFACTOR = 90),
    CHECK (0 <= [mWorldNo])
);


GO

