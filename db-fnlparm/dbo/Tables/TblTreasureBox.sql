CREATE TABLE [dbo].[TblTreasureBox] (
    [mIdx]    INT        IDENTITY (1, 1) NOT NULL,
    [mMID]    INT        NOT NULL,
    [mType]   SMALLINT   NOT NULL,
    [mIID]    INT        NOT NULL,
    [mAmount] SMALLINT   NOT NULL,
    [mDice]   INT        NOT NULL,
    [mStatus] SMALLINT   NOT NULL,
    [mDesc]   CHAR (200) NULL
);


GO

