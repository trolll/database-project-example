CREATE TABLE [dbo].[TblHalidom] (
    [mRegDate] DATETIME   DEFAULT (getdate()) NOT NULL,
    [mNo]      BIGINT     NOT NULL,
    [mPosX]    FLOAT (53) NOT NULL,
    [mPosY]    FLOAT (53) NOT NULL,
    [mPosZ]    FLOAT (53) NOT NULL,
    [mWidth]   FLOAT (53) NOT NULL,
    [mDir]     FLOAT (53) NOT NULL,
    [mDesc]    CHAR (50)  NOT NULL,
    [mGroup]   BIGINT     NOT NULL,
    CONSTRAINT [UCL_TblHalidom] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

