CREATE TABLE [dbo].[TblInvalidUserId] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblInvali__mRegD__37FA4C37] DEFAULT (getdate()) NOT NULL,
    [mString]  VARCHAR (12) NOT NULL,
    CONSTRAINT [PkTblInvalidUserId] PRIMARY KEY CLUSTERED ([mString] ASC) WITH (FILLFACTOR = 90)
);


GO

