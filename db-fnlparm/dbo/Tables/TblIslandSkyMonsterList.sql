CREATE TABLE [dbo].[TblIslandSkyMonsterList] (
    [mMID]       INT     NOT NULL,
    [mCamp]      TINYINT CONSTRAINT [DF_TblIsLandSkyMonsterList_mCamp] DEFAULT (0) NULL,
    [mIsEvent]   BIT     CONSTRAINT [DF_TblIsLandSkyMonsterList_mIsEvent] DEFAULT (0) NULL,
    [mIsHunting] BIT     CONSTRAINT [DF_TblIsLandSkyMonsterList_mIsHunting] DEFAULT (0) NULL
);


GO

