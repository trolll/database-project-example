CREATE TABLE [dbo].[TblUserQuest] (
    [mUserNo]     INT      NOT NULL,
    [mQuestNo]    INT      NOT NULL,
    [mValue]      INT      CONSTRAINT [DF__TblUserQu__mValu__37B03374] DEFAULT (0) NOT NULL,
    [mUpdateDate] DATETIME CONSTRAINT [DF__TblUserQu__mUpda__38A457AD] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblUserQuest] PRIMARY KEY CLUSTERED ([mUserNo] ASC, [mQuestNo] ASC) WITH (FILLFACTOR = 90)
);


GO

