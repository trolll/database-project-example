CREATE TABLE [dbo].[TblMonitorItem] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblMonito__mRegD__11BF94B6] DEFAULT (getdate()) NOT NULL,
    [mItemNo]  INT           NOT NULL,
    [mName]    VARCHAR (40)  NOT NULL,
    CONSTRAINT [PkTblMonitorItem] PRIMARY KEY NONCLUSTERED ([mItemNo] ASC) WITH (FILLFACTOR = 90)
);


GO

