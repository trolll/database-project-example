CREATE TABLE [dbo].[TblCastleStone] (
    [mNo]      BIGINT     NOT NULL,
    [mPosx]    FLOAT (53) NOT NULL,
    [mPosy]    FLOAT (53) NOT NULL,
    [mPosz]    FLOAT (53) NOT NULL,
    [mWidth]   FLOAT (53) NOT NULL,
    [mDir]     FLOAT (53) CONSTRAINT [DF_TblCastleStone_mDir] DEFAULT (0) NOT NULL,
    [mDesc]    CHAR (50)  CONSTRAINT [DF_TblCastleStone_mDesc] DEFAULT ('') NOT NULL,
    [mRegDate] DATETIME   CONSTRAINT [DF_TblCastleStone_mRegDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_TblCastleStone] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

