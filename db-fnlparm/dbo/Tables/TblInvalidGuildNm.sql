CREATE TABLE [dbo].[TblInvalidGuildNm] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblInvali__mRegD__338A9CD5] DEFAULT (getdate()) NOT NULL,
    [mString]  VARCHAR (12) NOT NULL,
    CONSTRAINT [PkTblInvalidGuildNm] PRIMARY KEY CLUSTERED ([mString] ASC) WITH (FILLFACTOR = 90)
);


GO

