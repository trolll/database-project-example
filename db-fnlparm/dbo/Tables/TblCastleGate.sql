CREATE TABLE [dbo].[TblCastleGate] (
    [mNo]      BIGINT     NOT NULL,
    [mPosx]    FLOAT (53) NOT NULL,
    [mPosy]    FLOAT (53) NOT NULL,
    [mPosz]    FLOAT (53) NOT NULL,
    [mWidth]   FLOAT (53) NOT NULL,
    [mDir]     FLOAT (53) CONSTRAINT [DF_TblCastleGate_mDir] DEFAULT (0) NOT NULL,
    [mDesc]    CHAR (50)  CONSTRAINT [DF_TblCastleGate_mDesc] DEFAULT ('') NOT NULL,
    [mRegDate] DATETIME   CONSTRAINT [DF_TblCastleGate_mRegDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_TblCastleGate] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

