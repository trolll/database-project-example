CREATE TABLE [dbo].[DT_SkillEnhancementMaterial] (
    [mESPID]   INT     NOT NULL,
    [mOrderNo] TINYINT NOT NULL,
    [mItemID]  INT     NOT NULL,
    [mCnt]     INT     NOT NULL,
    CONSTRAINT [UCL_PK_DT_SkillEnhancementMaterial] PRIMARY KEY CLUSTERED ([mESPID] ASC, [mOrderNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancementMaterial', @level2type = N'COLUMN', @level2name = N'mItemID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼ø¼­', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancementMaterial', @level2type = N'COLUMN', @level2name = N'mOrderNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°­È­µÈ ½ºÅ³ÆÑ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancementMaterial', @level2type = N'COLUMN', @level2name = N'mESPID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼ö·®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancementMaterial', @level2type = N'COLUMN', @level2name = N'mCnt';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Æ¯¼º °­È­ ½ºÅ³ Àç·á', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DT_SkillEnhancementMaterial';


GO

