CREATE TABLE [dbo].[TblQuestInfo] (
    [mQuestNo] INT           NOT NULL,
    [mType]    TINYINT       NOT NULL,
    [mParmA]   INT           CONSTRAINT [DF_TblQuestInfo_mParmA] DEFAULT (0) NULL,
    [mParmB]   INT           CONSTRAINT [DF_TblQuestInfo_mParmB] DEFAULT (0) NULL,
    [mParmC]   INT           CONSTRAINT [DF_TblQuestInfo_mParmC] DEFAULT (0) NULL,
    [mDesc]    VARCHAR (100) NULL,
    [mSeqNo]   INT           NOT NULL,
    CONSTRAINT [UCL_PK_TblQuestInfo_mQuestNo] PRIMARY KEY CLUSTERED ([mQuestNo] ASC, [mType] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblQuestInfo_mSeqNo]
    ON [dbo].[TblQuestInfo]([mSeqNo] ASC) WITH (FILLFACTOR = 90);


GO

