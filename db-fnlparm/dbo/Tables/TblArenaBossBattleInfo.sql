CREATE TABLE [dbo].[TblArenaBossBattleInfo] (
    [mPlace]  INT          CONSTRAINT [DF_TblArenaBossBattleInfo_mPlace] DEFAULT ((0)) NOT NULL,
    [mStartX] FLOAT (53)   CONSTRAINT [DF_TblArenaBossBattleInfo_mStartX] DEFAULT ((0)) NOT NULL,
    [mStartZ] FLOAT (53)   CONSTRAINT [DF_TblArenaBossBattleInfo_mStartZ] DEFAULT ((0)) NOT NULL,
    [mEndX]   FLOAT (53)   CONSTRAINT [DF_TblArenaBossBattleInfo_mEndX] DEFAULT ((0)) NOT NULL,
    [mEndZ]   FLOAT (53)   CONSTRAINT [DF_TblArenaBossBattleInfo_mEndZ] DEFAULT ((0)) NOT NULL,
    [mDesc]   VARCHAR (50) NULL,
    CONSTRAINT [UCL_PK_TblArenaBossBattleInfo_mPlace] PRIMARY KEY CLUSTERED ([mPlace] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'矫累 X谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo', @level2type = N'COLUMN', @level2name = N'mStartX';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'焊胶傈 冀磐 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'场 X谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo', @level2type = N'COLUMN', @level2name = N'mEndX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'带傈 汲疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo', @level2type = N'COLUMN', @level2name = N'mDesc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PlaceNo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'矫累 Z谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo', @level2type = N'COLUMN', @level2name = N'mStartZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'场 Z谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblArenaBossBattleInfo', @level2type = N'COLUMN', @level2name = N'mEndZ';


GO

