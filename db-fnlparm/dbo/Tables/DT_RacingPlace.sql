CREATE TABLE [dbo].[DT_RacingPlace] (
    [mPlace]  INT NOT NULL,
    [mIsOpen] BIT NOT NULL,
    CONSTRAINT [PK_DT_RacingPlace] PRIMARY KEY CLUSTERED ([mPlace] ASC) WITH (FILLFACTOR = 90)
);


GO

