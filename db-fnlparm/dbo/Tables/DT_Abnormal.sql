CREATE TABLE [dbo].[DT_Abnormal] (
    [AID]      INT           NOT NULL,
    [AType]    INT           NULL,
    [ALevel]   TINYINT       NULL,
    [APercent] TINYINT       NULL,
    [ATime]    INT           CONSTRAINT [DF_DT_Abnormal_ATime] DEFAULT (0) NULL,
    [AGrade]   TINYINT       CONSTRAINT [DF_DT_Abnormal_AGrade] DEFAULT (0) NOT NULL,
    [ADesc]    VARCHAR (200) NULL
);


GO

