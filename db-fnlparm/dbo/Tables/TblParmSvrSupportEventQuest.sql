CREATE TABLE [dbo].[TblParmSvrSupportEventQuest] (
    [mSvrNo]     SMALLINT      NOT NULL,
    [mEQuestNo]  INT           NOT NULL,
    [mBeginDate] SMALLDATETIME NOT NULL,
    [mEndDate]   SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblParmSvrSupportEventQuest_mSvrNo] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mEQuestNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TblParmSvrSupportEventQuest_TblEventQuest] FOREIGN KEY ([mEQuestNo]) REFERENCES [dbo].[TblEventQuest] ([mEQuestNo]),
    CONSTRAINT [FK_TblParmSvrSupportEventQuest_TblParmSvr] FOREIGN KEY ([mSvrNo]) REFERENCES [dbo].[TblParmSvr] ([mSvrNo])
);


GO

