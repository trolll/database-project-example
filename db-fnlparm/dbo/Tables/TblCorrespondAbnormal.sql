CREATE TABLE [dbo].[TblCorrespondAbnormal] (
    [mOrigAID]   INT      NOT NULL,
    [mCorrAID]   INT      NOT NULL,
    [mType]      TINYINT  NOT NULL,
    [mCorrSkill] INT      NOT NULL,
    [mUsePerHP]  SMALLINT DEFAULT (0) NULL,
    [mUsePerMP]  SMALLINT DEFAULT (0) NULL,
    [mMaxCnt]    TINYINT  DEFAULT (0) NULL,
    [mPercent]   SMALLINT NOT NULL,
    CONSTRAINT [UCL_TblCorrespondAbnormal] PRIMARY KEY CLUSTERED ([mOrigAID] ASC, [mCorrAID] ASC) WITH (FILLFACTOR = 90)
);


GO

