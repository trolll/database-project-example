CREATE TABLE [dbo].[DT_ItemBeadModule] (
    [IID] INT NOT NULL,
    [MID] INT NOT NULL,
    CONSTRAINT [UNC_DT_ItemBeadModule_IID_MID] UNIQUE NONCLUSTERED ([IID] ASC, [MID] ASC) WITH (FILLFACTOR = 90)
);


GO

