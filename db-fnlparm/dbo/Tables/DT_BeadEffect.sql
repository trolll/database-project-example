CREATE TABLE [dbo].[DT_BeadEffect] (
    [mBeadNo]      INT           NOT NULL,
    [mName]        NVARCHAR (40) NULL,
    [mBeadType]    INT           NOT NULL,
    [mChkGroup]    TINYINT       NOT NULL,
    [mPercent]     FLOAT (53)    NOT NULL,
    [mApplyTarget] TINYINT       NOT NULL,
    [mParamA]      INT           NOT NULL,
    [mParamB]      INT           NOT NULL,
    [mParamC]      INT           NOT NULL,
    [mParamD]      INT           NOT NULL,
    [mParamE]      INT           NOT NULL,
    CONSTRAINT [CL_PK_DT_BeadEffect_mBeadNo] PRIMARY KEY CLUSTERED ([mBeadNo] ASC) WITH (FILLFACTOR = 90),
    CHECK ([mBeadNo] <> 0),
    CHECK ([mBeadType] <> 0),
    CHECK ([mChkGroup] <> 0)
);


GO

