CREATE TABLE [dbo].[TblServantSkillTree] (
    [IID]   INT     NOT NULL,
    [SStep] TINYINT NOT NULL,
    [STID1] INT     CONSTRAINT [DF_TblServantSkillTree_STID1] DEFAULT ((0)) NOT NULL,
    [STID2] INT     CONSTRAINT [DF_TblServantSkillTree_STID2] DEFAULT ((0)) NOT NULL,
    [STID3] INT     CONSTRAINT [DF_TblServantSkillTree_STID3] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CL_PK_TblServantSkillTree] PRIMARY KEY CLUSTERED ([IID] ASC, [SStep] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ®º° ½ºÅ³Æ®¸®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantSkillTree';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³Æ®¸®¾ÆÀÌµð3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantSkillTree', @level2type = N'COLUMN', @level2name = N'STID3';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³Æ®¸®¾ÆÀÌµð1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantSkillTree', @level2type = N'COLUMN', @level2name = N'STID1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³Æ®¸®¾ÆÀÌµð2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantSkillTree', @level2type = N'COLUMN', @level2name = N'STID2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³Æ®¸® ±×·ì ¼ø¼­', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantSkillTree', @level2type = N'COLUMN', @level2name = N'SStep';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblServantSkillTree', @level2type = N'COLUMN', @level2name = N'IID';


GO

