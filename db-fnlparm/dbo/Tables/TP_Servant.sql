CREATE TABLE [dbo].[TP_Servant] (
    [SType]        SMALLINT       NOT NULL,
    [STypeName]    NVARCHAR (40)  NOT NULL,
    [STypeNameKey] NVARCHAR (128) NOT NULL,
    CONSTRAINT [CL_PK_TP_Servant] PRIMARY KEY CLUSTERED ([SType] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Å¸ÀÔ ÀÌ¸§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_Servant', @level2type = N'COLUMN', @level2name = N'STypeName';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_Servant', @level2type = N'COLUMN', @level2name = N'SType';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® Å¸ÀÔ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_Servant';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Å¸ÀÔ ÀÌ¸§ Å°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_Servant', @level2type = N'COLUMN', @level2name = N'STypeNameKey';


GO

