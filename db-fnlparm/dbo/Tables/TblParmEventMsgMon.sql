CREATE TABLE [dbo].[TblParmEventMsgMon] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF_TblParmEventMsgMon_mRegDate] DEFAULT (getdate()) NOT NULL,
    [MID]      INT           NOT NULL,
    CONSTRAINT [UCL_TblParmEventMsgMon_MID] PRIMARY KEY CLUSTERED ([MID] ASC) WITH (FILLFACTOR = 90)
);


GO

