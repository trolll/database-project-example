CREATE TABLE [dbo].[DT_Racing] (
    [mNID]     INT       NOT NULL,
    [mPlace]   INT       NOT NULL,
    [mNpcNm]   CHAR (14) NOT NULL,
    [mNpcNo]   INT       NOT NULL,
    [mAbility] INT       NOT NULL,
    CONSTRAINT [PK_DT_Racing] PRIMARY KEY CLUSTERED ([mNID] ASC) WITH (FILLFACTOR = 90)
);


GO

