CREATE TABLE [dbo].[DT_DiscipleEtcInfo] (
    [mType]  INT NOT NULL,
    [mValue] INT NOT NULL,
    CONSTRAINT [CL_PK_DT_DiscipleEtcInfo] PRIMARY KEY CLUSTERED ([mType] ASC) WITH (FILLFACTOR = 90)
);


GO

