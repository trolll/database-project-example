CREATE TABLE [dbo].[TP_GMTransformInfo] (
    [mGroupID] INT NOT NULL,
    [AID]      INT NOT NULL
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'函脚府胶飘 GroupID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_GMTransformInfo', @level2type = N'COLUMN', @level2name = N'mGroupID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'GM 函脚府胶飘 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_GMTransformInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑怕捞惑 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TP_GMTransformInfo', @level2type = N'COLUMN', @level2name = N'AID';


GO

