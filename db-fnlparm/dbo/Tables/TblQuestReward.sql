CREATE TABLE [dbo].[TblQuestReward] (
    [mRewardNo] INT     NOT NULL,
    [mExp]      BIGINT  NOT NULL,
    [mID]       INT     NOT NULL,
    [mCnt]      INT     NOT NULL,
    [mBinding]  TINYINT NOT NULL,
    [mStatus]   TINYINT NOT NULL,
    [mEffTime]  INT     CONSTRAINT [DF_TblQuestReward_mEffTime] DEFAULT (0) NOT NULL,
    [mValTime]  INT     CONSTRAINT [DF_TblQuestReward_mValTime] DEFAULT (0) NOT NULL,
    CONSTRAINT [UCL_PK_TblQuestReward_mRewardNo] PRIMARY KEY CLUSTERED ([mRewardNo] ASC, [mExp] ASC, [mID] ASC) WITH (FILLFACTOR = 90)
);


GO

