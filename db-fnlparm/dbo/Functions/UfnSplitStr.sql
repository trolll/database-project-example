CREATE FUNCTION [dbo].[UfnSplitStr]
  (@string varchar(8000), @separator CHAR(1) = N',') 
RETURNS TABLE
AS
RETURN
  SELECT
    mNo - LEN(REPLACE(LEFT(s, mNo), @separator, '')) + 1 AS pos,
    SUBSTRING(s, mNo,
      CHARINDEX(@separator, s + @separator, mNo) - mNo) AS element
  FROM (SELECT @string AS s) AS D
    JOIN dbo.TblNums
      ON mNo <= LEN(s)
      AND SUBSTRING(@separator + s, mNo, 1) = @separator

GO

