CREATE FUNCTION dbo.UfnGetExpireEventItem
(
	@pSvrNo	SMALLINT
	,@pDate	SMALLDATETIME	-- 泅犁老
) RETURNS TABLE
AS
RETURN 
(

	SELECT 
		mObjID
	FROM dbo.TblEventObj WITH(NOLOCK)
	WHERE mEventID IN (
		SELECT  mEventID
		FROM dbo.TblParmSvrSupportEvent  WITH(NOLOCK)
		WHERE mSvrNo = @pSvrNo
			AND mEndDate <= @pDate )
		AND mObjType = 1	-- item object 
	GROUP BY mObjID

)

GO

