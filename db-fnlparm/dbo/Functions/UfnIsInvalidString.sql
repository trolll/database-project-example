CREATE FUNCTION dbo.UfnIsInvalidString
(
	 @pString	VARCHAR(100)
) RETURNS INT
--------------WITH ENCRYPTION
AS
BEGIN
	DECLARE @aCnt	INT
	SET @aCnt = 0
	
	-- [String]가 VARCHAR이 아닌 CHAR이라면 RTRIM()을 해야 한다.	
	--IF EXISTS(SELECT * FROM TblInvalidString WHERE @pString LIKE '%'+[mString]+'%')
	IF EXISTS(SELECT * FROM TblInvalidString WHERE @pString = [mString])
	 BEGIN
		SET @aCnt = 1
	 END
 
	RETURN @aCnt
END

GO

